package iiride.app.rider.comman;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.google.gson.Gson;

import iiride.app.rider.been.initData.InitData;

import static iiride.app.rider.other.Global.context;

/**
 * This common class to store the require data by using SharedPreferences.
 */
public class SessionSave {
    //Store data's into sharedPreference
    public static void saveUserSession(String key, String value, Context context) {
        Editor editor = context.getSharedPreferences(Common.PREFRENCE_USER, Activity.MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.apply();
    }

    // Get Data's from SharedPreferences
    public static String getUserSession(String key, Context context) {
        SharedPreferences prefs = context.getSharedPreferences(Common.PREFRENCE_USER, Activity.MODE_PRIVATE);
        return prefs.getString(key, "");
    }

    public static void saveInit(String key, String value, Context context) {
        Editor editor = context.getSharedPreferences(Common.INIT_PREF, Activity.MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static InitData getInitData(){
        if(context != null) {
            SharedPreferences prefs = context.getSharedPreferences(Common.INIT_PREF, Activity.MODE_PRIVATE);
            return new Gson().fromJson(prefs.getString(Common.INIT_DATA, ""), InitData.class);
        }
        return null;
    }
}

package iiride.app.rider.comman;

/**
 * Created by user on 4/13/2016.
 */
public interface WebServiceAPI {

//    Socket URL : http://13.54.230.131:8080
//    Webservice URL : http://13.54.230.131/

//    //Base Url
//    String BASE_URL = "http://54.206.55.185/web/Passenger_Api/";
//    String BASE_URL_IMAGE  = "http://54.206.55.185/web/";
//    public static String HEADER_KEY = "key";
//    public static String HEADER_VALUE = "TicktocApp123*";

    //Base Url
    String BASE_URL = "https://iiride.com/Passenger_Api/";//"http://3.105.155.250/web/Passenger_Api/";//"https://www.ticktoc.net/web/Passenger_Api/";
    String BASE_URL_IMAGE  = "https://iiride.com/";//"http://3.105.155.250/web/";//"https://www.ticktoc.net/web/";

    public static String HEADER_KEY = "key";
    public static String HEADER_VALUE = "iiRide@123*#*";

    //ALL API.............
    public static String API_REGISTER = BASE_URL + "Register";
    public static String API_GET_CLASS_TYPE = BASE_URL + "GetCarClass";
    public static String API_LOGIN = BASE_URL + "Login";
    public static String API_FORGOT_PASSWORD = BASE_URL + "ForgotPassword";
    public static String API_BOOKING_REQUEST = BASE_URL + "SubmitBookingRequest";
    public static String API_ADVANCE_BOOKING = BASE_URL + "AdvancedBooking";
    public static String API_UPDATE_BOOKING_DETAIL = BASE_URL + "UpdateBookingDetails";
    public static String API_BOOKING_HISTORY = BASE_URL + "BookingHistory/";
    public static String API_SHOW_ALL_DRIVER = BASE_URL + "Driver/";
    public static String API_CHANGE_PASSWORD = BASE_URL + "ChangePassword";
    public static String API_UPDATE_PROFILE = BASE_URL + "UpdateProfile";
    public static String API_GET_ESTIMATED_FARE = BASE_URL + "GetEstimateFare";
    public static String API_CURRENT_BOOCING = BASE_URL + "CurrentBooking/";
    public static String API_ADD_CARD = BASE_URL + "AddNewCard";
    public static String API_ADD_MONEY = BASE_URL + "AddMoney";
    public static String API_GET_ALL_CARDS = BASE_URL + "Cards/";
    public static String API_GET_TRANSACTION_HISTORY = BASE_URL + "TransactionHistory/";
    public static String API_SEND_MONEY = BASE_URL + "SendMoney";
    public static String API_QR_CODE_DETAIL = BASE_URL + "QRCodeDetails";
    public static String API_REMOVE_CARD = BASE_URL + "RemoveCard/";
    public static String API_TICK_PAY = BASE_URL + "Tickpay";
    public static String API_ADD_ADDRESS = BASE_URL + "AddAddress";
    public static String API_GET_ADDRESS = BASE_URL + "GetAddress/";
    public static String API_REMOVE_ADDRESS = BASE_URL + "RemoveAddress/";
    public static String API_VERIFY_USER = BASE_URL + "VarifyUser";
    public static String API_GET_TICKPAY_RATE = BASE_URL + "GetTickpayRate";
    public static String API_TICKPAY_INVOICE = BASE_URL + "TickpayInvoice";
    public static String API_CHECK_VERSION = BASE_URL + "Init/";
    public static String API_GIVE_RATTING = BASE_URL + "ReviewRating";
    public static String API_GET_APPROVAL_STATUS = BASE_URL + "GetTickpayApprovalStatus/";
    public static String API_TRANSFER_TO_BANK = BASE_URL + "TransferToBank";
    public static String API_UPDATE_BANK_ACCOUNT_DETAIL = BASE_URL + "UpdateBankAccountDetails";
    public static String API_MOBILE_EMAIL_OTP = BASE_URL + "OtpForRegister";
    String API_CAR_LIST = BASE_URL+"CityWiseCarClass/";
    public static String API_PREVIOUS_DUE = BASE_URL + "PastDues/";
    public static String API_PAST_DUES_PAYOUT = BASE_URL+"PastDuesPayout";

    //ALL PARAMETER............
    public static String PARAM_EMAIL = "Email";
    public static String PARAM_MOBILE_NUMBER = "MobileNo";
    public static String PARAM_PASSWORD = "Password";
    public static String PARAM_GENDER = "Gender";
    public static String PARAM_FIRST_NAME = "Firstname";
    public static String PARAM_LAST_NAME = "Lastname";
    public static String PARAM_DEVICE_TYPE = "DeviceType";
    public static String PARAM_TOKEN = "Token";
    public static String PARAM_LAT = "Lat";
    public static String PARAM_LONG = "Lng";
    public static String PARAM_IMAGE = "Image";
    public static String PARAM_PASSPORT = "Passport";
    public static String PARAM_REFERRAL_CODE = "ReferralCode";
    public static String PARAM_USER_NAME = "Username";
    public static String PARAM_PASSENGER_ID = "PassengerId";
    public static String PARAM_MODEL_ID = "ModelId";
    public static String PARAM_FULL_NAME = "Fullname";
    public static String PARAM_ADDRESS = "Address";
    public static String PARAM_PICKUP_LOCATION = "PickupLocation";
    public static String PARAM_DROPOFF_LOCATION = "DropoffLocation";
    public static String PARAM_PICKUP_LAT = "PickupLat";
    public static String PARAM_PICKUP_LONG = "PickupLng";
    public static String PARAM_DROPOFF_LAT = "DropOffLat";
    public static String PARAM_DROPOFF_LONG = "DropOffLon";
    public static String PARAM_PASSENGER_TYPE = "PassengerType"; //(myself,other)
    public static String PARAM_PASSENGER_NAME = "PassengerName"; //
    public static String PARAM_PASSENGER_CONTACT = "PassengerContact";
    public static String PARAM_PICKUP_DATE_TIME = "PickupDateTime";
    public static String PARAM_FLIGHT_NUMBER = "FlightNumber";
    public static String PARAM_IDS = "Ids";
    public static String PARAM_PICKUP_LONG_ = "PickupLong";
    public static String PARAM_PROMO_CODE = "PromoCode";
    public static String PARAM_NOTES = "Notes";
    public static String PARAM_CARD_NUMBER = "CardNo";
    public static String PARAM_CARD_CVV = "Cvv";
    public static String PARAM_ALIAS = "Alias";
    public static String PARAM_CARD_EXPIRY = "Expiry";
    public static String PARAM_AMOUNT = "Amount";
    public static String PARAM_CARD_ID = "CardId";
    public static String PARAM_SENDER_ID= "SenderId";
    public static String PARAM_QR_CODE = "QRCode";
    public static String PARAM_PAYMENT_TYPE = "PaymentType";
    public static String PARAM_NAME = "Name";
    public static String PARAM_ABN = "ABN";
    public static String PARAM_CARD_CVV_CAP = "CVV";
    public static String PARAM_TYPE = "Type";
    public static String PARAM_COMPANY_NAME = "CompanyName";
    public static String PARAM_TICKPAY_ID = "TickpayId";
    public static String PARAM_INVOICE_TYPE = "InvoiceType";
    public static String PARAM_INVOICE_TYPE_SMS = "SMS";
    public static String PARAM_INVOICE_TYPE_EMAIL = "Email";
    public static String PARAM_CUSTOMER_NAME = "CustomerName";
    public static String PARAM_BOOKING_ID= "BookingId";
    public static String PARAM_RATTING = "Rating";
    public static String PARAM_COMMENT = "Comment";
    public static String PARAM_BOOKING_TYPE= "BookingType";
    public static String BOOK_NOW = "BookNow";
    public static String BOOK_LATER = "BookLater";
    public static String PARAM_HOLDER_NAME = "HolderName";
    public static String PARAM_BANK_NAME = "BankName";
    public static String PARAM_BSB = "BSB";
    public static String PARAM_ACCOUNT_NO = "AccountNo";
    public static String PARAM_ACCOUNT_HOLDER_NAME = "AccountHolderName";
    public static String PARAM_BANK_ACCOUNT_NUMBER = "BankAccountNo";
    public static String PARAM_DESCRIPTION = "Description";
    public static String PARAM_BABYSEAT = "BabySeat";
    public static String PARAM_CITYID = "CityId";
    public static String PARAM_PAST_DUE_ID="PastDuesId";

    String PARAM_PICKUP_SUBURB = "PickupSuburb";
    String PARAM_DROPOFF_SUBURB = "DropoffSuburb";
}

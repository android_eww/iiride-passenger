package iiride.app.rider.fragment;

/**
 * Created by ADMIN on 10/23/2016.
 */
import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.hendraanggrian.widget.ExpandableRecyclerView;

import iiride.app.rider.R;
import iiride.app.rider.activity.MyBookingActivity;
import iiride.app.rider.adapter.UpComming_Adapter;
import iiride.app.rider.been.OnGoing_Been;
import iiride.app.rider.been.PastBooking_Been;
import iiride.app.rider.been.UpComming_Been;
import iiride.app.rider.comman.Common;
import iiride.app.rider.comman.SessionSave;
import iiride.app.rider.comman.WebServiceAPI;
import iiride.app.rider.other.Global;
import iiride.app.rider.other.InternetDialog;

import org.json.JSONArray;
import org.json.JSONObject;

public class UpComming_Fragment extends Fragment {

    private ExpandableRecyclerView recyclerView;
    private ExpandableRecyclerView.Adapter mAdapter;
    private ExpandableRecyclerView.LayoutManager layoutManager;
    private TextView tv_NoDataFound;

    private String TAG = "CallMyRecipt";
    private AQuery aQuery;
    public SwipeRefreshLayout swipeRefreshLayout;

    public UpComming_Fragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_up_comming, container, false);

        aQuery = new AQuery(getActivity());
        recyclerView = (ExpandableRecyclerView) view.findViewById(R.id.rv_dispatched_job);
        tv_NoDataFound = (TextView) view.findViewById(R.id.tv_NoDataFound);

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);

        if (MyBookingActivity.upComming_beens!=null && MyBookingActivity.upComming_beens.size()>0)
        {
            recyclerView.setVisibility(View.VISIBLE);
            tv_NoDataFound.setVisibility(View.GONE);
        }
        else
        {
            recyclerView.setVisibility(View.GONE);
            tv_NoDataFound.setVisibility(View.VISIBLE);
        }

        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new UpComming_Adapter((LinearLayoutManager) layoutManager);
        recyclerView.setAdapter(mAdapter);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (Global.isNetworkconn(getActivity()))
                {
                    swipeRefreshLayout.setRefreshing(true);
                    call_BookingHistoryApi();
                }
                else
                {
                    InternetDialog internetDialog = new InternetDialog(getActivity());
                    internetDialog.showDialog("Please check your internet connection!");
                }
            }
        });

        ActivityCompat.requestPermissions(getActivity(),new String[]{Manifest.permission.WRITE_CALENDAR}, 0);

        return view;
    }

    public void call_BookingHistoryApi()
    {
        String url = WebServiceAPI.API_BOOKING_HISTORY + SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID,getActivity());

        MyBookingActivity.onGoing_beens.clear();
        MyBookingActivity.upComming_beens.clear();
        MyBookingActivity.pastBooking_beens.clear();

        Log.e("call", "url = " + url);

        aQuery.ajax(url.trim(), null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e("responseCode", " = " + responseCode);
                    Log.e("Response", " = " + json);

                    if (json!=null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                if (json.has("history"))
                                {
                                    JSONArray history = json.getJSONArray("history");

                                    if (history!=null && history.length()>0)
                                    {
                                        for (int i=0; i<history.length(); i++)
                                        {
                                            JSONObject jsonObject = history.getJSONObject(i);

                                            if (jsonObject!=null)
                                            {
                                                if (jsonObject.has("Status"))
                                                {
                                                    String Status1 = jsonObject.getString("Status");

                                                    if (Status1!=null && !Status1.trim().equalsIgnoreCase(""))
                                                    {
                                                        if (Status1.equalsIgnoreCase("pending") || Status1.equalsIgnoreCase("accepted"))
                                                        {
                                                            String Id="",CompanyId="",PassengerId="",ModelId="",DriverId="",CreatedDate="",TransactionId="",PaymentStatus="";
                                                            String PickupTime="",PickupDateTime="",DropTime="",TripDuration="",TripDistance="",PickupLocation="",DropoffLocation="";
                                                            String NightFareApplicable="",NightFare="",TripFare="",WaitingTime="",WaitingTimeCost="",TollFee="",BookingCharge="";
                                                            String Tax="",PromoCode="",CompanyTax="",SubTotal="",GrandTotal="",Status="",Reason="",PaymentType="",AdminAmount="";
                                                            String CompanyAmount="",PassengerType="",PassengerName="",PassengerContact="",FlightNumber="",Model="",DriverName="";
                                                            String HistoryType="",BookingType="",DropOffDateTime="",DriverMobileNo="",Notes="",BabySeat="",CardId="";
                                                            String PickupLat="",PickupLng="",DropOffLat="",DropOffLon="",PickupSuburb="",DropoffSuburb="";

                                                            String CarDetails_Id="",CarDetails_CompanyId="",CarDetails_DriverId="",CarDetails_VehicleModel="";
                                                            String CarDetails_Company="",CarDetails_Color="",CarDetails_VehicleRegistrationNo="",CarDetails_RegistrationCertificate="";
                                                            String CarDetails_VehicleInsuranceCertificate="",CarDetails_RegistrationCertificateExpire="",CarDetails_VehicleInsuranceCertificateExpire="";
                                                            String CarDetails_VehicleImage="",CarDetails_Description="";

                                                            if (jsonObject.has("Id"))
                                                            {
                                                                Id = jsonObject.getString("Id");
                                                            }

                                                            if (jsonObject.has("CompanyId"))
                                                            {
                                                                CompanyId = jsonObject.getString("CompanyId");
                                                            }

                                                            if (jsonObject.has("PassengerId"))
                                                            {
                                                                PassengerId = jsonObject.getString("PassengerId");
                                                            }

                                                            if (jsonObject.has("ModelId"))
                                                            {
                                                                ModelId = jsonObject.getString("ModelId");
                                                            }

                                                            if (jsonObject.has("DriverId"))
                                                            {
                                                                DriverId = jsonObject.getString("DriverId");
                                                            }

                                                            if (jsonObject.has("CreatedDate"))
                                                            {
                                                                CreatedDate = jsonObject.getString("CreatedDate");
                                                            }

                                                            if (jsonObject.has("TransactionId"))
                                                            {
                                                                TransactionId = jsonObject.getString("TransactionId");
                                                            }

                                                            if (jsonObject.has("PaymentStatus"))
                                                            {
                                                                PaymentStatus = jsonObject.getString("PaymentStatus");
                                                            }

                                                            if (jsonObject.has("PickupTime"))
                                                            {
                                                                PickupTime = jsonObject.getString("PickupTime");
                                                            }

                                                            if (jsonObject.has("PickupDateTime") && !jsonObject.getString("PickupDateTime").equals("false"))
                                                            {
                                                                PickupDateTime = jsonObject.getString("PickupDateTime");
                                                            }

                                                            if (jsonObject.has("DropOffDateTime") && !jsonObject.getString("DropOffDateTime").equals("false"))
                                                            {
                                                                DropOffDateTime = jsonObject.getString("DropOffDateTime");
                                                            }

                                                            if (jsonObject.has("DropTime"))
                                                            {
                                                                DropTime = jsonObject.getString("DropTime");
                                                            }

                                                            if (jsonObject.has("TripDuration"))
                                                            {
                                                                TripDuration = jsonObject.getString("TripDuration");
                                                            }

                                                            if (jsonObject.has("TripDistance"))
                                                            {
                                                                TripDistance = jsonObject.getString("TripDistance");
                                                            }

                                                            if (jsonObject.has("PickupLocation"))
                                                            {
                                                                PickupLocation = jsonObject.getString("PickupLocation");
                                                            }

                                                            if (jsonObject.has("DropoffLocation"))
                                                            {
                                                                DropoffLocation = jsonObject.getString("DropoffLocation");
                                                            }

                                                            if (jsonObject.has("NightFareApplicable"))
                                                            {
                                                                NightFareApplicable = jsonObject.getString("NightFareApplicable");
                                                            }

                                                            if (jsonObject.has("NightFare"))
                                                            {
                                                                NightFare = jsonObject.getString("NightFare");
                                                            }

                                                            if (jsonObject.has("TripFare"))
                                                            {
                                                                TripFare = jsonObject.getString("TripFare");
                                                            }

                                                            if (jsonObject.has("WaitingTime"))
                                                            {
                                                                WaitingTime = jsonObject.getString("WaitingTime");
                                                            }

                                                            if (jsonObject.has("WaitingTimeCost"))
                                                            {
                                                                WaitingTimeCost = jsonObject.getString("WaitingTimeCost");
                                                            }

                                                            if (jsonObject.has("TollFee"))
                                                            {
                                                                TollFee = jsonObject.getString("TollFee");
                                                            }

                                                            if (jsonObject.has("BookingCharge"))
                                                            {
                                                                BookingCharge = jsonObject.getString("BookingCharge");
                                                            }

                                                            if (jsonObject.has("Tax"))
                                                            {
                                                                Tax = jsonObject.getString("Tax");
                                                            }

                                                            if (jsonObject.has("PromoCode"))
                                                            {
                                                                PromoCode = jsonObject.getString("PromoCode");
                                                            }

                                                            if (jsonObject.has("CompanyTax"))
                                                            {
                                                                CompanyTax = jsonObject.getString("CompanyTax");
                                                            }

                                                            if (jsonObject.has("SubTotal"))
                                                            {
                                                                SubTotal = jsonObject.getString("SubTotal");
                                                            }

                                                            if (jsonObject.has("GrandTotal"))
                                                            {
                                                                GrandTotal = jsonObject.getString("GrandTotal");
                                                            }

                                                            if (jsonObject.has("Status"))
                                                            {
                                                                Status = jsonObject.getString("Status");
                                                            }

                                                            if (jsonObject.has("Reason"))
                                                            {
                                                                Reason = jsonObject.getString("Reason");
                                                            }

                                                            if (jsonObject.has("PaymentType"))
                                                            {
                                                                PaymentType = jsonObject.getString("PaymentType");
                                                            }

                                                            if (jsonObject.has("AdminAmount"))
                                                            {
                                                                AdminAmount = jsonObject.getString("AdminAmount");
                                                            }

                                                            if (jsonObject.has("CompanyAmount"))
                                                            {
                                                                CompanyAmount = jsonObject.getString("CompanyAmount");
                                                            }

                                                            if (jsonObject.has("PassengerType"))
                                                            {
                                                                PassengerType = jsonObject.getString("PassengerType");
                                                            }

                                                            if (jsonObject.has("PassengerName"))
                                                            {
                                                                PassengerName = jsonObject.getString("PassengerName");
                                                            }

                                                            if (jsonObject.has("PassengerContact"))
                                                            {
                                                                PassengerContact = jsonObject.getString("PassengerContact");
                                                            }

                                                            if (jsonObject.has("FlightNumber"))
                                                            {
                                                                FlightNumber = jsonObject.getString("FlightNumber");
                                                            }

                                                            if (jsonObject.has("Model"))
                                                            {
                                                                Model = jsonObject.getString("Model");
                                                            }

                                                            if (jsonObject.has("DriverName"))
                                                            {
                                                                DriverName = jsonObject.getString("DriverName");
                                                            }

                                                            if (jsonObject.has("HistoryType"))
                                                            {
                                                                HistoryType = jsonObject.getString("HistoryType");
                                                            }

                                                            if (jsonObject.has("BookingType"))
                                                            {
                                                                BookingType = jsonObject.getString("BookingType");
                                                            }

                                                            if (jsonObject.has("DriverMobileNo") && jsonObject.getString("DriverMobileNo")!=null && !jsonObject.getString("DriverMobileNo").equalsIgnoreCase("NULL") && !jsonObject.getString("DriverMobileNo").equalsIgnoreCase("null"))
                                                            {
                                                                DriverMobileNo = jsonObject.getString("DriverMobileNo");
                                                            }

                                                            if (jsonObject.has("Notes") && jsonObject.getString("Notes")!=null)
                                                            {
                                                                Notes = jsonObject.getString("Notes");
                                                            }

                                                            if (jsonObject.has("BabySeat") && jsonObject.getString("BabySeat")!=null)
                                                            {
                                                                BabySeat = jsonObject.getString("BabySeat");
                                                            }

                                                            if (jsonObject.has("CardId") && jsonObject.getString("CardId")!=null)
                                                            {
                                                                CardId = jsonObject.getString("CardId");
                                                            }

                                                            if (jsonObject.has("PickupLat") && jsonObject.getString("PickupLat") != null) {
                                                                PickupLat = jsonObject.getString("PickupLat");
                                                            }
                                                            if (jsonObject.has("PickupLng") && jsonObject.getString("PickupLng") != null) {
                                                                PickupLng = jsonObject.getString("PickupLng");
                                                            }
                                                            if (jsonObject.has("DropOffLat") && jsonObject.getString("DropOffLat") != null) {
                                                                DropOffLat = jsonObject.getString("DropOffLat");
                                                            }
                                                            if (jsonObject.has("DropOffLon") && jsonObject.getString("DropOffLon") != null) {
                                                                DropOffLon = jsonObject.getString("DropOffLon");
                                                            }

                                                            if (jsonObject.has("PickupSuburb") && jsonObject.getString("PickupSuburb") != null) {
                                                                PickupSuburb = jsonObject.getString("PickupSuburb");
                                                            }
                                                            if (jsonObject.has("DropoffSuburb") && jsonObject.getString("DropoffSuburb") != null) {
                                                                DropoffSuburb = jsonObject.getString("DropoffSuburb");
                                                            }

                                                            if (jsonObject.has("CarDetails"))
                                                            {
                                                                JSONObject CarDetails = jsonObject.getJSONObject("CarDetails");

                                                                if (CarDetails!=null)
                                                                {
                                                                    if (CarDetails.has("Id"))
                                                                    {
                                                                        CarDetails_Id = CarDetails.getString("Id");
                                                                    }

                                                                    if (CarDetails.has("CompanyId"))
                                                                    {
                                                                        CarDetails_CompanyId = CarDetails.getString("CompanyId");
                                                                    }

                                                                    if (CarDetails.has("DriverId"))
                                                                    {
                                                                        CarDetails_DriverId = CarDetails.getString("DriverId");
                                                                    }

                                                                    if (CarDetails.has("VehicleModel"))
                                                                    {
                                                                        CarDetails_VehicleModel = CarDetails.getString("VehicleModel");
                                                                    }

                                                                    if (CarDetails.has("Company"))
                                                                    {
                                                                        CarDetails_Company = CarDetails.getString("Company");
                                                                    }

                                                                    if (CarDetails.has("Color"))
                                                                    {
                                                                        CarDetails_Color = CarDetails.getString("Color");
                                                                    }

                                                                    if (CarDetails.has("VehicleRegistrationNo"))
                                                                    {
                                                                        CarDetails_VehicleRegistrationNo = CarDetails.getString("VehicleRegistrationNo");
                                                                    }

                                                                    if (CarDetails.has("RegistrationCertificate"))
                                                                    {
                                                                        CarDetails_RegistrationCertificate = CarDetails.getString("RegistrationCertificate");
                                                                    }

                                                                    if (CarDetails.has("VehicleInsuranceCertificate"))
                                                                    {
                                                                        CarDetails_VehicleInsuranceCertificate = CarDetails.getString("VehicleInsuranceCertificate");
                                                                    }

                                                                    if (CarDetails.has("RegistrationCertificateExpire"))
                                                                    {
                                                                        CarDetails_RegistrationCertificateExpire = CarDetails.getString("RegistrationCertificateExpire");
                                                                    }

                                                                    if (CarDetails.has("VehicleInsuranceCertificateExpire"))
                                                                    {
                                                                        CarDetails_VehicleInsuranceCertificateExpire = CarDetails.getString("VehicleInsuranceCertificateExpire");
                                                                    }

                                                                    if (CarDetails.has("VehicleImage"))
                                                                    {
                                                                        CarDetails_VehicleImage = WebServiceAPI.BASE_URL_IMAGE + CarDetails.getString("VehicleImage");
                                                                    }

                                                                    if (CarDetails.has("Description"))
                                                                    {
                                                                        CarDetails_Description = CarDetails.getString("Description");
                                                                    }
                                                                }
                                                            }

                                                            MyBookingActivity.upComming_beens.add(new UpComming_Been(
                                                                    Id,
                                                                    CompanyId,
                                                                    PassengerId,
                                                                    ModelId,
                                                                    DriverId,
                                                                    CreatedDate,
                                                                    TransactionId,
                                                                    PaymentStatus,
                                                                    PickupTime,
                                                                    PickupDateTime,
                                                                    DropTime,
                                                                    TripDuration,
                                                                    TripDistance,
                                                                    PickupLocation,
                                                                    DropoffLocation,
                                                                    NightFareApplicable,
                                                                    NightFare,
                                                                    TripFare,
                                                                    WaitingTime,
                                                                    WaitingTimeCost,
                                                                    TollFee,
                                                                    BookingCharge,
                                                                    Tax,
                                                                    PromoCode,
                                                                    CompanyTax,
                                                                    SubTotal,
                                                                    GrandTotal,
                                                                    Status,
                                                                    Reason,
                                                                    PaymentType,
                                                                    AdminAmount,
                                                                    CompanyAmount,
                                                                    PassengerType,
                                                                    PassengerName,
                                                                    PassengerContact,
                                                                    FlightNumber,
                                                                    Model,
                                                                    DriverName,
                                                                    HistoryType,
                                                                    BookingType,
                                                                    DropOffDateTime,
                                                                    DriverMobileNo,

                                                                    CarDetails_Id,
                                                                    CarDetails_CompanyId,
                                                                    CarDetails_DriverId,
                                                                    CarDetails_VehicleModel,
                                                                    CarDetails_Company,
                                                                    CarDetails_Color,
                                                                    CarDetails_VehicleRegistrationNo,
                                                                    CarDetails_RegistrationCertificate,
                                                                    CarDetails_VehicleInsuranceCertificate,
                                                                    CarDetails_RegistrationCertificateExpire,
                                                                    CarDetails_VehicleInsuranceCertificateExpire,
                                                                    CarDetails_VehicleImage,
                                                                    CarDetails_Description,
                                                                    Notes,
                                                                    BabySeat,
                                                                    CardId,
                                                                    PickupLat,
                                                                    PickupLng,
                                                                    DropOffLat,
                                                                    DropOffLon,
                                                                    PickupSuburb,
                                                                    DropoffSuburb
                                                            ));
                                                        }
                                                        else if (Status1.equalsIgnoreCase("traveling"))
                                                        {
                                                            String Id="",PassengerId="",ModelId="",DriverId="",CreatedDate="",TransactionId="",PaymentStatus="";
                                                            String PickupTime="",DropTime="",TripDuration="",TripDistance="",PickupLocation="",DropoffLocation="";
                                                            String NightFareApplicable="",NightFare="",TripFare="",WaitingTime="",WaitingTimeCost="",TollFee="",BookingCharge="";
                                                            String Tax="",PromoCode="",Discount="",SubTotal="",GrandTotal="",Status="",Reason="",PaymentType="",AdminAmount="";
                                                            String CompanyAmount="",PickupLat="",PickupLng="",DropOffLat="",DropOffLon="",Model="",DriverName="";
                                                            String CarDetails_Id="",CarDetails_CompanyId="",CarDetails_DriverId="",CarDetails_VehicleModel="";
                                                            String CarDetails_Company="",CarDetails_Color="",CarDetails_VehicleRegistrationNo="",CarDetails_RegistrationCertificate="";
                                                            String CarDetails_VehicleInsuranceCertificate="",CarDetails_RegistrationCertificateExpire="",CarDetails_VehicleInsuranceCertificateExpire="";
                                                            String CarDetails_VehicleImage="",CarDetails_Description="",HistoryType="",BookingType="";
                                                            String PickupDateTime="",DropOffDateTime="",DriverMobileNo="";
                                                            String CardId="",Notes="",BabySeat="",PickupSuburb="",DropoffSuburb="";

                                                            if (jsonObject.has("CardId")) {
                                                                CardId = jsonObject.getString("CardId");
                                                            }

                                                            if (jsonObject.has("Notes")) {
                                                                Notes = jsonObject.getString("Notes");
                                                            }

                                                            if (jsonObject.has("BabySeat")) {
                                                                BabySeat = jsonObject.getString("BabySeat");
                                                            }

                                                            if (jsonObject.has("PickupSuburb")) {
                                                                PickupSuburb = jsonObject.getString("PickupSuburb");
                                                            }

                                                            if (jsonObject.has("DropoffSuburb")) {
                                                                DropoffSuburb = jsonObject.getString("DropoffSuburb");
                                                            }

                                                            if (jsonObject.has("Id"))
                                                            {
                                                                Id = jsonObject.getString("Id");
                                                            }

                                                            if (jsonObject.has("PassengerId"))
                                                            {
                                                                PassengerId = jsonObject.getString("PassengerId");
                                                            }

                                                            if (jsonObject.has("ModelId"))
                                                            {
                                                                ModelId = jsonObject.getString("ModelId");
                                                            }

                                                            if (jsonObject.has("DriverId"))
                                                            {
                                                                DriverId = jsonObject.getString("DriverId");
                                                            }

                                                            if (jsonObject.has("CreatedDate"))
                                                            {
                                                                CreatedDate = jsonObject.getString("CreatedDate");
                                                            }

                                                            if (jsonObject.has("TransactionId"))
                                                            {
                                                                TransactionId = jsonObject.getString("TransactionId");
                                                            }

                                                            if (jsonObject.has("PaymentStatus"))
                                                            {
                                                                PaymentStatus = jsonObject.getString("PaymentStatus");
                                                            }

                                                            if (jsonObject.has("PickupTime"))
                                                            {
                                                                PickupTime = jsonObject.getString("PickupTime");
                                                            }

                                                            if (jsonObject.has("DropTime"))
                                                            {
                                                                DropTime = jsonObject.getString("DropTime");
                                                            }

                                                            if (jsonObject.has("PickupDateTime") && !jsonObject.getString("PickupDateTime").equals("false"))
                                                            {
                                                                PickupDateTime = jsonObject.getString("PickupDateTime");
                                                            }

                                                            if (jsonObject.has("DropOffDateTime") && !jsonObject.getString("DropOffDateTime").equals("false"))
                                                            {
                                                                DropOffDateTime = jsonObject.getString("DropOffDateTime");
                                                            }

                                                            if (jsonObject.has("TripDuration"))
                                                            {
                                                                TripDuration = jsonObject.getString("TripDuration");
                                                            }

                                                            if (jsonObject.has("TripDistance"))
                                                            {
                                                                TripDistance = jsonObject.getString("TripDistance");
                                                            }

                                                            if (jsonObject.has("PickupLocation"))
                                                            {
                                                                PickupLocation = jsonObject.getString("PickupLocation");
                                                            }

                                                            if (jsonObject.has("DropoffLocation"))
                                                            {
                                                                DropoffLocation = jsonObject.getString("DropoffLocation");
                                                            }

                                                            if (jsonObject.has("NightFareApplicable"))
                                                            {
                                                                NightFareApplicable = jsonObject.getString("NightFareApplicable");
                                                            }

                                                            if (jsonObject.has("NightFare"))
                                                            {
                                                                NightFare = jsonObject.getString("NightFare");
                                                            }

                                                            if (jsonObject.has("TripFare"))
                                                            {
                                                                TripFare = jsonObject.getString("TripFare");
                                                            }

                                                            if (jsonObject.has("WaitingTime"))
                                                            {
                                                                WaitingTime = jsonObject.getString("WaitingTime");
                                                            }

                                                            if (jsonObject.has("WaitingTimeCost"))
                                                            {
                                                                WaitingTimeCost = jsonObject.getString("WaitingTimeCost");
                                                            }

                                                            if (jsonObject.has("TollFee"))
                                                            {
                                                                TollFee = jsonObject.getString("TollFee");
                                                            }

                                                            if (jsonObject.has("BookingCharge"))
                                                            {
                                                                BookingCharge = jsonObject.getString("BookingCharge");
                                                            }

                                                            if (jsonObject.has("Tax"))
                                                            {
                                                                Tax = jsonObject.getString("Tax");
                                                            }

                                                            if (jsonObject.has("PromoCode"))
                                                            {
                                                                PromoCode = jsonObject.getString("PromoCode");
                                                            }

                                                            if (jsonObject.has("Discount"))
                                                            {
                                                                Discount = jsonObject.getString("Discount");
                                                            }

                                                            if (jsonObject.has("SubTotal"))
                                                            {
                                                                SubTotal = jsonObject.getString("SubTotal");
                                                            }

                                                            if (jsonObject.has("GrandTotal"))
                                                            {
                                                                GrandTotal = jsonObject.getString("GrandTotal");
                                                            }

                                                            if (jsonObject.has("Status"))
                                                            {
                                                                Status = jsonObject.getString("Status");
                                                            }

                                                            if (jsonObject.has("Reason"))
                                                            {
                                                                Reason = jsonObject.getString("Reason");
                                                            }

                                                            if (jsonObject.has("PaymentType"))
                                                            {
                                                                PaymentType = jsonObject.getString("PaymentType");
                                                            }

                                                            if (jsonObject.has("AdminAmount"))
                                                            {
                                                                AdminAmount = jsonObject.getString("AdminAmount");
                                                            }

                                                            if (jsonObject.has("CompanyAmount"))
                                                            {
                                                                CompanyAmount = jsonObject.getString("CompanyAmount");
                                                            }

                                                            if (jsonObject.has("PickupLat"))
                                                            {
                                                                PickupLat = jsonObject.getString("PickupLat");
                                                            }

                                                            if (jsonObject.has("PickupLng"))
                                                            {
                                                                PickupLng = jsonObject.getString("PickupLng");
                                                            }

                                                            if (jsonObject.has("DropOffLat"))
                                                            {
                                                                DropOffLat = jsonObject.getString("DropOffLat");
                                                            }

                                                            if (jsonObject.has("DropOffLon"))
                                                            {
                                                                DropOffLon = jsonObject.getString("DropOffLon");
                                                            }

                                                            if (jsonObject.has("Model"))
                                                            {
                                                                Model = jsonObject.getString("Model");
                                                            }

                                                            if (jsonObject.has("DriverName"))
                                                            {
                                                                DriverName = jsonObject.getString("DriverName");
                                                            }

                                                            if (jsonObject.has("DriverMobileNo") && jsonObject.getString("DriverMobileNo")!=null && !jsonObject.getString("DriverMobileNo").equalsIgnoreCase("NULL") && !jsonObject.getString("DriverMobileNo").equalsIgnoreCase("null"))
                                                            {
                                                                DriverMobileNo = jsonObject.getString("DriverMobileNo");
                                                            }

                                                            if (jsonObject.has("CarDetails"))
                                                            {
                                                                JSONObject CarDetails = jsonObject.getJSONObject("CarDetails");

                                                                if (CarDetails!=null)
                                                                {
                                                                    if (CarDetails.has("Id"))
                                                                    {
                                                                        CarDetails_Id = CarDetails.getString("Id");
                                                                    }

                                                                    if (CarDetails.has("CompanyId"))
                                                                    {
                                                                        CarDetails_CompanyId = CarDetails.getString("CompanyId");
                                                                    }

                                                                    if (CarDetails.has("VehicleModel"))
                                                                    {
                                                                        CarDetails_VehicleModel = CarDetails.getString("VehicleModel");
                                                                    }

                                                                    if (CarDetails.has("Company"))
                                                                    {
                                                                        CarDetails_Company = CarDetails.getString("Company");
                                                                    }

                                                                    if (CarDetails.has("Color"))
                                                                    {
                                                                        CarDetails_Color = CarDetails.getString("Color");
                                                                    }

                                                                    if (CarDetails.has("VehicleRegistrationNo"))
                                                                    {
                                                                        CarDetails_VehicleRegistrationNo = CarDetails.getString("VehicleRegistrationNo");
                                                                    }

                                                                    if (CarDetails.has("RegistrationCertificate"))
                                                                    {
                                                                        CarDetails_RegistrationCertificate = CarDetails.getString("RegistrationCertificate");
                                                                    }

                                                                    if (CarDetails.has("VehicleInsuranceCertificate"))
                                                                    {
                                                                        CarDetails_VehicleInsuranceCertificate = CarDetails.getString("VehicleInsuranceCertificate");
                                                                    }

                                                                    if (CarDetails.has("RegistrationCertificateExpire"))
                                                                    {
                                                                        CarDetails_RegistrationCertificateExpire = CarDetails.getString("RegistrationCertificateExpire");
                                                                    }

                                                                    if (CarDetails.has("VehicleInsuranceCertificateExpire"))
                                                                    {
                                                                        CarDetails_VehicleInsuranceCertificateExpire = CarDetails.getString("VehicleInsuranceCertificateExpire");
                                                                    }

                                                                    if (CarDetails.has("VehicleImage"))
                                                                    {
                                                                        CarDetails_VehicleImage = WebServiceAPI.BASE_URL_IMAGE + CarDetails.getString("VehicleImage");
                                                                    }

                                                                    if (CarDetails.has("Description"))
                                                                    {
                                                                        CarDetails_Description = CarDetails.getString("Description");
                                                                    }
                                                                }
                                                            }

                                                            if (jsonObject.has("HistoryType"))
                                                            {
                                                                HistoryType = jsonObject.getString("HistoryType");
                                                            }

                                                            if (jsonObject.has("BookingType"))
                                                            {
                                                                BookingType = jsonObject.getString("BookingType");
                                                            }

                                                            MyBookingActivity.onGoing_beens.add(new OnGoing_Been(Id,
                                                                    PassengerId,
                                                                    ModelId,
                                                                    DriverId,
                                                                    CreatedDate,
                                                                    TransactionId,
                                                                    PaymentStatus,
                                                                    PickupTime,
                                                                    DropTime,
                                                                    TripDuration,
                                                                    TripDistance,
                                                                    PickupLocation,
                                                                    DropoffLocation,
                                                                    NightFareApplicable,
                                                                    NightFare,
                                                                    TripFare,
                                                                    WaitingTime,
                                                                    WaitingTimeCost,
                                                                    TollFee,
                                                                    BookingCharge,
                                                                    Tax,
                                                                    PromoCode,
                                                                    Discount,
                                                                    SubTotal,
                                                                    GrandTotal,
                                                                    Status,
                                                                    Reason,
                                                                    PaymentType,
                                                                    AdminAmount,
                                                                    CompanyAmount,
                                                                    PickupLat,
                                                                    PickupLng,
                                                                    DropOffLat,
                                                                    DropOffLon,
                                                                    Model,
                                                                    DriverName,
                                                                    CarDetails_Id,
                                                                    CarDetails_CompanyId,
                                                                    CarDetails_DriverId,
                                                                    CarDetails_VehicleModel,
                                                                    CarDetails_Company,
                                                                    CarDetails_Color,
                                                                    CarDetails_VehicleRegistrationNo,
                                                                    CarDetails_RegistrationCertificate,
                                                                    CarDetails_VehicleInsuranceCertificate,
                                                                    CarDetails_RegistrationCertificateExpire,
                                                                    CarDetails_VehicleInsuranceCertificateExpire,
                                                                    CarDetails_VehicleImage,
                                                                    CarDetails_Description,
                                                                    HistoryType,
                                                                    BookingType,
                                                                    PickupDateTime,
                                                                    DropOffDateTime,
                                                                    DriverMobileNo,
                                                                    CardId,
                                                                    Notes,
                                                                    BabySeat,
                                                                    PickupSuburb,
                                                                    DropoffSuburb
                                                            ));
                                                        }
                                                        else if (Status1.equalsIgnoreCase("completed") || Status1.equalsIgnoreCase("canceled"))
                                                        {
                                                            String Id="",PassengerId="",ModelId="",DriverId="",CreatedDate="",TransactionId="",PaymentStatus="";
                                                            String PickupTime="",DropTime="",TripDuration="",TripDistance="",PickupLocation="",DropoffLocation="";
                                                            String NightFareApplicable="",NightFare="",TripFare="",WaitingTime="",WaitingTimeCost="",TollFee="",BookingCharge="";
                                                            String Tax="",PromoCode="",Discount="",SubTotal="",GrandTotal="",Status="",Reason="",PaymentType="",AdminAmount="";
                                                            String CompanyAmount="",PickupLat="",PickupLng="",DropOffLat="",DropOffLon="",Model="",DriverName="";
                                                            String CarDetails_Id="",CarDetails_CompanyId="",CarDetails_DriverId="",CarDetails_VehicleModel="";
                                                            String CarDetails_Company="",CarDetails_Color="",CarDetails_VehicleRegistrationNo="",CarDetails_RegistrationCertificate="";
                                                            String CarDetails_VehicleInsuranceCertificate="",CarDetails_RegistrationCertificateExpire="",CarDetails_VehicleInsuranceCertificateExpire="";
                                                            String CarDetails_VehicleImage="",CarDetails_Description="",HistoryType="",BookingType="";
                                                            String PickupDateTime="",DropOffDateTime="",DriverMobileNo="";

                                                            if (jsonObject.has("Id"))
                                                            {
                                                                Id = jsonObject.getString("Id");
                                                            }

                                                            if (jsonObject.has("PassengerId"))
                                                            {
                                                                PassengerId = jsonObject.getString("PassengerId");
                                                            }

                                                            if (jsonObject.has("ModelId"))
                                                            {
                                                                ModelId = jsonObject.getString("ModelId");
                                                            }

                                                            if (jsonObject.has("DriverId"))
                                                            {
                                                                DriverId = jsonObject.getString("DriverId");
                                                            }

                                                            if (jsonObject.has("CreatedDate"))
                                                            {
                                                                CreatedDate = jsonObject.getString("CreatedDate");
                                                            }

                                                            if (jsonObject.has("TransactionId"))
                                                            {
                                                                TransactionId = jsonObject.getString("TransactionId");
                                                            }

                                                            if (jsonObject.has("PaymentStatus"))
                                                            {
                                                                PaymentStatus = jsonObject.getString("PaymentStatus");
                                                            }

                                                            if (jsonObject.has("PickupTime"))
                                                            {
                                                                PickupTime = jsonObject.getString("PickupTime");
                                                            }

                                                            if (jsonObject.has("DropTime"))
                                                            {
                                                                DropTime = jsonObject.getString("DropTime");
                                                            }

                                                            if (jsonObject.has("PickupDateTime") && !jsonObject.getString("PickupDateTime").equals("false"))
                                                            {
                                                                PickupDateTime = jsonObject.getString("PickupDateTime");
                                                            }

                                                            if (jsonObject.has("DropOffDateTime") && !jsonObject.getString("DropOffDateTime").equals("false"))
                                                            {
                                                                DropOffDateTime = jsonObject.getString("DropOffDateTime");
                                                            }

                                                            if (jsonObject.has("TripDuration"))
                                                            {
                                                                TripDuration = jsonObject.getString("TripDuration");
                                                            }

                                                            if (jsonObject.has("TripDistance"))
                                                            {
                                                                TripDistance = jsonObject.getString("TripDistance");
                                                            }

                                                            if (jsonObject.has("PickupLocation"))
                                                            {
                                                                PickupLocation = jsonObject.getString("PickupLocation");
                                                            }

                                                            if (jsonObject.has("DropoffLocation"))
                                                            {
                                                                DropoffLocation = jsonObject.getString("DropoffLocation");
                                                            }

                                                            if (jsonObject.has("NightFareApplicable"))
                                                            {
                                                                NightFareApplicable = jsonObject.getString("NightFareApplicable");
                                                            }

                                                            if (jsonObject.has("NightFare"))
                                                            {
                                                                NightFare = jsonObject.getString("NightFare");
                                                            }

                                                            if (jsonObject.has("TripFare"))
                                                            {
                                                                TripFare = jsonObject.getString("TripFare");
                                                            }

                                                            if (jsonObject.has("WaitingTime"))
                                                            {
                                                                WaitingTime = jsonObject.getString("WaitingTime");
                                                            }

                                                            if (jsonObject.has("WaitingTimeCost"))
                                                            {
                                                                WaitingTimeCost = jsonObject.getString("WaitingTimeCost");
                                                            }

                                                            if (jsonObject.has("TollFee"))
                                                            {
                                                                TollFee = jsonObject.getString("TollFee");
                                                            }

                                                            if (jsonObject.has("BookingCharge"))
                                                            {
                                                                BookingCharge = jsonObject.getString("BookingCharge");
                                                            }

                                                            if (jsonObject.has("Tax"))
                                                            {
                                                                Tax = jsonObject.getString("Tax");
                                                            }

                                                            if (jsonObject.has("PromoCode"))
                                                            {
                                                                PromoCode = jsonObject.getString("PromoCode");
                                                            }

                                                            if (jsonObject.has("Discount"))
                                                            {
                                                                Discount = jsonObject.getString("Discount");
                                                            }

                                                            if (jsonObject.has("SubTotal"))
                                                            {
                                                                SubTotal = jsonObject.getString("SubTotal");
                                                            }

                                                            if (jsonObject.has("GrandTotal"))
                                                            {
                                                                GrandTotal = jsonObject.getString("GrandTotal");
                                                            }

                                                            if (jsonObject.has("Status"))
                                                            {
                                                                Status = jsonObject.getString("Status");
                                                            }

                                                            if (jsonObject.has("Reason"))
                                                            {
                                                                Reason = jsonObject.getString("Reason");
                                                            }

                                                            if (jsonObject.has("PaymentType"))
                                                            {
                                                                PaymentType = jsonObject.getString("PaymentType");
                                                            }

                                                            if (jsonObject.has("AdminAmount"))
                                                            {
                                                                AdminAmount = jsonObject.getString("AdminAmount");
                                                            }

                                                            if (jsonObject.has("CompanyAmount"))
                                                            {
                                                                CompanyAmount = jsonObject.getString("CompanyAmount");
                                                            }

                                                            if (jsonObject.has("PickupLat"))
                                                            {
                                                                PickupLat = jsonObject.getString("PickupLat");
                                                            }

                                                            if (jsonObject.has("PickupLng"))
                                                            {
                                                                PickupLng = jsonObject.getString("PickupLng");
                                                            }

                                                            if (jsonObject.has("DropOffLat"))
                                                            {
                                                                DropOffLat = jsonObject.getString("DropOffLat");
                                                            }

                                                            if (jsonObject.has("DropOffLon"))
                                                            {
                                                                DropOffLon = jsonObject.getString("DropOffLon");
                                                            }

                                                            if (jsonObject.has("Model"))
                                                            {
                                                                Model = jsonObject.getString("Model");
                                                            }

                                                            if (jsonObject.has("DriverName"))
                                                            {
                                                                DriverName = jsonObject.getString("DriverName");
                                                            }

                                                            if (jsonObject.has("DriverMobileNo") && jsonObject.getString("DriverMobileNo")!=null && !jsonObject.getString("DriverMobileNo").equalsIgnoreCase("NULL") && !jsonObject.getString("DriverMobileNo").equalsIgnoreCase("null"))
                                                            {
                                                                DriverMobileNo = jsonObject.getString("DriverMobileNo");
                                                            }

                                                            if (jsonObject.has("CarDetails"))
                                                            {
                                                                JSONObject CarDetails = jsonObject.getJSONObject("CarDetails");

                                                                if (CarDetails!=null)
                                                                {
                                                                    if (CarDetails.has("Id"))
                                                                    {
                                                                        CarDetails_Id = CarDetails.getString("Id");
                                                                    }

                                                                    if (CarDetails.has("CompanyId"))
                                                                    {
                                                                        CarDetails_CompanyId = CarDetails.getString("CompanyId");
                                                                    }

                                                                    if (CarDetails.has("VehicleModel"))
                                                                    {
                                                                        CarDetails_VehicleModel = CarDetails.getString("VehicleModel");
                                                                    }

                                                                    if (CarDetails.has("Company"))
                                                                    {
                                                                        CarDetails_Company = CarDetails.getString("Company");
                                                                    }

                                                                    if (CarDetails.has("Color"))
                                                                    {
                                                                        CarDetails_Color = CarDetails.getString("Color");
                                                                    }

                                                                    if (CarDetails.has("VehicleRegistrationNo"))
                                                                    {
                                                                        CarDetails_VehicleRegistrationNo = CarDetails.getString("VehicleRegistrationNo");
                                                                    }

                                                                    if (CarDetails.has("RegistrationCertificate"))
                                                                    {
                                                                        CarDetails_RegistrationCertificate = CarDetails.getString("RegistrationCertificate");
                                                                    }

                                                                    if (CarDetails.has("VehicleInsuranceCertificate"))
                                                                    {
                                                                        CarDetails_VehicleInsuranceCertificate = CarDetails.getString("VehicleInsuranceCertificate");
                                                                    }

                                                                    if (CarDetails.has("RegistrationCertificateExpire"))
                                                                    {
                                                                        CarDetails_RegistrationCertificateExpire = CarDetails.getString("RegistrationCertificateExpire");
                                                                    }

                                                                    if (CarDetails.has("VehicleInsuranceCertificateExpire"))
                                                                    {
                                                                        CarDetails_VehicleInsuranceCertificateExpire = CarDetails.getString("VehicleInsuranceCertificateExpire");
                                                                    }

                                                                    if (CarDetails.has("VehicleImage"))
                                                                    {
                                                                        CarDetails_VehicleImage = WebServiceAPI.BASE_URL_IMAGE + CarDetails.getString("VehicleImage");
                                                                    }

                                                                    if (CarDetails.has("Description"))
                                                                    {
                                                                        CarDetails_Description = CarDetails.getString("Description");
                                                                    }
                                                                }
                                                            }

                                                            if (jsonObject.has("HistoryType"))
                                                            {
                                                                HistoryType = jsonObject.getString("HistoryType");
                                                            }

                                                            if (jsonObject.has("BookingType"))
                                                            {
                                                                BookingType = jsonObject.getString("BookingType");
                                                            }

                                                            MyBookingActivity.pastBooking_beens.add(new PastBooking_Been(Id,
                                                                    PassengerId,
                                                                    ModelId,
                                                                    DriverId,
                                                                    CreatedDate,
                                                                    TransactionId,
                                                                    PaymentStatus,
                                                                    PickupTime,
                                                                    DropTime,
                                                                    TripDuration,
                                                                    TripDistance,
                                                                    PickupLocation,
                                                                    DropoffLocation,
                                                                    NightFareApplicable,
                                                                    NightFare,
                                                                    TripFare,
                                                                    WaitingTime,
                                                                    WaitingTimeCost,
                                                                    TollFee,
                                                                    BookingCharge,
                                                                    Tax,
                                                                    PromoCode,
                                                                    Discount,
                                                                    SubTotal,
                                                                    GrandTotal,
                                                                    Status,
                                                                    Reason,
                                                                    PaymentType,
                                                                    AdminAmount,
                                                                    CompanyAmount,
                                                                    PickupLat,
                                                                    PickupLng,
                                                                    DropOffLat,
                                                                    DropOffLon,
                                                                    Model,
                                                                    DriverName,
                                                                    CarDetails_Id,
                                                                    CarDetails_CompanyId,
                                                                    CarDetails_DriverId,
                                                                    CarDetails_VehicleModel,
                                                                    CarDetails_Company,
                                                                    CarDetails_Color,
                                                                    CarDetails_VehicleRegistrationNo,
                                                                    CarDetails_RegistrationCertificate,
                                                                    CarDetails_VehicleInsuranceCertificate,
                                                                    CarDetails_RegistrationCertificateExpire,
                                                                    CarDetails_VehicleInsuranceCertificateExpire,
                                                                    CarDetails_VehicleImage,
                                                                    CarDetails_Description,
                                                                    HistoryType,
                                                                    BookingType,
                                                                    PickupDateTime,
                                                                    DropOffDateTime,
                                                                    DriverMobileNo));
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                Log.e("call","jsonObject null position = "+i);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Log.e("call","history null or lenth 0");
                                    }

                                    Log.e("call","onGoing_beens.size() = "+MyBookingActivity.onGoing_beens.size());
                                    Log.e("call","upComming_beens.size() = "+MyBookingActivity.upComming_beens.size());
                                    Log.e("call","pastBooking_beens.size() = "+MyBookingActivity.pastBooking_beens.size());

                                    swipeRefreshLayout.setRefreshing(false);
                                    mAdapter.notifyDataSetChanged();
                                }
                                else
                                {
                                    Log.e("call","history not found");
                                    swipeRefreshLayout.setRefreshing(false);
                                    mAdapter.notifyDataSetChanged();
                                }
                            }
                            else
                            {
                                Log.e("call","status false");
                                swipeRefreshLayout.setRefreshing(false);
                                mAdapter.notifyDataSetChanged();
                            }
                        }
                        else
                        {
                            Log.e("call","status not found");
                            swipeRefreshLayout.setRefreshing(false);
                            mAdapter.notifyDataSetChanged();
                        }
                    }
                    else
                    {
                        Log.e("call","json null");
                        swipeRefreshLayout.setRefreshing(false);
                        mAdapter.notifyDataSetChanged();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Exception","Exception "+e.toString());
                    swipeRefreshLayout.setRefreshing(false);
                    mAdapter.notifyDataSetChanged();
                }
            }

        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 0) {
            if (permissions[0].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            }else{
                //requestPermissions(new String[]{Manifest.permission.WRITE_CALENDAR}, 0);
                //ActivityCompat.requestPermissions(getActivity(),new String[]{Manifest.permission.WRITE_CALENDAR}, 0);
            }
        }
    }

}
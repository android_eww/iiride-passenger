package iiride.app.rider.fragment;

import android.graphics.PointF;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.dlazaro66.qrcodereaderview.QRCodeReaderView;

import iiride.app.rider.R;
import iiride.app.rider.activity.Wallet_Transfer_Activity;
import iiride.app.rider.comman.WebServiceAPI;
import iiride.app.rider.other.DialogClass;
import iiride.app.rider.other.Global;
import iiride.app.rider.other.InternetDialog;

import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;

public class Wallet_Transfer_SendMoney_Fragment extends Fragment{

    private LinearLayout ll_StartScan,ll_Camera;
    private QRCodeReaderView qrCodeReaderView;
    public static TextView tv_ScanResult;
    private DialogClass dialogClass;
    private AQuery aQuery;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sendmoney_transfer_wallet, container, false);
        ll_StartScan = (LinearLayout) rootView.findViewById(R.id.star_scan_layout);
        ll_Camera = (LinearLayout) rootView.findViewById(R.id.camera_layout);
        tv_ScanResult = (TextView) rootView.findViewById(R.id.scan_result);
        ll_Camera.setVisibility(View.GONE);
        ll_StartScan.setVisibility(View.VISIBLE);
        tv_ScanResult.setText("Tap to activate Scanner");

        dialogClass = new DialogClass(getActivity(),0);
        aQuery = new AQuery(getActivity());

        qrCodeReaderView = (QRCodeReaderView) rootView.findViewById(R.id.qrdecoderview);
        ll_StartScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ll_StartScan.setVisibility(View.GONE);
                ll_Camera.setVisibility(View.VISIBLE);
                initialize();
            }
        });

        return rootView;
    }

    public void reset()
    {
        if (tv_ScanResult!=null)
        {
            tv_ScanResult.setText("Tap to activate Scanner");
            Wallet_Transfer_Activity.qr_code = "";
        }
    }

    public void initialize()
    {
        qrCodeReaderView.setOnQRCodeReadListener(new QRCodeReaderView.OnQRCodeReadListener() {
            @Override
            public void onQRCodeRead(String text, PointF[] points) {

                Log.e("call","onQRCodeRead response = "+text);
                if (text!=null && !text.equalsIgnoreCase(""))
                {
                    Wallet_Transfer_Activity.qr_code = text;
                    ll_Camera.setVisibility(View.GONE);
                    ll_StartScan.setVisibility(View.VISIBLE);
                    qrCodeReaderView.setOnQRCodeReadListener(null);
                    sendViaQrCode(Wallet_Transfer_Activity.qr_code);
                }
            }
        });

        // Use this function to enable/disable decoding
        qrCodeReaderView.setQRDecodingEnabled(true);

        // Use this function to change the autofocus interval (default is 5 secs)
        qrCodeReaderView.setAutofocusInterval(2000L);

        // Use this function to enable/disable Torch
//        qrCodeReaderView.setTorchEnabled(true);

        // Use this function to set front camera preview
        qrCodeReaderView.setFrontCamera();

        // Use this function to set back camera preview
        qrCodeReaderView.setBackCamera();

        qrCodeReaderView.startCamera();
    }

    public void sendViaQrCode(String qr_code)
    {
        if (qr_code!=null && !qr_code.equalsIgnoreCase(""))
        {
            if (Global.isNetworkconn(getActivity()))
            {
                callApi();
            }
            else
            {
                InternetDialog internetDialog = new InternetDialog(getActivity());
                internetDialog.showDialog("Please check your internet connection!");
            }
        }
        else
        {
            InternetDialog internetDialog = new InternetDialog(getActivity());
            internetDialog.showDialog("Please try again QRCode not found!");
        }
    }

    public void callApi()
    {
        dialogClass.showDialog();
        String url = WebServiceAPI.API_QR_CODE_DETAIL;

        //QRCode,SenderId,Amount

        Map<String, Object> params = new HashMap<String, Object>();
        params.put(WebServiceAPI.PARAM_QR_CODE,Wallet_Transfer_Activity.qr_code);

        Log.e("call", "url = " + url);
        Log.e("call", "param = " + params);

        aQuery.ajax(url.trim(), params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e("responseCode", " = " + responseCode);
                    Log.e("Response", " = " + json);

                    if (json!=null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                if (json.has("data"))
                                {
                                    JSONObject data = json.getJSONObject("data");

                                    if (data!=null)
                                    {
                                        String name="",number="";
                                        if (data.has("Fullname"))
                                        {
                                            name = data.getString("Fullname");
                                        }

                                        if (data.has("MobileNo"))
                                        {
                                            number = data.getString("MobileNo");
                                        }

                                        tv_ScanResult.setText("Name: "+name+"\n\n"+"Mobile: "+number);
                                        dialogClass.hideDialog();
                                    }
                                    else
                                    {
                                        dialogClass.hideDialog();
                                        InternetDialog internetDialog = new InternetDialog(getActivity());
                                        internetDialog.showDialog("Problem in getting detail please try again later!");
                                    }
                                }
                                else
                                {
                                    dialogClass.hideDialog();
                                    InternetDialog internetDialog = new InternetDialog(getActivity());
                                    internetDialog.showDialog("Problem in getting detail please try again later!");
                                }
                            }
                            else
                            {
                                dialogClass.hideDialog();
                                InternetDialog internetDialog = new InternetDialog(getActivity());
                                internetDialog.showDialog("Problem in getting detail please try again later!");
                            }
                        }
                        else
                        {
                            dialogClass.hideDialog();
                            InternetDialog internetDialog = new InternetDialog(getActivity());
                            internetDialog.showDialog("Problem in getting detail please try again later!");
                        }
                    }
                    else
                    {
                        dialogClass.hideDialog();
                        InternetDialog internetDialog = new InternetDialog(getActivity());
                        internetDialog.showDialog("Problem in getting detail please try again later!");
                    }
                }
                catch (Exception e)
                {
                    dialogClass.hideDialog();
                    InternetDialog internetDialog = new InternetDialog(getActivity());
                    internetDialog.showDialog("Problem in getting detail please try again later!");
                }
            }

        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    @Override
    public void onResume() {
        super.onResume();
        if (qrCodeReaderView!=null)
        {
            qrCodeReaderView.startCamera();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (qrCodeReaderView!=null)
        {
            qrCodeReaderView.stopCamera();
        }
    }
}
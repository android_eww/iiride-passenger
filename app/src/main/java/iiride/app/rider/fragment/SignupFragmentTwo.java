package iiride.app.rider.fragment;


import android.app.Activity;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import iiride.app.rider.R;
import iiride.app.rider.activity.MainActivity;
import iiride.app.rider.activity.SignUpActivity;
import iiride.app.rider.comman.Common;
import iiride.app.rider.comman.SessionSave;
import iiride.app.rider.comman.WebServiceAPI;
import iiride.app.rider.other.DialogClass;
import iiride.app.rider.other.GPSTracker;
import iiride.app.rider.other.Global;
import iiride.app.rider.other.InternetDialog;
import iiride.app.rider.view.MySnackBar;

import org.json.JSONArray;
import org.json.JSONObject;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import de.hdodenhof.circleimageview.CircleImageView;
import static android.app.Activity.RESULT_OK;

public class SignupFragmentTwo extends Fragment {

    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;
    Context context = getContext();
    LinearLayout layoutSignup;
    EditText editTitle,editFirstName,editLastName,editReferralCode;
    CircleImageView imageProfile;
    Uri mImageUri;
    private Uri imageUri;
    Bitmap bitmapImage;
    String picturePath;
    private static int RESULT_LOAD_IMAGE = 1;
    static Uri uriGallery;

    private LinearLayout ll_Back;
    private ImageView iv_Back;
    private RadioButton radioButtonMale,radioButtonFemale;
    private LinearLayout ll_Male,ll_Female;

    private DialogClass dialogClass;
    private AQuery aQuery;
    private LinearLayout ll_RootLayout;
    private MySnackBar mySnackBar;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_signup1, container,false);

        mImageUri=null;
        imageUri=null;
        bitmapImage=null;
        picturePath=null;
        uriGallery=null;

        SignUpActivity.gender = "male";
        dialogClass = new DialogClass(getActivity(),0);
        aQuery = new AQuery(getActivity());
        openRun();

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        ll_RootLayout = (LinearLayout) rootView.findViewById(R.id.main_content);

        layoutSignup = (LinearLayout)rootView.findViewById(R.id.layout_signup);
        editTitle = (EditText)rootView.findViewById(R.id.input_title);
        editFirstName = (EditText)rootView.findViewById(R.id.input_first_name);
        editLastName = (EditText)rootView.findViewById(R.id.input_last_name);
        editReferralCode = (EditText)rootView.findViewById(R.id.input_referal_code);
        imageProfile = (CircleImageView) rootView.findViewById(R.id.image_profile);
        ll_Back = (LinearLayout)rootView.findViewById(R.id.back_layout);
        iv_Back = (ImageView) rootView.findViewById(R.id.back_imageview);

        ll_Male = (LinearLayout) rootView.findViewById(R.id.radio_button_male_layout);
        ll_Female = (LinearLayout) rootView.findViewById(R.id.radio_button_female_layout);

        radioButtonMale = (RadioButton) rootView.findViewById(R.id.radio_button_male);
        radioButtonFemale = (RadioButton) rootView.findViewById(R.id.radio_button_female);

        mySnackBar = new MySnackBar(getActivity());

        ll_Male.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                radioButtonMale.setChecked(true);
                SignUpActivity.gender = "male";
                radioButtonFemale.setChecked(false);
            }
        });

        ll_Female.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                radioButtonMale.setChecked(false);
                SignUpActivity.gender = "female";
                radioButtonFemale.setChecked(true);
            }
        });

        imageProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takePickure();
            }
        });

        layoutSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Global.isNetworkconn(getActivity()))
                {
                    gotoSignup();
                }
                else
                {
                    InternetDialog internetDialog = new InternetDialog(getActivity());
                    internetDialog.showDialog("Please check your internet connection!");
                }
            }
        });

        iv_Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((SignUpActivity)getActivity()).mViewPager.setCurrentItem(0);
            }
        });

        ll_Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((SignUpActivity)getActivity()).mViewPager.setCurrentItem(0);
            }
        });

        return rootView;
    }

    public void takePickure()
    {
        final Dialog dialogCamera = new Dialog(getActivity());
        dialogCamera.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogCamera.setContentView(R.layout.camera_dialog);
        dialogCamera.show();

        ImageView imgCamera = (ImageView)dialogCamera.findViewById(R.id.imgCamera);
        ImageView imgGallery = (ImageView)dialogCamera.findViewById(R.id.imgGallery);

        imgCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialogCamera.dismiss();
                if (((SignUpActivity)getActivity()).checkAndRequestPermissions())
                {
                    camaraImage();
                }

            }
        });

        imgGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogCamera.dismiss();
                if (((SignUpActivity)getActivity()).checkAndRequestPermissions())
                {
                    pickFromGallery();
                }
            }
        });
    }

    public void gotoSignup()
    {
        if (editFirstName.getText().toString()!=null && !editFirstName.getText().toString().equals(""))
        {
            if (editLastName.getText().toString() != null && !editLastName.getText().toString().equals(""))
            {
                if (SignUpActivity.userImage!=null)
                {
                    SignUpActivity.firstName = editFirstName.getText().toString().trim();
                    SignUpActivity.lastName = editLastName.getText().toString().trim();
                    SignUpActivity.referralCode = editReferralCode.getText().toString().trim();

                    if (SignUpActivity.token!=null && !SignUpActivity.token.equalsIgnoreCase(""))
                    {
                        registerUser();
                    }
                    else
                    {
                        InternetDialog internetDialog = new InternetDialog(getActivity());
                        internetDialog.showDialog("Something went wrong please restart your application and login again");
                    }
                }
                else
                {
                    mySnackBar.showSnackBar(ll_RootLayout,"Please select Profile Picture!");
                }
            }
            else
            {
                mySnackBar.showSnackBar(ll_RootLayout,"Please enter LastName!");
                editLastName.setFocusableInTouchMode(true);
                editLastName.requestFocus();
            }
        }
        else
        {
            mySnackBar.showSnackBar(ll_RootLayout,"Please enter FirstName!");
            editFirstName.setFocusableInTouchMode(true);
            editFirstName.requestFocus();
        }
    }

    //Image Pick From Camera
    private void camaraImage() {

        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "NewPicture");
        imageUri = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        startActivityForResult(intent, 0);
    }

    //Image Pick From Gallary
    private void pickFromGallery() {
        Intent intent_gallery = new Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent_gallery, RESULT_LOAD_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        try
        {
            if ( resultCode == RESULT_OK )
            {

                if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data)
                {
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = { MediaStore.Images.Media.DATA };

                    uriGallery = data.getData();
                    Log.d("2222",""+data.getData());
                    Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                            filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    picturePath = cursor.getString(columnIndex);
                    cursor.close();
                    Log.d("filePathUri", "" + picturePath);
                    mImageUri = Uri.parse(picturePath);
                /*frameActivity( filePathUri );*/
                    bitmapImage = BitmapFactory.decodeFile(String.valueOf(mImageUri));
                    bitmapImage = getResizedBitmap(bitmapImage,400);
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmapImage.compress(Bitmap.CompressFormat.PNG, 10, stream);
                    byte[] byteArray = stream.toByteArray();

                    SignUpActivity.userImage = byteArray;

                    imageProfile.setImageBitmap(bitmapImage);

                }
                else if(requestCode==0)
                {
                    Bitmap thumbnail = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), imageUri);
                    Log.e("#########","bmp height = "+thumbnail.getHeight());
                    Log.e("#########","bmp width = "+thumbnail.getWidth());
                    thumbnail = getResizedBitmap(thumbnail,400);
                    bitmapImage=thumbnail;
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    thumbnail.compress(Bitmap.CompressFormat.PNG, 10, bytes);
                    byte[] byteArray = bytes.toByteArray();
                    SignUpActivity.userImage = bytes.toByteArray();
                    imageProfile.setImageBitmap(bitmapImage);
                }
                else
                {
                    SignUpActivity.userImage = null;
                }
            }
        }
        catch (Exception e)
        {
            Log.e("call","Exception in selecting image = "+e.getMessage());
        }

    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public static boolean validateFirstName(String firstName) {
        boolean check=false;
        if(!Pattern.matches("[a-zA-Z]+", firstName)) {

            check =true;

        } else {
            check=false;
        }
        return check;

    }
    // end method validateFirstName

    // validate last name
    public static boolean validateLastName( String lastName ){
        boolean check=false;
        if(!Pattern.matches("[a-zA-Z]+", lastName)) {

            check =true;

        } else {
            check=false;
        }
        return check;
    }

    private void openRun() {

        if (Build.VERSION.SDK_INT >= 23) {
            insertDummyContactWrapper();
            // Marshmallow+
        } else {
            // Pre-Marshmallow
        }
    }

    private void insertDummyContactWrapper() {
        List<String> permissionsNeeded = new ArrayList<String>();

        final List<String> permissionsList = new ArrayList<String>();
        if (!addPermission(permissionsList, android.Manifest.permission.CAMERA))
            permissionsNeeded.add("Camera");
        if (!addPermission(permissionsList, android.Manifest.permission.READ_EXTERNAL_STORAGE))
            permissionsNeeded.add("Read Storage");
        if (!addPermission(permissionsList, android.Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("Write Storage");

        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                // Need Rationale
                String message = "You need to grant access to " + permissionsNeeded.get(0);
                for (int i = 1; i < permissionsNeeded.size(); i++)
                    message = message + ", " + permissionsNeeded.get(i);
//                showMessageOKCancel(message,
//                        new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                            REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                }
//                            }
//                        });
                return;
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            }
            return;
        }
//        insertDummyContact();
    }

    private boolean addPermission(List<String> permissionsList, String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (getActivity().checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(permission);
                // Check for Rationale Option
                if (!shouldShowRequestPermissionRationale(permission))
                    return false;
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                Map<String, Integer> perms = new HashMap<String, Integer>();
                // Initial
                perms.put(android.Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(android.Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(android.Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                // Fill with results
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                // Check for ACCESS_FINE_LOCATION
                if (perms.get(android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                        && perms.get(android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    // All Permissions Granted
//                    insertDummyContact();
                } else {
                    // Permission Denied
//                    Toast.makeText(SplashActivity.this, "Some Permission is Denied", Toast.LENGTH_SHORT)
//                            .show();
                    openRun();
                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public void registerUser()
    {
        GPSTracker gpsTracker = new GPSTracker(getActivity());

        SignUpActivity.latitude = gpsTracker.getLatitude()+"";
        SignUpActivity.longitude = gpsTracker.getLongitude()+"";

        String url = WebServiceAPI.API_REGISTER;

        //Email,MobileNo,Password,Gender,Firstname,Lastname,DeviceType(IOS : 1 , Android : 2),Token,Lat,Lng,Image

        Map<String, Object> params = new HashMap<String, Object>();
        params.put(WebServiceAPI.PARAM_EMAIL,SignUpActivity.email);
        params.put(WebServiceAPI.PARAM_MOBILE_NUMBER,SignUpActivity.phoneNumber);
        params.put(WebServiceAPI.PARAM_PASSWORD,SignUpActivity.password);
        params.put(WebServiceAPI.PARAM_GENDER,SignUpActivity.gender);
        params.put(WebServiceAPI.PARAM_FIRST_NAME,SignUpActivity.firstName);
        params.put(WebServiceAPI.PARAM_LAST_NAME,SignUpActivity.lastName);
        params.put(WebServiceAPI.PARAM_DEVICE_TYPE,SignUpActivity.deviceType);
        params.put(WebServiceAPI.PARAM_TOKEN,SignUpActivity.token);
        params.put(WebServiceAPI.PARAM_LAT,SignUpActivity.latitude);
        params.put(WebServiceAPI.PARAM_LONG,SignUpActivity.longitude);
        params.put(WebServiceAPI.PARAM_IMAGE,SignUpActivity.userImage);
        params.put(WebServiceAPI.PARAM_REFERRAL_CODE,SignUpActivity.referralCode);

        Log.e("call", "url = " + url);
        Log.e("call", "param = " + params);

        dialogClass.showDialog();
        aQuery.ajax(url.trim(), params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e("responseCode", " = " + responseCode);
                    Log.e("Response", " = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                if (json.has("walletBalance"))
                                {
                                    String walletBallence = json.getString("walletBalance");

                                    if (walletBallence!=null && !walletBallence.equalsIgnoreCase(""))
                                    {
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_WALLET_BALLENCE,walletBallence,getActivity());
                                    }
                                    else
                                    {
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_WALLET_BALLENCE,"0",getActivity());
                                    }
                                }
                                else
                                {
                                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_WALLET_BALLENCE,"0",getActivity());
                                }

                                if (json.has("profile"))
                                {
                                    JSONObject profile = json.getJSONObject("profile");

                                    if (profile!=null)
                                    {
                                        String Fullname="",Email="",Password="",MobileNo="",Gender="",Image="",DeviceType="",Token="";
                                        String Lat="",Lng="",Status="",CreatedDate="",Id="",QRCode="",Verify="";
                                        String CompanyName="",ABN="",LicenceImage="",referalCode="",referalTotal="";

                                        if (profile.has("CompanyName"))
                                        {
                                            CompanyName = profile.getString("CompanyName");
                                        }

                                        if (profile.has("ABN"))
                                        {
                                            ABN = profile.getString("ABN");
                                        }

                                        if (profile.has("LicenceImage"))
                                        {
                                            LicenceImage = profile.getString("LicenceImage");
                                        }

                                        if (profile.has("Fullname"))
                                        {
                                            Fullname = profile.getString("Fullname");
                                        }

                                        if (profile.has("Email"))
                                        {
                                            Email = profile.getString("Email");
                                        }

                                        if (profile.has("Password"))
                                        {
                                            Password = profile.getString("Password");
                                        }

                                        if (profile.has("MobileNo"))
                                        {
                                            MobileNo = "+"+profile.getString("MobileNo");
                                        }

                                        if (profile.has("Gender"))
                                        {
                                            Gender = profile.getString("Gender");
                                        }

                                        if (profile.has("Image"))
                                        {
                                            Image = profile.getString("Image");
                                        }

                                        if (profile.has("DeviceType"))
                                        {
                                            DeviceType = profile.getString("DeviceType");
                                        }

                                        if (profile.has("Token"))
                                        {
                                            Token = profile.getString("Token");
                                        }

                                        if (profile.has("Lat"))
                                        {
                                            Lat = profile.getString("Lat");
                                        }

                                        if (profile.has("Lng"))
                                        {
                                            Lng = profile.getString("Lng");
                                        }

                                        if (profile.has("Status"))
                                        {
                                            Status = profile.getString("Status");
                                        }

                                        if (profile.has("CreatedDate"))
                                        {
                                            CreatedDate = profile.getString("CreatedDate");
                                        }

                                        if (profile.has("Id"))
                                        {
                                            Id = profile.getString("Id");
                                        }

                                        if (profile.has("QRCode"))
                                        {
                                            QRCode = profile.getString("QRCode");
                                        }

                                        if (profile.has("Verify"))
                                        {
                                            Verify = profile.getString("Verify");
                                        }

                                        if (profile.has("ReferralCode"))
                                        {
                                            referalCode = profile.getString("ReferralCode");
                                        }

                                        if (profile.has("ReferralAmount"))
                                        {
                                            referalTotal = profile.getString("ReferralAmount");
                                        }

                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_FULL_NAME,Fullname,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_EMAIL,Email,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_PASSWORD,Password,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_PHONE_NUMBER,MobileNo,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_GENDER,Gender,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_IMAGE,Image,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_DEVICE_TYPE,DeviceType,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TOKEN,Token,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_LATITUDE,Lat,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_LONGITUDE,Lng,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_STATUS,Status,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_CREATED_DATE,CreatedDate,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_ID,Id,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_QR_CODE,QRCode,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_VERIFY_USER,Verify,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TICK_PAY_SPLASH,"0",getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_COMPANY_NAME,CompanyName,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_ABN,ABN,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_LICENCE_IMAGE,LicenceImage,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_REFERAL_CODE,referalCode,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_REFERAL_TOTAL,referalTotal,getActivity());
                                        SessionSave.saveUserSession(Common.CREATED_PASSCODE,"",getActivity());
                                        SessionSave.saveUserSession(Common.IS_PASSCODE_REQUIRED,"0",getActivity());

                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BANK_NAME,"",getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BSB,"",getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BANK_ACCOUNT_NUMBER,"",getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_DESCRIPTION,"",getActivity());
                                    }
                                    else
                                    {
                                        Log.e("errorrr","profile null");
                                    }
                                }
                                else
                                {
                                    Log.e("errorrr","profile not available");
                                }

                                if (json.has("car_class"))
                                {
                                    JSONArray car_class = json.getJSONArray("car_class");

                                    if (car_class!=null && car_class.length()>0)
                                    {
                                        SignUpActivity.firstName="";
                                        SignUpActivity.lastName="";
                                        SignUpActivity.email="";
                                        SignUpActivity.phoneNumber="";
                                        SignUpActivity.password="";
                                        SignUpActivity.confirmPassword="";
                                        SignUpActivity.gender="male";
                                        SignUpActivity.latitude="";
                                        SignUpActivity.longitude="";
                                        SignUpActivity.deviceType="2";
                                        SignUpActivity.userImage = null;
                                        SignUpActivity.token="";
                                        SignUpActivity.referralCode="";

                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_CAR_CLASS,car_class.toString(),getActivity());
                                        dialogClass.hideDialog();
                                        Intent intent = new Intent(getActivity(), MainActivity.class);
                                        startActivity(intent);
                                        getActivity().finish();
                                        ((Activity)getActivity()).overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
                                    }
                                    else
                                    {
                                        Log.e("errorrr","car_class null or lenth available");
                                        dialogClass.hideDialog();
                                    }
                                }
                                else
                                {
                                    Log.e("errorrr","car_class not available");
                                    dialogClass.hideDialog();
                                }
                            }
                            else
                            {
                                Log.e("errorrr","status false");
                                dialogClass.hideDialog();

                                String message = "Please try again later!";

                                if (json.has("message"))
                                {
                                    message = json.getString("message");
                                }

                                InternetDialog internetDialog = new InternetDialog(getActivity());
                                internetDialog.showDialog(message);
                            }
                        }
                        else
                        {
                            Log.e("errorrr","status not available");
                            dialogClass.hideDialog();

                            String message = "Please try again later!";

                            if (json.has("message"))
                            {
                                message = json.getString("message");
                            }

                            InternetDialog internetDialog = new InternetDialog(getActivity());
                            internetDialog.showDialog(message);
                        }
                    }
                    else
                    {
                        Log.e("errorrr","json null ");
                        dialogClass.hideDialog();

                        String message = "Please try again later!";

                        if (json.has("message"))
                        {
                            message = json.getString("message");
                        }

                        InternetDialog internetDialog = new InternetDialog(getActivity());
                        internetDialog.showDialog(message);
                    }
                }
                catch (Exception e)
                {
                    Log.e("Exception","Exception "+e.toString());
                    dialogClass.hideDialog();

                    String message = "Please try again later!";

                    InternetDialog internetDialog = new InternetDialog(getActivity());
                    internetDialog.showDialog(message);
                }
            }

        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }
}

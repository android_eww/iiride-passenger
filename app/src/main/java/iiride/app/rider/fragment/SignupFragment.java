package iiride.app.rider.fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.gson.Gson;
import com.hbb20.CountryCodePicker;

import iiride.app.rider.R;
import iiride.app.rider.activity.SignUpActivity;
import iiride.app.rider.comman.Common;
import iiride.app.rider.comman.SessionSave;
import iiride.app.rider.comman.WebServiceAPI;
import iiride.app.rider.other.DialogClass;
import iiride.app.rider.other.InternetDialog;
import iiride.app.rider.view.MySnackBar;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SignupFragment extends Fragment {

    private LinearLayout layout_next,layout_next1,llOtp,llDetail;
    private EditText editPhone,editEmail,editPass,editConfirmPass,editOtp;
    private View rotView = null;
    private LinearLayout ll_Back;
    private ImageView iv_Back;
    private LinearLayout ll_RootView;
    private MySnackBar mySnackBar;
    private TextView tvResendOtp;
    private String otp = "";

    private DialogClass dialogClass;
    private AQuery aQuery;
    private CountryCodePicker codePicker;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        final View rootView = inflater.inflate(R.layout.fragment_signup, container,false);
        rotView = rootView;

        dialogClass = new DialogClass(getActivity(),0);
        aQuery = new AQuery(getActivity());

        layout_next = (LinearLayout)rootView.findViewById(R.id.layout_next);
        layout_next1 = rootView.findViewById(R.id.layout_next1);
        llOtp = rootView.findViewById(R.id.llOtp);
        llDetail = rootView.findViewById(R.id.llDetail);
        editPhone = (EditText)rootView.findViewById(R.id.input_phone);
        editOtp = rootView.findViewById(R.id.input_otp);
        editEmail = (EditText)rootView.findViewById(R.id.input_email);
        editPass = (EditText)rootView.findViewById(R.id.input_pass);
        editConfirmPass = (EditText)rootView.findViewById(R.id.input_confirm_pass);
        codePicker = rootView.findViewById(R.id.countryCodePicker);
        ll_RootView = (LinearLayout) rootView.findViewById(R.id.main_content);
        tvResendOtp = rootView.findViewById(R.id.tvResendOtp);

        ll_Back = (LinearLayout)rootView.findViewById(R.id.back_layout);
        iv_Back = (ImageView) rootView.findViewById(R.id.back_imageview);

        layout_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToNext();
            }
        });

        tvResendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callGetOtp(editEmail.getText().toString(),codePicker.getSelectedCountryCodeWithPlus()+editPhone.getText().toString());
            }
        });

        layout_next1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(otp.equalsIgnoreCase(editOtp.getText().toString()))
                {
                    ((SignUpActivity)getActivity()).mViewPager.setCurrentItem(1);
                }
                else
                {
//                    InternetDialog internetDialog = new InternetDialog(getActivity());
//                    internetDialog.showDialog("Your mobile verification code is not valid");
                    mySnackBar.showSnackBar(ll_RootView, "Your mobile verification code is not valid");
                    editConfirmPass.setFocusableInTouchMode(true);
                    editConfirmPass.requestFocus();
                }
            }
        });

        mySnackBar = new MySnackBar(getActivity());

        iv_Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(llOtp.getVisibility() == View.VISIBLE)
                {
                    llOtp.setVisibility(View.GONE);
                    llDetail.setVisibility(View.VISIBLE);
                    editOtp.setText("");
                    return;
                }

                SignUpActivity.firstName="";
                SignUpActivity.lastName="";
                SignUpActivity.email="";
                SignUpActivity.phoneNumber="";
                SignUpActivity.password="";
                SignUpActivity.confirmPassword="";
                SignUpActivity.gender="male";
                SignUpActivity.latitude="";
                SignUpActivity.longitude="";
                SignUpActivity.deviceType="2";
                SignUpActivity.token="";

                (getActivity()).finish();
                getActivity().overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);
            }
        });

        ll_Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(llOtp.getVisibility() == View.VISIBLE)
                {
                    llOtp.setVisibility(View.GONE);
                    llDetail.setVisibility(View.VISIBLE);
                    editOtp.setText("");
                    return;
                }

                SignUpActivity.firstName="";
                SignUpActivity.lastName="";
                SignUpActivity.email="";
                SignUpActivity.phoneNumber="";
                SignUpActivity.password="";
                SignUpActivity.confirmPassword="";
                SignUpActivity.gender="male";
                SignUpActivity.latitude="";
                SignUpActivity.longitude="";
                SignUpActivity.deviceType="2";
                SignUpActivity.userImage = null;
                SignUpActivity.token="";

                (getActivity()).finish();
                getActivity().overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);
            }
        });

        initializeCountry();

        return rootView;
    }

    private void initializeCountry()
    {
        try
        {
            if (SessionSave.getUserSession(Common.INIT_RESPONSE,getActivity())!=null && !SessionSave.getUserSession(Common.INIT_RESPONSE,getActivity()).equalsIgnoreCase(""))
            {
                JSONObject jsonObject = new JSONObject(SessionSave.getUserSession(Common.INIT_RESPONSE,getActivity()));

                if (jsonObject!=null && jsonObject.has("countries"))
                {
                    JSONArray jsonArray = jsonObject.getJSONArray("countries");

                    if (jsonArray!=null && jsonArray.length()>0)
                    {
                        String country = null;
                        String countryNameCode = "AU";
                        for (int i=0; i<jsonArray.length(); i++)
                        {
                            JSONObject countryObject = jsonArray.getJSONObject(i);

                            if (country==null)
                            {
                                if (countryObject!=null && countryObject.has("CountryCode"))
                                {
                                    if (countryObject.getString("CountryCode")!=null && countryObject.getString("CountryCode").equalsIgnoreCase("AU"))
                                    {
                                        Log.e("pos::", i + "");
                                        country = "AU";
                                        countryNameCode = "AU";

                                    }
                                }

                                if (countryObject!=null && countryObject.has("CountryCode"))
                                {
                                    if (countryObject.getString("CountryCode")!=null && countryObject.getString("CountryCode").equalsIgnoreCase("US"))
                                    {
                                        Log.e("pos::", i + "");
                                        country = "US";
                                        countryNameCode = "US";
                                    }
                                }
                            }
                            else
                            {
                                if (countryObject!=null && countryObject.has("CountryCode"))
                                {
                                    if (countryObject.getString("CountryCode")!=null && countryObject.getString("CountryCode").equalsIgnoreCase("AU"))
                                    {
                                        Log.e("pos::", i + "");
                                        country += ",AU";

                                    }
                                }

                                if (countryObject!=null && countryObject.has("CountryCode"))
                                {
                                    if (countryObject.getString("CountryCode")!=null && countryObject.getString("CountryCode").equalsIgnoreCase("US"))
                                    {
                                        Log.e("pos::", i + "");
                                        country += ",US";
                                    }
                                }
                            }
                        }

                        if (country != null)
                            codePicker.setCustomMasterCountries(country);
                        else
                            codePicker.setCustomMasterCountries("");

                        codePicker.setCountryForNameCode(countryNameCode);
                    }
                    else
                    {
                        /*codePicker.setCustomMasterCountries("AU,US");
                        codePicker.setCountryForNameCode("AU,US");*/
                    }
                }
                else
                {
                    /*codePicker.setCustomMasterCountries("AU,US");
                    codePicker.setCountryForNameCode("AU,US");*/
                }
            }
            else
            {
                /*codePicker.setCustomMasterCountries("AU,US");
                codePicker.setCountryForNameCode("AU,US");*/
            }

        }
        catch (Exception e)
        {
            Log.e("call","country parse exception : "+e.getMessage());
            /*codePicker.setCustomMasterCountries("AU,US");
            codePicker.setCountryForNameCode("AU,US");*/
        }
    }

    // validating email id
    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    // validating password with retype password
    private boolean isValidPassword(String pass) {
        if (pass != null && pass.length() > 6) {
            return true;
        }
        return false;
    }

    private boolean isValidMobile(String phone) {
        boolean check=false;

        if(!Pattern.matches("[0-9]+", phone)) {

            check =true;
        } else {
            check=false;
        }
        return check;
    }

    public void goToNext()
    {
        if (editPhone.getText().toString() != null && !editPhone.getText().toString().equals(""))
        {
            if (codePicker.getSelectedCountryCodeWithPlus().equalsIgnoreCase("+61"))
            {
                if (editPhone.getText().toString().trim().length() == 9)
                {
                    if (editEmail.getText().toString() != null && !editEmail.getText().toString().equals(""))
                    {
                        if (editPass.getText().toString() != null && !editPass.getText().toString().equals(""))
                        {
                            if (editConfirmPass.getText().toString() != null && !editConfirmPass.getText().toString().equals(""))
                            {
                                if (isValidEmail(editEmail.getText().toString()))
                                {
                                    if (editPass.getText().toString().length() >= 6)
                                    {
                                        if (editPass.getText().toString().equals(editConfirmPass.getText().toString()))
                                        {
                                            SignUpActivity.phoneNumber = codePicker.getSelectedCountryCodeWithPlus()+editPhone.getText().toString().trim();
                                            SignUpActivity.email = editEmail.getText().toString().trim();
                                            SignUpActivity.password = editPass.getText().toString().trim();
                                            SignUpActivity.confirmPassword = editConfirmPass.getText().toString().trim();
                                            callGetOtp(editEmail.getText().toString(), codePicker.getSelectedCountryCodeWithPlus()+editPhone.getText().toString());
                                        }
                                        else
                                        {
                                            mySnackBar.showSnackBar(ll_RootView, "Password and Confirm Password not match!");
                                            editConfirmPass.setFocusableInTouchMode(true);
                                            editConfirmPass.requestFocus();
                                        }
                                    }
                                    else
                                    {
                                        mySnackBar.showSnackBar(ll_RootView, "Password length must be greater than 5!");
                                        editPass.setFocusableInTouchMode(true);
                                        editPass.requestFocus();
                                    }
                                }
                                else
                                {
                                    mySnackBar.showSnackBar(ll_RootView, "Invalid Email!");
                                    editEmail.setFocusableInTouchMode(true);
                                    editEmail.requestFocus();
                                }
                            }
                            else
                            {
                                mySnackBar.showSnackBar(ll_RootView, "Please enter Confirm Password!");
                                editConfirmPass.setFocusableInTouchMode(true);
                                editConfirmPass.requestFocus();
                            }
                        }
                        else
                        {
                            mySnackBar.showSnackBar(ll_RootView, "Please enter Password!");
                            editPass.setFocusableInTouchMode(true);
                            editPass.requestFocus();
                        }
                    }
                    else
                    {
                        mySnackBar.showSnackBar(ll_RootView, "Please enter Email!");
                        editEmail.setFocusableInTouchMode(true);
                        editEmail.requestFocus();
                    }
                }
                else
                {
                    mySnackBar.showSnackBar(ll_RootView, "Please enter Valid Phone Number!");
                    editPhone.setFocusableInTouchMode(true);
                    editPhone.requestFocus();
                }
            }
            else
            {
                if (editPhone.getText().toString().trim().length() == 10)
                {
                    if (editEmail.getText().toString() != null && !editEmail.getText().toString().equals(""))
                    {
                        if (editPass.getText().toString() != null && !editPass.getText().toString().equals(""))
                        {
                            if (editConfirmPass.getText().toString() != null && !editConfirmPass.getText().toString().equals(""))
                            {
                                if (isValidEmail(editEmail.getText().toString()))
                                {
                                    if (editPass.getText().toString().length() >= 6)
                                    {
                                        if (editPass.getText().toString().equals(editConfirmPass.getText().toString()))
                                        {
                                            SignUpActivity.phoneNumber = codePicker.getSelectedCountryCodeWithPlus()+editPhone.getText().toString().trim();
                                            SignUpActivity.email = editEmail.getText().toString().trim();
                                            SignUpActivity.password = editPass.getText().toString().trim();
                                            SignUpActivity.confirmPassword = editConfirmPass.getText().toString().trim();
                                            callGetOtp(editEmail.getText().toString(), codePicker.getSelectedCountryCodeWithPlus()+editPhone.getText().toString());
                                        }
                                        else
                                        {
                                            mySnackBar.showSnackBar(ll_RootView, "Password and Confirm Password not match!");
                                            editConfirmPass.setFocusableInTouchMode(true);
                                            editConfirmPass.requestFocus();
                                        }
                                    }
                                    else
                                    {
                                        mySnackBar.showSnackBar(ll_RootView, "Password length must be greater than 5!");
                                        editPass.setFocusableInTouchMode(true);
                                        editPass.requestFocus();
                                    }
                                }
                                else
                                {
                                    mySnackBar.showSnackBar(ll_RootView, "Invalid Email!");
                                    editEmail.setFocusableInTouchMode(true);
                                    editEmail.requestFocus();
                                }
                            }
                            else
                            {
                                mySnackBar.showSnackBar(ll_RootView, "Please enter Confirm Password!");
                                editConfirmPass.setFocusableInTouchMode(true);
                                editConfirmPass.requestFocus();
                            }
                        }
                        else
                        {
                            mySnackBar.showSnackBar(ll_RootView, "Please enter Password!");
                            editPass.setFocusableInTouchMode(true);
                            editPass.requestFocus();
                        }
                    }
                    else
                    {
                        mySnackBar.showSnackBar(ll_RootView, "Please enter Email!");
                        editEmail.setFocusableInTouchMode(true);
                        editEmail.requestFocus();
                    }
                }
                else
                {
                    mySnackBar.showSnackBar(ll_RootView, "Please enter Valid Phone Number!");
                    editPhone.setFocusableInTouchMode(true);
                    editPhone.requestFocus();
                }
            }

            /*if (editEmail.getText().toString() != null && !editEmail.getText().toString().equals(""))
            {
                if (editPass.getText().toString() != null && !editPass.getText().toString().equals(""))
                {
                    if (editConfirmPass.getText().toString() != null && !editConfirmPass.getText().toString().equals(""))
                    {
                        if (isValidEmail(editEmail.getText().toString()))
                        {
                            if (editPass.getText().toString().length() >= 6)
                            {
                                if (editPass.getText().toString().equals(editConfirmPass.getText().toString()))
                                {
                                    SignUpActivity.phoneNumber = codePicker.getSelectedCountryCodeWithPlus()+editPhone.getText().toString().trim();
                                    SignUpActivity.email = editEmail.getText().toString().trim();
                                    SignUpActivity.password = editPass.getText().toString().trim();
                                    SignUpActivity.confirmPassword = editConfirmPass.getText().toString().trim();

                                    callGetOtp(editEmail.getText().toString(), codePicker.getSelectedCountryCodeWithPlus()+editPhone.getText().toString());
                                }
                                else
                                {
                                    mySnackBar.showSnackBar(ll_RootView, "Password and Confirm Password not match!");
                                    editConfirmPass.setFocusableInTouchMode(true);
                                    editConfirmPass.requestFocus();
                                }
                            }
                            else
                            {
                                mySnackBar.showSnackBar(ll_RootView, "Password length must be greater than 5!");
                                editPass.setFocusableInTouchMode(true);
                                editPass.requestFocus();
                            }
                        }
                        else
                        {
                            mySnackBar.showSnackBar(ll_RootView, "Invalid Email!");
                            editEmail.setFocusableInTouchMode(true);
                            editEmail.requestFocus();
                        }
                    }
                    else
                    {
                        mySnackBar.showSnackBar(ll_RootView, "Please enter Confirm Password!");
                        editConfirmPass.setFocusableInTouchMode(true);
                        editConfirmPass.requestFocus();
                    }
                }
                else
                {
                    mySnackBar.showSnackBar(ll_RootView, "Please enter Password!");
                    editPass.setFocusableInTouchMode(true);
                    editPass.requestFocus();
                }
            }
            else
            {
                mySnackBar.showSnackBar(ll_RootView, "Please enter Email!");
                editEmail.setFocusableInTouchMode(true);
                editEmail.requestFocus();
            }*/
        }
        else
        {
            mySnackBar.showSnackBar(ll_RootView, "Please enter Phone Number!");
            editPhone.setFocusableInTouchMode(true);
            editPhone.requestFocus();
        }
    }

    private void callGetOtp(String email, String mobile)
    {

        String url = WebServiceAPI.API_MOBILE_EMAIL_OTP;

        //Email,MobileNo,Password,Gender,Firstname,Lastname,DeviceType(IOS : 1 , Android : 2),Token,Lat,Lng,Image

        Map<String, Object> params = new HashMap<String, Object>();
        params.put(WebServiceAPI.PARAM_EMAIL,email);
        params.put(WebServiceAPI.PARAM_MOBILE_NUMBER,mobile);

        Log.e("call", "url = " + url);
        Log.e("call", "param = " + params);

        dialogClass.showDialog();
        aQuery.ajax(url.trim(), params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    dialogClass.hideDialog();
                    Log.e("call", "json = " + json);
                    if(json != null && !json.toString().equalsIgnoreCase(""))
                    {
                        Log.e("call", "json = " + json.toString());

                        JSONObject jsonObject = new JSONObject(json.toString());
                        if(jsonObject.has("status") && jsonObject.getBoolean("status"))
                        {
                            otp = jsonObject.optString("otp");
                            llOtp.setVisibility(View.VISIBLE);
                            llDetail.setVisibility(View.GONE);
                        }
                        else
                        {
                            InternetDialog internetDialog = new InternetDialog(getActivity());
                            internetDialog.showDialog(jsonObject.getString("message"));
                        }
                    }

                }
                catch (Exception e)
                {
                    Log.e("Exception","Exception "+e.toString());
                    dialogClass.hideDialog();

                    String message = "Please try again later!";

                    InternetDialog internetDialog = new InternetDialog(getActivity());
                    internetDialog.showDialog(message);
                }
            }

        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }
}


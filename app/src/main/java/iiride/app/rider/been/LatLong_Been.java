package iiride.app.rider.been;

/**
 * Created by ADMIN on 10/21/2016.
 */
public class LatLong_Been {

    private double Lat,Long;

    public LatLong_Been(double Lat, double Long)
    {
        this.Lat=Lat;
        this.Long=Long;
    }

    public double getLat() {
        return Lat;
    }

    public void setLat(double lat) {
        Lat = lat;
    }

    public double getLong() {
        return Long;
    }

    public void setLong(double aLong) {
        Long = aLong;
    }
}

package iiride.app.rider.been;

/**
 * Created by ADMIN on 10/21/2016.
 */
public class CarType_Been {

    private String Id,CategoryId,Name,Sort,BaseFare,MinKm,PerKmCharge,CancellationFee,NightCharge,NightTimeFrom;
    private String NightTimeTo,SpecialEventSurcharge,SpecialEventTimeFrom,SpecialEventTimeTo,WaitingTimeCost,MinuteFare;
    private String BookingFee,Capacity,Image,Description,Status;
    private int avai = 0;

    public CarType_Been(
            String Id,
            String CategoryId,
            String Name,
            String Sort,
            String BaseFare,
            String MinKm,
            String PerKmCharge,
            String CancellationFee,
            String NightCharge,
            String NightTimeFrom,
            String NightTimeTo,
            String SpecialEventSurcharge,
            String SpecialEventTimeFrom,
            String SpecialEventTimeTo,
            String WaitingTimeCost,
            String MinuteFare,
            String BookingFee,
            String Capacity,
            String Image,
            String Description,
            String Status,
            int avai)
    {
        this.Id=Id;
        this.CategoryId=CategoryId;
        this.Name=Name;
        this.Sort=Sort;
        this.BaseFare=BaseFare;
        this.MinKm=MinKm;
        this.PerKmCharge=PerKmCharge;
        this.CancellationFee=CancellationFee;
        this.NightCharge=NightCharge;
        this.NightTimeFrom=NightTimeFrom;
        this.NightTimeTo=NightTimeTo;
        this.SpecialEventSurcharge=SpecialEventSurcharge;
        this.SpecialEventTimeFrom=SpecialEventTimeFrom;
        this.SpecialEventTimeTo=SpecialEventTimeTo;
        this.WaitingTimeCost=WaitingTimeCost;
        this.MinuteFare=MinuteFare;
        this.BookingFee=BookingFee;
        this.Capacity=Capacity;
        this.Image=Image;
        this.Description=Description;
        this.Status=Status;
        this.avai=avai;
    }

    public String getBaseFare() {
        return BaseFare;
    }

    public void setBaseFare(String baseFare) {
        BaseFare = baseFare;
    }

    public String getBookingFee() {
        return BookingFee;
    }

    public void setBookingFee(String bookingFee) {
        BookingFee = bookingFee;
    }

    public String getCancellationFee() {
        return CancellationFee;
    }

    public void setCancellationFee(String cancellationFee) {
        CancellationFee = cancellationFee;
    }

    public String getCapacity() {
        return Capacity;
    }

    public void setCapacity(String capacity) {
        Capacity = capacity;
    }

    public void setCategoryId(String categoryId) {
        CategoryId = categoryId;
    }

    public String getCategoryId() {
        return CategoryId;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public void setId(String id) {
        Id = id;
    }

    public void setImage(String image) {
        Image = image;
    }

    public void setMinKm(String minKm) {
        MinKm = minKm;
    }

    public String getDescription() {
        return Description;
    }

    public String getId() {
        return Id;
    }

    public String getImage() {
        return Image;
    }

    public String getMinKm() {
        return MinKm;
    }

    public void setMinuteFare(String minuteFare) {
        MinuteFare = minuteFare;
    }

    public String getMinuteFare() {
        return MinuteFare;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getName() {
        return Name;
    }

    public void setNightCharge(String nightCharge) {
        NightCharge = nightCharge;
    }

    public String getNightCharge() {
        return NightCharge;
    }

    public void setNightTimeFrom(String nightTimeFrom) {
        NightTimeFrom = nightTimeFrom;
    }

    public String getNightTimeFrom() {
        return NightTimeFrom;
    }

    public String getNightTimeTo() {
        return NightTimeTo;
    }

    public void setNightTimeTo(String nightTimeTo) {
        NightTimeTo = nightTimeTo;
    }

    public String getPerKmCharge() {
        return PerKmCharge;
    }

    public void setPerKmCharge(String perKmCharge) {
        PerKmCharge = perKmCharge;
    }

    public String getSort() {
        return Sort;
    }

    public void setSort(String sort) {
        Sort = sort;
    }

    public String getSpecialEventSurcharge() {
        return SpecialEventSurcharge;
    }

    public void setSpecialEventSurcharge(String specialEventSurcharge) {
        SpecialEventSurcharge = specialEventSurcharge;
    }

    public String getSpecialEventTimeFrom() {
        return SpecialEventTimeFrom;
    }

    public void setSpecialEventTimeFrom(String specialEventTimeFrom) {
        SpecialEventTimeFrom = specialEventTimeFrom;
    }

    public String getSpecialEventTimeTo() {
        return SpecialEventTimeTo;
    }

    public void setSpecialEventTimeTo(String specialEventTimeTo) {
        SpecialEventTimeTo = specialEventTimeTo;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getWaitingTimeCost() {
        return WaitingTimeCost;
    }

    public void setWaitingTimeCost(String waitingTimeCost) {
        WaitingTimeCost = waitingTimeCost;
    }

    public int getAvai() {
        return avai;
    }

    public void setAvai(int avai) {
        this.avai = avai;
    }
}

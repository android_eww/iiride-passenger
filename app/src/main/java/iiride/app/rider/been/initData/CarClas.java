package iiride.app.rider.been.initData;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CarClas {

@SerializedName("Id")
@Expose
private String id;
@SerializedName("CategoryId")
@Expose
private String categoryId;
@SerializedName("CityId")
@Expose
private String cityId;
@SerializedName("Name")
@Expose
private String name;
@SerializedName("Sort")
@Expose
private String sort;
@SerializedName("BaseFare")
@Expose
private String baseFare;
@SerializedName("MinKm")
@Expose
private String minKm;
@SerializedName("BelowAndAboveKmLimit")
@Expose
private String belowAndAboveKmLimit;
@SerializedName("BelowPerKmCharge")
@Expose
private String belowPerKmCharge;
@SerializedName("AbovePerKmCharge")
@Expose
private String abovePerKmCharge;
@SerializedName("CancellationFee")
@Expose
private String cancellationFee;
@SerializedName("NightChargeApplicable")
@Expose
private String nightChargeApplicable;
@SerializedName("NightCharge")
@Expose
private String nightCharge;
@SerializedName("NightTimeFrom")
@Expose
private String nightTimeFrom;
@SerializedName("NightTimeTo")
@Expose
private String nightTimeTo;
@SerializedName("SpecialEventSurcharge")
@Expose
private String specialEventSurcharge;
@SerializedName("SpecialEventTimeFrom")
@Expose
private String specialEventTimeFrom;
@SerializedName("SpecialEventTimeTo")
@Expose
private String specialEventTimeTo;
@SerializedName("WaitingTimePerMinuteCharge")
@Expose
private String waitingTimePerMinuteCharge;
@SerializedName("MinuteFare")
@Expose
private String minuteFare;
@SerializedName("BookingFee")
@Expose
private String bookingFee;
@SerializedName("Capacity")
@Expose
private String capacity;
@SerializedName("Image")
@Expose
private String image;
@SerializedName("Description")
@Expose
private String description;
@SerializedName("Status")
@Expose
private String status;

public String getId() {
return id;
}

public void setId(String id) {
this.id = id;
}

public String getCategoryId() {
return categoryId;
}

public void setCategoryId(String categoryId) {
this.categoryId = categoryId;
}

public String getCityId() {
return cityId;
}

public void setCityId(String cityId) {
this.cityId = cityId;
}

public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

public String getSort() {
return sort;
}

public void setSort(String sort) {
this.sort = sort;
}

public String getBaseFare() {
return baseFare;
}

public void setBaseFare(String baseFare) {
this.baseFare = baseFare;
}

public String getMinKm() {
return minKm;
}

public void setMinKm(String minKm) {
this.minKm = minKm;
}

public String getBelowAndAboveKmLimit() {
return belowAndAboveKmLimit;
}

public void setBelowAndAboveKmLimit(String belowAndAboveKmLimit) {
this.belowAndAboveKmLimit = belowAndAboveKmLimit;
}

public String getBelowPerKmCharge() {
return belowPerKmCharge;
}

public void setBelowPerKmCharge(String belowPerKmCharge) {
this.belowPerKmCharge = belowPerKmCharge;
}

public String getAbovePerKmCharge() {
return abovePerKmCharge;
}

public void setAbovePerKmCharge(String abovePerKmCharge) {
this.abovePerKmCharge = abovePerKmCharge;
}

public String getCancellationFee() {
return cancellationFee;
}

public void setCancellationFee(String cancellationFee) {
this.cancellationFee = cancellationFee;
}

public String getNightChargeApplicable() {
return nightChargeApplicable;
}

public void setNightChargeApplicable(String nightChargeApplicable) {
this.nightChargeApplicable = nightChargeApplicable;
}

public String getNightCharge() {
return nightCharge;
}

public void setNightCharge(String nightCharge) {
this.nightCharge = nightCharge;
}

public String getNightTimeFrom() {
return nightTimeFrom;
}

public void setNightTimeFrom(String nightTimeFrom) {
this.nightTimeFrom = nightTimeFrom;
}

public String getNightTimeTo() {
return nightTimeTo;
}

public void setNightTimeTo(String nightTimeTo) {
this.nightTimeTo = nightTimeTo;
}

public String getSpecialEventSurcharge() {
return specialEventSurcharge;
}

public void setSpecialEventSurcharge(String specialEventSurcharge) {
this.specialEventSurcharge = specialEventSurcharge;
}

public String getSpecialEventTimeFrom() {
return specialEventTimeFrom;
}

public void setSpecialEventTimeFrom(String specialEventTimeFrom) {
this.specialEventTimeFrom = specialEventTimeFrom;
}

public String getSpecialEventTimeTo() {
return specialEventTimeTo;
}

public void setSpecialEventTimeTo(String specialEventTimeTo) {
this.specialEventTimeTo = specialEventTimeTo;
}

public String getWaitingTimePerMinuteCharge() {
return waitingTimePerMinuteCharge;
}

public void setWaitingTimePerMinuteCharge(String waitingTimePerMinuteCharge) {
this.waitingTimePerMinuteCharge = waitingTimePerMinuteCharge;
}

public String getMinuteFare() {
return minuteFare;
}

public void setMinuteFare(String minuteFare) {
this.minuteFare = minuteFare;
}

public String getBookingFee() {
return bookingFee;
}

public void setBookingFee(String bookingFee) {
this.bookingFee = bookingFee;
}

public String getCapacity() {
return capacity;
}

public void setCapacity(String capacity) {
this.capacity = capacity;
}

public String getImage() {
return image;
}

public void setImage(String image) {
this.image = image;
}

public String getDescription() {
return description;
}

public void setDescription(String description) {
this.description = description;
}

public String getStatus() {
return status;
}

public void setStatus(String status) {
this.status = status;
}

}



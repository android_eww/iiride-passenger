package iiride.app.rider.been;

public class PreviousDue_Been {

    private String Id, BookingBy, PassengerId, CityId, SPId, ModelId, DriverId, CreatedDate, TransactionId, PaymentStatus;
    private String PickupTime, DropTime, TripDuration, TripDistance, PickupLocation, DropoffLocation;
    private String NightFareApplicable, NightFare, TripFare, DistanceFare, WaitingTime, WaitingTimeCost, TollFee, BookingCharge;
    private String Tax, PromoCode, Discount, SubTotal, GrandTotal, Status, Trash, Reason, PaymentType, CardId, ByDriverAmount, AdminAmount;
    private String CompanyAmount, SPAmount, PickupLat, PickupLng, DropOffLat, DropOffLon, PassengerName, PassengerContact, PassengerEmail, Notes, FlightNumber, PaidToDriver, PaidSP, BabySeat, ModifyDate, PickupSuburb, DropoffSuburb;
    private String BookingType, ByDriverId, PastDuesId;

    public PreviousDue_Been(String id,
                            String bookingBy,
                            String passengerId,
                            String cityId,
                            String SPId,
                            String modelId,
                            String driverId,
                            String createdDate,
                            String transactionId,
                            String paymentStatus,
                            String pickupTime,
                            String dropTime,
                            String tripDuration,
                            String tripDistance,
                            String pickupLocation,
                            String dropoffLocation,
                            String nightFareApplicable,
                            String nightFare,
                            String tripFare,
                            String distanceFare,
                            String waitingTime,
                            String waitingTimeCost,
                            String tollFee,
                            String bookingCharge,
                            String tax,
                            String promoCode,
                            String discount,
                            String subTotal,
                            String grandTotal,
                            String status,
                            String trash,
                            String reason,
                            String paymentType,
                            String cardId,
                            String byDriverAmount,
                            String adminAmount,
                            String companyAmount,
                            String SPAmount,
                            String pickupLat,
                            String pickupLng,
                            String dropOffLat,
                            String dropOffLon,
                            String passengerName,
                            String passengerContact,
                            String passengerEmail,
                            String notes,
                            String flightNumber,
                            String paidToDriver,
                            String paidSP,
                            String babySeat,
                            String modifyDate,
                            String pickupSuburb,
                            String dropoffSuburb,
                            String bookingType,
                            String byDriverId,
                            String pastDuesId) {
        Id = id;
        BookingBy = bookingBy;
        PassengerId = passengerId;
        CityId = cityId;
        this.SPId = SPId;
        ModelId = modelId;
        DriverId = driverId;
        CreatedDate = createdDate;
        TransactionId = transactionId;
        PaymentStatus = paymentStatus;
        PickupTime = pickupTime;
        DropTime = dropTime;
        TripDuration = tripDuration;
        TripDistance = tripDistance;
        PickupLocation = pickupLocation;
        DropoffLocation = dropoffLocation;
        NightFareApplicable = nightFareApplicable;
        NightFare = nightFare;
        TripFare = tripFare;
        DistanceFare = distanceFare;
        WaitingTime = waitingTime;
        WaitingTimeCost = waitingTimeCost;
        TollFee = tollFee;
        BookingCharge = bookingCharge;
        Tax = tax;
        PromoCode = promoCode;
        Discount = discount;
        SubTotal = subTotal;
        GrandTotal = grandTotal;
        Status = status;
        Trash = trash;
        Reason = reason;
        PaymentType = paymentType;
        CardId = cardId;
        ByDriverAmount = byDriverAmount;
        AdminAmount = adminAmount;
        CompanyAmount = companyAmount;
        this.SPAmount = SPAmount;
        PickupLat = pickupLat;
        PickupLng = pickupLng;
        DropOffLat = dropOffLat;
        DropOffLon = dropOffLon;
        PassengerName = passengerName;
        PassengerContact = passengerContact;
        PassengerEmail = passengerEmail;
        Notes = notes;
        FlightNumber = flightNumber;
        PaidToDriver = paidToDriver;
        PaidSP = paidSP;
        BabySeat = babySeat;
        ModifyDate = modifyDate;
        PickupSuburb = pickupSuburb;
        DropoffSuburb = dropoffSuburb;
        BookingType = bookingType;
        ByDriverId = byDriverId;
        PastDuesId =pastDuesId;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getBookingBy() {
        return BookingBy;
    }

    public void setBookingBy(String bookingBy) {
        BookingBy = bookingBy;
    }

    public String getPassengerId() {
        return PassengerId;
    }

    public void setPassengerId(String passengerId) {
        PassengerId = passengerId;
    }

    public String getCityId() {
        return CityId;
    }

    public void setCityId(String cityId) {
        CityId = cityId;
    }

    public String getSPId() {
        return SPId;
    }

    public void setSPId(String SPId) {
        this.SPId = SPId;
    }

    public String getModelId() {
        return ModelId;
    }

    public void setModelId(String modelId) {
        ModelId = modelId;
    }

    public String getDriverId() {
        return DriverId;
    }

    public void setDriverId(String driverId) {
        DriverId = driverId;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public String getTransactionId() {
        return TransactionId;
    }

    public void setTransactionId(String transactionId) {
        TransactionId = transactionId;
    }

    public String getPaymentStatus() {
        return PaymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        PaymentStatus = paymentStatus;
    }

    public String getPickupTime() {
        return PickupTime;
    }

    public void setPickupTime(String pickupTime) {
        PickupTime = pickupTime;
    }

    public String getDropTime() {
        return DropTime;
    }

    public void setDropTime(String dropTime) {
        DropTime = dropTime;
    }

    public String getTripDuration() {
        return TripDuration;
    }

    public void setTripDuration(String tripDuration) {
        TripDuration = tripDuration;
    }

    public String getTripDistance() {
        return TripDistance;
    }

    public void setTripDistance(String tripDistance) {
        TripDistance = tripDistance;
    }

    public String getPickupLocation() {
        return PickupLocation;
    }

    public void setPickupLocation(String pickupLocation) {
        PickupLocation = pickupLocation;
    }

    public String getDropoffLocation() {
        return DropoffLocation;
    }

    public void setDropoffLocation(String dropoffLocation) {
        DropoffLocation = dropoffLocation;
    }

    public String getNightFareApplicable() {
        return NightFareApplicable;
    }

    public void setNightFareApplicable(String nightFareApplicable) {
        NightFareApplicable = nightFareApplicable;
    }

    public String getNightFare() {
        return NightFare;
    }

    public void setNightFare(String nightFare) {
        NightFare = nightFare;
    }

    public String getTripFare() {
        return TripFare;
    }

    public void setTripFare(String tripFare) {
        TripFare = tripFare;
    }

    public String getDistanceFare() {
        return DistanceFare;
    }

    public void setDistanceFare(String distanceFare) {
        DistanceFare = distanceFare;
    }

    public String getWaitingTime() {
        return WaitingTime;
    }

    public void setWaitingTime(String waitingTime) {
        WaitingTime = waitingTime;
    }

    public String getWaitingTimeCost() {
        return WaitingTimeCost;
    }

    public void setWaitingTimeCost(String waitingTimeCost) {
        WaitingTimeCost = waitingTimeCost;
    }

    public String getTollFee() {
        return TollFee;
    }

    public void setTollFee(String tollFee) {
        TollFee = tollFee;
    }

    public String getBookingCharge() {
        return BookingCharge;
    }

    public void setBookingCharge(String bookingCharge) {
        BookingCharge = bookingCharge;
    }

    public String getTax() {
        return Tax;
    }

    public void setTax(String tax) {
        Tax = tax;
    }

    public String getPromoCode() {
        return PromoCode;
    }

    public void setPromoCode(String promoCode) {
        PromoCode = promoCode;
    }

    public String getDiscount() {
        return Discount;
    }

    public void setDiscount(String discount) {
        Discount = discount;
    }

    public String getSubTotal() {
        return SubTotal;
    }

    public void setSubTotal(String subTotal) {
        SubTotal = subTotal;
    }

    public String getGrandTotal() {
        return GrandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        GrandTotal = grandTotal;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getTrash() {
        return Trash;
    }

    public void setTrash(String trash) {
        Trash = trash;
    }

    public String getReason() {
        return Reason;
    }

    public void setReason(String reason) {
        Reason = reason;
    }

    public String getPaymentType() {
        return PaymentType;
    }

    public void setPaymentType(String paymentType) {
        PaymentType = paymentType;
    }

    public String getCardId() {
        return CardId;
    }

    public void setCardId(String cardId) {
        CardId = cardId;
    }

    public String getByDriverAmount() {
        return ByDriverAmount;
    }

    public void setByDriverAmount(String byDriverAmount) {
        ByDriverAmount = byDriverAmount;
    }

    public String getAdminAmount() {
        return AdminAmount;
    }

    public void setAdminAmount(String adminAmount) {
        AdminAmount = adminAmount;
    }

    public String getCompanyAmount() {
        return CompanyAmount;
    }

    public void setCompanyAmount(String companyAmount) {
        CompanyAmount = companyAmount;
    }

    public String getSPAmount() {
        return SPAmount;
    }

    public void setSPAmount(String SPAmount) {
        this.SPAmount = SPAmount;
    }

    public String getPickupLat() {
        return PickupLat;
    }

    public void setPickupLat(String pickupLat) {
        PickupLat = pickupLat;
    }

    public String getPickupLng() {
        return PickupLng;
    }

    public void setPickupLng(String pickupLng) {
        PickupLng = pickupLng;
    }

    public String getDropOffLat() {
        return DropOffLat;
    }

    public void setDropOffLat(String dropOffLat) {
        DropOffLat = dropOffLat;
    }

    public String getDropOffLon() {
        return DropOffLon;
    }

    public void setDropOffLon(String dropOffLon) {
        DropOffLon = dropOffLon;
    }

    public String getPassengerName() {
        return PassengerName;
    }

    public void setPassengerName(String passengerName) {
        PassengerName = passengerName;
    }

    public String getPassengerContact() {
        return PassengerContact;
    }

    public void setPassengerContact(String passengerContact) {
        PassengerContact = passengerContact;
    }

    public String getPassengerEmail() {
        return PassengerEmail;
    }

    public void setPassengerEmail(String passengerEmail) {
        PassengerEmail = passengerEmail;
    }

    public String getNotes() {
        return Notes;
    }

    public void setNotes(String notes) {
        Notes = notes;
    }

    public String getFlightNumber() {
        return FlightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        FlightNumber = flightNumber;
    }

    public String getPaidToDriver() {
        return PaidToDriver;
    }

    public void setPaidToDriver(String paidToDriver) {
        PaidToDriver = paidToDriver;
    }

    public String getPaidSP() {
        return PaidSP;
    }

    public void setPaidSP(String paidSP) {
        PaidSP = paidSP;
    }

    public String getBabySeat() {
        return BabySeat;
    }

    public void setBabySeat(String babySeat) {
        BabySeat = babySeat;
    }

    public String getModifyDate() {
        return ModifyDate;
    }

    public void setModifyDate(String modifyDate) {
        ModifyDate = modifyDate;
    }

    public String getPickupSuburb() {
        return PickupSuburb;
    }

    public void setPickupSuburb(String pickupSuburb) {
        PickupSuburb = pickupSuburb;
    }

    public String getDropoffSuburb() {
        return DropoffSuburb;
    }

    public void setDropoffSuburb(String dropoffSuburb) {
        DropoffSuburb = dropoffSuburb;
    }

    public String getBookingType() {
        return BookingType;
    }

    public void setBookingType(String bookingType) {
        BookingType = bookingType;
    }

    public String getByDriverId() {
        return ByDriverId;
    }

    public void setByDriverId(String byDriverId) {
        ByDriverId = byDriverId;
    }

    public String getPastDuesId() {
        return PastDuesId;
    }

    public void setPastDuesId(String pastDuesId) {
        PastDuesId = pastDuesId;
    }
}

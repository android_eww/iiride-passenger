package iiride.app.rider.been;

/**
 * Created by ADMIN on 10/21/2016.
 */
public class ShoppingGreed_Been {

    private int id;
    private String title;

    public ShoppingGreed_Been(int id, String title)
    {
        this.id=id;
        this.title=title;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}

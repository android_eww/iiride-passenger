package iiride.app.rider.been;

/**
 * Created by ADMIN on 10/21/2016.
 */
public class UpComming_Been {

    private String Id,CompanyId,PassengerId,ModelId,DriverId,CreatedDate,TransactionId,PaymentStatus,PickupTime;
    private String PickupDateTime,DropTime,TripDuration,TripDistance,PickupLocation,DropoffLocation,NightFareApplicable;
    private String NightFare,TripFare,WaitingTime,WaitingTimeCost,TollFee,BookingCharge,Tax,PromoCode,CompanyTax;
    private String SubTotal,GrandTotal,Status,Reason,PaymentType,AdminAmount,CompanyAmount,PassengerType,PassengerName;
    private String PassengerContact,FlightNumber,Model,DriverName,HistoryType,BookingType,DropOffDateTime;
    private String DriverMobileNo,CarDetails_Id,CarDetails_CompanyId,CarDetails_DriverId,CarDetails_VehicleModel;
    private String CarDetails_Company,CarDetails_Color,CarDetails_VehicleRegistrationNo,CarDetails_RegistrationCertificate;
    private String CarDetails_VehicleInsuranceCertificate,CarDetails_RegistrationCertificateExpire,CarDetails_VehicleInsuranceCertificateExpire;
    private String CarDetails_VehicleImage,CarDetails_Description,Notes,BabySeat,CardId;
    private String PickupLat,PickupLng,DropOffLat,DropOffLon,PickupSuburb,DropoffSuburb;

    public UpComming_Been(
            String Id,
            String CompanyId,
            String PassengerId,
            String ModelId,
            String DriverId,
            String CreatedDate,
            String TransactionId,
            String PaymentStatus,
            String PickupTime,
            String PickupDateTime,
            String DropTime,
            String TripDuration,
            String TripDistance,
            String PickupLocation,
            String DropoffLocation,
            String NightFareApplicable,
            String NightFare,
            String TripFare,
            String WaitingTime,
            String WaitingTimeCost,
            String TollFee,
            String BookingCharge,
            String Tax,
            String PromoCode,
            String CompanyTax,
            String SubTotal,
            String GrandTotal,
            String Status,
            String Reason,
            String PaymentType,
            String AdminAmount,
            String CompanyAmount,
            String PassengerType,
            String PassengerName,
            String PassengerContact,
            String FlightNumber,
            String Model,
            String DriverName,
            String HistoryType,
            String BookingType,
            String DropOffDateTime,
            String DriverMobileNo,

            String CarDetails_Id,
            String CarDetails_CompanyId,
            String CarDetails_DriverId,
            String CarDetails_VehicleModel,
            String CarDetails_Company,
            String CarDetails_Color,
            String CarDetails_VehicleRegistrationNo,
            String CarDetails_RegistrationCertificate,
            String CarDetails_VehicleInsuranceCertificate,
            String CarDetails_RegistrationCertificateExpire,
            String CarDetails_VehicleInsuranceCertificateExpire,
            String CarDetails_VehicleImage,
            String CarDetails_Description,
            String Notes,
            String BabySeat,
            String CardId,
            String PickupLat,
            String PickupLng,
            String DropOffLat,
            String DropOffLon,
            String PickupSuburb,
            String DropoffSuburb
    )
    {
        this.Id=Id;
        this.CompanyId=CompanyId;
        this.PassengerId=PassengerId;
        this.ModelId=ModelId;
        this.DriverId=DriverId;
        this.CreatedDate=CreatedDate;
        this.TransactionId=TransactionId;
        this.PaymentStatus=PaymentStatus;
        this.PickupTime=PickupTime;
        this.PickupDateTime=PickupDateTime;
        this.DropTime=DropTime;
        this.TripDuration=TripDuration;
        this.TripDistance=TripDistance;
        this.PickupLocation=PickupLocation;
        this.DropoffLocation=DropoffLocation;
        this.NightFareApplicable=NightFareApplicable;
        this.NightFare=NightFare;
        this.TripFare=TripFare;
        this.WaitingTime=WaitingTime;
        this.WaitingTimeCost=WaitingTimeCost;
        this.TollFee=TollFee;
        this.BookingCharge=BookingCharge;
        this.Tax=Tax;
        this.PromoCode=PromoCode;
        this.CompanyTax=CompanyTax;
        this.SubTotal=SubTotal;
        this.GrandTotal=GrandTotal;
        this.Status=Status;
        this.Reason=Reason;
        this.PaymentType=PaymentType;
        this.AdminAmount=AdminAmount;
        this.CompanyAmount=CompanyAmount;
        this.PassengerType=PassengerType;
        this.PassengerName=PassengerName;
        this.PassengerContact=PassengerContact;
        this.FlightNumber=FlightNumber;
        this.Model=Model;
        this.DriverName=DriverName;
        this.HistoryType=HistoryType;
        this.BookingType=BookingType;
        this.DropOffDateTime=DropOffDateTime;
        this.DriverMobileNo=DriverMobileNo;

        this.CarDetails_Id=CarDetails_Id;
        this.CarDetails_CompanyId=CarDetails_CompanyId;
        this.CarDetails_DriverId=CarDetails_DriverId;
        this.CarDetails_VehicleModel=CarDetails_VehicleModel;
        this.CarDetails_Company=CarDetails_Company;
        this.CarDetails_Color=CarDetails_Color;
        this.CarDetails_VehicleRegistrationNo=CarDetails_VehicleRegistrationNo;
        this.CarDetails_RegistrationCertificate=CarDetails_RegistrationCertificate;
        this.CarDetails_VehicleInsuranceCertificate=CarDetails_VehicleInsuranceCertificate;
        this.CarDetails_RegistrationCertificateExpire=CarDetails_RegistrationCertificateExpire;
        this.CarDetails_VehicleInsuranceCertificateExpire=CarDetails_VehicleInsuranceCertificateExpire;
        this.CarDetails_VehicleImage=CarDetails_VehicleImage;
        this.CarDetails_Description=CarDetails_Description;
        this.Notes=Notes;
        this.BabySeat=BabySeat;
        this.CardId=CardId;
        this.PickupLat=PickupLat;
        this.PickupLng=PickupLng;
        this.DropOffLat=DropOffLat;
        this.DropOffLon=DropOffLon;
        this.PickupSuburb=PickupSuburb;
        this.DropoffSuburb=DropoffSuburb;
    }

    public String getCarDetails_VehicleRegistrationNo() {
        return CarDetails_VehicleRegistrationNo;
    }

    public void setCarDetails_VehicleRegistrationNo(String carDetails_VehicleRegistrationNo) {
        CarDetails_VehicleRegistrationNo = carDetails_VehicleRegistrationNo;
    }

    public String getCarDetails_VehicleModel() {
        return CarDetails_VehicleModel;
    }

    public void setCarDetails_VehicleModel(String carDetails_VehicleModel) {
        CarDetails_VehicleModel = carDetails_VehicleModel;
    }

    public String getCarDetails_VehicleInsuranceCertificateExpire() {
        return CarDetails_VehicleInsuranceCertificateExpire;
    }

    public void setCarDetails_VehicleInsuranceCertificateExpire(String carDetails_VehicleInsuranceCertificateExpire) {
        CarDetails_VehicleInsuranceCertificateExpire = carDetails_VehicleInsuranceCertificateExpire;
    }

    public String getCarDetails_VehicleInsuranceCertificate() {
        return CarDetails_VehicleInsuranceCertificate;
    }

    public void setCarDetails_VehicleInsuranceCertificate(String carDetails_VehicleInsuranceCertificate) {
        CarDetails_VehicleInsuranceCertificate = carDetails_VehicleInsuranceCertificate;
    }

    public String getCarDetails_VehicleImage() {
        return CarDetails_VehicleImage;
    }

    public void setCarDetails_VehicleImage(String carDetails_VehicleImage) {
        CarDetails_VehicleImage = carDetails_VehicleImage;
    }

    public String getCarDetails_RegistrationCertificateExpire() {
        return CarDetails_RegistrationCertificateExpire;
    }

    public void setCarDetails_RegistrationCertificateExpire(String carDetails_RegistrationCertificateExpire) {
        CarDetails_RegistrationCertificateExpire = carDetails_RegistrationCertificateExpire;
    }

    public String getCarDetails_RegistrationCertificate() {
        return CarDetails_RegistrationCertificate;
    }

    public void setCarDetails_RegistrationCertificate(String carDetails_RegistrationCertificate) {
        CarDetails_RegistrationCertificate = carDetails_RegistrationCertificate;
    }

    public String getCarDetails_Id() {
        return CarDetails_Id;
    }

    public void setCarDetails_Id(String carDetails_Id) {
        CarDetails_Id = carDetails_Id;
    }

    public String getCarDetails_DriverId() {
        return CarDetails_DriverId;
    }

    public void setCarDetails_DriverId(String carDetails_DriverId) {
        CarDetails_DriverId = carDetails_DriverId;
    }

    public String getCarDetails_Description() {
        return CarDetails_Description;
    }

    public void setCarDetails_Description(String carDetails_Description) {
        CarDetails_Description = carDetails_Description;
    }

    public String getCarDetails_CompanyId() {
        return CarDetails_CompanyId;
    }

    public void setCarDetails_CompanyId(String carDetails_CompanyId) {
        CarDetails_CompanyId = carDetails_CompanyId;
    }

    public String getCarDetails_Company() {
        return CarDetails_Company;
    }

    public void setCarDetails_Company(String carDetails_Company) {
        CarDetails_Company = carDetails_Company;
    }

    public String getCarDetails_Color() {
        return CarDetails_Color;
    }

    public void setCarDetails_Color(String carDetails_Color) {
        CarDetails_Color = carDetails_Color;
    }

    //car detail end

    public String getDriverMobileNo() {
        return DriverMobileNo;
    }

    public void setDriverMobileNo(String driverMobileNo) {
        DriverMobileNo = driverMobileNo;
    }

    public String getDropOffDateTime() {
        return DropOffDateTime;
    }

    public void setDropOffDateTime(String dropOffDateTime) {
        DropOffDateTime = dropOffDateTime;
    }

    public String getWaitingTime() {
        return WaitingTime;
    }

    public void setWaitingTime(String waitingTime) {
        WaitingTime = waitingTime;
    }

    public String getTripFare() {
        return TripFare;
    }

    public void setTripFare(String tripFare) {
        TripFare = tripFare;
    }

    public String getTripDuration() {
        return TripDuration;
    }

    public void setTripDuration(String tripDuration) {
        TripDuration = tripDuration;
    }

    public String getTripDistance() {
        return TripDistance;
    }

    public void setTripDistance(String tripDistance) {
        TripDistance = tripDistance;
    }

    public String getTransactionId() {
        return TransactionId;
    }

    public void setTransactionId(String transactionId) {
        TransactionId = transactionId;
    }

    public String getTollFee() {
        return TollFee;
    }

    public void setTollFee(String tollFee) {
        TollFee = tollFee;
    }

    public String getPickupTime() {
        return PickupTime;
    }

    public void setPickupTime(String pickupTime) {
        PickupTime = pickupTime;
    }

    public String getTax() {
        return Tax;
    }

    public void setTax(String tax) {
        Tax = tax;
    }

    public String getPaymentStatus() {
        return PaymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        PaymentStatus = paymentStatus;
    }

    public String getSubTotal() {
        return SubTotal;
    }

    public void setSubTotal(String subTotal) {
        SubTotal = subTotal;
    }

    public String getPromoCode() {
        return PromoCode;
    }

    public void setPromoCode(String promoCode) {
        PromoCode = promoCode;
    }

    public String getNightFareApplicable() {
        return NightFareApplicable;
    }

    public void setNightFareApplicable(String nightFareApplicable) {
        NightFareApplicable = nightFareApplicable;
    }

    public String getReason() {
        return Reason;
    }

    public void setReason(String reason) {
        Reason = reason;
    }

    public String getNightFare() {
        return NightFare;
    }

    public void setNightFare(String nightFare) {
        NightFare = nightFare;
    }

    public String getPaymentType() {
        return PaymentType;
    }

    public void setPaymentType(String paymentType) {
        PaymentType = paymentType;
    }

    public String getGrandTotal() {
        return GrandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        GrandTotal = grandTotal;
    }

    public String getDropTime() {
        return DropTime;
    }

    public void setDropTime(String dropTime) {
        DropTime = dropTime;
    }

    public String getDriverId() {
        return DriverId;
    }

    public void setDriverId(String driverId) {
        DriverId = driverId;
    }

    public String getBookingCharge() {
        return BookingCharge;
    }

    public void setBookingCharge(String bookingCharge) {
        BookingCharge = bookingCharge;
    }

    public String getWaitingTimeCost() {
        return WaitingTimeCost;
    }

    public void setWaitingTimeCost(String waitingTimeCost) {
        WaitingTimeCost = waitingTimeCost;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getAdminAmount() {
        return AdminAmount;
    }

    public void setAdminAmount(String adminAmount) {
        AdminAmount = adminAmount;
    }

    public String getCompanyAmount() {
        return CompanyAmount;
    }

    public void setCompanyAmount(String companyAmount) {
        CompanyAmount = companyAmount;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public String getDropoffLocation() {
        return DropoffLocation;
    }

    public void setDropoffLocation(String dropoffLocation) {
        DropoffLocation = dropoffLocation;
    }

    public String getModelId() {
        return ModelId;
    }

    public void setModelId(String modelId) {
        ModelId = modelId;
    }

    public String getPassengerId() {
        return PassengerId;
    }

    public void setPassengerId(String passengerId) {
        PassengerId = passengerId;
    }

    public String getPickupLocation() {
        return PickupLocation;
    }

    public void setPickupLocation(String pickupLocation) {
        PickupLocation = pickupLocation;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getCompanyId() {
        return CompanyId;
    }

    public void setCompanyId(String companyId) {
        CompanyId = companyId;
    }

    public String getCompanyTax() {
        return CompanyTax;
    }

    public void setCompanyTax(String companyTax) {
        CompanyTax = companyTax;
    }


    public String getDriverName() {
        return DriverName;
    }

    public void setDriverName(String driverName) {
        DriverName = driverName;
    }

    public String getFlightNumber() {
        return FlightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        FlightNumber = flightNumber;
    }

    public String getHistoryType() {
        return HistoryType;
    }

    public void setHistoryType(String historyType) {
        HistoryType = historyType;
    }

    public String getModel() {
        return Model;
    }

    public void setModel(String model) {
        Model = model;
    }

    public String getPassengerContact() {
        return PassengerContact;
    }

    public void setPassengerContact(String passengerContact) {
        PassengerContact = passengerContact;
    }

    public String getPassengerName() {
        return PassengerName;
    }

    public void setPassengerName(String passengerName) {
        PassengerName = passengerName;
    }

    public String getPassengerType() {
        return PassengerType;
    }

    public void setPassengerType(String passengerType) {
        PassengerType = passengerType;
    }

    public String getPickupDateTime() {
        return PickupDateTime;
    }

    public void setPickupDateTime(String pickupDateTime) {
        PickupDateTime = pickupDateTime;
    }

    public String getBookingType() {
        return BookingType;
    }

    public void setBookingType(String bookingType) {
        BookingType = bookingType;
    }

    public String getNotes() {
        return Notes;
    }

    public void setNotes(String notes) {
        Notes = notes;
    }

    public String getBabySeat() {
        return BabySeat;
    }

    public void setBabySeat(String babySeat) {
        BabySeat = babySeat;
    }

    public String getCardId() {
        return CardId;
    }

    public void setCardId(String cardId) {
        CardId = cardId;
    }

    public String getPickupLat() {
        return PickupLat;
    }

    public void setPickupLat(String pickupLat) {
        PickupLat = pickupLat;
    }

    public String getPickupLng() {
        return PickupLng;
    }

    public void setPickupLng(String pickupLng) {
        PickupLng = pickupLng;
    }

    public String getDropOffLat() {
        return DropOffLat;
    }

    public void setDropOffLat(String dropOffLat) {
        DropOffLat = dropOffLat;
    }

    public String getDropOffLon() {
        return DropOffLon;
    }

    public void setDropOffLon(String dropOffLon) {
        DropOffLon = dropOffLon;
    }

    public String getPickupSuburb() {
        return PickupSuburb;
    }

    public void setPickupSuburb(String pickupSuburb) {
        PickupSuburb = pickupSuburb;
    }

    public String getDropoffSuburb() {
        return DropoffSuburb;
    }

    public void setDropoffSuburb(String dropoffSuburb) {
        DropoffSuburb = dropoffSuburb;
    }
}

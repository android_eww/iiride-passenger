package iiride.app.rider.been;

/**
 * Created by ADMIN on 10/21/2016.
 */
public class BookingDriverLocation_Been {

    private double latitude,longitude;

    public BookingDriverLocation_Been(
            double latitude,
            double longitude
    )
    {
        this.latitude=latitude;
        this.longitude=longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}

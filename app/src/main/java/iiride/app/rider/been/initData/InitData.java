package iiride.app.rider.been.initData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class InitData {

@SerializedName("status")
@Expose
private Boolean status;
@SerializedName("car_class")
@Expose
private List<CarClas> carClass = null;
@SerializedName("city_list")
@Expose
private List<CityList> cityList = null;

public Boolean getStatus() {
return status;
}

public void setStatus(Boolean status) {
this.status = status;
}

public List<CarClas> getCarClass() {
return carClass;
}

public void setCarClass(List<CarClas> carClass) {
this.carClass = carClass;
}

public List<CityList> getCityList() {
return cityList;
}

public void setCityList(List<CityList> cityList) {
this.cityList = cityList;
}

}

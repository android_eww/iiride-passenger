package iiride.app.rider.been;


public class CreditCard_List_Been {
    private String Id,CardNum,CardNum_,cardType,cardName;


    public CreditCard_List_Been(String Id, String CardNum, String CardNum_, String cardType, String cardName) {
        this.Id = Id;
        this.CardNum = CardNum;
        this.CardNum_ = CardNum_;
        this.cardType = cardType;
        this.cardName = cardName;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getCardNum() {
        return CardNum;
    }

    public void setCardNum(String cardNum) {
        CardNum = cardNum;
    }

    public String getCardNum_() {
        return CardNum_;
    }

    public void setCardNum_(String cardNum_) {
        CardNum_ = cardNum_;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getCardName() {
        return cardName;
    }

    public void setCardName(String cardName) {
        this.cardName = cardName;
    }
}

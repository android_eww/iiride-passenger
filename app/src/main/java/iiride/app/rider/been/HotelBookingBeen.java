package iiride.app.rider.been;


public class HotelBookingBeen {

    private String place_id,name,address,contact,distance,image,ratting;
    private double latitude,longitude, source_latitude, source_longitude;

    public HotelBookingBeen(String place_id,
                            String name,
                            String address,
                            String contact,
                            String distance,
                            String image,
                            String ratting,
                            double latitude,
                            double longitude ,
                            double source_latitude,
                            double source_longitude) {
        this.place_id=place_id;
        this.name = name;
        this.address = address;
        this.contact = contact;
        this.distance = distance;
        this.image = image;
        this.ratting = ratting;
        this.latitude=latitude;
        this.longitude=longitude;
        this.source_latitude=longitude;
        this.source_longitude=source_longitude;
    }

    public String getPlace_id() {
        return place_id;
    }

    public void setPlace_id(String place_id) {
        this.place_id = place_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }


    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getRatting() {
        return ratting;
    }

    public void setRatting(String ratting) {
        this.ratting = ratting;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setSource_latitude(double source_latitude) {
        this.source_latitude = source_latitude;
    }
    public double getSource_latitude() {
        return source_latitude;
    }

    public void setSource_longitude(double source_longitude) {
        this.source_longitude = source_longitude;
    }
    public double getSource_longitude() {
        return source_longitude;
    }
}

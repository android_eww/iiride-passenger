package iiride.app.rider.been;


public class Address_Been {

    private String Id,PassengerId,Type,Address;
    private double Lat,Lng;

    public Address_Been(String Id, String PassengerId, String Type, String Address,
                        double Lat,
                        double Lng) {
        this.Id=Id;
        this.PassengerId=PassengerId;
        this.Type=Type;
        this.Address=Address;
        this.Lat=Lat;
        this.Lng=Lng;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getPassengerId() {
        return PassengerId;
    }

    public void setPassengerId(String passengerId) {
        PassengerId = passengerId;
    }

    public double getLat() {
        return Lat;
    }

    public void setLat(double lat) {
        Lat = lat;
    }

    public double getLng() {
        return Lng;
    }

    public void setLng(double lng) {
        Lng = lng;
    }
}

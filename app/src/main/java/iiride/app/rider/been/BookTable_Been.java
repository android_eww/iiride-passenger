package iiride.app.rider.been;

/**
 * Created by ADMIN on 10/21/2016.
 */
public class BookTable_Been {

    private int id;
    private String title,count;

    public BookTable_Been(int id, String title, String count)
    {
        this.id=id;
        this.title=title;
        this.count=count;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }
}

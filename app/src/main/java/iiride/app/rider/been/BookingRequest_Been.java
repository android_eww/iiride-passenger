package iiride.app.rider.been;

/**
 * Created by ADMIN on 10/21/2016.
 */
public class BookingRequest_Been {

    private String PassengerId,ModelId,PickupLocation,DropoffLocation;
    private double PickupLat,PickupLng,DropOffLat,DropOffLon;
    private String CreatedDate,Status,BookingId;

    public BookingRequest_Been(
            String PassengerId,
            String ModelId,
            String PickupLocation,
            String DropoffLocation,
            double PickupLat,
            double PickupLng,
            double DropOffLat,
            double DropOffLon,
            String CreatedDate,
            String Status,
            String BookingId
    )
    {
        this.PassengerId=PassengerId;
        this.ModelId=ModelId;
        this.PickupLocation=PickupLocation;
        this.DropoffLocation=DropoffLocation;
        this.PickupLat=PickupLat;
        this.PickupLng=PickupLng;
        this.DropOffLat=DropOffLat;
        this.DropOffLon=DropOffLon;
        this.CreatedDate=CreatedDate;
        this.Status=Status;
        this.BookingId=BookingId;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public double getDropOffLat() {
        return DropOffLat;
    }

    public void setDropOffLat(double dropOffLat) {
        DropOffLat = dropOffLat;
    }

    public double getDropOffLon() {
        return DropOffLon;
    }

    public void setDropOffLon(double dropOffLon) {
        DropOffLon = dropOffLon;
    }

    public double getPickupLat() {
        return PickupLat;
    }

    public void setPickupLat(double pickupLat) {
        PickupLat = pickupLat;
    }

    public double getPickupLng() {
        return PickupLng;
    }

    public void setPickupLng(double pickupLng) {
        PickupLng = pickupLng;
    }

    public String getBookingId() {
        return BookingId;
    }

    public void setBookingId(String bookingId) {
        BookingId = bookingId;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public String getDropoffLocation() {
        return DropoffLocation;
    }

    public void setDropoffLocation(String dropoffLocation) {
        DropoffLocation = dropoffLocation;
    }

    public String getModelId() {
        return ModelId;
    }

    public void setModelId(String modelId) {
        ModelId = modelId;
    }

    public String getPassengerId() {
        return PassengerId;
    }

    public void setPassengerId(String passengerId) {
        PassengerId = passengerId;
    }

    public String getPickupLocation() {
        return PickupLocation;
    }

    public void setPickupLocation(String pickupLocation) {
        PickupLocation = pickupLocation;
    }
}

package iiride.app.rider.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.hendraanggrian.widget.ExpandableItem;
import com.hendraanggrian.widget.ExpandableRecyclerView;

import iiride.app.rider.R;
import iiride.app.rider.activity.MyReceiptsActivity;
import iiride.app.rider.other.DialogClass;
import iiride.app.rider.other.Global;
import iiride.app.rider.other.InternetDialog;


public class MyReceipt_Adapter extends ExpandableRecyclerView.Adapter<MyReceipt_Adapter.ViewHolder> {
    Context context;
    String TAG = "AcceptDispatchJob_Req";
    private AQuery aQuery;
    private DialogClass dialogClass;


    public MyReceipt_Adapter(LinearLayoutManager lm) {
        super(lm);
    }


    public class ViewHolder extends ExpandableRecyclerView.ViewHolder {
        public View layout;

        //header
        private LinearLayout ll_dropOffLoc,ll_pickupLoc;
        public TextView tv_PassengerName,tv_drop_off_location,tv_date_time_Created,tv_pickup_location,tv_GetReceipt;

        //content
        private LinearLayout ll_row_item_pastJob, ll_VehicleType, ll_DistanceTravelled
                , ll_TollFee, ll_DiscountApplied, ll_ChargedCard, ll_FareTotal;
        public TextView tv_VehicleType,  tv_DistanceTravelled
                , tv_TollFee, tv_DiscountApplied, tv_ChargedCard,tv_FareTotal;
        private ExpandableItem expandableItem;


        public ViewHolder(View v) {
            super(v);
            layout = v;
            expandableItem = (ExpandableItem) v. findViewById(R.id.row);

            tv_drop_off_location = (TextView) expandableItem.findViewById(R.id.tv_drop_off_location);
            tv_pickup_location= (TextView) expandableItem.findViewById(R.id.tv_pickup_location);
            tv_date_time_Created = (TextView) expandableItem.findViewById(R.id.tv_date_time_Created);
            tv_PassengerName = (TextView) expandableItem.findViewById(R.id.tv_PassengerName);
            tv_GetReceipt = (TextView) expandableItem.findViewById(R.id.tv_GetReceipt);

            ll_dropOffLoc = (LinearLayout) expandableItem.findViewById(R.id.ll_dropOffLoc);
            ll_pickupLoc= (LinearLayout) expandableItem.findViewById(R.id.ll_pickupLoc);

            tv_VehicleType = (TextView) expandableItem.findViewById(R.id.tv_VehicleType);
            tv_DistanceTravelled = (TextView) expandableItem.findViewById(R.id.tv_DistanceTravelled);
            tv_TollFee = (TextView) expandableItem.findViewById(R.id.tv_TollFee);
            tv_DiscountApplied = (TextView) expandableItem.findViewById(R.id.tv_DiscountApplied);
            tv_ChargedCard = (TextView) expandableItem.findViewById(R.id.tv_ChargedCard);
            tv_FareTotal = (TextView) expandableItem.findViewById(R.id.tv_FareTotal);


            ll_row_item_pastJob = (LinearLayout) expandableItem.findViewById(R.id.ll_row_item_pastJob);

            ll_VehicleType = (LinearLayout) expandableItem.findViewById(R.id.ll_VehicleType);
            ll_DistanceTravelled = (LinearLayout) expandableItem.findViewById(R.id.ll_DistanceTravelled);
            ll_TollFee = (LinearLayout) expandableItem.findViewById(R.id.ll_TollFee);
            ll_DiscountApplied = (LinearLayout) expandableItem.findViewById(R.id.ll_DiscountApplied);
            ll_ChargedCard = (LinearLayout) expandableItem.findViewById(R.id.ll_ChargedCard);
            ll_FareTotal = (LinearLayout) expandableItem.findViewById(R.id.ll_FareTotal);
        }
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        this.context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.row_my_receipt, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position)
    {
        super.onBindViewHolder(holder, position);

        if (MyReceiptsActivity.list.get(position).getDropoffLocation()!=null && !MyReceiptsActivity.list.get(position).getDropoffLocation().equalsIgnoreCase(""))
        {
            holder.tv_drop_off_location.setText(MyReceiptsActivity.list.get(position).getDropoffLocation());
        }
        else
        {
            holder.ll_dropOffLoc.setVisibility(View.GONE);
        }

        if (MyReceiptsActivity.list.get(position).getDriverName()!=null && !MyReceiptsActivity.list.get(position).getDriverName().equalsIgnoreCase(""))
        {
            holder.tv_PassengerName.setText(MyReceiptsActivity.list.get(position).getDriverName());
        }
        else
        {
            holder.tv_PassengerName.setVisibility(View.INVISIBLE);
        }

        if (MyReceiptsActivity.list.get(position).getPickupLocation()!=null && !MyReceiptsActivity.list.get(position).getPickupLocation().equalsIgnoreCase(""))
        {
            holder.tv_pickup_location.setText(MyReceiptsActivity.list.get(position).getPickupLocation());
        }
        else
        {
            holder.ll_pickupLoc.setVisibility(View.GONE);
        }

        if (MyReceiptsActivity.list.get(position).getCreatedDate()!=null && !MyReceiptsActivity.list.get(position).getCreatedDate().equalsIgnoreCase(""))
        {
            holder.tv_date_time_Created.setText(MyReceiptsActivity.list.get(position).getCreatedDate());
        }
        else
        {
            holder.tv_date_time_Created.setVisibility(View.GONE);
        }

        if (MyReceiptsActivity.list.get(position).getTollFee()!=null && !MyReceiptsActivity.list.get(position).getTollFee().equalsIgnoreCase(""))
        {
            holder.ll_TollFee.setVisibility(View.GONE);
            holder.tv_TollFee.setText(MyReceiptsActivity.list.get(position).getTollFee());
        }
        else
        {
            holder.ll_TollFee.setVisibility(View.GONE);
        }

        //pickup
        //dropoff
        //vehicle type
        //distance
        //toll
        //fare total
        //discount
        //charged card

        if (MyReceiptsActivity.list.get(position).getModel()!=null && !MyReceiptsActivity.list.get(position).getModel().equalsIgnoreCase(""))
        {
            holder.tv_VehicleType.setText(MyReceiptsActivity.list.get(position).getModel());
        }
        else
        {
            holder.ll_VehicleType.setVisibility(View.GONE);
        }

        Log.e("call","distance = "+MyReceiptsActivity.list.get(position).getTripDistance());

        if (MyReceiptsActivity.list.get(position).getTripDistance()!=null && !MyReceiptsActivity.list.get(position).getTripDistance().equalsIgnoreCase(""))
        {
            holder.tv_DistanceTravelled.setText(MyReceiptsActivity.list.get(position).getTripDistance());
        }
        else
        {
            holder.ll_DistanceTravelled.setVisibility(View.GONE);
        }


        Log.e("call","tv_FareTotal = "+MyReceiptsActivity.list.get(position).getGrandTotal());

        if (MyReceiptsActivity.list.get(position).getGrandTotal()!=null && !MyReceiptsActivity.list.get(position).getGrandTotal().equalsIgnoreCase(""))
        {
            holder.tv_FareTotal.setText(MyReceiptsActivity.list.get(position).getGrandTotal());
        }
        else
        {
            holder.ll_FareTotal.setVisibility(View.GONE);
        }

        Log.e("call","tv_DiscountApplied = "+MyReceiptsActivity.list.get(position).getDiscount());
        if (MyReceiptsActivity.list.get(position).getDiscount()!=null && !MyReceiptsActivity.list.get(position).getDiscount().equalsIgnoreCase(""))
        {
            holder.tv_DiscountApplied.setText(MyReceiptsActivity.list.get(position).getDiscount());
        }
        else
        {
            holder.ll_DiscountApplied.setVisibility(View.GONE);
        }

        Log.e("call","tv_ChargedCard = "+MyReceiptsActivity.list.get(position).getPaymentType());
        if (MyReceiptsActivity.list.get(position).getPaymentType()!=null && !MyReceiptsActivity.list.get(position).getPaymentType().equalsIgnoreCase(""))
        {
            holder.tv_ChargedCard.setText(MyReceiptsActivity.list.get(position).getPaymentType());
        }
        else
        {
            holder.ll_ChargedCard.setVisibility(View.GONE);
        }

        holder.tv_GetReceipt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("call","tv_GetReceipt clicked");

                if (Global.isNetworkconn(context))
                {
                    Log.e("call","getShreUrl = "+position+MyReceiptsActivity.list.get(position).getShreUrl());
                    if (MyReceiptsActivity.list.get(position).getShreUrl()!=null && !MyReceiptsActivity.list.get(position).getShreUrl().equalsIgnoreCase(""))
                    {
                        Intent shareIntent = new Intent(Intent.ACTION_SEND);
                        shareIntent.setType("text/plain");
                        shareIntent.putExtra(Intent.EXTRA_TEXT, "Please download from below link \n\n "+MyReceiptsActivity.list.get(position).getShreUrl());
                        context.startActivity(Intent.createChooser(shareIntent, "Share Via"));
                    }
                    else
                    {
                        InternetDialog internetDialog = new InternetDialog(context);
                        internetDialog.showDialog("Something went wrong please try again later!");
                    }
                }
                else
                {
                    InternetDialog internetDialog = new InternetDialog(context);
                    internetDialog.showDialog("Please check your internet connection!");
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return MyReceiptsActivity.list.size();
    }

}

package iiride.app.rider.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import iiride.app.rider.R;
import iiride.app.rider.activity.Wallet_Add_Cards_Activity;
import iiride.app.rider.been.PreviousDue_Been;

public class PreviousDueAdapter extends RecyclerView.Adapter<PreviousDueAdapter.ViewHolder> {

    private Context mContext;
    private List<PreviousDue_Been> previousDue_beens;

    public PreviousDueAdapter(Context mContext, List<PreviousDue_Been> previousDue_beens) {
        this.mContext = mContext;
        this.previousDue_beens = previousDue_beens;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_previous_due, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.setIsRecyclable(false);

        final PreviousDue_Been previousDueBeen = previousDue_beens.get(position);

        holder.tv_PreviousDue_BookingId.setText(previousDueBeen.getId());
        holder.tv_PreviousDue_PickupLocation.setText(previousDueBeen.getPickupLocation());
        holder.tv_PreviousDue_DropoffLocation.setText(previousDueBeen.getDropoffLocation());
        holder.tv_PreviousDue_Amount.setText(previousDueBeen.getGrandTotal());

        holder.ll_PreviousDue_PayNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, Wallet_Add_Cards_Activity.class);
                intent.putExtra("from","PreviousDueActivity");
                intent.putExtra("pastDueId",previousDueBeen.getPastDuesId());
                mContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return previousDue_beens.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_PreviousDue_BookingId, tv_PreviousDue_PickupLocation, tv_PreviousDue_DropoffLocation, tv_PreviousDue_Amount;
LinearLayout ll_PreviousDue_PayNow;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_PreviousDue_BookingId = itemView.findViewById(R.id.tv_PreviousDue_BookingId);
            tv_PreviousDue_PickupLocation= itemView.findViewById(R.id.tv_PreviousDue_PickupLocation);
            tv_PreviousDue_DropoffLocation= itemView.findViewById(R.id.tv_PreviousDue_DropoffLocation);
            tv_PreviousDue_Amount= itemView.findViewById(R.id.tv_PreviousDue_Amount);
            ll_PreviousDue_PayNow= itemView.findViewById(R.id.ll_PreviousDue_PayNow);
        }
    }
}

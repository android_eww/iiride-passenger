package iiride.app.rider.adapter;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.net.Uri;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.hendraanggrian.widget.ExpandableItem;
import com.hendraanggrian.widget.ExpandableRecyclerView;

import iiride.app.rider.R;
import iiride.app.rider.activity.BookLaterActivity;
import iiride.app.rider.activity.MyBookingActivity;
import iiride.app.rider.other.DialogClass;
import iiride.app.rider.other.Global;
import iiride.app.rider.other.InternetDialog;

import java.text.SimpleDateFormat;
import java.util.Calendar;


public class OnGoing_Adapter extends ExpandableRecyclerView.Adapter<OnGoing_Adapter.ViewHolder> {
    Context context;
    int POSITION;
    String TAG = "PastBooking_Req";
    private AQuery aQuery;
    private DialogClass dialogClass;


    public OnGoing_Adapter(LinearLayoutManager lm) {
        super(lm);
    }


    public class ViewHolder extends ExpandableRecyclerView.ViewHolder {
        public View layout;

        //header
        private LinearLayout ll_DropoffLocation, ll_Date;
        public TextView tv_DropoffLocation, tv_Date,tv_BookingId,tv_DriverName;

        //content
        private LinearLayout ll_PickupLocation,ll_PickupTime,ll_DropoffTime,ll_VehicleType,ll_DistanceTravelled,ll_TripFare;
        private LinearLayout ll_NightFare,ll_TollFee,ll_WaitingCost,ll_WaitingTime,ll_BookingCharge,ll_Tax,ll_Discount,ll_PaymentType,ll_Total,ll_ContactNumber;

        private TextView tv_PickupLocation,tv_PickupTime,tv_DropoffTime,tv_VehicleType,tv_DistanceTravelled,tv_TripFare,tv_NightFare,tv_TollFee;
        private TextView tv_WaitingCost,tv_WaitingTime,tv_BookingCharge,tv_Tax,tv_Discount,tv_PaymentType,
                tv_Total,tv_ContactNumber,tvEditBooking;

        private ExpandableItem expandableItem;


        public ViewHolder(View v) {
            super(v);
            layout = v;
            expandableItem = (ExpandableItem) v. findViewById(R.id.row);

            //header

            ll_DropoffLocation = (LinearLayout) expandableItem.findViewById(R.id.ll_DropoffLocation);
            ll_Date = (LinearLayout) expandableItem.findViewById(R.id.ll_Date);

            tv_DropoffLocation = (TextView) expandableItem.findViewById(R.id.tv_DropoffLocation);
            tv_Date = (TextView) expandableItem.findViewById(R.id.tv_Date);
            tv_BookingId = (TextView) expandableItem.findViewById(R.id.tv_BookingId);
            tv_DriverName = (TextView) expandableItem.findViewById(R.id.tv_DriverName);

            //content

            ll_PickupLocation = (LinearLayout) expandableItem.findViewById(R.id.ll_PickupLocation);
            ll_PickupTime = (LinearLayout) expandableItem.findViewById(R.id.ll_PickupTime);
            ll_DropoffTime = (LinearLayout) expandableItem.findViewById(R.id.ll_DropoffTime);
            ll_VehicleType = (LinearLayout) expandableItem.findViewById(R.id.ll_VehicleType);
            ll_DistanceTravelled = (LinearLayout) expandableItem.findViewById(R.id.ll_DistanceTravelled);
            ll_TripFare = (LinearLayout) expandableItem.findViewById(R.id.ll_TripFare);
            ll_NightFare = (LinearLayout) expandableItem.findViewById(R.id.ll_NightFare);
            ll_TollFee = (LinearLayout) expandableItem.findViewById(R.id.ll_TollFee);
            ll_WaitingCost = (LinearLayout) expandableItem.findViewById(R.id.ll_WaitingCost);
            ll_WaitingTime = (LinearLayout) expandableItem.findViewById(R.id.ll_WaitingTime);
            ll_BookingCharge = (LinearLayout) expandableItem.findViewById(R.id.ll_BookingCharge);
            ll_Tax = (LinearLayout) expandableItem.findViewById(R.id.ll_Tax);
            ll_Discount = (LinearLayout) expandableItem.findViewById(R.id.ll_Discount);
            ll_PaymentType = (LinearLayout) expandableItem.findViewById(R.id.ll_PaymentType);
            ll_Total = (LinearLayout) expandableItem.findViewById(R.id.ll_Total);
            ll_ContactNumber = (LinearLayout) expandableItem.findViewById(R.id.ll_ContactNumber);

            tv_PickupLocation = (TextView) expandableItem.findViewById(R.id.tv_PickupLocation);
            tv_PickupTime = (TextView) expandableItem.findViewById(R.id.tv_PickupTime);
            tv_DropoffTime = (TextView) expandableItem.findViewById(R.id.tv_DropoffTime);
            tv_VehicleType = (TextView) expandableItem.findViewById(R.id.tv_VehicleType);
            tv_DistanceTravelled = (TextView) expandableItem.findViewById(R.id.tv_DistanceTravelled);
            tv_TripFare = (TextView) expandableItem.findViewById(R.id.tv_TripFare);
            tv_NightFare = (TextView) expandableItem.findViewById(R.id.tv_NightFare);
            tv_TollFee = (TextView) expandableItem.findViewById(R.id.tv_TollFee);
            tv_WaitingCost = (TextView) expandableItem.findViewById(R.id.tv_WaitingCost);
            tv_WaitingTime = (TextView) expandableItem.findViewById(R.id.tv_WaitingTime);
            tv_BookingCharge = (TextView) expandableItem.findViewById(R.id.tv_BookingCharge);
            tv_Tax = (TextView) expandableItem.findViewById(R.id.tv_Tax);
            tv_Discount = (TextView) expandableItem.findViewById(R.id.tv_Discount);
            tv_PaymentType = (TextView) expandableItem.findViewById(R.id.tv_PaymentType);
            tv_Total = (TextView) expandableItem.findViewById(R.id.tv_Total);
            tv_ContactNumber = (TextView) expandableItem.findViewById(R.id.tv_ContactNumber);
            tvEditBooking = (TextView) expandableItem.findViewById(R.id.tvEditBooking);

        }
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        this.context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.row_on_going, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position)
    {
        super.onBindViewHolder(holder, position);
        POSITION = position;

        /*holder.iv_status_active.setVisibility(View.GONE);
        holder.iv_status_inactive.setVisibility(View.GONE);*/

        if (MyBookingActivity.onGoing_beens.get(position).getDropoffLocation()!=null && !MyBookingActivity.onGoing_beens.get(position).getDropoffLocation().equalsIgnoreCase(""))
        {
            holder.tv_DropoffLocation.setText(MyBookingActivity.onGoing_beens.get(position).getDropoffLocation());
        }
        else
        {
            holder.ll_DropoffLocation.setVisibility(View.GONE);
        }

        if (MyBookingActivity.onGoing_beens.get(position).getDriverName()!=null && !MyBookingActivity.onGoing_beens.get(position).getDriverName().equalsIgnoreCase(""))
        {
            holder.tv_DriverName.setText(MyBookingActivity.onGoing_beens.get(position).getDriverName());
        }
        else
        {
            holder.tv_DriverName.setVisibility(View.GONE);
        }

        if (MyBookingActivity.onGoing_beens.get(position).getId()!=null && !MyBookingActivity.onGoing_beens.get(position).getId().equalsIgnoreCase(""))
        {
            holder.tv_BookingId.setText("Booking Id :("+MyBookingActivity.onGoing_beens.get(position).getId()+")");
        }
        else
        {
            holder.tv_BookingId.setVisibility(View.GONE);
        }

        if (MyBookingActivity.onGoing_beens.get(position).getPickupDateTime()!=null && !MyBookingActivity.onGoing_beens.get(position).getPickupDateTime().equalsIgnoreCase(""))
        {
            holder.tv_Date.setText(Global.getDateInFormat(MyBookingActivity.onGoing_beens.get(position).getPickupDateTime()));
        }
        else
        {
            holder.ll_Date.setVisibility(View.GONE);
        }

        if (MyBookingActivity.onGoing_beens.get(position).getPickupLocation()!=null && !MyBookingActivity.onGoing_beens.get(position).getPickupLocation().equalsIgnoreCase(""))
        {
            holder.tv_PickupLocation.setText(MyBookingActivity.onGoing_beens.get(position).getPickupLocation());
        }
        else
        {
            holder.ll_PickupLocation.setVisibility(View.GONE);
        }

        if (MyBookingActivity.onGoing_beens.get(position).getDriverMobileNo()!=null && !MyBookingActivity.onGoing_beens.get(position).getDriverMobileNo().equalsIgnoreCase("null")
                && !MyBookingActivity.onGoing_beens.get(position).getDriverMobileNo().equalsIgnoreCase("NULL") &&
                !MyBookingActivity.onGoing_beens.get(position).getDriverMobileNo().equalsIgnoreCase(""))
        {
            //holder.ll_ContactNumber.setVisibility(View.VISIBLE);
            holder.tv_ContactNumber.setText(MyBookingActivity.onGoing_beens.get(position).getDriverMobileNo());
            holder.tv_ContactNumber.setPaintFlags(holder.tv_ContactNumber.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
        }
        else
        {
            holder.ll_ContactNumber.setVisibility(View.GONE);
        }

//        if (MyBookingActivity.onGoing_beens.get(position).getPickupTime()!=null && !MyBookingActivity.onGoing_beens.get(position).getPickupTime().equalsIgnoreCase(""))
//        {
//            String dateString = getDate(Long.parseLong(MyBookingActivity.onGoing_beens.get(position).getPickupTime()+"000"), "HH/mm  yyyy/MM/dd");
//            holder.tv_PickupTime.setText(dateString);
//        }
//        else
//        {
//            holder.ll_PickupTime.setVisibility(View.GONE);
//        }

        if (MyBookingActivity.onGoing_beens.get(position).getDropOffDateTime()!=null && !MyBookingActivity.onGoing_beens.get(position).getDropOffDateTime().equalsIgnoreCase(""))
        {
//            String dateString = getDate(Long.parseLong(MyBookingActivity.onGoing_beens.get(position).getDropTime()+"000"), "HH/mm  yyyy/MM/dd");
            holder.tv_DropoffTime.setText(Global.getDateInFormat(MyBookingActivity.onGoing_beens.get(position).getDropOffDateTime()));
        }
        else
        {
            holder.ll_DropoffTime.setVisibility(View.GONE);
        }

        if (MyBookingActivity.onGoing_beens.get(position).getCarDetails_Company()!=null && !MyBookingActivity.onGoing_beens.get(position).getCarDetails_Company().equalsIgnoreCase(""))
        {
            holder.ll_VehicleType.setVisibility(View.VISIBLE);
            holder.tv_VehicleType.setText(MyBookingActivity.onGoing_beens.get(position).getCarDetails_Company()+" "+MyBookingActivity.onGoing_beens.get(position).getCarDetails_Color());
        }
        else
        {
            holder.ll_VehicleType.setVisibility(View.GONE);
        }

        if (MyBookingActivity.onGoing_beens.get(position).getTripDistance()!=null && !MyBookingActivity.onGoing_beens.get(position).getTripDistance().equalsIgnoreCase(""))
        {
            holder.tv_DistanceTravelled.setText(MyBookingActivity.onGoing_beens.get(position).getTripDistance());
        }
        else
        {
            holder.ll_DistanceTravelled.setVisibility(View.GONE);
        }

        if (MyBookingActivity.onGoing_beens.get(position).getTripFare()!=null && !MyBookingActivity.onGoing_beens.get(position).getTripFare().equalsIgnoreCase(""))
        {
            holder.tv_TripFare.setText(MyBookingActivity.onGoing_beens.get(position).getTripFare());
        }
        else
        {
            holder.ll_TripFare.setVisibility(View.GONE);
        }

        if (MyBookingActivity.onGoing_beens.get(position).getNightFare()!=null && !MyBookingActivity.onGoing_beens.get(position).getNightFare().equalsIgnoreCase(""))
        {
            holder.tv_NightFare.setText(MyBookingActivity.onGoing_beens.get(position).getNightFare());
        }
        else
        {
            holder.ll_NightFare.setVisibility(View.GONE);
        }

        if (MyBookingActivity.onGoing_beens.get(position).getTollFee()!=null && !MyBookingActivity.onGoing_beens.get(position).getTollFee().equalsIgnoreCase(""))
        {
            holder.ll_TollFee.setVisibility(View.GONE);
            holder.tv_TollFee.setText(MyBookingActivity.onGoing_beens.get(position).getTollFee());
        }
        else
        {
            holder.ll_TollFee.setVisibility(View.GONE);
        }

        if (MyBookingActivity.onGoing_beens.get(position).getWaitingTimeCost()!=null && !MyBookingActivity.onGoing_beens.get(position).getWaitingTimeCost().equalsIgnoreCase(""))
        {
            holder.tv_WaitingCost.setText(MyBookingActivity.onGoing_beens.get(position).getWaitingTimeCost());
        }
        else
        {
            holder.ll_WaitingCost.setVisibility(View.GONE);
        }

        if (MyBookingActivity.onGoing_beens.get(position).getWaitingTime()!=null && !MyBookingActivity.onGoing_beens.get(position).getWaitingTime().equalsIgnoreCase(""))
        {
            holder.tv_WaitingTime.setText(MyBookingActivity.onGoing_beens.get(position).getWaitingTime());
        }
        else
        {
            holder.ll_WaitingTime.setVisibility(View.GONE);
        }

        if (MyBookingActivity.onGoing_beens.get(position).getBookingCharge()!=null && !MyBookingActivity.onGoing_beens.get(position).getBookingCharge().equalsIgnoreCase(""))
        {
            holder.tv_BookingCharge.setText(MyBookingActivity.onGoing_beens.get(position).getBookingCharge());
        }
        else
        {
            holder.ll_BookingCharge.setVisibility(View.GONE);
        }

        if (MyBookingActivity.onGoing_beens.get(position).getTax()!=null && !MyBookingActivity.onGoing_beens.get(position).getTax().equalsIgnoreCase(""))
        {
            holder.tv_Tax.setText(MyBookingActivity.onGoing_beens.get(position).getTax());
        }
        else
        {
            holder.ll_Tax.setVisibility(View.GONE);
        }

        if (MyBookingActivity.onGoing_beens.get(position).getDiscount()!=null && !MyBookingActivity.onGoing_beens.get(position).getDiscount().equalsIgnoreCase(""))
        {
            holder.tv_Discount.setText(MyBookingActivity.onGoing_beens.get(position).getDiscount());
        }
        else
        {
            holder.ll_Discount.setVisibility(View.GONE);
        }

        if (MyBookingActivity.onGoing_beens.get(position).getPaymentType()!=null && !MyBookingActivity.onGoing_beens.get(position).getPaymentType().equalsIgnoreCase(""))
        {
            holder.tv_PaymentType.setText(MyBookingActivity.onGoing_beens.get(position).getPaymentType());
        }
        else
        {
            holder.ll_PaymentType.setVisibility(View.GONE);
        }

        if (MyBookingActivity.onGoing_beens.get(position).getGrandTotal()!=null && !MyBookingActivity.onGoing_beens.get(position).getGrandTotal().equalsIgnoreCase(""))
        {
            holder.tv_Total.setText(MyBookingActivity.onGoing_beens.get(position).getGrandTotal());
        }
        else
        {
            holder.ll_Total.setVisibility(View.GONE);
        }

        holder.tvEditBooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Global.isNetworkconn(context))
                {
                    Intent intent = new Intent(context, BookLaterActivity.class);
                    intent.putExtra("from","ongoing");
                    intent.putExtra("modelId",MyBookingActivity.onGoing_beens.get(position).getModelId());
                    intent.putExtra("editName","");
                    intent.putExtra("editNumber","");
                    intent.putExtra("pickup",MyBookingActivity.onGoing_beens.get(position).getPickupLocation());
                    intent.putExtra("dropoff",MyBookingActivity.onGoing_beens.get(position).getDropoffLocation());
                    intent.putExtra("PassengerType","myself");
                    intent.putExtra("bookingType",MyBookingActivity.onGoing_beens.get(position).getBookingType());
                    intent.putExtra("PromoCode",MyBookingActivity.onGoing_beens.get(position).getPromoCode());
                    intent.putExtra("PickupDateTime",MyBookingActivity.onGoing_beens.get(position).getPickupDateTime());
                    intent.putExtra("CardId",MyBookingActivity.onGoing_beens.get(position).getCardId());
                    intent.putExtra("FlightNumber","");
                    intent.putExtra("Notes",MyBookingActivity.onGoing_beens.get(position).getNotes());
                    intent.putExtra("BabySeat",MyBookingActivity.onGoing_beens.get(position).getBabySeat());
                    intent.putExtra("BookingId",MyBookingActivity.onGoing_beens.get(position).getId());
                    intent.putExtra("PickupLat",MyBookingActivity.onGoing_beens.get(position).getPickupLat());
                    intent.putExtra("PickupLng",MyBookingActivity.onGoing_beens.get(position).getPickupLng());
                    intent.putExtra("DropoffLat",MyBookingActivity.onGoing_beens.get(position).getDropOffLat());
                    intent.putExtra("DropoffLon",MyBookingActivity.onGoing_beens.get(position).getDropOffLon());
                    intent.putExtra("pickUpSubArb",MyBookingActivity.onGoing_beens.get(position).getPickupSuburb());
                    intent.putExtra("dropOffSubAArb",MyBookingActivity.onGoing_beens.get(position).getDropoffSuburb());
                    context.startActivity(intent);
                    ((Activity)context).overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
                }
                else
                {
                    InternetDialog internetDialog = new InternetDialog(context);
                    internetDialog.showDialog("Please check your internet connection!");
                }
            }
        });

        holder.tv_ContactNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (MyBookingActivity.onGoing_beens.get(position).getDriverMobileNo()!=null &&
                        !MyBookingActivity.onGoing_beens.get(position).getDriverMobileNo().equalsIgnoreCase("") &&
                        !MyBookingActivity.onGoing_beens.get(position).getDriverMobileNo().equalsIgnoreCase("null") &&
                        !MyBookingActivity.onGoing_beens.get(position).getDriverMobileNo().equalsIgnoreCase("NULL"))
                {
                    callToDriver(MyBookingActivity.onGoing_beens.get(position).getDriverMobileNo());
                }
                else
                {
                    InternetDialog internetDialog = new InternetDialog(context);
                    internetDialog.showDialog("Number not found!");
                }
            }
        });

    }

    public void callToDriver(String driverPhone)
    {
        TelephonyManager telMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        int simState = telMgr.getSimState();
        InternetDialog internetDialog = null;
        switch (simState) {
            case TelephonyManager.SIM_STATE_ABSENT:
                internetDialog = new InternetDialog(context);
                internetDialog.showDialog("Sim card not available");
                break;
            case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
                internetDialog = new InternetDialog(context);
                internetDialog.showDialog("Sim state network locked");
                break;
            case TelephonyManager.SIM_STATE_PIN_REQUIRED:
                internetDialog = new InternetDialog(context);
                internetDialog.showDialog("Sim state pin required");
                break;
            case TelephonyManager.SIM_STATE_PUK_REQUIRED:
                internetDialog = new InternetDialog(context);
                internetDialog.showDialog("Sim state puk required");
                break;
            case TelephonyManager.SIM_STATE_READY:
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + driverPhone));
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                context.startActivity(intent);
                break;
            case TelephonyManager.SIM_STATE_UNKNOWN:
                internetDialog = new InternetDialog(context);
                internetDialog.showDialog("Sim state unknown");
                break;
        }
    }

    public String getDate(long milliSeconds, String dateFormat)
    {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    @Override
    public int getItemCount() {
        return MyBookingActivity.onGoing_beens.size();
    }

}

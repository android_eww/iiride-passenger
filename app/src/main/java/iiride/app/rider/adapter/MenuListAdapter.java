package iiride.app.rider.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;

import androidx.drawerlayout.widget.DrawerLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import iiride.app.rider.R;
import iiride.app.rider.activity.Add_Card_In_List_Activity;
import iiride.app.rider.activity.BarsAndClubActivity;
import iiride.app.rider.activity.BookTableActivity;
import iiride.app.rider.activity.Create_Passcode_Activity;
import iiride.app.rider.activity.FavoriteActivity;
import iiride.app.rider.activity.HotelReservationActivity;
import iiride.app.rider.activity.InviteFriendsActivity;
import iiride.app.rider.activity.LoginActivity;
import iiride.app.rider.activity.MainActivity;
import iiride.app.rider.activity.MyBookingActivity;
import iiride.app.rider.activity.MyReceiptsActivity;
import iiride.app.rider.activity.PreviousDueActivity;
import iiride.app.rider.activity.SettingActivity;
import iiride.app.rider.activity.ShoppingGridActivity;
import iiride.app.rider.activity.TickPayActivity;
import iiride.app.rider.activity.TickPayRegisterActivity;
import iiride.app.rider.activity.TickPayReview;
import iiride.app.rider.activity.Tickpay_Splash_Activity;
import iiride.app.rider.activity.Wallet_Add_Cards_Activity;
import iiride.app.rider.activity.Wallet__Activity;
import iiride.app.rider.been.CreditCard_List_Been;
import iiride.app.rider.been.MenuList_Been;
import iiride.app.rider.comman.Common;
import iiride.app.rider.comman.SessionSave;
import iiride.app.rider.comman.WebServiceAPI;
import iiride.app.rider.other.DialogClass;
import iiride.app.rider.other.Global;
import iiride.app.rider.other.InternetDialog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 4/27/2016.
 */
public class MenuListAdapter extends BaseAdapter {

    private Context context;
    private List<MenuList_Been> list;
    private DrawerLayout drawerLayout;
    private LinearLayout ll_MainDrawerLayout;
    private Handler handler;
    private Runnable runnable;
    private DialogClass dialogClass;
    private AQuery aQuery;

    public MenuListAdapter(Context context, List<MenuList_Been> list, DrawerLayout drawerLayout, LinearLayout ll_MainDrawerLayout) {

        this.context = context;
        this.list = list;
        this.drawerLayout = drawerLayout;
        this.ll_MainDrawerLayout = ll_MainDrawerLayout;
        dialogClass = new DialogClass(context, 0);
        aQuery = new AQuery(context);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public MenuList_Been getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.row_menu_item, null);
            holder = new ViewHolder();

            holder.tv_Title = (TextView) convertView.findViewById(R.id.row_menu_title);
            holder.tv_DescriptionOne = (TextView) convertView.findViewById(R.id.row_menu_description_one);
            holder.tv_DescriptionTwo = (TextView) convertView.findViewById(R.id.row_menu_description_two);
            holder.iv_Icon = (ImageView) convertView.findViewById(R.id.row_menu_icon);
            holder.iv_PayImage = (ImageView) convertView.findViewById(R.id.pay_image);
            holder.ll_Row = (LinearLayout) convertView.findViewById(R.id.row_menu_main_row);
            holder.row = (LinearLayout) convertView.findViewById(R.id.row);

            convertView.setTag(holder);

        } else {

            holder = (ViewHolder) convertView.getTag();
        }

        if (position == (list.size() - 1)) {
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(0, 40, 0, 0);
            holder.row.setLayoutParams(layoutParams);
        } else {
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(0, 0, 0, 0);
            holder.row.setLayoutParams(layoutParams);
        }

        holder.tv_DescriptionOne.setText(list.get(position).getDescriptionOne());
        holder.tv_DescriptionTwo.setText(list.get(position).getDescritpionTwo());

        if (!list.get(position).getDescriptionOne().equals("") && !list.get(position).getDescritpionTwo().equals("")) {
            holder.tv_DescriptionOne.setVisibility(View.VISIBLE);
            holder.tv_DescriptionTwo.setVisibility(View.VISIBLE);
        } else {
            holder.tv_DescriptionOne.setVisibility(View.GONE);
            holder.tv_DescriptionTwo.setVisibility(View.GONE);
        }

        holder.tv_Title.setText(list.get(position).getTitle());
        holder.iv_Icon.setImageResource(list.get(position).getIcon());


        holder.ll_Row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                drawerLayout.closeDrawer(ll_MainDrawerLayout);
                displayView(position);
            }
        });

        return convertView;
    }

    static class ViewHolder {
        private ImageView iv_Icon, iv_PayImage;
        private TextView tv_Title, tv_DescriptionOne, tv_DescriptionTwo;
        private LinearLayout ll_Row, row;
    }

    public void displayView(int position) {
        if (position == 0) {
            call_MyBooking();
        } else if (position == 1) {
            call_PreviousDue();
        } else if (position == 2) {
            call_PaymentOption();
        }
//        else if (position==2)
//        {
//            call_Wallet();
//        }
//        else if (position==3)
//        {
//            call_Pay();
//        }
        else if (position == 3) {
            call_Favorite();
        } else if (position == 4) {
            call_MyReceipts();
        } else if (position == 5) {
            call_InviteFriends();
        } else if (position == 6) {
            call_BarsAndClubs();
        } else if (position == 7) {
            call_HotelReservation();
        } else if (position == 8) {
            call_BookAtable();
        } else if (position == 9) {
            call_Shopping();
        } else if (position == 10) {
            call_Setting();
        } else if (position == 11) {
            call_BecomeADriver();
        } else if (position == 12) {
            call_Logout();
        }
    }

    public void call_MyBooking() {
        handler = new Handler();
        runnable = new Runnable() {

            @Override
            public void run() {
                try {

                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    Intent intent = new Intent(context, MyBookingActivity.class);
                    context.startActivity(intent);
                    ((Activity) context).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                }
            }
        };
        handler.postDelayed(runnable, 300);
    }

    public void call_PreviousDue() {
        handler = new Handler();
        runnable = new Runnable() {

            @Override
            public void run() {
                try {

                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    Intent intent = new Intent(context, PreviousDueActivity.class);
                    context.startActivity(intent);
                    ((Activity) context).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                }
            }
        };
        handler.postDelayed(runnable, 300);
    }

    public void call_Logout() {
        handler = new Handler();
        runnable = new Runnable() {

            @Override
            public void run() {
                try {
                    ((MainActivity) context).disconnectSocket();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    String Fullname = "", Email = "", Password = "", MobileNo = "", Gender = "", Image = "", DeviceType = "", Token = "";
                    String Lat = "", Lng = "", Status = "", CreatedDate = "", Id = "", Address = "";

                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_ID, Id, context);
                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_FULL_NAME, Fullname, context);
                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_EMAIL, Email, context);
                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_PASSWORD, Password, context);
                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_PHONE_NUMBER, MobileNo, context);
                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_GENDER, Gender, context);
                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_IMAGE, Image, context);
                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_DEVICE_TYPE, DeviceType, context);
                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TOKEN, Token, context);
                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_LATITUDE, Lat, context);
                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_LONGITUDE, Lng, context);
                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_STATUS, Status, context);
                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_CREATED_DATE, CreatedDate, context);
                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_ADDRESS, Address, context);
                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_LICENCE_IMAGE, "", context);
                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BANK_ACCOUNT_NUMBER, "", context);
                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BANK_NAME, "", context);
                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_ABN, "", context);
                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BSB, "", context);
                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_COMPANY_NAME, "", context);

                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_CAR_CLASS, "".toString(), context);

                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, "".toString(), context);
                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "0".toString(), context);
                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, "-1".toString(), context);
                    SessionSave.saveUserSession(Common.CREATED_PASSCODE, "", context);
                    SessionSave.saveUserSession(Common.IS_PASSCODE_REQUIRED, "0", context);

                    Intent intent = new Intent(context, LoginActivity.class);
                    context.startActivity(intent);
                    ((Activity) context).finish();
                    ((Activity) context).overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
                }
            }
        };
        handler.postDelayed(runnable, 300);


    }

    public void call_Favorite() {
        handler = new Handler();
        runnable = new Runnable() {

            @Override
            public void run() {
                try {

                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    Intent intent = new Intent(context, FavoriteActivity.class);
                    context.startActivity(intent);
                    ((Activity) context).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                }
            }
        };
        handler.postDelayed(runnable, 300);
    }

    public void call_Wallet() {
        handler = new Handler();
        runnable = new Runnable() {

            @Override
            public void run() {
                try {

                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (SessionSave.getUserSession(Common.IS_PASSCODE_REQUIRED, context) != null && SessionSave.getUserSession(Common.IS_PASSCODE_REQUIRED, context).equalsIgnoreCase("1")) {
                        Intent intent = new Intent(context, Create_Passcode_Activity.class);
                        intent.putExtra("from", "Wallet__Activity");
                        context.startActivity(intent);
                    } else {
                        Intent intent = new Intent(context, Wallet__Activity.class);
                        context.startActivity(intent);
                        ((Activity) context).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                    }
                }
            }
        };
        handler.postDelayed(runnable, 300);
    }

    public void call_Pay() {
        handler = new Handler();
        runnable = new Runnable() {

            @Override
            public void run() {
                try {

                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (SessionSave.getUserSession(Common.IS_PASSCODE_REQUIRED, context) != null && SessionSave.getUserSession(Common.IS_PASSCODE_REQUIRED, context).equalsIgnoreCase("1")) {
                        Intent intent = new Intent(context, Create_Passcode_Activity.class);
                        intent.putExtra("from", "TickPay");
                        context.startActivity(intent);
                    } else {
                        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TICK_PAY_SPLASH, context) != null && SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TICK_PAY_SPLASH, context).equalsIgnoreCase("0")) {
                            Intent intent = new Intent(context, Tickpay_Splash_Activity.class);
                            context.startActivity(intent);
                        } else if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_VERIFY_USER, context) != null && SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_VERIFY_USER, context).equalsIgnoreCase("2")) {
                            Intent intent = new Intent(context, TickPayActivity.class);
                            context.startActivity(intent);
                            ((Activity) context).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                        } else if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_VERIFY_USER, context) != null && SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_VERIFY_USER, context).equalsIgnoreCase("1")) {
                            if (Global.isNetworkconn(context)) {
                                call_GetTickpayApprovalStatus();
                            } else {
                                InternetDialog internetDialog = new InternetDialog(context);
                                internetDialog.showDialog("Please check your internet connection!");
                            }
                        } else if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TICK_PAY_SPLASH, context) != null && SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TICK_PAY_SPLASH, context).equalsIgnoreCase("1")) {
                            Intent intent = new Intent(context, TickPayRegisterActivity.class);
                            context.startActivity(intent);
                            ((Activity) context).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                        }
                    }

                }
            }
        };
        handler.postDelayed(runnable, 300);
    }

    public void call_PaymentOption() {
        handler = new Handler();
        runnable = new Runnable() {

            @Override
            public void run() {
                try {

                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (checkCardList()) {
                        Intent intent = new Intent(context, Wallet_Add_Cards_Activity.class);
                        context.startActivity(intent);
                        ((Activity) context).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                    } else {
                        Intent intent = new Intent(context, Add_Card_In_List_Activity.class);
                        context.startActivity(intent);
                        ((Activity) context).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                    }
                }
            }
        };
        handler.postDelayed(runnable, 300);
    }

    public void call_MyReceipts() {
        handler = new Handler();
        runnable = new Runnable() {

            @Override
            public void run() {
                try {

                } catch (Exception e) {
                    e.printStackTrace();
                } finally {

                    Intent intent = new Intent(context, MyReceiptsActivity.class);
                    context.startActivity(intent);
                    ((Activity) context).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                }
            }
        };
        handler.postDelayed(runnable, 300);
    }

    public void call_InviteFriends() {
        handler = new Handler();
        runnable = new Runnable() {

            @Override
            public void run() {
                try {

                } catch (Exception e) {
                    e.printStackTrace();
                } finally {

                    Intent intent = new Intent(context, InviteFriendsActivity.class);
                    context.startActivity(intent);
                    ((Activity) context).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                }
            }
        };
        handler.postDelayed(runnable, 300);
    }

    public void call_BarsAndClubs() {
        handler = new Handler();
        runnable = new Runnable() {

            @Override
            public void run() {
                try {

                } catch (Exception e) {
                    e.printStackTrace();
                } finally {

                    Intent intent = new Intent(context, BarsAndClubActivity.class);
                    context.startActivity(intent);
                    ((Activity) context).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                }
            }
        };
        handler.postDelayed(runnable, 300);
    }

    public void call_HotelReservation() {
        handler = new Handler();
        runnable = new Runnable() {

            @Override
            public void run() {
                try {

                } catch (Exception e) {
                    e.printStackTrace();
                } finally {

                    Intent intent = new Intent(context, HotelReservationActivity.class);
                    context.startActivity(intent);
                    ((Activity) context).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                }
            }
        };
        handler.postDelayed(runnable, 300);
    }

    public void call_BookAtable() {
        handler = new Handler();
        runnable = new Runnable() {

            @Override
            public void run() {
                try {

                } catch (Exception e) {
                    e.printStackTrace();
                } finally {

                    Intent intent = new Intent(context, BookTableActivity.class);
                    context.startActivity(intent);
                    ((Activity) context).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                }
            }
        };
        handler.postDelayed(runnable, 300);
    }

    public void call_Shopping() {
        handler = new Handler();
        runnable = new Runnable() {

            @Override
            public void run() {
                try {

                } catch (Exception e) {
                    e.printStackTrace();
                } finally {

                    Intent intent = new Intent(context, ShoppingGridActivity.class);
                    context.startActivity(intent);
                    ((Activity) context).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                }
            }
        };
        handler.postDelayed(runnable, 300);
    }

    public void call_Setting() {
        handler = new Handler();
        runnable = new Runnable() {

            @Override
            public void run() {
                try {

                } catch (Exception e) {
                    e.printStackTrace();
                } finally {

                    Intent intent = new Intent(context, SettingActivity.class);
                    context.startActivity(intent);
                    ((Activity) context).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                }
            }
        };
        handler.postDelayed(runnable, 300);
    }


    public void call_BecomeADriver() {
        handler = new Handler();
        runnable = new Runnable() {

            @Override
            public void run() {
                try {

                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    final String appPackageName = context.getApplicationContext().getPackageName();
                    ;
                    try {
                        ((Activity) context).startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        ((Activity) context).startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                    }
                }
            }
        };
        handler.postDelayed(runnable, 300);
    }

    public boolean checkCardList() {
        try {
            Log.e("call", "111111111111111");
            String strCardList = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_CARD_LIST, context);
            List<CreditCard_List_Been> cardListBeens = new ArrayList<CreditCard_List_Been>();

            if (strCardList != null && !strCardList.equalsIgnoreCase("")) {
                Log.e("call", "222222222222222");
                JSONObject json = new JSONObject(strCardList);

                if (json != null) {
                    Log.e("call", "33333333333333");
                    if (json.has("cards")) {
                        Log.e("call", "6666666666666666");
                        JSONArray cards = json.getJSONArray("cards");

                        if (cards != null && cards.length() > 0) {
                            return true;
                        } else {
                            Log.e("call", "no cards available");
                            return false;
                        }
                    } else {
                        Log.e("call", "no cards found");
                        return false;
                    }
                } else {
                    Log.e("call", "json null");
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception e) {
            Log.e("call", "Exception in getting card list = " + e.getMessage());
            return false;
        }
    }

    private void call_GetTickpayApprovalStatus() {
        String url = WebServiceAPI.API_GET_APPROVAL_STATUS + SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID, context);
        Log.e("call", "url = " + url);
        dialogClass.showDialog();
        aQuery.ajax(url.trim(), null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("responseCode", " = " + responseCode);
                    Log.e("Response", " = " + json);

                    if (json != null) {
                        if (json.has("Verify")) {
                            if (json.getString("Verify") != null) {
                                String veriry = json.getString("Verify");

                                if (veriry.equalsIgnoreCase("1")) {
                                    dialogClass.hideDialog();
                                    Intent intent = new Intent(context, TickPayReview.class);
                                    context.startActivity(intent);
                                    ((Activity) context).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                                } else if (veriry.equalsIgnoreCase("2")) {
                                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_VERIFY_USER, "2", context);
                                    Intent intent = new Intent(context, TickPayActivity.class);
                                    context.startActivity(intent);
                                    ((Activity) context).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                                } else {
                                    dialogClass.hideDialog();
                                    Intent intent = new Intent(context, TickPayReview.class);
                                    context.startActivity(intent);
                                    ((Activity) context).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                                }
                            } else {
                                dialogClass.hideDialog();
                                Intent intent = new Intent(context, TickPayReview.class);
                                context.startActivity(intent);
                                ((Activity) context).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                            }
                        } else {
                            dialogClass.hideDialog();
                            Intent intent = new Intent(context, TickPayReview.class);
                            context.startActivity(intent);
                            ((Activity) context).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                        }
                    } else {
                        dialogClass.hideDialog();
                        Intent intent = new Intent(context, TickPayReview.class);
                        context.startActivity(intent);
                        ((Activity) context).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                    }
                } catch (Exception e) {
                    Log.e("Exception", "Exception " + e.toString());
                    dialogClass.hideDialog();
                    Intent intent = new Intent(context, TickPayReview.class);
                    context.startActivity(intent);
                    ((Activity) context).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                }
            }

        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }
}

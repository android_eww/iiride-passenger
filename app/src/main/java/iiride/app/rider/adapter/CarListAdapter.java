package iiride.app.rider.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import iiride.app.rider.R;
import iiride.app.rider.been.CarType_Been;
import iiride.app.rider.been.GetEstimateFare_Been;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class CarListAdapter extends RecyclerView.Adapter
{
    private Context context;
    private List<CarType_Been> list = new ArrayList<>() ;
    //private List<CarType_Been> tempList = new ArrayList<>();
    private List<GetEstimateFare_Been> getEstimateFare_beens;
    private int selectedPos = -1;//Integer.MAX_VALUE;
    private SetCarListener setCarListener;

    public CarListAdapter(Context context, List<CarType_Been> carType_beens, List<GetEstimateFare_Been> getEstimateFare_beens,SetCarListener setCarListener)
    {
        this.context = context;
        //list = carType_beens;
        list.addAll(carType_beens);
        this.getEstimateFare_beens = getEstimateFare_beens;
        this.setCarListener = setCarListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_car,viewGroup,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i)
    {
        if(viewHolder instanceof MyViewHolder)
        {
            MyViewHolder holder = (MyViewHolder) viewHolder;

            holder.car_textview.setText("Avail "+list.get(i).getAvai());

            Picasso.with(context).load(list.get(i).getImage()).into(holder.imageview_car);

            if(getEstimateFare_beens != null &&
                    getEstimateFare_beens.size() > 0)
            {
                if(list.get(i).getAvai() == 0)
                {
                    holder.time.setText("0 min");
                }
                else
                {
                    holder.time.setText(getEstimateFare_beens.get(i).getDuration()+" min");
                }
                if(getEstimateFare_beens != null &&
                        getEstimateFare_beens.size()>0)
                {
                    holder.price.setText("$"+getEstimateFare_beens.get(i).getTotal());
                }
                //Log.e("TAG1","getEstimateFare_beens.get(i).getTotal() = "+getEstimateFare_beens.get(i).getTotal());
            }
            else
            {
                holder.time.setText("0 min");
                holder.price.setText("$0");
                Log.e("TAG1","11111111111111111 = 00000");
            }

            if(selectedPos == i)
            {holder.car_image_layout.setBackgroundResource(R.drawable.circle_red_border);}
            else {
                holder.car_image_layout.setBackgroundResource(R.drawable.circle_white_border);
            }

            //
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder
    {
        private TextView car_textview,time,price;
        private LinearLayout car_image_layout;
        private ImageView imageview_car;

        public MyViewHolder(View itemView) {
            super(itemView);
            price = itemView.findViewById(R.id.price);
            time = itemView.findViewById(R.id.time);
            car_textview = itemView.findViewById(R.id.car_textview);
            car_image_layout = itemView.findViewById(R.id.car_image_layout);
            imageview_car = itemView.findViewById(R.id.imageview_car);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectedPos = getAdapterPosition();
                    setCarListener.onCarClick(selectedPos,list.get(selectedPos));
                    car_image_layout.setBackgroundResource(R.drawable.circle_red_border);
                    notifyDataSetChanged();
                }
            });
        }
    }

    public void setCarList(List<CarType_Been> carType_beens)
    {
        list.clear();
        list.addAll(carType_beens);
        //this.list = carType_beens;
        notifyDataSetChanged();
    }

    public void setPosition(int pos)
    {
        selectedPos = pos;
        notifyDataSetChanged();
    }

    public void estimateTimeList()
    {}

    public interface SetCarListener
    {
        void onCarClick(int poition,CarType_Been carType_been);
    }
}

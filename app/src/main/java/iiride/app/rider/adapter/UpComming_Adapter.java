package iiride.app.rider.adapter;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.provider.CalendarContract;
import android.provider.Settings;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventAttendee;
import com.google.api.services.calendar.model.EventDateTime;
import com.google.api.services.calendar.model.EventReminder;
import com.hendraanggrian.widget.ExpandableItem;
import com.hendraanggrian.widget.ExpandableRecyclerView;

import iiride.app.rider.R;
import iiride.app.rider.activity.BookLaterActivity;
import iiride.app.rider.activity.MyBookingActivity;
import iiride.app.rider.comman.Common;
import iiride.app.rider.comman.SessionSave;
import iiride.app.rider.other.DialogClass;
import iiride.app.rider.other.Global;
import iiride.app.rider.other.InternetDialog;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;


public class UpComming_Adapter extends ExpandableRecyclerView.Adapter<UpComming_Adapter.ViewHolder> {
    Context context;
    int POSITION;
    String TAG = "PastBooking_Req";
    private AQuery aQuery;
    private DialogClass dialogClass;


    public UpComming_Adapter(LinearLayoutManager lm) {
        super(lm);
    }


    public class ViewHolder extends ExpandableRecyclerView.ViewHolder {
        public View layout;

        //header
        private LinearLayout ll_DropoffLocation, ll_Date,ll_CancelRequest;
        public TextView tv_DropoffLocation, tv_Date,tv_BookingId,tv_DriverName,tv_CancelRequest,tvAddToCaleder,tvEditBooking;

        //content
        private LinearLayout ll_PickupLocation,ll_PickupTime,ll_DropoffTime,ll_VehicleType,ll_DistanceTravelled,ll_TripFare,ll_ContactNumber;
        private LinearLayout ll_NightFare,ll_TollFee,ll_WaitingCost,ll_WaitingTime,ll_BookingCharge,ll_Tax,ll_Discount,ll_PaymentType,ll_Total;
        private TextView tv_PickupLocation,tv_PickupTime,tv_DropoffTime,tv_VehicleType,tv_DistanceTravelled,tv_TripFare,tv_NightFare,tv_TollFee;
        private TextView tv_WaitingCost,tv_WaitingTime,tv_BookingCharge,tv_Tax,tv_Discount,tv_PaymentType,tv_Total,tv_ContactNumber;

        private ExpandableItem expandableItem;


        public ViewHolder(View v) {
            super(v);
            layout = v;
            expandableItem = (ExpandableItem) v. findViewById(R.id.row);

            //header

            ll_DropoffLocation = (LinearLayout) expandableItem.findViewById(R.id.ll_DropoffLocation);
            ll_Date = (LinearLayout) expandableItem.findViewById(R.id.ll_Date);
            ll_CancelRequest = (LinearLayout) expandableItem.findViewById(R.id.ll_CancelRequest);

            tv_DropoffLocation = (TextView) expandableItem.findViewById(R.id.tv_DropoffLocation);
            tv_Date = (TextView) expandableItem.findViewById(R.id.tv_Date);
            tv_BookingId = (TextView) expandableItem.findViewById(R.id.tv_BookingId);
            tv_DriverName = (TextView) expandableItem.findViewById(R.id.tv_DriverName);
            tv_CancelRequest = (TextView) expandableItem.findViewById(R.id.tv_CancelRequest);
            tvAddToCaleder = expandableItem.findViewById(R.id.tvAddToCaleder);
            tvEditBooking = expandableItem.findViewById(R.id.tvEditBooking);

            //content

            ll_PickupLocation = (LinearLayout) expandableItem.findViewById(R.id.ll_PickupLocation);
            ll_PickupTime = (LinearLayout) expandableItem.findViewById(R.id.ll_PickupTime);
            ll_DropoffTime = (LinearLayout) expandableItem.findViewById(R.id.ll_DropoffTime);
            ll_VehicleType = (LinearLayout) expandableItem.findViewById(R.id.ll_VehicleType);
            ll_DistanceTravelled = (LinearLayout) expandableItem.findViewById(R.id.ll_DistanceTravelled);
            ll_TripFare = (LinearLayout) expandableItem.findViewById(R.id.ll_TripFare);
            ll_NightFare = (LinearLayout) expandableItem.findViewById(R.id.ll_NightFare);
            ll_TollFee = (LinearLayout) expandableItem.findViewById(R.id.ll_TollFee);
            ll_WaitingCost = (LinearLayout) expandableItem.findViewById(R.id.ll_WaitingCost);
            ll_WaitingTime = (LinearLayout) expandableItem.findViewById(R.id.ll_WaitingTime);
            ll_BookingCharge = (LinearLayout) expandableItem.findViewById(R.id.ll_BookingCharge);
            ll_Tax = (LinearLayout) expandableItem.findViewById(R.id.ll_Tax);
            ll_Discount = (LinearLayout) expandableItem.findViewById(R.id.ll_Discount);
            ll_PaymentType = (LinearLayout) expandableItem.findViewById(R.id.ll_PaymentType);
            ll_Total = (LinearLayout) expandableItem.findViewById(R.id.ll_Total);
            ll_ContactNumber = (LinearLayout) expandableItem.findViewById(R.id.ll_ContactNumber);

            tv_PickupLocation = (TextView) expandableItem.findViewById(R.id.tv_PickupLocation);
            tv_PickupTime = (TextView) expandableItem.findViewById(R.id.tv_PickupTime);
            tv_DropoffTime = (TextView) expandableItem.findViewById(R.id.tv_DropoffTime);
            tv_VehicleType = (TextView) expandableItem.findViewById(R.id.tv_VehicleType);
            tv_DistanceTravelled = (TextView) expandableItem.findViewById(R.id.tv_DistanceTravelled);
            tv_TripFare = (TextView) expandableItem.findViewById(R.id.tv_TripFare);
            tv_NightFare = (TextView) expandableItem.findViewById(R.id.tv_NightFare);
            tv_TollFee = (TextView) expandableItem.findViewById(R.id.tv_TollFee);
            tv_WaitingCost = (TextView) expandableItem.findViewById(R.id.tv_WaitingCost);
            tv_WaitingTime = (TextView) expandableItem.findViewById(R.id.tv_WaitingTime);
            tv_BookingCharge = (TextView) expandableItem.findViewById(R.id.tv_BookingCharge);
            tv_Tax = (TextView) expandableItem.findViewById(R.id.tv_Tax);
            tv_Discount = (TextView) expandableItem.findViewById(R.id.tv_Discount);
            tv_PaymentType = (TextView) expandableItem.findViewById(R.id.tv_PaymentType);
            tv_Total = (TextView) expandableItem.findViewById(R.id.tv_Total);
            tv_ContactNumber = (TextView) expandableItem.findViewById(R.id.tv_ContactNumber);

        }
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        this.context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.row_up_comming, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position)
    {
        super.onBindViewHolder(holder, position);
        POSITION = position;

        /*holder.iv_status_active.setVisibility(View.GONE);
        holder.iv_status_inactive.setVisibility(View.GONE);*/

        if (MyBookingActivity.upComming_beens.get(position).getDropoffLocation()!=null && !MyBookingActivity.upComming_beens.get(position).getDropoffLocation().equalsIgnoreCase(""))
        {
            holder.tv_DropoffLocation.setText(MyBookingActivity.upComming_beens.get(position).getDropoffLocation());
        }
        else
        {
            holder.ll_DropoffLocation.setVisibility(View.GONE);
        }

        if (MyBookingActivity.upComming_beens.get(position).getDriverName()!=null && !MyBookingActivity.upComming_beens.get(position).getDriverName().equalsIgnoreCase("") && !MyBookingActivity.upComming_beens.get(position).getDriverName().equalsIgnoreCase("null"))
        {
            holder.tv_DriverName.setText(MyBookingActivity.upComming_beens.get(position).getDriverName());
        }
        else
        {
            holder.tv_DriverName.setVisibility(View.GONE);
        }

//        if (MyBookingActivity.upComming_beens.get(position).getId()!=null && !MyBookingActivity.upComming_beens.get(position).getId().equalsIgnoreCase(""))
//        {
//            holder.tv_BookingId.setText("Booking Id :("+MyBookingActivity.upComming_beens.get(position).getId()+")");
//        }
//        else
//        {
//            holder.tv_BookingId.setVisibility(View.GONE);
//        }

        if (MyBookingActivity.upComming_beens.get(position).getPickupDateTime()!=null && !MyBookingActivity.upComming_beens.get(position).getPickupDateTime().equalsIgnoreCase(""))
        {
            holder.tv_Date.setText(Global.getDateInFormat(MyBookingActivity.upComming_beens.get(position).getPickupDateTime()));
        }
        else
        {
            holder.ll_Date.setVisibility(View.GONE);
        }

        if (MyBookingActivity.upComming_beens.get(position).getDriverMobileNo()!=null && !MyBookingActivity.upComming_beens.get(position).getDriverMobileNo().equalsIgnoreCase("null")
                && !MyBookingActivity.upComming_beens.get(position).getDriverMobileNo().equalsIgnoreCase("NULL") &&
                !MyBookingActivity.upComming_beens.get(position).getDriverMobileNo().equalsIgnoreCase(""))
        {
            //holder.ll_ContactNumber.setVisibility(View.VISIBLE);
            holder.tv_ContactNumber.setText(MyBookingActivity.upComming_beens.get(position).getDriverMobileNo());
            holder.tv_ContactNumber.setPaintFlags(holder.tv_ContactNumber.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
        }
        else
        {
            holder.ll_ContactNumber.setVisibility(View.GONE);
        }

        if (MyBookingActivity.upComming_beens.get(position).getPickupLocation()!=null && !MyBookingActivity.upComming_beens.get(position).getPickupLocation().equalsIgnoreCase(""))
        {
            holder.tv_PickupLocation.setText(MyBookingActivity.upComming_beens.get(position).getPickupLocation());
        }
        else
        {
            holder.ll_PickupLocation.setVisibility(View.GONE);
        }

        if (MyBookingActivity.upComming_beens.get(position).getCarDetails_Company()!=null && !MyBookingActivity.upComming_beens.get(position).getCarDetails_Company().equalsIgnoreCase(""))
        {
            holder.ll_VehicleType.setVisibility(View.VISIBLE);
            holder.tv_VehicleType.setText(MyBookingActivity.upComming_beens.get(position).getCarDetails_Company()+" "+MyBookingActivity.upComming_beens.get(position).getCarDetails_Color());
        }
        else
        {
            holder.ll_VehicleType.setVisibility(View.GONE);
        }

//        if (MyBookingActivity.upComming_beens.get(position).getPickupDateTime()!=null && !MyBookingActivity.upComming_beens.get(position).getPickupDateTime().equalsIgnoreCase(""))
//        {
//            holder.ll_PickupTime.setVisibility(View.VISIBLE);
//            holder.tv_PickupTime.setText(MyBookingActivity.upComming_beens.get(position).getPickupDateTime());
//        }
//        else
//        {
//            holder.ll_PickupTime.setVisibility(View.GONE);
//        }

        if (MyBookingActivity.upComming_beens.get(position).getDropOffDateTime()!=null && !MyBookingActivity.upComming_beens.get(position).getDropOffDateTime().equalsIgnoreCase(""))
        {
//            String dateString = getDate(Long.parseLong(MyBookingActivity.pastBooking_beens.get(position).getDropTime()+"000"), "HH:mm  yyyy/MM/dd");
            holder.tv_DropoffTime.setText(Global.getDateInFormat(MyBookingActivity.upComming_beens.get(position).getDropOffDateTime()));
        }
        else
        {
            holder.ll_DropoffTime.setVisibility(View.GONE);
        }


        if (MyBookingActivity.upComming_beens.get(position).getTripDistance()!=null && !MyBookingActivity.upComming_beens.get(position).getTripDistance().equalsIgnoreCase(""))
        {
            holder.tv_DistanceTravelled.setText(MyBookingActivity.upComming_beens.get(position).getTripDistance());
        }
        else
        {
            holder.ll_DistanceTravelled.setVisibility(View.GONE);
        }

        if (MyBookingActivity.upComming_beens.get(position).getTripFare()!=null && !MyBookingActivity.upComming_beens.get(position).getTripFare().equalsIgnoreCase(""))
        {
            holder.tv_TripFare.setText(MyBookingActivity.upComming_beens.get(position).getTripFare());
        }
        else
        {
            holder.ll_TripFare.setVisibility(View.GONE);
        }

        if (MyBookingActivity.upComming_beens.get(position).getNightFare()!=null && !MyBookingActivity.upComming_beens.get(position).getNightFare().equalsIgnoreCase(""))
        {
            holder.tv_NightFare.setText(MyBookingActivity.upComming_beens.get(position).getNightFare());
        }
        else
        {
            holder.ll_NightFare.setVisibility(View.GONE);
        }

        if (MyBookingActivity.upComming_beens.get(position).getTollFee()!=null && !MyBookingActivity.upComming_beens.get(position).getTollFee().equalsIgnoreCase(""))
        {
            holder.ll_TollFee.setVisibility(View.GONE);
            holder.tv_TollFee.setText(MyBookingActivity.upComming_beens.get(position).getTollFee());
        }
        else
        {
            holder.ll_TollFee.setVisibility(View.GONE);
        }

        if (MyBookingActivity.upComming_beens.get(position).getWaitingTimeCost()!=null && !MyBookingActivity.upComming_beens.get(position).getWaitingTimeCost().equalsIgnoreCase(""))
        {
            holder.tv_WaitingCost.setText(MyBookingActivity.upComming_beens.get(position).getWaitingTimeCost());
        }
        else
        {
            holder.ll_WaitingCost.setVisibility(View.GONE);
        }

        if (MyBookingActivity.upComming_beens.get(position).getWaitingTime()!=null && !MyBookingActivity.upComming_beens.get(position).getWaitingTime().equalsIgnoreCase(""))
        {
            holder.tv_WaitingTime.setText(MyBookingActivity.upComming_beens.get(position).getWaitingTime());
        }
        else
        {
            holder.ll_WaitingTime.setVisibility(View.GONE);
        }

        if (MyBookingActivity.upComming_beens.get(position).getBookingCharge()!=null && !MyBookingActivity.upComming_beens.get(position).getBookingCharge().equalsIgnoreCase(""))
        {
            holder.tv_BookingCharge.setText(MyBookingActivity.upComming_beens.get(position).getBookingCharge());
        }
        else
        {
            holder.ll_BookingCharge.setVisibility(View.GONE);
        }

        if (MyBookingActivity.upComming_beens.get(position).getTax()!=null && !MyBookingActivity.upComming_beens.get(position).getTax().equalsIgnoreCase(""))
        {
            holder.tv_Tax.setText(MyBookingActivity.upComming_beens.get(position).getTax());
        }
        else
        {
            holder.ll_Tax.setVisibility(View.GONE);
        }

//        if (MyBookingActivity.upComming_beens.get(position).getDiscount()!=null && !MyBookingActivity.upComming_beens.get(position).getDiscount().equalsIgnoreCase(""))
//        {
//            holder.tv_Discount.setText(MyBookingActivity.upComming_beens.get(position).getDiscount());
//        }
//        else
//        {
//            holder.ll_Discount.setVisibility(View.GONE);
//        }
        holder.ll_Discount.setVisibility(View.GONE);

        if (MyBookingActivity.upComming_beens.get(position).getPaymentType()!=null && !MyBookingActivity.upComming_beens.get(position).getPaymentType().equalsIgnoreCase(""))
        {
            holder.tv_PaymentType.setText(MyBookingActivity.upComming_beens.get(position).getPaymentType());
        }
        else
        {
            holder.ll_PaymentType.setVisibility(View.GONE);
        }

        if (MyBookingActivity.upComming_beens.get(position).getGrandTotal()!=null && !MyBookingActivity.upComming_beens.get(position).getGrandTotal().equalsIgnoreCase(""))
        {
            holder.tv_Total.setText(MyBookingActivity.upComming_beens.get(position).getGrandTotal());
        }
        else
        {
            holder.ll_Total.setVisibility(View.GONE);
        }

        holder.tv_ContactNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (MyBookingActivity.upComming_beens.get(position).getDriverMobileNo()!=null &&
                        !MyBookingActivity.upComming_beens.get(position).getDriverMobileNo().equalsIgnoreCase("") &&
                        !MyBookingActivity.upComming_beens.get(position).getDriverMobileNo().equalsIgnoreCase("null") &&
                        !MyBookingActivity.upComming_beens.get(position).getDriverMobileNo().equalsIgnoreCase("NULL"))
                {
                    callToDriver(MyBookingActivity.upComming_beens.get(position).getDriverMobileNo());
                }
                else
                {
                    InternetDialog internetDialog = new InternetDialog(context);
                    internetDialog.showDialog("Number not found!");
                }
            }
        });

        holder.tvEditBooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Global.isNetworkconn(context))
                {
                    Intent intent = new Intent(context, BookLaterActivity.class);
                    intent.putExtra("from","upcomming");
                    intent.putExtra("modelId",MyBookingActivity.upComming_beens.get(position).getModelId());
                    intent.putExtra("editName",MyBookingActivity.upComming_beens.get(position).getPassengerName());
                    intent.putExtra("editNumber",MyBookingActivity.upComming_beens.get(position).getPassengerContact());
                    intent.putExtra("pickup",MyBookingActivity.upComming_beens.get(position).getPickupLocation());
                    intent.putExtra("dropoff",MyBookingActivity.upComming_beens.get(position).getDropoffLocation());
                    intent.putExtra("PassengerType",MyBookingActivity.upComming_beens.get(position).getPassengerType());
                    intent.putExtra("bookingType",MyBookingActivity.upComming_beens.get(position).getBookingType());
                    intent.putExtra("PromoCode",MyBookingActivity.upComming_beens.get(position).getPromoCode());
                    intent.putExtra("PickupDateTime",MyBookingActivity.upComming_beens.get(position).getPickupDateTime());
                    intent.putExtra("CardId",MyBookingActivity.upComming_beens.get(position).getCardId());
                    intent.putExtra("FlightNumber",MyBookingActivity.upComming_beens.get(position).getFlightNumber());
                    intent.putExtra("Notes",MyBookingActivity.upComming_beens.get(position).getNotes());
                    intent.putExtra("BabySeat",MyBookingActivity.upComming_beens.get(position).getBabySeat());
                    intent.putExtra("BookingId",MyBookingActivity.upComming_beens.get(position).getId());
                    intent.putExtra("PickupLat",MyBookingActivity.upComming_beens.get(position).getPickupLat());
                    intent.putExtra("PickupLng",MyBookingActivity.upComming_beens.get(position).getPickupLng());
                    intent.putExtra("DropoffLat",MyBookingActivity.upComming_beens.get(position).getDropOffLat());
                    intent.putExtra("DropoffLon",MyBookingActivity.upComming_beens.get(position).getDropOffLon());
                    intent.putExtra("pickUpSubArb",MyBookingActivity.upComming_beens.get(position).getPickupSuburb());
                    intent.putExtra("dropOffSubAArb",MyBookingActivity.upComming_beens.get(position).getDropoffSuburb());
                    context.startActivity(intent);
                    ((Activity)context).overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
                }
                else
                {
                    InternetDialog internetDialog = new InternetDialog(context);
                    internetDialog.showDialog("Please check your internet connection!");
                }
            }
        });

        holder.tv_CancelRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int tripFlag = 0;

                if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG,context)!=null && SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG,context).equalsIgnoreCase("2"))
                {
                    InternetDialog internetDialog =new InternetDialog(context);
                    internetDialog.showDialog("Please first complete your trip!");
                }
                else
                {
                    if (Global.isNetworkconn(context))
                    {
                        showYesNoDialog(position);
                    }
                    else
                    {
                        InternetDialog internetDialog = new InternetDialog(context);
                        internetDialog.showDialog("Please check your internet connection!");
                    }
                }

            }
        });

        /*holder.ll_CancelRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int tripFlag = 0;

                if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG,context)!=null && SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG,context).equalsIgnoreCase("2"))
                {
                    InternetDialog internetDialog =new InternetDialog(context);
                    internetDialog.showDialog("Please first complete your trip!");
                }
                else
                {
                    if (Global.isNetworkconn(context))
                    {
                        if (MyBookingActivity.upComming_beens.get(position).getBookingType()!=null && MyBookingActivity.upComming_beens.get(position).getBookingType().equalsIgnoreCase("Book Later"))
                        {
                            cancel_AdvanceBookingRequest(position);
                        }
                        else if (MyBookingActivity.upComming_beens.get(position).getBookingType()!=null && MyBookingActivity.upComming_beens.get(position).getBookingType().equalsIgnoreCase("Book Now"))
                        {
                            canelBookingRequest(position);
                        }
                    }
                    else
                    {
                        InternetDialog internetDialog = new InternetDialog(context);
                        internetDialog.showDialog("Please check your internet connection!");
                    }
                }

            }
        });*/

        holder.tvAddToCaleder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED)
                {
                    // Permission is not granted

                    new AlertDialog.Builder(context)
                            .setTitle("Require Calender Permission")
                            .setMessage("Please allow calender permission from setting.")
                            .setPositiveButton("Setting", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                    Uri uri = Uri.fromParts("package", context.getPackageName(), null);
                                    intent.setData(uri);
                                    context.startActivity(intent);
                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                }
                            })
                            .show();
                }
                else
                {
                    showYesNoDialogAddToCalender(position);
                }
            }
        });

    }

    public void showYesNoDialog(final int position)
    {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.confirmation_dialog);

        TextView tv_yes = (TextView) dialog.findViewById(R.id.yes);
        TextView tv_No = (TextView) dialog.findViewById(R.id.no);
        TextView tv_Message = (TextView) dialog.findViewById(R.id.message);
        LinearLayout ll_Yes = (LinearLayout) dialog.findViewById(R.id.yes_layout);
        LinearLayout ll_No = (LinearLayout) dialog.findViewById(R.id.no_layout);

        tv_Message.setText("Are you sure you want to cancel your trip?");

        ll_Yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (MyBookingActivity.upComming_beens.get(position).getBookingType()!=null && MyBookingActivity.upComming_beens.get(position).getBookingType().equalsIgnoreCase("Book Later"))
                {
                    cancel_AdvanceBookingRequest(position);
                }
                else if (MyBookingActivity.upComming_beens.get(position).getBookingType()!=null && MyBookingActivity.upComming_beens.get(position).getBookingType().equalsIgnoreCase("Book Now"))
                {
                    canelBookingRequest(position);
                }
            }
        });

        tv_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (MyBookingActivity.upComming_beens.get(position).getBookingType()!=null && MyBookingActivity.upComming_beens.get(position).getBookingType().equalsIgnoreCase("Book Later"))
                {
                    cancel_AdvanceBookingRequest(position);
                }
                else if (MyBookingActivity.upComming_beens.get(position).getBookingType()!=null && MyBookingActivity.upComming_beens.get(position).getBookingType().equalsIgnoreCase("Book Now"))
                {
                    canelBookingRequest(position);
                }
            }
        });

        ll_No.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        tv_No.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        dialog.show();
    }

    public void showYesNoDialogAddToCalender(final int position)
    {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.confirmation_dialog);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        TextView tv_yes = (TextView) dialog.findViewById(R.id.yes);
        TextView tv_No = (TextView) dialog.findViewById(R.id.no);
        TextView tv_Message = (TextView) dialog.findViewById(R.id.message);
        LinearLayout ll_Yes = (LinearLayout) dialog.findViewById(R.id.yes_layout);
        LinearLayout ll_No = (LinearLayout) dialog.findViewById(R.id.no_layout);

        tv_Message.setText("Are you sure you want to add this trip into your calender?");

        ll_Yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                addEventInCalender(
                        MyBookingActivity.upComming_beens.get(position).getPickupLocation(),
                        MyBookingActivity.upComming_beens.get(position).getDropoffLocation(),
                        MyBookingActivity.upComming_beens.get(position).getPickupDateTime()
                );
            }
        });

        tv_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                addEventInCalender(
                        MyBookingActivity.upComming_beens.get(position).getPickupLocation(),
                        MyBookingActivity.upComming_beens.get(position).getDropoffLocation(),
                        MyBookingActivity.upComming_beens.get(position).getPickupDateTime()
                );
            }
        });

        ll_No.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        tv_No.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void addEventInCalender(String pickup,String dropoffLocation, String pickupTime)
    {
        //03:45 PM 05/03/2020
        try {
            SimpleDateFormat input = new SimpleDateFormat("hh:mm a dd/MM/yyyy");
            Date dateObj = input.parse(pickupTime);
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm dd/MM/yyyy");
            pickupTime = sdf.format(dateObj);
        }
        catch (Exception e)
        {

        }

        String[] x = pickupTime.split(" ");
        String time = x[0];
        String date = x[1];

        int hour = 0 ,minute = 0;
        if(time != null && !time.equalsIgnoreCase(""))
        {
            String[] p = time.split(":");
            hour = Integer.parseInt(p[0]);
            minute = Integer.parseInt(p[1]);
        }


        int day = 0 ,month = 0 ,year = 0;
        if(date != null && !date.equalsIgnoreCase(""))
        {
            String[] p = date.split("/");
            day = Integer.parseInt(p[0]);
            month = Integer.parseInt(p[1]);
            year = Integer.parseInt(p[2]);

            month -= 1;
        }


        long calID = 3;
        long startMillis = 0;
        long endMillis = 0;

        Calendar beginTime = Calendar.getInstance();
        beginTime.set(year, month, day, hour-1, minute);
        startMillis = beginTime.getTimeInMillis();

        Calendar endTime = Calendar.getInstance();
        endTime.set(year, month, day, hour, minute);
        endMillis = endTime.getTimeInMillis();

//        ContentResolver cr = context.getContentResolver();
//        ContentValues values = new ContentValues();
//        values.put(CalendarContract.Events.DTSTART, startMillis);
//        values.put(CalendarContract.Events.DTEND, endMillis);
//        values.put(CalendarContract.Events.TITLE, "Upcoming trip at \nPickup Location: \n"
//                +pickup+
//                "\nDropoff Location: \n"+dropoffLocation+
//                "\nTime:\n"+pickupTime
//        );
//        values.put(CalendarContract.Events.DESCRIPTION, pickup);
//        values.put(CalendarContract.Events.CALENDAR_ID, calID);
////        values.put(CalendarContract.Events.EVENT_TIMEZONE, "Australia/Melbourne");
//        values.put(CalendarContract.Events.EVENT_TIMEZONE, "Asia/Kolkata");
//        Uri uri = cr.insert(CalendarContract.Events.CONTENT_URI, values);
//
//        long eventID = Long.parseLong(uri.getLastPathSegment());
//        Log.e(TAG, "addEventInCalender() eventID:- " + eventID);
//
//        new AlertDialog.Builder(context)
//                .setTitle("iiRide")
//                .setMessage("Event Added Successfully.")
//                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//                    }
//                })
//                .show();

        Intent intent = new Intent(Intent.ACTION_INSERT)
                .setData(CalendarContract.Events.CONTENT_URI)
                .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, startMillis)
                .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endMillis)
                .putExtra(CalendarContract.Events.TITLE, "Upcoming trip at \nPickup Location: \n"
                        +pickup+
                        "\nDropoff Location: \n"+dropoffLocation+
                        "\nTime:\n"+pickupTime)
                .putExtra(CalendarContract.Events.DESCRIPTION, pickup)
//                .putExtra(CalendarContract.Events.EVENT_LOCATION, "The gym")
                .putExtra(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_BUSY);
//                .putExtra(Intent.EXTRA_EMAIL, "rowan@example.com,trevor@example.com");
        context.startActivity(intent);

    }

    public String getDate(long milliSeconds, String dateFormat)
    {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    public void callToDriver(String driverPhone)
    {
        TelephonyManager telMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        int simState = telMgr.getSimState();
        InternetDialog internetDialog = null;
        switch (simState) {
            case TelephonyManager.SIM_STATE_ABSENT:
                internetDialog = new InternetDialog(context);
                internetDialog.showDialog("Sim card not available");
                break;
            case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
                internetDialog = new InternetDialog(context);
                internetDialog.showDialog("Sim state network locked");
                break;
            case TelephonyManager.SIM_STATE_PIN_REQUIRED:
                internetDialog = new InternetDialog(context);
                internetDialog.showDialog("Sim state pin required");
                break;
            case TelephonyManager.SIM_STATE_PUK_REQUIRED:
                internetDialog = new InternetDialog(context);
                internetDialog.showDialog("Sim state puk required");
                break;
            case TelephonyManager.SIM_STATE_READY:
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + driverPhone));
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                context.startActivity(intent);
                break;
            case TelephonyManager.SIM_STATE_UNKNOWN:
                internetDialog = new InternetDialog(context);
                internetDialog.showDialog("Sim state unknown");
                break;
        }
    }

    @Override
    public int getItemCount() {
        return MyBookingActivity.upComming_beens.size();
    }

    public void canelBookingRequest(int position)
    {
        try
        {
            Log.e("call","canelBookingRequest");
            if (Common.socket!=null && Common.socket.connected())
            {
                Log.e("call","socket connected");
                Log.e("call","BookingId = "+MyBookingActivity.upComming_beens.get(position).getId());
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("BookingId",MyBookingActivity.upComming_beens.get(position).getId());
                Common.socket.emit("CancelTripByPassenger",jsonObject);
                Log.e("call","CancelTripByPassenger = "+jsonObject.toString());

                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG,"0",context);
                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR,"-1",context);
                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL,"",context);

                showDialog();
            }
        }
        catch (Exception e)
        {
            Log.e("call","Exception = "+e.getMessage());
        }
    }

    public void cancel_AdvanceBookingRequest(int position)
    {
        try
        {
            Log.e("call","cancelAdvanceBookingRequest");
            if (Common.socket!=null && Common.socket.connected())
            {
                Log.e("call","socket connected");
                Log.e("call","BookingId = "+MyBookingActivity.upComming_beens.get(position).getId());
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("BookingId",MyBookingActivity.upComming_beens.get(position).getId());
                Common.socket.emit("AdvancedBookingCancelTripByPassenger",jsonObject);
                Log.e("call","AdvancedBookingCancelTripByPassenger = "+jsonObject.toString());


                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG,"0",context);
                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR,"-1",context);
                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL,"",context);

                showDialog();
            }
        }
        catch (Exception e)
        {
            Log.e("call","Exception = "+e.getMessage());
        }
    }

    public void showDialog()
    {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.my_dialog_class);

        TextView tv_Ok = (TextView) dialog.findViewById(R.id.Ok);
        TextView tv_Message = (TextView) dialog.findViewById(R.id.message);
        LinearLayout ll_Ok = (LinearLayout) dialog.findViewById(R.id.ok_layout);

        tv_Message.setText("Your request canceled successfully!");

        ll_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
//                ((Activity)context).finish();
//                ((Activity)context).overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);
            }
        });

        tv_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
//                ((Activity)context).finish();
//                ((Activity)context).overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        dialog.show();
    }
}

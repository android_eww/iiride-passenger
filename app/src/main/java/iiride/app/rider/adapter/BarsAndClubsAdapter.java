package iiride.app.rider.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;

import iiride.app.rider.R;
import iiride.app.rider.activity.BarsAndClubActivity;
import iiride.app.rider.activity.MainActivity;
import iiride.app.rider.been.BarsAndClubs_Been;
import iiride.app.rider.comman.TaxiUtil;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;

public class BarsAndClubsAdapter extends RecyclerView.Adapter<BarsAndClubsAdapter.ViewHolder> {

    private ArrayList<BarsAndClubs_Been> arrayList;
    private Context context;

    public BarsAndClubsAdapter(ArrayList<BarsAndClubs_Been> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_bars_and_clubs, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {

        viewHolder.tv_hotelname.setText(arrayList.get(position).getName());
        viewHolder.tv_hoteladd.setText(arrayList.get(position).getAddress());
        viewHolder.tv_hoteldistance.setText(arrayList.get(position).getDistance());

        if (arrayList.get(position).getImage()!=null && !arrayList.get(position).getImage().equalsIgnoreCase(""))
        {
            Picasso.with(context).load(arrayList.get(position).getImage()).into(viewHolder.iv_hotelimage);
        }
        else
        {
            viewHolder.iv_hotelimage.setVisibility(View.INVISIBLE);
        }

        if (arrayList.get(position).getRatting()!=null && !arrayList.get(position).getRatting().equalsIgnoreCase(""))
        {
            viewHolder.rb_hotelrating.setRating(Float.parseFloat(arrayList.get(position).getRatting()));
        }
        else
        {
            viewHolder.rb_hotelrating.setRating(0);
        }

        viewHolder.sample1.setShowMode(SwipeLayout.ShowMode.PullOut);
        viewHolder.sample1.addDrag(SwipeLayout.DragEdge.Right, viewHolder.sample1.findViewById(R.id.bottom_wrapper));

        
        viewHolder.sample1.getSurfaceView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("HotelsBookingAdapter", "click on surface");
                if (viewHolder.sample1.isLeftSwipeEnabled())
                {
                    viewHolder.sample1.close();
                }
                else
                {
                    viewHolder.sample1.open();
                }
            }
        });
        viewHolder.sample1.getSurfaceView().setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Log.d("HotelsBookingAdapter", "longClick on surface");
                return true;
            }
        });

        MainActivity.redirectBooking ="";
        MainActivity.redirectBookingAdrs ="";
        TaxiUtil.dropoff_Lat = 0;
        TaxiUtil.dropoff_Long = 0;

        viewHolder.tv_booknow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewHolder.sample1.close();
                Log.e("HotelsBookingAdapter", "longClick on tv_booknow : "+arrayList.get(position).getAddress());

                Intent intent =new Intent(context, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |Intent.FLAG_ACTIVITY_SINGLE_TOP);
                MainActivity.redirectBooking = "booknow";
                MainActivity.redirectBookingAdrs = arrayList.get(position).getAddress();
                TaxiUtil.dropoff_Address = arrayList.get(position).getAddress();
                TaxiUtil.dropoff_Lat = arrayList.get(position).getSource_latitude();
                TaxiUtil.dropoff_Long = arrayList.get(position).getSource_longitude();
                ((BarsAndClubActivity)context).startActivity(intent);
            }
        });

        viewHolder.tv_booklater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewHolder.sample1.close();
                Log.e("HotelsBookingAdapter", "longClick on tv_booklater : "+arrayList.get(position).getAddress());
                Intent intent =new Intent(context, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |Intent.FLAG_ACTIVITY_SINGLE_TOP);
                MainActivity.redirectBooking = "booklater";
                MainActivity.redirectBookingAdrs = arrayList.get(position).getAddress();
                TaxiUtil.dropoff_Address = arrayList.get(position).getAddress();
                TaxiUtil.dropoff_Lat = arrayList.get(position).getSource_latitude();
                TaxiUtil.dropoff_Long = arrayList.get(position).getSource_longitude();
                ((BarsAndClubActivity)context).startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tv_hotelname,tv_hoteladd,tv_hoteldistance,tv_booknow,tv_booklater;
        private ImageView iv_hotelimage;
        private RatingBar rb_hotelrating;
        private SwipeLayout sample1;


        public ViewHolder(View view) {
            super(view);

            tv_hotelname = (TextView)view.findViewById(R.id.tv_hotelname);
            tv_hoteladd = (TextView)view.findViewById(R.id.tv_hoteladd);
            tv_hoteldistance = (TextView)view.findViewById(R.id.tv_distance);
            iv_hotelimage = (ImageView)view.findViewById(R.id.iv_hotelimage);
            rb_hotelrating = (RatingBar)view.findViewById(R.id.rb_hotelrating);

            tv_booknow = (TextView)view.findViewById(R.id.tv_booknow);
            tv_booklater = (TextView)view.findViewById(R.id.tv_booklater);
            sample1 = (SwipeLayout) view.findViewById(R.id.sample1);
        }
    }
}

package iiride.app.rider.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import iiride.app.rider.R;
import iiride.app.rider.activity.BookTableByTypeActivity;
import iiride.app.rider.been.BookTable_Been;

import java.util.List;


public class BookTableAdapter extends BaseAdapter {

    private Context context;
    private List<BookTable_Been> list;

    public BookTableAdapter(Context context, List<BookTable_Been> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public BookTable_Been getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;

        if (convertView == null)
        {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.row_book_table, null);
            holder = new ViewHolder();

            holder.tv_Title= (TextView) convertView.findViewById(R.id.title);
            holder.tv_Count= (TextView) convertView.findViewById(R.id.count);
            holder.iv_Image = (ImageView) convertView.findViewById(R.id.picture);
            holder.iv_ImageInvisible = (ImageView) convertView.findViewById(R.id.picture_invisible);
            holder.ll_Row = (LinearLayout) convertView.findViewById(R.id.row);

            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }

//        Log.e("DHeight"," = "+Constants.DEVICE_HEIGHT);
//        Log.e("HHeight"," = "+Constants.HEADER_HEIGHT);
//        Log.e("SHeight"," = "+Constants.STATUS_BAR_HEIGHT);
//        int Sum = Constants.DEVICE_HEIGHT + Constants.HEADER_HEIGHT + Constants.STATUS_BAR_HEIGHT;
//
//        Log.e("Sum","Grid_Activity.navigationBarHeight  "+Grid_Activity.navigationBarHeight );
//
//        int height1 = Constants.DEVICE_HEIGHT - Constants.HEADER_HEIGHT - Constants.STATUS_BAR_HEIGHT - 28 + Grid_Activity.navigationBarHeight ;///(Sum/2160*28)
//        Log.e("Height"," = "+height1);
//        Log.e("Height","devide = "+(height1/3));
//        holder.ll_Row.getLayoutParams().height = (height1/3);
//
//        holder.ll_Row.setBackgroundColor(Color.argb(30,0,0,0));

        holder.tv_Title.setText(list.get(position).getTitle().trim());
        holder.tv_Count.setText(list.get(position).getCount().trim());
        holder.iv_Image.setImageResource(list.get(position).getId());
        holder.iv_ImageInvisible.setImageResource(list.get(position).getId());

        holder.ll_Row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, BookTableByTypeActivity.class);
                intent.putExtra("title",list.get(position).getTitle().trim());
                //list.get(position).getName().trim()
                context.startActivity(intent);
                ((Activity)context).overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);

            }
        });

        return convertView;
    }

    public static class ViewHolder {
        private TextView tv_Title,tv_Count;
        private ImageView iv_Image,iv_ImageInvisible;
        private LinearLayout ll_Row;
    }
}


package iiride.app.rider.activity;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;

import iiride.app.rider.R;
import iiride.app.rider.application.TicktocApplication;
import iiride.app.rider.been.initData.InitData;
import iiride.app.rider.comman.Common;
import iiride.app.rider.comman.Constants;
import iiride.app.rider.comman.SessionSave;
import iiride.app.rider.comman.WebServiceAPI;
import iiride.app.rider.notification.FCMUtil;
import iiride.app.rider.other.DialogClass;
import iiride.app.rider.other.Global;
import iiride.app.rider.other.InternetDialog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.MessageDigest;

import android.provider.Settings.Secure;

public class Splash_Activity extends AppCompatActivity {

    //Defining variable
    public static Splash_Activity activity;
    private TicktocApplication application;

    private static final String TAG = "Splash_Activity";

    private Handler handler;
    private Runnable runnable;
    private final int SPLASH_DISPLAY_LENGTH = 1000;
    private String versionName;

    private AQuery aQuery;
    private DialogClass dialogClass;
    private String android_id;
    private TelephonyManager tm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        activity = this;
        application = (TicktocApplication) getApplicationContext();

        aQuery = new AQuery(activity);
        dialogClass = new DialogClass(activity, 0);

        android_id = Secure.getString(getContentResolver(),Secure.ANDROID_ID);
        Common.USER_DEVICE_ID = android_id;
        Log.e("call","android_id = "+Common.USER_DEVICE_ID);

        try
        {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            versionName = pInfo.versionName;
            int versionCode = pInfo.versionCode;

            Log.e("call","version name = "+versionName);
            Log.e("call","version code = "+versionCode);

            PackageInfo info = getPackageManager().getPackageInfo("com.Ticktoc", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures)
            {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("Splash KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        }
        catch (Exception e)
        {
            Log.e("call","Splash Activity Exception = "+e.getMessage());
        }

        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_CARD_LIST,"",Splash_Activity.this);
        Log.e("call","before getToken()");
        getToken();
        Log.e("call","after getToken()");

        getHeightAndWidth();

        if (Global.isNetworkconn(activity))
        {
            checkUpdate();
        }
        else
        {
            InternetDialog internetDialog = new InternetDialog(activity);
            internetDialog.showDialog("Please check your internet connection!");
        }
    }

    public void getHeightAndWidth()
    {
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;

        if (width > 5)
        {

            Log.e("abcd", "width width width : " + width + "height height height : " + height);
            Constants.DEVICE_WIDTH = width;
            Constants.DEVICE_HEIGHT = height;
        }
        else {
            Display display = getWindowManager().getDefaultDisplay();
            Method mGetRawH = null;
            try {
                mGetRawH = Display.class.getMethod("getRawHeight");
                Method mGetRawW = Display.class.getMethod("getRawWidth");
                width = (Integer) mGetRawW.invoke(display);
                height = (Integer) mGetRawH.invoke(display);

                Log.e("abcd", "width width width 222 ;" + width + "height height height2222 : " + height);
                Constants.DEVICE_WIDTH = width;
                Constants.DEVICE_HEIGHT = height;
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    public void getToken()
    {
        Log.e("call","getToken()");
        new Thread(new Runnable()
        {
            public void run()
            {

                try
                {
                    FirebaseInstanceId.getInstance().getInstanceId()
                            .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                                @Override
                                public void onComplete(@NonNull Task<InstanceIdResult> task) {

                                    try
                                    {
                                        String token = task.getResult().getToken();
                                        Log.e(TAG, "getTokan() token:- " + token);

                                        SharedPreferences pref = getSharedPreferences("pref", MODE_PRIVATE);

                                        SharedPreferences.Editor editor = pref.edit();
                                        editor.putString("token", token);
                                        editor.commit();

                                        FCMUtil.saveToken(activity, token);

                                        Log.e(TAG, "getTokan() FCMUtil token:- " + FCMUtil.getFcmToken(activity));
                                    }
                                    catch (Exception e)
                                    {
                                        Log.e(TAG, "getTokan() Exception:- " + e.getMessage());
                                    }

                                }
                            });
                }
                catch (Exception e)
                {
                    Log.v("TAG", "Failed to complete token refresh", e);
                }
            }
        }).start();
    }

    private void checkUpdate()
    {
        dialogClass.showDialog();
        String url = WebServiceAPI.API_CHECK_VERSION + versionName + "/AndroidPassenger";

        Log.e(TAG, "url = " + url);

        aQuery.ajax(url.trim(), null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e(TAG,"responseCode = " + responseCode);
                    Log.e(TAG,"Response = " + json);

                    SessionSave.saveUserSession(Common.INIT_RESPONSE,json.toString(),activity);

                    if (json!=null && !json.toString().equalsIgnoreCase("null"))
                    {
                        SessionSave.saveInit(Common.INIT_DATA,json.toString(),activity);
                        if(json.has("car_class"))
                        {
                            JSONArray car_class = json.getJSONArray("car_class");
                            if (car_class!=null && car_class.length()>0)
                            {
                                Log.e("call","car_class.size = "+car_class.length());
                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_CAR_CLASS,car_class.toString(),activity);
                                dialogClass.hideDialog();
                            }
                        }
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                dialogClass.hideDialog();
                                String message = "Please try again later";
                                if (json.has("message"))
                                {
                                    message = json.getString("message");
                                    boolean bool_status = false;
                                    boolean bool_maintenance= false;

                                    if (json.has("update"))
                                    {
                                        bool_status = json.getBoolean("update");
                                    }

                                    if (json.has("maintenance"))
                                    {
                                        bool_maintenance = json.getBoolean("maintenance");
                                    }

                                    showDialog(message,bool_status,bool_maintenance);
                                }
                                else
                                {
                                    init();
                                }
                            }
                            else
                            {
                                String message = "Please try again later";
                                if (json.has("message"))
                                {
                                    message = json.getString("message");
                                }

                                boolean bool_status = false;
                                boolean bool_maintenance = false;

                                if (json.has("update"))
                                {
                                    bool_status = json.getBoolean("update");
                                }

                                if (json.has("maintenance"))
                                {
                                    bool_maintenance = json.getBoolean("maintenance");
                                }

                                dialogClass.hideDialog();
                                showDialog(message,bool_status,bool_maintenance);
                            }
                        }
                        else
                        {
                            String message = "Please try again later";
                            if (json.has("message"))
                            {
                                message = json.getString("message");
                            }
                            dialogClass.hideDialog();
                            InternetDialog internetDialog = new InternetDialog(activity);
                            internetDialog.showDialog(message);
                        }
                    }
                    else
                    {
                        String message = "Please try again later";
                        if (json.has("message"))
                        {
                            message = json.getString("message");
                        }
                        dialogClass.hideDialog();
                        InternetDialog internetDialog = new InternetDialog(activity);
                        internetDialog.showDialog(message);
                    }
                }
                catch (Exception e)
                {
                    Log.e("Exception","Exception "+e.toString());
                    dialogClass.hideDialog();
                    InternetDialog internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog("Something went wrong prlease try again later");
                }
            }

        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    public void showDialog(String message, final boolean update, final boolean maintenance)
    {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.tick_update_dialog);

        TextView tv_update= (TextView) dialog.findViewById(R.id.update);
        TextView tv_later= (TextView) dialog.findViewById(R.id.later);
        TextView tv_Message = (TextView) dialog.findViewById(R.id.message);
        LinearLayout ll_update = (LinearLayout) dialog.findViewById(R.id.update_layout);
        LinearLayout ll_later = (LinearLayout) dialog.findViewById(R.id.later_layout);

        tv_Message.setText(message);

        if (update==true)
        {
            ll_later.setVisibility(View.GONE);
            tv_update.setText("Update");
        }
        else
        {
            if (maintenance==true)
            {
                tv_update.setText("Ok");
                ll_later.setVisibility(View.GONE);
            }
            else
            {
                tv_update.setText("Update");
                ll_later.setVisibility(View.VISIBLE);
            }
        }

        tv_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (maintenance==true)
                {
                    dialog.dismiss();
                    finish();
                }
                else
                {
                    gotoPlayStore();
                }
            }
        });

        ll_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (maintenance==true)
                {
                    dialog.dismiss();
                    finish();
                }
                else
                {
                    gotoPlayStore();
                }
            }
        });

        tv_later.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                init();
            }
        });

        ll_later.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                init();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        dialog.show();
    }

    public void gotoPlayStore()
    {
        final String appPackageName = getPackageName();
        try
        {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        }
        catch (android.content.ActivityNotFoundException anfe)
        {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }
    private void init() {

        handler = new Handler();
        runnable = new Runnable() {

            @Override
            public void run() {
                try
                {

                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                finally
                {
                    startNextActivity();
                }
            }
        };
        handler.postDelayed(runnable, SPLASH_DISPLAY_LENGTH);

    }

    private void startNextActivity()
    {
        Log.e("TAG","11111111111111111 = "+SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_STATUS,activity));
        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_STATUS,activity)!=null &&
                !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_STATUS,activity).equals("") &&
                SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_STATUS,activity).equals("1"))
        {
            Intent intent = new Intent(this,MainActivity.class);
            intent.putExtra("from","Splash_Activity");
            startActivity(intent);
            finish();
        }
        else
        {
            Intent loginScreen = new Intent(Splash_Activity.this,LoginActivity.class);
            startActivity(loginScreen);
            finish();
            overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
        }
    }
}


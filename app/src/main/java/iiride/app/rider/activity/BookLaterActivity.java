package iiride.app.rider.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;

import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.textfield.TextInputLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;


import iiride.app.rider.R;
import iiride.app.rider.application.TicktocApplication;
import iiride.app.rider.been.CreditCard_List_Been;
import iiride.app.rider.comman.Common;
import iiride.app.rider.comman.Constants;
import iiride.app.rider.comman.SessionSave;
import iiride.app.rider.comman.TaxiUtil;
import iiride.app.rider.comman.Utility;
import iiride.app.rider.comman.WebServiceAPI;
import iiride.app.rider.other.DialogClass;
import iiride.app.rider.other.GPSTracker;
import iiride.app.rider.other.Global;
import iiride.app.rider.other.InternetDialog;
import iiride.app.rider.view.CustomSpinner;
import iiride.app.rider.view.MySnackBar;
import com.squareup.picasso.Picasso;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.chainsaw.Main;
import org.json.JSONArray;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class BookLaterActivity extends AppCompatActivity implements View.OnClickListener{

    public static BookLaterActivity activity;
    private LinearLayout ll_Back;
    private ImageView iv_Back;
    private TextView tv_Title;

    private CheckBox checkBox_Myself,checkBox_Others,checkBox_Flight,checkBox_Notes,checkBoxBabySeat;
    public static int mySelfOtherFlag = 0; //0 = myself, 1=others
    public static boolean flight = false; //not applicable.
    public static boolean notes = false; //

    private TextInputLayout til_FlightNumber,til_Notes;
    private EditText et_Name,et_Phone,et_Flight,et_Notes;
    private TextView tv_PickupTime,tv_Submit;
    private LinearLayout ll_Submit;

    private AutoCompleteTextView auto_Pickup,auto_Dropoff;
    private TextView tvPickup,tvDropoff;

    private String jsonurl = "";
    private ArrayList<String> names, Place_id_type,secondryAddress;
    private ParseData parse;
    public static JSONObject json;
    private JSONArray contacts = null;
    private static final String TAG_RESULT = "predictions";
    private String place_id="";
    private String pickUpSubArb ="",dropOffSubAArb = "";

    public static int addFlag = 1;

    public String pickupLocation = "";
    public String dropoffLocation = "";
    public double pickup_Lat,pickup_Long;
    public double dropoff_Lat,dropoff_Long;
    public String pickup_Date = "";
    public String pickup_Time = "";

    private ImageView iv_DateTime;
    private LinearLayout ll_DateTime,llDateAndTimeMain;
    private TextView dateTimetxt,tv_ModelName,tv_Promocode;
    private ImageView iv_ModelImage;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private DialogClass dialogClass;
    private AQuery aQuery;
    public static String dateTime = "";
    private LinearLayout ll_ClearPickup,ll_ClearDropoff,ll_Promocode;
    private LinearLayout ll_RootLayout;
    private MySnackBar mySnackBar;
//    private Spinner spinnerCompany;
//    private ArrayList<String> companyNameList = new ArrayList<String>();
//    public static int selectedCompanyNamePos = 0;
    private boolean pickup_Touch = false;
    private boolean dropoff_Touch = false;
    private GPSTracker gpsTracker;
    private String promocodeStr = "";
    private double latitude = 0;
    private double longintude = 0;
    private CustomSpinner Spinner_paymentMethod;
    public static String cardId="";
    public static String cardNumber="";
    public static String cardType="";
    public static boolean isCardSelected = false;
    public static String selectedPaymentType = "";
    private CustomerAdapter customerAdapter;
    public static int spinnerSelectedPosition = 0;
    public static int pressNo = 0;
    private RelativeLayout ll_Spinner;

    private int addressFlag = 0;
    private LinearLayout ll_add_payment_method,llMySelfOtherMain,llPassengerInfo,llFlightNumberMain;

    private boolean isBabySeatInclude = false;

    int PICKUP_AUTOCOMPLETE_REQUEST_CODE = 201;
    int DROPOFF_AUTOCOMPLETE_REQUEST_CODE = 202;
    List<Place.Field> fields;
    private String from = "", modelId="",editName="",editNumber="",passengerType="",bookingType="",PromoCode="";
    private String PickupDateTime="",CardId="",FlightNumber="",Notes="",BabySeat="",BookingId="";
    private String PickupLat="",PickupLng="",DropoffLat="",DropoffLon="";
    private int editSelectedCar=-1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_later);
        pressNo = 0;
        activity = BookLaterActivity.this;
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        cardId="";
        cardNumber="";
        cardType="";
        mySelfOtherFlag = 0;
        spinnerSelectedPosition = 0;
        flight = false;
        addressFlag = 0;
        selectedPaymentType = "";
        isCardSelected = false;
        notes = false;
        addFlag = 1;
        pickupLocation = "";
        dropoffLocation = "";
        pickup_Touch = false;
        dropoff_Touch = false;
        pickup_Lat=0;
        pickup_Long=0;
        dropoff_Lat=0;
        dropoff_Long=0;
        pickup_Date = "";
        pickup_Time = "";
        dateTime = "";
        promocodeStr = "";
        isBabySeatInclude = false;
        dialogClass = new DialogClass(activity,0);
        aQuery = new AQuery(activity);
        gpsTracker = new GPSTracker(activity);

        fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG);

        if (this.getIntent()!=null && this.getIntent().getStringExtra("from")!=null)
        {
            if (this.getIntent().getStringExtra("from")!=null)
            {
                from = this.getIntent().getStringExtra("from");
            }
            else
            {
                from = "";
            }

            if (this.getIntent().getStringExtra("modelId")!=null)
            {
                modelId = this.getIntent().getStringExtra("modelId");
            }
            else
            {
                modelId = "";
            }

            if (this.getIntent().getStringExtra("PassengerType")!=null)
            {
                passengerType = this.getIntent().getStringExtra("PassengerType");
                if (passengerType.equalsIgnoreCase("myself"))
                {
                    mySelfOtherFlag = 0;
                }
                else
                {
                    mySelfOtherFlag = 1;
                }
            }
            else
            {
                mySelfOtherFlag = 0;
            }

            if (this.getIntent().getStringExtra("editName")!=null)
            {
                editName = this.getIntent().getStringExtra("editName");
            }
            else
            {
                editName = "";
            }

            if (this.getIntent().getStringExtra("editNumber")!=null)
            {
                editNumber = this.getIntent().getStringExtra("editNumber");
            }
            else
            {
                editNumber = "";
            }

            if (this.getIntent().getStringExtra("pickup")!=null)
            {
                TaxiUtil.picup_Address = this.getIntent().getStringExtra("pickup");
            }

            if (this.getIntent().getStringExtra("dropoff")!=null)
            {
                TaxiUtil.dropoff_Address = this.getIntent().getStringExtra("dropoff");
            }

            if (this.getIntent().getStringExtra("bookingType")!=null)
            {
                bookingType = this.getIntent().getStringExtra("bookingType");//Book Later, Book Now
            }

            if (bookingType!=null && bookingType.equalsIgnoreCase("Book Now"))
            {
                mySelfOtherFlag = 0;
            }

            if (this.getIntent().getStringExtra("PromoCode")!=null)
            {
                PromoCode = this.getIntent().getStringExtra("PromoCode");
            }

            if (this.getIntent().getStringExtra("PickupDateTime")!=null)
            {
                PickupDateTime = this.getIntent().getStringExtra("PickupDateTime");
            }

            if (this.getIntent().getStringExtra("CardId")!=null)
            {
                CardId = this.getIntent().getStringExtra("CardId");
            }

            if (this.getIntent().getStringExtra("FlightNumber")!=null)
            {
                FlightNumber = this.getIntent().getStringExtra("FlightNumber");
            }

            if (this.getIntent().getStringExtra("Notes")!=null)
            {
                Notes = this.getIntent().getStringExtra("Notes");
            }

            if (this.getIntent().getStringExtra("BabySeat")!=null)
            {
                BabySeat = this.getIntent().getStringExtra("BabySeat");
            }

            if (this.getIntent().getStringExtra("BookingId")!=null)
            {
                BookingId = this.getIntent().getStringExtra("BookingId");
            }

            if (this.getIntent().getStringExtra("PickupLat")!=null)
            {
                PickupLat = this.getIntent().getStringExtra("PickupLat");
                if (PickupLat!=null && !PickupLat.trim().equalsIgnoreCase(""))
                {
                    pickup_Lat = Double.parseDouble(PickupLat);
                }
            }

            if (this.getIntent().getStringExtra("PickupLng")!=null)
            {
                PickupLng = this.getIntent().getStringExtra("PickupLng");
                if (PickupLng!=null && !PickupLng.trim().equalsIgnoreCase(""))
                {
                    pickup_Long = Double.parseDouble(PickupLng);
                }
            }

            if (this.getIntent().getStringExtra("DropoffLat")!=null)
            {
                DropoffLat = this.getIntent().getStringExtra("DropoffLat");
                if (DropoffLat!=null && !DropoffLat.trim().equalsIgnoreCase(""))
                {
                    dropoff_Lat = Double.parseDouble(DropoffLat);
                }
            }

            if (this.getIntent().getStringExtra("DropoffLon")!=null)
            {
                DropoffLon = this.getIntent().getStringExtra("DropoffLon");
                if (DropoffLon!=null && !DropoffLon.trim().equalsIgnoreCase(""))
                {
                    dropoff_Long = Double.parseDouble(DropoffLon);
                }
            }

            if (BabySeat!=null && BabySeat.equalsIgnoreCase("1"))
            {
                isBabySeatInclude = true;
            }
            else
            {
                isBabySeatInclude = false;
            }

            if (Notes!=null && !Notes.equalsIgnoreCase(""))
            {
                notes=true;
            }
            else
            {
                notes=false;
            }

            if (FlightNumber!=null && !FlightNumber.equalsIgnoreCase(""))
            {
                flight=true;
            }
            else
            {
                flight=false;
            }
        }
        else
        {
            from = "";
            modelId = "";
            editName = "";
            editNumber = "";
        }

        init();
    }



    @Override
    protected void onPause() {
        Log.i("Life Cycle", "onPause");
        if (Spinner_paymentMethod!=null)
        {
            Spinner_paymentMethod.onDetachedFromWindow();
        }
        super.onPause();
    }

    private void init() {

        pickUpSubArb = getIntent().getStringExtra("pickUpSubArb");
        dropOffSubAArb = getIntent().getStringExtra("dropOffSubAArb");

        ll_Back = (LinearLayout) findViewById(R.id.back_layout);
        iv_Back = (ImageView) findViewById(R.id.back_imageview);
        tv_Title = (TextView) findViewById(R.id.title_textview);
        tv_Promocode = (TextView) findViewById(R.id.promo_code_textview);
        tv_Title.setText("Book Later");
        Spinner_paymentMethod = (CustomSpinner) findViewById(R.id.Spinner_paymentMethod);
        ll_ClearPickup = (LinearLayout) findViewById(R.id.clear_pickup_location);
        ll_ClearDropoff = (LinearLayout) findViewById(R.id.clear_dropoff_location);
        ll_Promocode = (LinearLayout) findViewById(R.id.promo_code_layout);
        ll_add_payment_method = findViewById(R.id.ll_add_payment_method);
        llMySelfOtherMain = findViewById(R.id.llMySelfOtherMain);
        llPassengerInfo = findViewById(R.id.llPassengerInfo);
        llFlightNumberMain = findViewById(R.id.llFlightNumberMain);

        dateTimetxt = (TextView) findViewById(R.id.date_time_textview);
        ll_Spinner =  findViewById(R.id.select_card_layout);

        ll_RootLayout = (LinearLayout) findViewById(R.id.rootView);
        tv_Submit = (TextView) findViewById(R.id.submit_textview);
        ll_Submit = (LinearLayout) findViewById(R.id.submit_layout);

        tv_ModelName = (TextView) findViewById(R.id.model_name);
        iv_ModelImage = (ImageView) findViewById(R.id.model_image);

//        spinnerCompany = (Spinner) findViewById(R.id.spinner_company);

        tv_PickupTime = (TextView) findViewById(R.id.title_textview);
        et_Flight = (EditText) findViewById(R.id.input_flight_number);
        et_Notes = (EditText) findViewById(R.id.input_note);
        et_Phone = (EditText) findViewById(R.id.input_phone);
        et_Name = (EditText) findViewById(R.id.input_name);

        checkBox_Flight = (CheckBox) findViewById(R.id.checkbox_flight);
        checkBox_Notes = (CheckBox) findViewById(R.id.checkbox_notes);
        checkBox_Myself = (CheckBox) findViewById(R.id.checkbox_myself);
        checkBox_Others = (CheckBox) findViewById(R.id.checkbox_others);
        checkBoxBabySeat = (CheckBox) findViewById(R.id.checkBoxBabySeat);

        iv_DateTime = (ImageView) findViewById(R.id.date_time_image);
        ll_DateTime = (LinearLayout) findViewById(R.id.date_time_layout);
        llDateAndTimeMain = (LinearLayout) findViewById(R.id.llDateAndTimeMain);

        til_FlightNumber = (TextInputLayout) findViewById(R.id.input_layout_flight_number);
        til_Notes = (TextInputLayout) findViewById(R.id.input_layout_note);

        auto_Pickup = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView_pickup);
        auto_Dropoff = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView_drop_off);

        tvPickup = findViewById(R.id.tvPickup);
        tvDropoff = findViewById(R.id.tvDropoff);

        mySnackBar = new MySnackBar(activity);


        tvDropoff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields)
//                        .setCountry("AU")
                        .setCountry("IN")
                        .build(activity);
                startActivityForResult(intent, DROPOFF_AUTOCOMPLETE_REQUEST_CODE);
            }
        });

        tvPickup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields)
//                        .setCountry("AU")
                        .setCountry("IN")
                        .build(activity);
                startActivityForResult(intent, PICKUP_AUTOCOMPLETE_REQUEST_CODE);
            }
        });


        checkBox_Others.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checkBox_Others.setChecked(true);
                mySelfOtherFlag = 1;
                checkBox_Myself.setChecked(false);
                setNameNumber(mySelfOtherFlag);
            }
        });

        checkBox_Myself.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checkBox_Others.setChecked(false);
                mySelfOtherFlag = 0;
                checkBox_Myself.setChecked(true);
                setNameNumber(mySelfOtherFlag);
            }
        });

        checkBox_Flight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (flight)
                {
                    til_FlightNumber.setVisibility(View.GONE);
                    flight=false;
                }
                else
                {
                    til_FlightNumber.setVisibility(View.VISIBLE);
                    flight=true;
                }
            }
        });

        checkBox_Notes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (notes)
                {
                    til_Notes.setVisibility(View.GONE);
                    notes=false;
                }
                else
                {
                    til_Notes.setVisibility(View.VISIBLE);
                    notes=true;
                }
            }
        });

        checkBoxBabySeat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isBabySeatInclude = isChecked;
            }
        });

        if(MainActivity.cardListBeens.size() == 0)
        {
            ll_add_payment_method.setVisibility(View.VISIBLE);
        }

        ll_add_payment_method.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Wallet_Add_Cards_Activity.resumeFlag = 0;
                Intent i =  new Intent(activity, Add_Card_In_List_Activity.class);
                startActivity(i);
            }
        });

        if (from!=null && !from.equalsIgnoreCase(""))
        {
            if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG,activity)!=null &&
                    SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG,activity).equalsIgnoreCase("1"))
            {
                tvPickup.setEnabled(true);
                tvPickup.setClickable(true);
            }
            else if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG,activity)!=null &&
                    SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG,activity).equalsIgnoreCase("2"))
            {
                tvPickup.setEnabled(false);
                tvPickup.setClickable(false);
            }
            else
            {
                tvPickup.setEnabled(true);
                tvPickup.setClickable(true);
            }

            if (MainActivity.cardListBeens!=null && MainActivity.cardListBeens.size()>0)
            {
                for (int i=0; i<MainActivity.cardListBeens.size(); i++)
                {
                    if (CardId.equalsIgnoreCase(MainActivity.cardListBeens.get(i).getId()))
                    {
                        spinnerSelectedPosition = i;
                    }
                }
            }
        }
        selectedPaymentType = "card";
        customerAdapter = new CustomerAdapter(activity, MainActivity.cardListBeens);
        Spinner_paymentMethod.setAdapter(customerAdapter);

        Spinner_paymentMethod.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                cardId = MainActivity.cardListBeens.get(position).getId();
                cardNumber = MainActivity.cardListBeens.get(position).getCardNum_();

                /*if (position == (MainActivity.cardListBeens.size() -1))
                {
                    Log.e("call","2222");
                    selectedPaymentType = "cash";//PaymentType : cash,wallet,card

                }
                else
                {
                    Log.e("call","3333");
                    selectedPaymentType = "card";//PaymentType : cash,wallet,card
                }
                selectedPaymentType = "card";*/
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

                Log.e("call","onNothing selected");
            }
        });

        if (from!=null && !from.equalsIgnoreCase(""))
        {
            Spinner_paymentMethod.setSelection(spinnerSelectedPosition);
        }

        auto_Pickup.addTextChangedListener(new TextWatcher() {
            String search_text[];

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (addressFlag==0)
                {
                    addFlag = 1;
                    if (gpsTracker!=null && gpsTracker.canGetLocation())
                    {
                        latitude = gpsTracker.getLatitude();
                        longintude = gpsTracker.getLongitude();
                    }
                    search_text = auto_Pickup.getText().toString().split(",");
                    //jsonurl = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=" + search_text[0].replace(" ", "%20") + "&location=" +latitude+","+longintude + "&radius=1000&sensor=true&key="+Constants.GOOGLE_API_KEY+"&components=&language=en";
                    jsonurl = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=" + search_text[0].replace(" ", "%20") + "&location=" +latitude+","+longintude+"&radius=1000&sensor=true&key="+ Constants.GOOGLE_API_KEY+"&components=country:au&language=en";
                    Log.e("url","autocomplete = "+jsonurl);
                    names = new ArrayList<String>();
                    Place_id_type = new ArrayList<String>();
                    secondryAddress = new ArrayList<String>();
                    if (parse != null) {
                        parse.cancel(true);
                        parse = null;
                    }
                    parse = new ParseData();
                    parse.execute();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        auto_Pickup.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (event != null || actionId == EditorInfo.IME_ACTION_DONE) {
                    if (auto_Pickup.getText().toString().trim().length() > 0) {
//                        getLocationFromAddress(auto_Pickup.getText().toString());
                    }
                    else
                    {
                        pickup_Lat = 0;
                        pickup_Long = 0;
                        pickupLocation = "";
                    }
                    // finish();
                }
                return false;
            }
        });

        auto_Dropoff.addTextChangedListener(new TextWatcher() {
            String search_text[];

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (addressFlag==0)
                {
                    addFlag = 2;
                    if (gpsTracker!=null && gpsTracker.canGetLocation())
                    {
                        latitude = gpsTracker.getLatitude();
                        longintude = gpsTracker.getLongitude();
                    }
                    search_text = auto_Dropoff.getText().toString().split(",");
                    jsonurl = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=" + search_text[0].replace(" ", "%20") +"&location=" +latitude+","+longintude+ "&radius=1000&sensor=true&key="+Constants.GOOGLE_API_KEY+"&components=country:au&language=en";
                    //jsonurl = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=" + search_text[0].replace(" ", "%20") + "&location=" +latitude+","+longintude + "&radius=1000&sensor=true&key="+Constants.GOOGLE_API_KEY+"&components=&language=en";
                    Log.e("url","autocomplete = "+jsonurl);
                    names = new ArrayList<String>();
                    Place_id_type = new ArrayList<String>();
                    secondryAddress = new ArrayList<String>();
                    if (parse != null) {
                        parse.cancel(true);
                        parse = null;
                    }
                    parse = new ParseData();
                    parse.execute();
                }
                else
                {
                    addressFlag = 0;
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        auto_Dropoff.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (event != null || actionId == EditorInfo.IME_ACTION_DONE) {
                    if (auto_Dropoff.getText().toString().trim().length() > 0) {
//                        getLocationFromAddress(auto_Dropoff.getText().toString());
                    } else {
                        dropoff_Lat = 0;
                        dropoff_Long = 0;
                        dropoffLocation= "";
                    }
                    // finish();
                }
                return false;
            }
        });

        ll_Back.setOnClickListener(activity);
        iv_Back.setOnClickListener(activity);
        tv_Submit.setOnClickListener(activity);
        ll_Submit.setOnClickListener(activity);
        iv_DateTime.setOnClickListener(activity);
        ll_DateTime.setOnClickListener(activity);
        ll_ClearDropoff.setOnClickListener(activity);
        ll_ClearPickup.setOnClickListener(activity);
        ll_Promocode.setOnClickListener(activity);
        tv_Promocode.setOnClickListener(activity);

        if (from!=null && !from.trim().equalsIgnoreCase(""))
        {
            for (int i=0; i<MainActivity.carType_beens.size(); i++)
            {
                if (modelId.equalsIgnoreCase(MainActivity.carType_beens.get(i).getId()))
                {
                    editSelectedCar = i;
                }
            }

            auto_Dropoff.setText("");
            dropoffLocation = "";
            dropoff_Touch =true;
            auto_Dropoff.dismissDropDown();

            Log.e("call","modelId = "+MainActivity.carType_beens.get(editSelectedCar).getId());
            if (MainActivity.carType_beens.get(editSelectedCar).getImage()!=null && !MainActivity.carType_beens.get(editSelectedCar).getImage().equalsIgnoreCase(""))
            {
                Picasso.with(activity).load(MainActivity.carType_beens.get(editSelectedCar).getImage()).into(iv_ModelImage);
            }
            else
            {
                iv_ModelImage.setImageResource(R.drawable.car_image);
            }

            if (MainActivity.carType_beens.get(editSelectedCar).getName()!=null && !MainActivity.carType_beens.get(editSelectedCar).getName().equalsIgnoreCase(""))
            {
                tv_ModelName.setText(MainActivity.carType_beens.get(editSelectedCar).getName());
            }
            else
            {
                tv_ModelName.setText("");
            }

            if (bookingType.equalsIgnoreCase("Book Later"))
            {
                if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG,activity)!=null &&
                        SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG,activity).equalsIgnoreCase("1"))
                {
                    llDateAndTimeMain.setVisibility(View.VISIBLE);
                    llMySelfOtherMain.setVisibility(View.VISIBLE);
                    llPassengerInfo.setVisibility(View.VISIBLE);
                    llFlightNumberMain.setVisibility(View.VISIBLE);
                    et_Phone.setText(editNumber+"");
                    et_Name.setText(editName+"");
                    if (PickupDateTime.contains(" "))
                    {
                        String[] dateTimeArray=PickupDateTime.split(" ");

                        if (dateTimeArray!=null && dateTimeArray.length==3)
                        {
                            String datetimeStr = dateTimeArray[2]+" "+dateTimeArray[0]+" "+dateTimeArray[1];
                            dateTimetxt.setText(datetimeStr);

                            //03:16 PM 15/05/2021
                            String dateArray[] = null;
                            String timeArray[] = null;
                            if (dateTimeArray[2]!=null && dateTimeArray[2].contains("/"))
                            {
                                dateArray = dateTimeArray[2].split("/");
                            }
                            if (dateTimeArray[0]!=null && dateTimeArray[0].contains(":"))
                            {
                                timeArray = dateTimeArray[0].split(":");
                            }

                            if (dateArray!=null && timeArray!=null && dateArray.length==3 && timeArray.length==2)
                            {
                                pickup_Date = dateArray[2] + "-" + dateArray[1] + "-" + dateArray[0];
                                pickup_Time = timeArray[0] + "-" + timeArray[1] + "-00";
                                dateTime = pickup_Date + " " + pickup_Time;
                            }
                        }
                    }
                }
                else if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG,activity)!=null &&
                        SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG,activity).equalsIgnoreCase("2"))
                {
                    llDateAndTimeMain.setVisibility(View.GONE);
                    llMySelfOtherMain.setVisibility(View.GONE);
                    llPassengerInfo.setVisibility(View.GONE);
                    llFlightNumberMain.setVisibility(View.GONE);
                }
                else
                {
                    llDateAndTimeMain.setVisibility(View.VISIBLE);
                    llMySelfOtherMain.setVisibility(View.VISIBLE);
                    llPassengerInfo.setVisibility(View.VISIBLE);
                    llFlightNumberMain.setVisibility(View.VISIBLE);
                    et_Phone.setText(editNumber+"");
                    et_Name.setText(editName+"");
                    if (PickupDateTime.contains(" "))
                    {
                        String[] dateTimeArray=PickupDateTime.split(" ");

                        if (dateTimeArray!=null && dateTimeArray.length==3)
                        {
                            String datetimeStr = dateTimeArray[2]+" "+dateTimeArray[0]+" "+dateTimeArray[1];
                            dateTimetxt.setText(datetimeStr);

                            //03:16 PM 15/05/2021
                            String dateArray[] = null;
                            String timeArray[] = null;
                            if (dateTimeArray[2]!=null && dateTimeArray[2].contains("/"))
                            {
                                dateArray = dateTimeArray[2].split("/");
                            }
                            if (dateTimeArray[0]!=null && dateTimeArray[0].contains(":"))
                            {
                                timeArray = dateTimeArray[0].split(":");
                            }

                            if (dateArray!=null && timeArray!=null && dateArray.length==3 && timeArray.length==2)
                            {
                                pickup_Date = dateArray[2] + "-" + dateArray[1] + "-" + dateArray[0];
                                pickup_Time = timeArray[0] + "-" + timeArray[1] + "-00";
                                dateTime = pickup_Date + " " + pickup_Time;
                            }
                        }
                    }
                }
            }
            else
            {
                llDateAndTimeMain.setVisibility(View.GONE);
                llMySelfOtherMain.setVisibility(View.GONE);
                llPassengerInfo.setVisibility(View.GONE);
                llFlightNumberMain.setVisibility(View.GONE);
            }

            addressFlag = 1;
            tvPickup.setText(TaxiUtil.picup_Address+"");
            pickup_Touch = true;
            dropoff_Touch = true;
            pickupLocation = TaxiUtil.picup_Address+"";
            dropoffLocation = TaxiUtil.dropoff_Address+"";
            tvDropoff.setText(TaxiUtil.dropoff_Address+"");

            if (flight)
            {
                til_FlightNumber.setVisibility(View.VISIBLE);
                et_Flight.setText(FlightNumber);
                checkBox_Flight.setChecked(true);
            }
            else
            {
                til_FlightNumber.setVisibility(View.GONE);
                et_Flight.setText("");
                checkBox_Flight.setChecked(false);
            }

            if (notes)
            {
                til_Notes.setVisibility(View.VISIBLE);
                et_Notes.setText(Notes);
                checkBox_Notes.setChecked(true);
            }
            else
            {
                til_Notes.setVisibility(View.GONE);
                et_Notes.setText("");
                checkBox_Notes.setChecked(false);
            }

            if (isBabySeatInclude)
            {
                checkBoxBabySeat.setChecked(true);
            }
            else
            {
                checkBoxBabySeat.setChecked(false);
            }

            if (mySelfOtherFlag==0)
            {
                checkBox_Myself.setChecked(true);
                checkBox_Others.setChecked(false);
                et_Phone.setEnabled(false);
                et_Name.setEnabled(false);
            }
            else
            {
                checkBox_Myself.setChecked(false);
                checkBox_Others.setChecked(true);
                et_Phone.setEnabled(true);
                et_Name.setEnabled(true);
            }
        }
        else
        {
            if (MainActivity.redirectBooking!=null &&
                    !MainActivity.redirectBooking.equalsIgnoreCase("")
                && MainActivity.redirectBooking.equalsIgnoreCase("booklater"))
            {
                if (MainActivity.redirectBookingAdrs!=null &&
                        !MainActivity.redirectBookingAdrs.equalsIgnoreCase(""))
                {
                    auto_Dropoff.setText(MainActivity.redirectBookingAdrs);
                    dropoffLocation = MainActivity.redirectBookingAdrs;
                    dropoff_Touch =true;
                    auto_Dropoff.dismissDropDown();

                    MainActivity.redirectBooking = "";
                    MainActivity.redirectBookingAdrs = "";
                }
                else
                {
                    dropoff_Touch =false;
                    auto_Dropoff.setText("");
                    MainActivity.redirectBooking = "";
                    MainActivity.redirectBookingAdrs = "";
                }
            }
            else
            {
                dropoff_Touch =false;
                auto_Dropoff.setText("");
                MainActivity.redirectBooking = "";
                MainActivity.redirectBookingAdrs = "";
            }

            Log.e("call","modelId = "+MainActivity.carType_beens.get(MainActivity.selectedCar).getId());
            if (MainActivity.carType_beens.get(MainActivity.selectedCar).getImage()!=null && !MainActivity.carType_beens.get(MainActivity.selectedCar).getImage().equalsIgnoreCase(""))
            {
                Picasso.with(activity).load(MainActivity.carType_beens.get(MainActivity.selectedCar).getImage()).into(iv_ModelImage);
            }
            else
            {
                iv_ModelImage.setImageResource(R.drawable.car_image);
            }

            if (MainActivity.carType_beens.get(MainActivity.selectedCar).getName()!=null && !MainActivity.carType_beens.get(MainActivity.selectedCar).getName().equalsIgnoreCase(""))
            {
                tv_ModelName.setText(MainActivity.carType_beens.get(MainActivity.selectedCar).getName());
            }
            else
            {
                tv_ModelName.setText("");
            }

            setNameNumber(mySelfOtherFlag);

            if (MainActivity.isBookLaterActivate)
            {
                addressFlag = 1;
                tvPickup.setText(TaxiUtil.picup_Address+"");
                pickup_Touch = true;
                dropoff_Touch = true;
                pickupLocation = TaxiUtil.picup_Address+"";
                dropoffLocation = TaxiUtil.dropoff_Address+"";
                tvDropoff.setText(TaxiUtil.dropoff_Address+"");
            }
            else
            {
                getAddressFromLatLong();
            }
        }
    }

//    public void showCreaditCardPopup()
//    {
//        final Dialog dialog = new Dialog(activity);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setCancelable(false);
//        dialog.setContentView(R.layout.yes_no_dialog_layout);
//
//        TextView tv_Message = (TextView) dialog.findViewById(R.id.message);
//
//        TextView tv_Yes = (TextView) dialog.findViewById(R.id.yes);
//        TextView tv_No = (TextView) dialog.findViewById(R.id.no);
//
//        LinearLayout ll_Yes = (LinearLayout) dialog.findViewById(R.id.yes_layout);
//        LinearLayout ll_No = (LinearLayout) dialog.findViewById(R.id.no_layout);
//
//        tv_Message.setText("Do you want to add card?");
//
//        ll_No.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                dialog.dismiss();
//                pressNo = 1;
//                openPromocodePopup();
//            }
//        });
//
//        tv_No.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                dialog.dismiss();
//                pressNo = 1;
//                openPromocodePopup();
//            }
//        });
//
//        ll_Yes.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                dialog.dismiss();
//                Intent intent = new Intent(activity,Add_Card_In_List_Activity.class);
//                startActivity(intent);
//                overridePendingTransition(R.anim.slide_up,R.anim.no_change);
//
//            }
//        });
//
//        tv_Yes.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                dialog.dismiss();
//                Intent intent = new Intent(activity,Add_Card_In_List_Activity.class);
//                startActivity(intent);
//                overridePendingTransition(R.anim.slide_up,R.anim.no_change);
//
//            }
//        });
//
//        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//        lp.copyFrom(dialog.getWindow().getAttributes());
//        //lp.width = Comman.DEVICE_WIDTH;
//        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
//        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
//        lp.gravity = Gravity.CENTER;
//
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//        dialog.getWindow().setAttributes(lp);
//
//        dialog.show();
//    }

    public boolean checkCardList()
    {
        try
        {
            Log.e("call","111111111111111");
            String strCardList = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_CARD_LIST,activity);
            List<CreditCard_List_Been> cardListBeens = new ArrayList<CreditCard_List_Been>();

            if (strCardList!=null && !strCardList.equalsIgnoreCase(""))
            {
                Log.e("call","222222222222222");
                JSONObject json = new JSONObject(strCardList);

                if (json!=null)
                {
                    Log.e("call","33333333333333");
                    if (json.has("cards"))
                    {
                        Log.e("call","6666666666666666");
                        JSONArray cards = json.getJSONArray("cards");

                        if (cards!=null && cards.length()>0)
                        {
                            return true;
                        }
                        else
                        {
                            Log.e("call","no cards available");
                            return false;
                        }
                    }
                    else
                    {
                        Log.e("call","no cards found");
                        return false;
                    }
                }
                else
                {
                    Log.e("call","json null");
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        catch (Exception e)
        {
            Log.e("call","Exception in getting card list = "+e.getMessage());
            return false;
        }
    }

    public void getAddressFromLatLong()
    {
        try
        {
            gpsTracker = new GPSTracker(activity);

            if (gpsTracker!=null && gpsTracker.canGetLocation())
            {
                Geocoder geocoder;
                List<Address> addresses;
                geocoder = new Geocoder(this, Locale.getDefault());

                double latitude = gpsTracker.getLatitude();
                double longitude = gpsTracker.getLongitude();

                if (latitude!=0 && longitude!=0)
                {
                    addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

                    String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    String city = addresses.get(0).getLocality();
                    String state = addresses.get(0).getAdminArea();
                    String country = addresses.get(0).getCountryName();
                    String postalCode = addresses.get(0).getPostalCode();
                    String knownName = addresses.get(0).getFeatureName();

                    Log.e("111111111111","address = "+address);
                    Log.e("111111111111","city = "+city);
                    Log.e("111111111111","state = "+state);
                    Log.e("111111111111","country = "+country);
                    Log.e("111111111111","postalCode = "+postalCode);
                    Log.e("111111111111","knownName = "+knownName);

                    auto_Pickup.setText(address);
                    pickupLocation = address;
                    pickup_Touch = true;
                    pickup_Lat = latitude;
                    pickup_Long = longitude;
                    auto_Pickup.dismissDropDown();

                    auto_Dropoff.setFocusableInTouchMode(true);
                    auto_Dropoff.requestFocus();
                }
            }
        }
        catch (Exception e)
        {
            Log.e("call","Exception = "+e.getMessage());
        }
    }

    public void setNameNumber(int flag)
    {
        if (from!=null && !from.equalsIgnoreCase(""))
        {
            if (flag==0)
            {
                et_Phone.setText(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_PHONE_NUMBER,activity));
                et_Name.setText(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_FULL_NAME,activity));
                et_Name.setEnabled(false);
                et_Phone.setEnabled(false);
            }
            else
            {
                et_Phone.setText(editNumber+"");
                et_Name.setText(editName+"");
                et_Name.setEnabled(true);
                et_Phone.setEnabled(true);
            }
        }
        else
        {
            if (flag==0)
            {
                et_Phone.setText(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_PHONE_NUMBER,activity));
                et_Name.setText(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_FULL_NAME,activity));
                et_Name.setEnabled(false);
                et_Phone.setEnabled(false);
            }
            else
            {
                et_Phone.setText("");
                et_Name.setText("");
                et_Name.setEnabled(true);
                et_Phone.setEnabled(true);
            }
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.back_layout:
                onBackPressed();
                break;

            case R.id.back_imageview:
                onBackPressed();
                break;

            case R.id.submit_layout:
                call_CheckValidation();
                break;

            case R.id.submit_textview:
                call_CheckValidation();
                break;

            case R.id.date_time_image:
                selectDateAndTime();
                break;

            case R.id.date_time_layout:
                selectDateAndTime();
                break;

            case R.id.promo_code_layout:
                openPromocodePopup();
                break;

            case R.id.promo_code_textview:
                openPromocodePopup();
                break;

            case R.id.clear_pickup_location:
                pickupLocation="";
                auto_Pickup.setText("");
                pickup_Touch = false;
                auto_Pickup.setFocusableInTouchMode(true);
                auto_Pickup.requestFocus();
                break;

            case R.id.clear_dropoff_location:
                dropoffLocation="";
                auto_Dropoff.setText("");
                dropoff_Touch = false;

                if (auto_Pickup.getText().toString().trim().length()>0)
                {
                    auto_Dropoff.setFocusableInTouchMode(true);
                    auto_Dropoff.requestFocus();
                }
                else
                {
                    auto_Pickup.setFocusableInTouchMode(true);
                    auto_Pickup.requestFocus();
                }
                break;
        }
    }

    @Override
    public void onBackPressed()
    {
        MainActivity.selectedCar = -1;
        MainActivity.myLocation = true;
        super.onBackPressed();
        finish();
    }

    public void openPromocodePopup()
    {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.promocode_layout);

        TextView tv_Apply = (TextView) dialog.findViewById(R.id.apply_textview);
        TextView tv_Cancel = (TextView) dialog.findViewById(R.id.cancel_textview);

        final EditText et_Promocode = (EditText) dialog.findViewById(R.id.promocode_edittext);
        final TextInputLayout til_Promocode = (TextInputLayout) dialog.findViewById(R.id.promocode_input_layout);

        et_Promocode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (charSequence.toString().trim().length()>0)
                {
                    til_Promocode.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        tv_Apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (et_Promocode.getText().toString().trim().length()>0)
                {
                    tv_Promocode.setText(et_Promocode.getText().toString());
                    promocodeStr = et_Promocode.getText().toString();
                    dialog.dismiss();
                }
                else
                {
                    til_Promocode.setError("Please enter promocode");
                    promocodeStr = "";
                }
            }
        });

        tv_Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                promocodeStr = "";
                dialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        dialog.show();
    }

    // Parse the google API result and loaded into list
    public class ParseData extends AsyncTask<Void, Integer, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            // TODO Auto-generated method stub
            try
            {
                json = GetAddress(jsonurl.toString());
                if (json != null)
                {
                    names.clear();
                    Place_id_type.clear();
                    secondryAddress.clear();
                    // Getting Array of Contacts
                    contacts = json.getJSONArray(TAG_RESULT);
                    for (int i = 0; i < contacts.length(); i++)
                    {
                        JSONObject c = contacts.getJSONObject(i);
                        String description = c.getString("description");
                        String plc_id = c.getString("place_id");
                        Log.d("description", description);
                        names.add(description);
                        Place_id_type.add(plc_id);
                        String secondAddress="";
                        if(c.has("structured_formatting"))
                        {
                            secondAddress = c.getJSONObject("structured_formatting").optString("secondary_text");
                        }
                        secondryAddress.add(secondAddress);
                    }
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result)
        {
            if (addFlag==1)
            {
                Log.e("names", "" + names);
                try
                {
                    ArrayAdapter<String> adp;
                    adp = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, names) {
                        @Override
                        public View getView(final int position, View convertView, ViewGroup parent) {
                            View view = super.getView(position, convertView, parent);
                            view.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                            final TextView text = (TextView) view.findViewById(android.R.id.text1);
                            text.setTextColor(getResources().getColor(R.color.colorBlack));
                            text.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    // TODO Auto-generated method stub
                                    if (Place_id_type.size() > 0)
                                    {
                                        place_id = Place_id_type.get(position);
                                        System.out.println("Place_id_type" + Place_id_type.get(position));
                                    }
                                    if(secondryAddress.size() > 0)
                                    {
                                        pickUpSubArb = secondryAddress.get(position);
                                        Log.e("TAG","pickUpSubArb = "+pickUpSubArb);
                                    }

                                    pickup_Touch = true;
                                    auto_Pickup.setText(text.getText().toString());
                                    getLocationFromAddress(text.getText().toString());
                                    auto_Pickup.dismissDropDown();

                                    if (auto_Dropoff.getText().toString().trim().length()<=0)
                                    {
                                        auto_Dropoff.setFocusableInTouchMode(true);
                                        auto_Dropoff.requestFocus();
                                    }
                                    else if (et_Name.getText().toString().trim().length()<=0)
                                    {
                                        et_Name.setFocusableInTouchMode(true);
                                        et_Name.requestFocus();
                                    }
                                    else if (et_Phone.getText().toString().trim().length()<=0)
                                    {
                                        et_Phone.setFocusableInTouchMode(true);
                                        et_Phone.requestFocus();
                                    }
                                    else if (flight==true)
                                    {
                                        if (et_Flight.getText().toString().trim().length()<=0)
                                        {
                                            et_Flight.setFocusableInTouchMode(true);
                                            et_Flight.requestFocus();
                                        }
                                    }
                                }
                            });
                            return view;
                        }
                    };
                    auto_Pickup.setAdapter(adp);
                    adp.notifyDataSetChanged();
                }
                catch (Exception e)
                {
                    // TODO: handle exception
                }
            }
            else if (addFlag==2)
            {
                Log.e("names", "" + names);
                try
                {
                    ArrayAdapter<String> adp;
                    adp = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, names) {
                        @Override
                        public View getView(final int position, View convertView, ViewGroup parent) {
                            View view = super.getView(position, convertView, parent);
                            view.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                            final TextView text = (TextView) view.findViewById(android.R.id.text1);
                            text.setTextColor(getResources().getColor(R.color.colorBlack));
                            text.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    // TODO Auto-generated method stub
                                    if (Place_id_type.size() > 0)
                                    {
                                        place_id = Place_id_type.get(position);
                                        System.out.println("Place_id_type" + Place_id_type.get(position));
                                    }
                                    if(secondryAddress.size() > 0)
                                    {
                                        dropOffSubAArb = secondryAddress.get(position);
                                        Log.e("TAG","dropOffSubAArb = "+dropOffSubAArb);
                                    }

                                    dropoff_Touch = true;
                                    auto_Dropoff.setText(text.getText().toString());
                                    getLocationFromAddress(text.getText().toString());
                                    auto_Dropoff.dismissDropDown();

                                    if (auto_Pickup.getText().toString().trim().length()<=0)
                                    {
                                        auto_Pickup.setFocusableInTouchMode(true);
                                        auto_Pickup.requestFocus();
                                    }
                                    else if (et_Name.getText().toString().trim().length()<=0)
                                    {
                                        et_Name.setFocusableInTouchMode(true);
                                        et_Name.requestFocus();
                                    }
                                    else if (et_Phone.getText().toString().trim().length()<=0)
                                    {
                                        et_Phone.setFocusableInTouchMode(true);
                                        et_Phone.requestFocus();
                                    }
                                    else if (flight==true)
                                    {
                                        if (et_Flight.getText().toString().trim().length()<=0)
                                        {
                                            et_Flight.setFocusableInTouchMode(true);
                                            et_Flight.requestFocus();
                                        }
                                    }
                                }
                            });
                            return view;
                        }
                    };
                    auto_Dropoff.setAdapter(adp);
                    adp.notifyDataSetChanged();
                }
                catch (Exception e)
                {
                    // TODO: handle exception
                }
            }
        }
    }

    // Get the lat and lng from given address
    public void getLocationFromAddress(String strAddress)
    {
        try
        {
            if (addFlag==1)
            {
                if (auto_Pickup.getText().toString().trim().length() > 0)
                {
                    pickupLocation = auto_Pickup.getText().toString();
                    String encode_url = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + place_id + "&key="+Constants.GOOGLE_API_KEY;
                    Log.e("call","pickupurl = "+encode_url);
                    new GetGeoCoderAddress(encode_url,strAddress).execute();
                }
            }
            else if (addFlag==2)
            {
                if (auto_Dropoff.getText().toString().trim().length() > 0)
                {
                    dropoffLocation = auto_Dropoff.getText().toString();
                    String encode_url = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + place_id + "&key="+Constants.GOOGLE_API_KEY;
                    Log.e("call","dropoffurl = "+encode_url);
                    new GetGeoCoderAddress(encode_url,strAddress).execute();
                }
            }
        }
        catch (IndexOutOfBoundsException ex)
        {
            ex.printStackTrace();
        }
        catch (Exception e)
        {
            // TODO: handle exception
            Log.e("call","exception = "+e.getMessage());
            if (addFlag==1)
            {
                pickup_Lat = 0.0;
                pickup_Long = 0.0;
                pickupLocation = "";
            }
            else if (addFlag==2)
            {
                dropoff_Lat = 0.0;
                dropoff_Long = 0.0;
                dropoffLocation = "";
            }
        }
    }

    public class GetGeoCoderAddress extends AsyncTask<String, Void, Void> {
        private final String Urlcoreconfig;
        String lat, lng;
        private String jsonResult;
        private String strAddress;

        public GetGeoCoderAddress(String url,String strAddress) {
            this.Urlcoreconfig = url;
            this.strAddress=strAddress;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... params) {
            try
            {
                HttpClient httpclient = new DefaultHttpClient();
                HttpGet httppost = new HttpGet(Urlcoreconfig);
                HttpResponse response = httpclient.execute(httppost);
                jsonResult = inputStreamToString(response.getEntity().getContent()).toString();
                System.out.println("sureshhhhhhhhh " + jsonResult);
                JSONObject json = new JSONObject(jsonResult);
                System.out.println("ssssssssssss " + json);
                json = json.getJSONObject("result");
                json = json.getJSONObject("geometry");
                json = json.getJSONObject("location");
                lat = json.getString("lat");
                lng = json.getString("lng");
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            try
            {
                if (lat!=null && !lat.trim().equalsIgnoreCase("") && lng!=null && !lng.equalsIgnoreCase(""))
                {
                    if (addFlag==1)
                    {
                        pickup_Lat = Double.valueOf(lat);
                        pickup_Long = Double.valueOf(lng);
                        System.out.println("sureshhhhhhhhh " + pickupLocation + pickup_Lat + " , " + pickup_Long);
                    }
                    else if (addFlag==2)
                    {
                        dropoff_Lat = Double.valueOf(lat);
                        dropoff_Long = Double.valueOf(lng);
                        System.out.println("sureshhhhhhhhh " + dropoffLocation + dropoff_Lat + " , " + dropoff_Long);
                    }
                }
                else
                {
                    Geocoder coder = new Geocoder(activity);
                    List<Address> address;
                    Address location = null;

                    address = coder.getFromLocationName(strAddress, 5);
                    address = coder.getFromLocationName(strAddress, 5);
                    if (!address.isEmpty())
                    {
                        location = address.get(0);
                        location.getLatitude();
                        location.getLongitude();
                    }

                    if (!address.isEmpty())
                    {
                        if (addFlag==1)
                        {
                            pickup_Lat= location.getLatitude();
                            pickup_Long = location.getLongitude();
                            if (auto_Pickup.getText().toString().trim().length() > 0)
                            {
                                pickupLocation = auto_Pickup.getText().toString();
                            }
                        }
                        else if (addFlag==2)
                        {
                            dropoff_Lat= location.getLatitude();
                            dropoff_Long = location.getLongitude();
                            if (auto_Dropoff.getText().toString().trim().length() > 0)
                            {
                                dropoffLocation = auto_Dropoff.getText().toString();
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Log.e("call","exception = "+e.getMessage());
            }
        }
    }

    public StringBuilder inputStreamToString(InputStream is) {
        String rLine = "";
        StringBuilder answer = new StringBuilder();
        BufferedReader rd = new BufferedReader(new InputStreamReader(is));
        try
        {
            while ((rLine = rd.readLine()) != null)
            {
                answer.append(rLine);
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return answer;
    }

    // Get the google API result and convert into JSON format.
    private JSONObject GetAddress(String Url) {
        try
        {
            HttpClient httpclient = new DefaultHttpClient();
            HttpGet httppost = new HttpGet(Url);
            HttpResponse response = httpclient.execute(httppost);
            String jsonResult = Utility.inputStreamToString(response.getEntity().getContent()).toString();
            JSONObject json = new JSONObject(jsonResult);
            return json;
        }
        catch (Exception e)
        {
            // TODO: handle exception
        }
        return null;
    }

    public void selectDateAndTime()
    {
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        final DatePickerDialog datePickerDialog = new DatePickerDialog(this,R.style.datepicker,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        dateTimetxt.setText("");
                        dateTime = "";
                        showTimePicker(dayOfMonth,(monthOfYear + 1),year);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
    }

    public void showTimePicker(final int dayOfMonth, final int monthOfYear, final int year)
    {
        String selectedDate = "";
        if (monthOfYear<10)
        {
            if (dayOfMonth<10)
            {
                selectedDate = year+"-0"+monthOfYear+"-0"+dayOfMonth;
            }
            else
            {
                selectedDate = year+"-0"+monthOfYear+"-"+dayOfMonth;
            }
        }
        else
        {
            if (dayOfMonth<10)
            {
                selectedDate = year+"-"+monthOfYear+"-0"+dayOfMonth;
            }
            else
            {
                selectedDate = year+"-"+monthOfYear+"-"+dayOfMonth;
            }
        }

        final String formattedDate = TicktocApplication.getCurrentDate().split(",")[0].trim();
        Log.e("call","sdlfjsldjfsldjflj = "+formattedDate);
        // Get Current Time
        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);
        Log.e("call","sdfsdfsdfsfsf selectedDate = "+selectedDate);
        if (formattedDate.equalsIgnoreCase(selectedDate))
        {
            Log.e("call","111111111111");

            if (c.get(Calendar.MINUTE ) + 31>=60)
            {
                mMinute = (c.get(Calendar.MINUTE ) + 31 - 60);
                mHour = mHour + 1;
            }
            else
            {
                mMinute = (c.get(Calendar.MINUTE ) + 31);
            }
        }
        else
        {
            Log.e("call","22222222222222");
            mMinute = c.get(Calendar.MINUTE);
        }


        // Launch Time Picker Dialog
        final String finalSelectedDate = selectedDate;
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,R.style.datepicker,new TimePickerDialog.OnTimeSetListener()
        {

            @Override
            public void onTimeSet(TimePicker view, int hourOfDay,int minute)
            {
                try
                {
                    int checkMinut = minute;
                    int checkHour = hourOfDay;
                    Calendar datetime1 = Calendar.getInstance();
                    Calendar c1 = Calendar.getInstance();
                    datetime1.set(Calendar.MINUTE, minute);
                    datetime1.set(Calendar.HOUR_OF_DAY, hourOfDay);

                    Calendar datetime = Calendar.getInstance();
                    Calendar c = Calendar.getInstance();
                    int flag = 0;
                    if (formattedDate.equalsIgnoreCase(finalSelectedDate))
                    {
                        Log.e("call","333333333333333333");

                        if (minute+31>=60)
                        {
                            flag = 1;
                            hourOfDay = Math.abs(hourOfDay + 1);
                        }
                    }

                    Log.e("call","hourOfDay ========= "+hourOfDay);
                    Log.e("call","minutes ========= " + minute);

                    datetime.set(Calendar.MINUTE, minute);
                    datetime.set(Calendar.HOUR_OF_DAY, hourOfDay);

                    String my_date = dayOfMonth+"/"+monthOfYear+"/"+year;

                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                    Date strDate = sdf.parse(my_date);

                    if(sdf.parse(my_date).before(new Date()))
                    {
                        if (checkValidTime(my_date+" "+checkHour+":"+checkMinut+":00"))
                        {
                            String AM_PM ;
                            if(hourOfDay < 12)
                            {
                                AM_PM = "AM";
                            }
                            else
                            {
                                AM_PM = "PM";
                            }

                            int hour;
                            if (flag == 1)
                            {
                                hourOfDay = hourOfDay - 1;
                            }

                            if (hourOfDay == 0 || hourOfDay==12)
                            {
                                hour = 12;
                            }
                            else if (hourOfDay < 12)
                            {
                                hour = hourOfDay;
                            }
                            else
                            {
                                hour = hourOfDay - 12;
                            }

                            Log.e("call","hour ========= "+hour);
                            Log.e("call","minutes ========= " + minute);
                            Log.e("call","date and time = "+dateTime);
                            dateTimetxt.setText(dayOfMonth + "-" + monthOfYear + "-" + year + "  " + hour + ":" + minute + " "+AM_PM);
                            pickup_Date = year + "-" + monthOfYear + "-" + dayOfMonth;
                            pickup_Time = hourOfDay + "-" + minute + "-00";
                            dateTime = pickup_Date + " " + pickup_Time;
                            Log.e("call","date and time = "+dateTime);
                        }
                        else
                        {
                            InternetDialog internetDialog = new InternetDialog(activity);
                            internetDialog.showDialog("Please select 30 minutes greater time from current time!");
                        }
                    }
                    else if(sdf.parse(my_date).equals(new Date()))
                    {
                        if (checkValidTime(my_date+" "+checkHour+":"+checkMinut+":00"))
                        {
                            String AM_PM ;
                            if(hourOfDay < 12)
                            {
                                AM_PM = "AM";
                            }
                            else
                            {
                                AM_PM = "PM";
                            }

                            int hour;
                            if (flag == 1)
                            {
                                hourOfDay = hourOfDay - 1;
                            }

                            if (hourOfDay == 0 || hourOfDay==12)
                            {
                                hour = 12;
                            }
                            else if (hourOfDay < 12)
                            {
                                hour = hourOfDay;
                            }
                            else
                            {
                                hour = hourOfDay - 12;
                            }

                            Log.e("call","hour ========= "+hour);
                            Log.e("call","minutes ========= " + minute);
                            Log.e("call","date and time = "+dateTime);
                            dateTimetxt.setText(dayOfMonth + "-" + monthOfYear + "-" + year + "  " + hour + ":" + minute + " "+AM_PM);
                            pickup_Date = year + "-" + monthOfYear + "-" + dayOfMonth;
                            pickup_Time = hourOfDay + "-" + minute + "-00";
                            dateTime = pickup_Date + " " + pickup_Time;
                            Log.e("call","date and time = "+dateTime);
                        }
                        else
                        {
                            InternetDialog internetDialog = new InternetDialog(activity);
                            internetDialog.showDialog("Please select 30 minutes greater time from current time!");
                        }
                    }
                    else
                    {
                        if (checkValidTime(my_date+" "+checkHour+":"+checkMinut+":00"))
                        {
                            String AM_PM ;
                            if(hourOfDay < 12)
                            {
                                AM_PM = "AM";
                            }
                            else
                            {
                                AM_PM = "PM";
                            }

                            int hour;
                            if (flag == 1)
                            {
                                hourOfDay = hourOfDay - 1;
                            }

                            if (hourOfDay == 0 || hourOfDay==12)
                            {
                                hour = 12;
                            }
                            else if (hourOfDay < 12)
                            {
                                hour = hourOfDay;
                            }
                            else
                            {
                                hour = hourOfDay - 12;
                            }

                            Log.e("call","hour ========= "+hour);
                            Log.e("call","minutes ========= " + minute);
                            Log.e("call","date and time = "+dateTime);
                            dateTimetxt.setText(dayOfMonth + "-" + monthOfYear + "-" + year + "  " + hour + ":" + minute + " "+AM_PM);
                            pickup_Date = year + "-" + monthOfYear + "-" + dayOfMonth;
                            pickup_Time = hourOfDay + "-" + minute + "-00";
                            dateTime = pickup_Date + " " + pickup_Time;
                            Log.e("call","date and time = "+dateTime);
                        }
                        else
                        {
                            InternetDialog internetDialog = new InternetDialog(activity);
                            internetDialog.showDialog("Please select 30 minutes greater time from current time!");
                        }
                    }
                }
                catch (Exception e)
                {
                    Log.e("Exception"," = "+e.getMessage());
                }

            }
        }, mHour, mMinute, false);
        timePickerDialog.show();
    }

    public boolean checkValidTime(String selectedDateTime)
    {


        try
        {

            Log.e("call","selected = "+selectedDateTime);
            Log.e("call","current = "+new Date().toString());

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            Date selected = simpleDateFormat.parse(selectedDateTime);
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            String strDate = sdf.format(cal.getTime());
            Date current =simpleDateFormat.parse(strDate);

            long diff = selected.getTime() - current.getTime();
            long seconds = diff / 1000;
            long minutes = seconds / 60;

            Log.e("call","selected = "+selected.toString());
            Log.e("call","current = "+current.toString());
            Log.e("call","difference in minutes = "+minutes);

            if (minutes>=30)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            Log.e("call","exception in date formate = "+e.getMessage());
            return false;
        }
    }

    public void call_CheckValidation()
    {
        if (pickupLocation==null || pickupLocation.equalsIgnoreCase("") || pickup_Touch==false)
        {
            mySnackBar.showSnackBar(ll_RootLayout,"whoops, you have missed a field, please complete and then try again");//Please enter a valid pickup location!
        }
        else if (dropoffLocation==null || dropoffLocation.equalsIgnoreCase("") || dropoff_Touch==false)
        {
            mySnackBar.showSnackBar(ll_RootLayout,"whoops, you have missed a field, please complete and then try again");//Please enter a valid destination!
        }
        else if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG,activity)!=null &&
                SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG,activity).equalsIgnoreCase("1") &&
                bookingType.equalsIgnoreCase("Book Later") && et_Name.getText().toString().trim().equals(""))
        {
            mySnackBar.showSnackBar(ll_RootLayout,"whoops, you have missed a field, please complete and then try again");//Please enter Name!
            et_Name.setFocusableInTouchMode(true);
            et_Name.requestFocus();
        }
        else if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG,activity)!=null &&
                SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG,activity).equalsIgnoreCase("1") &&
                bookingType.equalsIgnoreCase("Book Later") &&
                et_Phone.getText().toString().trim().equals(""))
        {
            mySnackBar.showSnackBar(ll_RootLayout,"whoops, you have missed a field, please complete and then try again");//Please enter Phone Number!
            et_Phone.setFocusableInTouchMode(true);
            et_Phone.requestFocus();
        }
        else if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG,activity)!=null &&
                SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG,activity).equalsIgnoreCase("1") &&
                bookingType.equalsIgnoreCase("Book Later") &&
                et_Phone.getText().toString().trim().length()!=10)
        {
            mySnackBar.showSnackBar(ll_RootLayout,"whoops, you have missed a field, please complete and then try again");//Invalid Phone Number!
            et_Phone.setFocusableInTouchMode(true);
            et_Phone.requestFocus();
        }
        else if (selectedPaymentType==null || selectedPaymentType.equalsIgnoreCase("") || selectedPaymentType.equalsIgnoreCase("cash"))
        {
            mySnackBar.showSnackBar(ll_RootLayout,"whoops, you have missed a field, please complete and then try again");//Please select payment method!
        }
        else if (flight==true)
        {
            if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG,activity)!=null &&
                    SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG,activity).equalsIgnoreCase("1") &&
                    bookingType.equalsIgnoreCase("Book Later") &&
                    et_Flight.getText().toString().trim().equals(""))
            {
                mySnackBar.showSnackBar(ll_RootLayout,"whoops, you have missed a field, please complete and then try again");//Please enter flight number!
                et_Flight.setFocusableInTouchMode(true);
                et_Flight.requestFocus();
            }
            else
            {
                if (from!=null && !from.equalsIgnoreCase(""))
                {
                    if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG,activity)!=null &&
                            SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG,activity).equalsIgnoreCase("1") &&
                            bookingType!=null &&
                            bookingType.equalsIgnoreCase("Book Later"))
                    {
                        if (dateTimetxt.getText().toString().trim().equalsIgnoreCase(""))
                        {
                            mySnackBar.showSnackBar(ll_RootLayout,"whoops, you have missed a field, please complete and then try again");//Please select date and time!
                        }
                        else if (Global.isNetworkconn(activity))
                        {
                            call_BookingDetailEdit();
                        }
                        else
                        {
                            InternetDialog internetDialog = new InternetDialog(activity);
                            internetDialog.showDialog("Please check your internet connection!");
                        }
                    }
                    else
                    {
                        if (Global.isNetworkconn(activity))
                        {
                            call_BookingDetailEdit();
                        }
                        else
                        {
                            InternetDialog internetDialog = new InternetDialog(activity);
                            internetDialog.showDialog("Please check your internet connection!");
                        }
                    }
                }
                else
                {
                    if (dateTimetxt.getText().toString().trim().equalsIgnoreCase(""))
                    {
                        mySnackBar.showSnackBar(ll_RootLayout,"whoops, you have missed a field, please complete and then try again");//Please select date and time!
                    }
                    else if (Global.isNetworkconn(activity))
                    {
                        call_BookLaterApi();
                    }
                    else
                    {
                        InternetDialog internetDialog = new InternetDialog(activity);
                        internetDialog.showDialog("Please check your internet connection!");
                    }
                }
            }
        }
        else
        {
            if (from!=null && !from.equalsIgnoreCase(""))
            {
                if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG,activity)!=null &&
                        SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG,activity).equalsIgnoreCase("1") &&
                        bookingType!=null &&
                        bookingType.equalsIgnoreCase("Book Later"))
                {
                    if (dateTimetxt.getText().toString().trim().equalsIgnoreCase(""))
                    {
                        mySnackBar.showSnackBar(ll_RootLayout,"whoops, you have missed a field, please complete and then try again");//Please select date and time!
                    }
                    else if (Global.isNetworkconn(activity))
                    {
                        call_BookingDetailEdit();
                    }
                    else
                    {
                        InternetDialog internetDialog = new InternetDialog(activity);
                        internetDialog.showDialog("Please check your internet connection!");
                    }
                }
                else
                {
                    if (Global.isNetworkconn(activity))
                    {
                        call_BookingDetailEdit();
                    }
                    else
                    {
                        InternetDialog internetDialog = new InternetDialog(activity);
                        internetDialog.showDialog("Please check your internet connection!");
                    }
                }
            }
            else
            {
                if (dateTimetxt.getText().toString().trim().equalsIgnoreCase(""))
                {
                    mySnackBar.showSnackBar(ll_RootLayout,"whoops, you have missed a field, please complete and then try again");//Please select date and time!
                }
                else if (Global.isNetworkconn(activity))
                {
                    call_BookLaterApi();
                }
                else
                {
                    InternetDialog internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog("Please check your internet connection!");
                }
            }
        }
    }

    private void call_BookLaterApi()
    {
        dialogClass.showDialog();
        String url = WebServiceAPI.API_ADVANCE_BOOKING;

        String passenger_type = "myself";

        if (mySelfOtherFlag==0)
        {
            passenger_type = "myself";
        }
        else
        {
            passenger_type = "other";
        }

        Map<String, Object> params = new HashMap<String, Object>();
        params.put(WebServiceAPI.PARAM_PASSENGER_ID,SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID,activity));
        params.put(WebServiceAPI.PARAM_MODEL_ID,MainActivity.carType_beens.get(MainActivity.selectedCar).getId());
        params.put(WebServiceAPI.PARAM_PICKUP_LOCATION,pickupLocation);
        params.put(WebServiceAPI.PARAM_DROPOFF_LOCATION,dropoffLocation);
        params.put(WebServiceAPI.PARAM_PASSENGER_TYPE,passenger_type);
        params.put(WebServiceAPI.PARAM_PASSENGER_NAME,et_Name.getText().toString().trim());
        params.put(WebServiceAPI.PARAM_PASSENGER_CONTACT,et_Phone.getText().toString().trim());
        params.put(WebServiceAPI.PARAM_PICKUP_DATE_TIME,dateTime);
        params.put(WebServiceAPI.PARAM_PROMO_CODE,promocodeStr);
        params.put(WebServiceAPI.PARAM_PAYMENT_TYPE,selectedPaymentType);
        params.put(WebServiceAPI.PARAM_BABYSEAT,isBabySeatInclude ? "1":"0");
        params.put(WebServiceAPI.PARAM_CITYID,MainActivity.cityId);

        if(pickUpSubArb != null && dropOffSubAArb != null)
        {
            params.put(WebServiceAPI.PARAM_PICKUP_SUBURB,pickUpSubArb);
            params.put(WebServiceAPI.PARAM_DROPOFF_SUBURB,dropOffSubAArb);
        }

        if (notes==true)
        {
            params.put(WebServiceAPI.PARAM_NOTES,et_Notes.getText().toString().trim());
        }

        if (selectedPaymentType.equalsIgnoreCase("card"))
        {
            params.put(WebServiceAPI.PARAM_CARD_ID,cardId);
        }

        if (flight==true)
        {
            params.put(WebServiceAPI.PARAM_FLIGHT_NUMBER,et_Flight.getText().toString().trim());
        }

        Log.e("call", "call_BookLaterApi url = " + url);
        Log.e("call", "call_BookLaterApi param = " + params);

        aQuery.ajax(url.trim(), params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e("responseCode", "call_BookLaterApi = " + responseCode);
                    Log.e("Response", "call_BookLaterApi = " + json);

                    if (json!=null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                Log.e("call","book later send request successfully");
                                dialogClass.hideDialog();

                                String message = "";

                                if (json.has("message"))
                                {
                                    message = json.getString("message");
                                }

                                showSuccessMessage(message);
                            }
                            else
                            {
                                Log.e("call","book later status false");
                                dialogClass.hideDialog();

                                if (json.has("message"))
                                {
                                    InternetDialog internetDialog = new InternetDialog(activity);
                                    internetDialog.showDialog(json.getString("message"));
                                }
                                else
                                {
                                    InternetDialog internetDialog = new InternetDialog(activity);
                                    internetDialog.showDialog("Something went wrong.");
                                }
                            }
                        }
                        else
                        {
                            Log.e("call","book later status not found");
                            dialogClass.hideDialog();
                        }
                    }
                    else
                    {
                        Log.e("call","book later json null");
                        dialogClass.hideDialog();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Exception","Exception "+e.toString());
                    dialogClass.hideDialog();
                }
            }

        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    private void call_BookingDetailEdit()
    {
        dialogClass.showDialog();
        String url = WebServiceAPI.API_UPDATE_BOOKING_DETAIL;
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(WebServiceAPI.PARAM_BOOKING_ID,BookingId);
        if (bookingType.equalsIgnoreCase("Book Now"))
        {
            params.put(WebServiceAPI.PARAM_BOOKING_TYPE,"BookNow");
        }
        else
        {
            params.put(WebServiceAPI.PARAM_BOOKING_TYPE,"BookLater");
        }
        params.put(WebServiceAPI.PARAM_PICKUP_LOCATION,pickupLocation);
        params.put(WebServiceAPI.PARAM_PICKUP_LAT,pickup_Lat);
        params.put(WebServiceAPI.PARAM_PICKUP_LONG,pickup_Long);
        params.put(WebServiceAPI.PARAM_DROPOFF_LOCATION,dropoffLocation);
        params.put(WebServiceAPI.PARAM_DROPOFF_LAT,dropoff_Lat);
        params.put(WebServiceAPI.PARAM_DROPOFF_LONG,dropoff_Long);
        if (selectedPaymentType.equalsIgnoreCase("card"))
        {
            params.put(WebServiceAPI.PARAM_CARD_ID,cardId);
        }
        if (notes==true)
        {
            params.put(WebServiceAPI.PARAM_NOTES,et_Notes.getText().toString().trim());
        }
        else
        {
            params.put(WebServiceAPI.PARAM_NOTES,"");
        }
        params.put(WebServiceAPI.PARAM_CITYID,MainActivity.cityId);
        if(pickUpSubArb != null && dropOffSubAArb != null)
        {
            params.put(WebServiceAPI.PARAM_PICKUP_SUBURB,pickUpSubArb);
            params.put(WebServiceAPI.PARAM_DROPOFF_SUBURB,dropOffSubAArb);
        }
        params.put(WebServiceAPI.PARAM_BABYSEAT,isBabySeatInclude ? "1":"0");

        params.put(WebServiceAPI.PARAM_PASSENGER_ID,SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID,activity));
        params.put(WebServiceAPI.PARAM_MODEL_ID,MainActivity.carType_beens.get(editSelectedCar).getId());

        if (bookingType.equalsIgnoreCase("Book Later"))
        {
            String passenger_type = "myself";

            if (mySelfOtherFlag==0)
            {
                passenger_type = "myself";
            }
            else
            {
                passenger_type = "other";
            }
            params.put(WebServiceAPI.PARAM_PASSENGER_TYPE,passenger_type);
            params.put(WebServiceAPI.PARAM_PASSENGER_NAME,et_Name.getText().toString().trim());
            params.put(WebServiceAPI.PARAM_PASSENGER_CONTACT,et_Phone.getText().toString().trim());
            params.put(WebServiceAPI.PARAM_PICKUP_DATE_TIME,dateTime);
            if (flight==true)
            {
                params.put(WebServiceAPI.PARAM_FLIGHT_NUMBER,et_Flight.getText().toString().trim());
            }
            else
            {
                params.put(WebServiceAPI.PARAM_FLIGHT_NUMBER,"");
            }
        }

        params.put(WebServiceAPI.PARAM_PROMO_CODE,promocodeStr);
        params.put(WebServiceAPI.PARAM_PAYMENT_TYPE,selectedPaymentType);

        Log.e("call", "call_BookLaterApi url = " + url);
        Log.e("call", "call_BookLaterApi param = " + params);

        aQuery.ajax(url.trim(), params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e("responseCode", "call_BookLaterApi = " + responseCode);
                    Log.e("Response", "call_BookLaterApi = " + json);

                    if (json!=null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                Log.e("call","book later send request successfully");
                                dialogClass.hideDialog();

                                String message = "";

                                if (json.has("message"))
                                {
                                    message = json.getString("message");
                                }

                                if (bookingType.equals("Book Now"))
                                {
                                    MainActivity.isBookingDetailEdited = true;
                                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, json.toString(), activity);
                                }
                                showSuccessMessageEditBooking(message);
                            }
                            else
                            {
                                Log.e("call","book later status false");
                                dialogClass.hideDialog();

                                if (json.has("message"))
                                {
                                    InternetDialog internetDialog = new InternetDialog(activity);
                                    internetDialog.showDialog(json.getString("message"));
                                }
                                else
                                {
                                    InternetDialog internetDialog = new InternetDialog(activity);
                                    internetDialog.showDialog("Something went wrong.");
                                }
                            }
                        }
                        else
                        {
                            Log.e("call","book later status not found");
                            dialogClass.hideDialog();
                        }
                    }
                    else
                    {
                        Log.e("call","book later json null");
                        dialogClass.hideDialog();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Exception","Exception "+e.toString());
                    dialogClass.hideDialog();
                }
            }

        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    public void showSuccessMessage(String message)
    {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.my_dialog_class);

        TextView tv_Ok = (TextView) dialog.findViewById(R.id.Ok);
        TextView tv_Message = (TextView) dialog.findViewById(R.id.message);
        LinearLayout ll_Ok = (LinearLayout) dialog.findViewById(R.id.ok_layout);

        tv_Message.setText(message);

        ll_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                MainActivity.selectedCar = -1;
                MainActivity.myLocation = true;
                finish();
                overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);
            }
        });

        tv_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                MainActivity.selectedCar = -1;
                MainActivity.myLocation = true;
                finish();
                overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        dialog.show();
    }

    public void showSuccessMessageEditBooking(String message)
    {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.my_dialog_class);

        TextView tv_Ok = (TextView) dialog.findViewById(R.id.Ok);
        TextView tv_Message = (TextView) dialog.findViewById(R.id.message);
        LinearLayout ll_Ok = (LinearLayout) dialog.findViewById(R.id.ok_layout);

        tv_Message.setText(message);

        ll_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                finish();
                overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);
            }
        });

        tv_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                finish();
                overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        dialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();

        TicktocApplication.setCurrentActivity(activity);
        saveCardList();
        if (isCardSelected)
        {
            Spinner_paymentMethod.setSelection((2+spinnerSelectedPosition));
        }
    }

    public void saveCardList()
    {
        try
        {
            String strCardList = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_CARD_LIST,activity);
            MainActivity.cardListBeens.clear();

            if (strCardList!=null && !strCardList.equalsIgnoreCase(""))
            {
                JSONObject json = new JSONObject(strCardList);

                if (json!=null)
                {
                    if (json.has("cards"))
                    {
                        JSONArray cards = json.getJSONArray("cards");

                        if (cards!=null && cards.length()>0)
                        {
                            for (int i=0; i<cards.length(); i++)
                            {
                                JSONObject cardsData = cards.getJSONObject(i);

                                if (cardsData!=null)
                                {
                                    String Id="",CardNum="",CardNum_="",Type="",Alias="";

                                    if (cardsData.has("Id"))
                                    {
                                        Id = cardsData.getString("Id");
                                    }

                                    if (cardsData.has("CardNum"))
                                    {
                                        CardNum = cardsData.getString("CardNum");
                                    }

                                    if (cardsData.has("CardNum2"))
                                    {
                                        CardNum_ = cardsData.getString("CardNum2");
                                    }

                                    if (cardsData.has("Type"))
                                    {
                                        Type = cardsData.getString("Type");
                                    }

                                    if (cardsData.has("Alias"))
                                    {
                                        Alias = cardsData.getString("Alias");
                                    }

                                    MainActivity.cardListBeens.add(new CreditCard_List_Been(Id,CardNum,CardNum_,Type,Alias));
                                }
                            }
                            if(MainActivity.cardListBeens.size()>0)
                            {ll_add_payment_method.setVisibility(View.GONE);}
                            customerAdapter.notifyDataSetChanged();
                            //MainActivity.cardListBeens.add(new CreditCard_List_Been("","","Add a Card","",""));
//                            MainActivity.cardListBeens.add(new CreditCard_List_Been("","","wallet","",""));
                        }
                        else
                        {
                            //MainActivity.cardListBeens.add(new CreditCard_List_Been("","","Add a Card","",""));
//                            MainActivity.cardListBeens.add(new CreditCard_List_Been("","","wallet","",""));
                        }
                    }
                    else
                    {
                        //MainActivity.cardListBeens.add(new CreditCard_List_Been("","","Add a Card","",""));
//                        MainActivity.cardListBeens.add(new CreditCard_List_Been("","","wallet","",""));
                    }
                }
                else
                {
                    //MainActivity.cardListBeens.add(new CreditCard_List_Been("","","Add a Card","",""));
//                    MainActivity.cardListBeens.add(new CreditCard_List_Been("","","wallet","",""));
                }
            }
            else
            {
                //MainActivity.cardListBeens.add(new CreditCard_List_Been("","","Add a Card","",""));
//                MainActivity.cardListBeens.add(new CreditCard_List_Been("","","wallet","",""));
            }
        }
        catch (Exception e)
        {

        }
    }

    public class CustomerAdapter extends ArrayAdapter<CreditCard_List_Been> {
        ArrayList<CreditCard_List_Been> customers, tempCustomer, suggestions;

        public CustomerAdapter(Context context, ArrayList<CreditCard_List_Been> objects) {
            super(context, R.layout.row_spinner_item, R.id.spinner_cardnumber, objects);
            this.customers = objects;
            this.tempCustomer = new ArrayList<CreditCard_List_Been>(objects);
            this.suggestions = new ArrayList<CreditCard_List_Been>(objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return initView(position, convertView, parent);
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return initView(position, convertView, null);
        }

        private View initView(int position, View convertView, ViewGroup parent) {
            CreditCard_List_Been customer = getItem(position);
            if (convertView == null) {
                if (parent == null)
                    convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_spinner_item, null);
                else
                    convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_spinner_item, parent, false);
            }
            TextView txtCustomer = (TextView) convertView.findViewById(R.id.spinner_cardnumber);
            LinearLayout ll_Image = (LinearLayout) convertView.findViewById(R.id.spinner_image_layout);
            ImageView iv_Image = (ImageView) convertView.findViewById(R.id.image);
            LinearLayout ll_Add_Card = (LinearLayout) convertView.findViewById(R.id.ll_add_payment_method);

            if (txtCustomer != null)
                txtCustomer.setText(customer.getCardNum_());

            if (customer.getCardType()!=null && customer.getCardType().equalsIgnoreCase("visa"))
            {
                iv_Image.setImageResource(R.drawable.ic_card_icon_visa);
            }
            else if (customer.getCardType()!=null && customer.getCardType().equalsIgnoreCase("mastercard"))
            {
                iv_Image.setImageResource(R.drawable.ic_card_icon_master);
            }
            else if (customer.getCardType()!=null && customer.getCardType().equalsIgnoreCase("amex"))
            {
                iv_Image.setImageResource(R.drawable.ic_card_icon_american);
            }
            else if (customer.getCardType()!=null && customer.getCardType().equalsIgnoreCase("diners"))
            {
                iv_Image.setImageResource(R.drawable.ic_card_icon_dinner);
            }
            else if (customer.getCardType()!=null && customer.getCardType().equalsIgnoreCase("discover"))
            {
                iv_Image.setImageResource(R.drawable.ic_card_icon_discover);
            }
            else if (customer.getCardType()!=null && customer.getCardType().equalsIgnoreCase("jcb"))
            {
                iv_Image.setImageResource(R.drawable.ic_card_icon_jcb);
            }
            else if (customer.getCardType()!=null && customer.getCardType().equalsIgnoreCase("other"))
            {
                iv_Image.setImageResource(R.drawable.ic_card_icon_visa);
            }
            else if (customer.getCardNum_()!=null && customer.getCardNum_().equalsIgnoreCase("cash"))
            {
                iv_Image.setImageResource(R.drawable.ic_card_icon_cash);
            }
            else if (customer.getCardNum_()!=null && customer.getCardNum_().equalsIgnoreCase("wallet"))
            {
                iv_Image.setImageResource(R.drawable.ic_card_icon_wallet);
            }
            else if (customer.getCardNum_()!=null && customer.getCardNum_().equalsIgnoreCase("Add a Card"))
            {
                iv_Image.setImageResource(R.drawable.icon_plus_black);
            }

            if (position == (customers.size()-1))
            {
                if (checkCardList())
                {
                    ll_Add_Card.setVisibility(View.GONE);
                }
                else
                {
                    //ll_Add_Card.setVisibility(View.VISIBLE);//Saurav 12-2-2020
                }
            }
            else
            {
                ll_Add_Card.setVisibility(View.GONE);
            }

            ll_Add_Card.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(activity,Add_Card_In_List_Activity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_up,R.anim.no_change);
                }
            });

            return convertView;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICKUP_AUTOCOMPLETE_REQUEST_CODE && resultCode == Activity.RESULT_OK) {

            pickup_Touch = true;

            Place place = Autocomplete.getPlaceFromIntent(data);
            Log.e("TAG", "Place: " + place.getName() + ", " + place.getAddress());
            tvPickup.setText(place.getName() + "," + place.getAddress());
            pickupLocation = place.getName() + "," + place.getAddress();

            tvPickup.setSelected(true);
            tvPickup.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            tvPickup.setSingleLine(true);

            pickup_Lat = place.getLatLng().latitude;
            pickup_Long = place.getLatLng().longitude;

            getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
            );
            /*View view = this.getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }*/

        } else if (requestCode == DROPOFF_AUTOCOMPLETE_REQUEST_CODE && resultCode == Activity.RESULT_OK) {

            dropoff_Touch = true;

            Place place = Autocomplete.getPlaceFromIntent(data);
            Log.e("TAG", "Place: " + place.getName() + ", " + place.getAddress());
            tvDropoff.setText(place.getName() + "," + place.getAddress());
            dropoffLocation = place.getName() + "," + place.getAddress();

            tvDropoff.setSelected(true);
            tvDropoff.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            tvDropoff.setSingleLine(true);


            dropoff_Lat = place.getLatLng().latitude;
            dropoff_Long = place.getLatLng().longitude;

            getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
            );
            /*View view = this.getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }*/
        }
    }
}

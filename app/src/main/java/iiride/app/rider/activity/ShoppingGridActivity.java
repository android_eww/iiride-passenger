package iiride.app.rider.activity;

import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import iiride.app.rider.R;
import iiride.app.rider.adapter.ShoppingGridAdapter;
import iiride.app.rider.application.TicktocApplication;
import iiride.app.rider.been.ShoppingGreed_Been;
import iiride.app.rider.comman.Constants;

import java.util.ArrayList;
import java.util.List;


public class ShoppingGridActivity extends AppCompatActivity implements View.OnClickListener{

    public static ShoppingGridActivity activity;
    private LinearLayout ll_Back;
    private ImageView iv_Back;
    private TextView tv_Title;
    private GridView gridView;
    private List<ShoppingGreed_Been> list = new ArrayList<ShoppingGreed_Been>();
    private ShoppingGridAdapter gridAdapter;
    private LinearLayout headerMainLayout;
    public static int navigationBarHeight = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_grid);

        activity = ShoppingGridActivity.this;

        init();
    }




    private void init() {

        ll_Back = (LinearLayout) findViewById(R.id.back_layout);
        iv_Back = (ImageView) findViewById(R.id.back_imageview);

        tv_Title = (TextView) findViewById(R.id.title_textview);

        tv_Title.setText("Shopping");
        headerMainLayout = (LinearLayout) findViewById(R.id.headerLayout);
        gridView = (GridView)findViewById(R.id.grid_view);

        list.clear();

        list.add(new ShoppingGreed_Been(R.drawable.ic_grid_bottle_shop,"Bottle Shops"));
        list.add(new ShoppingGreed_Been(R.drawable.ic_grid_grosery,"Groceries"));
        list.add(new ShoppingGreed_Been(R.drawable.ic_grid_auto_parts,"Auto Parts"));
        list.add(new ShoppingGreed_Been(R.drawable.ic_grid_chemist,"Chemist"));
        list.add(new ShoppingGreed_Been(R.drawable.ic_grid_hardware,"Hardware"));
        list.add(new ShoppingGreed_Been(R.drawable.ic_grid_retail,"Retail"));

        Log.e("navigationBarHeight","navigationBarHeight: "+navigationBarHeight+ "\nhasNavBar :" + hasNavBar(getResources())+"\nhasSoftKeys "+hasSoftKeys(getWindowManager()));

        int resourceId = getResources().getIdentifier("navigation_bar_height", "dimen", "android");

        if(hasSoftKeys(getWindowManager())==true)
        {
            Log.e("navigationBar","hasSoftKeys == true");
            if (hasNavBar(getResources())==false)
            {
                Log.e("navigationBar","hasNavBar == true");
                if (resourceId > 0)
                {
                    Log.e("navigationBar",">0");
                    navigationBarHeight = 0;
                }
            }
            else
            {
                Log.e("navigationBar","hasNavBar == true");
                navigationBarHeight=0;
            }
        }
        else
        {
            Log.e("navigationBar","hasSoftKeys == false");
            navigationBarHeight=0;
        }

        ViewTreeObserver vtoHeader = headerMainLayout.getViewTreeObserver();

        vtoHeader.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                headerMainLayout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                int width  = headerMainLayout.getMeasuredWidth();
                int height = headerMainLayout.getMeasuredHeight();

                Constants.HEADER_HEIGHT = height;
                Constants.HEADER_WIDTH = width;

                Rect rectgle= new Rect();
                Window window= getWindow();
                window.getDecorView().getWindowVisibleDisplayFrame(rectgle);
                int StatusBarHeight= rectgle.top;
                int contentViewTop=window.findViewById(Window.ID_ANDROID_CONTENT).getTop();
                int TitleBarHeight= contentViewTop - StatusBarHeight;

                Constants.STATUS_BAR_HEIGHT = StatusBarHeight;

                gridAdapter = new ShoppingGridAdapter(activity,list);
                gridView.setAdapter(gridAdapter);

                Log.e("***  :: ", "StatusBar Height= " + StatusBarHeight + " , TitleBar Height = " + TitleBarHeight);
            }
        });

        ll_Back.setOnClickListener(activity);
        iv_Back.setOnClickListener(activity);

    }

    public boolean hasNavBar (Resources resources)
    {
        int id = resources.getIdentifier("config_showNavigationBar", "bool", "android");
        return id > 0 && resources.getBoolean(id);
    }

    public static boolean hasSoftKeys(WindowManager windowManager){
        boolean hasSoftwareKeys = true;

        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.JELLY_BEAN_MR1){
            Display d = activity.getWindowManager().getDefaultDisplay();

            DisplayMetrics realDisplayMetrics = new DisplayMetrics();
            d.getRealMetrics(realDisplayMetrics);

            int realHeight = realDisplayMetrics.heightPixels;
            int realWidth = realDisplayMetrics.widthPixels;

            DisplayMetrics displayMetrics = new DisplayMetrics();
            d.getMetrics(displayMetrics);

            int displayHeight = displayMetrics.heightPixels;
            int displayWidth = displayMetrics.widthPixels;

            hasSoftwareKeys =  (realWidth - displayWidth) > 0 || (realHeight - displayHeight) > 0;
        }else{
            boolean hasMenuKey = ViewConfiguration.get(activity).hasPermanentMenuKey();
            boolean hasBackKey = KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_BACK);
            hasSoftwareKeys = !hasMenuKey && !hasBackKey;
        }
        return hasSoftwareKeys;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.back_layout:
                onBackPressed();
                break;

            case R.id.back_imageview:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);
    }

    @Override
    protected void onResume() {
        super.onResume();

        TicktocApplication.setCurrentActivity(activity);

    }
}

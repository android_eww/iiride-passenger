package iiride.app.rider.activity;

import android.os.AsyncTask;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import iiride.app.rider.R;
import iiride.app.rider.adapter.HotelsBookingAdapter;
import iiride.app.rider.application.TicktocApplication;
import iiride.app.rider.been.HotelBookingBeen;
import iiride.app.rider.comman.Constants;
import iiride.app.rider.comman.Utility;
import iiride.app.rider.other.DialogClass;
import iiride.app.rider.other.GPSTracker;
import iiride.app.rider.other.Global;
import iiride.app.rider.other.HPLinearLayoutManager;
import iiride.app.rider.other.InternetDialog;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class HotelReservationActivity extends AppCompatActivity implements View.OnClickListener{

    public static HotelReservationActivity activity;
    private LinearLayout ll_Back;
    private ImageView iv_Back;
    private TextView tv_Title;

    private RecyclerView recyclerView;
    private HotelsBookingAdapter adapter;
    private ArrayList<HotelBookingBeen> arrayList;
    private HPLinearLayoutManager layoutManager;
    double latitude = 0;
    double longitude = 0;
    private GPSTracker gpsTracker;
    private DialogClass dialogClass;
    private ParseData parseData;
    private String jsonUrl = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_reservation);

        activity = HotelReservationActivity.this;
        gpsTracker = new GPSTracker(activity);
        dialogClass = new DialogClass(activity,0);
        init();
    }




    private void init() {

        ll_Back = (LinearLayout) findViewById(R.id.back_layout);
        iv_Back = (ImageView) findViewById(R.id.back_imageview);

        tv_Title = (TextView) findViewById(R.id.title_textview);

        tv_Title.setText("Hotel Reservation");

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        // Layout Managers:
        layoutManager = new HPLinearLayoutManager(activity);
        layoutManager.setOrientation(HPLinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        arrayList = new ArrayList<>();

        adapter = new HotelsBookingAdapter(arrayList,activity);
        recyclerView.setAdapter(adapter);

        ll_Back.setOnClickListener(activity);
        iv_Back.setOnClickListener(activity);

        if (gpsTracker.canGetLocation())
        {
            latitude = gpsTracker.getLatitude();
            longitude = gpsTracker.getLongitude();

            jsonUrl = jsonUrl + "location="+latitude+","+longitude+"&radius=5000&type=hotel&key="+ Constants.GOOGLE_API_KEY;

            Log.e("call","jsonUrl = "+jsonUrl);
            if (Global.isNetworkconn(activity))
            {
                dialogClass.showDialog();
                parseData = new ParseData(jsonUrl);
                parseData.execute();
            }
            else
            {
                InternetDialog internetDialog = new InternetDialog(activity);
                internetDialog.showDialog("Please check your internet connection!");
            }
        }
        else
        {
            InternetDialog internetDialog = new InternetDialog(activity);
            internetDialog.showDialog("Please first on your gps location.");
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.back_layout:
                onBackPressed();
                break;

            case R.id.back_imageview:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);
    }

    @Override
    protected void onResume() {
        super.onResume();

        TicktocApplication.setCurrentActivity(activity);

    }

    // Get the google API result and convert into JSON format.
    private JSONObject GetRestaurant(String Url) {
        try
        {
            HttpClient httpclient = new DefaultHttpClient();
            HttpGet httppost = new HttpGet(Url);
            HttpResponse response = httpclient.execute(httppost);
            String jsonResult = Utility.inputStreamToString(response.getEntity().getContent()).toString();
            JSONObject json = new JSONObject(jsonResult);
            return json;
        }
        catch (Exception e)
        {
            // TODO: handle exception
        }
        return null;
    }

    // Parse the google API result and loaded into list
    public class ParseData extends AsyncTask<Void, Integer, JSONObject>
    {
        private String url;
        public ParseData(String url)
        {
            this.url=url;
            arrayList.clear();
        }

        @Override
        protected JSONObject doInBackground(Void... params) {
            // TODO Auto-generated method stub
            JSONObject json = null;
            try
            {
                Log.e("call","doInBackground ParseData = "+url);

                json = GetRestaurant(url);
            }
            catch (Exception e)
            {
                json = null;
                Log.e("call","Exception = "+e.getMessage());
            }
            return json;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject)
        {
            try
            {
                Log.e("call","response = "+jsonObject);

                if (jsonObject!=null)
                {
                    if (jsonObject.has("status"))
                    {
                        if (jsonObject.getString("status").equalsIgnoreCase("OK"))
                        {
                            JSONArray results = jsonObject.getJSONArray("results");

                            if (results!=null && results.length()>0)
                            {
                                for (int i=0; i<results.length(); i++)
                                {
                                    String place_id="",name="",address="",contact="",distance="",image="",ratting="";
                                    double source_latitude=0,source_longitude=0;

                                    JSONObject data = results.getJSONObject(i);

                                    if (data!=null)
                                    {
                                        if (data.has("geometry"))
                                        {
                                            JSONObject geometry = data.getJSONObject("geometry");

                                            if (geometry!=null)
                                            {
                                                if (geometry.has("location"))
                                                {
                                                    JSONObject location = geometry.getJSONObject("location");

                                                    if (location!=null)
                                                    {
                                                        if (location.has("lat"))
                                                        {
                                                            source_latitude = location.getDouble("lat");
                                                        }

                                                        if (location.has("lng"))
                                                        {
                                                            source_longitude = location.getDouble("lng");
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        distance = getDistanceFromLatLonInKm(latitude,longitude,source_latitude,source_longitude);

                                        if (data.has("place_id"))
                                        {
                                            place_id = data.getString("place_id");
                                        }

                                        if (data.has("name"))
                                        {
                                            name = data.getString("name");
                                        }

                                        if (data.has("vicinity"))
                                        {
                                            address = data.getString("vicinity");
                                        }

                                        if (data.has("icon"))
                                        {
                                            image = data.getString("icon");
                                        }

                                        if (data.has("rating"))
                                        {
                                            ratting = data.getString("rating");
                                        }

                                        arrayList.add(new HotelBookingBeen(place_id,name,address,contact,distance,image,ratting,latitude,longitude, source_latitude, source_longitude));
                                    }
                                    else
                                    {
                                        Log.e("call","data null at position = "+i);
                                    }
                                }

                                Log.e("call","arrayList.size() = "+arrayList.size());

                                dialogClass.hideDialog();

                                if (arrayList.size()>0)
                                {
                                    Collections.sort(arrayList, new Comparator<HotelBookingBeen>(){
                                        public int compare(HotelBookingBeen obj1, HotelBookingBeen obj2) {
                                            return obj1.getDistance().compareToIgnoreCase(obj2.getDistance());
                                        }
                                    });

                                    adapter.notifyDataSetChanged();
                                    Log.e("call","success fully set data");
                                }
                            }
                            else
                            {
                                dialogClass.hideDialog();
                                Log.e("call","0 result found");
                            }
                        }
                        else
                        {
                            dialogClass.hideDialog();
                            Log.e("call","status not ok");
                        }
                    }
                    else
                    {
                        dialogClass.hideDialog();
                        Log.e("call","status not found");
                    }
                }
                else
                {
                    dialogClass.hideDialog();
                    Log.e("call","jsonObject null");
                }
            }
            catch (Exception e)
            {
                dialogClass.hideDialog();
                Log.e("call","onPost exception = "+e.getMessage());
            }
        }
    }

    private String getDistanceFromLatLonInKm(double lat1, double lon1, double lat2, double lon2) {

        double R = 6371; // Radius of the earth in km
        double dLat = deg2rad(lat2-lat1);  // deg2rad below
        double dLon = deg2rad(lon2-lon1);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *  Math.sin(dLon/2) * Math.sin(dLon/2);

        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double d = R * c; // Distance in km
        String getDistance;

        /*if ( d > 1)
        {

            Log.d("MainActivity", "DistanceKm:- " + d );
            getDistance = String.format("%.1f",d) + " km";
        }
        else
        {
            d = (int) (d*1000);
            Log.d("MainActivity", "DistanceMeter:- " + d );
            getDistance = (int) d + " m";
        }*/
        getDistance = String.format("%.1f",d) + " km";

        return getDistance;

    }

    private double deg2rad(double deg) {
        return deg * (Math.PI/180);
    }
}

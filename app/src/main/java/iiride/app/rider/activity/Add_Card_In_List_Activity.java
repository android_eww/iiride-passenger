package iiride.app.rider.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.IsoDep;
import android.os.Bundle;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.github.devnied.emvnfccard.model.EmvCard;
import com.github.devnied.emvnfccard.parser.EmvParser;
import com.github.devnied.emvnfccard.utils.AtrUtils;

import iiride.app.rider.R;
import iiride.app.rider.application.TicktocApplication;
import iiride.app.rider.been.CreditCard_List_Been;
import iiride.app.rider.comman.Common;
import iiride.app.rider.comman.SessionSave;
import iiride.app.rider.comman.WebServiceAPI;
import iiride.app.rider.nfc.IContentActivity;
import iiride.app.rider.nfc.IRefreshable;
import iiride.app.rider.nfc.NFCUtils;
import iiride.app.rider.nfc.Provider;
import iiride.app.rider.nfc.SimpleAsyncTask;
import iiride.app.rider.other.DialogClass;
import iiride.app.rider.other.Global;
import iiride.app.rider.other.InternetDialog;
import iiride.app.rider.view.MySnackBar;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import fr.devnied.bitlib.BytesUtils;
import io.card.payment.CardIOActivity;
import io.card.payment.CardType;
import io.card.payment.CreditCard;


public class Add_Card_In_List_Activity extends AppCompatActivity implements View.OnClickListener, IContentActivity {

    public static Add_Card_In_List_Activity activity;
    private List<CreditCard_List_Been> cardList = new ArrayList<CreditCard_List_Been>();

    private LinearLayout main_layout, ll_cardNumber, ll_valid_through, ll_cvv;
    private ImageView iv_CardImage;
    private TextView tv_valid_through, tv_add_payment_method;
    private EditText et_CardNumber, et_cvv,et_Alias;
    private LinearLayout ll_Back;
    private ImageView iv_Back;
    private TextView tv_Title;

    ///for Bottom Dialog
    private BottomSheetDialog mBottomSheetDialog;
    private NumberPicker numberPickerMonth,numberPickerYear;
    private String[] valueMonth ;
    private String[] valueYear ;
    private String strExpiry = "";
    private String cardNumberParam = "";
    int MonthInNumber, YearInNumber ;
    private String[] monthName = {
            "Jan", "Feb", "Mar",
            "Apr", "May", "Jun",
            "Jul", "Aug", "Sep",
            "Oct", "Nov", "Dec"
    };

    private View view_cvv, view_valid_through, view_cardNumber;

    boolean validornot = false;
    private MySnackBar mySnackBar;
    private AQuery aQuery;
    private DialogClass dialogClass;
    private String strCardList = "";
    private JSONObject jsonObject = null;
    private String from = "";
    private CardView scanCard,scanNFCCard;

    protected static final String TAG = MainActivity.class.getSimpleName();

    private static final int REQUEST_SCAN = 100;
    private static final int REQUEST_AUTOTEST =  200;
    private String strCardNumber,strCardType,strCardExpiry;

    private NFCUtils mNfcUtils;
    private ProgressDialog mDialog;
    private AlertDialog mAlertDialog;
    private Provider mProvider = new Provider();
    private EmvCard mReadCard;
    private WeakReference<IRefreshable> mRefreshableContent;
    private byte[] lastAts;
    public static int resumeFlag = 1;

    public static boolean isScan = false;
    public static int clickFlag = 0;

    String year="",month="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_add_card_in_list);

        activity = Add_Card_In_List_Activity.this;
        resumeFlag = 1;
        isScan = false;
        mNfcUtils = new NFCUtils(this);
        mySnackBar = new MySnackBar(activity);
        aQuery = new AQuery(activity);
        dialogClass = new DialogClass(activity,0);
        strCardList = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_CARD_LIST,activity);
        clickFlag = 0;
        cardNumberParam = "";

        if (this.getIntent()!=null)
        {
            if (this.getIntent().getStringExtra("from")!=null)
            {
                from = this.getIntent().getStringExtra("from");
            }
            else
            {
                from = "";
            }
        }
        else
        {
            from = "";
        }

        try
        {
            if (strCardList!=null && !strCardList.equalsIgnoreCase(""))
            {
                jsonObject = new JSONObject(strCardList);
            }
            else
            {
                jsonObject = null;
            }
        }
        catch (Exception e)
        {
            Log.e("call","exception = "+e.getMessage());
        }

        initUI();
    }

    private void initUI() {
        ll_Back = (LinearLayout) findViewById(R.id.back_layout);
        iv_Back = (ImageView) findViewById(R.id.back_imageview);

        tv_Title = (TextView) findViewById(R.id.title_textview);

        tv_Title.setText("Cards");

        scanCard = (CardView) findViewById(R.id.card_scan);
        scanNFCCard = (CardView) findViewById(R.id.card_scan_nfc);

        main_layout = (LinearLayout) findViewById(R.id.main_layout);
        ll_cardNumber = (LinearLayout) findViewById(R.id.ll_cardNumber);
        ll_valid_through = (LinearLayout) findViewById(R.id.ll_valid_through);
        ll_cvv = (LinearLayout) findViewById(R.id.ll_cvv);
        iv_CardImage = (ImageView) findViewById(R.id.iv_CardImage);

        tv_valid_through = (TextView) findViewById(R.id.tv_valid_through);
        tv_add_payment_method = (TextView) findViewById(R.id.tv_add_payment_method);

        view_cvv = (View) findViewById(R.id.view_cvv);
        view_valid_through = (View) findViewById(R.id.view_valid_through);
        view_cardNumber = (View) findViewById(R.id.view_cardNumber);

        et_CardNumber = (EditText) findViewById(R.id.et_CardNumber);
        et_cvv = (EditText) findViewById(R.id.et_cvv);
        et_Alias = (EditText) findViewById(R.id.et_alias);

        et_CardNumber.setFilters(new InputFilter[] { new InputFilter.LengthFilter(19) });
        et_CardNumber.addTextChangedListener(new FourDigitCardFormatWatcher());

        tv_valid_through.setOnClickListener(activity);
        ll_valid_through.setOnClickListener(activity);

        ll_Back.setOnClickListener(activity);
        iv_Back.setOnClickListener(activity);
        scanCard.setOnClickListener(activity);
        scanNFCCard.setOnClickListener(activity);

        tv_add_payment_method.setOnClickListener(activity);

        et_CardNumber.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                isScan = false;
                Log.e("call","isScan = "+isScan);
                return false;
            }
        });

        et_cvv.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                view_cvv.setBackgroundColor(ContextCompat.getColor(activity, R.color.colorRed));
                view_cardNumber.setBackgroundColor(ContextCompat.getColor(activity, R.color.colorBlack));

                return false;
            }
        });

        view_cardNumber.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                view_cardNumber.setBackgroundColor(ContextCompat.getColor(activity, R.color.colorRed));
                view_cvv.setBackgroundColor(ContextCompat.getColor(activity, R.color.colorBlack));

                return false;
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch(v.getId())
        {
            case R.id.back_layout:
                onBackPressed();
                break;

            case R.id.back_imageview:
                onBackPressed();
                break;

            case R.id.tv_valid_through:
                showBottomSheetDailog_payment();
                break;

            case R.id.ll_valid_through:
                showBottomSheetDailog_payment();
                break;

            case R.id.tv_add_payment_method:
                LoginFlagForCurrentUser();
                break;

            case R.id.card_scan:
                cardNumberParam = "";
                strExpiry = "";
                et_CardNumber.setText("");
                et_Alias.setText("");
                et_cvv.setText("");
                tv_valid_through.setText("");
                scanCard();
                break;

            case R.id.card_scan_nfc:
                clickFlag = 1;
                mNfcUtils.enableDispatch();
                startNFC();
                break;
        }
    }

    public void startNFC()
    {
        cardNumberParam = "";
        strExpiry = "";
        et_CardNumber.setText("");
        et_Alias.setText("");
        et_cvv.setText("");
        tv_valid_through.setText("");
        scanNFCCard();
    }

    public void scanCard()
    {
        isScan = true;
        resumeFlag = 0;
        Intent intent = new Intent(Add_Card_In_List_Activity.this, CardIOActivity.class)
                .putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, true)
                .putExtra(CardIOActivity.EXTRA_SCAN_EXPIRY, true)
                .putExtra(CardIOActivity.EXTRA_GUIDE_COLOR, Color.TRANSPARENT)
                .putExtra("debug_autoAcceptResult", true);;
        startActivityForResult(intent, REQUEST_SCAN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.v(TAG, "onActivityResult(" + requestCode + ", " + resultCode + ", " + data + ")");


        if ((requestCode == REQUEST_SCAN || requestCode == REQUEST_AUTOTEST) && data != null
                && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT))
        {
            CreditCard result = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);

            Log.e(TAG, "Rusult: " + result);
            if (result != null)
            {

                cardNumberParam = result.getFormattedCardNumber().replace(" ","");
                strCardNumber = getFormattedCardNumber(cardNumberParam);


                CardType cardType = result.getCardType();
                strCardType = cardType.name();

                String year = result.expiryYear + "";

                if (year!=null && year.length()>2)
                {
                    year = year.substring(year.length()-2);
                }

                String month = getMonth(result.expiryMonth);

                if (month!=null && month.length()>3)
                {
                    month = month.substring(0,3);
                }

                if (result.expiryMonth < 10)
                {
                    strExpiry = "0" + result.expiryMonth + "/" + year;
                }
                else
                {
                    strExpiry = result.expiryMonth + "/" + year;
                }

                strCardExpiry = month + ", " + result.expiryYear;
                et_Alias.setText(strCardType);
                et_CardNumber.setText(strCardNumber);
                tv_valid_through.setText(strCardExpiry);
            }
        }

        Log.e(TAG, "strCardNumber: " + strCardNumber);
        Log.e(TAG, "strCardType: " + strCardType);
        Log.e(TAG, "strCardExpiry: " + strCardExpiry);

    }

    public String getMonth(int month) {
        return new DateFormatSymbols().getMonths()[month-1];
    }

    private void cardScan() {

        onNewIntent(getIntent());
    }

    public void LoginFlagForCurrentUser()
    {
        if (TextUtils.isEmpty(et_CardNumber.getText().toString().trim()))
        {
            mySnackBar.showSnackBar(main_layout,getString(R.string.add_payment_error_message));
        }
        else if (TextUtils.isEmpty(tv_valid_through.getText().toString().trim()))
        {
            mySnackBar.showSnackBar(main_layout,getString(R.string.add_payment_error_message));
        }
        else if (TextUtils.isEmpty(et_cvv.getText().toString().trim()))
        {
            mySnackBar.showSnackBar(main_layout,getString(R.string.add_payment_error_message));
        }
        else if (TextUtils.isEmpty(et_Alias.getText().toString().trim()))
        {
            mySnackBar.showSnackBar(main_layout,getString(R.string.enter_bank_name));
        }
        else if(!checkDateValidation(Integer.parseInt(year),Integer.parseInt(month)))
        {
            mySnackBar.showSnackBar(main_layout, getString(R.string.plaese_enter_vali_expiry_date));
        }
        else
        {
            Log.e("length","et_CardNumber"+et_CardNumber.getText().toString().trim().length());
            if(et_CardNumber.getText().length()>11) {
                if (validornot)
                {
                    if (Global.isNetworkconn(activity))
                    {
                        call_AddCardApi();
                    }
                    else
                    {
                        InternetDialog internetDialog = new InternetDialog(activity);
                        internetDialog.showDialog("Please check your internet connection!");
                    }
                }
                else
                {
//                    displayValidationMessage(getResources().getString(R.string.invalid_card_number));
                    mySnackBar.showSnackBar(main_layout,getString(R.string.invalid_card_number));
                }
            }
            else
            {
//                displayValidationMessage(getResources().getString(R.string.invalid_card_number));
                mySnackBar.showSnackBar(main_layout,getString(R.string.invalid_card_number));
            }
        }
    }

    public void call_AddCardApi()
    {


        dialogClass.showDialog();
        String url = WebServiceAPI.API_ADD_CARD;

        Map<String, Object> params = new HashMap<String, Object>();
        params.put(WebServiceAPI.PARAM_PASSENGER_ID, SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID,activity));

        if (isScan==true)
        {
            params.put(WebServiceAPI.PARAM_CARD_NUMBER, cardNumberParam);
        }
        else
        {
            params.put(WebServiceAPI.PARAM_CARD_NUMBER, et_CardNumber.getText().toString().replace(" ",""));
        }

        params.put(WebServiceAPI.PARAM_CARD_EXPIRY, strExpiry);
        params.put(WebServiceAPI.PARAM_CARD_CVV, et_cvv.getText().toString());
        params.put(WebServiceAPI.PARAM_ALIAS, et_Alias.getText().toString());

        Log.e("call", "url = " + url);
        Log.e("call", "params = " + params);

        aQuery.ajax(url.trim(), params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e("responseCode", " = " + responseCode);
                    Log.e("Response", " = " + json);

                    if (json!=null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                String message = "Card saved successfully";
                                if (json.has("message"))
                                {
                                    message = json.getString("message");
                                }
                                JSONArray cards = null;
                                if (json.has("cards"))
                                {
                                    cards = json.getJSONArray("cards");

                                    if (cards!=null && cards.length()>0)
                                    {
                                        saveCard(cards);
                                    }
                                }
                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_CARD_LIST,json.toString(),activity);
                                dialogClass.hideDialog();
                                showSuccessDialog(message,cards);
                            }
                            else
                            {
                                String message = "Something went wrong prlease try again later";
                                if (json.has("message"))
                                {
                                    message = json.getString("message");
                                }
                                dialogClass.hideDialog();
                                InternetDialog internetDialog = new InternetDialog(activity);
                                internetDialog.showDialog(message);
                            }
                        }
                        else
                        {
                            dialogClass.hideDialog();
                            InternetDialog internetDialog = new InternetDialog(activity);
                            internetDialog.showDialog("Something went wrong prlease try again later");
                        }
                    }
                    else
                    {
                        Log.e("call","json null");
                        dialogClass.hideDialog();
                        InternetDialog internetDialog = new InternetDialog(activity);
                        internetDialog.showDialog("Something went wrong prlease try again later");
                    }
                }
                catch (Exception e)
                {
                    Log.e("Exception","Exception "+e.toString());
                    dialogClass.hideDialog();
                    InternetDialog internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog("Something went wrong prlease try again later");
                }
            }

        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    public void saveCard(JSONArray cardsArray)
    {
        try
        {
            if (jsonObject!=null && cardsArray.length()>0)
            {
                if (jsonObject.has("cards"))
                {
                    JSONArray cards = jsonObject.getJSONArray("cards");
                    cards.put(cardsArray.get(0));
                    Log.e("jsonObject"," = "+jsonObject);
                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_CARD_LIST,jsonObject.toString(),activity);
                }
                else
                {
                    Log.e("call","no cards found");
                }
            }
            else
            {
                Log.e("call","json null");
            }
        }
        catch (Exception e)
        {
            Log.e("call","Exception "+e.getMessage());
        }
    }

    public void showSuccessDialog(String message, final JSONArray cards)
    {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.my_dialog_class);

        TextView tv_Ok = (TextView) dialog.findViewById(R.id.Ok);
        TextView tv_Message = (TextView) dialog.findViewById(R.id.message);
        LinearLayout ll_Ok = (LinearLayout) dialog.findViewById(R.id.ok_layout);

        tv_Message.setText(message);

        ll_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                try
                {
                    String cardType = "",cardNumber="",cardId = "";
                    if (cards!=null && cards.length()>0)
                    {
                        JSONObject jsonObject = cards.getJSONObject(0);

                        if (jsonObject.has("Type"))
                        {
                            cardType = jsonObject.getString("Type");
                        }
                        else
                        {
                            cardType = "";
                        }

                        if (jsonObject.has("CardNum2"))
                        {
                            cardNumber= jsonObject.getString("CardNum2");
                        }
                        else
                        {
                            cardNumber= "";
                        }

                        if (jsonObject.has("Id"))
                        {
                            cardId= jsonObject.getString("Id");
                        }
                        else
                        {
                            cardId= "";
                        }
                    }

                    if (from!=null && !from.equalsIgnoreCase("") && from.equalsIgnoreCase("Wallet_Balance_TopUp_Activity"))
                    {
                        Wallet_Balance_TopUp_Activity.cardId= cardId;
                        Wallet_Balance_TopUp_Activity.cardNumber= cardNumber;
                        Wallet_Balance_TopUp_Activity.cardType = cardType;
                    }
                    else if (from!=null && !from.equalsIgnoreCase("") && from.equalsIgnoreCase("Wallet_Balance_TransferToBank_Activity"))
                    {
                        Wallet_Balance_TransferToBank_Activity.cardId= cardId;
                        Wallet_Balance_TransferToBank_Activity.cardNumber= cardNumber;
                        Wallet_Balance_TransferToBank_Activity.cardType = cardType;
                    }
                    else
                    {
                        Wallet_Balance_TopUp_Activity.cardId= "";
                        Wallet_Balance_TopUp_Activity.cardNumber= "";
                        Wallet_Balance_TopUp_Activity.cardType = "";

                        Wallet_Balance_TransferToBank_Activity.cardId= "";
                        Wallet_Balance_TransferToBank_Activity.cardNumber= "";
                        Wallet_Balance_TransferToBank_Activity.cardType = "";

                        Wallet_Add_Cards_Activity.callApi = true;
                    }

                    finish();
                }
                catch (Exception e)
                {
                    Wallet_Balance_TopUp_Activity.cardId= "";
                    Wallet_Balance_TopUp_Activity.cardNumber= "";
                    Wallet_Balance_TopUp_Activity.cardType = "";

                    Wallet_Balance_TransferToBank_Activity.cardId= "";
                    Wallet_Balance_TransferToBank_Activity.cardNumber= "";
                    Wallet_Balance_TransferToBank_Activity.cardType = "";

                    Log.e("call","exception = "+e.getMessage());
                }
            }
        });

        tv_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                try
                {
                    String cardType = "",cardNumber="",cardId = "";
                    if (cards!=null && cards.length()>0)
                    {
                        JSONObject jsonObject = cards.getJSONObject(0);

                        if (jsonObject.has("Type"))
                        {
                            cardType = jsonObject.getString("Type");
                        }
                        else
                        {
                            cardType = "";
                        }

                        if (jsonObject.has("CardNum2"))
                        {
                            cardNumber= jsonObject.getString("CardNum2");
                        }
                        else
                        {
                            cardNumber= "";
                        }

                        if (jsonObject.has("Id"))
                        {
                            cardId= jsonObject.getString("Id");
                        }
                        else
                        {
                            cardId= "";
                        }
                    }

                    if (from!=null && !from.equalsIgnoreCase("") && from.equalsIgnoreCase("Wallet_Balance_TopUp_Activity"))
                    {
                        Wallet_Balance_TopUp_Activity.cardId= cardId;
                        Wallet_Balance_TopUp_Activity.cardNumber= cardNumber;
                        Wallet_Balance_TopUp_Activity.cardType = cardType;
                    }
                    else if (from!=null && !from.equalsIgnoreCase("") && from.equalsIgnoreCase("Wallet_Balance_TransferToBank_Activity"))
                    {
                        Wallet_Balance_TransferToBank_Activity.cardId= cardId;
                        Wallet_Balance_TransferToBank_Activity.cardNumber= cardNumber;
                        Wallet_Balance_TransferToBank_Activity.cardType = cardType;
                    }
                    else
                    {
                        Wallet_Balance_TopUp_Activity.cardId= "";
                        Wallet_Balance_TopUp_Activity.cardNumber= "";
                        Wallet_Balance_TopUp_Activity.cardType = "";

                        Wallet_Balance_TransferToBank_Activity.cardId= "";
                        Wallet_Balance_TransferToBank_Activity.cardNumber= "";
                        Wallet_Balance_TransferToBank_Activity.cardType = "";

                        Wallet_Add_Cards_Activity.callApi = true;
                    }

                    finish();
                }
                catch (Exception e)
                {
                    Wallet_Balance_TopUp_Activity.cardId= "";
                    Wallet_Balance_TopUp_Activity.cardNumber= "";
                    Wallet_Balance_TopUp_Activity.cardType = "";

                    Wallet_Balance_TransferToBank_Activity.cardId= "";
                    Wallet_Balance_TransferToBank_Activity.cardNumber= "";
                    Wallet_Balance_TransferToBank_Activity.cardType = "";

                    Log.e("call","exception = "+e.getMessage());
                }
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        dialog.show();
    }

    public void showBottomSheetDailog_payment() {

        View view = getLayoutInflater().inflate(R.layout.bottom_sheet_payment, null);
        mBottomSheetDialog = new BottomSheetDialog(activity);
        mBottomSheetDialog.setContentView(view);


        final TextView tv_Ok, tv_Cancle;

        tv_Ok = (TextView) view.findViewById(R.id.bottom_sheet_payment_ok_textview);
        tv_Cancle = (TextView) view.findViewById(R.id.bottom_sheet_payment_cancle_textview);
        numberPickerMonth = (NumberPicker) view.findViewById(R.id.numberPickerMonth);
        numberPickerYear = (NumberPicker) view.findViewById(R.id.numberPickerYear);

        numberPickerMonth.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        numberPickerYear.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

        setDividerColor(numberPickerMonth,ContextCompat.getColor(activity, R.color.colorRed));
        setDividerColor(numberPickerYear,ContextCompat.getColor(activity, R.color.colorRed));

        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH)+1;

        Log.e("Date","year"+year+"month"+month);

        numberPickerMonth.setMinValue(1);
        numberPickerMonth.setMaxValue(12);
        numberPickerYear.setMinValue(year);
        numberPickerYear.setMaxValue(year+5);

        valueMonth = new String[12];
        valueYear = new String[6];

        for(int i=0; i<12; i++)
        {
            valueMonth[i] = monthName[i]+"";
            Log.d("valueNumber","valueMonth : "+ valueMonth[i]);
        }

        for(int i=0; i<6; i++)
        {
            valueYear[i] = (year+i)+"";
            Log.d("valueNumber","valueYear : "+ valueYear[i]);
        }

        numberPickerMonth.setValue(month);
        numberPickerMonth.setDisplayedValues(monthName);
        numberPickerYear.setValue(year);


        tv_Cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mBottomSheetDialog!=null)
                {
                    mBottomSheetDialog.dismiss();
                }
            }
        });

        tv_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBottomSheetDialog!=null)
                {
                    MonthInNumber = numberPickerMonth.getValue();
                    YearInNumber = numberPickerYear.getValue();

                    String strYear = YearInNumber + "";

                    if (strYear.length()>2)
                    {
                        strYear = strYear.substring(strYear.length() - 2);
                    }

                    if (MonthInNumber < 10)
                    {
                        strExpiry = "0" + MonthInNumber + "/" + strYear;
                    }
                    else
                    {
                        strExpiry = MonthInNumber + "/" + strYear;
                    }

                    Log.e("strExpiry"," = "+strExpiry);
                    Log.e("MonthInNumber"," = "+MonthInNumber);
                    Log.e("YearInNumber"," = "+strYear);

                    tv_valid_through.setText(monthName[MonthInNumber-1]);
                    tv_valid_through.append(", "+YearInNumber);

                    setValue(MonthInNumber,YearInNumber);

                    mBottomSheetDialog.dismiss();
                }
            }
        });

        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mBottomSheetDialog = null;
            }
        });

        mBottomSheetDialog.show();
    }

    private void setValue(int monthInNumber, int yearInNumber)
    {
        year = String.valueOf(yearInNumber);
        month = String.valueOf(monthInNumber);
    }

    private void setDividerColor(NumberPicker picker, int color) {

        java.lang.reflect.Field[] pickerFields = NumberPicker.class.getDeclaredFields();
        for (java.lang.reflect.Field pf : pickerFields) {
            if (pf.getName().equals("mSelectionDivider")) {
                pf.setAccessible(true);
                try {
                    ColorDrawable colorDrawable = new ColorDrawable(color);
                    pf.set(picker, colorDrawable);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                }
                catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    public class FourDigitCardFormatWatcher implements TextWatcher {

        // Change this to what you want... ' ', '-' etc..
        private static final char space = ' ';

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            // Remove spacing char
            if (s.length() > 0 && (s.length() % 5) == 0) {
                final char c = s.charAt(s.length() - 1);
                if (space == c) {
                    s.delete(s.length() - 1, s.length());
                }
            }
            // Insert char where needed.
            if (s.length() > 0 && (s.length() % 5) == 0) {
                char c = s.charAt(s.length() - 1);
                // Only if its a digit where there should be a space we insert a space
                if (Character.isDigit(c) && TextUtils.split(s.toString(), String.valueOf(space)).length <= 3) {
                    s.insert(s.length() - 1, String.valueOf(space));
                }
            }
            FindCardType(s);
        }
    }

    public void FindCardType(CharSequence s) {

        String valid = "INVALID";

        if (isScan == true)
        {
            s = cardNumberParam;
        }

        Log.e("call","isScan = "+isScan + " = "+s.toString());
        if (s.toString().length() > 5)
        {
            String number = s.toString().replace(" ", "");
            String digit1 = number.substring(0, 1);
            String digit2 = number.substring(0, 2);
            String digit3 = number.substring(0, 3);
            String digit4 = number.substring(0, 4);

            iv_CardImage.setBackgroundResource(R.drawable.card_dummy);
            et_CardNumber.setTextColor(ContextCompat.getColor(activity, R.color.colorBlack));

            if (digit1.equals("4"))
            {
                Log.e("call","1111111111111");
                iv_CardImage.setBackgroundResource(R.drawable.card_visa);
                if (number.length() == 13 || number.length() == 16) {
                    valid = "VISA";
                    validornot = validCCNumber(number);

                    if (number.length() > 12) {
                        if (validornot) {
                            et_CardNumber.setTextColor(ContextCompat.getColor(activity, R.color.colorGreen));
                            iv_CardImage.setBackgroundResource(R.drawable.card_visa);
                        } else {
                            et_CardNumber.setTextColor(ContextCompat.getColor(activity, R.color.colorRed));
                            mySnackBar.showSnackBar(main_layout,getString(R.string.card_is_not_valid));
                            iv_CardImage.setBackgroundResource(R.drawable.card_dummy);
                        }
                    }
                }
            } else if (digit4.equalsIgnoreCase("5018") || digit4.equalsIgnoreCase("5020") || digit4.equalsIgnoreCase("5038") || digit4.equalsIgnoreCase("5612") || digit4.equalsIgnoreCase("5893")
                    || digit4.equalsIgnoreCase("6304") || digit4.equalsIgnoreCase("6759") || digit4.equalsIgnoreCase("6761") || digit4.equalsIgnoreCase("6762") || digit4.equalsIgnoreCase("6763")
                    || digit4.equalsIgnoreCase("0604") || digit4.equalsIgnoreCase("6390"))
            {
                Log.e("call","22222222222222");
                iv_CardImage.setBackgroundResource(R.drawable.card_maestro);
                if (number.length() == 16) {
                    valid = "MAESTRO";
                    Log.e("MAESTRO", "MAESTRO");
                    validornot = validCCNumber(number);

                    if (number.length() == 16) {
                        if (validornot) {
                            et_CardNumber.setTextColor(ContextCompat.getColor(activity, R.color.colorGreen));
                            iv_CardImage.setBackgroundResource(R.drawable.card_maestro);

                        } else {
                            et_CardNumber.setTextColor(ContextCompat.getColor(activity, R.color.colorRed));
                            mySnackBar.showSnackBar(main_layout,getString(R.string.card_is_not_valid));
                            iv_CardImage.setBackgroundResource(R.drawable.card_dummy);
                        }
                    }
                }
            }
            else if (digit2.equals("34") || digit2.equals("37"))
            {
                Log.e("call","33333333333333333");
                iv_CardImage.setBackgroundResource(R.drawable.card_american);
                if (number.length() == 15) {
                    valid = "AMERICAN_EXPRESS";
                    validornot = validCCNumber(number);

                    if (number.length() == 15) {
                        if (validornot) {
                            et_CardNumber.setTextColor(ContextCompat.getColor(activity, R.color.colorGreen));
                            iv_CardImage.setBackgroundResource(R.drawable.card_american);

                        } else {
                            et_CardNumber.setTextColor(ContextCompat.getColor(activity, R.color.colorRed));
                            mySnackBar.showSnackBar(main_layout,getString(R.string.card_is_not_valid));
                            iv_CardImage.setBackgroundResource(R.drawable.card_dummy);
                        }
                    }
                }
            }
            else if (digit2.equals("36") || digit2.equals("38") || (digit3.compareTo("300") >= 0 && digit3.compareTo("305") <= 0))
            {
                Log.e("call","444444444444444444");
                iv_CardImage.setBackgroundResource(R.drawable.card_dinner);
                if (number.length() == 14) {
                    valid = "DINERS_CLUB";
                    validornot = validCCNumber(number);

                    if (number.length() == 14) {
                        if (validornot) {
                            iv_CardImage.setBackgroundResource(R.drawable.card_dinner);
                            et_CardNumber.setTextColor(ContextCompat.getColor(activity, R.color.colorGreen));

                        } else {
                            et_CardNumber.setTextColor(ContextCompat.getColor(activity, R.color.colorRed));
                            mySnackBar.showSnackBar(main_layout,getString(R.string.card_is_not_valid));
                            iv_CardImage.setBackgroundResource(R.drawable.card_dummy);
                        }
                    }
                }

            }
            else if (digit1.equals("6"))
            {
                Log.e("call","5555555555555555");
                iv_CardImage.setBackgroundResource(R.drawable.card_discover);
                if (number.length() == 16) {
                    valid = "DISCOVER";
                    validornot = validCCNumber(number);

                    if (number.length() == 16) {
                        if (validornot) {
                            iv_CardImage.setBackgroundResource(R.drawable.card_discover);
                            et_CardNumber.setTextColor(ContextCompat.getColor(activity, R.color.colorGreen));

                        } else {
                            et_CardNumber.setTextColor(ContextCompat.getColor(activity, R.color.colorRed));
                            mySnackBar.showSnackBar(main_layout,getString(R.string.card_is_not_valid));
                            iv_CardImage.setBackgroundResource(R.drawable.card_dummy);
                        }
                    }
                }

            }
            else if (digit2.equals("35"))
            {
                Log.e("call","6666666666666666666");
                iv_CardImage.setBackgroundResource(R.drawable.card_jcbs);
                if (number.length() == 16 || number.length() == 17 || number.length() == 18 || number.length() == 19) {
                    valid = "JBC";
                    validornot = validCCNumber(number);

                    if (number.length() > 15) {
                        if (validornot) {
                            iv_CardImage.setBackgroundResource(R.drawable.card_jcbs);
                            et_CardNumber.setTextColor(ContextCompat.getColor(activity, R.color.colorGreen));

                        } else {
                            et_CardNumber.setTextColor(ContextCompat.getColor(activity, R.color.colorRed));
                            mySnackBar.showSnackBar(main_layout,getString(R.string.card_is_not_valid));
                            iv_CardImage.setBackgroundResource(R.drawable.card_dummy);
                        }
                    }
                }

            }
            else if (digit2.compareTo("51") >= 0 && digit2.compareTo("55") <= 0 || digit1.equalsIgnoreCase("2"))
            {
                Log.e("call","7777777777777");
                iv_CardImage.setBackgroundResource(R.drawable.card_master);
                if (number.length() == 16)
                    valid = "MASTERCARD";
                validornot = validCCNumber(number);

                if (number.length() == 16) {
                    if (validornot) {
                        iv_CardImage.setBackgroundResource(R.drawable.card_master);
                        et_CardNumber.setTextColor(ContextCompat.getColor(activity, R.color.colorGreen));

                    } else {
                        et_CardNumber.setTextColor(ContextCompat.getColor(activity, R.color.colorRed));
                        mySnackBar.showSnackBar(main_layout,getString(R.string.card_is_not_valid));
                        iv_CardImage.setBackgroundResource(R.drawable.card_dummy);
                    }
                }
            }
            else
            {
                Log.e("call","99999999999999");
                validornot = false;
                iv_CardImage.setBackgroundResource(R.drawable.card_dummy);
            }
        }
        else
        {
            Log.e("call","8888888888888");
            validornot = false;
            iv_CardImage.setBackgroundResource(R.drawable.card_dummy);
        }

    }

    public static boolean validCCNumber(String n) {
        try {

            int j = n.length();

            String [] s1 = new String[j];
            for (int i=0; i < n.length(); i++) s1[i] = "" + n.charAt(i);

            int checksum = 0;

            for (int i=s1.length-1; i >= 0; i-= 2) {
                int k = 0;

                if (i > 0) {
                    k = Integer.valueOf(s1[i-1]).intValue() * 2;
                    if (k > 9) {
                        String s = "" + k;
                        k = Integer.valueOf(s.substring(0,1)).intValue() +
                                Integer.valueOf(s.substring(1)).intValue();
                    }
                    checksum += Integer.valueOf(s1[i]).intValue() + k;
                }
                else
                    checksum += Integer.valueOf(s1[0]).intValue();
            }
            return ((checksum % 10) == 0);
        }
        catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (clickFlag==1)
        {
            mNfcUtils.enableDispatch();
        }

        TicktocApplication.setCurrentActivity(activity);

    }

    @Override
    protected void onRestart() {
        super.onRestart();

        Log.e("call","onRestart onRestart onRestart");
        Log.e("call","onRestart resumeFlag = "+resumeFlag);

        if (resumeFlag==1)
        {
            if (SessionSave.getUserSession(Common.IS_PASSCODE_REQUIRED,activity)!=null && SessionSave.getUserSession(Common.IS_PASSCODE_REQUIRED,activity).equalsIgnoreCase("1"))
            {
                resumeFlag = 0;

                if (Wallet__Activity.activity!=null)
                {
                    Wallet__Activity.activity.finish();
                }

                if (Wallet_Balance_Activity.activity!=null)
                {
                    Wallet_Balance_Activity.activity.finish();
                }

                if (Wallet_Balance_TopUp_Activity.activity!=null)
                {
                    Wallet_Balance_TopUp_Activity.activity.finish();
                }

                if (Wallet_Add_Cards_Activity.activity!=null)
                {
                    Wallet_Add_Cards_Activity.activity.finish();
                }

                Intent intent = new Intent(activity,Create_Passcode_Activity.class);
                intent.putExtra("from","Wallet__Activity");
                startActivity(intent);
                finish();
            }
            else
            {
                startNFC();
            }
        }
        else
        {
            resumeFlag=1;
            startNFC();
        }
    }

    //for nfc card scan....................................................................

    public void scanNFCCard()
    {
        checkNFC();
    }

    private void checkNFC() {

        // Close
        if (mAlertDialog != null && mAlertDialog.isShowing()) {
            mAlertDialog.cancel();
        }

        // Check if NFC is available
        if (!NFCUtils.isNfcAvailable(getApplicationContext())) {

            AlertDialog.Builder alertbox = new AlertDialog.Builder(this);
            alertbox.setTitle(getString(R.string.msg_info));
            alertbox.setMessage(getString(R.string.msg_nfc_not_available));
            alertbox.setPositiveButton(getString(R.string.msg_ok), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(final DialogInterface dialog, final int which) {
                    dialog.dismiss();
                }
            });
            alertbox.setCancelable(false);
            mAlertDialog = alertbox.show();
        }
        else
        {
            cardScan();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mNfcUtils.disableDispatch();
        Log.e("call","onPause onPause onPause onPause");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onNewIntent(final Intent intent)
    {
        super.onNewIntent(intent);
        final Tag mTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
        isScan = true;
        if (mTag != null) {

            new SimpleAsyncTask() {

                /**
                 * Tag comm
                 */
                private IsoDep mTagcomm;

                /**
                 * Emv Card
                 */
                private EmvCard mCard;

                /**
                 * Boolean to indicate exception
                 */
                private boolean mException;

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();

//                    backToHomeScreen();
                    mProvider.getLog().setLength(0);
                    // Show dialog
                    if (mDialog == null) {
                        mDialog = ProgressDialog.show(Add_Card_In_List_Activity.this, getString(R.string.card_reading),
                                getString(R.string.card_reading_desc), true, false);
                    } else {
                        mDialog.show();
                    }
                }

                @Override
                protected void doInBackground() {

                    mTagcomm = IsoDep.get(mTag);
                    if (mTagcomm == null)
                    {
//                        Toast.makeText(getApplicationContext(),R.string.error_communication_nfc,Toast.LENGTH_SHORT).show();
                        showDialogForScanningFailed(getResources().getString(R.string.error_communication_nfc));
                        return;
                    }
                    mException = false;

                    try
                    {
                        mReadCard = null;
                        // Open connection
                        mTagcomm.connect();
                        lastAts = getAts(mTagcomm);

                        mProvider.setmTagCom(mTagcomm);

                        EmvParser parser = new EmvParser(mProvider, true);
                        mCard = parser.readEmvCard();
                        if (mCard != null) {
                            mCard.setAtrDescription(extractAtsDescription(lastAts));
                        }

                    } catch (IOException e) {
                        mException = true;
                    } finally {
                        // close tagcomm
                        IOUtils.closeQuietly(mTagcomm);
                    }
                }

                @Override
                protected void onPostExecute(final Object result) {
                    // close dialog
                    if (mDialog != null) {
                        mDialog.cancel();
                    }

                    if (!mException) {
                        if (mCard != null) {
                            if (StringUtils.isNotBlank(mCard.getCardNumber())) {
//                                Toast.makeText(getApplicationContext(),R.string.card_read,Toast.LENGTH_SHORT).show();
                                mReadCard = mCard;

                                String str = mReadCard.getExpireDate().toString();
                                String[] splited = str.trim().split("\\s+");
                                String month = splited[1];
                                String year = splited[splited.length-1];

                                Log.e("HomeActivity" ,
                                        "Card Number:- " + mReadCard.getCardNumber() +
                                                "\nCard Exp:- " + mReadCard.getExpireDate() +
                                                "\nCard Month:- " + month +
                                                "\nCard Year:- " + year +
                                                "\nCard Type:- " + mReadCard.getType() +
                                                "\nCard First Name:- " + mReadCard.getHolderFirstname() +
                                                "\nCard LastName:- " + mReadCard.getHolderLastname() +
                                                "\nCard Application:- " + mReadCard.getApplicationLabel() +
                                                "\nCard Aid:- " + mReadCard.getAid() +
                                                "\nCard LeftPinTrsy:- " + mReadCard.getLeftPinTry() +
                                                "\nCard Service:- " + mReadCard.getService());

                                String expiryMonth = "";
                                String expiryYear = "";
                                if (mReadCard.getCardNumber()!=null)
                                {
                                    try
                                    {
                                        Calendar cal = Calendar.getInstance();
                                        cal.setTime(new SimpleDateFormat("MMM").parse(month));
                                        int monthInt = cal.get(Calendar.MONTH) + 1;

                                        if (monthInt<10)
                                        {
                                            expiryMonth = "0" + monthInt;
                                        }
                                        else
                                        {
                                            expiryMonth = monthInt + "";
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        expiryMonth = "";
                                        Log.e("call","exception = "+e.getMessage());
                                    }
                                }

                                String cardNumber = "";

                                if (mReadCard.getCardNumber().length()>4)
                                {
                                    cardNumberParam = mReadCard.getCardNumber().replace(" ","");

                                    cardNumber = getFormattedCardNumber(cardNumberParam);

                                }
                                else
                                {
                                    cardNumberParam = "";
                                }

                                et_CardNumber.setText(cardNumber);

                                if (year.length()>2)
                                {
                                    expiryYear = year.substring(year.length() - 2);
                                }

                                if ((expiryMonth!=null && !expiryMonth.equalsIgnoreCase("")) && (expiryYear!=null && !expiryYear.equalsIgnoreCase("")))
                                {
                                    strExpiry = expiryMonth +"/"+expiryYear;
                                }
                                else
                                {
                                    strExpiry = "";
                                }

                                Log.e("call","expiryDate = "+strExpiry);
                                tv_valid_through.setText(month + ", " + year);


                            }
                            else if (mCard.isNfcLocked())
                            {
                                Toast.makeText(getApplicationContext(),R.string.nfc_locked,Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
//                            Toast.makeText(getApplicationContext(),R.string.error_card_unknown,Toast.LENGTH_SHORT).show();
                            showDialogForScanningFailed(getResources().getString(R.string.error_card_unknown));
                        }
                    }
                    else
                    {
                        showDialogForScanningFailed(getResources().getString(R.string.error_communication_nfc));
//                        Toast.makeText(getApplicationContext(),R.string.error_communication_nfc,Toast.LENGTH_SHORT).show();
                    }
                    refreshContent();
                }

            }.execute();
        }

    }

    public void showDialogForScanningFailed(String message)
    {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.my_dialog_class);

        TextView tv_Ok = (TextView) dialog.findViewById(R.id.Ok);
        TextView tv_Message = (TextView) dialog.findViewById(R.id.message);
        LinearLayout ll_Ok = (LinearLayout) dialog.findViewById(R.id.ok_layout);

        tv_Message.setText(message);

        ll_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                startNFC();
            }
        });

        tv_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                startNFC();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        dialog.show();
    }

    public String getFormattedCardNumber(String input)
    {
        String cardNumber = "";
        String strCardFirst = input.substring(0,(input.length()-4));
        String strCardEnd = input.substring(input.length()-4);

        Log.e("call","strCardFirst = "+strCardFirst);
        Log.e("call","strCardEnd = "+strCardEnd);

        String displayCard = "";
        for (int i=0; i<strCardFirst.length(); i++)
        {
            displayCard = displayCard + "*";
        }

        displayCard = displayCard + strCardEnd;



        for (int i=0; i<displayCard.length(); i++)
        {
            if (i>3 && (i%4)==0)
            {
                cardNumber = cardNumber  + " " + displayCard.substring(i,i+1);
            }
            else
            {
                cardNumber = cardNumber  + displayCard.substring(i,i+1);
            }
        }

        return cardNumber;
    }
    /**
     * Method used to get description from ATS
     *
     * @param pAts
     *            ATS byte
     */
    public Collection<String> extractAtsDescription(final byte[] pAts) {
        return AtrUtils.getDescriptionFromAts(BytesUtils.bytesToString(pAts));
    }

    private void refreshContent() {
        if (mRefreshableContent != null && mRefreshableContent.get() != null) {
            mRefreshableContent.get().update();
        }
    }



    /**
     * Get ATS from isoDep
     *
     * @param pIso
     *            isodep
     * @return ATS byte array
     */
    private byte[] getAts(final IsoDep pIso)
    {
        byte[] ret = null;
        if (pIso.isConnected()) {
            // Extract ATS from NFC-A
            ret = pIso.getHistoricalBytes();
            if (ret == null) {
                // Extract ATS from NFC-B
                ret = pIso.getHiLayerResponse();
            }
        }
        return ret;
    }



    @Override
    public StringBuffer getLog() {
        return mProvider.getLog();
    }

    @Override
    public EmvCard getCard() {
        return mReadCard;
    }



    @Override
    public void setRefreshableContent(final IRefreshable pRefreshable) {
        mRefreshableContent = new WeakReference<IRefreshable>(pRefreshable);
    }

    /**
     * Method used to clear data
     */
    public void clear() {
        mReadCard = null;
        mProvider.getLog().setLength(0);
        IRefreshable content = mRefreshableContent.get();
        if (content != null) {
            content.update();
        }
    }

    /**
     * Get the last ATS
     *
     * @return the last card ATS
     */
    public byte[] getLastAts() {
        return lastAts;
    }

    public boolean checkDateValidation(int year,int month)
    {
        Log.e(TAG, "checkDateValidation() year:- " + year);
        Log.e(TAG, "checkDateValidation() month:- " + month);

        int day = getLastDayOfMonth(month+"/"+year);

        Calendar today = Calendar.getInstance();
        today.clear(Calendar.HOUR); today.clear(Calendar.MINUTE); today.clear(Calendar.SECOND);
        Date todayDate = today.getTime();

        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month);
        c.set(Calendar.DAY_OF_MONTH, day);
        //Date dateSpecified = c.getTime();

        Calendar myCalendar = new GregorianCalendar(year, month-1, day);
        Date dateSpecified = myCalendar.getTime();

        if(dateSpecified.before(todayDate))
        {
            return false;
        }
        return true;
    }

    public int getLastDayOfMonth(String dateString)
    {
        Log.e(TAG, "getLastDayOfMonth() dateString:- " + dateString);

        DateFormat dateFormat = new SimpleDateFormat("MM/yy");
        Calendar calendar = Calendar.getInstance();
        try {
            calendar.setTime(dateFormat.parse(dateString));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
    }
}
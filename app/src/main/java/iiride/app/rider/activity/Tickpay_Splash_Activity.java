package iiride.app.rider.activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;


import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import iiride.app.rider.R;
import iiride.app.rider.application.TicktocApplication;
import iiride.app.rider.comman.Common;
import iiride.app.rider.comman.SessionSave;
import iiride.app.rider.comman.WebServiceAPI;
import iiride.app.rider.other.DialogClass;
import iiride.app.rider.other.Global;
import iiride.app.rider.other.InternetDialog;

import org.json.JSONObject;

public class Tickpay_Splash_Activity extends AppCompatActivity {

    //Defining variable
    public static Tickpay_Splash_Activity activity;
    private TicktocApplication application;
    private TextView ll_Close;
    private DialogClass dialogClass;
    private AQuery aQuery;
//    private VideoView videoView;

    public static int resumeFlag = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tickpay_splash);

        activity = this;
        dialogClass = new DialogClass(activity,0);
        aQuery = new AQuery(activity);
        resumeFlag = 1;
        init();
    }

    private void init() {

        application = (TicktocApplication) getApplicationContext();
        ll_Close = (TextView) findViewById(R.id.start);
//        videoView = (VideoView)findViewById(R.id.video);
        ll_Close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                if (videoView!=null)
//                {
//                    videoView.pause();
//                }
                startNextActivity();
            }
        });

//        videoView();
    }

    private void videoView() {

//        videoView.setVisibility(View.VISIBLE);
//        String uriPath = "android.resource://"+getPackageName()+"/raw/"+  R.raw.tick_toc_video;
//        Uri UrlPath=Uri.parse(uriPath);
//        MediaController mediaController = new MediaController(activity);
//        mediaController.setAnchorView(videoView);
//        videoView.setMediaController(null);
//        videoView.setVideoURI(UrlPath);
//        videoView.start();
//        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
//
//            @Override
//            public void onPrepared(MediaPlayer mp) {
//                mp.setLooping(true);
//            }
//        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        TicktocApplication.setCurrentActivity(activity);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (resumeFlag==1)
        {
            if (SessionSave.getUserSession(Common.IS_PASSCODE_REQUIRED,activity)!=null && SessionSave.getUserSession(Common.IS_PASSCODE_REQUIRED,activity).equalsIgnoreCase("1"))
            {
                resumeFlag = 0;
                Intent intent = new Intent(Tickpay_Splash_Activity.this,Create_Passcode_Activity.class);
                intent.putExtra("from","TickPay");
                startActivity(intent);
                finish();
            }
        }
        else
        {
            resumeFlag=1;
        }
    }

    private void startNextActivity()
    {
        resumeFlag = 0;
        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TICK_PAY_SPLASH,"1",activity);

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_VERIFY_USER,activity)!=null && SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_VERIFY_USER,activity).equalsIgnoreCase("2"))
        {
            Intent intent = new Intent(activity,TickPayActivity.class);
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        }
        else if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_VERIFY_USER,activity)!=null && SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_VERIFY_USER,activity).equalsIgnoreCase("1"))
        {
            if (Global.isNetworkconn(activity))
            {
                call_GetTickpayApprovalStatus();
            }
            else
            {
                InternetDialog internetDialog = new InternetDialog(activity);
                internetDialog.showDialog("Please check your internet connection!");
            }
        }
        else
        {
            Intent loginScreen = new Intent(Tickpay_Splash_Activity.this,TickPayRegisterActivity.class);
            startActivity(loginScreen);
            finish();
        }
    }

    private void call_GetTickpayApprovalStatus()
    {
        String url = WebServiceAPI.API_GET_APPROVAL_STATUS + SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID,activity);
        Log.e("call", "url = " + url);
        dialogClass.showDialog();
        aQuery.ajax(url.trim(), null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e("responseCode", " = " + responseCode);
                    Log.e("Response", " = " + json);

                    resumeFlag = 0;

                    if (json!=null)
                    {
                        if (json.has("Verify"))
                        {
                            if (json.getString("Verify")!=null)
                            {
                                String veriry = json.getString("Verify");

                                if (veriry.equalsIgnoreCase("1"))
                                {
                                    dialogClass.hideDialog();
                                    Intent intent = new Intent(activity,TickPayReview.class);
                                    startActivity(intent);
                                    finish();
                                    overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                                }
                                else if (veriry.equalsIgnoreCase("2"))
                                {
                                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_VERIFY_USER,"2",activity);
                                    Intent intent = new Intent(activity,TickPayActivity.class);
                                    startActivity(intent);
                                    finish();
                                    overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                                }
                                else
                                {
                                    dialogClass.hideDialog();
                                    Intent intent = new Intent(activity,TickPayReview.class);
                                    startActivity(intent);
                                    finish();
                                    overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                                }
                            }
                            else
                            {
                                dialogClass.hideDialog();
                                Intent intent = new Intent(activity,TickPayReview.class);
                                startActivity(intent);
                                finish();
                                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                            }
                        }
                        else
                        {
                            dialogClass.hideDialog();
                            Intent intent = new Intent(activity,TickPayReview.class);
                            startActivity(intent);
                            finish();
                            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                        }
                    }
                    else
                    {
                        dialogClass.hideDialog();
                        Intent intent = new Intent(activity,TickPayReview.class);
                        startActivity(intent);
                        finish();
                        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                    }
                }
                catch (Exception e)
                {
                    Log.e("Exception","Exception "+e.toString());
                    dialogClass.hideDialog();
                    Intent intent = new Intent(activity,TickPayReview.class);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                }
            }

        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }
}


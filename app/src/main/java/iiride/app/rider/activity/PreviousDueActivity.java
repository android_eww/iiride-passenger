package iiride.app.rider.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import iiride.app.rider.R;
import iiride.app.rider.adapter.PreviousDueAdapter;
import iiride.app.rider.adapter.UpComming_Adapter;
import iiride.app.rider.application.TicktocApplication;
import iiride.app.rider.been.PreviousDue_Been;
import iiride.app.rider.comman.Common;
import iiride.app.rider.comman.SessionSave;
import iiride.app.rider.comman.WebServiceAPI;
import iiride.app.rider.other.DialogClass;
import iiride.app.rider.other.Global;
import iiride.app.rider.other.InternetDialog;

public class PreviousDueActivity extends AppCompatActivity implements View.OnClickListener {

    public static PreviousDueActivity activity;
    private LinearLayout ll_Back;
    private ImageView iv_Back;
    private TextView tv_Title;
    private DialogClass dialogClass;
    private AQuery aQuery;
    public static List<PreviousDue_Been> previousDue_beens = new ArrayList<PreviousDue_Been>();
    private TextView tv_NoDataFound;
    private RecyclerView rv_previous_due;
    private PreviousDueAdapter previousDueAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_previous_due);

        activity = PreviousDueActivity.this;
        dialogClass = new DialogClass(activity, 0);
        aQuery = new AQuery(activity);

        init();

    }


    private void init() {

        ll_Back = (LinearLayout) findViewById(R.id.back_layout);
        iv_Back = (ImageView) findViewById(R.id.back_imageview);
        tv_Title = (TextView) findViewById(R.id.title_textview);
        tv_NoDataFound = (TextView) findViewById(R.id.tv_NoDataFound);
        rv_previous_due = (RecyclerView) findViewById(R.id.rv_previous_due);

        tv_Title.setText(getResources().getString(R.string.title_previous_due));

        ll_Back.setOnClickListener(activity);
        iv_Back.setOnClickListener(activity);

        if (Global.isNetworkconn(activity)) {
            call_PreviousDueApi();
        } else {
            InternetDialog internetDialog = new InternetDialog(activity);
            internetDialog.showDialog("Please check your internet connection!");
        }

        rv_previous_due.setLayoutManager(new LinearLayoutManager(activity));
        previousDueAdapter = new PreviousDueAdapter(activity, previousDue_beens);
        rv_previous_due.setAdapter(previousDueAdapter);

    }

    private void call_PreviousDueApi() {
        dialogClass.showDialog();
        String url = WebServiceAPI.API_PREVIOUS_DUE + SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID, activity);

        Log.e("call", "API_PREVIOUS_DUE = " + url);


        aQuery.ajax(url.trim(), null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {
                previousDue_beens.clear();
                try {
                    int responseCode = status.getCode();
                    Log.e("responseCode", " = " + responseCode);
                    Log.e("Response", " = " + json);

                    if (json != null) {
                        if (json.has("status")) {
                            if (json.getBoolean("status")) {

                                if (json.has("data")) {

                                    Object json_obj = new JSONTokener(json.getString("data")).nextValue();
                                    if (json_obj instanceof JSONObject) {
                                        JSONObject data = json.getJSONObject("data");

                                        if (data != null) {
                                            {
                                                String Id = "", BookingBy = "", PassengerId = "", CityId = "", SPId = "", ModelId = "", DriverId = "", CreatedDate = "", TransactionId = "", PaymentStatus = "";
                                                String PickupTime = "", DropTime = "", TripDuration = "", TripDistance = "", PickupLocation = "", DropoffLocation = "";
                                                String NightFareApplicable = "", NightFare = "", TripFare = "", DistanceFare = "", WaitingTime = "", WaitingTimeCost = "", TollFee = "", BookingCharge = "";
                                                String Tax = "", PromoCode = "", Discount = "", SubTotal = "", GrandTotal = "", Status = "", Trash = "", Reason = "", PaymentType = "", CardId = "", ByDriverAmount = "", AdminAmount = "";
                                                String CompanyAmount = "", SPAmount = "", PickupLat = "", PickupLng = "", DropOffLat = "", DropOffLon = "", PassengerName = "", PassengerContact = "", PassengerEmail = "", Notes = "", FlightNumber = "", PaidToDriver = "", PaidSP = "", BabySeat = "", ModifyDate = "", PickupSuburb = "", DropoffSuburb = "";
                                                String BookingType = "", ByDriverId = "", PastDuesId="";

                                                if (data.has("Id")) {
                                                    Id = data.getString("Id");
                                                }

                                                if (data.has("BookingBy")) {
                                                    BookingBy = data.getString("BookingBy");
                                                }

                                                if (data.has("PassengerId")) {
                                                    PassengerId = data.getString("PassengerId");
                                                }

                                                if (data.has("CityId")) {
                                                    CityId = data.getString("CityId");
                                                }

                                                if (data.has("SPId")) {
                                                    SPId = data.getString("SPId");
                                                }
                                                if (data.has("ModelId")) {
                                                    ModelId = data.getString("ModelId");
                                                }

                                                if (data.has("DriverId")) {
                                                    DriverId = data.getString("DriverId");
                                                }

                                                if (data.has("CreatedDate")) {
                                                    CreatedDate = data.getString("CreatedDate");
                                                }

                                                if (data.has("TransactionId")) {
                                                    TransactionId = data.getString("TransactionId");
                                                }

                                                if (data.has("PaymentStatus")) {
                                                    PaymentStatus = data.getString("PaymentStatus");
                                                }

                                                if (data.has("PickupTime")) {
                                                    PickupTime = data.getString("PickupTime");
                                                }

                                                if (data.has("DropTime")) {
                                                    DropTime = data.getString("DropTime");
                                                }

                                                if (data.has("TripDuration")) {
                                                    TripDuration = data.getString("TripDuration");
                                                }

                                                if (data.has("TripDistance")) {
                                                    TripDistance = data.getString("TripDistance");
                                                }

                                                if (data.has("PickupLocation")) {
                                                    PickupLocation = data.getString("PickupLocation");
                                                }

                                                if (data.has("DropoffLocation")) {
                                                    DropoffLocation = data.getString("DropoffLocation");
                                                }

                                                if (data.has("NightFareApplicable")) {
                                                    NightFareApplicable = data.getString("NightFareApplicable");
                                                }

                                                if (data.has("NightFare")) {
                                                    NightFare = data.getString("NightFare");
                                                }

                                                if (data.has("TripFare")) {
                                                    TripFare = data.getString("TripFare");
                                                }

                                                if (data.has("DistanceFare")) {
                                                    DistanceFare = data.getString("DistanceFare");
                                                }

                                                if (data.has("WaitingTime")) {
                                                    WaitingTime = data.getString("WaitingTime");
                                                }

                                                if (data.has("WaitingTimeCost")) {
                                                    WaitingTimeCost = data.getString("WaitingTimeCost");
                                                }

                                                if (data.has("TollFee")) {
                                                    TollFee = data.getString("TollFee");
                                                }

                                                if (data.has("BookingCharge")) {
                                                    BookingCharge = data.getString("BookingCharge");
                                                }

                                                if (data.has("Tax")) {
                                                    Tax = data.getString("Tax");
                                                }

                                                if (data.has("PromoCode")) {
                                                    PromoCode = data.getString("PromoCode");
                                                }

                                                if (data.has("Discount")) {
                                                    Discount = data.getString("Discount");
                                                }

                                                if (data.has("SubTotal")) {
                                                    SubTotal = data.getString("SubTotal");
                                                }

                                                if (data.has("GrandTotal")) {
                                                    GrandTotal = data.getString("GrandTotal");
                                                }

                                                if (data.has("Status")) {
                                                    Status = data.getString("Status");
                                                }

                                                if (data.has("Trash")) {
                                                    Trash = data.getString("Trash");
                                                }

                                                if (data.has("Reason")) {
                                                    Reason = data.getString("Reason");
                                                }

                                                if (data.has("PaymentType")) {
                                                    PaymentType = data.getString("PaymentType");
                                                }

                                                if (data.has("CardId")) {
                                                    CardId = data.getString("CardId");
                                                }

                                                if (data.has("ByDriverAmount")) {
                                                    ByDriverAmount = data.getString("ByDriverAmount");
                                                }

                                                if (data.has("AdminAmount")) {
                                                    AdminAmount = data.getString("AdminAmount");
                                                }

                                                if (data.has("CompanyAmount")) {
                                                    CompanyAmount = data.getString("CompanyAmount");
                                                }

                                                if (data.has("PickupLat")) {
                                                    PickupLat = data.getString("PickupLat");
                                                }

                                                if (data.has("PickupLng")) {
                                                    PickupLng = data.getString("PickupLng");
                                                }

                                                if (data.has("DropOffLat")) {
                                                    DropOffLat = data.getString("DropOffLat");
                                                }

                                                if (data.has("DropOffLon")) {
                                                    DropOffLon = data.getString("DropOffLon");
                                                }
                                                if (data.has("SPAmount")) {
                                                    SPAmount = data.getString("SPAmount");
                                                }


                                                if (data.has("PassengerName")) {
                                                    PassengerName = data.getString("PassengerName");
                                                }

                                                if (data.has("PassengerContact")) {
                                                    PassengerContact = data.getString("PassengerContact");
                                                }

                                                if (data.has("PassengerEmail")) {
                                                    PassengerEmail = data.getString("PassengerEmail");
                                                }

                                                if (data.has("Notes")) {
                                                    Notes = data.getString("Notes");
                                                }

                                                if (data.has("FlightNumber")) {
                                                    FlightNumber = data.getString("FlightNumber");
                                                }
                                                if (data.has("PaidToDriver")) {
                                                    PaidToDriver = data.getString("PaidToDriver");
                                                }
                                                if (data.has("PaidSP")) {
                                                    PaidSP = data.getString("PaidSP");
                                                }
                                                if (data.has("BabySeat")) {
                                                    BabySeat = data.getString("BabySeat");
                                                }
                                                if (data.has("ModifyDate")) {
                                                    ModifyDate = data.getString("ModifyDate");
                                                }
                                                if (data.has("PickupSuburb")) {
                                                    PickupSuburb = data.getString("PickupSuburb");
                                                }
                                                if (data.has("DropoffSuburb")) {
                                                    DropoffSuburb = data.getString("DropoffSuburb");
                                                }


                                                if (data.has("BookingType")) {
                                                    BookingType = data.getString("BookingType");
                                                }

                                                if (data.has("ByDriverId")) {
                                                    ByDriverId = data.getString("ByDriverId");
                                                }

                                                if(data.has("PastDuesId")){
                                                    PastDuesId = data.getString("PastDuesId");
                                                }

                                                if (Status != null && !Status.equalsIgnoreCase("") && Status.equalsIgnoreCase("completed")) {
                                                    previousDue_beens.add(new PreviousDue_Been(
                                                            Id,
                                                            BookingBy,
                                                            PassengerId,
                                                            CityId,
                                                            SPId,
                                                            ModelId,
                                                            DriverId,
                                                            CreatedDate,
                                                            TransactionId,
                                                            PaymentStatus,
                                                            PickupTime,
                                                            DropTime,
                                                            TripDuration,
                                                            TripDistance,
                                                            PickupLocation,
                                                            DropoffLocation,
                                                            NightFareApplicable,
                                                            NightFare,
                                                            TripFare,
                                                            DistanceFare,
                                                            WaitingTime,
                                                            WaitingTimeCost,
                                                            TollFee,
                                                            BookingCharge,
                                                            Tax,
                                                            PromoCode,
                                                            Discount,
                                                            SubTotal,
                                                            GrandTotal,
                                                            Status,
                                                            Trash,
                                                            Reason,
                                                            PaymentType,
                                                            CardId,
                                                            ByDriverAmount,
                                                            AdminAmount,
                                                            CompanyAmount,
                                                            SPAmount,
                                                            PickupLat,
                                                            PickupLng,
                                                            DropOffLat,
                                                            DropOffLon,
                                                            BookingType,
                                                            ByDriverId,
                                                            PassengerName,
                                                            PassengerContact,
                                                            PassengerEmail,
                                                            Notes,
                                                            FlightNumber,
                                                            PaidToDriver,
                                                            PaidSP,
                                                            BabySeat,
                                                            ModifyDate,
                                                            PickupSuburb,
                                                            DropoffSuburb,
                                                            PastDuesId
                                                    ));
                                                    previousDueAdapter.notifyDataSetChanged();
                                                } else {
                                                }
                                            }
                                        }


                                    } else {
                                        Log.e("call", "history null or lenth 0");
                                        tv_NoDataFound.setVisibility(View.VISIBLE);
                                        tv_NoDataFound.setText(json.getString("message"));
                                    }
                                    dialogClass.hideDialog();
                                } else {
                                    dialogClass.hideDialog();
                                    tv_NoDataFound.setVisibility(View.VISIBLE);
                                    tv_NoDataFound.setText(json.getString("message"));
                                }

                              /*  tv_NoDataFound.setVisibility(View.GONE);


                                rv_previous_due.setLayoutManager(new LinearLayoutManager(activity));
                                previousDueAdapter = new PreviousDueAdapter(activity, previousDue_beens);
                                rv_previous_due.setAdapter(previousDueAdapter);*/
                            } else {
                                Log.e("call", "status false");
                                dialogClass.hideDialog();
                                tv_NoDataFound.setVisibility(View.VISIBLE);
                                tv_NoDataFound.setText(json.getString("message"));
                            }
                            dialogClass.hideDialog();
                        } else {
                            Log.e("call", "status not found");
                            dialogClass.hideDialog();
                        }
                    } else {
                        Log.e("call", "json null");
                        dialogClass.hideDialog();
                    }
                } catch (Exception e) {
                    Log.e("Exception", "Exception " + e.toString());
                    dialogClass.hideDialog();
                }
            }

        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back_layout:
                onBackPressed();
                break;

            case R.id.back_imageview:
                onBackPressed();
                break;
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
    }


    @Override
    protected void onResume() {
        super.onResume();

        TicktocApplication.setCurrentActivity(activity);

    }

    /*private void addTempData() {
        for (int i = 0; i < 5; i++) {
            previousDue_beens.add(new PreviousDue_Been("1", "Ahmedabad", "Surat", "55"));
        }
    }*/

    public void call_PayNowApi(String pastDueId, String cardId) {
        dialogClass.showDialog();
        String url = WebServiceAPI.API_PAST_DUES_PAYOUT;


        Map<String, Object> params = new HashMap<String, Object>();
        params.put(WebServiceAPI.PARAM_PAST_DUE_ID, pastDueId);
        params.put(WebServiceAPI.PARAM_CARD_ID, cardId);


        Log.e("call", "call_PayNowApi API_PREVIOUS_DUE = " + url);

        Log.e("call", "call_PayNowApi PARAMS = " + params);


        aQuery.ajax(url.trim(), params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {
                previousDue_beens.clear();
                try {
                    int responseCode = status.getCode();
                    Log.e("responseCode", "call_PayNowApi = " + responseCode);
                    Log.e("Response", "call_PayNowApi = " + json);

                    if (json != null) {
                        if (json.has("status")) {
                            if (json.getBoolean("status")) {
                                Toast.makeText(activity, json.getString("message").toString(), Toast.LENGTH_LONG).show();
                                onBackPressed();
                            } else {
                                dialogClass.hideDialog();
                                InternetDialog internetDialog = new InternetDialog(activity);
                                internetDialog.showPastDueDialog(json.getString("message"));
                            }
                        } else {
                            dialogClass.hideDialog();
                            InternetDialog internetDialog = new InternetDialog(activity);
                            internetDialog.showDialog("Something went wrong.");
                        }
                    } else {
                        dialogClass.hideDialog();
                        InternetDialog internetDialog = new InternetDialog(activity);
                        internetDialog.showDialog("Something went wrong.");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    dialogClass.hideDialog();
                    InternetDialog internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog("Something went wrong.");
                }
            }
        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }
}

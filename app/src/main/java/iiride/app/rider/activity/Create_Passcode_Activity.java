package iiride.app.rider.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import iiride.app.rider.R;
import iiride.app.rider.comman.Common;
import iiride.app.rider.comman.SessionSave;
import iiride.app.rider.comman.WebServiceAPI;
import iiride.app.rider.other.DialogClass;
import iiride.app.rider.other.Global;
import iiride.app.rider.other.InternetDialog;

import org.json.JSONObject;


public class Create_Passcode_Activity extends AppCompatActivity
{
    public static Create_Passcode_Activity activity;

    TextView tv_zero, tv_one, tv_two, tv_three, tv_four, tv_five, tv_six, tv_seven, tv_eight, tv_nine, tv_headerPassword;
    TextView tv_1, tv_2, tv_3, tv_4,tv_forgot;
    LinearLayout shakeLayout, main_layout, ll_Erase;

    Handler handler;
    int showDialogPass=0;
    boolean activateET1=false, activateET2=false, activateET3=false, activateET4=false;

    String sessionPasscode="", firstPasscode="", secondPasscode="";
    int firstTimeCreate=0;

    DialogClass dialogClass;
    AQuery aQuery;

    String from = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_passcode);

        activity = Create_Passcode_Activity.this;
        sessionPasscode="";
        firstPasscode="";
        secondPasscode="";
        MainActivity.PasscodeBackPress=0;

        aQuery = new AQuery(activity);
        dialogClass = new DialogClass(activity, 1);

        if (this.getIntent()!=null)
        {
            if (this.getIntent().getStringExtra("from")!=null)
            {
                from = this.getIntent().getStringExtra("from");
            }
            else
            {
                from = "";
            }
        }
        else
        {
            from = "";
        }

        init();
    }

    private void init() {

        main_layout = (LinearLayout) findViewById(R.id.main_layout);
        shakeLayout = (LinearLayout) findViewById(R.id.shakeLayout);
        tv_forgot = (TextView) findViewById(R.id.tv_forgot);
        ll_Erase = (LinearLayout) findViewById(R.id.ll_Erase);

        tv_headerPassword = (TextView) findViewById(R.id.tv_headerPassword);

        tv_zero = (TextView) findViewById(R.id.tv_zero);
        tv_one = (TextView) findViewById(R.id.tv_one);
        tv_two = (TextView) findViewById(R.id.tv_two);
        tv_three = (TextView) findViewById(R.id.tv_three);

        tv_four = (TextView) findViewById(R.id.tv_four);
        tv_five = (TextView) findViewById(R.id.tv_five);
        tv_six = (TextView) findViewById(R.id.tv_six);
        tv_seven = (TextView) findViewById(R.id.tv_seven);

        tv_eight = (TextView) findViewById(R.id.tv_eight);
        tv_nine = (TextView) findViewById(R.id.tv_nine);

        tv_1 = (TextView) findViewById(R.id.tv_1);
        tv_2 = (TextView) findViewById(R.id.tv_2);
        tv_3 = (TextView) findViewById(R.id.tv_3);
        tv_4 = (TextView) findViewById(R.id.tv_4);

        activateET1=false;

        sessionPasscode = SessionSave.getUserSession(Common.CREATED_PASSCODE, activity);

        Log.e("call","from from ="+from);
        Log.e("call","SettingActivity.flageChangePasscode ="+SettingActivity.flageChangePasscode);

        if (from!=null && from.equalsIgnoreCase("Setting") && SettingActivity.flageChangePasscode == 1)
        {
            Log.e("call","iffffffffffffff");
            firstTimeCreate=3;
            tv_headerPassword.setText(getResources().getString(R.string.passcode_old));
            tv_forgot.setVisibility(View.VISIBLE);
        }
        else
        {
            Log.e("call","elseeeeeeeeee");
            if (sessionPasscode!=null && !sessionPasscode.equalsIgnoreCase(""))
            {
                Log.e("call","iffffffffffffff22222222222");
                tv_headerPassword.setText(getResources().getString(R.string.passcode_required));
                firstTimeCreate=2;
                tv_forgot.setVisibility(View.VISIBLE);
            }
            else
            {
                Log.e("call","elseeeeeeeeee22222222");
                firstTimeCreate=0;
                tv_headerPassword.setText(getResources().getString(R.string.passcode_create));
                tv_forgot.setVisibility(View.GONE);
            }
        }
        Log.e("call","firstTimeCreate ="+firstTimeCreate);

        tv_forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setCancelable(false);
                builder.setMessage("If you can't remember your passcode, you must sign out of TiCKTOC and login with your email address and password.");

                String positiveText = getString(android.R.string.ok);
                builder.setPositiveButton(positiveText,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String driverId = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID, activity);
                                if (driverId != null && !driverId.equalsIgnoreCase("")) {
                                    DriverLogOut(driverId, dialog);
                                }
                            }
                        });

                builder.setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

                final AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        ll_Erase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!activateET1)
                {
                   Log.e("jjjjjjjj","11111111111111111");
                }
                else if(!activateET2)
                {
                    Log.e("jjjjjjjj","2222222222222222222");
                    tv_1.setText("");
                    activateET1=false;
                }
                else if(!activateET3)
                {
                    Log.e("jjjjjjjj","3333333333333333");
                    tv_2.setText("");
                    activateET2=false;
                }
                else if(!activateET4)
                {
                    Log.e("jjjjjjjj","444444444444444444444");
                    tv_3.setText("");
                    activateET3=false;
                }
            }
        });
        tv_zero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!activateET1)
                {
                    setZero(tv_1);
                    activateET1=true;
                }
                else if(!activateET2)
                {
                    setZero(tv_2);
                    activateET2=true;
                }
                else if(!activateET3)
                {
                    setZero(tv_3);
                    activateET3=true;
                }
                else if(!activateET4)
                {
                    setZero(tv_4);
                    activateET4=true;

                    activateET1=false;
                    activateET2=false;
                    activateET3=false;
                    activateET4=false;
                }
            }
        });

        tv_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!activateET1)
                {
                    setOne(tv_1);
                    activateET1=true;
                }
                else if(!activateET2)
                {
                    setOne(tv_2);
                    activateET2=true;
                }
                else if(!activateET3)
                {
                    setOne(tv_3);
                    activateET3=true;
                }
                else if(!activateET4)
                {
                    setOne(tv_4);
                    activateET4=true;

                    activateET1=false;
                    activateET2=false;
                    activateET3=false;
                    activateET4=false;
                }
            }
        });

        tv_two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!activateET1)
                {
                    setTwo(tv_1);
                    activateET1=true;
                }
                else if(!activateET2)
                {
                    setTwo(tv_2);
                    activateET2=true;
                }
                else if(!activateET3)
                {
                    setTwo(tv_3);
                    activateET3=true;
                }
                else if(!activateET4)
                {
                    setTwo(tv_4);
                    activateET4=true;

                    activateET1=false;
                    activateET2=false;
                    activateET3=false;
                    activateET4=false;
                }

            }
        });

        tv_three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!activateET1)
                {
                    setThree(tv_1);
                    activateET1=true;
                }
                else if(!activateET2)
                {
                    setThree(tv_2);
                    activateET2=true;
                }
                else if(!activateET3)
                {
                    setThree(tv_3);
                    activateET3=true;
                }
                else if(!activateET4)
                {
                    setThree(tv_4);
                    activateET4=true;

                    activateET1=false;
                    activateET2=false;
                    activateET3=false;
                    activateET4=false;
                }
            }
        });

        tv_four.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!activateET1)
                {
                    setFour(tv_1);
                    activateET1=true;
                }
                else if(!activateET2)
                {
                    setFour(tv_2);
                    activateET2=true;
                }
                else if(!activateET3)
                {
                    setFour(tv_3);
                    activateET3=true;
                }
                else if(!activateET4)
                {
                    setFour(tv_4);
                    activateET4=true;

                    activateET1=false;
                    activateET2=false;
                    activateET3=false;
                    activateET4=false;
                }

            }
        });

        tv_five.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!activateET1)
                {
                    setFive(tv_1);
                    activateET1=true;
                }
                else if(!activateET2)
                {
                    setFive(tv_2);
                    activateET2=true;
                }
                else if(!activateET3)
                {
                    setFive(tv_3);
                    activateET3=true;
                }
                else if(!activateET4)
                {
                    setFive(tv_4);
                    activateET4=true;

                    activateET1=false;
                    activateET2=false;
                    activateET3=false;
                    activateET4=false;
                }

            }
        });

        tv_six.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!activateET1)
                {
                    setSix(tv_1);
                    activateET1=true;
                }
                else if(!activateET2)
                {
                    setSix(tv_2);
                    activateET2=true;
                }
                else if(!activateET3)
                {
                    setSix(tv_3);
                    activateET3=true;
                }
                else if(!activateET4)
                {
                    setSix(tv_4);
                    activateET4=true;

                    activateET1=false;
                    activateET2=false;
                    activateET3=false;
                    activateET4=false;
                }
            }
        });

        tv_seven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!activateET1)
                {
                    setSeven(tv_1);
                    activateET1=true;
                }
                else if(!activateET2)
                {
                    setSeven(tv_2);
                    activateET2=true;
                }
                else if(!activateET3)
                {
                    setSeven(tv_3);
                    activateET3=true;
                }
                else if(!activateET4)
                {
                    setSeven(tv_4);
                    activateET4=true;

                    activateET1=false;
                    activateET2=false;
                    activateET3=false;
                    activateET4=false;
                }
            }
        });

        tv_eight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!activateET1)
                {
                    setEight(tv_1);
                    activateET1=true;
                }
                else if(!activateET2)
                {
                    setEight(tv_2);
                    activateET2=true;
                }
                else if(!activateET3)
                {
                    setEight(tv_3);
                    activateET3=true;
                }
                else if(!activateET4)
                {
                    setEight(tv_4);
                    activateET4=true;

                    activateET1=false;
                    activateET2=false;
                    activateET3=false;
                    activateET4=false;
                }
            }
        });

        tv_nine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!activateET1)
                {
                    setNine(tv_1);
                    activateET1=true;
                }
                else if(!activateET2)
                {
                    setNine(tv_2);
                    activateET2=true;
                }
                else if(!activateET3)
                {
                    setNine(tv_3);
                    activateET3=true;
                }
                else if(!activateET4)
                {
                    setNine(tv_4);
                    activateET4=true;

                    activateET1=false;
                    activateET2=false;
                    activateET3=false;
                    activateET4=false;
                }
            }
        });

        tv_4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(tv_4.getText().length() == 1)
                {
                    tv_zero.setEnabled(false);
                    tv_one.setEnabled(false);
                    tv_two.setEnabled(false);
                    tv_three.setEnabled(false);
                    tv_four.setEnabled(false);
                    tv_five.setEnabled(false);
                    tv_six.setEnabled(false);
                    tv_seven.setEnabled(false);
                    tv_eight.setEnabled(false);
                    tv_nine.setEnabled(false);

                    handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
//                            String passwordFromUser = tv_1.getText().toString() + tv_2.getText().toString() + tv_3.getText().toString() + tv_4.getText().toString();
                            if (firstTimeCreate==0)
                            {
                                Log.e("vvvvvvvvvvvvvv","first Time Create");
                                firstTimeCreate=1;
                                firstPasscode = tv_1.getText().toString() + tv_2.getText().toString() + tv_3.getText().toString() + tv_4.getText().toString();
                                tv_headerPassword.setText(getResources().getString(R.string.passcode_retype));
                                tv_1.setText("");
                                tv_2.setText("");
                                tv_3.setText("");
                                tv_4.setText("");

                                tv_zero.setEnabled(true);
                                tv_one.setEnabled(true);
                                tv_two.setEnabled(true);
                                tv_three.setEnabled(true);
                                tv_four.setEnabled(true);
                                tv_five.setEnabled(true);
                                tv_six.setEnabled(true);
                                tv_seven.setEnabled(true);
                                tv_eight.setEnabled(true);
                                tv_nine.setEnabled(true);
                            }
                            else if (firstTimeCreate==1)
                            {
                                Log.e("vvvvvvvvvvvvvv","second Time Create");
                                secondPasscode = tv_1.getText().toString() + tv_2.getText().toString() + tv_3.getText().toString() + tv_4.getText().toString();
                                if (firstPasscode!=null && !firstPasscode.equalsIgnoreCase("") && firstPasscode.equalsIgnoreCase(secondPasscode))
                                {
                                    SessionSave.saveUserSession(Common.CREATED_PASSCODE, firstPasscode, activity);
                                    rightPassword();
                                }
                                else
                                {
                                    Log.e("vvvvvvvvvvvvvv","passcode is nottttttttt matched");
                                    Animation animation = AnimationUtils.loadAnimation(activity, R.anim.shake);
                                    shakeLayout.startAnimation(animation);
                                    animation.setAnimationListener(new Animation.AnimationListener() {
                                        @Override
                                        public void onAnimationStart(Animation animation) {
                                            tv_1.setText("");
                                            tv_2.setText("");
                                            tv_3.setText("");
                                            tv_4.setText("");
                                        }

                                        @Override
                                        public void onAnimationEnd(Animation animation) {
                                            tv_zero.setEnabled(true);
                                            tv_one.setEnabled(true);
                                            tv_two.setEnabled(true);
                                            tv_three.setEnabled(true);
                                            tv_four.setEnabled(true);
                                            tv_five.setEnabled(true);
                                            tv_six.setEnabled(true);
                                            tv_seven.setEnabled(true);
                                            tv_eight.setEnabled(true);
                                            tv_nine.setEnabled(true);
                                        }

                                        @Override
                                        public void onAnimationRepeat(Animation animation) {

                                        }
                                    });
                                }
                            }
                            else if (firstTimeCreate==2)
                            {
                                String passwordFromUser = SessionSave.getUserSession(Common.CREATED_PASSCODE, activity);
                                secondPasscode = tv_1.getText().toString() + tv_2.getText().toString() + tv_3.getText().toString() + tv_4.getText().toString();
                                if (passwordFromUser!=null && !passwordFromUser.equalsIgnoreCase("") && passwordFromUser.equalsIgnoreCase(secondPasscode))
                                {
                                    rightPassword();
                                }
                                else
                                {
                                    Log.e("vvvvvvvvvvvvvv","passcode is nottttttttt matched");
                                    Animation animation = AnimationUtils.loadAnimation(activity, R.anim.shake);
                                    shakeLayout.startAnimation(animation);
                                    animation.setAnimationListener(new Animation.AnimationListener() {
                                        @Override
                                        public void onAnimationStart(Animation animation) {
                                            tv_1.setText("");
                                            tv_2.setText("");
                                            tv_3.setText("");
                                            tv_4.setText("");
                                        }

                                        @Override
                                        public void onAnimationEnd(Animation animation) {
                                            tv_zero.setEnabled(true);
                                            tv_one.setEnabled(true);
                                            tv_two.setEnabled(true);
                                            tv_three.setEnabled(true);
                                            tv_four.setEnabled(true);
                                            tv_five.setEnabled(true);
                                            tv_six.setEnabled(true);
                                            tv_seven.setEnabled(true);
                                            tv_eight.setEnabled(true);
                                            tv_nine.setEnabled(true);
                                        }

                                        @Override
                                        public void onAnimationRepeat(Animation animation) {

                                        }
                                    });
                                }
                            }
                            else if (firstTimeCreate==3)
                            {
                                String passwordFromUser = SessionSave.getUserSession(Common.CREATED_PASSCODE, activity);
                                secondPasscode = tv_1.getText().toString() + tv_2.getText().toString() + tv_3.getText().toString() + tv_4.getText().toString();
                                if (passwordFromUser!=null && !passwordFromUser.equalsIgnoreCase("") && passwordFromUser.equalsIgnoreCase(secondPasscode))
                                {
                                    firstTimeCreate=0;
                                    tv_headerPassword.setText(getResources().getString(R.string.passcode_create));
                                    tv_forgot.setVisibility(View.GONE);

                                    tv_1.setText("");
                                    tv_2.setText("");
                                    tv_3.setText("");
                                    tv_4.setText("");

                                    tv_zero.setEnabled(true);
                                    tv_one.setEnabled(true);
                                    tv_two.setEnabled(true);
                                    tv_three.setEnabled(true);
                                    tv_four.setEnabled(true);
                                    tv_five.setEnabled(true);
                                    tv_six.setEnabled(true);
                                    tv_seven.setEnabled(true);
                                    tv_eight.setEnabled(true);
                                    tv_nine.setEnabled(true);
                                }
                                else
                                {
                                    Log.e("vvvvvvvvvvvvvv","passcode is nottttttttt matched");
                                    Animation animation = AnimationUtils.loadAnimation(activity, R.anim.shake);
                                    shakeLayout.startAnimation(animation);
                                    animation.setAnimationListener(new Animation.AnimationListener() {
                                        @Override
                                        public void onAnimationStart(Animation animation) {
                                            tv_1.setText("");
                                            tv_2.setText("");
                                            tv_3.setText("");
                                            tv_4.setText("");
                                        }

                                        @Override
                                        public void onAnimationEnd(Animation animation) {
                                            tv_zero.setEnabled(true);
                                            tv_one.setEnabled(true);
                                            tv_two.setEnabled(true);
                                            tv_three.setEnabled(true);
                                            tv_four.setEnabled(true);
                                            tv_five.setEnabled(true);
                                            tv_six.setEnabled(true);
                                            tv_seven.setEnabled(true);
                                            tv_eight.setEnabled(true);
                                            tv_nine.setEnabled(true);
                                        }

                                        @Override
                                        public void onAnimationRepeat(Animation animation) {

                                        }
                                    });
                                }
                            }
                        }
                    }, 500);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void rightPassword()
    {
        Log.e("vvvvvvvvvvvvvv","passcode is matched = "+from);
        if (from!=null && !from.equalsIgnoreCase("") && from.equalsIgnoreCase("Setting"))
        {
            SettingActivity.falge = 1;
            SessionSave.saveUserSession(Common.IS_PASSCODE_REQUIRED,"1",activity);
            finish();
        }
        else if (from!=null && !from.equalsIgnoreCase("") && from.equalsIgnoreCase("Wallet__Activity"))
        {
            Intent intent = new Intent(activity,Wallet__Activity.class);
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        }
        else if (from!=null && !from.equalsIgnoreCase("") && from.equalsIgnoreCase("TickPay"))
        {
            if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TICK_PAY_SPLASH,activity)!=null && SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TICK_PAY_SPLASH,activity).equalsIgnoreCase("0"))
            {
                Intent intent = new Intent(activity,Tickpay_Splash_Activity.class);
                startActivity(intent);
                finish();
            }
            else if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_VERIFY_USER,activity)!=null && SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_VERIFY_USER,activity).equalsIgnoreCase("2"))
            {
                Intent intent = new Intent(activity,TickPayActivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }
            else if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_VERIFY_USER,activity)!=null && SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_VERIFY_USER,activity).equalsIgnoreCase("1"))
            {
                if (Global.isNetworkconn(activity))
                {
                    call_GetTickpayApprovalStatus();
                }
                else
                {
                    InternetDialog internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog("Please check your internet connection!");
                }
            }
            else if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TICK_PAY_SPLASH,activity)!=null && SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TICK_PAY_SPLASH,activity).equalsIgnoreCase("1"))
            {
                Intent intent = new Intent(activity,TickPayRegisterActivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }
        }
    }

    private void call_GetTickpayApprovalStatus()
    {
        String url = WebServiceAPI.API_GET_APPROVAL_STATUS + SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID,activity);
        Log.e("call", "url = " + url);
        dialogClass.showDialog();
        aQuery.ajax(url.trim(), null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e("responseCode", " = " + responseCode);
                    Log.e("Response", " = " + json);

                    if (json!=null)
                    {
                        if (json.has("Verify"))
                        {
                            if (json.getString("Verify")!=null)
                            {
                                String veriry = json.getString("Verify");

                                if (veriry.equalsIgnoreCase("1"))
                                {
                                    dialogClass.hideDialog();
                                    Intent intent = new Intent(activity,TickPayReview.class);
                                    startActivity(intent);
                                    finish();
                                    overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                                }
                                else if (veriry.equalsIgnoreCase("2"))
                                {
                                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_VERIFY_USER,"2",activity);
                                    Intent intent = new Intent(activity,TickPayActivity.class);
                                    startActivity(intent);
                                    finish();
                                    overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                                }
                                else
                                {
                                    dialogClass.hideDialog();
                                    Intent intent = new Intent(activity,TickPayReview.class);
                                    startActivity(intent);
                                    finish();
                                    overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                                }
                            }
                            else
                            {
                                dialogClass.hideDialog();
                                Intent intent = new Intent(activity,TickPayReview.class);
                                startActivity(intent);
                                finish();
                                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                            }
                        }
                        else
                        {
                            dialogClass.hideDialog();
                            Intent intent = new Intent(activity,TickPayReview.class);
                            startActivity(intent);
                            finish();
                            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                        }
                    }
                    else
                    {
                        dialogClass.hideDialog();
                        Intent intent = new Intent(activity,TickPayReview.class);
                        startActivity(intent);
                        finish();
                        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                    }
                }
                catch (Exception e)
                {
                    Log.e("Exception","Exception "+e.toString());
                    dialogClass.hideDialog();
                    Intent intent = new Intent(activity,TickPayReview.class);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                }
            }

        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    private void setZero(TextView editText)
    {
        editText.setText("0");
    }
    private void setOne(TextView editText)
    {
        editText.setText("1");
    }
    private void setTwo(TextView editText)
    {
        editText.setText("2");
    }
    private void setThree(TextView editText)
    {
        editText.setText("3");
    }
    private void setFour(TextView editText)
    {
        editText.setText("4");
    }
    private void setFive(TextView editText)
    {
        editText.setText("5");
    }
    private void setSix(TextView editText)
    {
        editText.setText("6");
    }
    private void setSeven(TextView editText)
    {
        editText.setText("7");
    }
    private void setEight(TextView editText)
    {
        editText.setText("8");
    }
    private void setNine(TextView editText)
    {
        editText.setText("9");
    }


    private void DriverLogOut(String driverId, final DialogInterface dialog)
    {
        String Fullname="",Email="",Password="",MobileNo="",Gender="",Image="",DeviceType="",Token="";
        String Lat="",Lng="",Status="",CreatedDate="",Id="",Address="";

        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_ID,Id,activity);
        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_FULL_NAME,Fullname,activity);
        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_EMAIL,Email,activity);
        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_PASSWORD,Password,activity);
        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_PHONE_NUMBER,MobileNo,activity);
        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_GENDER,Gender,activity);
        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_IMAGE,Image,activity);
        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_DEVICE_TYPE,DeviceType,activity);
        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TOKEN,Token,activity);
        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_LATITUDE,Lat,activity);
        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_LONGITUDE,Lng,activity);
        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_STATUS,Status,activity);
        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_CREATED_DATE,CreatedDate,activity);
        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_ADDRESS,Address,activity);
        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_LICENCE_IMAGE,"",activity);
        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BANK_ACCOUNT_NUMBER,"",activity);
        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BANK_NAME,"",activity);
        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_ABN,"",activity);
        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BSB,"",activity);
        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_COMPANY_NAME,"",activity);

        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_CAR_CLASS,"".toString(),activity);

        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL,"".toString(),activity);
        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG,"0".toString(),activity);
        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR,"-1".toString(),activity);

        Intent intent = new Intent(activity,LoginActivity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        SettingActivity.falge = 1;
        MainActivity.PasscodeBackPress = 1;
    }
}
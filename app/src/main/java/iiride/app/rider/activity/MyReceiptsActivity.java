package iiride.app.rider.activity;

import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.hendraanggrian.widget.ExpandableRecyclerView;

import iiride.app.rider.R;
import iiride.app.rider.adapter.MyReceipt_Adapter;
import iiride.app.rider.application.TicktocApplication;
import iiride.app.rider.been.MyReceipt_Been;
import iiride.app.rider.comman.Common;
import iiride.app.rider.comman.SessionSave;
import iiride.app.rider.comman.WebServiceAPI;
import iiride.app.rider.other.DialogClass;
import iiride.app.rider.other.Global;
import iiride.app.rider.other.InternetDialog;
import iiride.app.rider.view.MySnackBar;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MyReceiptsActivity extends AppCompatActivity implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener{

    public static MyReceiptsActivity activity;
    private LinearLayout ll_Back;
    private ImageView iv_Back;
    private TextView tv_Title;

    private ExpandableRecyclerView recyclerView;
    private ExpandableRecyclerView.Adapter mAdapter;
    private ExpandableRecyclerView.LayoutManager layoutManager;
    public static List<MyReceipt_Been> list = new ArrayList<MyReceipt_Been>();

    private String TAG = "CallMyRecipt";
    private AQuery aQuery;
    private DialogClass dialogClass;
    public SwipeRefreshLayout swipeRefreshLayout;
    int RunAct_FirstTime;
    TextView tv_NoDataFound;
    public static LinearLayout main_layout;
    private MySnackBar mySnackBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_receipts);

        activity = MyReceiptsActivity.this;
        mySnackBar = new MySnackBar(activity);
        dialogClass = new DialogClass(activity, 1);
        init();
    }




    private void init() {

        ll_Back = (LinearLayout) findViewById(R.id.back_layout);
        iv_Back = (ImageView) findViewById(R.id.back_imageview);

        tv_Title = (TextView) findViewById(R.id.title_textview);

        tv_Title.setText("My Receipts");

        aQuery = new AQuery(activity);
        list.clear();

        main_layout = (LinearLayout) findViewById(R.id.rootView);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);

        tv_NoDataFound = (TextView) findViewById(R.id.tv_NoDataFound);
        tv_NoDataFound.setVisibility(View.GONE);

        recyclerView = (ExpandableRecyclerView) findViewById(R.id.rv_dispatched_job);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(activity);
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new MyReceipt_Adapter((LinearLayoutManager) layoutManager);
        recyclerView.setAdapter(mAdapter);

        swipeRefreshLayout.setOnRefreshListener(activity);

        String userId = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID, activity);
        if (userId!=null && !userId.equalsIgnoreCase(""))
        {
            Log.e(TAG, "firstTime call");
            RunAct_FirstTime=0;
            if (Global.isNetworkconn(activity))
            {
                call_MyBookingHistoryApi();
            }
            else
            {
                InternetDialog internetDialog = new InternetDialog(activity);
                internetDialog.showDialog("Please check your internet connection!");
            }
        }

        ll_Back.setOnClickListener(activity);
        iv_Back.setOnClickListener(activity);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.back_layout:
                onBackPressed();
                break;

            case R.id.back_imageview:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);
    }

    @Override
    public void onRefresh()
    {
        String userId = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID, activity);
        if (userId!=null && !userId.equalsIgnoreCase(""))
        {
            Log.e(TAG, "On refresh");
            swipeRefreshLayout.setRefreshing(true);
            call_MyBookingHistoryApi();
        }
    }

    private void call_MyBookingHistoryApi()
    {
        tv_NoDataFound.setVisibility(View.GONE);
        list.clear();
        mAdapter.notifyDataSetChanged();
        if (RunAct_FirstTime==0)
        {
            dialogClass.showDialog();
            RunAct_FirstTime=1;
        }
        String url = WebServiceAPI.API_BOOKING_HISTORY + SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID,activity);
        Log.e("call", "url = " + url);

        aQuery.ajax(url.trim(), null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e("responseCode", " = " + responseCode);
                    Log.e("Response", " = " + json);

                    if (json!=null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                if (json.has("history"))
                                {
                                    JSONArray history = json.getJSONArray("history");

                                    if (history!=null && history.length()>0)
                                    {
                                        for (int i=0; i<history.length(); i++)
                                        {
                                            JSONObject jsonObject = history.getJSONObject(i);

                                            if (jsonObject!=null)
                                            {
                                                if (jsonObject.has("HistoryType"))
                                                {
                                                    if (jsonObject.getString("HistoryType")!=null && jsonObject.getString("HistoryType").equalsIgnoreCase("Past"))
                                                    {
                                                        String Id="",PassengerId="",ModelId="",DriverId="",CreatedDate="",TransactionId="",PaymentStatus="";
                                                        String PickupTime="",DropTime="",TripDuration="",TripDistance="",PickupLocation="",DropoffLocation="";
                                                        String NightFareApplicable="",NightFare="",TripFare="",WaitingTime="",WaitingTimeCost="",TollFee="",BookingCharge="";
                                                        String Tax="",PromoCode="",Discount="",SubTotal="",GrandTotal="",Status="",Reason="",PaymentType="",AdminAmount="";
                                                        String CompanyAmount="",PickupLat="",PickupLng="",DropOffLat="",DropOffLon="",Model="",DriverName="";
                                                        String CarDetails_Id="",CarDetails_CompanyId="",CarDetails_DriverId="",CarDetails_VehicleModel="";
                                                        String CarDetails_Company="",CarDetails_Color="",CarDetails_VehicleRegistrationNo="",CarDetails_RegistrationCertificate="";
                                                        String CarDetails_VehicleInsuranceCertificate="",CarDetails_RegistrationCertificateExpire="",CarDetails_VehicleInsuranceCertificateExpire="";
                                                        String CarDetails_VehicleImage="",CarDetails_Description="",HistoryType="",BookingType="";

                                                        if (jsonObject.has("Status"))
                                                        {
                                                            Status = jsonObject.getString("Status");
                                                        }


                                                        if (Status!=null && Status.equalsIgnoreCase("completed"))
                                                        {
                                                            if (jsonObject.has("Id"))
                                                            {
                                                                Id = jsonObject.getString("Id");
                                                            }

                                                            if (jsonObject.has("PassengerId"))
                                                            {
                                                                PassengerId = jsonObject.getString("PassengerId");
                                                            }

                                                            if (jsonObject.has("ModelId"))
                                                            {
                                                                ModelId = jsonObject.getString("ModelId");
                                                            }

                                                            if (jsonObject.has("DriverId"))
                                                            {
                                                                DriverId = jsonObject.getString("DriverId");
                                                            }

                                                            if (jsonObject.has("CreatedDate"))
                                                            {
                                                                CreatedDate = jsonObject.getString("CreatedDate");
                                                            }

                                                            if (jsonObject.has("TransactionId"))
                                                            {
                                                                TransactionId = jsonObject.getString("TransactionId");
                                                            }

                                                            if (jsonObject.has("PaymentStatus"))
                                                            {
                                                                PaymentStatus = jsonObject.getString("PaymentStatus");
                                                            }

                                                            if (jsonObject.has("PickupTime"))
                                                            {
                                                                PickupTime = jsonObject.getString("PickupTime");
                                                            }

                                                            if (jsonObject.has("DropTime"))
                                                            {
                                                                DropTime = jsonObject.getString("DropTime");
                                                            }

                                                            if (jsonObject.has("TripDuration"))
                                                            {
                                                                TripDuration = jsonObject.getString("TripDuration");
                                                            }

                                                            if (jsonObject.has("TripDistance"))
                                                            {
                                                                TripDistance = jsonObject.getString("TripDistance");
                                                            }

                                                            if (jsonObject.has("PickupLocation"))
                                                            {
                                                                PickupLocation = jsonObject.getString("PickupLocation");
                                                            }

                                                            if (jsonObject.has("DropoffLocation"))
                                                            {
                                                                DropoffLocation = jsonObject.getString("DropoffLocation");
                                                            }

                                                            if (jsonObject.has("NightFareApplicable"))
                                                            {
                                                                NightFareApplicable = jsonObject.getString("NightFareApplicable");
                                                            }

                                                            if (jsonObject.has("NightFare"))
                                                            {
                                                                NightFare = jsonObject.getString("NightFare");
                                                            }

                                                            if (jsonObject.has("TripFare"))
                                                            {
                                                                TripFare = jsonObject.getString("TripFare");
                                                            }

                                                            if (jsonObject.has("WaitingTime"))
                                                            {
                                                                WaitingTime = jsonObject.getString("WaitingTime");
                                                            }

                                                            if (jsonObject.has("WaitingTimeCost"))
                                                            {
                                                                WaitingTimeCost = jsonObject.getString("WaitingTimeCost");
                                                            }

                                                            if (jsonObject.has("TollFee"))
                                                            {
                                                                TollFee = jsonObject.getString("TollFee");
                                                            }

                                                            if (jsonObject.has("BookingCharge"))
                                                            {
                                                                BookingCharge = jsonObject.getString("BookingCharge");
                                                            }

                                                            if (jsonObject.has("Tax"))
                                                            {
                                                                Tax = jsonObject.getString("Tax");
                                                            }

                                                            if (jsonObject.has("PromoCode"))
                                                            {
                                                                PromoCode = jsonObject.getString("PromoCode");
                                                            }

                                                            if (jsonObject.has("Discount"))
                                                            {
                                                                Discount = jsonObject.getString("Discount");
                                                            }

                                                            if (jsonObject.has("SubTotal"))
                                                            {
                                                                SubTotal = jsonObject.getString("SubTotal");
                                                            }

                                                            if (jsonObject.has("GrandTotal"))
                                                            {
                                                                GrandTotal = jsonObject.getString("GrandTotal");
                                                            }

                                                            if (jsonObject.has("Reason"))
                                                            {
                                                                Reason = jsonObject.getString("Reason");
                                                            }

                                                            if (jsonObject.has("PaymentType"))
                                                            {
                                                                PaymentType = jsonObject.getString("PaymentType");
                                                            }

                                                            if (jsonObject.has("AdminAmount"))
                                                            {
                                                                AdminAmount = jsonObject.getString("AdminAmount");
                                                            }

                                                            if (jsonObject.has("CompanyAmount"))
                                                            {
                                                                CompanyAmount = jsonObject.getString("CompanyAmount");
                                                            }

                                                            if (jsonObject.has("PickupLat"))
                                                            {
                                                                PickupLat = jsonObject.getString("PickupLat");
                                                            }

                                                            if (jsonObject.has("PickupLng"))
                                                            {
                                                                PickupLng = jsonObject.getString("PickupLng");
                                                            }

                                                            if (jsonObject.has("DropOffLat"))
                                                            {
                                                                DropOffLat = jsonObject.getString("DropOffLat");
                                                            }

                                                            if (jsonObject.has("DropOffLon"))
                                                            {
                                                                DropOffLon = jsonObject.getString("DropOffLon");
                                                            }

                                                            if (jsonObject.has("Model"))
                                                            {
                                                                Model = jsonObject.getString("Model");
                                                            }

                                                            if (jsonObject.has("DriverName"))
                                                            {
                                                                DriverName = jsonObject.getString("DriverName");
                                                            }

                                                            if (jsonObject.has("CarDetails"))
                                                            {
                                                                JSONObject CarDetails = jsonObject.getJSONObject("CarDetails");

                                                                if (CarDetails!=null)
                                                                {
                                                                    if (CarDetails.has("Id"))
                                                                    {
                                                                        CarDetails_Id = CarDetails.getString("Id");
                                                                    }

                                                                    if (CarDetails.has("CompanyId"))
                                                                    {
                                                                        CarDetails_CompanyId = CarDetails.getString("CompanyId");
                                                                    }

                                                                    if (CarDetails.has("VehicleModel"))
                                                                    {
                                                                        CarDetails_VehicleModel = CarDetails.getString("VehicleModel");
                                                                    }

                                                                    if (CarDetails.has("Company"))
                                                                    {
                                                                        CarDetails_Company = CarDetails.getString("Company");
                                                                    }

                                                                    if (CarDetails.has("Color"))
                                                                    {
                                                                        CarDetails_Color = CarDetails.getString("Color");
                                                                    }

                                                                    if (CarDetails.has("VehicleRegistrationNo"))
                                                                    {
                                                                        CarDetails_VehicleRegistrationNo = CarDetails.getString("VehicleRegistrationNo");
                                                                    }

                                                                    if (CarDetails.has("RegistrationCertificate"))
                                                                    {
                                                                        CarDetails_RegistrationCertificate = CarDetails.getString("RegistrationCertificate");
                                                                    }

                                                                    if (CarDetails.has("VehicleInsuranceCertificate"))
                                                                    {
                                                                        CarDetails_VehicleInsuranceCertificate = CarDetails.getString("VehicleInsuranceCertificate");
                                                                    }

                                                                    if (CarDetails.has("RegistrationCertificateExpire"))
                                                                    {
                                                                        CarDetails_RegistrationCertificateExpire = CarDetails.getString("RegistrationCertificateExpire");
                                                                    }

                                                                    if (CarDetails.has("VehicleInsuranceCertificateExpire"))
                                                                    {
                                                                        CarDetails_VehicleInsuranceCertificateExpire = CarDetails.getString("VehicleInsuranceCertificateExpire");
                                                                    }

                                                                    if (CarDetails.has("VehicleImage"))
                                                                    {
                                                                        CarDetails_VehicleImage = WebServiceAPI.BASE_URL_IMAGE + CarDetails.getString("VehicleImage");
                                                                    }

                                                                    if (CarDetails.has("Description"))
                                                                    {
                                                                        CarDetails_Description = CarDetails.getString("Description");
                                                                    }
                                                                }
                                                            }

                                                            if (jsonObject.has("HistoryType"))
                                                            {
                                                                HistoryType = jsonObject.getString("HistoryType");
                                                            }

                                                            if (jsonObject.has("BookingType"))
                                                            {
                                                                BookingType = jsonObject.getString("BookingType");
                                                            }

                                                            String shreUrl = "";
                                                            if (jsonObject.has("ShareUrl"))
                                                            {
                                                                shreUrl = jsonObject.getString("ShareUrl");
                                                            }

                                                            list.add(new MyReceipt_Been(Id,
                                                                    PassengerId,
                                                                    ModelId,
                                                                    DriverId,
                                                                    CreatedDate,
                                                                    TransactionId,
                                                                    PaymentStatus,
                                                                    PickupTime,
                                                                    DropTime,
                                                                    TripDuration,
                                                                    TripDistance,
                                                                    PickupLocation,
                                                                    DropoffLocation,
                                                                    NightFareApplicable,
                                                                    NightFare,
                                                                    TripFare,
                                                                    WaitingTime,
                                                                    WaitingTimeCost,
                                                                    TollFee,
                                                                    BookingCharge,
                                                                    Tax,
                                                                    PromoCode,
                                                                    Discount,
                                                                    SubTotal,
                                                                    GrandTotal,
                                                                    Status,
                                                                    Reason,
                                                                    PaymentType,
                                                                    AdminAmount,
                                                                    CompanyAmount,
                                                                    PickupLat,
                                                                    PickupLng,
                                                                    DropOffLat,
                                                                    DropOffLon,
                                                                    Model,
                                                                    DriverName,
                                                                    CarDetails_Id,
                                                                    CarDetails_CompanyId,
                                                                    CarDetails_DriverId,
                                                                    CarDetails_VehicleModel,
                                                                    CarDetails_Company,
                                                                    CarDetails_Color,
                                                                    CarDetails_VehicleRegistrationNo,
                                                                    CarDetails_RegistrationCertificate,
                                                                    CarDetails_VehicleInsuranceCertificate,
                                                                    CarDetails_RegistrationCertificateExpire,
                                                                    CarDetails_VehicleInsuranceCertificateExpire,
                                                                    CarDetails_VehicleImage,
                                                                    CarDetails_Description,
                                                                    HistoryType,
                                                                    BookingType,
                                                                    shreUrl));
                                                        }
                                                        else
                                                        {
                                                            Log.e("call","status = "+Status);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        Log.e("call","history type not match at position = "+i);
                                                    }
                                                }
                                                else
                                                {
                                                    Log.e("call","HistoryType not found position = "+i);
                                                }
                                            }
                                            else
                                            {
                                                Log.e("call","jsonObject null position = "+i);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Log.e("call","history null or lenth 0");
                                    }

                                    Log.e("call","pastBooking_beens.size() = "+list.size());

                                    dialogClass.hideDialog();
                                    swipeRefreshLayout.setRefreshing(false);
                                    mAdapter.notifyDataSetChanged();
                                }
                                else
                                {
                                    Log.e("call","history not found");
                                    dialogClass.hideDialog();
                                    swipeRefreshLayout.setRefreshing(false);
                                    mAdapter.notifyDataSetChanged();
                                }
                            }
                            else
                            {
                                Log.e("call","status false");
                                dialogClass.hideDialog();
                                swipeRefreshLayout.setRefreshing(false);
                                mAdapter.notifyDataSetChanged();
                            }
                        }
                        else
                        {
                            Log.e("call","status not found");
                            dialogClass.hideDialog();
                            swipeRefreshLayout.setRefreshing(false);
                            mAdapter.notifyDataSetChanged();
                        }
                    }
                    else
                    {
                        Log.e("call","json null");
                        dialogClass.hideDialog();
                        swipeRefreshLayout.setRefreshing(false);
                        mAdapter.notifyDataSetChanged();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Exception","Exception "+e.toString());
                    dialogClass.hideDialog();
                    swipeRefreshLayout.setRefreshing(false);
                    mAdapter.notifyDataSetChanged();
                }
            }

        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    @Override
    protected void onResume() {
        super.onResume();

        TicktocApplication.setCurrentActivity(activity);

    }
}

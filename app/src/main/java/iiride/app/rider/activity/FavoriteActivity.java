package iiride.app.rider.activity;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import iiride.app.rider.R;
import iiride.app.rider.adapter.AddressAdapter;
import iiride.app.rider.application.TicktocApplication;
import iiride.app.rider.been.Address_Been;
import iiride.app.rider.comman.Common;
import iiride.app.rider.comman.SessionSave;
import iiride.app.rider.comman.WebServiceAPI;
import iiride.app.rider.other.DialogClass;
import iiride.app.rider.other.Global;
import iiride.app.rider.other.HPLinearLayoutManager;
import iiride.app.rider.other.InternetDialog;

import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;

public class FavoriteActivity extends AppCompatActivity implements View.OnClickListener{

    public static FavoriteActivity activity;
    private LinearLayout ll_Back;
    private ImageView iv_Back;
    private TextView tv_Title,tv_NoDataFound;
    private DialogClass dialogClass;
    private AQuery aQuery;
    private RecyclerView recyclerView;
    private ArrayList<Address_Been> address_beens = new ArrayList<Address_Been>();
    private AddressAdapter adapter;
    private HPLinearLayoutManager layoutManager;
    private LinearLayout ll_recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite);

        activity = FavoriteActivity.this;

        dialogClass = new DialogClass(activity,0);
        aQuery = new AQuery(activity);
        init();
    }




    private void init() {

        ll_Back = (LinearLayout) findViewById(R.id.back_layout);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        ll_recyclerView = (LinearLayout) findViewById(R.id.recyclerView_layout);

        iv_Back = (ImageView) findViewById(R.id.back_imageview);

        tv_Title = (TextView) findViewById(R.id.title_textview);
        tv_NoDataFound = (TextView) findViewById(R.id.no_data_found);

        tv_Title.setText("Favourite Location");

        // Layout Managers:
        layoutManager = new HPLinearLayoutManager(activity);
        layoutManager.setOrientation(HPLinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new AddressAdapter(address_beens,activity);
        recyclerView.setAdapter(adapter);

        if (address_beens.size()>0)
        {
            tv_NoDataFound.setVisibility(View.GONE);
            ll_recyclerView.setVisibility(View.VISIBLE);
        }
        else
        {
            tv_NoDataFound.setVisibility(View.GONE);
            ll_recyclerView.setVisibility(View.VISIBLE);
        }

        ll_Back.setOnClickListener(activity);
        iv_Back.setOnClickListener(activity);

        if (Global.isNetworkconn(activity))
        {
            getAddressList();
        }
        else
        {
            InternetDialog internetDialog = new InternetDialog(activity);
            internetDialog.showDialog("Please check your internet connection!");
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.back_layout:
                onBackPressed();
                break;

            case R.id.back_imageview:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        Log.e("call","onBackPress favorite flag = "+MainActivity.favorite);
        finish();
        overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);
    }

    @Override
    protected void onResume() {
        super.onResume();

        TicktocApplication.setCurrentActivity(activity);

    }

    public void getAddressList()
    {
        dialogClass.showDialog();
        String url = WebServiceAPI.API_GET_ADDRESS + SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID,activity);

        Log.e("call", "url = " + url);

        aQuery.ajax(url.trim(), null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e("responseCode", " = " + responseCode);
                    Log.e("Response", " = " + json);

                    if (json!=null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                if (json.has("address"))
                                {
                                    JSONArray address = json.getJSONArray("address");

                                    if (address!=null && address.length()>0)
                                    {
                                        for (int i=0; i<address.length(); i++)
                                        {
                                            JSONObject addressData = address.getJSONObject(i);

                                            if (addressData!=null)
                                            {
                                                String Id="",PassengerId="",Type="",Address="";
                                                double Lat=0,Lng=0;

                                                if (addressData.has("Id"))
                                                {
                                                    Id = addressData.getString("Id");
                                                }

                                                if (addressData.has("PassengerId"))
                                                {
                                                    PassengerId = addressData.getString("PassengerId");
                                                }

                                                if (addressData.has("Type"))
                                                {
                                                    Type = addressData.getString("Type");
                                                }

                                                if (addressData.has("Address"))
                                                {
                                                    Address = addressData.getString("Address");
                                                }

                                                if (addressData.has("Lat"))
                                                {
                                                    String latitude = addressData.getString("Lat");

                                                    if (latitude!=null && !latitude.equalsIgnoreCase(""))
                                                    {
                                                        Lat = Double.parseDouble(latitude);
                                                    }
                                                }

                                                if (addressData.has("Lng"))
                                                {
                                                    String longitude = addressData.getString("Lng");

                                                    if (longitude!=null && !longitude.equalsIgnoreCase(""))
                                                    {
                                                        Lng = Double.parseDouble(longitude);
                                                    }
                                                }

                                                address_beens.add(new Address_Been(Id,PassengerId,Type,Address,Lat,Lng));
                                            }
                                        }

                                        if (address_beens.size()>0)
                                        {
                                            dialogClass.hideDialog();
                                            ll_recyclerView.setVisibility(View.VISIBLE);
                                            tv_NoDataFound.setVisibility(View.GONE);
                                            adapter.notifyDataSetChanged();
                                        }
                                        else
                                        {
                                            dialogClass.hideDialog();
                                            ll_recyclerView.setVisibility(View.GONE);
                                            tv_NoDataFound.setVisibility(View.VISIBLE);
                                        }
                                    }
                                    else
                                    {
                                        Log.e("call","no cards available");
                                        dialogClass.hideDialog();
                                        ll_recyclerView.setVisibility(View.GONE);
                                        tv_NoDataFound.setVisibility(View.VISIBLE);
                                    }
                                }
                                else
                                {
                                    Log.e("call","no cards found");
                                    dialogClass.hideDialog();
                                    ll_recyclerView.setVisibility(View.GONE);
                                    tv_NoDataFound.setVisibility(View.VISIBLE);
                                }
                            }
                            else
                            {
                                Log.e("call","status false");
                                dialogClass.hideDialog();
                                ll_recyclerView.setVisibility(View.GONE);
                                tv_NoDataFound.setVisibility(View.VISIBLE);
                            }
                        }
                        else
                        {
                            Log.e("call","status not found");
                            dialogClass.hideDialog();
                            ll_recyclerView.setVisibility(View.GONE);
                            tv_NoDataFound.setVisibility(View.VISIBLE);
                        }

                    }
                    else
                    {
                        Log.e("call","json null");
                        dialogClass.hideDialog();
                        ll_recyclerView.setVisibility(View.GONE);
                        tv_NoDataFound.setVisibility(View.VISIBLE);
                    }
                }
                catch (Exception e)
                {
                    Log.e("Exception","Exception "+e.toString());
                    dialogClass.hideDialog();
                    ll_recyclerView.setVisibility(View.GONE);
                    tv_NoDataFound.setVisibility(View.VISIBLE);
                }
            }

        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }
}

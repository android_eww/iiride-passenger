package iiride.app.rider.activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import iiride.app.rider.R;
import iiride.app.rider.application.TicktocApplication;
import iiride.app.rider.comman.Common;
import iiride.app.rider.comman.SessionSave;


public class TickPayReview extends AppCompatActivity implements View.OnClickListener {

    public static TickPayReview activity;
    private LinearLayout ll_Back;
    private ImageView iv_Back;
    private TextView tv_Title;
    public static int resumeFlag = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tic_pay_review);

        activity = TickPayReview.this;
        resumeFlag = 1;
        init();
    }

    private void init() {

        ll_Back = (LinearLayout) findViewById(R.id.back_layout);
        iv_Back = (ImageView) findViewById(R.id.back_imageview);

        tv_Title = (TextView) findViewById(R.id.title_textview);
        tv_Title.setText("Tick Pay");

        ll_Back.setOnClickListener(activity);
        iv_Back.setOnClickListener(activity);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.back_layout:
                onBackPressed();
                break;

            case R.id.back_imageview:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);
    }

    @Override
    protected void onResume() {
        super.onResume();

        TicktocApplication.setCurrentActivity(activity);

    }

    @Override
    protected void onRestart() {
        super.onRestart();

        if (resumeFlag==1)
        {
            if (SessionSave.getUserSession(Common.IS_PASSCODE_REQUIRED,activity)!=null && SessionSave.getUserSession(Common.IS_PASSCODE_REQUIRED,activity).equalsIgnoreCase("1"))
            {
                resumeFlag = 0;
                Intent intent = new Intent(TickPayReview.this,Create_Passcode_Activity.class);
                intent.putExtra("from","TickPay");
                startActivity(intent);
                finish();
            }
        }
        else
        {
            resumeFlag=1;
        }
    }
}

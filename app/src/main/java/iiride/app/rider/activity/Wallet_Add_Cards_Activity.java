package iiride.app.rider.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import iiride.app.rider.R;
import iiride.app.rider.adapter.CreditCardList_Adapter;
import iiride.app.rider.application.TicktocApplication;
import iiride.app.rider.been.CreditCard_List_Been;
import iiride.app.rider.comman.Common;
import iiride.app.rider.comman.SessionSave;
import iiride.app.rider.comman.WebServiceAPI;
import iiride.app.rider.other.DialogClass;
import iiride.app.rider.other.Global;
import iiride.app.rider.other.InternetDialog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class Wallet_Add_Cards_Activity extends AppCompatActivity implements View.OnClickListener {

    public static Wallet_Add_Cards_Activity activity;

    private LinearLayout ll_Back, ll_Add;
    private ImageView iv_Back, iv_Add;
    private TextView tv_Title;

    private RecyclerView recyclerView;
    private CreditCardList_Adapter adapter;
    private List<CreditCard_List_Been> cardList = new ArrayList<>();
    private AQuery aQuery;
    private DialogClass dialogClass;
    public static boolean callApi = false;
    public static String from = "", pastDueId = "";
    private String strCardList = "";
    public static int resumeFlag = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_add_card);

        activity = Wallet_Add_Cards_Activity.this;
        callApi = false;
        from = "";
        resumeFlag = 1;
        aQuery = new AQuery(activity);
        dialogClass = new DialogClass(activity, 0);
        strCardList = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_CARD_LIST, activity);
        if (this.getIntent() != null) {
            if (this.getIntent().getStringExtra("from") != null) {
                from = this.getIntent().getStringExtra("from");
            } else {
                from = "";
            }

            if (this.getIntent().hasExtra("pastDueId")) {
                pastDueId = this.getIntent().getStringExtra("pastDueId");
            } else {
                pastDueId = "";
            }
        } else {
            from = "";
            pastDueId = "";
        }

        initUI();
    }

    private void initUI() {
        ll_Back = (LinearLayout) findViewById(R.id.back_layout);
        iv_Back = (ImageView) findViewById(R.id.back_imageview);
        ll_Add = (LinearLayout) findViewById(R.id.ll_right);
        iv_Add = (ImageView) findViewById(R.id.iv_right);
        ll_Add.setVisibility(View.VISIBLE);

        tv_Title = (TextView) findViewById(R.id.title_textview);

        tv_Title.setText("Cards");

        recyclerView = (RecyclerView) findViewById(R.id.rv_CreditCard);

        adapter = new CreditCardList_Adapter(activity, cardList);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        ll_Back.setOnClickListener(activity);
        iv_Back.setOnClickListener(activity);
        ll_Add.setOnClickListener(activity);
        iv_Add.setOnClickListener(activity);

        if (checkCardList()) {
            saveCardList();
        } else {
            if (Global.isNetworkconn(activity)) {
                getAllCards();
            } else {
                InternetDialog internetDialog = new InternetDialog(activity);
                internetDialog.showDialog("Please check your internet connection!");
            }
        }
    }

    public boolean checkCardList() {
        try {
            Log.e("call", "111111111111111");
            String strCardList = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_CARD_LIST, activity);
            List<CreditCard_List_Been> cardListBeens = new ArrayList<CreditCard_List_Been>();

            if (strCardList != null && !strCardList.equalsIgnoreCase("")) {
                Log.e("call", "222222222222222");
                JSONObject json = new JSONObject(strCardList);

                if (json != null) {
                    Log.e("call", "33333333333333");
                    if (json.has("cards")) {
                        Log.e("call", "6666666666666666");
                        JSONArray cards = json.getJSONArray("cards");

                        if (cards != null && cards.length() > 0) {
                            return true;
                        } else {
                            Log.e("call", "no cards available");
                            return false;
                        }
                    } else {
                        Log.e("call", "no cards found");
                        return false;
                    }
                } else {
                    Log.e("call", "json null");
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception e) {
            Log.e("call", "Exception in getting card list = " + e.getMessage());
            return false;
        }
    }

    public void saveCardList() {
        try {
            Log.e("call", "111111111111111");
            cardList.clear();
            if (strCardList != null && !strCardList.equalsIgnoreCase("")) {
                Log.e("call", "222222222222222");
                JSONObject json = new JSONObject(strCardList);

                if (json != null) {
                    Log.e("call", "33333333333333");
                    if (json.has("cards")) {
                        Log.e("call", "6666666666666666");
                        JSONArray cards = json.getJSONArray("cards");

                        if (cards != null && cards.length() > 0) {
                            Log.e("call", "77777777777777");
                            for (int i = 0; i < cards.length(); i++) {
                                Log.e("call", "8888888888888");
                                JSONObject cardsData = cards.getJSONObject(i);

                                if (cardsData != null) {
                                    String Id = "", CardNum = "", CardNum_ = "", Type = "", Alias = "";

                                    if (cardsData.has("Id")) {
                                        Id = cardsData.getString("Id");
                                    }

                                    if (cardsData.has("CardNum")) {
                                        CardNum = cardsData.getString("CardNum");
                                    }

                                    if (cardsData.has("CardNum2")) {
                                        CardNum_ = cardsData.getString("CardNum2");
                                    }

                                    if (cardsData.has("Type")) {
                                        Type = cardsData.getString("Type");
                                    }

                                    if (cardsData.has("Alias")) {
                                        Alias = cardsData.getString("Alias");
                                    }

                                    cardList.add(new CreditCard_List_Been(Id, CardNum, CardNum_, Type, Alias));
                                }
                            }
                            Log.e("call", "9999999999999 size = " + cardList.size());
                            adapter.notifyDataSetChanged();
                        } else {
                            Log.e("call", "no cards available");
                        }
                    } else {
                        Log.e("call", "no cards found");
                    }
                } else {
                    Log.e("call", "json null");
                }
            }
        } catch (Exception e) {
            Log.e("call", "Exception in getting card list = " + e.getMessage());
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_layout:
                onBackPressed();
                break;

            case R.id.back_imageview:
                onBackPressed();
                break;

            case R.id.ll_right:
                resumeFlag = 0;
                gotoAddCardScreen();
                break;

            case R.id.iv_right:
                resumeFlag = 0;
                gotoAddCardScreen();
                break;
        }
    }

    private void gotoAddCardScreen() {
        Intent i = new Intent(activity, Add_Card_In_List_Activity.class);
        startActivity(i);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
    }

    @Override
    protected void onResume() {
        super.onResume();

        TicktocApplication.setCurrentActivity(activity);

        if (callApi) {
            strCardList = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_CARD_LIST, activity);

            if (strCardList != null && !strCardList.equalsIgnoreCase("")) {
                saveCardList();
            } else {
                if (Global.isNetworkconn(activity)) {
                    getAllCards();
                } else {
                    InternetDialog internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog("Please check your internet connection!");
                }
            }
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        if (resumeFlag == 1) {
            if (SessionSave.getUserSession(Common.IS_PASSCODE_REQUIRED, activity) != null && SessionSave.getUserSession(Common.IS_PASSCODE_REQUIRED, activity).equalsIgnoreCase("1")) {
                resumeFlag = 0;

                if (Wallet__Activity.activity != null) {
                    Wallet__Activity.activity.finish();
                }

                if (Wallet_Balance_Activity.activity != null) {
                    Wallet_Balance_Activity.activity.finish();
                }

                if (Wallet_Balance_TopUp_Activity.activity != null) {
                    Wallet_Balance_TopUp_Activity.activity.finish();
                }

                if (Add_Card_In_List_Activity.activity != null) {
                    Add_Card_In_List_Activity.activity.finish();
                }

                Intent intent = new Intent(activity, Create_Passcode_Activity.class);
                intent.putExtra("from", "Wallet__Activity");
                startActivity(intent);
                finish();
            }
        } else {
            resumeFlag = 1;
        }
    }

    public void getAllCards() {
        callApi = false;
        dialogClass.showDialog();
        cardList.clear();
        String url = WebServiceAPI.API_GET_ALL_CARDS + SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID, activity);

        Log.e("call", "url = " + url);

        aQuery.ajax(url.trim(), null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("responseCode", " = " + responseCode);
                    Log.e("Response", " = " + json);

                    if (json != null) {
                        if (json.has("status")) {
                            if (json.getBoolean("status")) {
                                if (json.has("cards")) {
                                    JSONArray cards = json.getJSONArray("cards");

                                    if (cards != null && cards.length() > 0) {
                                        for (int i = 0; i < cards.length(); i++) {
                                            JSONObject cardsData = cards.getJSONObject(i);

                                            if (cardsData != null) {
                                                String Id = "", CardNum = "", CardNum_ = "", Type = "", Alias = "";

                                                if (cardsData.has("Id")) {
                                                    Id = cardsData.getString("Id");
                                                }

                                                if (cardsData.has("CardNum")) {
                                                    CardNum = cardsData.getString("CardNum");
                                                }

                                                if (cardsData.has("CardNum2")) {
                                                    CardNum_ = cardsData.getString("CardNum2");
                                                }

                                                if (cardsData.has("Type")) {
                                                    Type = cardsData.getString("Type");
                                                }

                                                if (cardsData.has("Alias")) {
                                                    Alias = cardsData.getString("Alias");
                                                }

                                                cardList.add(new CreditCard_List_Been(Id, CardNum, CardNum_, Type, Alias));
                                            }
                                        }

                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_CARD_LIST, json.toString(), activity);

                                        dialogClass.hideDialog();
                                        adapter.notifyDataSetChanged();
                                    } else {
                                        Log.e("call", "no cards available");
                                        dialogClass.hideDialog();
                                        adapter.notifyDataSetChanged();
                                    }
                                } else {
                                    Log.e("call", "no cards found");
                                    dialogClass.hideDialog();
                                    adapter.notifyDataSetChanged();
                                }
                            } else {
                                Log.e("call", "status false");
                                dialogClass.hideDialog();
                                adapter.notifyDataSetChanged();
                            }
                        } else {
                            Log.e("call", "status not found");
                            dialogClass.hideDialog();
                            adapter.notifyDataSetChanged();
                        }
                    } else {
                        Log.e("call", "json null");
                        dialogClass.hideDialog();
                        adapter.notifyDataSetChanged();
                    }
                } catch (Exception e) {
                    Log.e("Exception", "Exception " + e.toString());
                    dialogClass.hideDialog();
                    adapter.notifyDataSetChanged();
                }
            }

        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }
}
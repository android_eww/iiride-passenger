package iiride.app.rider.activity;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import iiride.app.rider.R;
import iiride.app.rider.adapter.BookTableAdapter;
import iiride.app.rider.application.TicktocApplication;
import iiride.app.rider.been.BookTable_Been;

import java.util.ArrayList;
import java.util.List;


public class BookTableActivity extends AppCompatActivity implements View.OnClickListener{

    public static BookTableActivity activity;
    private LinearLayout ll_Back;
    private ImageView iv_Back;
    private TextView tv_Title;
    private GridView gridView;
    private List<BookTable_Been> list = new ArrayList<BookTable_Been>();
    private BookTableAdapter gridAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_table);

        activity = BookTableActivity.this;

        init();
    }




    private void init() {

        ll_Back = (LinearLayout) findViewById(R.id.back_layout);
        iv_Back = (ImageView) findViewById(R.id.back_imageview);

        tv_Title = (TextView) findViewById(R.id.title_textview);

        tv_Title.setText("Restaurants");

        gridView = (GridView)findViewById(R.id.grid_view);

        list.clear();

        list.add(new BookTable_Been(R.drawable.ic_asian,"Asian","restaurants"));
        list.add(new BookTable_Been(R.drawable.ic_italian,"Italian","restaurants"));
        list.add(new BookTable_Been(R.drawable.ic_mexican,"Mexican","restaurants"));
        list.add(new BookTable_Been(R.drawable.ic_seafood,"Seafood","restaurants"));
        list.add(new BookTable_Been(R.drawable.ic_indian,"Indian","restaurants"));
        list.add(new BookTable_Been(R.drawable.ic_middle,"Japanese","restaurants"));
        list.add(new BookTable_Been(R.drawable.ic_estrern,"Middle Eastern","restaurants"));
        list.add(new BookTable_Been(R.drawable.ic_burger,"Burgers","restaurants"));
        list.add(new BookTable_Been(R.drawable.ic_pizza,"Pizza","restaurants"));
        list.add(new BookTable_Been(R.drawable.ic_french,"French","restaurants"));
        list.add(new BookTable_Been(R.drawable.ic_greek,"Greek","restaurants"));
        list.add(new BookTable_Been(R.drawable.ic_greek,"Mediterranean","restaurants"));

        gridAdapter = new BookTableAdapter(activity,list);
        gridView.setAdapter(gridAdapter);

        ll_Back.setOnClickListener(activity);
        iv_Back.setOnClickListener(activity);

    }

    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.back_layout:
                onBackPressed();
                break;

            case R.id.back_imageview:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);
    }

    @Override
    protected void onResume() {
        super.onResume();

        TicktocApplication.setCurrentActivity(activity);

    }
}

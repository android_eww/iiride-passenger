package iiride.app.rider.activity;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import iiride.app.rider.R;
import iiride.app.rider.application.TicktocApplication;
import iiride.app.rider.comman.Common;
import iiride.app.rider.comman.SessionSave;
import iiride.app.rider.comman.WebServiceAPI;
import iiride.app.rider.other.DialogClass;
import iiride.app.rider.other.Global;
import iiride.app.rider.other.InternetDialog;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class Profile_AccountDetail_Activity extends AppCompatActivity implements View.OnClickListener{

    Profile_AccountDetail_Activity activity;

    ImageView iv_back, iv_save;
    LinearLayout ll_save, main_layout;

    EditText et_acountHolder_name, et_abn, et_bank_name, et_bsb, et_bankAccount, et_description;

    DialogClass dialogClass;
    AQuery aQuery;
    String userId;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_accountdetail);

        activity = Profile_AccountDetail_Activity.this;

        aQuery = new AQuery(activity);

        initUI();
    }

    private void initUI() {
        iv_back = (ImageView) findViewById(R.id.iv_back);
        iv_save = (ImageView) findViewById(R.id.iv_save);

        ll_save = (LinearLayout) findViewById(R.id.ll_save);
        main_layout = (LinearLayout) findViewById(R.id.main_layout);

        et_acountHolder_name = (EditText) findViewById(R.id.et_acountHolder_name);
        et_abn = (EditText) findViewById(R.id.et_abn);
        et_bank_name = (EditText) findViewById(R.id.et_bank_name);
        et_bsb = (EditText) findViewById(R.id.et_bsb);
        et_bankAccount = (EditText) findViewById(R.id.et_bankAccount);
        et_description = (EditText) findViewById(R.id.et_description);


        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_COMPANY_NAME,activity)!= null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_COMPANY_NAME,activity).equalsIgnoreCase(""))
        {
            et_acountHolder_name.setText(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_COMPANY_NAME,activity));
        }

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ABN,activity)!= null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ABN,activity).equalsIgnoreCase(""))
        {
            et_abn.setText(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ABN,activity));
        }
        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BANK_NAME,activity)!= null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BANK_NAME,activity).equalsIgnoreCase(""))
        {
            et_bank_name.setText(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BANK_NAME,activity));
        }

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BSB,activity)!= null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BSB,activity).equalsIgnoreCase(""))
        {
            et_bsb.setText(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BSB,activity));
        }

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BANK_ACCOUNT_NUMBER,activity)!= null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BANK_ACCOUNT_NUMBER,activity).equalsIgnoreCase(""))
        {
            et_bankAccount.setText(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BANK_ACCOUNT_NUMBER,activity));
        }
        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_DESCRIPTION,activity)!= null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_DESCRIPTION,activity).equalsIgnoreCase(""))
        {
            et_description.setText(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_DESCRIPTION,activity));
        }


        iv_back.setOnClickListener(this);
        iv_save.setOnClickListener(this);
        ll_save.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.iv_back:
                onBackPressed();
                break;

            case R.id.iv_save:
                CheckData();
                break;

            case R.id.ll_save:
                CheckData();
                break;
        }
    }

    private void CheckData()
    {
        if (TextUtils.isEmpty(et_acountHolder_name.getText().toString()))
        {
            InternetDialog internetDialog = new InternetDialog(activity);
            internetDialog.showDialog("Please enter Account Holder Name!");
        }
        else  if (TextUtils.isEmpty(et_abn.getText().toString()))
        {
            InternetDialog internetDialog = new InternetDialog(activity);
            internetDialog.showDialog("Please enter ABN!");
        }
        else  if (TextUtils.isEmpty(et_bank_name.getText().toString()))
        {
            InternetDialog internetDialog = new InternetDialog(activity);
            internetDialog.showDialog("Please enter Bank Name!");
        }
        else  if (TextUtils.isEmpty(et_bsb.getText().toString()))
        {
            InternetDialog internetDialog = new InternetDialog(activity);
            internetDialog.showDialog("Please enter BSB!");
        }
        else  if (TextUtils.isEmpty(et_bankAccount.getText().toString()))
        {
            InternetDialog internetDialog = new InternetDialog(activity);
            internetDialog.showDialog("Please enter BSB!");
        }
        else  if (TextUtils.isEmpty(et_description.getText().toString()))
        {
            InternetDialog internetDialog = new InternetDialog(activity);
            internetDialog.showDialog("Please enter Description!");
        }
        else
        {
            userId = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID, activity);
            if (userId!=null && !userId.equalsIgnoreCase(""))
            {
                if (Global.isNetworkconn(activity))
                {
                    UpdateAccountDetail(userId);
                }
                else
                {
                    InternetDialog internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog("Please check your internet connection!");
                }
            }
        }
    }

    private void UpdateAccountDetail(String userId)
    {
        dialogClass = new DialogClass(activity, 1);
        dialogClass.showDialog();
        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.API_UPDATE_BANK_ACCOUNT_DETAIL;

        //PassengerId,AccountHolderName,ABN,BankName,BSB,BankAccountNo,Description

        params.put(WebServiceAPI.PARAM_PASSENGER_ID, userId);
        params.put(WebServiceAPI.PARAM_ACCOUNT_HOLDER_NAME, et_acountHolder_name.getText().toString());
        params.put(WebServiceAPI.PARAM_ABN, et_abn.getText().toString());
        params.put(WebServiceAPI.PARAM_BANK_NAME, et_bank_name.getText().toString());
        params.put(WebServiceAPI.PARAM_BSB, et_bsb.getText().toString());
        params.put(WebServiceAPI.PARAM_BANK_ACCOUNT_NUMBER, et_bankAccount.getText().toString());
        params.put(WebServiceAPI.PARAM_DESCRIPTION, et_description.getText().toString());

        Log.e("url", "UpdateCarDetail = " + url);
        Log.e("param", "UpdateCarDetail = " + params);

        aQuery.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e("responseCode", "UpdateCarDetail = " + responseCode);
                    Log.e("Response", "UpdateCarDetail = " + json);

                    if (json!=null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                if (json.has("profile"))
                                {
                                    JSONObject profile = json.getJSONObject("profile");

                                    if (profile!=null)
                                    {
                                        String Fullname="",Email="",Password="",MobileNo="",Gender="",Image="",DeviceType="",Token="";
                                        String Lat="",Lng="",Status="",CreatedDate="",Id="",Address="",QRCode="",Verify="";
                                        String CompanyName="",ABN="",LicenceImage="",referalCode="",BankName="",BSB="",BankAccountNo="",Description="";

                                        if (profile.has("Id"))
                                        {
                                            Id = profile.getString("Id");
                                        }

                                        if (profile.has("Fullname"))
                                        {
                                            Fullname = profile.getString("Fullname");
                                        }

                                        if (profile.has("Email"))
                                        {
                                            Email = profile.getString("Email");
                                        }

                                        if (profile.has("Password"))
                                        {
                                            Password = profile.getString("Password");
                                        }

                                        if (profile.has("MobileNo"))
                                        {
                                            MobileNo = profile.getString("MobileNo");
                                        }

                                        if (profile.has("Image"))
                                        {
                                            Image = profile.getString("Image");
                                        }

                                        if (profile.has("QRCode"))
                                        {
                                            QRCode = profile.getString("QRCode");
                                        }

                                        if (profile.has("CompanyName"))
                                        {
                                            CompanyName = profile.getString("CompanyName");
                                        }

                                        if (profile.has("ABN"))
                                        {
                                            ABN = profile.getString("ABN");
                                        }

                                        if (profile.has("BankName"))
                                        {
                                            BankName = profile.getString("BankName");
                                        }

                                        if (profile.has("BSB"))
                                        {
                                            BSB = profile.getString("BSB");
                                        }

                                        if (profile.has("BankAccountNo"))
                                        {
                                            BankAccountNo = profile.getString("BankAccountNo");
                                        }

                                        if (profile.has("Description"))
                                        {
                                            Description = profile.getString("Description");
                                        }

                                        if (profile.has("LicenceImage"))
                                        {
                                            LicenceImage = profile.getString("LicenceImage");
                                        }

                                        if (profile.has("Gender"))
                                        {
                                            Gender = profile.getString("Gender");
                                        }

                                        if (profile.has("ReferralCode"))
                                        {
                                            referalCode = profile.getString("ReferralCode");
                                        }

                                        if (profile.has("Address"))
                                        {
                                            Address = profile.getString("Address");
                                        }

                                        if (profile.has("DeviceType"))
                                        {
                                            DeviceType = profile.getString("DeviceType");
                                        }

                                        if (profile.has("Token"))
                                        {
                                            Token = profile.getString("Token");
                                        }

                                        if (profile.has("Lat"))
                                        {
                                            Lat = profile.getString("Lat");
                                        }

                                        if (profile.has("Lng"))
                                        {
                                            Lng = profile.getString("Lng");
                                        }

                                        if (profile.has("Status"))
                                        {
                                            Status = profile.getString("Status");
                                        }

                                        if (profile.has("CreatedDate"))
                                        {
                                            CreatedDate = profile.getString("CreatedDate");
                                        }

                                        if (profile.has("Verify"))
                                        {
                                            Verify = profile.getString("Verify");
                                        }

                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_ID,Id,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_FULL_NAME,Fullname,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_EMAIL,Email,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_PASSWORD,Password,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_PHONE_NUMBER,MobileNo,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_GENDER,Gender,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_IMAGE,Image,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_DEVICE_TYPE,DeviceType,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TOKEN,Token,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_LATITUDE,Lat,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_LONGITUDE,Lng,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_STATUS,Status,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_CREATED_DATE,CreatedDate,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_ADDRESS,Address,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_QR_CODE,QRCode,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_VERIFY_USER,Verify,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_COMPANY_NAME,CompanyName,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_ABN,ABN,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_LICENCE_IMAGE,LicenceImage,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_REFERAL_CODE,referalCode,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BANK_NAME,BankName,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BSB,BSB,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BANK_ACCOUNT_NUMBER,BankAccountNo,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_DESCRIPTION,Description,activity);

                                        dialogClass.hideDialog();
                                        InternetDialog internetDialog = new InternetDialog(activity);
                                        internetDialog.showDialog("Bank detail updated successfully!");
                                    }
                                }
                                else
                                {
                                    Log.e("errorrr","profile not available");
                                    dialogClass.hideDialog();
                                }
                            }
                        }
                    }
                    dialogClass.hideDialog();
                }
                catch (Exception e)
                {
                    Log.e("UpdateCarDetail", "Exception : " + e.toString());
                    dialogClass.hideDialog();
                }
            }
        }.header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (activity!=null)
        {
            TicktocApplication.setCurrentActivity(activity);
        }
    }
}
package iiride.app.rider.activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import iiride.app.rider.R;
import iiride.app.rider.adapter.Transfer_History_Adapter;
import iiride.app.rider.application.TicktocApplication;
import iiride.app.rider.been.Transfer_History_Been;
import iiride.app.rider.comman.Common;
import iiride.app.rider.comman.SessionSave;
import iiride.app.rider.comman.WebServiceAPI;
import iiride.app.rider.other.DialogClass;
import iiride.app.rider.other.Global;
import iiride.app.rider.other.InternetDialog;

import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;


public class Wallet_Balance_Activity extends AppCompatActivity implements View.OnClickListener
{
    public static Wallet_Balance_Activity activity;

    private LinearLayout ll_Back;
    private ImageView iv_Back;
    private TextView tv_Title;

    private ImageView iv_bank,iv_dollar,iv_history;
    private TextView tv_bank,tv_dollar,tv_history;
    private LinearLayout ll_dollar,ll_bank,ll_history;
    private RecyclerView rv_transferHistory;
    private List<Transfer_History_Been> list = new ArrayList<>();
    private Transfer_History_Adapter adapter;
    private AQuery aQuery;
    private DialogClass dialogClass;
    public static boolean callGetMonny = false;
    private TextView tv_WalleteBallence;
    public static int resumeFlag = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_balance);

        activity = Wallet_Balance_Activity.this;
        callGetMonny = false;
        resumeFlag = 1;
        aQuery = new AQuery(activity);
        dialogClass = new DialogClass(activity,0);
        initUI();
    }

    private void initUI()
    {
        ll_Back = (LinearLayout) findViewById(R.id.back_layout);
        iv_Back = (ImageView) findViewById(R.id.back_imageview);

        tv_Title = (TextView) findViewById(R.id.title_textview);
        tv_Title.setText("Balance");

        ll_dollar = (LinearLayout)findViewById(R.id.ll_dollar);
        ll_bank = (LinearLayout)findViewById(R.id.ll_bank);
        ll_history = (LinearLayout)findViewById(R.id.ll_history);

        iv_dollar = (ImageView)findViewById(R.id.iv_dollar);
        iv_bank = (ImageView)findViewById(R.id.iv_bank);
        iv_history = (ImageView)findViewById(R.id.iv_history);

        tv_dollar = (TextView)findViewById(R.id.tv_dollar);
        tv_bank = (TextView)findViewById(R.id.tv_bank);
        tv_history = (TextView)findViewById(R.id.tv_history);
        tv_WalleteBallence = (TextView)findViewById(R.id.wallet_ballence);

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_WALLET_BALLENCE,activity)!=null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_WALLET_BALLENCE,activity).equalsIgnoreCase(""))
        {
            tv_WalleteBallence.setText("$"+SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_WALLET_BALLENCE,activity));
        }
        else
        {
            tv_WalleteBallence.setText("$0.00");
        }

        rv_transferHistory = (RecyclerView) findViewById(R.id.rv_transferHistory);

        adapter = new Transfer_History_Adapter(activity, list);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
        rv_transferHistory.setLayoutManager(mLayoutManager);
        rv_transferHistory.setItemAnimator(new DefaultItemAnimator());
        rv_transferHistory.setAdapter(adapter);

        ll_dollar.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                switch(event.getAction()) {

                    case MotionEvent.ACTION_DOWN:
                        iv_dollar.setImageResource(R.drawable.ic_dollar_select);
                        tv_dollar.setTextColor(ContextCompat.getColor(activity, R.color.colorSelectText));
                        break;

                    case MotionEvent.ACTION_UP:
                        iv_dollar.setImageResource(R.drawable.ic_dollar_unselect);
                        tv_dollar.setTextColor(ContextCompat.getColor(activity, R.color.colorUnselectText));
                        break;
                }

                return false;
            }
        });

        ll_bank.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                switch(event.getAction()) {

                    case MotionEvent.ACTION_DOWN:
                        iv_bank.setImageResource(R.drawable.ic_bank_select);
                        tv_bank.setTextColor(ContextCompat.getColor(activity, R.color.colorSelectText));
                        break;

                    case MotionEvent.ACTION_UP:
                        iv_bank.setImageResource(R.drawable.ic_bank_unselect);
                        tv_bank.setTextColor(ContextCompat.getColor(activity, R.color.colorUnselectText));
                        break;
                }

                return false;
            }
        });

        ll_history.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                switch(event.getAction()) {

                    case MotionEvent.ACTION_DOWN:
                        iv_history.setImageResource(R.drawable.ic_history_select);
                        tv_history.setTextColor(ContextCompat.getColor(activity, R.color.colorSelectText));
                        break;

                    case MotionEvent.ACTION_UP:
                        iv_history.setImageResource(R.drawable.ic_history_unselect);
                        tv_history.setTextColor(ContextCompat.getColor(activity, R.color.colorUnselectText));
                        break;
                }

                return false;
            }
        });

        ll_Back.setOnClickListener(activity);
        iv_Back.setOnClickListener(activity);
        ll_dollar.setOnClickListener(activity);
        ll_bank.setOnClickListener(activity);
        ll_history.setOnClickListener(activity);

        if (Global.isNetworkconn(activity))
        {
            getMoneyHistory();
        }
        else
        {
            InternetDialog internetDialog = new InternetDialog(activity);
            internetDialog.showDialog("Please check your internet connection!");
        }
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.back_layout:
                onBackPressed();
                break;

            case R.id.back_imageview:
                onBackPressed();
                break;

            case R.id.ll_dollar:
                resumeFlag = 0;
                Goto_TopUp_dollar();
                break;

            case R.id.ll_bank:
                resumeFlag = 0;
                Goto_TransferToBank();
                break;

            case R.id.ll_history:
                resumeFlag = 0;
                Goto_TransferHistory();
                break;

        }
    }

    private void Goto_TopUp_dollar()
    {
        Intent intent = new Intent(Wallet_Balance_Activity.this, Wallet_Balance_TopUp_Activity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
    }

    private void Goto_TransferToBank()
    {
        Intent intent = new Intent(Wallet_Balance_Activity.this, Wallet_Balance_TransferToBank_Activity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
    }

    private void Goto_TransferHistory()
    {
        Intent intent = new Intent(activity, Wallet_Transfer_History_Activity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
    }

    @Override
    protected void onResume() {
        super.onResume();

        TicktocApplication.setCurrentActivity(activity);

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_WALLET_BALLENCE,activity)!=null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_WALLET_BALLENCE,activity).equalsIgnoreCase(""))
        {
            tv_WalleteBallence.setText("$"+SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_WALLET_BALLENCE,activity));
        }
        else
        {
            tv_WalleteBallence.setText("$0.00");
        }

        if (callGetMonny)
        {
            if (Global.isNetworkconn(activity))
            {
                getMoneyHistory();
            }
            else
            {
                InternetDialog internetDialog = new InternetDialog(activity);
                internetDialog.showDialog("Please check your internet connection!");
            }
        }

    }

    @Override
    protected void onRestart() {
        super.onRestart();

        Log.e("call","resumeFlag = "+resumeFlag);
        Log.e("call","IS_PASSCODE_REQUIRED = "+SessionSave.getUserSession(Common.IS_PASSCODE_REQUIRED,activity));

        if (resumeFlag==1)
        {
            if (SessionSave.getUserSession(Common.IS_PASSCODE_REQUIRED,activity)!=null && SessionSave.getUserSession(Common.IS_PASSCODE_REQUIRED,activity).equalsIgnoreCase("1"))
            {
                resumeFlag = 0;

                if (Wallet__Activity.activity!=null)
                {
                    Wallet__Activity.activity.finish();
                }

                Intent intent = new Intent(activity,Create_Passcode_Activity.class);
                intent.putExtra("from","Wallet__Activity");
                startActivity(intent);
                finish();
            }
        }
        else
        {
            resumeFlag=1;
        }
    }

    public void getMoneyHistory()
    {
        callGetMonny = false;
        dialogClass.showDialog();
        list.clear();
        String url = WebServiceAPI.API_GET_TRANSACTION_HISTORY + SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID,activity);

        Log.e("call", "url = " + url);

        aQuery.ajax(url.trim(), null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e("responseCode", " = " + responseCode);
                    Log.e("Response", " = " + json);

                    if (json!=null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                if (json.has("walletBalance"))
                                {
                                    String walleteBallence = json.getString("walletBalance");

                                    if (walleteBallence!=null && !walleteBallence.equalsIgnoreCase(""))
                                    {
                                        Wallet__Activity.walleteBallence = walleteBallence;
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_WALLET_BALLENCE,Wallet__Activity.walleteBallence,activity);
                                        tv_WalleteBallence.setText("$"+walleteBallence);
                                    }
                                    else
                                    {
                                        tv_WalleteBallence.setText("$0");
                                    }
                                }
                                else
                                {
                                    tv_WalleteBallence.setText("$0");
                                }

                                if (json.has("history"))
                                {
                                    JSONArray history = json.getJSONArray("history");

                                    if (history!=null && history.length()>0)
                                    {
                                        int lenth = history.length();

                                        if (history.length()>5)
                                        {
                                            lenth = 5;
                                        }
                                        else
                                        {
                                            lenth = history.length();
                                        }

                                        for (int i=0; i<lenth; i++)
                                        {
                                            JSONObject historyData = history.getJSONObject(i);

                                            if (historyData!=null)
                                            {
                                                String Id="",UserId="",WalletId="",UpdatedDate="",Amount="",Type="",Description="",Status="";

                                                if (historyData.has("Id"))
                                                {
                                                    Id = historyData.getString("Id");
                                                }

                                                if (historyData.has("UserId"))
                                                {
                                                    UserId = historyData.getString("UserId");
                                                }

                                                if (historyData.has("WalletId"))
                                                {
                                                    WalletId = historyData.getString("WalletId");
                                                }

                                                if (historyData.has("UpdatedDate"))
                                                {
                                                    UpdatedDate = historyData.getString("UpdatedDate");
                                                }

                                                if (historyData.has("Amount"))
                                                {
                                                    Amount = historyData.getString("Amount");
                                                }

                                                if (historyData.has("Type"))
                                                {
                                                    Type = historyData.getString("Type");
                                                }

                                                if (historyData.has("Description"))
                                                {
                                                    Description = historyData.getString("Description");
                                                }

                                                if (historyData.has("Status"))
                                                {
                                                    Status = historyData.getString("Status");
                                                }

                                                list.add(new Transfer_History_Been(Id,UserId,WalletId,UpdatedDate,Amount,Type,Description,Status));
                                            }
                                        }
                                        dialogClass.hideDialog();
                                        adapter.notifyDataSetChanged();
                                    }
                                    else
                                    {
                                        Log.e("call","no cards available");
                                        dialogClass.hideDialog();
                                        adapter.notifyDataSetChanged();
                                    }
                                }
                                else
                                {
                                    Log.e("call","no cards found");
                                    dialogClass.hideDialog();
                                    adapter.notifyDataSetChanged();
                                }
                            }
                            else
                            {
                                Log.e("call","status false");
                                dialogClass.hideDialog();
                                adapter.notifyDataSetChanged();
                            }
                        }
                        else
                        {
                            Log.e("call","status not found");
                            dialogClass.hideDialog();
                            adapter.notifyDataSetChanged();
                        }

                    }
                    else
                    {
                        Log.e("call","json null");
                        dialogClass.hideDialog();
                        adapter.notifyDataSetChanged();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Exception","Exception "+e.toString());
                    dialogClass.hideDialog();
                    adapter.notifyDataSetChanged();
                }
            }

        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }
}
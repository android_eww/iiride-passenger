package iiride.app.rider.activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import iiride.app.rider.R;
import iiride.app.rider.application.TicktocApplication;
import iiride.app.rider.comman.Common;
import iiride.app.rider.comman.SessionSave;
import iiride.app.rider.comman.WebServiceAPI;
import iiride.app.rider.fragment.Wallet_Transfer_ReceiveMoney_Fragment;
import iiride.app.rider.fragment.Wallet_Transfer_SendMoney_Fragment;
import iiride.app.rider.other.DialogClass;
import iiride.app.rider.other.Global;
import iiride.app.rider.other.InternetDialog;

import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;


public class Wallet_Transfer_Activity extends AppCompatActivity implements View.OnClickListener
{
    public static Wallet_Transfer_Activity activity;

    private LinearLayout ll_Back;
    private ImageView iv_Back;
    private TextView tv_Title;
    private RelativeLayout rl_send_money, rl_receive_money, rl_TransferToBank;
    private LinearLayout ll_Send;
    private EditText et_Amount;
    private TextView tv_Send,tv_Dollar;
    public static String qr_code = "";
    private DialogClass dialogClass;
    private AQuery aQuery;


    FrameLayout frame_transfer;
    String clickName = "";
    String CLICK_SEND_MONEY="send_money", CLICK_RECEIVE_MONEY="receive_money";
    public static int resumeFlag = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_transfer);

        activity = Wallet_Transfer_Activity.this;
        qr_code = "";
        resumeFlag = 1;
        dialogClass = new DialogClass(activity,0);
        aQuery = new AQuery(activity);
        initUI();
    }

    private void initUI()
    {
        ll_Back = (LinearLayout) findViewById(R.id.back_layout);
        iv_Back = (ImageView) findViewById(R.id.back_imageview);

        tv_Title = (TextView) findViewById(R.id.title_textview);
        tv_Send = (TextView) findViewById(R.id.tv_Send);
        tv_Dollar = (TextView) findViewById(R.id.dollar);
        ll_Send = (LinearLayout) findViewById(R.id.amout_layout);
        et_Amount = (EditText) findViewById(R.id.et_Amount);

        tv_Title.setText("Transfer");

        rl_send_money = (RelativeLayout) findViewById(R.id.rl_send_money);
        rl_receive_money = (RelativeLayout) findViewById(R.id.rl_receive_money);
        rl_TransferToBank = (RelativeLayout) findViewById(R.id.rl_TransferToBank);


        frame_transfer = (FrameLayout) findViewById(R.id.frame_transfer);

        ll_Back.setOnClickListener(activity);
        iv_Back.setOnClickListener(activity);

        rl_send_money.setOnClickListener(activity);
        rl_receive_money.setOnClickListener(activity);
        rl_TransferToBank.setOnClickListener(activity);
        tv_Send.setOnClickListener(activity);

        et_Amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (charSequence.toString().trim().length()==1 && charSequence.toString().trim().equalsIgnoreCase("."))
                {
                    et_Amount.setText("");
                }
                else
                {
                    if (charSequence.length()<=0)
                    {
                        tv_Dollar.setVisibility(View.GONE);
                    }
                    else
                    {
                        tv_Dollar.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        setSendMoney();
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.back_layout:
                onBackPressed();
                break;

            case R.id.back_imageview:
                onBackPressed();
                break;

            case R.id.tv_Send:
                sendViaQrCode();
                break;

            case R.id.rl_send_money:
                qr_code = "";
                setSendMoney();
                break;

            case R.id.rl_receive_money:
                qr_code = "";
                setReceiveMoney();
                break;

            case R.id.rl_TransferToBank:
                qr_code = "";
                tranferToBank();
                break;
        }
    }

    public void sendViaQrCode()
    {
        if (Global.isNetworkconn(activity))
        {
            if (qr_code!=null && !qr_code.equalsIgnoreCase(""))
            {
                if (!et_Amount.getText().toString().trim().equalsIgnoreCase(""))
                {
                    callApi();
                }
                else
                {
                    InternetDialog internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog("Please enter amount!");
                }
            }
            else
            {
                InternetDialog internetDialog = new InternetDialog(activity);
                internetDialog.showDialog("Please try again QRCode not found!");
            }
        }
        else
        {
            InternetDialog internetDialog = new InternetDialog(activity);
            internetDialog.showDialog("Please check your internet connection!");
        }

    }

    public void callApi()
    {
        dialogClass.showDialog();
        String url = WebServiceAPI.API_SEND_MONEY;

        //QRCode,SenderId,Amount

        Map<String, Object> params = new HashMap<String, Object>();
        params.put(WebServiceAPI.PARAM_SENDER_ID, SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID,activity));
        params.put(WebServiceAPI.PARAM_AMOUNT,et_Amount.getText().toString().trim());
        params.put(WebServiceAPI.PARAM_QR_CODE,qr_code);

        Log.e("call", "url = " + url);
        Log.e("call", "param = " + params);

        aQuery.ajax(url.trim(), params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e("responseCode", " = " + responseCode);
                    Log.e("Response", " = " + json);

                    if (json!=null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                String message = "Money send successfully!";

                                if (json.has("message"))
                                {
                                    message = json.getString("message");
                                }
                                dialogClass.hideDialog();
                                et_Amount.setText("");
                                Wallet_Transfer_SendMoney_Fragment sendMoneyFragment = new Wallet_Transfer_SendMoney_Fragment();
                                sendMoneyFragment.reset();
                                InternetDialog internetDialog = new InternetDialog(activity);
                                internetDialog.showDialog(message);
                            }
                            else
                            {
                                String message = "Problem in sending money please try again later!";

                                if (json.has("message"))
                                {
                                    message = json.getString("message");
                                }
                                dialogClass.hideDialog();
                                InternetDialog internetDialog = new InternetDialog(activity);
                                internetDialog.showDialog(message);
                            }
                        }
                        else
                        {
                            String message = "Problem in sending money please try again later!";

                            if (json.has("message"))
                            {
                                message = json.getString("message");
                            }
                            dialogClass.hideDialog();
                            InternetDialog internetDialog = new InternetDialog(activity);
                            internetDialog.showDialog(message);
                        }
                    }
                    else
                    {
                        String message = "Problem in sending money please try again later!";
                        dialogClass.hideDialog();
                        InternetDialog internetDialog = new InternetDialog(activity);
                        internetDialog.showDialog(message);
                    }
                }
                catch (Exception e)
                {
                    Log.e("Exception","Exception "+e.toString());
                    dialogClass.hideDialog();
                }
            }

        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    public void setSendMoney()
    {
        ll_Send.setVisibility(View.VISIBLE);
        if (!clickName.equalsIgnoreCase(CLICK_SEND_MONEY))
        {
            Wallet_Transfer_SendMoney_Fragment fragment = new Wallet_Transfer_SendMoney_Fragment();
            FragmentTransaction transaction =getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
            transaction.replace(R.id.frame_transfer, fragment);
            transaction.addToBackStack(null);
            transaction.commitAllowingStateLoss();
        }
        clickName = CLICK_SEND_MONEY;
    }


    public void setReceiveMoney()
    {
        ll_Send.setVisibility(View.GONE);
        if (!clickName.equalsIgnoreCase(CLICK_RECEIVE_MONEY))
        {
            Wallet_Transfer_ReceiveMoney_Fragment fragment = new Wallet_Transfer_ReceiveMoney_Fragment();
            FragmentTransaction transaction =getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
            transaction.replace(R.id.frame_transfer, fragment);
            transaction.addToBackStack(null);
            transaction.commitAllowingStateLoss();
        }
        clickName = CLICK_RECEIVE_MONEY;
    }

    public void tranferToBank()
    {
        Intent intent = new Intent(activity, Wallet_Balance_TransferToBank_Activity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
    }


    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
    }

    @Override
    protected void onResume() {
        super.onResume();

        TicktocApplication.setCurrentActivity(activity);

    }

    @Override
    protected void onRestart()
    {
        super.onRestart();

        if (resumeFlag==1)
        {
            if (SessionSave.getUserSession(Common.IS_PASSCODE_REQUIRED,activity)!=null && SessionSave.getUserSession(Common.IS_PASSCODE_REQUIRED,activity).equalsIgnoreCase("1"))
            {
                resumeFlag = 0;

                if (Wallet__Activity.activity!=null)
                {
                    Wallet__Activity.activity.finish();
                }

                Intent intent = new Intent(activity,Create_Passcode_Activity.class);
                intent.putExtra("from","Wallet__Activity");
                startActivity(intent);
                finish();
            }
        }
        else
        {
            resumeFlag=1;
        }
    }
}
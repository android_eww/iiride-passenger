package iiride.app.rider.activity;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import iiride.app.rider.R;
import iiride.app.rider.application.TicktocApplication;
import iiride.app.rider.comman.Common;
import iiride.app.rider.comman.SessionSave;
import iiride.app.rider.comman.WebServiceAPI;
import iiride.app.rider.other.DialogClass;
import iiride.app.rider.other.Global;
import iiride.app.rider.other.InternetDialog;
import iiride.app.rider.view.MySnackBar;

import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;

public class ChangePasswordActivity extends AppCompatActivity implements View.OnClickListener{

    public static ChangePasswordActivity activity;
    private LinearLayout ll_Back;
    private ImageView iv_Back;
    private TextView tv_Title,tv_Submit;
    private EditText et_NewPassword,et_ConfirmPassword;//et_OldPassword,

    private DialogClass dialogClass;
    private AQuery aQuery;
    private MySnackBar mySnackBar;
    private LinearLayout ll_RootLayour;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        activity = ChangePasswordActivity.this;
        ll_RootLayour = (LinearLayout) findViewById(R.id.rootView);
        dialogClass = new DialogClass(activity,0);
        aQuery = new AQuery(activity);
        mySnackBar = new MySnackBar(activity);
        init();
    }




    private void init() {

        ll_Back = (LinearLayout) findViewById(R.id.back_layout);
        iv_Back = (ImageView) findViewById(R.id.back_imageview);
        tv_Title = (TextView) findViewById(R.id.title_textview);
        tv_Title.setText("Change Password");

//        et_OldPassword = (EditText) findViewById(R.id.input_old_password);
        et_NewPassword = (EditText) findViewById(R.id.input_new_password);
        et_ConfirmPassword = (EditText) findViewById(R.id.input_confirm_password);

        tv_Submit = (TextView) findViewById(R.id.submit_textview_change_password);

        ll_Back.setOnClickListener(activity);
        iv_Back.setOnClickListener(activity);

        tv_Submit.setOnClickListener(activity);

    }

    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.back_layout:
                onBackPressed();
                break;

            case R.id.back_imageview:
                onBackPressed();
                break;

            case R.id.submit_textview_change_password:
                if (Global.isNetworkconn(activity))
                {
                    changePassword();
                }
                else
                {
                    InternetDialog internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog("Please check your internet connection!");
                }
                break;
        }
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);
    }

    private void changePassword()
    {
        Log.e("call","new password "+et_NewPassword.getText().toString().trim());
        Log.e("call","confirm password "+et_ConfirmPassword.getText().toString().trim());

        if (et_NewPassword.getText().toString().trim().equalsIgnoreCase(""))
        {
            mySnackBar.showSnackBar(ll_RootLayour,"Please enter New Password!");
            et_NewPassword.setFocusableInTouchMode(true);
            et_NewPassword.requestFocus();
        }
        else if (et_ConfirmPassword.getText().toString().trim().equalsIgnoreCase(""))
        {
            mySnackBar.showSnackBar(ll_RootLayour,"Please enter Confirm Password!");
            et_ConfirmPassword.setFocusableInTouchMode(true);
            et_ConfirmPassword.requestFocus();
        }
        else if (et_NewPassword.getText().toString().length()<=5)
        {
            mySnackBar.showSnackBar(ll_RootLayour,"Password must be greater than 5 character!");
            et_NewPassword.setFocusableInTouchMode(true);
            et_NewPassword.requestFocus();
        }
        else if (et_ConfirmPassword.getText().toString().length()<=5)
        {
            mySnackBar.showSnackBar(ll_RootLayour,"Password must be greater than 5 character!");
            et_ConfirmPassword.setFocusableInTouchMode(true);
            et_ConfirmPassword.requestFocus();
        }
        else if (!et_NewPassword.getText().toString().equals(et_ConfirmPassword.getText().toString()))
        {
            mySnackBar.showSnackBar(ll_RootLayour,"Your New Password and Confirm Password not match!");
            et_ConfirmPassword.setFocusableInTouchMode(true);
            et_ConfirmPassword.requestFocus();
        }
        else
        {
            dialogClass.showDialog();
            String url = WebServiceAPI.API_CHANGE_PASSWORD;

            //http://54.206.55.185/web/Passenger_Api/ChangePassword
            Log.e("call","url = "+url);

            Map<String, Object> params = new HashMap<String, Object>();
            params.put(WebServiceAPI.PARAM_PASSENGER_ID, SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID,activity));
            params.put(WebServiceAPI.PARAM_PASSWORD,et_NewPassword.getText().toString());

            Log.e("call", "param = " + params);

            aQuery.ajax(url.trim(), params, JSONObject.class, new AjaxCallback<JSONObject>() {

                @Override
                public void callback(String url, JSONObject json, AjaxStatus status) {

                    try
                    {
                        int responseCode = status.getCode();
                        Log.e("responseCode", " = " + responseCode);
                        Log.e("Response", " = " + json);

                        if (json!=null)
                        {
                            if (json.has("status"))
                            {
                                if (json.has("message"))
                                {
                                    dialogClass.hideDialog();
                                    showSuccessMessage(json.getString("message"));
                                }
                                else
                                {
                                    dialogClass.hideDialog();
                                    showSuccessMessage("Please try again later!");
                                }
                            }
                            else
                            {
                                dialogClass.hideDialog();
                                showSuccessMessage("Please try again later!");
                            }
                        }
                        else
                        {
                            dialogClass.hideDialog();
                            showSuccessMessage("Please try again later!");
                        }
                    }
                    catch (Exception e)
                    {
                        Log.e("Exception","Exception "+e.toString());
                        dialogClass.hideDialog();
                    }
                }

            }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
        }
    }

    public void showSuccessMessage(String message)
    {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.my_dialog_class);

        TextView tv_Ok = (TextView) dialog.findViewById(R.id.Ok);
        TextView tv_Message = (TextView) dialog.findViewById(R.id.message);
        LinearLayout ll_Ok = (LinearLayout) dialog.findViewById(R.id.ok_layout);

        tv_Message.setText(message);

        ll_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                finish();
                overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);
            }
        });

        tv_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                finish();
                overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        dialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();

        TicktocApplication.setCurrentActivity(activity);

    }
}

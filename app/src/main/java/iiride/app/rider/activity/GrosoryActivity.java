package iiride.app.rider.activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import iiride.app.rider.R;
import iiride.app.rider.application.TicktocApplication;
import iiride.app.rider.other.Global;
import iiride.app.rider.other.InternetDialog;


public class GrosoryActivity extends AppCompatActivity implements View.OnClickListener{

    public static GrosoryActivity activity;
    private LinearLayout ll_Back;
    private ImageView iv_Back;
    private TextView tv_Title;
    private ImageView iv_Amazon,iv_Closes,iv_Woolworth,iv_Aldi,iv_Cosco,iv_IGA;
    private String title = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grosory);

        activity = GrosoryActivity.this;

        if (this.getIntent()!=null)
        {
            if (this.getIntent().getStringExtra("title")!=null)
            {
                title = this.getIntent().getStringExtra("title");
            }
        }
        initUI();
    }

    private void initUI() {
        ll_Back = (LinearLayout) findViewById(R.id.back_layout);
        iv_Back = (ImageView) findViewById(R.id.back_imageview);

        iv_Amazon = (ImageView) findViewById(R.id.image_amazon);
        iv_Closes = (ImageView) findViewById(R.id.image_closes);
        iv_Woolworth = (ImageView) findViewById(R.id.image_woolworth);
        iv_Aldi = (ImageView) findViewById(R.id.image_aldi);
        iv_Cosco = (ImageView) findViewById(R.id.image_cosco);
        iv_IGA = (ImageView) findViewById(R.id.image_IGA);

        tv_Title = (TextView) findViewById(R.id.title_textview);

        tv_Title.setText(title);

        ll_Back.setOnClickListener(activity);
        iv_Back.setOnClickListener(activity);

        iv_Amazon.setOnClickListener(activity);
        iv_Closes.setOnClickListener(activity);
        iv_Woolworth.setOnClickListener(activity);
        iv_Aldi.setOnClickListener(activity);
        iv_Cosco.setOnClickListener(activity);
        iv_IGA.setOnClickListener(activity);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId())
        {
            case R.id.back_layout:
                onBackPressed();
                break;

            case R.id.back_imageview:
                onBackPressed();
                break;

            case R.id.image_amazon:
                gotoWebViewScreen(1);
                break;

            case R.id.image_closes:
                gotoWebViewScreen(2);
                break;

            case R.id.image_woolworth:
                gotoWebViewScreen(3);
                break;

            case R.id.image_aldi:
                gotoWebViewScreen(4);
                break;

            case R.id.image_cosco:
                gotoWebViewScreen(5);
                break;

            case R.id.image_IGA:
                gotoWebViewScreen(6);
                break;
        }
    }

    public void gotoWebViewScreen(int flag)
    {
        if (Global.isNetworkconn(activity))
        {
            String url = "";
            String title = "";
            if (flag==1)
            {
                url = getResources().getString(R.string.url_amazon);
                title = "Amazon";
            }
            else if (flag==2)
            {
                url = getResources().getString(R.string.url_closes);
                title = "Coles";
            }
            else if (flag==3)
            {
                url = getResources().getString(R.string.url_woolworth);
                title = "Woolworths";
            }
            else if (flag==4)
            {
                url = getResources().getString(R.string.url_aldi);
                title = "Aldi";
            }
            else if (flag==5)
            {
                url = getResources().getString(R.string.url_cosco);
                title = "Costco Wholesale";
            }
            else if (flag==6)
            {
                url = getResources().getString(R.string.url_iga);
                title = "IGA";
            }
            Intent intent = new Intent(GrosoryActivity.this,WebViewActivity.class);
            intent.putExtra("url",url);
            intent.putExtra("title",title);
            startActivity(intent);
        }
        else
        {
            InternetDialog internetDialog = new InternetDialog(activity);
            internetDialog.showDialog("Please check your internet connection!");
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
    }

    @Override
    protected void onResume() {
        super.onResume();
        TicktocApplication.setCurrentActivity(activity);
    }
}
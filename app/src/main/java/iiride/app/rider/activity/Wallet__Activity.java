package iiride.app.rider.activity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import iiride.app.rider.R;
import iiride.app.rider.application.TicktocApplication;
import iiride.app.rider.been.CreditCard_List_Been;
import iiride.app.rider.comman.Common;
import iiride.app.rider.comman.SessionSave;
import iiride.app.rider.fragment.BpayFragment;
import iiride.app.rider.fragment.EntertianmentFragment;
import iiride.app.rider.fragment.TravelFragment;

import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;


public class Wallet__Activity extends AppCompatActivity implements View.OnClickListener
{
    public static Wallet__Activity activity;

    private LinearLayout  ll_transfer, ll_balance, ll_cards;
    private LinearLayout ll_Back;
    private ImageView iv_Back;
    private TextView tv_Title;
    private Intent intent;
    public static String walleteBallence = "0";
    private TextView tv_WalletBallence;

    private ImageView imageBack, iv_bpay, iv_travel, iv_entertainment;
    private TextView tv_bpay, tv_travel, tv_entertainment;

    private LinearLayout ll_bpay, ll_travel, ll_entertainment;
    private FrameLayout fl_frame;
    private Fragment bpayFragment, travelFragmetn, entertainmentFregment;
    private int tabPrivPossition = 0,tabCurrPosition = 0;
    private SharedPreferences permissionStatus;
    private boolean sentToSettings = false;
    private static final int PERMISSION_REQUEST_CODE = 1;
    public static int resumeFlag = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);

        activity = Wallet__Activity.this;
        walleteBallence = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_WALLET_BALLENCE,activity);
        permissionStatus = getSharedPreferences("permissionStatus",MODE_PRIVATE);
        resumeFlag = 1;

        takePermision(0);

        initUI();
    }

    public void takePermision(int flag)
    {
        if (ActivityCompat.checkSelfPermission(Wallet__Activity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
        {
            Log.e("call"," permision 1111111111");
            if (ActivityCompat.shouldShowRequestPermissionRationale(Wallet__Activity.this, Manifest.permission.CAMERA))
            {
                Log.e("call"," permision 222222222222");
                //Show Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(Wallet__Activity.this);
                builder.setTitle(getResources().getString(R.string.need_camera_permission));
                builder.setMessage(getResources().getString(R.string.this_app_need_camera_permission));
                builder.setPositiveButton(getResources().getString(R.string.grant), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Log.e("call"," permision 33333333");
                        ActivityCompat.requestPermissions(Wallet__Activity.this, new String[]{Manifest.permission.CAMERA}, PERMISSION_REQUEST_CODE);
                    }
                });
                builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Log.e("call"," permision 44444444444");
                    }
                });
                builder.show();
            }
            else
            {
                Log.e("call"," permision 5555555555");
                //just request the permission
                ActivityCompat.requestPermissions(Wallet__Activity.this, new String[]{Manifest.permission.CAMERA}, PERMISSION_REQUEST_CODE);
            }
            Log.e("call"," permision 6666666666666");
            SharedPreferences.Editor editor = permissionStatus.edit();
            editor.putBoolean(Manifest.permission.CAMERA,true);
            editor.commit();
        }
        else
        {
            if (flag==1)
            {
                Intent intent = new Intent(Wallet__Activity.this, Wallet_Transfer_Activity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults)
    {
        Log.e("call","onRequestPermissionResult call");
        switch (requestCode)
        {
            case PERMISSION_REQUEST_CODE:
                Log.e("call","onRequestPermissionResult call111");
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    Log.e("call","onRequestPermissionResult call 222");
                }
                else
                {
                    Log.e("call","onRequestPermissionsResult permission not granted");
                }
                break;
        }
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        Log.e("call","onRequestPermissionResult call 333");
        if (sentToSettings)
        {
            if (ActivityCompat.checkSelfPermission(Wallet__Activity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {

            }
        }
    }

    private void initUI() {
        ll_Back = (LinearLayout) findViewById(R.id.back_layout);
        iv_Back = (ImageView) findViewById(R.id.back_imageview);

        tv_Title = (TextView) findViewById(R.id.title_textview);
        tv_WalletBallence = (TextView) findViewById(R.id.wallet_ballence);

        tv_Title.setText("Wallet");
        tv_WalletBallence.setText("$"+walleteBallence);

        fl_frame = (FrameLayout) findViewById(R.id.fl_frame);

        ll_bpay = (LinearLayout) findViewById(R.id.ll_bpay);
        ll_travel = (LinearLayout) findViewById(R.id.ll_travel);
        ll_entertainment = (LinearLayout) findViewById(R.id.ll_entertainment);

        iv_bpay = (ImageView) findViewById(R.id.iv_bpay);
        iv_travel = (ImageView) findViewById(R.id.iv_travel);
        iv_entertainment = (ImageView) findViewById(R.id.iv_entertainment);

        tv_bpay = (TextView) findViewById(R.id.tv_bpay);
        tv_travel = (TextView) findViewById(R.id.tv_travel);
        tv_entertainment = (TextView) findViewById(R.id.tv_entertainment);

        ll_transfer = (LinearLayout) findViewById(R.id.ll_transfer);
        ll_balance = (LinearLayout) findViewById(R.id.ll_balance);
        ll_cards = (LinearLayout) findViewById(R.id.ll_cards);

        ll_Back.setOnClickListener(activity);
        iv_Back.setOnClickListener(activity);

        ll_transfer.setOnClickListener(activity);
        ll_balance.setOnClickListener(activity);
        ll_cards.setOnClickListener(activity);
        ll_bpay.setOnClickListener(activity);
        ll_travel.setOnClickListener(activity);
        ll_entertainment.setOnClickListener(activity);

        bpayFragment();

    }

    @Override
    public void onClick(View v) {
        switch(v.getId())
        {
            case R.id.back_layout:
                onBackPressed();
                break;

            case R.id.back_imageview:
                onBackPressed();
                break;

            case R.id.ll_balance:
                resumeFlag = 0;
                intent = new Intent(Wallet__Activity.this, Wallet_Balance_Activity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                break;

            case R.id.ll_transfer:
                resumeFlag = 0;
                takePermision(1);
                break;

            case R.id.ll_cards:
                resumeFlag = 0;
                if (checkCardList()==false)
                {
                    intent = new Intent(Wallet__Activity.this, Add_Card_In_List_Activity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                }
                else
                {
                    intent = new Intent(Wallet__Activity.this, Wallet_Add_Cards_Activity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                }
                break;

            case R.id.ll_bpay:

                tabPrivPossition = tabCurrPosition;
                tabCurrPosition = 0;

                bpayFragment();

                break;

            case R.id.ll_travel:

                tabPrivPossition = tabCurrPosition;
                tabCurrPosition = 1;
                travelFragmetn();

                break;

            case R.id.ll_entertainment:

                tabPrivPossition = tabCurrPosition;
                tabCurrPosition = 2;
                entertainmentFregment();

                break;
        }
    }

    public boolean checkCardList()
    {
        try
        {
            Log.e("call","111111111111111");
            String strCardList = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_CARD_LIST,activity);
            List<CreditCard_List_Been> cardListBeens = new ArrayList<CreditCard_List_Been>();

            if (strCardList!=null && !strCardList.equalsIgnoreCase(""))
            {
                Log.e("call","222222222222222");
                JSONObject json = new JSONObject(strCardList);

                if (json!=null)
                {
                    Log.e("call","33333333333333");
                    if (json.has("cards"))
                    {
                        Log.e("call","6666666666666666");
                        JSONArray cards = json.getJSONArray("cards");

                        if (cards!=null && cards.length()>0)
                        {
                            return true;
                        }
                        else
                        {
                            Log.e("call","no cards available");
                            return false;
                        }
                    }
                    else
                    {
                        Log.e("call","no cards found");
                        return false;
                    }
                }
                else
                {
                    Log.e("call","json null");
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        catch (Exception e)
        {
            Log.e("call","Exception in getting card list = "+e.getMessage());
            return false;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
    }

    private class MyPagerAdapter extends FragmentPagerAdapter {

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int pos) {
            switch(pos) {

                case 0: return new BpayFragment();
                case 1: return new EntertianmentFragment();
                case 2: return new TravelFragment();
            }
            return null;
        }

        @Override
        public int getCount() {
            return 3;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();


        TicktocApplication.setCurrentActivity(activity);
        walleteBallence = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_WALLET_BALLENCE,activity);
        tv_WalletBallence.setText("$"+walleteBallence);
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        if (resumeFlag==1)
        {
            if (SessionSave.getUserSession(Common.IS_PASSCODE_REQUIRED,activity)!=null && SessionSave.getUserSession(Common.IS_PASSCODE_REQUIRED,activity).equalsIgnoreCase("1"))
            {
                resumeFlag = 0;
                Intent intent = new Intent(Wallet__Activity.this,Create_Passcode_Activity.class);
                intent.putExtra("from","Wallet__Activity");
                startActivity(intent);
                finish();
            }
        }
        else
        {
            resumeFlag=1;
        }
    }

    private void bpayFragment()
    {
        btnClick(1);

        if (tabCurrPosition == tabPrivPossition)
        {

            bpayFragment = new BpayFragment();
            FragmentTransaction ftBpay = getSupportFragmentManager().beginTransaction();
            ftBpay.replace(R.id.fl_frame,bpayFragment);
            ftBpay.commit();
        }
        if (tabCurrPosition < tabPrivPossition)
        {
            bpayFragment = new BpayFragment();
            FragmentTransaction ftBpay = getSupportFragmentManager().beginTransaction();
            ftBpay.setCustomAnimations(R.anim.enter_from_left,R.anim.exit_to_right);
            ftBpay.replace(R.id.fl_frame,bpayFragment);
            ftBpay.commit();
        }
        if (tabCurrPosition > tabPrivPossition)
        {
            bpayFragment = new BpayFragment();
            FragmentTransaction ftBpay = getSupportFragmentManager().beginTransaction();
            ftBpay.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left);
            ftBpay.replace(R.id.fl_frame,bpayFragment);
            ftBpay.commit();
        }
    }

    private void travelFragmetn()
    {
        btnClick(2);

        if (tabCurrPosition == tabPrivPossition)
        {
            travelFragmetn = new TravelFragment();
            FragmentTransaction ftTravel = getSupportFragmentManager().beginTransaction();
            ftTravel.replace(R.id.fl_frame,travelFragmetn);
            ftTravel.commit();
        }
        if (tabCurrPosition < tabPrivPossition)
        {

            travelFragmetn = new TravelFragment();
            FragmentTransaction ftTravel = getSupportFragmentManager().beginTransaction();
            ftTravel.setCustomAnimations(R.anim.enter_from_left,R.anim.exit_to_right);
            ftTravel.replace(R.id.fl_frame,travelFragmetn);
            ftTravel.commit();
        }
        if (tabCurrPosition > tabPrivPossition)
        {

            travelFragmetn = new TravelFragment();
            FragmentTransaction ftTravel = getSupportFragmentManager().beginTransaction();
            ftTravel.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left);
            ftTravel.replace(R.id.fl_frame,travelFragmetn);
            ftTravel.commit();
        }
    }

    private void entertainmentFregment()
    {
        btnClick(3);

        if (tabCurrPosition == tabPrivPossition)
        {
            entertainmentFregment = new EntertianmentFragment();
            FragmentTransaction ftEntertainment = getSupportFragmentManager().beginTransaction();
            ftEntertainment.replace(R.id.fl_frame,entertainmentFregment);
            ftEntertainment.commit();
        }
        if (tabCurrPosition < tabPrivPossition)
        {
            entertainmentFregment = new EntertianmentFragment();
            FragmentTransaction ftEntertainment = getSupportFragmentManager().beginTransaction();
            ftEntertainment.setCustomAnimations(R.anim.enter_from_left,R.anim.exit_to_right);
            ftEntertainment.replace(R.id.fl_frame,entertainmentFregment);
            ftEntertainment.commit();
        }
        if (tabCurrPosition > tabPrivPossition)
        {
            entertainmentFregment = new EntertianmentFragment();
            FragmentTransaction ftEntertainment = getSupportFragmentManager().beginTransaction();
            ftEntertainment.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left);
            ftEntertainment.replace(R.id.fl_frame,entertainmentFregment);
            ftEntertainment.commit();
        }
    }

    private void btnClick(int i)
    {

        if (i == 1)
        {

            iv_bpay.setBackgroundResource(R.drawable.ic_dollar_select);
            iv_travel.setBackgroundResource(R.drawable.ic_travel_bag_unselect);
            iv_entertainment.setBackgroundResource(R.drawable.ic_movie_unselect);
            tv_bpay.setTextColor(getResources().getColor(R.color.colorSelectText));
            tv_travel.setTextColor(getResources().getColor(R.color.colorUnselectText));
            tv_entertainment.setTextColor(getResources().getColor(R.color.colorUnselectText));

        }
        else if(i == 2)
        {

            iv_bpay.setBackgroundResource(R.drawable.ic_dollar_unselect);
            iv_travel.setBackgroundResource(R.drawable.ic_travel_bag_select);
            iv_entertainment.setBackgroundResource(R.drawable.ic_movie_unselect);
            tv_bpay.setTextColor(getResources().getColor(R.color.colorUnselectText));
            tv_travel.setTextColor(getResources().getColor(R.color.colorSelectText));
            tv_entertainment.setTextColor(getResources().getColor(R.color.colorUnselectText));

        }
        else
        {

            iv_bpay.setBackgroundResource(R.drawable.ic_dollar_unselect);
            iv_travel.setBackgroundResource(R.drawable.ic_travel_bag_unselect);
            iv_entertainment.setBackgroundResource(R.drawable.ic_movie_select);
            tv_bpay.setTextColor(getResources().getColor(R.color.colorUnselectText));
            tv_travel.setTextColor(getResources().getColor(R.color.colorUnselectText));
            tv_entertainment.setTextColor(getResources().getColor(R.color.colorSelectText));
        }
    }

}

package iiride.app.rider.activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import iiride.app.rider.R;
import iiride.app.rider.comman.Common;
import iiride.app.rider.comman.Constants;
import iiride.app.rider.comman.SessionSave;
import iiride.app.rider.comman.WebServiceAPI;
import iiride.app.rider.notification.FCMUtil;
import iiride.app.rider.other.DialogClass;
import iiride.app.rider.other.GPSTracker;
import iiride.app.rider.other.Global;
import iiride.app.rider.other.InternetDialog;
import iiride.app.rider.other.MyAlertDialog;
import iiride.app.rider.view.MySnackBar;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoginActivity extends Activity implements View.OnClickListener{

    public static LoginActivity activity;
    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;
    public TextView textSignup,textviewLogin;
    private LinearLayout layoutLogin,ll_ForgotPassword;
    private EditText et_Email,et_Password;

    private DialogClass dialogClass;
    private AQuery aQuery;
    public static String userName="",password="",latitude="",longitude="",deviceType="2",token="";
    private static final int PERMISSION_REQUEST_CODE = 1;
    private GPSTracker gpsTracker;
    private LinearLayout ll_RootLayout;
    private MySnackBar mySnackBar;
    public static boolean permissionGranted = false;
    public static boolean locationFound = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        activity = LoginActivity.this;

        userName="";
        password="";
        latitude="";
        longitude="";
        deviceType="2";
        token="";

        permissionGranted = false;
        locationFound = false;

        dialogClass = new DialogClass(LoginActivity.this,0);
        aQuery = new AQuery(activity);
        gpsTracker = new GPSTracker(activity);

        ll_RootLayout = (LinearLayout) findViewById(R.id.rootView);
        mySnackBar = new MySnackBar(activity);

        generateToken();
        Log.e("call","######################### = "+ FCMUtil.getFcmToken(activity));
        checkGPS();
        init();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                token = FCMUtil.getFcmToken(activity);
                Log.e("call","totken token token = "+token);
            }
        },2000);
    }


    public void init()
    {
        layoutLogin = (LinearLayout)findViewById(R.id.layout_login);
        textSignup = (TextView)findViewById(R.id.textSignup);
        textviewLogin= (TextView)findViewById(R.id.textview_login);
        ll_ForgotPassword = (LinearLayout) findViewById(R.id.forgot_password_layout);

        et_Email = (EditText) findViewById(R.id.input_email);
        et_Password = (EditText) findViewById(R.id.input_pass);

        ll_ForgotPassword.setOnClickListener(activity);
        textviewLogin.setOnClickListener(activity);
        layoutLogin.setOnClickListener(activity);
        textSignup.setOnClickListener(activity);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.layout_login:
                checkEmailPassword();
                break;

            case R.id.forgot_password_layout:
                openForgotPasswordPopup();
                break;

            case R.id.textview_login:
                checkEmailPassword();
                break;

            case R.id.textSignup:
                Intent signup = new Intent(LoginActivity.this,SignUpActivity.class);
                startActivity(signup);
                overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
                break;
        }
    }

    public void openForgotPasswordPopup()
    {
        final Dialog dialog = new Dialog(LoginActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.forgot_password_dialog);

        TextView tv_Submit = (TextView) dialog.findViewById(R.id.submit_textview);
        TextView tv_Cancle = (TextView) dialog.findViewById(R.id.cancel_textview);
        final EditText et_Email = (EditText) dialog.findViewById(R.id.input_email);
        LinearLayout ll_Submit = (LinearLayout) dialog.findViewById(R.id.submit_layout);
        LinearLayout ll_Cancle = (LinearLayout) dialog.findViewById(R.id.cancel_layout);

        ll_Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (et_Email.getText()!=null && !et_Email.getText().toString().trim().equals(""))
                {
                    if (isValidEmail(et_Email.getText().toString().trim()))
                    {
                        dialog.dismiss();
                        callApiForgotPassword(et_Email.getText().toString().trim());
                    }
                    else
                    {
                        InternetDialog internetDialog = new InternetDialog(activity);
                        internetDialog.showDialog("Please enter valid Email!");
                    }
                }
                else
                {
                    InternetDialog internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog("Please enter email!");
                }
            }
        });

        tv_Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (et_Email.getText()!=null && !et_Email.getText().toString().trim().equals(""))
                {
                    if (isValidEmail(et_Email.getText().toString().trim()))
                    {
                        dialog.dismiss();
                        callApiForgotPassword(et_Email.getText().toString().trim());
                    }
                    else
                    {
                        InternetDialog internetDialog = new InternetDialog(activity);
                        internetDialog.showDialog("Please enter valid Email!");
                    }
                }
                else
                {
                    InternetDialog internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog("Please enter email!");
                }
            }
        });

        ll_Cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        tv_Cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        dialog.show();
    }

    public void callApiForgotPassword(String strEmail)
    {
        dialogClass.showDialog();
        String url = WebServiceAPI.API_FORGOT_PASSWORD;

        //Username,Password,DeviceType,Lat,Lng,Token

        Map<String, Object> params = new HashMap<String, Object>();
        params.put(WebServiceAPI.PARAM_EMAIL,strEmail);

        Log.e("call", "url = " + url);
        Log.e("call", "param = " + params);

        aQuery.ajax(url.trim(), params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e("responseCode", " = " + responseCode);
                    Log.e("Response", " = " + json);
                    dialogClass.hideDialog();

                    if (json!=null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                dialogClass.hideDialog();
                                if (json.has("message"))
                                {
                                    InternetDialog internetDialog = new InternetDialog(LoginActivity.this);
                                    internetDialog.showDialog(json.getString("message"));
                                }
                                else
                                {
                                    InternetDialog internetDialog = new InternetDialog(LoginActivity.this);
                                    internetDialog.showDialog("Something went wrong!");
                                }
                            }
                            else
                            {
                                if (json.has("message"))
                                {
                                    dialogClass.hideDialog();
                                    InternetDialog internetDialog = new InternetDialog(LoginActivity.this);
                                    internetDialog.showDialog(json.getString("message"));
                                }
                                else
                                {
                                    dialogClass.hideDialog();
                                    InternetDialog internetDialog = new InternetDialog(LoginActivity.this);
                                    internetDialog.showDialog("Something went wrong!");
                                }
                            }
                        }
                        else
                        {
                            dialogClass.hideDialog();
                            InternetDialog internetDialog = new InternetDialog(LoginActivity.this);
                            internetDialog.showDialog("Something went wrong!");
                        }
                    }
                    else
                    {
                        dialogClass.hideDialog();
                        InternetDialog internetDialog = new InternetDialog(LoginActivity.this);
                        internetDialog.showDialog("Something went wrong!");
                    }
                }
                catch (Exception e)
                {
                    Log.e("Exception","Exception "+e.toString());
                    dialogClass.hideDialog();
                }
            }

        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    public void checkEmailPassword()
    {
        Log.e("call","check email");

        if (et_Email.getText() != null && !et_Email.getText().toString().trim().equals(""))
        {
            Log.e("call","check email 111");
            if (et_Password.getText()!=null && !et_Password.getText().toString().trim().equals(""))
            {
                Log.e("call","check email 222");
                if (!isValidEmail(et_Email.getText().toString().trim()))
                {
                    mySnackBar.showSnackBar(ll_RootLayout,"Invalid Email!");
                    et_Email.setFocusableInTouchMode(true);
                    et_Email.requestFocus();
                }
                else if (et_Password.getText().toString().trim().length()<=5)
                {
                    mySnackBar.showSnackBar(ll_RootLayout,"Password must be greater than 5!");
                    et_Password.setFocusableInTouchMode(true);
                    et_Password.requestFocus();
                }
                else
                {
                    Log.e("flag","locationFound = "+locationFound);
                    Log.e("flag","permissionGranted = "+permissionGranted);
                    if (Global.isNetworkconn(activity))
                    {
                        Log.e("call","check email 333");
                        Log.e("call","check token token = "+token);
                        if (token!=null && !token.equalsIgnoreCase(""))
                        {
                            Log.e("call","check email 444");
                            login();
                        }
                        else
                        {
                            InternetDialog internetDialog = new InternetDialog(activity);
                            internetDialog.showDialog("Something went wrong please restart your application and login again");
                        }
//                        login();
                    }
                    else
                    {
                        InternetDialog internetDialog = new InternetDialog(activity);
                        internetDialog.showDialog("Please check your internet connection!");
                    }
                }
            }
            else
            {
                mySnackBar.showSnackBar(ll_RootLayout,"Please enter Password!");
                et_Password.setFocusableInTouchMode(true);
                et_Password.requestFocus();
            }
        }
        else
        {
            mySnackBar.showSnackBar(ll_RootLayout,"Please enter email!");
            et_Email.setFocusableInTouchMode(true);
            et_Email.requestFocus();
        }
    }

    // validating email id
    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public void checkGPS()
    {
        Log.e("call","1111111111111");
        if (checkPermission())
        {
            permissionGranted = true;
            Log.e("call","222222222222");
            gpsTracker = new GPSTracker(activity);
            if (gpsTracker.canGetLocation())
            {
                Log.e("call","3333333333333");
                latitude = gpsTracker.getLatitude()+"";
                longitude = gpsTracker.getLongitude()+"";
                if (latitude!=null && longitude!=null && !latitude.equalsIgnoreCase("") && !longitude.equalsIgnoreCase("") && !latitude.equalsIgnoreCase("0") && !longitude.equalsIgnoreCase("0"))
                {
                    locationFound = true;
                    Constants.newgpsLatitude = latitude+"";
                    Constants.newgpsLongitude = longitude+"";

                    Log.e("latitude", "" + Constants.newgpsLatitude);
                    Log.e("longitude", "" + Constants.newgpsLongitude);
                }
                else
                {
                    locationFound = false;
                }
            }
            else
            {
                locationFound = false;
                Log.e("call","44444444444444");
                AlertMessageNoGps();
            }
        }
        else
        {
            Log.e("call","5555555555555");
            permissionGranted = false;
            requestPermission();
        }
    }

    private boolean checkPermission() {

        int result = ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED){
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.ACCESS_FINE_LOCATION))
        {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
        }
        else
        {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults)
    {
        Log.e("call","onRequestPermissionsResult() 11 = "+requestCode);

        switch (requestCode)
        {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    permissionGranted = true;
                    Log.e("call","onRequestPermissionsResult() 22 = ");

                    gpsTracker = new GPSTracker(activity);
                    if (gpsTracker.canGetLocation())
                    {

                        Log.e("call","onRequestPermissionsResult() 33 = ");
                        latitude = gpsTracker.getLatitude()+"";
                        longitude = gpsTracker.getLongitude()+"";
                        if (latitude!=null && longitude!=null && !latitude.equalsIgnoreCase("") && !longitude.equalsIgnoreCase("") && !latitude.equalsIgnoreCase("0") && !longitude.equalsIgnoreCase("0"))
                        {
                            Constants.newgpsLatitude = gpsTracker.getLatitude() + "";
                            Constants.newgpsLongitude = gpsTracker.getLongitude() + "";
                            locationFound = true;
                        }
                        else
                        {
                            locationFound = false;
                        }
                    }
                    else
                    {
                        locationFound = false;
                        Log.e("call","onRequestPermissionsResult() 44 = ");
                        AlertMessageNoGps();
                    }
                }
                else
                {
                    permissionGranted = false;
                    Log.e("call","onRequestPermissionsResult() 55 = ");
                    MyAlertDialog dialog = new MyAlertDialog(LoginActivity.this);
                    dialog.setCancelable(false);
                    dialog.setAlertDialog(1, getResources().getString(R.string.permission_denied_for_location));
                }
                break;
        }
    }

    public void AlertMessageNoGps()
    {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(activity);
        builder.setCancelable(false);
        builder.setMessage(Html.fromHtml("<font color='#000000'>"+getResources().getString(R.string.new_gps_settings_message)+"</font>"));

        String positiveText = getResources().getString(R.string.new_gps_settings_positive_button);
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(intent);
                    }
                });

        String negativeText = getResources().getString(R.string.new_gps_settings_nagative_button);
        builder.setNegativeButton(negativeText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        final android.app.AlertDialog dialog = builder.create();

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface arg0) {
                dialog.getButton(android.app.AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(activity, R.color.colorPrimaryDark));
                dialog.getButton(android.app.AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(activity, R.color.colorPrimaryDark));
            }
        });

        dialog.show();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
        {
            permissionGranted = true;
            gpsTracker = new GPSTracker(activity);

            if (gpsTracker.canGetLocation())
            {
                Log.e("onPostResum","call");
                gpsTracker = new GPSTracker(activity);
                latitude = gpsTracker.getLatitude()+"";
                longitude = gpsTracker.getLongitude()+"";

                if (latitude!=null && longitude!=null && !latitude.equalsIgnoreCase("") && !longitude.equalsIgnoreCase("") && !latitude.equalsIgnoreCase("0") && !longitude.equalsIgnoreCase("0"))
                {
                    Constants.newgpsLatitude = latitude+"";
                    Constants.newgpsLongitude = longitude+"";
                    locationFound = true;
                }
                else
                {
                    locationFound = false;
                }
            }
            else
            {
                locationFound = false;
            }
        }
        else
        {
            permissionGranted = false;
        }
    }

    public void login()
    {
        dialogClass.showDialog();
        String url = WebServiceAPI.API_LOGIN;

        //Username,Password,DeviceType,Lat,Lng,Token

        gpsTracker = new GPSTracker(LoginActivity.this);
        latitude = gpsTracker.getLatitude()+"";
        longitude = gpsTracker.getLongitude()+"";

        Map<String, Object> params = new HashMap<String, Object>();
        params.put(WebServiceAPI.PARAM_USER_NAME,et_Email.getText().toString());
        params.put(WebServiceAPI.PARAM_PASSWORD,et_Password.getText().toString());
        params.put(WebServiceAPI.PARAM_DEVICE_TYPE,"2");
        params.put(WebServiceAPI.PARAM_LAT,latitude);
        params.put(WebServiceAPI.PARAM_LONG,longitude);
        params.put(WebServiceAPI.PARAM_TOKEN,token);

        Log.e("call", "url = " + url);
        Log.e("call", "param = " + params);

        aQuery.ajax(url.trim(), params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e("responseCode", " = " + responseCode);
                    Log.e("Response", " = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                if (json.has("walletBalance"))
                                {
                                    String walletBallence = json.getString("walletBalance");

                                    if (walletBallence!=null && !walletBallence.equalsIgnoreCase(""))
                                    {
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_WALLET_BALLENCE,walletBallence,activity);
                                    }
                                    else
                                    {
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_WALLET_BALLENCE,"0",activity);
                                    }
                                }
                                else
                                {
                                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_WALLET_BALLENCE,"0",activity);
                                }

                                if (json.has("profile"))
                                {
                                    JSONObject profile = json.getJSONObject("profile");

                                    if (profile!=null)
                                    {
                                        String Fullname="",Email="",Password="",MobileNo="",Gender="",Image="",DeviceType="",Token="";
                                        String Lat="",Lng="",Status="",CreatedDate="",Id="",Address="",QRCode="",Verify="";
                                        String CompanyName="",ABN="",LicenceImage="",referalCode="",referalTotal="",BankName="",BSB="",BankAccountNo="",Description="";

                                        if (profile.has("Id"))
                                        {
                                            Id = profile.getString("Id");
                                        }

                                        if (profile.has("Fullname"))
                                        {
                                            Fullname = profile.getString("Fullname");
                                        }

                                        if (profile.has("Email"))
                                        {
                                            Email = profile.getString("Email");
                                        }

                                        if (profile.has("Password"))
                                        {
                                            Password = profile.getString("Password");
                                        }

                                        if (profile.has("MobileNo"))
                                        {
                                            MobileNo = "+"+profile.getString("MobileNo");
                                        }

                                        if (profile.has("Image"))
                                        {
                                            Image = profile.getString("Image");
                                        }

                                        if (profile.has("QRCode"))
                                        {
                                            QRCode = profile.getString("QRCode");
                                        }

                                        if (profile.has("CompanyName"))
                                        {
                                            CompanyName = profile.getString("CompanyName");
                                        }

                                        if (profile.has("ABN"))
                                        {
                                            ABN = profile.getString("ABN");
                                        }

                                        if (profile.has("BankName"))
                                        {
                                            BankName = profile.getString("BankName");
                                        }

                                        if (profile.has("BSB"))
                                        {
                                            BSB = profile.getString("BSB");
                                        }

                                        if (profile.has("BankAccountNo"))
                                        {
                                            BankAccountNo = profile.getString("BankAccountNo");
                                        }

                                        if (profile.has("Description"))
                                        {
                                            Description = profile.getString("Description");
                                        }

                                        if (profile.has("LicenceImage"))
                                        {
                                            LicenceImage = profile.getString("LicenceImage");
                                        }

                                        if (profile.has("Gender"))
                                        {
                                            Gender = profile.getString("Gender");
                                        }

                                        if (profile.has("ReferralCode"))
                                        {
                                            referalCode = profile.getString("ReferralCode");
                                        }

                                        if (profile.has("Address"))
                                        {
                                            Address = profile.getString("Address");
                                        }

                                        if (profile.has("DeviceType"))
                                        {
                                            DeviceType = profile.getString("DeviceType");
                                        }

                                        if (profile.has("Token"))
                                        {
                                            Token = profile.getString("Token");
                                        }

                                        if (profile.has("Lat"))
                                        {
                                            Lat = profile.getString("Lat");
                                        }

                                        if (profile.has("Lng"))
                                        {
                                            Lng = profile.getString("Lng");
                                        }

                                        if (profile.has("Status"))
                                        {
                                            Status = profile.getString("Status");
                                        }

                                        if (profile.has("CreatedDate"))
                                        {
                                            CreatedDate = profile.getString("CreatedDate");
                                        }

                                        if (profile.has("Verify"))
                                        {
                                            Verify = profile.getString("Verify");
                                        }

                                        if (profile.has("ReferralAmount"))
                                        {
                                            referalTotal = profile.getString("ReferralAmount");
                                        }

                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_ID,Id,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_FULL_NAME,Fullname,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_EMAIL,Email,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_PASSWORD,Password,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_PHONE_NUMBER,MobileNo,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_GENDER,Gender,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_IMAGE,Image,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_DEVICE_TYPE,DeviceType,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TOKEN,Token,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_LATITUDE,Lat,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_LONGITUDE,Lng,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_STATUS,Status,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_CREATED_DATE,CreatedDate,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_ADDRESS,Address,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_QR_CODE,QRCode,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_VERIFY_USER,Verify,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TICK_PAY_SPLASH,"0",activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_COMPANY_NAME,CompanyName,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_ABN,ABN,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_LICENCE_IMAGE,LicenceImage,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_REFERAL_CODE,referalCode,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_REFERAL_TOTAL,referalTotal,activity);
                                        SessionSave.saveUserSession(Common.CREATED_PASSCODE,"",activity);
                                        SessionSave.saveUserSession(Common.IS_PASSCODE_REQUIRED,"0",activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BANK_NAME,BankName,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BSB,BSB,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BANK_ACCOUNT_NUMBER,BankAccountNo,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_DESCRIPTION,Description,activity);

                                    }
                                    else
                                    {
                                        Log.e("errorrr","profile null");
                                    }

                                    if (json.has("car_class"))
                                    {
                                        JSONArray car_class = json.getJSONArray("car_class");

                                        for (int i=0; i<car_class.length(); i++)
                                        {
                                            Log.e("call","car object = "+car_class.getJSONObject(i).toString());
                                        }

                                        if (car_class!=null && car_class.length()>0)
                                        {
                                            Log.e("call","car_class.size = "+car_class.length());
                                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_CAR_CLASS,car_class.toString(),activity);
                                            dialogClass.hideDialog();
                                            Intent intent = new Intent(activity, MainActivity.class);
                                            startActivity(intent);
                                            finish();
                                        }
                                        else
                                        {
                                            Log.e("call","car_class null or lenth 0");
                                            dialogClass.hideDialog();
                                            Intent intent = new Intent(activity, MainActivity.class);
                                            startActivity(intent);
                                            finish();
                                        }
                                    }
                                    else
                                    {
                                        dialogClass.hideDialog();
                                        Intent intent = new Intent(activity, MainActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                }
                                else
                                {
                                    Log.e("errorrr","profile not available");
                                    dialogClass.hideDialog();
                                }
                            }
                            else
                            {
                                Log.e("errorrr","status false");
                                dialogClass.hideDialog();
                                if (json.has("message"))
                                {
                                    mySnackBar.showSnackBar(ll_RootLayout,json.getString("message"));
                                }
                                else
                                {
                                    mySnackBar.showSnackBar(ll_RootLayout,"Please try again later!");
                                }
                            }
                        }
                        else
                        {
                            Log.e("errorrr","status not available");
                            dialogClass.hideDialog();
                            mySnackBar.showSnackBar(ll_RootLayout,"Please try again later!");
                        }
                    }
                    else
                    {
                        Log.e("errorrr","json null ");
                        dialogClass.hideDialog();
                        mySnackBar.showSnackBar(ll_RootLayout,"Please try again later!");
                    }
                }
                catch (Exception e)
                {
                    Log.e("Exception","Exception "+e.toString());
                    dialogClass.hideDialog();
                    mySnackBar.showSnackBar(ll_RootLayout,"Please try again later!");
                }
            }

        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }


    private void generateToken(){
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.e("TAG", "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();

                        // Log and toast
                        Log.e("TAG", "token = "+token);

                        SharedPreferences pref = getSharedPreferences("pref",MODE_PRIVATE);

                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("token",token);
                        editor.commit();

                        FCMUtil.saveToken(activity, token);

                    }
                });
    }
}

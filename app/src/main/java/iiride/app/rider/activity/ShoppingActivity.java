package iiride.app.rider.activity;

import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import iiride.app.rider.R;
import iiride.app.rider.adapter.ShoppingCenterAdapter;
import iiride.app.rider.application.TicktocApplication;
import iiride.app.rider.been.ShoppingCenter_Been;
import iiride.app.rider.comman.Constants;
import iiride.app.rider.comman.Utility;
import iiride.app.rider.other.DialogClass;
import iiride.app.rider.other.GPSTracker;
import iiride.app.rider.other.Global;
import iiride.app.rider.other.HPLinearLayoutManager;
import iiride.app.rider.other.InternetDialog;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

public class ShoppingActivity extends AppCompatActivity implements View.OnClickListener{

    public static ShoppingActivity activity;
    private LinearLayout ll_Back;
    private ImageView iv_Back;
    private TextView tv_Title;

    private RecyclerView recyclerView;
    private ShoppingCenterAdapter adapter;
    private ArrayList<ShoppingCenter_Been> arrayList;
    private HPLinearLayoutManager layoutManager;

    double latitude = 0;
    double longitude = 0;
    private GPSTracker gpsTracker;
    private DialogClass dialogClass;
    private ParseData parseData;
    private String jsonUrl = "https://maps.googleapis.com/maps/api/place/textsearch/json?query=";
    private String strCity = "";
    private String key = "&key="+ Constants.GOOGLE_API_KEY;
    private String query = "";
    public static String strQuery = "";
    private String title = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping);

        activity = ShoppingActivity.this;
        gpsTracker = new GPSTracker(activity);
        dialogClass = new DialogClass(activity,0);
        query = "";
        title = "";
        if (this.getIntent()!=null)
        {
            if (this.getIntent().getStringExtra("query")!=null)
            {
                strQuery = this.getIntent().getStringExtra("query");
                query = this.getIntent().getStringExtra("query")+"+in+";
            }

            if (this.getIntent().getStringExtra("title")!=null)
            {
                title = this.getIntent().getStringExtra("title");
            }
        }

        Log.e("call","query in activity = "+query);
        getAddressFromLatLong();
        init();
    }



    public void getAddressFromLatLong()
    {
        try
        {
            gpsTracker = new GPSTracker(activity);

            if (gpsTracker!=null && gpsTracker.canGetLocation())
            {
                Geocoder geocoder;
                List<Address> addresses;
                geocoder = new Geocoder(this, Locale.getDefault());

                double latitude = gpsTracker.getLatitude();
                double longitude = gpsTracker.getLongitude();

                if (latitude!=0 && longitude!=0)
                {
                    addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

                    String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    String city = addresses.get(0).getLocality();
                    String state = addresses.get(0).getAdminArea();
                    String country = addresses.get(0).getCountryName();
                    String postalCode = addresses.get(0).getPostalCode();
                    String knownName = addresses.get(0).getFeatureName();

                    strCity = city;
                    Log.e("111111111111","address = "+address);
                    Log.e("111111111111","city = "+city);
                    Log.e("111111111111","state = "+state);
                    Log.e("111111111111","country = "+country);
                    Log.e("111111111111","postalCode = "+postalCode);
                    Log.e("111111111111","knownName = "+knownName);
                }
            }
        }
        catch (Exception e)
        {
            Log.e("call","Exception = "+e.getMessage());
        }
    }


    private void init() {

        ll_Back = (LinearLayout) findViewById(R.id.back_layout);
        iv_Back = (ImageView) findViewById(R.id.back_imageview);

        tv_Title = (TextView) findViewById(R.id.title_textview);

        tv_Title.setText(title);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        // Layout Managers:
        layoutManager = new HPLinearLayoutManager(activity);
        layoutManager.setOrientation(HPLinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        arrayList = new ArrayList<>();

        adapter = new ShoppingCenterAdapter(arrayList,activity);
        recyclerView.setAdapter(adapter);

        ll_Back.setOnClickListener(activity);
        iv_Back.setOnClickListener(activity);

        if (gpsTracker.canGetLocation())
        {
            latitude = gpsTracker.getLatitude();
            longitude = gpsTracker.getLongitude();

            jsonUrl = jsonUrl + query + strCity + key;

            Log.e("call","jsonUrl = "+jsonUrl);
            if (Global.isNetworkconn(activity))
            {
                dialogClass.showDialog();
                parseData = new ParseData(jsonUrl);
                parseData.execute();
            }
            else
            {
                InternetDialog internetDialog = new InternetDialog(activity);
                internetDialog.showDialog("Please check your internet connection!");
            }
        }
        else
        {
            InternetDialog internetDialog = new InternetDialog(activity);
            internetDialog.showDialog("Please first on your gps location.");
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.back_layout:
                onBackPressed();
                break;

            case R.id.back_imageview:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);
    }

    @Override
    protected void onResume() {
        super.onResume();

        TicktocApplication.setCurrentActivity(activity);

    }

    // Get the google API result and convert into JSON format.
    private JSONObject GetRestaurant(String Url) {
        try
        {
            HttpClient httpclient = new DefaultHttpClient();
            HttpGet httppost = new HttpGet(Url);
            HttpResponse response = httpclient.execute(httppost);
            String jsonResult = Utility.inputStreamToString(response.getEntity().getContent()).toString();
            JSONObject json = new JSONObject(jsonResult);
            return json;
        }
        catch (Exception e)
        {
            // TODO: handle exception
        }
        return null;
    }

    // Parse the google API result and loaded into list
    public class ParseData extends AsyncTask<Void, Integer, JSONObject>
    {
        private String url;
        public ParseData(String url)
        {
            this.url=url;
            arrayList.clear();
        }

        @Override
        protected JSONObject doInBackground(Void... params) {
            // TODO Auto-generated method stub
            JSONObject json = null;
            try
            {
                Log.e("call","doInBackground ParseData = "+url);

                json = GetRestaurant(url);
            }
            catch (Exception e)
            {
                json = null;
                Log.e("call","Exception = "+e.getMessage());
            }
            return json;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject)
        {
            try
            {
                Log.e("call","response = "+jsonObject);

                if (jsonObject!=null)
                {
                    if (jsonObject.has("status"))
                    {
                        if (jsonObject.getString("status").equalsIgnoreCase("OK"))
                        {
                            JSONArray results = jsonObject.getJSONArray("results");

                            if (results!=null && results.length()>0)
                            {
                                for (int i=0; i<results.length(); i++)
                                {
                                    String place_id="",name="",address="",contact="",distance="",image="",ratting="";
                                    double source_latitude=0,source_longitude=0;
                                    boolean isOpen = false;

                                    JSONObject data = results.getJSONObject(i);

                                    if (data!=null)
                                    {
                                        if (data.has("geometry"))
                                        {
                                            JSONObject geometry = data.getJSONObject("geometry");

                                            if (geometry!=null)
                                            {
                                                if (geometry.has("location"))
                                                {
                                                    JSONObject location = geometry.getJSONObject("location");

                                                    if (location!=null)
                                                    {
                                                        if (location.has("lat"))
                                                        {
                                                            source_latitude = location.getDouble("lat");
                                                        }

                                                        if (location.has("lng"))
                                                        {
                                                            source_longitude = location.getDouble("lng");
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        distance = getDistanceFromLatLonInKm(latitude,longitude,source_latitude,source_longitude);

                                        if (data.has("opening_hours"))
                                        {
                                            JSONObject opening_hours = data.getJSONObject("opening_hours");

                                            if (opening_hours!=null)
                                            {
                                                if (opening_hours.has("open_now"))
                                                {
                                                    isOpen = opening_hours.getBoolean("open_now");
                                                }
                                            }
                                        }

                                        if (data.has("place_id"))
                                        {
                                            place_id = data.getString("place_id");
                                        }

                                        if (data.has("name"))
                                        {
                                            name = data.getString("name");
                                        }

                                        if (data.has("formatted_address"))
                                        {
                                            address = data.getString("formatted_address");
                                        }

                                        if (data.has("icon"))
                                        {
                                            image = data.getString("icon");
                                        }

                                        if (data.has("rating"))
                                        {
                                            ratting = data.getString("rating");
                                        }

                                        arrayList.add(new ShoppingCenter_Been(place_id,name,address,contact,distance
                                                ,image,ratting,latitude,longitude,isOpen, source_latitude, source_longitude));
                                    }
                                    else
                                    {
                                        Log.e("call","data null at position = "+i);
                                    }
                                }

                                Log.e("call","arrayList.size() = "+arrayList.size());

                                dialogClass.hideDialog();

                                if (arrayList.size()>0)
                                {
                                    Collections.sort(arrayList, new Comparator<ShoppingCenter_Been>(){
                                        public int compare(ShoppingCenter_Been obj1, ShoppingCenter_Been obj2) {
                                            return obj1.getDistance().compareToIgnoreCase(obj2.getDistance());
                                        }
                                    });

                                    adapter.notifyDataSetChanged();
                                    Log.e("call","success fully set data");
                                }
                            }
                            else
                            {
                                dialogClass.hideDialog();
                                Log.e("call","0 result found");
                            }
                        }
                        else
                        {
                            dialogClass.hideDialog();
                            Log.e("call","status not ok");
                        }
                    }
                    else
                    {
                        dialogClass.hideDialog();
                        Log.e("call","status not found");
                    }
                }
                else
                {
                    dialogClass.hideDialog();
                    Log.e("call","jsonObject null");
                }
            }
            catch (Exception e)
            {
                dialogClass.hideDialog();
                Log.e("call","onPost exception = "+e.getMessage());
            }
        }
    }

    private String getDistanceFromLatLonInKm(double lat1, double lon1, double lat2, double lon2) {

        double R = 6371; // Radius of the earth in km
        double dLat = deg2rad(lat2-lat1);  // deg2rad below
        double dLon = deg2rad(lon2-lon1);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *  Math.sin(dLon/2) * Math.sin(dLon/2);

        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double d = R * c; // Distance in km
        String getDistance;

        /*if ( d > 1)
        {

            Log.d("MainActivity", "DistanceKm:- " + d );
            getDistance = String.format("%.1f",d) + " km";
        }
        else
        {
            d = (int) (d*1000);
            Log.d("MainActivity", "DistanceMeter:- " + d );
            getDistance = (int) d + " m";
        }*/
        getDistance = String.format("%.1f",d) + " km";

        return getDistance;

    }

    private double deg2rad(double deg) {
        return deg * (Math.PI/180);
    }
}

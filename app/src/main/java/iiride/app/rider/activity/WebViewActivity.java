package iiride.app.rider.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;


import iiride.app.rider.R;
import iiride.app.rider.application.TicktocApplication;
import iiride.app.rider.comman.Common;
import iiride.app.rider.comman.SessionSave;


public class WebViewActivity extends AppCompatActivity implements OnClickListener{
	
	private WebView webview;
	private String value;

	private LinearLayout ll_Back;
	private ImageView iv_Back;
	private TextView tvTitle;
	private ProgressBar progress;
	public static WebViewActivity activity;
	private String title = "Detail";

	public static int resumeFlag = 1;
	public static String from = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_webview);

		activity = WebViewActivity.this;

		ll_Back = (LinearLayout) findViewById(R.id.back_layout);
		iv_Back = (ImageView) findViewById(R.id.back_imageview);

		tvTitle= (TextView) findViewById(R.id.title_textview);

		tvTitle.setText("Groceries");


		Intent intent = getIntent();
		value  = intent.getExtras().getString("url");
		title = intent.getExtras().getString("title");
//		title = intent.getExtras().getString("title");

		tvTitle.setText(title.toString().trim());

		ll_Back.setOnClickListener(this);
		iv_Back.setOnClickListener(this);

		progress = (ProgressBar) findViewById(R.id.progress_bar);
		webview = (WebView) findViewById(R.id.webView1);
		WebSettings webSettings = webview.getSettings();
		webSettings.setBuiltInZoomControls(false);
		webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
		webSettings.setJavaScriptEnabled(true);
		webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
		from = "";

		if (this.getIntent()!=null)
		{
			if (this.getIntent().getStringExtra("from")!=null)
			{
				from = this.getIntent().getStringExtra("from");
			}
			else
			{
				from = "";
			}
		}
		else
		{
			from = "";
		}

		if (from!=null && from.equalsIgnoreCase("wallet"))
		{
			resumeFlag = 1;
		}
		else
		{
			resumeFlag = 0;
		}

		if(value.contains("http://"))
		{
			webview.loadUrl(value);
		}
		else if(value.contains("https://"))
		{
			webview.loadUrl(value);
		}

		webview.setWebViewClient(new myWebClient());

		webview.setWebChromeClient(new WebChromeClient()
		{
			public void onProgressChanged(WebView view, int progressInt)
			{
				if (progressInt < 80 && progress.getVisibility() == ProgressBar.GONE)
				{
					progress.setVisibility(ProgressBar.VISIBLE);
				}

				if (progressInt >= 80)
				{
					progress.setVisibility(ProgressBar.GONE);
				}
			}
		});

	}

	@Override
	protected void onResume() {
		super.onResume();
		TicktocApplication.setCurrentActivity(activity);
	}

	public class myWebClient extends WebViewClient
	{
		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
// TODO Auto-generated method stub
			super.onPageStarted(view, url, favicon);
		}

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
// TODO Auto-generated method stub
			progress.setVisibility(View.VISIBLE);
			view.loadUrl(url);
			return true;

		}

		@Override
		public void onPageFinished(WebView view, String url) {
// TODO Auto-generated method stub
			super.onPageFinished(view, url);
			progress.setVisibility(View.GONE);
		}
	}


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId())
		{
			case R.id.back_layout:
				onBackPressed();
				break;

			case R.id.back_imageview:
				onBackPressed();
				break;

			default:
				break;
		}
	}
	

	@Override
	public void onBackPressed() {
		if (webview != null && webview.canGoBack()) {
			webview.goBack();
		}else {
			super.onBackPressed();
			finish();
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (event.getAction() == KeyEvent.ACTION_DOWN)
		{
			switch (keyCode)
			{
				case KeyEvent.KEYCODE_BACK:
					if (webview.canGoBack())
					{
						webview.goBack();
					}
					else
					{
						finish();
					}
					return true;
			}

		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	protected void onRestart() {
		super.onRestart();

		if (resumeFlag==1)
		{
			if (SessionSave.getUserSession(Common.IS_PASSCODE_REQUIRED,activity)!=null && SessionSave.getUserSession(Common.IS_PASSCODE_REQUIRED,activity).equalsIgnoreCase("1"))
			{
				resumeFlag = 0;
				if (Wallet__Activity.activity != null)
				{
					Wallet__Activity.activity.finish();
				}
				Intent intent = new Intent(WebViewActivity.this,Create_Passcode_Activity.class);
				intent.putExtra("from","Wallet__Activity");
				startActivity(intent);
				finish();
			}
		}
		else
		{
			if (from!=null && from.equalsIgnoreCase("wallet"))
			{
				resumeFlag = 1;
			}
			else
			{
				resumeFlag = 0;
			}
		}
	}
}

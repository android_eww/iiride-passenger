package iiride.app.rider.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.IsoDep;
import android.os.Handler;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.github.devnied.emvnfccard.model.EmvCard;
import com.github.devnied.emvnfccard.parser.EmvParser;
import com.github.devnied.emvnfccard.utils.AtrUtils;

import iiride.app.rider.R;
import iiride.app.rider.application.TicktocApplication;
import iiride.app.rider.comman.Common;
import iiride.app.rider.comman.SessionSave;
import iiride.app.rider.comman.WebServiceAPI;
import iiride.app.rider.nfc.IContentActivity;
import iiride.app.rider.nfc.IRefreshable;
import iiride.app.rider.nfc.NFCUtils;
import iiride.app.rider.nfc.Provider;
import iiride.app.rider.nfc.SimpleAsyncTask;
import iiride.app.rider.other.DialogClass;
import iiride.app.rider.other.Global;
import iiride.app.rider.other.InternetDialog;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import fr.devnied.bitlib.BytesUtils;
import io.card.payment.CardIOActivity;
import io.card.payment.CardType;
import io.card.payment.CreditCard;

public class TickPayActivity extends AppCompatActivity implements View.OnClickListener, IContentActivity {

    public static TickPayActivity activity;
    private LinearLayout ll_Back;
    private ImageView iv_Back;
    private TextView tv_Title;
    private EditText et_card_number,et_cvv,et_Alias;
    private TextView ed_expire_date;
    private String expiryYear = "";
    private String expiryMonth = "";
    private String expiryDate = "";

    private TextView tv_pay_now;
    private LinearLayout scanCard;

    private NFCUtils mNfcUtils;
    private ProgressDialog mDialog;
    private AlertDialog mAlertDialog;
    private Provider mProvider = new Provider();
    private EmvCard mReadCard;
    private WeakReference<IRefreshable> mRefreshableContent;
    private byte[] lastAts;

    private DialogClass dialogClass;
    private AQuery aQuery;
    private EditText et_Name,et_Abn,et_Amount;
    boolean validornot = false;

    private static final int REQUEST_SCAN = 100;
    private static final int REQUEST_AUTOTEST =  200;

    ///for Bottom Dialog
    private BottomSheetDialog mBottomSheetDialog;
    private NumberPicker numberPickerMonth,numberPickerYear;
    private String[] valueMonth ;
    private String[] valueYear ;
    private String strExpiry = "";
    int MonthInNumber, YearInNumber ;
    public static String payId = "";
    public static double rate = 0;
    public static double transactionRate = 0;
    private String strRate = "% Service fee";
    private String strTransactionRate = " Transaction fee";
    private TextView tv_Rate;

    private String[] monthName = {
            "Jan", "Feb", "Mar",
            "Apr", "May", "Jun",
            "Jul", "Aug", "Sep",
            "Oct", "Nov", "Dec"
    };

    private String companyName = "", Abn="";

    private ImageView iv_close;
    private RelativeLayout rl_gif;
    private TextView tv_Dollar,tv_FinalTotal;

    public static int resumeFlag = 1;
    public static boolean isScan = false;
    private String cardNumberParam = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tick_pay);

        activity = TickPayActivity.this;
        mNfcUtils = new NFCUtils(this);
        expiryYear = "";
        expiryMonth = "";
        expiryDate = "";
        payId = "";
        rate = 0;
        transactionRate = 0;
        resumeFlag = 1;
        isScan = false;
        cardNumberParam = "";
        dialogClass = new DialogClass(activity,0);
        aQuery = new AQuery(activity);

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_COMPANY_NAME,activity)!=null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_COMPANY_NAME,activity).equalsIgnoreCase(""))
        {
            companyName = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_COMPANY_NAME,activity);
        }

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ABN,activity)!=null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ABN,activity).equalsIgnoreCase(""))
        {
            Abn = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ABN,activity);
        }

        init();
    }

    private void init() {

        ll_Back = (LinearLayout) findViewById(R.id.back_layout);
        iv_Back = (ImageView) findViewById(R.id.back_imageview);

        scanCard = (LinearLayout) findViewById(R.id.scanCardLayout);

        tv_Title = (TextView) findViewById(R.id.title_textview);
        tv_Rate = (TextView) findViewById(R.id.tv_rate);
        tv_FinalTotal = (TextView) findViewById(R.id.tv_final_total);
        et_Name = (EditText)findViewById(R.id.et_name);
        et_Abn = (EditText)findViewById(R.id.et_abn);
        et_Amount = (EditText)findViewById(R.id.et_amount);

        et_card_number = (EditText)findViewById(R.id.et_card_number);
        ed_expire_date = (TextView)findViewById(R.id.et_expire_date);
        et_cvv = (EditText)findViewById(R.id.et_cvv);
        et_Alias = (EditText)findViewById(R.id.et_alias);
        tv_pay_now = (TextView) findViewById(R.id.tv_pay_now);

        iv_close = (ImageView) findViewById(R.id.iv_close);
        rl_gif = (RelativeLayout)findViewById(R.id.rl_Gif);
        tv_Dollar = (TextView)findViewById(R.id.tv_dollar);

        et_Name.setText(companyName);
        et_Abn.setText(Abn);

        ll_Back.setOnClickListener(activity);
        iv_Back.setOnClickListener(activity);
        tv_pay_now.setOnClickListener(activity);
        scanCard.setOnClickListener(activity);
        ed_expire_date.setOnClickListener(activity);
        iv_close.setOnClickListener(activity);

        et_card_number.addTextChangedListener(new FourDigitCardFormatWatcher());

        et_card_number.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                isScan = false;

                return false;
            }
        });

        et_Amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (charSequence.length()<=0)
                {
                    tv_Dollar.setVisibility(View.GONE);
                }
                else
                {
                    tv_Dollar.setVisibility(View.VISIBLE);
                }

                if (charSequence.toString().trim().length()>0)
                {
                    if (charSequence.toString().trim().length()==1 && charSequence.toString().trim().equalsIgnoreCase("."))
                    {
                        et_Amount.setText("");
                    }
                    else
                    {
                        String amount = charSequence.toString().trim();
                        double total = (((rate) * Double.parseDouble(amount)) / 100) + Double.parseDouble(amount);
                        total = total + transactionRate;
                        tv_FinalTotal.setText("$"+String.format("%.2f", total));

                        if (total >= 100)
                        {
                            et_cvv.setVisibility(View.VISIBLE);
                            et_cvv.setText("");
                        }
                        else
                        {
                            et_cvv.setVisibility(View.GONE);
                        }
                    }
                }
                else
                {
                    tv_FinalTotal.setText("");
                    et_cvv.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        if (Global.isNetworkconn(activity))
        {
            getTickPayRate();
        }
        else
        {
            InternetDialog internetDialog = new InternetDialog(activity);
            internetDialog.showDialog("Please check your internet connection!");
        }
    }

    private void checkNFC() {

        mNfcUtils.enableDispatch();
        // Close
        if (mAlertDialog != null && mAlertDialog.isShowing()) {
            mAlertDialog.cancel();
        }

        // Check if NFC is available
        if (!NFCUtils.isNfcAvailable(getApplicationContext())) {

            AlertDialog.Builder alertbox = new AlertDialog.Builder(this);
            alertbox.setTitle(getString(R.string.msg_info));
            alertbox.setMessage(getString(R.string.msg_nfc_not_available));
            alertbox.setPositiveButton(getString(R.string.msg_ok), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(final DialogInterface dialog, final int which) {
                    dialog.dismiss();
                }
            });
            alertbox.setCancelable(false);
            mAlertDialog = alertbox.show();
        }
        else
        {
            cardScanNFC();
        }
    }

    private void cardScanNFC() {

        onNewIntent(getIntent());
    }

    @Override
    protected void onResume() {
        super.onResume();
        TicktocApplication.setCurrentActivity(activity);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (resumeFlag==1)
        {
            if (SessionSave.getUserSession(Common.IS_PASSCODE_REQUIRED,activity)!=null && SessionSave.getUserSession(Common.IS_PASSCODE_REQUIRED,activity).equalsIgnoreCase("1"))
            {
                resumeFlag = 0;
                Intent intent = new Intent(TickPayActivity.this,Create_Passcode_Activity.class);
                intent.putExtra("from","TickPay");
                startActivity(intent);
                finish();
            }
        }
        else
        {
            resumeFlag=1;
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.back_layout:
                onBackPressed();
                break;

            case R.id.et_expire_date:
                showBottomSheetDailog_payment();
                break;

            case R.id.back_imageview:
                onBackPressed();
                break;

            case R.id.tv_pay_now:
                if (et_Name.getText().toString().trim().equalsIgnoreCase(""))
                {
                    InternetDialog internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog("Please enter Company name / Name.");
                }
                else if (et_Amount.getText().toString().trim().equalsIgnoreCase(""))
                {
                    InternetDialog internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog("Please enter amount.");
                }
                else if (et_card_number.getText().toString().trim().equalsIgnoreCase(""))
                {
                    InternetDialog internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog("Please enter card number.");
                }
                else if (expiryDate.equalsIgnoreCase(""))
                {
                    InternetDialog internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog("Please enter expiry date.");
                }
                else
                {
                    if(et_card_number.getText().length()>11)
                    {
                        if (validornot)
                        {
                            double total = Double.parseDouble(tv_FinalTotal.getText().toString().substring(1,tv_FinalTotal.getText().toString().length()));

                            Log.e("call","total = "+total);
                            if (total>=100)
                            {
                                if (!et_cvv.getText().toString().trim().equalsIgnoreCase(""))
                                {
                                    if (Global.isNetworkconn(activity))
                                    {
                                        payNow();
                                    }
                                    else
                                    {
                                        InternetDialog internetDialog = new InternetDialog(activity);
                                        internetDialog.showDialog("Please check your internet connection!");
                                    }
                                }
                                else
                                {
                                    InternetDialog internetDialog = new InternetDialog(activity);
                                    internetDialog.showDialog("Please enter CVV number!");
                                }
                            }
                            else
                            {
                                if (Global.isNetworkconn(activity))
                                {
                                    payNow();
                                }
                                else
                                {
                                    InternetDialog internetDialog = new InternetDialog(activity);
                                    internetDialog.showDialog("Please check your internet connection!");
                                }
                            }
                        }
                        else
                        {
                            InternetDialog internetDialog = new InternetDialog(activity);
                            internetDialog.showDialog(getString(R.string.invalid_card_number));
                        }
                    }
                    else
                    {
                        InternetDialog internetDialog = new InternetDialog(activity);
                        internetDialog.showDialog(getString(R.string.invalid_card_number));
                    }
                }
                Log.e("call","cardNumber = "+cardNumberParam);
                Log.e("call","expiryDate = "+expiryDate);

                break;

            case R.id.scanCardLayout:
                cardNumberParam = "";
                expiryDate = "";
                et_card_number.setText("");
                ed_expire_date.setText("");
                et_cvv.setText("");
                et_Alias.setText("");

                scanCard();

                break;

            case R.id.iv_close:

                rl_gif.setVisibility(View.GONE);
                break;
        }
    }

    public void startNFC()
    {
        cardNumberParam = "";
        expiryDate = "";
        et_card_number.setText("");
        ed_expire_date.setText("");
        et_cvv.setText("");
        et_Alias.setText("");

        Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                checkNFC();

            }
        };
        handler.postDelayed(runnable,1000);
    }

    public void showYesNoDialog(final String total, final String displayTotal)
    {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.yes_no_dialog);

        TextView tv_yes = (TextView) dialog.findViewById(R.id.yes);
        TextView tv_No = (TextView) dialog.findViewById(R.id.no);
        TextView tv_Message = (TextView) dialog.findViewById(R.id.message);
        LinearLayout ll_Yes = (LinearLayout) dialog.findViewById(R.id.yes_layout);
        LinearLayout ll_No = (LinearLayout) dialog.findViewById(R.id.no_layout);

        tv_Message.setText("Do you want to send Payment Receipt ?");

        ll_Yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                resumeFlag = 0;
                Intent intent = new Intent(activity,Receipt_Invoice_Activity.class);
                intent.putExtra("total",total);
                intent.putExtra("displayTotal",displayTotal);
                startActivity(intent);
            }
        });

        tv_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                resumeFlag = 0;
                Intent intent = new Intent(activity,Receipt_Invoice_Activity.class);
                intent.putExtra("total",total);
                intent.putExtra("displayTotal",displayTotal);
                startActivity(intent);
            }
        });

        ll_No.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                finish();
            }
        });

        tv_No.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                finish();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        dialog.show();
    }

    private void payNow()
    {
        dialogClass.showDialog();
        String url = WebServiceAPI.API_TICK_PAY;

        Map<String, Object> params = new HashMap<String, Object>();
        params.put(WebServiceAPI.PARAM_PASSENGER_ID, SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID,activity));
        params.put(WebServiceAPI.PARAM_NAME, et_Name.getText().toString());
        params.put(WebServiceAPI.PARAM_ABN, et_Abn.getText().toString());
        params.put(WebServiceAPI.PARAM_AMOUNT, et_Amount.getText().toString().replace("$",""));

        if (isScan==true)
        {
            params.put(WebServiceAPI.PARAM_CARD_NUMBER,cardNumberParam);
        }
        else
        {
            params.put(WebServiceAPI.PARAM_CARD_NUMBER,et_card_number.getText().toString().replace(" ",""));
        }
//        params.put(WebServiceAPI.PARAM_CARD_NUMBER, );//et_card_number.getText().toString().replace(" ","")
        params.put(WebServiceAPI.PARAM_CARD_EXPIRY, expiryDate);

        double total = Double.parseDouble(tv_FinalTotal.getText().toString().substring(1,tv_FinalTotal.getText().toString().length()));

        Log.e("call","total = "+total);
        if (total>=100)
        {
            params.put(WebServiceAPI.PARAM_CARD_CVV_CAP, et_cvv.getText().toString());
        }


        Log.e("call", "url = " + url);
        Log.e("call", "params = " + params);

        aQuery.ajax(url.trim(), params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e("responseCode", " = " + responseCode);
                    Log.e("Response", " = " + json);

                    if (json!=null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                if (json.has(""))
                                {
                                    String walletBalance = json.getString("walletBalance");
                                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_WALLET_BALLENCE,walletBalance,activity);
                                }

                                String totalStr = "";
                                if (tv_FinalTotal.getText().toString().length()>=1)
                                {
                                    totalStr = tv_FinalTotal.getText().toString().substring(1,tv_FinalTotal.getText().toString().length());
                                }
                                else
                                {
                                    totalStr = "";
                                }

//                                final String total = totalStr;
                                final String total = et_Amount.getText().toString().trim();
                                final String displayTotal = totalStr;

                                et_Amount.setText("");
                                tv_FinalTotal.setText("");
                                et_card_number.setText("");
                                ed_expire_date.setText("");
                                et_cvv.setText("");

                                if (json.has("tickpay_id"))
                                {
                                    payId = json.getString("tickpay_id");
                                }
                                else
                                {
                                    payId = "";
                                }

                                dialogClass.hideDialog();

                                if (payId!=null && !payId.equalsIgnoreCase(""))
                                {
                                    showYesNoDialog(total,displayTotal);
                                }
                                else
                                {
                                    String message = "Please try again later";
                                    dialogClass.hideDialog();
                                    InternetDialog internetDialog = new InternetDialog(activity);
                                    internetDialog.showDialog(message);
                                }

                            }
                            else
                            {
                                String message = "Please try again later";
                                if (json.has("message"))
                                {
                                    message = json.getString("message");
                                }
                                dialogClass.hideDialog();
                                InternetDialog internetDialog = new InternetDialog(activity);
                                internetDialog.showDialog(message);
                            }
                        }
                        else
                        {
                            String message = "Please try again later";
                            if (json.has("message"))
                            {
                                message = json.getString("message");
                            }
                            dialogClass.hideDialog();
                            InternetDialog internetDialog = new InternetDialog(activity);
                            internetDialog.showDialog(message);
                        }
                    }
                    else
                    {
                        String message = "Please try again later";
                        if (json.has("message"))
                        {
                            message = json.getString("message");
                        }
                        dialogClass.hideDialog();
                        InternetDialog internetDialog = new InternetDialog(activity);
                        internetDialog.showDialog(message);
                    }
                }
                catch (Exception e)
                {
                    Log.e("Exception","Exception "+e.toString());
                    dialogClass.hideDialog();
                    InternetDialog internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog("Something went wrong prlease try again later");
                }
            }

        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    private void getTickPayRate()
    {
        dialogClass.showDialog();
        String url = WebServiceAPI.API_GET_TICKPAY_RATE;

        Log.e("call", "url = " + url);

        aQuery.ajax(url.trim(), null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e("responseCode", " = " + responseCode);
                    Log.e("Response", " = " + json);

                    if (json!=null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                String message = "";

                                if (json.has("rate"))
                                {
                                    if (json.getString("rate")!=null && !json.getString("rate").equalsIgnoreCase(""))
                                    {
                                        rate = Double.parseDouble(json.getString("rate"));
                                    }
                                    else
                                    {
                                        rate = 0;
                                    }

                                    message = ""+json.getString("rate") + strRate;
                                }

                                if (json.has("TransactionFee"))
                                {
                                    if (json.getString("TransactionFee")!=null && !json.getString("TransactionFee").equalsIgnoreCase(""))
                                    {
                                        transactionRate = Double.parseDouble(json.getString("TransactionFee"));
                                    }
                                    else
                                    {
                                        transactionRate = 0;
                                    }

                                    message = message +"\n"+ "$" + json.getString("TransactionFee") + strTransactionRate;
                                }

                                tv_Rate.setText(message);

                                dialogClass.hideDialog();
                                startNFC();
                            }
                            else
                            {
                                tv_Rate.setText("");
                                String message = "Please try again later";
                                if (json.has("message"))
                                {
                                    message = json.getString("message");
                                }
                                dialogClass.hideDialog();
                                InternetDialog internetDialog = new InternetDialog(activity);
                                internetDialog.showDialog(message);
                            }
                        }
                        else
                        {
                            tv_Rate.setText("");
                            String message = "Please try again later";
                            if (json.has("message"))
                            {
                                message = json.getString("message");
                            }
                            dialogClass.hideDialog();
                            InternetDialog internetDialog = new InternetDialog(activity);
                            internetDialog.showDialog(message);
                        }
                    }
                    else
                    {
                        tv_Rate.setText("");
                        String message = "Please try again later";
                        if (json.has("message"))
                        {
                            message = json.getString("message");
                        }
                        dialogClass.hideDialog();
                        InternetDialog internetDialog = new InternetDialog(activity);
                        internetDialog.showDialog(message);
                    }
                }
                catch (Exception e)
                {
                    tv_Rate.setText("");
                    Log.e("Exception","Exception "+e.toString());
                    dialogClass.hideDialog();
                    InternetDialog internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog("Something went wrong prlease try again later");
                }
            }

        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mNfcUtils.disableDispatch();
    }

    @Override
    protected void onNewIntent(final Intent intent)
    {
        super.onNewIntent(intent);
        final Tag mTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
        isScan = true;
        if (mTag != null) {

            new SimpleAsyncTask() {

                /**
                 * Tag comm
                 */
                private IsoDep mTagcomm;

                /**
                 * Emv Card
                 */
                private EmvCard mCard;

                /**
                 * Boolean to indicate exception
                 */
                private boolean mException;


                @Override
                protected void onPreExecute() {
                    super.onPreExecute();

//                    backToHomeScreen();
                    mProvider.getLog().setLength(0);
                    // Show dialog
                    if (mDialog == null) {
                        mDialog = ProgressDialog.show(TickPayActivity.this, getString(R.string.card_reading),
                                getString(R.string.card_reading_desc), true, false);
                    }
                    else
                    {
                        mDialog.show();
                    }
                }

                @Override
                protected void doInBackground() {

                    mTagcomm = IsoDep.get(mTag);
                    if (mTagcomm == null)
                    {
//                        Toast.makeText(getApplicationContext(),R.string.error_communication_nfc,Toast.LENGTH_SHORT).show();
                        showDialogForScanningFailed(getResources().getString(R.string.error_communication_nfc));
                        return;
                    }
                    mException = false;

                    try
                    {
                        mReadCard = null;
                        // Open connection
                        mTagcomm.connect();
                        lastAts = getAts(mTagcomm);

                        mProvider.setmTagCom(mTagcomm);

                        EmvParser parser = new EmvParser(mProvider, true);
                        mCard = parser.readEmvCard();
                        if (mCard != null) {
                            mCard.setAtrDescription(extractAtsDescription(lastAts));
                        }

                    } catch (IOException e) {
                        mException = true;
                    } finally {
                        // close tagcomm
                        IOUtils.closeQuietly(mTagcomm);
                    }
                }

                @Override
                protected void onPostExecute(final Object result) {
                    // close dialog
                    if (mDialog != null) {
                        mDialog.cancel();
                    }

                    if (!mException) {
                        if (mCard != null) {
                            if (StringUtils.isNotBlank(mCard.getCardNumber())) {
//                                Toast.makeText(getApplicationContext(),R.string.card_read,Toast.LENGTH_SHORT).show();
                                mReadCard = mCard;

                                String str = mReadCard.getExpireDate().toString();
                                Log.e("call","mReadCard.getExpireDate().toString() str = "+str);
                                String[] splited = str.trim().split("\\s+");
                                String month = splited[1];
                                String year = splited[splited.length-1];

                                Log.e("HomeActivity" ,
                                        "Card Number:- " + mReadCard.getCardNumber() +
                                                "\nCard Exp:- " + mReadCard.getExpireDate() +
                                                "\nCard Month:- " + month +
                                                "\nCard Year:- " + year +
                                                "\nCard Type:- " + mReadCard.getType() +
                                                "\nCard First Name:- " + mReadCard.getHolderFirstname() +
                                                "\nCard LastName:- " + mReadCard.getHolderLastname() +
                                                "\nCard Application:- " + mReadCard.getApplicationLabel() +
                                                "\nCard Aid:- " + mReadCard.getAid() +
                                                "\nCard LeftPinTrsy:- " + mReadCard.getLeftPinTry() +
                                                "\nCard Service:- " + mReadCard.getService());

                                if (mReadCard.getCardNumber()!=null)
                                {
                                    try
                                    {
                                        Calendar cal = Calendar.getInstance();
                                        cal.setTime(new SimpleDateFormat("MMM").parse(month));
                                        int monthInt = cal.get(Calendar.MONTH) + 1;

                                        Log.e("call","monthINt = "+monthInt);
                                        if (monthInt<10)
                                        {
                                            expiryMonth = "0" + monthInt;
                                        }
                                        else
                                        {
                                            expiryMonth = monthInt + "";
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        expiryMonth = "";
                                        Log.e("call","exception = "+e.getMessage());
                                    }
                                }

                                String strCard = "";

                                if (mReadCard.getCardNumber().length()>4)
                                {
                                    cardNumberParam = mReadCard.getCardNumber().replace(" ","");
                                    strCard = getFormattedCardNumber(cardNumberParam);
                                }
                                else
                                {
                                    cardNumberParam = "";
                                }

                                et_card_number.setText(strCard);

                                if (year.length()>2)
                                {
                                    expiryYear = year.substring(year.length() - 2);
                                }

                                if ((expiryMonth!=null && !expiryMonth.equalsIgnoreCase("")) && (expiryYear!=null && !expiryYear.equalsIgnoreCase("")))
                                {
                                    expiryDate = expiryMonth +"/"+expiryYear;
                                }
                                else
                                {
                                    expiryDate = "";
                                }

                                Log.e("call","expiryDate = "+expiryDate);
                                ed_expire_date.setText(month + ", " + year);

                                rl_gif.setVisibility(View.GONE);


                            }
                            else if (mCard.isNfcLocked())
                            {
                                Toast.makeText(getApplicationContext(),R.string.nfc_locked,Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            showDialogForScanningFailed(getResources().getString(R.string.error_communication_nfc));
//                            Toast.makeText(getApplicationContext(),R.string.error_card_unknown,Toast.LENGTH_SHORT).show();

                        }
                    }
                    else
                    {
//                        Toast.makeText(getApplicationContext(),R.string.error_communication_nfc,Toast.LENGTH_SHORT).show();
                        showDialogForScanningFailed(getResources().getString(R.string.error_communication_nfc));
                    }

                    refreshContent();
                }

            }.execute();
        }

    }

    public void showDialogForScanningFailed(String message)
    {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.my_dialog_class);

        TextView tv_Ok = (TextView) dialog.findViewById(R.id.Ok);
        TextView tv_Message = (TextView) dialog.findViewById(R.id.message);
        LinearLayout ll_Ok = (LinearLayout) dialog.findViewById(R.id.ok_layout);

        tv_Message.setText(message);

        ll_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                startNFC();
            }
        });

        tv_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                startNFC();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        dialog.show();
    }

    public String getFormattedCardNumber(String input)
    {
        String cardNumber = "";
        String strCardFirst = input.substring(0,(input.length()-4));
        String strCardEnd = input.substring(input.length()-4);

        Log.e("call","strCardFirst = "+strCardFirst);
        Log.e("call","strCardEnd = "+strCardEnd);

        String displayCard = "";
        for (int i=0; i<strCardFirst.length(); i++)
        {
            displayCard = displayCard + "*";
        }

        displayCard = displayCard + strCardEnd;



        for (int i=0; i<displayCard.length(); i++)
        {
            if (i>3 && (i%4)==0)
            {
                cardNumber = cardNumber  + " " + displayCard.substring(i,i+1);
            }
            else
            {
                cardNumber = cardNumber  + displayCard.substring(i,i+1);
            }
        }

        return cardNumber;
    }

    /**
     * Method used to get description from ATS
     *
     * @param pAts
     *            ATS byte
     */
    public Collection<String> extractAtsDescription(final byte[] pAts) {
        return AtrUtils.getDescriptionFromAts(BytesUtils.bytesToString(pAts));
    }

    private void refreshContent() {
        if (mRefreshableContent != null && mRefreshableContent.get() != null) {
            mRefreshableContent.get().update();
        }
    }



    /**
     * Get ATS from isoDep
     *
     * @param pIso
     *            isodep
     * @return ATS byte array
     */
    private byte[] getAts(final IsoDep pIso)
    {
        byte[] ret = null;
        if (pIso.isConnected()) {
            // Extract ATS from NFC-A
            ret = pIso.getHistoricalBytes();
            if (ret == null) {
                // Extract ATS from NFC-B
                ret = pIso.getHiLayerResponse();
            }
        }
        return ret;
    }



    @Override
    public StringBuffer getLog() {
        return mProvider.getLog();
    }

    @Override
    public EmvCard getCard() {
        return mReadCard;
    }



    @Override
    public void setRefreshableContent(final IRefreshable pRefreshable) {
        mRefreshableContent = new WeakReference<IRefreshable>(pRefreshable);
    }

    /**
     * Method used to clear data
     */
    public void clear() {
        mReadCard = null;
        mProvider.getLog().setLength(0);
        IRefreshable content = mRefreshableContent.get();
        if (content != null) {
            content.update();
        }
    }

    /**
     * Get the last ATS
     *
     * @return the last card ATS
     */
    public byte[] getLastAts() {
        return lastAts;
    }


    public void showBottomSheetDailog_payment() {

        View view = getLayoutInflater().inflate(R.layout.bottom_sheet_payment, null);
        mBottomSheetDialog = new BottomSheetDialog(activity);
        mBottomSheetDialog.setContentView(view);


        final TextView tv_Ok, tv_Cancle;

        tv_Ok = (TextView) view.findViewById(R.id.bottom_sheet_payment_ok_textview);
        tv_Cancle = (TextView) view.findViewById(R.id.bottom_sheet_payment_cancle_textview);
        numberPickerMonth = (NumberPicker) view.findViewById(R.id.numberPickerMonth);
        numberPickerYear = (NumberPicker) view.findViewById(R.id.numberPickerYear);

        numberPickerMonth.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        numberPickerYear.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

        setDividerColor(numberPickerMonth, ContextCompat.getColor(activity, R.color.colorRed));
        setDividerColor(numberPickerYear,ContextCompat.getColor(activity, R.color.colorRed));

        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH)+1;

        Log.e("Date","year"+year+"month"+month);

        numberPickerMonth.setMinValue(1);
        numberPickerMonth.setMaxValue(12);
        numberPickerYear.setMinValue(year);
        numberPickerYear.setMaxValue(year+5);

        valueMonth = new String[12];
        valueYear = new String[6];

        for(int i=0; i<12; i++)
        {
            valueMonth[i] = monthName[i]+"";
            Log.d("valueNumber","valueMonth : "+ valueMonth[i]);
        }

        for(int i=0; i<6; i++)
        {
            valueYear[i] = (year+i)+"";
            Log.d("valueNumber","valueYear : "+ valueYear[i]);
        }

        numberPickerMonth.setValue(month);
        numberPickerMonth.setDisplayedValues(monthName);
        numberPickerYear.setValue(year);


        tv_Cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mBottomSheetDialog!=null)
                {
                    mBottomSheetDialog.dismiss();
                }
            }
        });

        tv_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBottomSheetDialog!=null)
                {
                    MonthInNumber = numberPickerMonth.getValue();
                    YearInNumber = numberPickerYear.getValue();

                    String strYear = YearInNumber + "";

                    if (strYear.length()>2)
                    {
                        strYear = strYear.substring(strYear.length() - 2);
                    }

                    if (MonthInNumber < 10)
                    {
                        strExpiry = "0" + MonthInNumber + "/" + strYear;
                    }
                    else
                    {
                        strExpiry = MonthInNumber + "/" + strYear;
                    }

                    expiryDate = strExpiry;
                    Log.e("strExpiry"," = "+strExpiry);
                    Log.e("MonthInNumber"," = "+MonthInNumber);
                    Log.e("YearInNumber"," = "+strYear);

                    ed_expire_date.setText(monthName[MonthInNumber-1]);
                    ed_expire_date.append(", "+YearInNumber);

                    mBottomSheetDialog.dismiss();
                }
            }
        });

        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mBottomSheetDialog = null;
            }
        });

        mBottomSheetDialog.show();
    }

    private void setDividerColor(NumberPicker picker, int color) {

        java.lang.reflect.Field[] pickerFields = NumberPicker.class.getDeclaredFields();
        for (java.lang.reflect.Field pf : pickerFields) {
            if (pf.getName().equals("mSelectionDivider")) {
                pf.setAccessible(true);
                try {
                    ColorDrawable colorDrawable = new ColorDrawable(color);
                    pf.set(picker, colorDrawable);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                }
                catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    public class FourDigitCardFormatWatcher implements TextWatcher {

        // Change this to what you want... ' ', '-' etc..
        private static final char space = ' ';

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            // Remove spacing char
            if (s.length() > 0 && (s.length() % 5) == 0) {
                final char c = s.charAt(s.length() - 1);
                if (space == c) {
                    s.delete(s.length() - 1, s.length());
                }
            }
            // Insert char where needed.
            if (s.length() > 0 && (s.length() % 5) == 0) {
                char c = s.charAt(s.length() - 1);
                // Only if its a digit where there should be a space we insert a space
                if (Character.isDigit(c) && TextUtils.split(s.toString(), String.valueOf(space)).length <= 3) {
                    s.insert(s.length() - 1, String.valueOf(space));
                }
            }
            FindCardType(s);
        }
    }

    public void FindCardType(CharSequence s) {

        String valid = "INVALID";

        if (isScan == true)
        {
            s = cardNumberParam;
        }

        if (s.toString().length() > 5) {
            String number = s.toString().replace(" ", "");
            String digit1 = number.substring(0, 1);
            String digit2 = number.substring(0, 2);
            String digit3 = number.substring(0, 3);
            String digit4 = number.substring(0, 4);

//            iv_CardImage.setBackgroundResource(R.drawable.card_dummy);
            et_card_number.setTextColor(ContextCompat.getColor(activity, R.color.colorBlack));

            if (digit1.equals("4")) {
//                iv_CardImage.setBackgroundResource(R.drawable.card_visa);
                if (number.length() == 13 || number.length() == 16) {
                    valid = "VISA";
                    validornot = validCCNumber(number);

                    if (number.length() > 12) {
                        if (validornot) {
                            et_card_number.setTextColor(ContextCompat.getColor(activity, R.color.colorGreen));
//                            iv_CardImage.setBackgroundResource(R.drawable.card_visa);
                        } else {
                            et_card_number.setTextColor(ContextCompat.getColor(activity, R.color.colorRed));
//                            mySnackBar.showSnackBar(main_layout,getString(R.string.card_is_not_valid));
//                            iv_CardImage.setBackgroundResource(R.drawable.card_dummy);
                        }
                    }
                }
            } else if (digit4.equalsIgnoreCase("5018") || digit4.equalsIgnoreCase("5020") || digit4.equalsIgnoreCase("5038") || digit4.equalsIgnoreCase("5612") || digit4.equalsIgnoreCase("5893")
                    || digit4.equalsIgnoreCase("6304") || digit4.equalsIgnoreCase("6759") || digit4.equalsIgnoreCase("6761") || digit4.equalsIgnoreCase("6762") || digit4.equalsIgnoreCase("6763")
                    || digit4.equalsIgnoreCase("0604") || digit4.equalsIgnoreCase("6390")) {
//                iv_CardImage.setBackgroundResource(R.drawable.card_maestro);
                if (number.length() == 16) {
                    valid = "MAESTRO";
                    Log.e("MAESTRO", "MAESTRO");
                    validornot = validCCNumber(number);

                    if (number.length() == 16) {
                        if (validornot) {
                            et_card_number.setTextColor(ContextCompat.getColor(activity, R.color.colorGreen));
//                            iv_CardImage.setBackgroundResource(R.drawable.card_maestro);

                        } else {
                            et_card_number.setTextColor(ContextCompat.getColor(activity, R.color.colorRed));
//                            mySnackBar.showSnackBar(main_layout,getString(R.string.card_is_not_valid));
//                            iv_CardImage.setBackgroundResource(R.drawable.card_dummy);
                        }
                    }
                }
            } else if (digit2.equals("34") || digit2.equals("37")) {
//                iv_CardImage.setBackgroundResource(R.drawable.card_american);
                if (number.length() == 15) {
                    valid = "AMERICAN_EXPRESS";
                    validornot = validCCNumber(number);

                    if (number.length() == 15) {
                        if (validornot) {
                            et_card_number.setTextColor(ContextCompat.getColor(activity, R.color.colorGreen));
//                            iv_CardImage.setBackgroundResource(R.drawable.card_american);

                        } else {
                            et_card_number.setTextColor(ContextCompat.getColor(activity, R.color.colorRed));
//                            mySnackBar.showSnackBar(main_layout,getString(R.string.card_is_not_valid));
//                            iv_CardImage.setBackgroundResource(R.drawable.card_dummy);
                        }
                    }
                }
            } else if (digit2.equals("36") || digit2.equals("38") || (digit3.compareTo("300") >= 0 && digit3.compareTo("305") <= 0)) {
//                iv_CardImage.setBackgroundResource(R.drawable.card_dinner);
                if (number.length() == 14) {
                    valid = "DINERS_CLUB";
                    validornot = validCCNumber(number);

                    if (number.length() == 14) {
                        if (validornot) {
//                            iv_CardImage.setBackgroundResource(R.drawable.card_dinner);
                            et_card_number.setTextColor(ContextCompat.getColor(activity, R.color.colorGreen));

                        } else {
                            et_card_number.setTextColor(ContextCompat.getColor(activity, R.color.colorRed));
//                            mySnackBar.showSnackBar(main_layout,getString(R.string.card_is_not_valid));
//                            iv_CardImage.setBackgroundResource(R.drawable.card_dummy);
                        }
                    }
                }

            } else if (digit1.equals("6")) {
//                iv_CardImage.setBackgroundResource(R.drawable.card_discover);
                if (number.length() == 16) {
                    valid = "DISCOVER";
                    validornot = validCCNumber(number);

                    if (number.length() == 16) {
                        if (validornot) {
//                            iv_CardImage.setBackgroundResource(R.drawable.card_discover);
                            et_card_number.setTextColor(ContextCompat.getColor(activity, R.color.colorGreen));

                        } else {
                            et_card_number.setTextColor(ContextCompat.getColor(activity, R.color.colorRed));
//                            mySnackBar.showSnackBar(main_layout,getString(R.string.card_is_not_valid));
//                            iv_CardImage.setBackgroundResource(R.drawable.card_dummy);
                        }
                    }
                }

            } else if (digit2.equals("35")) {
//                iv_CardImage.setBackgroundResource(R.drawable.card_jcbs);
                if (number.length() == 16 || number.length() == 17 || number.length() == 18 || number.length() == 19) {
                    valid = "JBC";
                    validornot = validCCNumber(number);

                    if (number.length() > 15) {
                        if (validornot) {
//                            iv_CardImage.setBackgroundResource(R.drawable.card_jcbs);
                            et_card_number.setTextColor(ContextCompat.getColor(activity, R.color.colorGreen));

                        } else {
                            et_card_number.setTextColor(ContextCompat.getColor(activity, R.color.colorRed));
//                            mySnackBar.showSnackBar(main_layout,getString(R.string.card_is_not_valid));
//                            iv_CardImage.setBackgroundResource(R.drawable.card_dummy);
                        }
                    }
                }

            } else if (digit2.compareTo("51") >= 0 && digit2.compareTo("55") <= 0 || digit1.equalsIgnoreCase("2")) {
//                iv_CardImage.setBackgroundResource(R.drawable.card_master);
                if (number.length() == 16)
                    valid = "MASTERCARD";
                validornot = validCCNumber(number);

                if (number.length() == 16) {
                    if (validornot) {
//                        iv_CardImage.setBackgroundResource(R.drawable.card_master);
                        et_card_number.setTextColor(ContextCompat.getColor(activity, R.color.colorGreen));

                    } else {
                        et_card_number.setTextColor(ContextCompat.getColor(activity, R.color.colorRed));
//                        mySnackBar.showSnackBar(main_layout,getString(R.string.card_is_not_valid));
//                        iv_CardImage.setBackgroundResource(R.drawable.card_dummy);
                    }
                }
            }
            else
            {
                Log.e("call","99999999999999");
                validornot = false;
//            iv_CardImage.setBackgroundResource(R.drawable.card_dummy);
            }
        }
        else
        {
            Log.e("call","99999999999999");
            validornot = false;
//            iv_CardImage.setBackgroundResource(R.drawable.card_dummy);
        }

    }

    public static boolean validCCNumber(String n) {
        try {

            int j = n.length();

            String [] s1 = new String[j];
            for (int i=0; i < n.length(); i++) s1[i] = "" + n.charAt(i);

            int checksum = 0;

            for (int i=s1.length-1; i >= 0; i-= 2) {
                int k = 0;

                if (i > 0) {
                    k = Integer.valueOf(s1[i-1]).intValue() * 2;
                    if (k > 9) {
                        String s = "" + k;
                        k = Integer.valueOf(s.substring(0,1)).intValue() +
                                Integer.valueOf(s.substring(1)).intValue();
                    }
                    checksum += Integer.valueOf(s1[i]).intValue() + k;
                }
                else
                    checksum += Integer.valueOf(s1[0]).intValue();
            }
            return ((checksum % 10) == 0);
        }
        catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public void scanCard()
    {
        resumeFlag = 0;
        isScan = true;
        Intent intent = new Intent(TickPayActivity.this, CardIOActivity.class)
                .putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, true)
                .putExtra(CardIOActivity.EXTRA_SCAN_EXPIRY, true)
                .putExtra(CardIOActivity.EXTRA_GUIDE_COLOR, Color.TRANSPARENT)
                .putExtra("debug_autoAcceptResult", true);;
        startActivityForResult(intent, REQUEST_SCAN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.v("TAG", "onActivityResult(" + requestCode + ", " + resultCode + ", " + data + ")");


        if ((requestCode == REQUEST_SCAN || requestCode == REQUEST_AUTOTEST) && data != null
                && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT))
        {
            CreditCard result = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);

            Log.e("TAG", "Rusult: " + result);
            String strCardNumber = "";
            String strCardExpiry = "";
            if (result != null)
            {

                cardNumberParam = result.getFormattedCardNumber().replace(" ","");
                strCardNumber = getFormattedCardNumber(cardNumberParam);

                CardType cardType = result.getCardType();

                String year = result.expiryYear + "";

                if (year!=null && year.length()>2)
                {
                    year = year.substring(year.length()-2);
                }

                String month = getMonth(result.expiryMonth);

                if (month!=null && month.length()>3)
                {
                    month = month.substring(0,3);
                }

                if (result.expiryMonth < 10)
                {
                    strExpiry = "0" + result.expiryMonth + "/" + year;
                }
                else
                {
                    strExpiry = result.expiryMonth + "/" + year;
                }

                expiryDate = strExpiry;

                strCardExpiry = month + ", " + result.expiryYear;
                et_card_number.setText(strCardNumber);
                ed_expire_date.setText(strCardExpiry);
                et_Alias.setText(cardType.name);
            }
        }

    }

    public String getMonth(int month) {
        return new DateFormatSymbols().getMonths()[month-1];
    }
}

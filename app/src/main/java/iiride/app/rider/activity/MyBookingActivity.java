package iiride.app.rider.activity;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import iiride.app.rider.R;
import iiride.app.rider.application.TicktocApplication;
import iiride.app.rider.been.OnGoing_Been;
import iiride.app.rider.been.PastBooking_Been;
import iiride.app.rider.been.UpComming_Been;
import iiride.app.rider.comman.Common;
import iiride.app.rider.comman.SessionSave;
import iiride.app.rider.comman.WebServiceAPI;
import iiride.app.rider.fragment.OnGoing_Fragment;
import iiride.app.rider.fragment.PastBooking_Fragment;
import iiride.app.rider.fragment.UpComming_Fragment;
import iiride.app.rider.other.DialogClass;
import iiride.app.rider.other.Global;
import iiride.app.rider.other.InternetDialog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MyBookingActivity extends AppCompatActivity implements View.OnClickListener {

    public static MyBookingActivity activity;
    private LinearLayout ll_Back;
    private ImageView iv_Back;
    private TextView tv_Title;
    private LinearLayout ll_OnGoing, ll_UpComming, ll_PastBooking;
    public static int selectedTab = 1;
    public static int preSelectedTab = 1;
    private DialogClass dialogClass;
    private AQuery aQuery;
    public static List<UpComming_Been> upComming_beens = new ArrayList<UpComming_Been>();
    public static List<OnGoing_Been> onGoing_beens = new ArrayList<OnGoing_Been>();
    public static List<PastBooking_Been> pastBooking_beens = new ArrayList<PastBooking_Been>();

    private View view_OnGoing, view_UpComming, view_PastBooking;

    private String notificationType = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_booking);

        activity = MyBookingActivity.this;
        selectedTab = 1;
        dialogClass = new DialogClass(activity, 0);
        aQuery = new AQuery(activity);

        upComming_beens.clear();
        onGoing_beens.clear();
        pastBooking_beens.clear();

        if (this.getIntent() != null) {
            if (this.getIntent().getStringExtra("notification") != null) {
                notificationType = this.getIntent().getStringExtra("notification");
            } else {
                notificationType = "";
            }
        } else {
            notificationType = "";
        }

        init();
    }

    private void init() {

        ll_Back = (LinearLayout) findViewById(R.id.back_layout);
        iv_Back = (ImageView) findViewById(R.id.back_imageview);
        tv_Title = (TextView) findViewById(R.id.title_textview);

        tv_Title.setText(getResources().getString(R.string.title_my_booking));

        ll_OnGoing = (LinearLayout) findViewById(R.id.my_booking_layout_on_going);
        ll_UpComming = (LinearLayout) findViewById(R.id.my_booking_layout_up_comming);
        ll_PastBooking = (LinearLayout) findViewById(R.id.my_booking_layout_past_booking);

        view_OnGoing = (View) findViewById(R.id.view_on_going);
        view_UpComming = (View) findViewById(R.id.view_up_comming);
        view_PastBooking = (View) findViewById(R.id.view_past_booking);


        ll_Back.setOnClickListener(activity);
        iv_Back.setOnClickListener(activity);

        ll_OnGoing.setOnClickListener(activity);
        ll_UpComming.setOnClickListener(activity);
        ll_PastBooking.setOnClickListener(activity);

        if (Global.isNetworkconn(activity)) {
            call_MyBookingHistoryApi();
        } else {
            InternetDialog internetDialog = new InternetDialog(activity);
            internetDialog.showDialog("Please check your internet connection!");
        }

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.back_layout:
                onBackPressed();
                break;

            case R.id.back_imageview:
                onBackPressed();
                break;

            case R.id.my_booking_layout_on_going:
                call_OnGoing();
                break;

            case R.id.my_booking_layout_up_comming:
                call_UpComming();
                break;

            case R.id.my_booking_layout_past_booking:
                call_PastBooking();
                break;
        }
    }

    private void call_PastBooking() {
        Log.e("call", "call_PastBooking()");
        preSelectedTab = selectedTab;
        selectedTab = 2;
        view_PastBooking.setBackgroundColor(getResources().getColor(R.color.colorRed));
        view_OnGoing.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        view_UpComming.setBackgroundColor(getResources().getColor(R.color.colorWhite));

//        ll_PastBooking.setBackgroundResource(R.drawable.square_bottom_arrow);
//        ll_UpComming.setBackgroundResource(R.drawable.square_bottom_arrow_unselect);
//        ll_OnGoing.setBackgroundResource(R.drawable.square_bottom_arrow_unselect);

        load_PastBooking();
    }

    private void call_UpComming() {
        Log.e("call", "call_UpComming()");
        preSelectedTab = selectedTab;
        selectedTab = 1;
//        ll_PastBooking.setBackgroundResource(R.drawable.square_bottom_arrow_unselect);
//        ll_UpComming.setBackgroundResource(R.drawable.square_bottom_arrow);
//        ll_OnGoing.setBackgroundResource(R.drawable.square_bottom_arrow_unselect);
        view_PastBooking.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        view_OnGoing.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        view_UpComming.setBackgroundColor(getResources().getColor(R.color.colorRed));
        load_UpComming();
    }

    private void call_OnGoing() {
        Log.e("call", "call_OnGoing()");
        preSelectedTab = selectedTab;
        selectedTab = 0;
//        ll_PastBooking.setBackgroundResource(R.drawable.square_bottom_arrow_unselect);
//        ll_UpComming.setBackgroundResource(R.drawable.square_bottom_arrow_unselect);
//        ll_OnGoing.setBackgroundResource(R.drawable.square_bottom_arrow);
        view_PastBooking.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        view_OnGoing.setBackgroundColor(getResources().getColor(R.color.colorRed));
        view_UpComming.setBackgroundColor(getResources().getColor(R.color.colorWhite));

        load_OnGoing();
    }

    private void load_PastBooking() {
        Log.e("call", "load_PastBooking()");
        if (preSelectedTab == selectedTab) {
            FragmentManager fragmentManager_items = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction_items = fragmentManager_items.beginTransaction();
            fragmentTransaction_items.replace(R.id.container_my_booking, new PastBooking_Fragment(), "PastBooking");
            fragmentTransaction_items.commitAllowingStateLoss();
        } else if (preSelectedTab < selectedTab) {
            FragmentManager fragmentManager_items = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction_items = fragmentManager_items.beginTransaction();
            fragmentTransaction_items.setCustomAnimations(R.anim.gla_there_come, R.anim.gla_there_gone, R.anim.gla_back_come, R.anim.gla_back_gone);
            fragmentTransaction_items.replace(R.id.container_my_booking, new PastBooking_Fragment(), "PastBooking");
            fragmentTransaction_items.commitAllowingStateLoss();
        } else {
            FragmentManager fragmentManager_items = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction_items = fragmentManager_items.beginTransaction();
            fragmentTransaction_items.setCustomAnimations(R.anim.gla_back_come, R.anim.gla_back_gone, R.anim.gla_there_come, R.anim.gla_there_gone);
            fragmentTransaction_items.replace(R.id.container_my_booking, new PastBooking_Fragment(), "PastBooking");
            fragmentTransaction_items.commitAllowingStateLoss();
        }
    }

    private void load_UpComming() {
        Log.e("call", "load_UpComming()");
        if (preSelectedTab == selectedTab) {
            FragmentManager fragmentManager_items = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction_items = fragmentManager_items.beginTransaction();
            fragmentTransaction_items.replace(R.id.container_my_booking, new UpComming_Fragment(), "UpComming");
            fragmentTransaction_items.commitAllowingStateLoss();
        } else if (preSelectedTab < selectedTab) {
            FragmentManager fragmentManager_items = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction_items = fragmentManager_items.beginTransaction();
            fragmentTransaction_items.setCustomAnimations(R.anim.gla_there_come, R.anim.gla_there_gone, R.anim.gla_back_come, R.anim.gla_back_gone);
            fragmentTransaction_items.replace(R.id.container_my_booking, new UpComming_Fragment(), "UpComming");
            fragmentTransaction_items.commitAllowingStateLoss();
        } else {
            FragmentManager fragmentManager_items = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction_items = fragmentManager_items.beginTransaction();
            fragmentTransaction_items.setCustomAnimations(R.anim.gla_back_come, R.anim.gla_back_gone, R.anim.gla_there_come, R.anim.gla_there_gone);
            fragmentTransaction_items.replace(R.id.container_my_booking, new UpComming_Fragment(), "UpComming");
            fragmentTransaction_items.commitAllowingStateLoss();
        }

    }

    private void load_OnGoing() {
        Log.e("call", "load_OnGoing()");
        if (preSelectedTab == selectedTab) {
            FragmentManager fragmentManager_items = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction_items = fragmentManager_items.beginTransaction();
            fragmentTransaction_items.replace(R.id.container_my_booking, new OnGoing_Fragment(), "OnGoing");
            fragmentTransaction_items.commitAllowingStateLoss();
        } else if (preSelectedTab < selectedTab) {
            FragmentManager fragmentManager_items = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction_items = fragmentManager_items.beginTransaction();
            fragmentTransaction_items.setCustomAnimations(R.anim.gla_there_come, R.anim.gla_there_gone, R.anim.gla_back_come, R.anim.gla_back_gone);
            fragmentTransaction_items.replace(R.id.container_my_booking, new OnGoing_Fragment(), "OnGoing");
            fragmentTransaction_items.commitAllowingStateLoss();
        } else {
            FragmentManager fragmentManager_items = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction_items = fragmentManager_items.beginTransaction();
            fragmentTransaction_items.setCustomAnimations(R.anim.gla_back_come, R.anim.gla_back_gone, R.anim.gla_there_come, R.anim.gla_there_gone);
            fragmentTransaction_items.replace(R.id.container_my_booking, new OnGoing_Fragment(), "OnGoing");
            fragmentTransaction_items.commitAllowingStateLoss();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
    }

    private void call_MyBookingHistoryApi() {
        dialogClass.showDialog();
        String url = WebServiceAPI.API_BOOKING_HISTORY + SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID, activity);

        onGoing_beens.clear();
        upComming_beens.clear();
        pastBooking_beens.clear();

        Log.e("call", "url = " + url);

        aQuery.ajax(url.trim(), null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("responseCode", " = " + responseCode);
                    Log.e("Response", " = " + json);

                    if (json != null) {
                        if (json.has("status")) {
                            if (json.getBoolean("status")) {
                                if (json.has("history")) {
                                    JSONArray history = json.getJSONArray("history");

                                    if (history != null && history.length() > 0) {
                                        for (int i = 0; i < history.length(); i++) {
                                            JSONObject jsonObject = history.getJSONObject(i);

                                            if (jsonObject != null)
                                            {
                                                if (jsonObject.has("Status"))
                                                {
                                                    String Status1 = jsonObject.getString("Status");

                                                    if (Status1!=null && !Status1.trim().equalsIgnoreCase(""))
                                                    {
                                                        if (Status1.equalsIgnoreCase("pending") || Status1.equalsIgnoreCase("accepted"))
                                                        {
                                                            String Id = "", CompanyId = "", PassengerId = "", ModelId = "", DriverId = "", CreatedDate = "", TransactionId = "", PaymentStatus = "";
                                                            String PickupTime = "", PickupDateTime = "", DropTime = "", TripDuration = "", TripDistance = "", PickupLocation = "", DropoffLocation = "";
                                                            String NightFareApplicable = "", NightFare = "", TripFare = "", WaitingTime = "", WaitingTimeCost = "", TollFee = "", BookingCharge = "";
                                                            String Tax = "", PromoCode = "", CompanyTax = "", SubTotal = "", GrandTotal = "", Status = "", Reason = "", PaymentType = "", AdminAmount = "";
                                                            String CompanyAmount = "", PassengerType = "", PassengerName = "", PassengerContact = "", FlightNumber = "", Model = "", DriverName = "";
                                                            String HistoryType = "", BookingType = "", DropOffDateTime = "", DriverMobileNo = "",Notes="",BabySeat="",CardId="";
                                                            String PickupLat="",PickupLng="",DropOffLat="",DropOffLon="",PickupSuburb="",DropoffSuburb="";

                                                            String CarDetails_Id = "", CarDetails_CompanyId = "", CarDetails_DriverId = "", CarDetails_VehicleModel = "";
                                                            String CarDetails_Company = "", CarDetails_Color = "", CarDetails_VehicleRegistrationNo = "", CarDetails_RegistrationCertificate = "";
                                                            String CarDetails_VehicleInsuranceCertificate = "", CarDetails_RegistrationCertificateExpire = "", CarDetails_VehicleInsuranceCertificateExpire = "";
                                                            String CarDetails_VehicleImage = "", CarDetails_Description = "";

                                                            if (jsonObject.has("Id")) {
                                                                Id = jsonObject.getString("Id");
                                                            }

                                                            if (jsonObject.has("CompanyId")) {
                                                                CompanyId = jsonObject.getString("CompanyId");
                                                            }

                                                            if (jsonObject.has("PassengerId")) {
                                                                PassengerId = jsonObject.getString("PassengerId");
                                                            }

                                                            if (jsonObject.has("ModelId")) {
                                                                ModelId = jsonObject.getString("ModelId");
                                                            }

                                                            if (jsonObject.has("DriverId")) {
                                                                DriverId = jsonObject.getString("DriverId");
                                                            }

                                                            if (jsonObject.has("CreatedDate")) {
                                                                CreatedDate = jsonObject.getString("CreatedDate");
                                                            }

                                                            if (jsonObject.has("TransactionId")) {
                                                                TransactionId = jsonObject.getString("TransactionId");
                                                            }

                                                            if (jsonObject.has("PaymentStatus")) {
                                                                PaymentStatus = jsonObject.getString("PaymentStatus");
                                                            }

                                                            if (jsonObject.has("PickupTime")) {
                                                                PickupTime = jsonObject.getString("PickupTime");
                                                            }

                                                            if (jsonObject.has("PickupDateTime") && !jsonObject.getString("PickupDateTime").equals("false")) {
                                                                PickupDateTime = jsonObject.getString("PickupDateTime");
                                                            }

                                                            if (jsonObject.has("DropOffDateTime") && !jsonObject.getString("DropOffDateTime").equals("false")) {
                                                                DropOffDateTime = jsonObject.getString("DropOffDateTime");
                                                            }

                                                            if (jsonObject.has("DropTime")) {
                                                                DropTime = jsonObject.getString("DropTime");
                                                            }

                                                            if (jsonObject.has("TripDuration")) {
                                                                TripDuration = jsonObject.getString("TripDuration");
                                                            }

                                                            if (jsonObject.has("TripDistance")) {
                                                                TripDistance = jsonObject.getString("TripDistance");
                                                            }

                                                            if (jsonObject.has("PickupLocation")) {
                                                                PickupLocation = jsonObject.getString("PickupLocation");
                                                            }

                                                            if (jsonObject.has("DropoffLocation")) {
                                                                DropoffLocation = jsonObject.getString("DropoffLocation");
                                                            }

                                                            if (jsonObject.has("NightFareApplicable")) {
                                                                NightFareApplicable = jsonObject.getString("NightFareApplicable");
                                                            }

                                                            if (jsonObject.has("NightFare")) {
                                                                NightFare = jsonObject.getString("NightFare");
                                                            }

                                                            if (jsonObject.has("TripFare")) {
                                                                TripFare = jsonObject.getString("TripFare");
                                                            }

                                                            if (jsonObject.has("WaitingTime")) {
                                                                WaitingTime = jsonObject.getString("WaitingTime");
                                                            }

                                                            if (jsonObject.has("WaitingTimeCost")) {
                                                                WaitingTimeCost = jsonObject.getString("WaitingTimeCost");
                                                            }

                                                            if (jsonObject.has("TollFee")) {
                                                                TollFee = jsonObject.getString("TollFee");
                                                            }

                                                            if (jsonObject.has("BookingCharge")) {
                                                                BookingCharge = jsonObject.getString("BookingCharge");
                                                            }

                                                            if (jsonObject.has("Tax")) {
                                                                Tax = jsonObject.getString("Tax");
                                                            }

                                                            if (jsonObject.has("PromoCode")) {
                                                                PromoCode = jsonObject.getString("PromoCode");
                                                            }

                                                            if (jsonObject.has("CompanyTax")) {
                                                                CompanyTax = jsonObject.getString("CompanyTax");
                                                            }

                                                            if (jsonObject.has("SubTotal")) {
                                                                SubTotal = jsonObject.getString("SubTotal");
                                                            }

                                                            if (jsonObject.has("GrandTotal")) {
                                                                GrandTotal = jsonObject.getString("GrandTotal");
                                                            }

                                                            if (jsonObject.has("Status")) {
                                                                Status = jsonObject.getString("Status");
                                                            }

                                                            if (jsonObject.has("Reason")) {
                                                                Reason = jsonObject.getString("Reason");
                                                            }

                                                            if (jsonObject.has("PaymentType")) {
                                                                PaymentType = jsonObject.getString("PaymentType");
                                                            }

                                                            if (jsonObject.has("AdminAmount")) {
                                                                AdminAmount = jsonObject.getString("AdminAmount");
                                                            }

                                                            if (jsonObject.has("CompanyAmount")) {
                                                                CompanyAmount = jsonObject.getString("CompanyAmount");
                                                            }

                                                            if (jsonObject.has("PassengerType")) {
                                                                PassengerType = jsonObject.getString("PassengerType");
                                                            }

                                                            if (jsonObject.has("PassengerName")) {
                                                                PassengerName = jsonObject.getString("PassengerName");
                                                            }

                                                            if (jsonObject.has("PassengerContact")) {
                                                                PassengerContact = jsonObject.getString("PassengerContact");
                                                            }

                                                            if (jsonObject.has("FlightNumber")) {
                                                                FlightNumber = jsonObject.getString("FlightNumber");
                                                            }

                                                            if (jsonObject.has("Model")) {
                                                                Model = jsonObject.getString("Model");
                                                            }

                                                            if (jsonObject.has("DriverName")) {
                                                                DriverName = jsonObject.getString("DriverName");
                                                            }

                                                            if (jsonObject.has("HistoryType")) {
                                                                HistoryType = jsonObject.getString("HistoryType");
                                                            }

                                                            if (jsonObject.has("BookingType")) {
                                                                BookingType = jsonObject.getString("BookingType");
                                                            }

                                                            if (jsonObject.has("DriverMobileNo") && jsonObject.getString("DriverMobileNo") != null && !jsonObject.getString("DriverMobileNo").equalsIgnoreCase("NULL") && !jsonObject.getString("DriverMobileNo").equalsIgnoreCase("null")) {
                                                                DriverMobileNo = jsonObject.getString("DriverMobileNo");
                                                            }

                                                            if (jsonObject.has("Notes") && jsonObject.getString("Notes") != null) {
                                                                Notes = jsonObject.getString("Notes");
                                                            }

                                                            if (jsonObject.has("BabySeat") && jsonObject.getString("BabySeat") != null) {
                                                                BabySeat = jsonObject.getString("BabySeat");
                                                            }

                                                            if (jsonObject.has("CardId") && jsonObject.getString("CardId") != null) {
                                                                CardId = jsonObject.getString("CardId");
                                                            }

                                                            if (jsonObject.has("PickupLat") && jsonObject.getString("PickupLat") != null) {
                                                                PickupLat = jsonObject.getString("PickupLat");
                                                            }
                                                            if (jsonObject.has("PickupLng") && jsonObject.getString("PickupLng") != null) {
                                                                PickupLng = jsonObject.getString("PickupLng");
                                                            }
                                                            if (jsonObject.has("DropOffLat") && jsonObject.getString("DropOffLat") != null) {
                                                                DropOffLat = jsonObject.getString("DropOffLat");
                                                            }
                                                            if (jsonObject.has("DropOffLon") && jsonObject.getString("DropOffLon") != null) {
                                                                DropOffLon = jsonObject.getString("DropOffLon");
                                                            }

                                                            if (jsonObject.has("PickupSuburb") && jsonObject.getString("PickupSuburb") != null) {
                                                                PickupSuburb = jsonObject.getString("PickupSuburb");
                                                            }
                                                            if (jsonObject.has("DropoffSuburb") && jsonObject.getString("DropoffSuburb") != null) {
                                                                DropoffSuburb = jsonObject.getString("DropoffSuburb");
                                                            }

                                                            if (jsonObject.has("CarDetails"))
                                                            {
                                                                JSONObject CarDetails = jsonObject.getJSONObject("CarDetails");

                                                                if (CarDetails != null) {
                                                                    if (CarDetails.has("Id")) {
                                                                        CarDetails_Id = CarDetails.getString("Id");
                                                                    }

                                                                    if (CarDetails.has("CompanyId")) {
                                                                        CarDetails_CompanyId = CarDetails.getString("CompanyId");
                                                                    }

                                                                    if (CarDetails.has("DriverId")) {
                                                                        CarDetails_DriverId = CarDetails.getString("DriverId");
                                                                    }

                                                                    if (CarDetails.has("VehicleModel")) {
                                                                        CarDetails_VehicleModel = CarDetails.getString("VehicleModel");
                                                                    }

                                                                    if (CarDetails.has("Company")) {
                                                                        CarDetails_Company = CarDetails.getString("Company");
                                                                    }

                                                                    if (CarDetails.has("Color")) {
                                                                        CarDetails_Color = CarDetails.getString("Color");
                                                                    }

                                                                    if (CarDetails.has("VehicleRegistrationNo")) {
                                                                        CarDetails_VehicleRegistrationNo = CarDetails.getString("VehicleRegistrationNo");
                                                                    }

                                                                    if (CarDetails.has("RegistrationCertificate")) {
                                                                        CarDetails_RegistrationCertificate = CarDetails.getString("RegistrationCertificate");
                                                                    }

                                                                    if (CarDetails.has("VehicleInsuranceCertificate")) {
                                                                        CarDetails_VehicleInsuranceCertificate = CarDetails.getString("VehicleInsuranceCertificate");
                                                                    }

                                                                    if (CarDetails.has("RegistrationCertificateExpire")) {
                                                                        CarDetails_RegistrationCertificateExpire = CarDetails.getString("RegistrationCertificateExpire");
                                                                    }

                                                                    if (CarDetails.has("VehicleInsuranceCertificateExpire")) {
                                                                        CarDetails_VehicleInsuranceCertificateExpire = CarDetails.getString("VehicleInsuranceCertificateExpire");
                                                                    }

                                                                    if (CarDetails.has("VehicleImage")) {
                                                                        CarDetails_VehicleImage = WebServiceAPI.BASE_URL_IMAGE + CarDetails.getString("VehicleImage");
                                                                    }

                                                                    if (CarDetails.has("Description")) {
                                                                        CarDetails_Description = CarDetails.getString("Description");
                                                                    }
                                                                }
                                                            }

                                                            upComming_beens.add(new UpComming_Been(
                                                                    Id,
                                                                    CompanyId,
                                                                    PassengerId,
                                                                    ModelId,
                                                                    DriverId,
                                                                    CreatedDate,
                                                                    TransactionId,
                                                                    PaymentStatus,
                                                                    PickupTime,
                                                                    PickupDateTime,
                                                                    DropTime,
                                                                    TripDuration,
                                                                    TripDistance,
                                                                    PickupLocation,
                                                                    DropoffLocation,
                                                                    NightFareApplicable,
                                                                    NightFare,
                                                                    TripFare,
                                                                    WaitingTime,
                                                                    WaitingTimeCost,
                                                                    TollFee,
                                                                    BookingCharge,
                                                                    Tax,
                                                                    PromoCode,
                                                                    CompanyTax,
                                                                    SubTotal,
                                                                    GrandTotal,
                                                                    Status,
                                                                    Reason,
                                                                    PaymentType,
                                                                    AdminAmount,
                                                                    CompanyAmount,
                                                                    PassengerType,
                                                                    PassengerName,
                                                                    PassengerContact,
                                                                    FlightNumber,
                                                                    Model,
                                                                    DriverName,
                                                                    HistoryType,
                                                                    BookingType,
                                                                    DropOffDateTime,
                                                                    DriverMobileNo,

                                                                    CarDetails_Id,
                                                                    CarDetails_CompanyId,
                                                                    CarDetails_DriverId,
                                                                    CarDetails_VehicleModel,
                                                                    CarDetails_Company,
                                                                    CarDetails_Color,
                                                                    CarDetails_VehicleRegistrationNo,
                                                                    CarDetails_RegistrationCertificate,
                                                                    CarDetails_VehicleInsuranceCertificate,
                                                                    CarDetails_RegistrationCertificateExpire,
                                                                    CarDetails_VehicleInsuranceCertificateExpire,
                                                                    CarDetails_VehicleImage,
                                                                    CarDetails_Description,
                                                                    Notes,
                                                                    BabySeat,
                                                                    CardId,
                                                                    PickupLat,
                                                                    PickupLng,
                                                                    DropOffLat,
                                                                    DropOffLon,
                                                                    PickupSuburb,
                                                                    DropoffSuburb
                                                            ));
                                                        }
                                                        else if (Status1.equalsIgnoreCase("traveling"))
                                                        {
                                                            String Id = "", PassengerId = "", ModelId = "", DriverId = "", CreatedDate = "", TransactionId = "", PaymentStatus = "";
                                                            String PickupTime = "", DropTime = "", TripDuration = "", TripDistance = "", PickupLocation = "", DropoffLocation = "";
                                                            String NightFareApplicable = "", NightFare = "", TripFare = "", WaitingTime = "", WaitingTimeCost = "", TollFee = "", BookingCharge = "";
                                                            String Tax = "", PromoCode = "", Discount = "", SubTotal = "", GrandTotal = "", Status = "", Reason = "", PaymentType = "", AdminAmount = "";
                                                            String CompanyAmount = "", PickupLat = "", PickupLng = "", DropOffLat = "", DropOffLon = "", Model = "", DriverName = "";
                                                            String CarDetails_Id = "", CarDetails_CompanyId = "", CarDetails_DriverId = "", CarDetails_VehicleModel = "";
                                                            String CarDetails_Company = "", CarDetails_Color = "", CarDetails_VehicleRegistrationNo = "", CarDetails_RegistrationCertificate = "";
                                                            String CarDetails_VehicleInsuranceCertificate = "", CarDetails_RegistrationCertificateExpire = "", CarDetails_VehicleInsuranceCertificateExpire = "";
                                                            String CarDetails_VehicleImage = "", CarDetails_Description = "", HistoryType = "", BookingType = "";
                                                            String PickupDateTime = "", DropOffDateTime = "", DriverMobileNo = "";
                                                            String CardId="",Notes="",BabySeat="",PickupSuburb="",DropoffSuburb="";

                                                            if (jsonObject.has("CardId")) {
                                                                CardId = jsonObject.getString("CardId");
                                                            }

                                                            if (jsonObject.has("Notes")) {
                                                                Notes = jsonObject.getString("Notes");
                                                            }

                                                            if (jsonObject.has("BabySeat")) {
                                                                BabySeat = jsonObject.getString("BabySeat");
                                                            }

                                                            if (jsonObject.has("PickupSuburb")) {
                                                                PickupSuburb = jsonObject.getString("PickupSuburb");
                                                            }

                                                            if (jsonObject.has("DropoffSuburb")) {
                                                                DropoffSuburb = jsonObject.getString("DropoffSuburb");
                                                            }

                                                            if (jsonObject.has("Id")) {
                                                                Id = jsonObject.getString("Id");
                                                            }

                                                            if (jsonObject.has("PassengerId")) {
                                                                PassengerId = jsonObject.getString("PassengerId");
                                                            }

                                                            if (jsonObject.has("ModelId")) {
                                                                ModelId = jsonObject.getString("ModelId");
                                                            }

                                                            if (jsonObject.has("DriverId")) {
                                                                DriverId = jsonObject.getString("DriverId");
                                                            }

                                                            if (jsonObject.has("CreatedDate")) {
                                                                CreatedDate = jsonObject.getString("CreatedDate");
                                                            }

                                                            if (jsonObject.has("TransactionId")) {
                                                                TransactionId = jsonObject.getString("TransactionId");
                                                            }

                                                            if (jsonObject.has("PaymentStatus")) {
                                                                PaymentStatus = jsonObject.getString("PaymentStatus");
                                                            }

                                                            if (jsonObject.has("PickupTime")) {
                                                                PickupTime = jsonObject.getString("PickupTime");
                                                            }

                                                            if (jsonObject.has("DropTime")) {
                                                                DropTime = jsonObject.getString("DropTime");
                                                            }

                                                            if (jsonObject.has("PickupDateTime") && !jsonObject.getString("PickupDateTime").equals("false")) {
                                                                PickupDateTime = jsonObject.getString("PickupDateTime");
                                                            }

                                                            if (jsonObject.has("DropOffDateTime") && !jsonObject.getString("DropOffDateTime").equals("false")) {
                                                                DropOffDateTime = jsonObject.getString("DropOffDateTime");
                                                            }

                                                            if (jsonObject.has("TripDuration")) {
                                                                TripDuration = jsonObject.getString("TripDuration");
                                                            }

                                                            if (jsonObject.has("TripDistance")) {
                                                                TripDistance = jsonObject.getString("TripDistance");
                                                            }

                                                            if (jsonObject.has("PickupLocation")) {
                                                                PickupLocation = jsonObject.getString("PickupLocation");
                                                            }

                                                            if (jsonObject.has("DropoffLocation")) {
                                                                DropoffLocation = jsonObject.getString("DropoffLocation");
                                                            }

                                                            if (jsonObject.has("NightFareApplicable")) {
                                                                NightFareApplicable = jsonObject.getString("NightFareApplicable");
                                                            }

                                                            if (jsonObject.has("NightFare")) {
                                                                NightFare = jsonObject.getString("NightFare");
                                                            }

                                                            if (jsonObject.has("TripFare")) {
                                                                TripFare = jsonObject.getString("TripFare");
                                                            }

                                                            if (jsonObject.has("WaitingTime")) {
                                                                WaitingTime = jsonObject.getString("WaitingTime");
                                                            }

                                                            if (jsonObject.has("WaitingTimeCost")) {
                                                                WaitingTimeCost = jsonObject.getString("WaitingTimeCost");
                                                            }

                                                            if (jsonObject.has("TollFee")) {
                                                                TollFee = jsonObject.getString("TollFee");
                                                            }

                                                            if (jsonObject.has("BookingCharge")) {
                                                                BookingCharge = jsonObject.getString("BookingCharge");
                                                            }

                                                            if (jsonObject.has("Tax")) {
                                                                Tax = jsonObject.getString("Tax");
                                                            }

                                                            if (jsonObject.has("PromoCode")) {
                                                                PromoCode = jsonObject.getString("PromoCode");
                                                            }

                                                            if (jsonObject.has("Discount")) {
                                                                Discount = jsonObject.getString("Discount");
                                                            }

                                                            if (jsonObject.has("SubTotal")) {
                                                                SubTotal = jsonObject.getString("SubTotal");
                                                            }

                                                            if (jsonObject.has("GrandTotal")) {
                                                                GrandTotal = jsonObject.getString("GrandTotal");
                                                            }

                                                            if (jsonObject.has("Status")) {
                                                                Status = jsonObject.getString("Status");
                                                            }

                                                            if (jsonObject.has("Reason")) {
                                                                Reason = jsonObject.getString("Reason");
                                                            }

                                                            if (jsonObject.has("PaymentType")) {
                                                                PaymentType = jsonObject.getString("PaymentType");
                                                            }

                                                            if (jsonObject.has("AdminAmount")) {
                                                                AdminAmount = jsonObject.getString("AdminAmount");
                                                            }

                                                            if (jsonObject.has("CompanyAmount")) {
                                                                CompanyAmount = jsonObject.getString("CompanyAmount");
                                                            }

                                                            if (jsonObject.has("PickupLat")) {
                                                                PickupLat = jsonObject.getString("PickupLat");
                                                            }

                                                            if (jsonObject.has("PickupLng")) {
                                                                PickupLng = jsonObject.getString("PickupLng");
                                                            }

                                                            if (jsonObject.has("DropOffLat")) {
                                                                DropOffLat = jsonObject.getString("DropOffLat");
                                                            }

                                                            if (jsonObject.has("DropOffLon")) {
                                                                DropOffLon = jsonObject.getString("DropOffLon");
                                                            }

                                                            if (jsonObject.has("Model")) {
                                                                Model = jsonObject.getString("Model");
                                                            }

                                                            if (jsonObject.has("DriverName")) {
                                                                DriverName = jsonObject.getString("DriverName");
                                                            }

                                                            if (jsonObject.has("DriverMobileNo") && jsonObject.getString("DriverMobileNo") != null && !jsonObject.getString("DriverMobileNo").equalsIgnoreCase("NULL") && !jsonObject.getString("DriverMobileNo").equalsIgnoreCase("null")) {
                                                                DriverMobileNo = jsonObject.getString("DriverMobileNo");
                                                            }

                                                            if (jsonObject.has("CarDetails"))
                                                            {
                                                                JSONObject CarDetails = jsonObject.getJSONObject("CarDetails");

                                                                if (CarDetails != null) {
                                                                    if (CarDetails.has("Id")) {
                                                                        CarDetails_Id = CarDetails.getString("Id");
                                                                    }

                                                                    if (CarDetails.has("CompanyId")) {
                                                                        CarDetails_CompanyId = CarDetails.getString("CompanyId");
                                                                    }

                                                                    if (CarDetails.has("VehicleModel")) {
                                                                        CarDetails_VehicleModel = CarDetails.getString("VehicleModel");
                                                                    }

                                                                    if (CarDetails.has("Company")) {
                                                                        CarDetails_Company = CarDetails.getString("Company");
                                                                    }

                                                                    if (CarDetails.has("Color")) {
                                                                        CarDetails_Color = CarDetails.getString("Color");
                                                                    }

                                                                    if (CarDetails.has("VehicleRegistrationNo")) {
                                                                        CarDetails_VehicleRegistrationNo = CarDetails.getString("VehicleRegistrationNo");
                                                                    }

                                                                    if (CarDetails.has("RegistrationCertificate")) {
                                                                        CarDetails_RegistrationCertificate = CarDetails.getString("RegistrationCertificate");
                                                                    }

                                                                    if (CarDetails.has("VehicleInsuranceCertificate")) {
                                                                        CarDetails_VehicleInsuranceCertificate = CarDetails.getString("VehicleInsuranceCertificate");
                                                                    }

                                                                    if (CarDetails.has("RegistrationCertificateExpire")) {
                                                                        CarDetails_RegistrationCertificateExpire = CarDetails.getString("RegistrationCertificateExpire");
                                                                    }

                                                                    if (CarDetails.has("VehicleInsuranceCertificateExpire")) {
                                                                        CarDetails_VehicleInsuranceCertificateExpire = CarDetails.getString("VehicleInsuranceCertificateExpire");
                                                                    }

                                                                    if (CarDetails.has("VehicleImage")) {
                                                                        CarDetails_VehicleImage = WebServiceAPI.BASE_URL_IMAGE + CarDetails.getString("VehicleImage");
                                                                    }

                                                                    if (CarDetails.has("Description")) {
                                                                        CarDetails_Description = CarDetails.getString("Description");
                                                                    }
                                                                }
                                                            }

                                                            if (jsonObject.has("HistoryType")) {
                                                                HistoryType = jsonObject.getString("HistoryType");
                                                            }

                                                            if (jsonObject.has("BookingType")) {
                                                                BookingType = jsonObject.getString("BookingType");
                                                            }

                                                            onGoing_beens.add(new OnGoing_Been(Id,
                                                                    PassengerId,
                                                                    ModelId,
                                                                    DriverId,
                                                                    CreatedDate,
                                                                    TransactionId,
                                                                    PaymentStatus,
                                                                    PickupTime,
                                                                    DropTime,
                                                                    TripDuration,
                                                                    TripDistance,
                                                                    PickupLocation,
                                                                    DropoffLocation,
                                                                    NightFareApplicable,
                                                                    NightFare,
                                                                    TripFare,
                                                                    WaitingTime,
                                                                    WaitingTimeCost,
                                                                    TollFee,
                                                                    BookingCharge,
                                                                    Tax,
                                                                    PromoCode,
                                                                    Discount,
                                                                    SubTotal,
                                                                    GrandTotal,
                                                                    Status,
                                                                    Reason,
                                                                    PaymentType,
                                                                    AdminAmount,
                                                                    CompanyAmount,
                                                                    PickupLat,
                                                                    PickupLng,
                                                                    DropOffLat,
                                                                    DropOffLon,
                                                                    Model,
                                                                    DriverName,
                                                                    CarDetails_Id,
                                                                    CarDetails_CompanyId,
                                                                    CarDetails_DriverId,
                                                                    CarDetails_VehicleModel,
                                                                    CarDetails_Company,
                                                                    CarDetails_Color,
                                                                    CarDetails_VehicleRegistrationNo,
                                                                    CarDetails_RegistrationCertificate,
                                                                    CarDetails_VehicleInsuranceCertificate,
                                                                    CarDetails_RegistrationCertificateExpire,
                                                                    CarDetails_VehicleInsuranceCertificateExpire,
                                                                    CarDetails_VehicleImage,
                                                                    CarDetails_Description,
                                                                    HistoryType,
                                                                    BookingType,
                                                                    PickupDateTime,
                                                                    DropOffDateTime,
                                                                    DriverMobileNo,
                                                                    CardId,
                                                                    Notes,
                                                                    BabySeat,
                                                                    PickupSuburb,
                                                                    DropoffSuburb
                                                                    ));
                                                        }
                                                        else if (Status1.equalsIgnoreCase("completed") || Status1.equalsIgnoreCase("canceled"))
                                                        {
                                                            String Id = "", PassengerId = "", ModelId = "", DriverId = "", CreatedDate = "", TransactionId = "", PaymentStatus = "";
                                                            String PickupTime = "", DropTime = "", TripDuration = "", TripDistance = "", PickupLocation = "", DropoffLocation = "";
                                                            String NightFareApplicable = "", NightFare = "", TripFare = "", WaitingTime = "", WaitingTimeCost = "", TollFee = "", BookingCharge = "";
                                                            String Tax = "", PromoCode = "", Discount = "", SubTotal = "", GrandTotal = "", Status = "", Reason = "", PaymentType = "", AdminAmount = "";
                                                            String CompanyAmount = "", PickupLat = "", PickupLng = "", DropOffLat = "", DropOffLon = "", Model = "", DriverName = "";
                                                            String CarDetails_Id = "", CarDetails_CompanyId = "", CarDetails_DriverId = "", CarDetails_VehicleModel = "";
                                                            String CarDetails_Company = "", CarDetails_Color = "", CarDetails_VehicleRegistrationNo = "", CarDetails_RegistrationCertificate = "";
                                                            String CarDetails_VehicleInsuranceCertificate = "", CarDetails_RegistrationCertificateExpire = "", CarDetails_VehicleInsuranceCertificateExpire = "";
                                                            String CarDetails_VehicleImage = "", CarDetails_Description = "", HistoryType = "", BookingType = "";
                                                            String PickupDateTime = "", DropOffDateTime = "", DriverMobileNo = "";

                                                            if (jsonObject.has("Id")) {
                                                                Id = jsonObject.getString("Id");
                                                            }

                                                            if (jsonObject.has("PassengerId")) {
                                                                PassengerId = jsonObject.getString("PassengerId");
                                                            }

                                                            if (jsonObject.has("ModelId")) {
                                                                ModelId = jsonObject.getString("ModelId");
                                                            }

                                                            if (jsonObject.has("DriverId")) {
                                                                DriverId = jsonObject.getString("DriverId");
                                                            }

                                                            if (jsonObject.has("CreatedDate")) {
                                                                CreatedDate = jsonObject.getString("CreatedDate");
                                                            }

                                                            if (jsonObject.has("TransactionId")) {
                                                                TransactionId = jsonObject.getString("TransactionId");
                                                            }

                                                            if (jsonObject.has("PaymentStatus")) {
                                                                PaymentStatus = jsonObject.getString("PaymentStatus");
                                                            }

                                                            if (jsonObject.has("PickupTime")) {
                                                                PickupTime = jsonObject.getString("PickupTime");
                                                            }

                                                            if (jsonObject.has("DropTime")) {
                                                                DropTime = jsonObject.getString("DropTime");
                                                            }

                                                            if (jsonObject.has("PickupDateTime") && !jsonObject.getString("PickupDateTime").equals("false")) {
                                                                PickupDateTime = jsonObject.getString("PickupDateTime");
                                                            }

                                                            if (jsonObject.has("DropOffDateTime") && !jsonObject.getString("DropOffDateTime").equals("false")) {
                                                                DropOffDateTime = jsonObject.getString("DropOffDateTime");
                                                            }

                                                            if (jsonObject.has("TripDuration")) {
                                                                TripDuration = jsonObject.getString("TripDuration");
                                                            }

                                                            if (jsonObject.has("TripDistance")) {
                                                                TripDistance = jsonObject.getString("TripDistance");
                                                            }

                                                            if (jsonObject.has("PickupLocation")) {
                                                                PickupLocation = jsonObject.getString("PickupLocation");
                                                            }

                                                            if (jsonObject.has("DropoffLocation")) {
                                                                DropoffLocation = jsonObject.getString("DropoffLocation");
                                                            }

                                                            if (jsonObject.has("NightFareApplicable")) {
                                                                NightFareApplicable = jsonObject.getString("NightFareApplicable");
                                                            }

                                                            if (jsonObject.has("NightFare")) {
                                                                NightFare = jsonObject.getString("NightFare");
                                                            }

                                                            if (jsonObject.has("TripFare")) {
                                                                TripFare = jsonObject.getString("TripFare");
                                                            }

                                                            if (jsonObject.has("WaitingTime")) {
                                                                WaitingTime = jsonObject.getString("WaitingTime");
                                                            }

                                                            if (jsonObject.has("WaitingTimeCost")) {
                                                                WaitingTimeCost = jsonObject.getString("WaitingTimeCost");
                                                            }

                                                            if (jsonObject.has("TollFee")) {
                                                                TollFee = jsonObject.getString("TollFee");
                                                            }

                                                            if (jsonObject.has("BookingCharge")) {
                                                                BookingCharge = jsonObject.getString("BookingCharge");
                                                            }

                                                            if (jsonObject.has("Tax")) {
                                                                Tax = jsonObject.getString("Tax");
                                                            }

                                                            if (jsonObject.has("PromoCode")) {
                                                                PromoCode = jsonObject.getString("PromoCode");
                                                            }

                                                            if (jsonObject.has("Discount")) {
                                                                Discount = jsonObject.getString("Discount");
                                                            }

                                                            if (jsonObject.has("SubTotal")) {
                                                                SubTotal = jsonObject.getString("SubTotal");
                                                            }

                                                            if (jsonObject.has("GrandTotal")) {
                                                                GrandTotal = jsonObject.getString("GrandTotal");
                                                            }

                                                            if (jsonObject.has("Status")) {
                                                                Status = jsonObject.getString("Status");
                                                            }

                                                            if (jsonObject.has("Reason")) {
                                                                Reason = jsonObject.getString("Reason");
                                                            }

                                                            if (jsonObject.has("PaymentType")) {
                                                                PaymentType = jsonObject.getString("PaymentType");
                                                            }

                                                            if (jsonObject.has("AdminAmount")) {
                                                                AdminAmount = jsonObject.getString("AdminAmount");
                                                            }

                                                            if (jsonObject.has("CompanyAmount")) {
                                                                CompanyAmount = jsonObject.getString("CompanyAmount");
                                                            }

                                                            if (jsonObject.has("PickupLat")) {
                                                                PickupLat = jsonObject.getString("PickupLat");
                                                            }

                                                            if (jsonObject.has("PickupLng")) {
                                                                PickupLng = jsonObject.getString("PickupLng");
                                                            }

                                                            if (jsonObject.has("DropOffLat")) {
                                                                DropOffLat = jsonObject.getString("DropOffLat");
                                                            }

                                                            if (jsonObject.has("DropOffLon")) {
                                                                DropOffLon = jsonObject.getString("DropOffLon");
                                                            }

                                                            if (jsonObject.has("Model")) {
                                                                Model = jsonObject.getString("Model");
                                                            }

                                                            if (jsonObject.has("DriverName")) {
                                                                DriverName = jsonObject.getString("DriverName");
                                                            }

                                                            if (jsonObject.has("DriverMobileNo") && jsonObject.getString("DriverMobileNo") != null && !jsonObject.getString("DriverMobileNo").equalsIgnoreCase("NULL") && !jsonObject.getString("DriverMobileNo").equalsIgnoreCase("null")) {
                                                                DriverMobileNo = jsonObject.getString("DriverMobileNo");
                                                            }

                                                            if (jsonObject.has("CarDetails"))
                                                            {
                                                                JSONObject CarDetails = jsonObject.getJSONObject("CarDetails");

                                                                if (CarDetails != null) {
                                                                    if (CarDetails.has("Id")) {
                                                                        CarDetails_Id = CarDetails.getString("Id");
                                                                    }

                                                                    if (CarDetails.has("CompanyId")) {
                                                                        CarDetails_CompanyId = CarDetails.getString("CompanyId");
                                                                    }

                                                                    if (CarDetails.has("VehicleModel")) {
                                                                        CarDetails_VehicleModel = CarDetails.getString("VehicleModel");
                                                                    }

                                                                    if (CarDetails.has("Company")) {
                                                                        CarDetails_Company = CarDetails.getString("Company");
                                                                    }

                                                                    if (CarDetails.has("Color")) {
                                                                        CarDetails_Color = CarDetails.getString("Color");
                                                                    }

                                                                    if (CarDetails.has("VehicleRegistrationNo")) {
                                                                        CarDetails_VehicleRegistrationNo = CarDetails.getString("VehicleRegistrationNo");
                                                                    }

                                                                    if (CarDetails.has("RegistrationCertificate")) {
                                                                        CarDetails_RegistrationCertificate = CarDetails.getString("RegistrationCertificate");
                                                                    }

                                                                    if (CarDetails.has("VehicleInsuranceCertificate")) {
                                                                        CarDetails_VehicleInsuranceCertificate = CarDetails.getString("VehicleInsuranceCertificate");
                                                                    }

                                                                    if (CarDetails.has("RegistrationCertificateExpire")) {
                                                                        CarDetails_RegistrationCertificateExpire = CarDetails.getString("RegistrationCertificateExpire");
                                                                    }

                                                                    if (CarDetails.has("VehicleInsuranceCertificateExpire")) {
                                                                        CarDetails_VehicleInsuranceCertificateExpire = CarDetails.getString("VehicleInsuranceCertificateExpire");
                                                                    }

                                                                    if (CarDetails.has("VehicleImage")) {
                                                                        CarDetails_VehicleImage = WebServiceAPI.BASE_URL_IMAGE + CarDetails.getString("VehicleImage");
                                                                    }

                                                                    if (CarDetails.has("Description")) {
                                                                        CarDetails_Description = CarDetails.getString("Description");
                                                                    }
                                                                }
                                                            }

                                                            if (jsonObject.has("HistoryType")) {
                                                                HistoryType = jsonObject.getString("HistoryType");
                                                            }

                                                            if (jsonObject.has("BookingType")) {
                                                                BookingType = jsonObject.getString("BookingType");
                                                            }

                                                            pastBooking_beens.add(new PastBooking_Been(Id,
                                                                    PassengerId,
                                                                    ModelId,
                                                                    DriverId,
                                                                    CreatedDate,
                                                                    TransactionId,
                                                                    PaymentStatus,
                                                                    PickupTime,
                                                                    DropTime,
                                                                    TripDuration,
                                                                    TripDistance,
                                                                    PickupLocation,
                                                                    DropoffLocation,
                                                                    NightFareApplicable,
                                                                    NightFare,
                                                                    TripFare,
                                                                    WaitingTime,
                                                                    WaitingTimeCost,
                                                                    TollFee,
                                                                    BookingCharge,
                                                                    Tax,
                                                                    PromoCode,
                                                                    Discount,
                                                                    SubTotal,
                                                                    GrandTotal,
                                                                    Status,
                                                                    Reason,
                                                                    PaymentType,
                                                                    AdminAmount,
                                                                    CompanyAmount,
                                                                    PickupLat,
                                                                    PickupLng,
                                                                    DropOffLat,
                                                                    DropOffLon,
                                                                    Model,
                                                                    DriverName,
                                                                    CarDetails_Id,
                                                                    CarDetails_CompanyId,
                                                                    CarDetails_DriverId,
                                                                    CarDetails_VehicleModel,
                                                                    CarDetails_Company,
                                                                    CarDetails_Color,
                                                                    CarDetails_VehicleRegistrationNo,
                                                                    CarDetails_RegistrationCertificate,
                                                                    CarDetails_VehicleInsuranceCertificate,
                                                                    CarDetails_RegistrationCertificateExpire,
                                                                    CarDetails_VehicleInsuranceCertificateExpire,
                                                                    CarDetails_VehicleImage,
                                                                    CarDetails_Description,
                                                                    HistoryType,
                                                                    BookingType,
                                                                    PickupDateTime,
                                                                    DropOffDateTime,
                                                                    DriverMobileNo));
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                Log.e("call", "jsonObject null position = " + i);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Log.e("call", "history null or lenth 0");
                                    }

                                    Log.e("call", "onGoing_beens.size() = " + onGoing_beens.size());
                                    Log.e("call", "upComming_beens.size() = " + upComming_beens.size());
                                    Log.e("call", "pastBooking_beens.size() = " + pastBooking_beens.size());

                                    dialogClass.hideDialog();

                                    if (notificationType != null && notificationType.equalsIgnoreCase("AcceptBooking")) {
                                        call_UpComming();
                                    } else if (notificationType != null && notificationType.equalsIgnoreCase("OnTheWay")) {
                                        call_UpComming();
                                    } else if (notificationType != null && notificationType.equalsIgnoreCase("CancelledBooking")) {
                                        call_UpComming();
                                    } else {
                                        call_PastBooking();
                                    }
                                } else {
                                    Log.e("call", "history not found");
                                    dialogClass.hideDialog();
                                    call_PastBooking();
                                }
                            } else {
                                Log.e("call", "status false");
                                dialogClass.hideDialog();
                                call_PastBooking();
                            }
                        } else {
                            Log.e("call", "status not found");
                            dialogClass.hideDialog();
                            call_PastBooking();
                        }
                    } else {
                        Log.e("call", "json null");
                        dialogClass.hideDialog();
                        call_PastBooking();
                    }
                } catch (Exception e) {
                    Log.e("Exception", "Exception " + e.toString());
                    dialogClass.hideDialog();
                    call_PastBooking();
                }
            }

        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    @Override
    protected void onResume() {
        super.onResume();

        TicktocApplication.setCurrentActivity(activity);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 0:
                List<Fragment> fragments = getSupportFragmentManager().getFragments();
                if (fragments != null) {
                    for (Fragment fragment : fragments) {
                        if (fragment instanceof UpComming_Fragment) {
                            fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
                        }
                    }
                }
                break;
        }
    }
}

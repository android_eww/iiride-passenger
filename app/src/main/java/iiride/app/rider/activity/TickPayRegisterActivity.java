package iiride.app.rider.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.IsoDep;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.github.devnied.emvnfccard.model.EmvCard;
import com.github.devnied.emvnfccard.parser.EmvParser;
import com.github.devnied.emvnfccard.utils.AtrUtils;

import iiride.app.rider.R;
import iiride.app.rider.application.TicktocApplication;
import iiride.app.rider.been.CreditCard_List_Been;
import iiride.app.rider.comman.Common;
import iiride.app.rider.comman.SessionSave;
import iiride.app.rider.comman.WebServiceAPI;
import iiride.app.rider.nfc.IContentActivity;
import iiride.app.rider.nfc.IRefreshable;
import iiride.app.rider.nfc.NFCUtils;
import iiride.app.rider.nfc.Provider;
import iiride.app.rider.nfc.SimpleAsyncTask;
import iiride.app.rider.other.DialogClass;
import iiride.app.rider.other.Global;
import iiride.app.rider.other.InternetDialog;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import fr.devnied.bitlib.BytesUtils;
import io.card.payment.CardIOActivity;
import io.card.payment.CardType;
import io.card.payment.CreditCard;

public class TickPayRegisterActivity extends AppCompatActivity implements View.OnClickListener, IContentActivity {

    public static TickPayRegisterActivity activity;
    private LinearLayout ll_Back,ll_DrivingLicence,ll_CardLayout,ll_Passport;
    private ImageView iv_Back,iv_DrivingLicence,iv_Passport;
    private TextView tv_Title;
    private EditText et_card_number,et_cvv,et_Alias,et_description;
    private TextView ed_expire_date;
    private String expiryYear = "";
    private String expiryMonth = "";
    private String expiryDate = "";

    private TextView tv_pay_now;
    private LinearLayout scanCard;
    private ImageView iv_close;
    private RelativeLayout rl_gif;

    private NFCUtils mNfcUtils;
    private ProgressDialog mDialog;
    private AlertDialog mAlertDialog;
    private Provider mProvider = new Provider();
    private EmvCard mReadCard;
    private WeakReference<IRefreshable> mRefreshableContent;
    private byte[] lastAts;

    private DialogClass dialogClass;
    private AQuery aQuery;
    private EditText et_Name,et_Abn;
    boolean validornot = false;

    private static final int REQUEST_SCAN = 100;
    private static final int REQUEST_AUTOTEST =  200;
    Uri mImageUri;
    String picturePath;
    private static int RESULT_LOAD_IMAGE = 1;
    static Uri uriGallery;
    static Uri uriGalleryPassport;

    ///for Bottom Dialog
    private BottomSheetDialog mBottomSheetDialog;
    private NumberPicker numberPickerMonth,numberPickerYear;
    private String[] valueMonth ;
    private String[] valueYear ;
    private String strExpiry = "";
    int MonthInNumber, YearInNumber ;
    private String[] monthName = {
            "Jan", "Feb", "Mar",
            "Apr", "May", "Jun",
            "Jul", "Aug", "Sep",
            "Oct", "Nov", "Dec"
    };

    private String filename="";
    private Uri imageUri;
    private SharedPreferences permissionStatus;
    public static Uri awsUri;
    public static Bitmap bmp = null;
    public static Bitmap bmpPassport = null;
    private boolean sentToSettings = false;
    public static byte[] byteArrayImage = null;
    public static byte[] byteArrayImagePassport = null;

    public static boolean isCardRequired= true;

    public static int resumeFlag = 1;
    public static boolean isScan = false;
    private String cardNumberParam = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tic_pay_register);

        activity = TickPayRegisterActivity.this;
        isCardRequired= true;
        mNfcUtils = new NFCUtils(this);
        expiryYear = "";
        expiryMonth = "";
        expiryDate = "";
        bmp = null;
        bmpPassport = null;
        resumeFlag = 1;
        isScan = false;
        cardNumberParam = "";
        byteArrayImage = null;
        byteArrayImagePassport = null;
        permissionStatus = getSharedPreferences("permissionStatus",MODE_PRIVATE);
        dialogClass = new DialogClass(activity,0);
        aQuery = new AQuery(activity);

        init();
    }

    private void init() {

        ll_Back = (LinearLayout) findViewById(R.id.back_layout);
        iv_Back = (ImageView) findViewById(R.id.back_imageview);

        scanCard = (LinearLayout) findViewById(R.id.card_scan);
        iv_close = (ImageView) findViewById(R.id.iv_close);
        rl_gif = (RelativeLayout)findViewById(R.id.rl_Gif);

        ll_CardLayout = (LinearLayout) findViewById(R.id.cardLayout);

        ll_DrivingLicence = (LinearLayout) findViewById(R.id.layout_driving_licence);
        ll_Passport = (LinearLayout) findViewById(R.id.layout_passport);

        iv_DrivingLicence = (ImageView) findViewById(R.id.image_driving_licence);
        iv_Passport = (ImageView) findViewById(R.id.image_passport);

        tv_Title = (TextView) findViewById(R.id.title_textview);
        et_Name = (EditText)findViewById(R.id.et_name);
        et_Abn = (EditText)findViewById(R.id.et_abn);
        et_Alias = (EditText)findViewById(R.id.et_alias);
        et_description = (EditText)findViewById(R.id.et_description);

        iv_DrivingLicence.setImageResource(R.drawable.ic_camera_black);
        iv_Passport.setImageResource(R.drawable.ic_camera_black);
        tv_Title.setText("Tick Pay");
        et_card_number = (EditText)findViewById(R.id.et_card_number);
        ed_expire_date = (TextView)findViewById(R.id.et_expire_date);
        et_cvv = (EditText)findViewById(R.id.et_cvv);
        tv_pay_now = (TextView) findViewById(R.id.tv_pay_now);

        ll_Back.setOnClickListener(activity);
        iv_Back.setOnClickListener(activity);
        tv_pay_now.setOnClickListener(activity);
        scanCard.setOnClickListener(activity);
        ed_expire_date.setOnClickListener(activity);
        ll_DrivingLicence.setOnClickListener(activity);
        iv_DrivingLicence.setOnClickListener(activity);
        ll_Passport.setOnClickListener(activity);
        iv_Passport.setOnClickListener(activity);

        et_card_number.addTextChangedListener(new FourDigitCardFormatWatcher());

        iv_close.setOnClickListener(activity);

        et_card_number.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                isScan = false;

                return false;
            }
        });

        startNFC();

        if (checkCardList())
        {
            isCardRequired= false;
            ll_CardLayout.setVisibility(View.GONE);
        }
        else
        {
            isCardRequired= true;
            ll_CardLayout.setVisibility(View.VISIBLE);
        }
    }


    public void showDialogForScanningFailed(String message)
    {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.my_dialog_class);

        TextView tv_Ok = (TextView) dialog.findViewById(R.id.Ok);
        TextView tv_Message = (TextView) dialog.findViewById(R.id.message);
        LinearLayout ll_Ok = (LinearLayout) dialog.findViewById(R.id.ok_layout);

        tv_Message.setText(message);

        ll_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                startNFC();
            }
        });

        tv_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                startNFC();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        dialog.show();
    }

    public boolean checkCardList()
    {
        try
        {
            Log.e("call","111111111111111");
            String strCardList = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_CARD_LIST,activity);
            List<CreditCard_List_Been> cardListBeens = new ArrayList<CreditCard_List_Been>();

            if (strCardList!=null && !strCardList.equalsIgnoreCase(""))
            {
                Log.e("call","222222222222222");
                JSONObject json = new JSONObject(strCardList);

                if (json!=null)
                {
                    Log.e("call","33333333333333");
                    if (json.has("cards"))
                    {
                        Log.e("call","6666666666666666");
                        JSONArray cards = json.getJSONArray("cards");

                        if (cards!=null && cards.length()>0)
                        {
                            return true;
                        }
                        else
                        {
                            Log.e("call","no cards available");
                            return false;
                        }
                    }
                    else
                    {
                        Log.e("call","no cards found");
                        return false;
                    }
                }
                else
                {
                    Log.e("call","json null");
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        catch (Exception e)
        {
            Log.e("call","Exception in getting card list = "+e.getMessage());
            return false;
        }
    }

    private void checkNFC() {

        Log.e("call","checkNFC()");
        mNfcUtils.enableDispatch();
        // Close
        if (mAlertDialog != null && mAlertDialog.isShowing()) {
            mAlertDialog.cancel();
        }

        // Check if NFC is available
        if (!NFCUtils.isNfcAvailable(getApplicationContext())) {

            Log.e("call","checkNFC() if");

            AlertDialog.Builder alertbox = new AlertDialog.Builder(this);
            alertbox.setTitle(getString(R.string.msg_info));
            alertbox.setMessage(getString(R.string.msg_nfc_not_available));
            alertbox.setPositiveButton(getString(R.string.msg_ok), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(final DialogInterface dialog, final int which) {
                    dialog.dismiss();
                }
            });
            alertbox.setCancelable(false);
            mAlertDialog = alertbox.show();
        }
        else
        {
            Log.e("call","checkNFC() else");
            cardScanNFC();
        }
    }

    private void cardScanNFC() {
        Log.e("call","cardScanNFC()");
        onNewIntent(this.getIntent());
    }

    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.back_layout:
                onBackPressed();
                break;

            case R.id.et_expire_date:
                showBottomSheetDailog_payment();
                break;

            case R.id.back_imageview:
                onBackPressed();
                break;

            case R.id.layout_driving_licence:
                takePermission(1);
                break;

            case R.id.image_driving_licence:
                takePermission(1);
                break;

            case R.id.layout_passport:
                takePermission(2);
                break;

            case R.id.image_passport:
                takePermission(2);
                break;

            case R.id.tv_pay_now:

                if (isCardRequired)
                {
                    if (et_Name.getText().toString().trim().equalsIgnoreCase(""))
                    {
                        InternetDialog internetDialog = new InternetDialog(activity);
                        internetDialog.showDialog("Please enter Company name / Name.");
                    }
                    else if (et_description.getText().toString().trim().equalsIgnoreCase(""))
                    {
                        InternetDialog internetDialog = new InternetDialog(activity);
                        internetDialog.showDialog("Please enter Description.");
                    }
                    else if (byteArrayImage==null)
                    {
                        InternetDialog internetDialog = new InternetDialog(activity);
                        internetDialog.showDialog("Please select Drivers license image.");
                    }
                    else if (byteArrayImagePassport==null)
                    {
                        InternetDialog internetDialog = new InternetDialog(activity);
                        internetDialog.showDialog("Please select passport image.");
                    }
                    else if (et_card_number.getText().toString().trim().equalsIgnoreCase(""))
                    {
                        InternetDialog internetDialog = new InternetDialog(activity);
                        internetDialog.showDialog("Please enter card number.");
                    }
                    else if (expiryDate.equalsIgnoreCase(""))
                    {
                        InternetDialog internetDialog = new InternetDialog(activity);
                        internetDialog.showDialog("Please enter expiry date.");
                    }
                    else if (et_cvv.getText().toString().trim().equalsIgnoreCase(""))
                    {
                        InternetDialog internetDialog = new InternetDialog(activity);
                        internetDialog.showDialog("Please enter CVV.");
                    }
                    else
                    {
                        if(et_card_number.getText().length()>11)
                        {
                            if (validornot)
                            {
                                if (Global.isNetworkconn(activity))
                                {
                                    payNow();
                                }
                                else
                                {
                                    InternetDialog internetDialog = new InternetDialog(activity);
                                    internetDialog.showDialog("Please check your internet connection!");
                                }
                            }
                            else
                            {
                                Log.e("call","1111111111111111");
                                InternetDialog internetDialog = new InternetDialog(activity);
                                internetDialog.showDialog(getString(R.string.invalid_card_number));
                            }
                        }
                        else
                        {
                            Log.e("call","22222222222222222");
                            InternetDialog internetDialog = new InternetDialog(activity);
                            internetDialog.showDialog(getString(R.string.invalid_card_number));
                        }
                    }
                }
                else
                {
                    if (et_Name.getText().toString().trim().equalsIgnoreCase(""))
                    {
                        InternetDialog internetDialog = new InternetDialog(activity);
                        internetDialog.showDialog("Please enter Company name / Name.");
                    }
                    else if (et_description.getText().toString().trim().equalsIgnoreCase(""))
                    {
                        InternetDialog internetDialog = new InternetDialog(activity);
                        internetDialog.showDialog("Please enter Description.");
                    }
                    else if (byteArrayImage==null)
                    {
                        InternetDialog internetDialog = new InternetDialog(activity);
                        internetDialog.showDialog("Please select Drivers license image.");
                    }
                    else if (byteArrayImagePassport==null)
                    {
                        InternetDialog internetDialog = new InternetDialog(activity);
                        internetDialog.showDialog("Please select passport image.");
                    }
                    else
                    {
                        if (Global.isNetworkconn(activity))
                        {
                            payNow();
                        }
                        else
                        {
                            InternetDialog internetDialog = new InternetDialog(activity);
                            internetDialog.showDialog("Please check your internet connection!");
                        }
                    }
                }

                Log.e("call","cardNumber = "+cardNumberParam);
                Log.e("call","expiryDate = "+expiryDate);

                break;

            case R.id.card_scan:
                cardNumberParam = "";
                expiryDate = "";
                et_card_number.setText("");
                ed_expire_date.setText("");
                et_cvv.setText("");
                et_Alias.setText("");
                scanCard();
                break;

            case R.id.iv_close:
                rl_gif.setVisibility(View.GONE);
                break;
        }
    }

    public void startNFC()
    {
        cardNumberParam = "";
        expiryDate = "";
        et_card_number.setText("");
        ed_expire_date.setText("");
        et_cvv.setText("");
        et_Alias.setText("");

        Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                checkNFC();

            }
        };
        handler.postDelayed(runnable,1000);
    }

    private void payNow()
    {
        //PassengerId,CardNo,Cvv,Expiry,Alias,CompanyName,ABN(optional),Image
        dialogClass.showDialog();
        String url = WebServiceAPI.API_VERIFY_USER;

        Map<String, Object> params = new HashMap<String, Object>();
        params.put(WebServiceAPI.PARAM_PASSENGER_ID, SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID,activity));
        params.put(WebServiceAPI.PARAM_COMPANY_NAME, et_Name.getText().toString());
        params.put(WebServiceAPI.PARAM_COMPANY_NAME, et_description.getText().toString());
        params.put(WebServiceAPI.PARAM_ABN, et_Abn.getText().toString());
        if (isCardRequired)
        {
            if (isScan==true)
            {
                params.put(WebServiceAPI.PARAM_CARD_NUMBER, cardNumberParam);
            }
            else
            {
                params.put(WebServiceAPI.PARAM_CARD_NUMBER, et_card_number.getText().toString().replace(" ",""));
            }

            params.put(WebServiceAPI.PARAM_CARD_NUMBER, cardNumberParam);//et_card_number.getText().toString().replace(" ","")
            params.put(WebServiceAPI.PARAM_CARD_EXPIRY, expiryDate);
            params.put(WebServiceAPI.PARAM_CARD_CVV, et_cvv.getText().toString());
            params.put(WebServiceAPI.PARAM_ALIAS, et_Alias.getText().toString());
        }
        params.put(WebServiceAPI.PARAM_IMAGE, byteArrayImage);
        params.put(WebServiceAPI.PARAM_PASSPORT, byteArrayImagePassport);

        Log.e("call", "url = " + url);
        Log.e("call", "params = " + params);

        aQuery.ajax(url.trim(), params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject response, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e("responseCode", " = " + responseCode);
                    Log.e("Response", " = " + response);

                    if (response!=null)
                    {
                        if (response.has("status"))
                        {
                            if (response.getBoolean("status"))
                            {
                                if (isCardRequired)
                                {
                                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_CARD_LIST,response.toString(),activity);
                                }
                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_COMPANY_NAME,et_Name.getText().toString(),activity);
                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_ABN,et_Abn.getText().toString(),activity);
                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_DESCRIPTION,et_description.getText().toString(),activity);
                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_VERIFY_USER,"1",activity);
                                dialogClass.hideDialog();
                                String message = "Successfully sign up for TICKPAY!";

                                if (response.has("message"))
                                {
                                    message = response.getString("message");

                                }
                                showSuccessDialog(message);
                            }
                            else
                            {
                                dialogClass.hideDialog();
                                String message = "Something went wrong please try again later!";
                                if (response.has("message"))
                                {
                                    message = response.getString("message");

                                }
                                InternetDialog internetDialog = new InternetDialog(activity);
                                internetDialog.showDialog(message);
                            }
                        }
                        else
                        {
                            dialogClass.hideDialog();
                            String message = "Something went wrong please try again later!";
                            InternetDialog internetDialog = new InternetDialog(activity);
                            internetDialog.showDialog(message);
                        }
                    }
                    else
                    {
                        dialogClass.hideDialog();
                        String message = "Something went wrong please try again later!";
                        InternetDialog internetDialog = new InternetDialog(activity);
                        internetDialog.showDialog(message);
                    }
                }
                catch (Exception e)
                {
                    Log.e("Exception","Exception "+e.toString());
                    dialogClass.hideDialog();
                    InternetDialog internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog("Something went wrong prlease try again later");
                }
            }

        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    public void showSuccessDialog(String message)
    {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.my_dialog_class);

        TextView tv_Ok = (TextView) dialog.findViewById(R.id.Ok);
        TextView tv_Message = (TextView) dialog.findViewById(R.id.message);
        LinearLayout ll_Ok = (LinearLayout) dialog.findViewById(R.id.ok_layout);

        tv_Message.setText(message);

        ll_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                finish();
            }
        });

        tv_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                finish();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        dialog.show();
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mNfcUtils.disableDispatch();
    }

    @Override
    protected void onNewIntent(final Intent intent)
    {
        super.onNewIntent(intent);
        final Tag mTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
        isScan = true;
        if (mTag != null) {

            new SimpleAsyncTask() {

                /**
                 * Tag comm
                 */
                private IsoDep mTagcomm;

                /**
                 * Emv Card
                 */
                private EmvCard mCard;

                /**
                 * Boolean to indicate exception
                 */
                private boolean mException;

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();

//                    backToHomeScreen();
                    mProvider.getLog().setLength(0);
                    // Show dialog
                    if (mDialog == null) {
                        mDialog = ProgressDialog.show(TickPayRegisterActivity.this, getString(R.string.card_reading),
                                getString(R.string.card_reading_desc), true, false);
                    } else {
                        mDialog.show();
                    }
                }

                @Override
                protected void doInBackground() {

                    mTagcomm = IsoDep.get(mTag);
                    if (mTagcomm == null)
                    {
//                        Toast.makeText(getApplicationContext(),R.string.error_communication_nfc,Toast.LENGTH_SHORT).show();
                        showDialogForScanningFailed(getResources().getString(R.string.error_communication_nfc));
                        return;
                    }
                    mException = false;

                    try {
                        mReadCard = null;
                        // Open connection
                        mTagcomm.connect();
                        lastAts = getAts(mTagcomm);

                        mProvider.setmTagCom(mTagcomm);

                        EmvParser parser = new EmvParser(mProvider, true);
                        mCard = parser.readEmvCard();
                        if (mCard != null) {
                            mCard.setAtrDescription(extractAtsDescription(lastAts));
                        }

                    } catch (IOException e) {
                        mException = true;
                    } finally {
                        // close tagcomm
                        IOUtils.closeQuietly(mTagcomm);
                    }
                }

                @Override
                protected void onPostExecute(final Object result) {
                    // close dialog
                    if (mDialog != null) {
                        mDialog.cancel();
                    }

                    if (!mException) {
                        if (mCard != null) {
                            if (StringUtils.isNotBlank(mCard.getCardNumber())) {
//                                Toast.makeText(getApplicationContext(),R.string.card_read,Toast.LENGTH_SHORT).show();
                                mReadCard = mCard;

                                String str = mReadCard.getExpireDate().toString();
                                String[] splited = str.trim().split("\\s+");
                                String month = splited[1];
                                String year = splited[splited.length-1];

                                Log.e("HomeActivity" ,
                                        "Card Number:- " + mReadCard.getCardNumber() +
                                                "\nCard Exp:- " + mReadCard.getExpireDate() +
                                                "\nCard Month:- " + month +
                                                "\nCard Year:- " + year +
                                                "\nCard Type:- " + mReadCard.getType() +
                                                "\nCard First Name:- " + mReadCard.getHolderFirstname() +
                                                "\nCard LastName:- " + mReadCard.getHolderLastname() +
                                                "\nCard Application:- " + mReadCard.getApplicationLabel() +
                                                "\nCard Aid:- " + mReadCard.getAid() +
                                                "\nCard LeftPinTrsy:- " + mReadCard.getLeftPinTry() +
                                                "\nCard Service:- " + mReadCard.getService());

                                if (mReadCard.getCardNumber()!=null)
                                {
                                    try
                                    {
                                        Calendar cal = Calendar.getInstance();
                                        cal.setTime(new SimpleDateFormat("MMM").parse(month));
                                        int monthInt = cal.get(Calendar.MONTH) + 1;

                                        if (monthInt<10)
                                        {
                                            expiryMonth = "0" + monthInt;
                                        }
                                        else
                                        {
                                            expiryMonth = monthInt + "";
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        expiryMonth = "";
                                        Log.e("call","exception = "+e.getMessage());
                                    }
                                }

                                cardNumberParam = mReadCard.getCardNumber();


                                String strCard = "";

                                if (mReadCard.getCardNumber().length()>4)
                                {
                                    cardNumberParam = mReadCard.getCardNumber().replace(" ","");
                                    strCard = getFormattedCardNumber(cardNumberParam);
                                }
                                else
                                {
                                    cardNumberParam = "";
                                }

                                et_card_number.setText(strCard);

                                if (year.length()>2)
                                {
                                    expiryYear = year.substring(year.length() - 2);
                                }

                                if ((expiryMonth!=null && !expiryMonth.equalsIgnoreCase("")) && (expiryYear!=null && !expiryYear.equalsIgnoreCase("")))
                                {
                                    expiryDate = expiryMonth +"/"+expiryYear;
                                }
                                else
                                {
                                    expiryDate = "";
                                }

                                Log.e("call","expiryDate = "+expiryDate);
                                ed_expire_date.setText(month + ", " + year);

                                rl_gif.setVisibility(View.GONE);

                            }
                            else if (mCard.isNfcLocked())
                            {
                                Toast.makeText(getApplicationContext(),R.string.nfc_locked,Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            showDialogForScanningFailed(getResources().getString(R.string.error_communication_nfc));
//                            Toast.makeText(getApplicationContext(),R.string.error_card_unknown,Toast.LENGTH_SHORT).show();
                        }
                    }
                    else
                    {
                        showDialogForScanningFailed(getResources().getString(R.string.error_communication_nfc));
//                        Toast.makeText(getApplicationContext(),R.string.error_communication_nfc,Toast.LENGTH_SHORT).show();
                    }

                    refreshContent();
                }

            }.execute();
        }
        else
        {
            Log.e("call","mTag null 11111111111 ");
        }
    }

    public String getFormattedCardNumber(String input)
    {
        Log.e("call","input input input = "+input);
        String cardNumber = "";
        String strCardFirst = input.substring(0,(input.length()-4));
        String strCardEnd = input.substring(input.length()-4);

        Log.e("call","strCardFirst = "+strCardFirst);
        Log.e("call","strCardEnd = "+strCardEnd);

        String displayCard = "";
        for (int i=0; i<strCardFirst.length(); i++)
        {
            displayCard = displayCard + "*";
        }

        displayCard = displayCard + strCardEnd;



        for (int i=0; i<displayCard.length(); i++)
        {
            if (i>3 && (i%4)==0)
            {
                cardNumber = cardNumber  + " " + displayCard.substring(i,i+1);
            }
            else
            {
                cardNumber = cardNumber  + displayCard.substring(i,i+1);
            }
        }

        return cardNumber;
    }

    /**
     * Method used to get description from ATS
     *
     * @param pAts
     *            ATS byte
     */
    public Collection<String> extractAtsDescription(final byte[] pAts) {
        return AtrUtils.getDescriptionFromAts(BytesUtils.bytesToString(pAts));
    }

    private void refreshContent() {
        if (mRefreshableContent != null && mRefreshableContent.get() != null) {
            mRefreshableContent.get().update();
        }
    }



    /**
     * Get ATS from isoDep
     *
     * @param pIso
     *            isodep
     * @return ATS byte array
     */
    private byte[] getAts(final IsoDep pIso)
    {
        byte[] ret = null;
        if (pIso.isConnected()) {
            // Extract ATS from NFC-A
            ret = pIso.getHistoricalBytes();
            if (ret == null) {
                // Extract ATS from NFC-B
                ret = pIso.getHiLayerResponse();
            }
        }
        return ret;
    }



    @Override
    public StringBuffer getLog() {
        return mProvider.getLog();
    }

    @Override
    public EmvCard getCard() {
        return mReadCard;
    }



    @Override
    public void setRefreshableContent(final IRefreshable pRefreshable) {
        mRefreshableContent = new WeakReference<IRefreshable>(pRefreshable);
    }

    /**
     * Method used to clear data
     */
    public void clear() {
        mReadCard = null;
        mProvider.getLog().setLength(0);
        IRefreshable content = mRefreshableContent.get();
        if (content != null) {
            content.update();
        }
    }

    /**
     * Get the last ATS
     *
     * @return the last card ATS
     */
    public byte[] getLastAts() {
        return lastAts;
    }


    public void showBottomSheetDailog_payment() {

        View view = getLayoutInflater().inflate(R.layout.bottom_sheet_payment, null);
        mBottomSheetDialog = new BottomSheetDialog(activity);
        mBottomSheetDialog.setContentView(view);


        final TextView tv_Ok, tv_Cancle;

        tv_Ok = (TextView) view.findViewById(R.id.bottom_sheet_payment_ok_textview);
        tv_Cancle = (TextView) view.findViewById(R.id.bottom_sheet_payment_cancle_textview);
        numberPickerMonth = (NumberPicker) view.findViewById(R.id.numberPickerMonth);
        numberPickerYear = (NumberPicker) view.findViewById(R.id.numberPickerYear);

        numberPickerMonth.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        numberPickerYear.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

        setDividerColor(numberPickerMonth, ContextCompat.getColor(activity, R.color.colorRed));
        setDividerColor(numberPickerYear,ContextCompat.getColor(activity, R.color.colorRed));

        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH)+1;

        Log.e("Date","year"+year+"month"+month);

        numberPickerMonth.setMinValue(1);
        numberPickerMonth.setMaxValue(12);
        numberPickerYear.setMinValue(year);
        numberPickerYear.setMaxValue(year+5);

        valueMonth = new String[12];
        valueYear = new String[6];

        for(int i=0; i<12; i++)
        {
            valueMonth[i] = monthName[i]+"";
            Log.d("valueNumber","valueMonth : "+ valueMonth[i]);
        }

        for(int i=0; i<6; i++)
        {
            valueYear[i] = (year+i)+"";
            Log.d("valueNumber","valueYear : "+ valueYear[i]);
        }

        numberPickerMonth.setValue(month);
        numberPickerMonth.setDisplayedValues(monthName);
        numberPickerYear.setValue(year);


        tv_Cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mBottomSheetDialog!=null)
                {
                    mBottomSheetDialog.dismiss();
                }
            }
        });

        tv_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBottomSheetDialog!=null)
                {
                    MonthInNumber = numberPickerMonth.getValue();
                    YearInNumber = numberPickerYear.getValue();

                    String strYear = YearInNumber + "";

                    if (strYear.length()>2)
                    {
                        strYear = strYear.substring(strYear.length() - 2);
                    }

                    if (MonthInNumber < 10)
                    {
                        strExpiry = "0" + MonthInNumber + "/" + strYear;
                    }
                    else
                    {
                        strExpiry = MonthInNumber + "/" + strYear;
                    }

                    expiryDate = strExpiry;
                    Log.e("strExpiry"," = "+strExpiry);
                    Log.e("MonthInNumber"," = "+MonthInNumber);
                    Log.e("YearInNumber"," = "+strYear);

                    ed_expire_date.setText(monthName[MonthInNumber-1]);
                    ed_expire_date.append(", "+YearInNumber);

                    mBottomSheetDialog.dismiss();
                }
            }
        });

        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mBottomSheetDialog = null;
            }
        });

        mBottomSheetDialog.show();
    }

    private void setDividerColor(NumberPicker picker, int color) {

        java.lang.reflect.Field[] pickerFields = NumberPicker.class.getDeclaredFields();
        for (java.lang.reflect.Field pf : pickerFields) {
            if (pf.getName().equals("mSelectionDivider")) {
                pf.setAccessible(true);
                try {
                    ColorDrawable colorDrawable = new ColorDrawable(color);
                    pf.set(picker, colorDrawable);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                }
                catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    public class FourDigitCardFormatWatcher implements TextWatcher {

        // Change this to what you want... ' ', '-' etc..
        private static final char space = ' ';

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            // Remove spacing char
            if (s.length() > 0 && (s.length() % 5) == 0) {
                final char c = s.charAt(s.length() - 1);
                if (space == c) {
                    s.delete(s.length() - 1, s.length());
                }
            }
            // Insert char where needed.
            if (s.length() > 0 && (s.length() % 5) == 0) {
                char c = s.charAt(s.length() - 1);
                // Only if its a digit where there should be a space we insert a space
                if (Character.isDigit(c) && TextUtils.split(s.toString(), String.valueOf(space)).length <= 3) {
                    s.insert(s.length() - 1, String.valueOf(space));
                }
            }
            FindCardType(s);
        }
    }

    public void FindCardType(CharSequence s) {

        String valid = "INVALID";

        if (isScan == true)
        {
            s = cardNumberParam;
        }

        if (s.toString().length() > 5) {
            String number = s.toString().replace(" ", "");
            String digit1 = number.substring(0, 1);
            String digit2 = number.substring(0, 2);
            String digit3 = number.substring(0, 3);
            String digit4 = number.substring(0, 4);

//            iv_CardImage.setBackgroundResource(R.drawable.card_dummy);
            et_card_number.setTextColor(ContextCompat.getColor(activity, R.color.colorBlack));

            if (digit1.equals("4")) {
//                iv_CardImage.setBackgroundResource(R.drawable.card_visa);
                if (number.length() == 13 || number.length() == 16) {
                    valid = "VISA";
                    validornot = validCCNumber(number);

                    if (number.length() > 12) {
                        if (validornot) {
                            et_card_number.setTextColor(ContextCompat.getColor(activity, R.color.colorGreen));
//                            iv_CardImage.setBackgroundResource(R.drawable.card_visa);
                        } else {
                            et_card_number.setTextColor(ContextCompat.getColor(activity, R.color.colorRed));
//                            mySnackBar.showSnackBar(main_layout,getString(R.string.card_is_not_valid));
//                            iv_CardImage.setBackgroundResource(R.drawable.card_dummy);
                        }
                    }
                }
            } else if (digit4.equalsIgnoreCase("5018") || digit4.equalsIgnoreCase("5020") || digit4.equalsIgnoreCase("5038") || digit4.equalsIgnoreCase("5612") || digit4.equalsIgnoreCase("5893")
                    || digit4.equalsIgnoreCase("6304") || digit4.equalsIgnoreCase("6759") || digit4.equalsIgnoreCase("6761") || digit4.equalsIgnoreCase("6762") || digit4.equalsIgnoreCase("6763")
                    || digit4.equalsIgnoreCase("0604") || digit4.equalsIgnoreCase("6390")) {
//                iv_CardImage.setBackgroundResource(R.drawable.card_maestro);
                if (number.length() == 16) {
                    valid = "MAESTRO";
                    Log.e("MAESTRO", "MAESTRO");
                    validornot = validCCNumber(number);

                    if (number.length() == 16) {
                        if (validornot) {
                            et_card_number.setTextColor(ContextCompat.getColor(activity, R.color.colorGreen));
//                            iv_CardImage.setBackgroundResource(R.drawable.card_maestro);

                        } else {
                            et_card_number.setTextColor(ContextCompat.getColor(activity, R.color.colorRed));
//                            mySnackBar.showSnackBar(main_layout,getString(R.string.card_is_not_valid));
//                            iv_CardImage.setBackgroundResource(R.drawable.card_dummy);
                        }
                    }
                }
            } else if (digit2.equals("34") || digit2.equals("37")) {
//                iv_CardImage.setBackgroundResource(R.drawable.card_american);
                if (number.length() == 15) {
                    valid = "AMERICAN_EXPRESS";
                    validornot = validCCNumber(number);

                    if (number.length() == 15) {
                        if (validornot) {
                            et_card_number.setTextColor(ContextCompat.getColor(activity, R.color.colorGreen));
//                            iv_CardImage.setBackgroundResource(R.drawable.card_american);

                        } else {
                            et_card_number.setTextColor(ContextCompat.getColor(activity, R.color.colorRed));
//                            mySnackBar.showSnackBar(main_layout,getString(R.string.card_is_not_valid));
//                            iv_CardImage.setBackgroundResource(R.drawable.card_dummy);
                        }
                    }
                }
            } else if (digit2.equals("36") || digit2.equals("38") || (digit3.compareTo("300") >= 0 && digit3.compareTo("305") <= 0)) {
//                iv_CardImage.setBackgroundResource(R.drawable.card_dinner);
                if (number.length() == 14) {
                    valid = "DINERS_CLUB";
                    validornot = validCCNumber(number);

                    if (number.length() == 14) {
                        if (validornot) {
//                            iv_CardImage.setBackgroundResource(R.drawable.card_dinner);
                            et_card_number.setTextColor(ContextCompat.getColor(activity, R.color.colorGreen));

                        } else {
                            et_card_number.setTextColor(ContextCompat.getColor(activity, R.color.colorRed));
//                            mySnackBar.showSnackBar(main_layout,getString(R.string.card_is_not_valid));
//                            iv_CardImage.setBackgroundResource(R.drawable.card_dummy);
                        }
                    }
                }

            } else if (digit1.equals("6")) {
//                iv_CardImage.setBackgroundResource(R.drawable.card_discover);
                if (number.length() == 16) {
                    valid = "DISCOVER";
                    validornot = validCCNumber(number);

                    if (number.length() == 16) {
                        if (validornot) {
//                            iv_CardImage.setBackgroundResource(R.drawable.card_discover);
                            et_card_number.setTextColor(ContextCompat.getColor(activity, R.color.colorGreen));

                        } else {
                            et_card_number.setTextColor(ContextCompat.getColor(activity, R.color.colorRed));
//                            mySnackBar.showSnackBar(main_layout,getString(R.string.card_is_not_valid));
//                            iv_CardImage.setBackgroundResource(R.drawable.card_dummy);
                        }
                    }
                }

            } else if (digit2.equals("35")) {
//                iv_CardImage.setBackgroundResource(R.drawable.card_jcbs);
                if (number.length() == 16 || number.length() == 17 || number.length() == 18 || number.length() == 19) {
                    valid = "JBC";
                    validornot = validCCNumber(number);

                    if (number.length() > 15) {
                        if (validornot) {
//                            iv_CardImage.setBackgroundResource(R.drawable.card_jcbs);
                            et_card_number.setTextColor(ContextCompat.getColor(activity, R.color.colorGreen));

                        } else {
                            et_card_number.setTextColor(ContextCompat.getColor(activity, R.color.colorRed));
//                            mySnackBar.showSnackBar(main_layout,getString(R.string.card_is_not_valid));
//                            iv_CardImage.setBackgroundResource(R.drawable.card_dummy);
                        }
                    }
                }

            } else if (digit2.compareTo("51") >= 0 && digit2.compareTo("55") <= 0 || digit1.equalsIgnoreCase("2")) {
//                iv_CardImage.setBackgroundResource(R.drawable.card_master);
                if (number.length() == 16)
                    valid = "MASTERCARD";
                validornot = validCCNumber(number);

                if (number.length() == 16) {
                    if (validornot) {
//                        iv_CardImage.setBackgroundResource(R.drawable.card_master);
                        et_card_number.setTextColor(ContextCompat.getColor(activity, R.color.colorGreen));

                    } else {
                        et_card_number.setTextColor(ContextCompat.getColor(activity, R.color.colorRed));
//                        mySnackBar.showSnackBar(main_layout,getString(R.string.card_is_not_valid));
//                        iv_CardImage.setBackgroundResource(R.drawable.card_dummy);
                    }
                }
            }
            else
            {
                validornot = false;
            }
        }
        else
        {
            validornot = false;
//            iv_CardImage.setBackgroundResource(R.drawable.card_dummy);
        }

    }

    public static boolean validCCNumber(String n) {
        try {

            int j = n.length();

            String [] s1 = new String[j];
            for (int i=0; i < n.length(); i++) s1[i] = "" + n.charAt(i);

            int checksum = 0;

            for (int i=s1.length-1; i >= 0; i-= 2) {
                int k = 0;

                if (i > 0) {
                    k = Integer.valueOf(s1[i-1]).intValue() * 2;
                    if (k > 9) {
                        String s = "" + k;
                        k = Integer.valueOf(s.substring(0,1)).intValue() +
                                Integer.valueOf(s.substring(1)).intValue();
                    }
                    checksum += Integer.valueOf(s1[i]).intValue() + k;
                }
                else
                    checksum += Integer.valueOf(s1[0]).intValue();
            }
            return ((checksum % 10) == 0);
        }
        catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public void scanCard()
    {
        resumeFlag = 0;
        isScan = true;
        Intent intent = new Intent(TickPayRegisterActivity.this, CardIOActivity.class)
                .putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, true)
                .putExtra(CardIOActivity.EXTRA_SCAN_EXPIRY, true)
                .putExtra(CardIOActivity.EXTRA_GUIDE_COLOR, Color.TRANSPARENT)
                .putExtra("debug_autoAcceptResult", true);;
        startActivityForResult(intent, REQUEST_SCAN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("TAG", "onActivityResult(" + requestCode + ", " + resultCode + ", " + data + ")");


        if ((requestCode == REQUEST_SCAN || requestCode == REQUEST_AUTOTEST) && data != null
                && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT))
        {
            CreditCard result = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);

            Log.e("TAG", "Rusult: " + result);
            String strCardNumber = "";
            String strCardExpiry = "";
            if (result != null)
            {
                cardNumberParam = result.getFormattedCardNumber().replace(" ","");
                Log.e("call","cardNumberParam cardNumberParam cardNumberParam = "+cardNumberParam);
                strCardNumber = getFormattedCardNumber(cardNumberParam);
                CardType cardType = result.getCardType();

                String year = result.expiryYear + "";

                if (year!=null && year.length()>2)
                {
                    year = year.substring(year.length()-2);
                }

                String month = getMonth(result.expiryMonth);

                if (month!=null && month.length()>3)
                {
                    month = month.substring(0,3);
                }

                if (result.expiryMonth < 10)
                {
                    strExpiry = "0" + result.expiryMonth + "/" + year;
                }
                else
                {
                    strExpiry = result.expiryMonth + "/" + year;
                }

                expiryDate = strExpiry;

                strCardExpiry = month + ", " + result.expiryYear;
                et_card_number.setText(strCardNumber);
                ed_expire_date.setText(strCardExpiry);
                et_Alias.setText(cardType.name);
            }
        }
        else if ( resultCode == RESULT_OK )
        {

            try
            {
                if (requestCode == 1 && resultCode == RESULT_OK && null != data)
                {
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = { MediaStore.Images.Media.DATA };

                    uriGallery = data.getData();
                    Log.d("2222",""+data.getData());
                    Cursor cursor = getContentResolver().query(selectedImage,
                            filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    picturePath = cursor.getString(columnIndex);
                    cursor.close();
                    Log.d("filePathUri", "" + picturePath);
                    mImageUri = Uri.parse(picturePath);
                /*frameActivity( filePathUri );*/
                    bmp = BitmapFactory.decodeFile(String.valueOf(mImageUri));
                    bmp = getResizedBitmap(bmp,400);
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bmp.compress(Bitmap.CompressFormat.PNG, 10, stream);
                    byte[] byteArray = stream.toByteArray();
                    byteArrayImage = byteArray;
                    iv_DrivingLicence.setImageBitmap(bmp);

                }
                else if(requestCode==0)
                {
                    Bitmap thumbnail = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
                    Log.e("#########","bmp height = "+thumbnail.getHeight());
                    Log.e("#########","bmp width = "+thumbnail.getWidth());
                    thumbnail = getResizedBitmap(thumbnail,400);
                    bmp=thumbnail;
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    thumbnail.compress(Bitmap.CompressFormat.PNG, 10, bytes);
                    byte[] byteArray = bytes.toByteArray();
                    byteArrayImage = byteArray;
                    iv_DrivingLicence.setImageBitmap(bmp);
                }
                else if (requestCode == 3 && resultCode == RESULT_OK && null != data)
                {
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = { MediaStore.Images.Media.DATA };

                    uriGalleryPassport = data.getData();
                    Log.d("2222",""+data.getData());
                    Cursor cursor = getContentResolver().query(selectedImage,
                            filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    picturePath = cursor.getString(columnIndex);
                    cursor.close();
                    Log.d("filePathUri", "" + picturePath);
                    mImageUri = Uri.parse(picturePath);
                /*frameActivity( filePathUri );*/
                    bmpPassport = BitmapFactory.decodeFile(String.valueOf(mImageUri));
                    bmpPassport = getResizedBitmap(bmpPassport,400);
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bmpPassport.compress(Bitmap.CompressFormat.PNG, 10, stream);
                    byte[] byteArray = stream.toByteArray();
                    byteArrayImagePassport = byteArray;
                    iv_Passport.setImageBitmap(bmpPassport);

                }
                else if(requestCode==2)
                {
                    Bitmap thumbnail = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
                    Log.e("#########","bmp height = "+thumbnail.getHeight());
                    Log.e("#########","bmp width = "+thumbnail.getWidth());
                    thumbnail = getResizedBitmap(thumbnail,400);
                    bmpPassport=thumbnail;
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    thumbnail.compress(Bitmap.CompressFormat.PNG, 10, bytes);
                    byte[] byteArray = bytes.toByteArray();
                    byteArrayImagePassport = byteArray;
                    iv_Passport.setImageBitmap(bmpPassport);
                }
                else
                {
                    bmp = null;
                    byteArrayImage = null;
                }
            }
            catch (Exception e)
            {
                Log.e("call","exception = "+e.getMessage());
                bmp = null;
                byteArrayImage = null;
            }
        }
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public void takeNfcPermission()
    {
        if (ActivityCompat.checkSelfPermission(TickPayRegisterActivity.this, Manifest.permission.NFC) != PackageManager.PERMISSION_GRANTED)
        {
            Log.e("call"," permision 1111111111");
            if (ActivityCompat.shouldShowRequestPermissionRationale(TickPayRegisterActivity.this, Manifest.permission.NFC))
            {
                Log.e("call"," permision 222222222222");
                //Show Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(TickPayRegisterActivity.this);
                builder.setTitle(getResources().getString(R.string.need_nfc_permission));
                builder.setMessage(getResources().getString(R.string.this_app_need_nfc_permission));
                builder.setPositiveButton(getResources().getString(R.string.grant), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Log.e("call"," permision 33333333");
                        ActivityCompat.requestPermissions(TickPayRegisterActivity.this, new String[]{Manifest.permission.NFC}, 5);
                    }
                });
                builder.setNegativeButton(getResources().getString(R.string.cancle), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Log.e("call"," permision 44444444444");
                    }
                });
                builder.show();
            }
            else
            {
                Log.e("call"," permision 5555555555");
                //just request the permission
                ActivityCompat.requestPermissions(TickPayRegisterActivity.this, new String[]{Manifest.permission.CAMERA}, 5);
            }
            Log.e("call"," permision 6666666666666");
            SharedPreferences.Editor editor = permissionStatus.edit();
            editor.putBoolean(Manifest.permission.NFC,true);
            editor.commit();
        }
        else
        {
            checkNFC();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        TicktocApplication.setCurrentActivity(activity);
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        if (resumeFlag==1)
        {
            if (SessionSave.getUserSession(Common.IS_PASSCODE_REQUIRED,activity)!=null && SessionSave.getUserSession(Common.IS_PASSCODE_REQUIRED,activity).equalsIgnoreCase("1"))
            {
                resumeFlag = 0;
                Intent intent = new Intent(TickPayRegisterActivity.this,Create_Passcode_Activity.class);
                intent.putExtra("from","TickPay");
                startActivity(intent);
                finish();
            }
        }
        else
        {
            resumeFlag=1;
        }
    }

    public String getMonth(int month) {
        return new DateFormatSymbols().getMonths()[month-1];
    }

    public void takePermission(int flag)
    {
        if (ActivityCompat.checkSelfPermission(TickPayRegisterActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
        {
            Log.e("call"," permision 1111111111");
            if (ActivityCompat.shouldShowRequestPermissionRationale(TickPayRegisterActivity.this, Manifest.permission.CAMERA))
            {
                Log.e("call"," permision 222222222222");
                //Show Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(TickPayRegisterActivity.this);
                builder.setTitle(getResources().getString(R.string.need_camera_permission));
                builder.setMessage(getResources().getString(R.string.this_app_need_camera_permission));
                builder.setPositiveButton(getResources().getString(R.string.grant), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Log.e("call"," permision 33333333");
                        ActivityCompat.requestPermissions(TickPayRegisterActivity.this, new String[]{Manifest.permission.CAMERA}, 5);
                    }
                });
                builder.setNegativeButton(getResources().getString(R.string.cancle), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Log.e("call"," permision 44444444444");
                    }
                });
                builder.show();
            }
            else
            {
                Log.e("call"," permision 5555555555");
                //just request the permission
                ActivityCompat.requestPermissions(TickPayRegisterActivity.this, new String[]{Manifest.permission.CAMERA}, 5);
            }
            Log.e("call"," permision 6666666666666");
            SharedPreferences.Editor editor = permissionStatus.edit();
            editor.putBoolean(Manifest.permission.CAMERA,true);
            editor.commit();
        }
        else if (ActivityCompat.checkSelfPermission(TickPayRegisterActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            if (ActivityCompat.shouldShowRequestPermissionRationale(TickPayRegisterActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            {
                Log.e("call"," permision 222222222222");
                //Show Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(TickPayRegisterActivity.this);
                builder.setTitle(getResources().getString(R.string.storage_permission));
                builder.setMessage(getResources().getString(R.string.this_app_need_storage_permission));
                builder.setPositiveButton(getResources().getString(R.string.grant), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Log.e("call"," permision 33333333");
                        ActivityCompat.requestPermissions(TickPayRegisterActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 5);
                    }
                });
                builder.setNegativeButton(getResources().getString(R.string.cancle), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Log.e("call"," permision 44444444444");
                    }
                });
                builder.show();
            }
            else
            {
                Log.e("call"," permision 5555555555");
                //just request the permission
                ActivityCompat.requestPermissions(TickPayRegisterActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 5);
            }
            Log.e("call"," permision 6666666666666");
            SharedPreferences.Editor editor = permissionStatus.edit();
            editor.putBoolean(Manifest.permission.WRITE_EXTERNAL_STORAGE,true);
            editor.commit();
        }
        else
        {
            seletcOption(flag);
        }
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        if (sentToSettings)
        {
            if (ActivityCompat.checkSelfPermission(TickPayRegisterActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                //Got Permission
            }

            if (ActivityCompat.checkSelfPermission(TickPayRegisterActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                //Got Permission
            }
        }
    }

    public void seletcOption(final int flag)
    {
        LayoutInflater layoutInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.select_option, null);

        final Dialog dialog = new Dialog(TickPayRegisterActivity.this,R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(view);

        final LinearLayout ll_Gallery,ll_Camera,ll_Cancel;
        final TextView tv_Gallery,tv_Camera,tv_Cancel;

        ll_Camera = (LinearLayout) view.findViewById(R.id.select_option_camera_layout);
        ll_Gallery = (LinearLayout) view.findViewById(R.id.select_option_gallery_layout);
        ll_Cancel = (LinearLayout) view.findViewById(R.id.select_option_cancel_layout);

        tv_Camera = (TextView) view.findViewById(R.id.select_option_camera_textview);
        tv_Gallery = (TextView) view.findViewById(R.id.select_option_gallery_textview);
        tv_Cancel = (TextView) view.findViewById(R.id.select_option_cancel_textview);

        ll_Camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                resumeFlag = 0;
                openCamera(flag);
            }
        });

        tv_Camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                resumeFlag = 0;
                openCamera(flag);
            }
        });

        ll_Gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                resumeFlag = 0;
                selectPhoto(flag);
            }
        });

        tv_Gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                resumeFlag = 0;
                selectPhoto(flag);
            }
        });

        ll_Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        tv_Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        dialog.show();
    }

    //Image Pick From Camera
    private void openCamera(int flag) {

        if (flag==1)
        {
            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.TITLE, "NewPicture");
            imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            startActivityForResult(intent, 0);
        }
        else
        {
            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.TITLE, "NewPicture");
            imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            startActivityForResult(intent, 2);
        }
    }

    //Image Pick From Gallary
    private void selectPhoto(int flag)
    {
        if (flag==1)
        {
            Intent intent_gallery = new Intent(
                    Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(intent_gallery, 1);
        }
        else
        {
            Intent intent_gallery = new Intent(
                    Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(intent_gallery, 3);
        }
    }
}

package iiride.app.rider.activity;

import android.os.Bundle;
import android.os.StrictMode;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import iiride.app.rider.R;
import iiride.app.rider.application.TicktocApplication;
import iiride.app.rider.comman.SessionSave;

import org.json.JSONArray;
import org.json.JSONObject;

public class TripCompleteActivity extends AppCompatActivity implements View.OnClickListener{

    public static TripCompleteActivity activity;
    private LinearLayout ll_Back;
    private ImageView iv_Back;
    private TextView tv_Title;

    private JSONObject tripCompleted = null;
    private TextView tv_PickupLocation,tv_DropOffLocation,tv_NightFair,tv_TripFee,tv_WaitingCost;
    private TextView tv_BookingCharge,tv_Discount,tv_SubTotal,tv_GrandTotal,tv_Status,tv_TollFee,tv_Tax,tv_DistanceFare;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip_complete);

        activity = TripCompleteActivity.this;

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        try
        {

            if (SessionSave.getUserSession("TripCompleted",activity)!=null && !SessionSave.getUserSession("TripCompleted",activity).equalsIgnoreCase(""))
            {
                tripCompleted = new JSONObject(SessionSave.getUserSession("TripCompleted",activity));
            }
        }
        catch (Exception e)
        {
            Log.e("call","exception = "+e.getMessage());
        }

        init();
    }




    private void init() {

        ll_Back = (LinearLayout) findViewById(R.id.back_layout);
        iv_Back = (ImageView) findViewById(R.id.back_imageview);
        tv_Title = (TextView) findViewById(R.id.title_textview);
        tv_Title.setText("Trip Detail");

        tv_PickupLocation = (TextView) findViewById(R.id.activity_trip_complete_textview_pickup_location);
        tv_DropOffLocation = (TextView) findViewById(R.id.activity_trip_complete_textview_dropoff_location);
        tv_NightFair = (TextView) findViewById(R.id.activity_trip_complete_textview_night_fair);
        tv_TripFee = (TextView) findViewById(R.id.activity_trip_complete_textview_trip_fee);
        tv_WaitingCost = (TextView) findViewById(R.id.activity_trip_complete_textview_waiting_cost);
        tv_BookingCharge = (TextView) findViewById(R.id.activity_trip_complete_textview_booking_charge);
        tv_Discount = (TextView) findViewById(R.id.activity_trip_complete_textview_discount);
        tv_SubTotal = (TextView) findViewById(R.id.activity_trip_complete_textview_sub_total);
        tv_GrandTotal = (TextView) findViewById(R.id.activity_trip_complete_textview_grand_total);
        tv_Status = (TextView) findViewById(R.id.activity_trip_complete_textview_status);
        tv_TollFee = (TextView) findViewById(R.id.activity_trip_complete_textview_toll_fee);
        tv_Tax = (TextView) findViewById(R.id.activity_trip_complete_textview_tax);
        tv_DistanceFare = (TextView) findViewById(R.id.activity_trip_complete_textview_distance_fare);

        try
        {
            if (tripCompleted!=null)
            {
                JSONArray jsonArray = tripCompleted.getJSONArray("Info");

                if (jsonArray!=null && jsonArray.length()>0)
                {
                    JSONObject data = jsonArray.getJSONObject(0);

                    if (data!=null)
                    {
                        if (data.has("PickupLocation"))
                        {
                            tv_PickupLocation.setText(data.getString("PickupLocation"));
                        }
                        else
                        {
                            tv_PickupLocation.setText("");
                        }

                        if (data.has("DropoffLocation"))
                        {
                            tv_DropOffLocation.setText(data.getString("DropoffLocation"));
                        }
                        else
                        {
                            tv_DropOffLocation.setText("");
                        }

                        if (data.has("NightFare"))
                        {
                            tv_NightFair.setText(data.getString("NightFare"));
                        }
                        else
                        {
                            tv_NightFair.setText("");
                        }

                        if (data.has("TripFare"))
                        {
                            tv_TripFee.setText(data.getString("TripFare"));
                        }
                        else
                        {
                            tv_TripFee.setText("");
                        }

                        if (data.has("WaitingTimeCost"))
                        {
                            tv_WaitingCost.setText(data.getString("WaitingTimeCost"));
                        }
                        else
                        {
                            tv_WaitingCost.setText("");
                        }

                        if (data.has("BookingCharge"))
                        {
                            tv_BookingCharge.setText(data.getString("BookingCharge"));
                        }
                        else
                        {
                            tv_BookingCharge.setText("");
                        }

                        if (data.has("Discount"))
                        {
                            tv_Discount.setText(data.getString("Discount"));
                        }
                        else
                        {
                            tv_Discount.setText("");
                        }

                        if (data.has("SubTotal"))
                        {
                            tv_SubTotal.setText(data.getString("SubTotal"));
                        }
                        else
                        {
                            tv_SubTotal.setText("");
                        }

                        if (data.has("GrandTotal"))
                        {
                            tv_GrandTotal.setText(data.getString("GrandTotal"));
                        }
                        else
                        {
                            tv_GrandTotal.setText("");
                        }

                        if (data.has("TollFee"))
                        {
                            tv_TollFee.setText(data.getString("TollFee"));
                        }
                        else
                        {
                            tv_TollFee.setText("");
                        }

                        if (data.has("Tax"))
                        {
                            tv_Tax.setText(data.getString("Tax"));
                        }
                        else
                        {
                            tv_Tax.setText("");
                        }

                        String distance = "";
                        if (data.has("TripDistance"))
                        {
                            distance = data.getString("TripDistance");
                        }
                        else
                        {
                            distance = "";
                        }

                        if (data.has("DistanceFare"))
                        {
                            if (distance!=null && !distance.equalsIgnoreCase(""))
                            {
                                tv_DistanceFare.setText(data.getString("DistanceFare")+" ("+distance+" km)");
                            }
                            else
                            {
                                tv_DistanceFare.setText(data.getString("DistanceFare"));
                            }
                        }
                        else
                        {
                            tv_DistanceFare.setText("0");
                        }

                        if (data.has("Status"))
                        {
                            tv_Status.setText(data.getString("Status"));
                        }
                        else
                        {
                            tv_Status.setText("");
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            Log.e("call","Exception = "+e.getMessage());
        }

        ll_Back.setOnClickListener(activity);
        iv_Back.setOnClickListener(activity);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.back_layout:
                onBackPressed();
                break;

            case R.id.back_imageview:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        Intent intent = new Intent(TripCompleteActivity.this,Splash_Activity.class);
//        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();

        TicktocApplication.setCurrentActivity(activity);

    }
}

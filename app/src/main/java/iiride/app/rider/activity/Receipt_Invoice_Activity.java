package iiride.app.rider.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import iiride.app.rider.R;
import iiride.app.rider.application.TicktocApplication;
import iiride.app.rider.comman.Common;
import iiride.app.rider.comman.SessionSave;
import iiride.app.rider.comman.WebServiceAPI;
import iiride.app.rider.other.DialogClass;
import iiride.app.rider.other.Global;
import iiride.app.rider.other.InternetDialog;

import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;


public class Receipt_Invoice_Activity extends AppCompatActivity implements View.OnClickListener{

    public static Receipt_Invoice_Activity activity;

    private TextView tv_send;

    private LinearLayout ll_Back;
    private ImageView iv_Back;
    private TextView tv_Title;

    private EditText et_Amount,et_Email,et_Phone,et_Description,et_Name;
    private DialogClass dialogClass;
    private AQuery aQuery;
    public static int selectedInvoice = 1;
    private RadioButton rb_SendViaEmail,rb_SendViaSms;
    private String description = "";
    public static int resumeFlag = 1;
    private String displayTotal = "";
    private String total = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receipt_invoice);

        activity = Receipt_Invoice_Activity.this;
        selectedInvoice = 1;
        resumeFlag = 1;
        dialogClass = new DialogClass(activity,0);
        aQuery = new AQuery(activity);

        if (this.getIntent()!=null)
        {
            if (this.getIntent().getStringExtra("total")!=null)
            {
                total = this.getIntent().getStringExtra("total");
            }
            else
            {
                total = "";
            }

            if (this.getIntent().getStringExtra("displayTotal")!=null)
            {
                displayTotal = this.getIntent().getStringExtra("displayTotal");
            }
            else
            {
                displayTotal = "";
            }
        }
        else
        {
            total = "";
            displayTotal = "";
        }

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_DESCRIPTION,activity)!=null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_DESCRIPTION,activity).equalsIgnoreCase(""))
        {
            description = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_DESCRIPTION,activity);
        }
        else
        {
            description = "";
        }

        init();
    }

    private void init() {
        ll_Back = (LinearLayout) findViewById(R.id.back_layout);
        iv_Back = (ImageView) findViewById(R.id.back_imageview);

        tv_Title = (TextView) findViewById(R.id.title_textview);

        tv_Title.setText("Invoice Receipt");

        rb_SendViaEmail = (RadioButton) findViewById(R.id.rb_SendViaEmail);
        rb_SendViaSms = (RadioButton) findViewById(R.id.rb_SendViaSms);
        rb_SendViaEmail.setChecked(true);
        rb_SendViaSms.setChecked(false);

        et_Amount = (EditText) findViewById(R.id.et_amount);
        et_Name = (EditText) findViewById(R.id.et_name);
        et_Description = (EditText) findViewById(R.id.et_description);

        et_Email = (EditText) findViewById(R.id.et_email);
        et_Phone = (EditText) findViewById(R.id.et_phoneNumber);

        et_Email.setVisibility(View.VISIBLE);
        et_Phone.setVisibility(View.GONE);

        if (displayTotal!=null && !displayTotal.equalsIgnoreCase(""))
        {
            et_Amount.setText("$"+displayTotal);
        }
        else
        {
            et_Amount.setText("");
        }
        et_Amount.setEnabled(false);
        et_Description.setText(description);

        tv_send = (TextView) findViewById(R.id.tv_send);

        ll_Back.setOnClickListener(activity);
        iv_Back.setOnClickListener(activity);
        tv_send.setOnClickListener(activity);

        rb_SendViaEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedInvoice = 1;
                et_Email.setVisibility(View.VISIBLE);
                et_Phone.setVisibility(View.GONE);
                rb_SendViaEmail.setChecked(true);
                rb_SendViaSms.setChecked(false);
            }
        });

        rb_SendViaSms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedInvoice = 2;
                et_Email.setVisibility(View.GONE);
                et_Phone.setVisibility(View.VISIBLE);
                rb_SendViaEmail.setChecked(false);
                rb_SendViaSms.setChecked(true);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch(v.getId())
        {
            case R.id.back_layout:
                onBackPressed();
                break;

            case R.id.back_imageview:
                onBackPressed();
                break;


            case R.id.tv_send:

                if (et_Name.getText().toString().trim().equalsIgnoreCase(""))
                {
                    InternetDialog internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog("Please enter Customer/Business name!");
                }
                else if (et_Description.getText().toString().trim().equalsIgnoreCase(""))
                {
                    InternetDialog internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog("Please enter Description!");
                }
                else if (total.trim().equalsIgnoreCase(""))
                {
                    InternetDialog internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog("Please enter Amount!");
                }
                else
                {
                    if (selectedInvoice==1)
                    {
                        if (et_Email.getText().toString().trim().equalsIgnoreCase(""))
                        {
                            InternetDialog internetDialog = new InternetDialog(activity);
                            internetDialog.showDialog("Please enter Email!");
                        }
                        else if (!TicktocApplication.isEmailValid(et_Email.getText().toString().trim()))
                        {
                            InternetDialog internetDialog = new InternetDialog(activity);
                            internetDialog.showDialog("Please enter valid Email!");
                        }
                        else
                        {
                            if (Global.isNetworkconn(activity))
                            {
                                send();
                            }
                            else
                            {
                                InternetDialog internetDialog = new InternetDialog(activity);
                                internetDialog.showDialog("Please check your internet connection!");
                            }
                        }
                    }
                    else
                    {
                        if (et_Phone.getText().toString().trim().equalsIgnoreCase(""))
                        {
                            InternetDialog internetDialog = new InternetDialog(activity);
                            internetDialog.showDialog("Please enter Phone Number!");
                        }
                        else if (et_Phone.getText().toString().trim().length()!=10)
                        {
                            InternetDialog internetDialog = new InternetDialog(activity);
                            internetDialog.showDialog("Please enter valid Phone Number!");
                        }
                        else
                        {
                            if (Global.isNetworkconn(activity))
                            {
                                send();
                            }
                            else
                            {
                                InternetDialog internetDialog = new InternetDialog(activity);
                                internetDialog.showDialog("Please check your internet connection!");
                            }
                        }
                    }
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
    }

    public void send()
    {
        dialogClass.showDialog();
        String url = WebServiceAPI.API_TICKPAY_INVOICE;
        //TickpayId,InvoiceType,CustomerName,Notes,Amount,Email,MobileNo(InvoiceType : SMS/Email)

        Map<String, Object> params = new HashMap<String, Object>();
        params.put(WebServiceAPI.PARAM_TICKPAY_ID, TickPayActivity.payId);

        if (selectedInvoice==1)
        {
            params.put(WebServiceAPI.PARAM_INVOICE_TYPE, WebServiceAPI.PARAM_INVOICE_TYPE_EMAIL);
            params.put(WebServiceAPI.PARAM_EMAIL, et_Email.getText().toString());
        }
        else if (selectedInvoice==2)
        {
            params.put(WebServiceAPI.PARAM_INVOICE_TYPE, WebServiceAPI.PARAM_INVOICE_TYPE_SMS);
            params.put(WebServiceAPI.PARAM_MOBILE_NUMBER, et_Phone.getText().toString());
        }

        params.put(WebServiceAPI.PARAM_CUSTOMER_NAME, et_Name.getText().toString());
        params.put(WebServiceAPI.PARAM_NOTES, et_Description.getText().toString());
        params.put(WebServiceAPI.PARAM_AMOUNT, total.replace("$",""));


        Log.e("call", "url = " + url);
        Log.e("call", "params = " + params);

        aQuery.ajax(url.trim(), params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e("responseCode", " = " + responseCode);
                    Log.e("Response", " = " + json);

                    if (json!=null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                dialogClass.hideDialog();
                                String message = "Payment Receipt send successfully!";

                                if (json.has("message"))
                                {
                                    message = json.getString("message");
                                }

                                showSuccessMessage(message);
                            }
                            else
                            {
                                String message = "Please try again later";
                                if (json.has("message"))
                                {
                                    message = json.getString("message");
                                }
                                dialogClass.hideDialog();
                                InternetDialog internetDialog = new InternetDialog(activity);
                                internetDialog.showDialog(message);
                            }
                        }
                        else
                        {
                            String message = "Please try again later";
                            if (json.has("message"))
                            {
                                message = json.getString("message");
                            }
                            dialogClass.hideDialog();
                            InternetDialog internetDialog = new InternetDialog(activity);
                            internetDialog.showDialog(message);
                        }
                    }
                    else
                    {
                        String message = "Please try again later";
                        if (json.has("message"))
                        {
                            message = json.getString("message");
                        }
                        dialogClass.hideDialog();
                        InternetDialog internetDialog = new InternetDialog(activity);
                        internetDialog.showDialog(message);
                    }
                }
                catch (Exception e)
                {
                    Log.e("Exception","Exception "+e.toString());
                    dialogClass.hideDialog();
                    InternetDialog internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog("Something went wrong prlease try again later");
                }
            }

        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    @Override
    protected void onResume() {
        super.onResume();

        TicktocApplication.setCurrentActivity(activity);

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (resumeFlag==1)
        {
            if (SessionSave.getUserSession(Common.IS_PASSCODE_REQUIRED,activity)!=null && SessionSave.getUserSession(Common.IS_PASSCODE_REQUIRED,activity).equalsIgnoreCase("1"))
            {
                resumeFlag = 0;

                if (TickPayActivity.activity!=null)
                {
                    TickPayActivity.activity.finish();
                }

                Intent intent = new Intent(Receipt_Invoice_Activity.this,Create_Passcode_Activity.class);
                intent.putExtra("from","TickPay");
                startActivity(intent);
                finish();
            }
        }
        else
        {
            resumeFlag=1;
        }
    }

    public void showSuccessMessage(String message)
    {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.tick_pay_ok_dialog);

        TextView tv_Ok = (TextView) dialog.findViewById(R.id.Ok);
        TextView tv_Message = (TextView) dialog.findViewById(R.id.message);
        LinearLayout ll_Ok = (LinearLayout) dialog.findViewById(R.id.ok_layout);

        tv_Message.setText(message);

        ll_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                TickPayActivity.activity.finish();
                finish();
            }
        });

        tv_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                TickPayActivity.activity.finish();
                finish();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        dialog.show();
    }
}

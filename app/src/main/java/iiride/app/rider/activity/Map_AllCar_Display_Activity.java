package iiride.app.rider.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import iiride.app.rider.R;
import iiride.app.rider.comman.Common;
import iiride.app.rider.comman.Constants;
import iiride.app.rider.comman.SessionSave;
import iiride.app.rider.comman.WebServiceAPI;
import iiride.app.rider.other.DialogClass;
import iiride.app.rider.other.GPSTracker;
import iiride.app.rider.other.Global;
import iiride.app.rider.other.InternetDialog;
import com.skyfishjy.library.RippleBackground;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;


public class Map_AllCar_Display_Activity extends AppCompatActivity implements View.OnClickListener{

    public static Map_AllCar_Display_Activity activity;
    private MapView mMapView;

    private GoogleMap googleMap;
    private GPSTracker gpsTracker;
    private Handler handler;
    double latitude, longitude;
    private Marker marker, markerDrivers;

    private AQuery aQuery;
    private DialogClass dialogClass;
    private String TAG = "ShowAllDriver";
    private Handler handler1;
    private Runnable runnable;
    private RippleBackground rippleBackground;
    private ImageView ivCenterImage;
//    private MapRipple mapRipple;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_all_car_display);

        activity = Map_AllCar_Display_Activity.this;

        rippleBackground=(RippleBackground)findViewById(R.id.content);


        aQuery = new AQuery(activity);
        handler1 = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(Map_AllCar_Display_Activity.this,MainActivity.class);
                intent.putExtra("from","Splash_Activity");
                startActivity(intent);
                finish();
            }
        };
        Init(savedInstanceState);
    }

    private void Init(Bundle savedInstanceState) {

        ivCenterImage = (ImageView) findViewById(R.id.centerImage);
        mMapView = (MapView) findViewById(R.id.mapView_all_car);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume();
        mMapView.setOnClickListener(this);
        // needed to get the map to display immediately

        try
        {
            MapsInitializer.initialize(getApplicationContext());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;

                // For showing a move to my location button
                if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    return;
                }
                googleMap.setMyLocationEnabled(false);
                // For dropping a marker at a point on the Map
                RefreshLocation();
            }
        });

        ivCenterImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.mapView_all_car:
                onBackPressed();
                break;
        }

    }

    public void RefreshLocation()
    {
        GPSTracker tracker = new GPSTracker(activity);
        if (!tracker.canGetLocation())
        {
            tracker.showSettingsAlert();
        }
        else
        {
            UpdateLocation();
        }
    }

    private void UpdateLocation()
    {
        gpsTracker = new GPSTracker(activity);
        gpsTracker.getLocation();

        Constants.newgpsLatitude = gpsTracker.getLatitude()+"";
        Constants.newgpsLongitude = gpsTracker.getLongitude()+"";

        Log.e("getLatitude()", "rrrrrrrrrrrrrr" + gpsTracker.getLatitude());
        Log.e("getLongitude()", "rrrrrrrrrrrrrr" + gpsTracker.getLongitude());
        Log.e("latitude", "rrrrrrrrrrrrrr" + Constants.newgpsLatitude);
        Log.e("longitude", "rrrrrrrrrrrrr" + Constants.newgpsLongitude);

        latitude = gpsTracker.getLatitude();
        longitude = gpsTracker.getLongitude();

        LatLng sydney = new LatLng(latitude, longitude);
        if (marker != null)
        {
            marker.remove();
        }
//        marker = googleMap.addMarker(new MarkerOptions().position(sydney).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));

        // For zooming automatically to the location of the marker
        CameraPosition cameraPosition = new CameraPosition.Builder().target(sydney).zoom(5).build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        if (Global.isNetworkconn(activity))
        {
            ShowAllDriver();
        }
        else
        {
            InternetDialog internetDialog = new InternetDialog(activity);
            internetDialog.showDialog("Please check your internet connection!");
        }
    }

    private void ShowAllDriver()
    {
        dialogClass = new DialogClass(activity, 1);
        dialogClass.showDialog();

        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.API_SHOW_ALL_DRIVER;

        Log.e(TAG, "URL = " + url);
        Log.e(TAG, "PARAMS = " + params);

        aQuery.ajax(url, null, JSONObject.class, new AjaxCallback<JSONObject>(){

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e(TAG, "responseCode = " + responseCode);
                    Log.e(TAG, "Response = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                Log.e("status", "true");
                                if (json.has("drivers"))
                                {
                                    JSONArray driverArray = json.getJSONArray("drivers");
                                    if (driverArray.length()>0)
                                    {
                                        String Id="", Fullname="", Lat="", Lng="",ModelId = "";

                                        for (int i=0; i<driverArray.length(); i++)
                                        {
                                            JSONObject driverObj = driverArray.getJSONObject(i);
                                            if (driverObj.has("Id"))
                                            {
                                                Id= driverObj.getString("Id");
                                            }
                                            if (driverObj.has("Fullname"))
                                            {
                                                Fullname= driverObj.getString("Fullname");
                                            }
                                            if (driverObj.has("Lat"))
                                            {
                                                Lat= driverObj.getString("Lat");
                                            }
                                            if (driverObj.has("Lng"))
                                            {
                                                Lng= driverObj.getString("Lng");
                                            }

                                            if (driverObj.has("Models"))
                                            {
                                                String Models = driverObj.getString("Models");

                                                if (Models!=null && !Models.equalsIgnoreCase(""))
                                                {
                                                    if (Models.contains(","))
                                                    {
                                                        String[] models = Models.split(",");

                                                        if (models.length>0)
                                                        {
                                                            ModelId = models[0];
                                                        }
                                                    }
                                                    else
                                                    {
                                                        ModelId = Models;
                                                    }
                                                }
                                                else
                                                {
                                                    ModelId = "";
                                                }
                                            }
                                            else
                                            {
                                                ModelId = "";
                                            }

                                            Log.e("call","ModelId ModelId ModelId ModelId , i = "+ModelId+","+i);

                                            if (Lat!=null && Lng!=null && !Lat.equalsIgnoreCase("") && !Lat.equalsIgnoreCase("0") && !Lng.equalsIgnoreCase("") && !Lng.equalsIgnoreCase("0"))
                                            {

                                                LatLng sydney = new LatLng(Double.parseDouble(Lat), Double.parseDouble(Lng));

                                                if (ModelId!=null && !ModelId.equalsIgnoreCase(""))
                                                {
                                                    if (ModelId.equalsIgnoreCase("1"))
                                                    {
                                                        markerDrivers = googleMap.addMarker(new MarkerOptions().position(sydney).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car_two)).anchor(0.5f, 0.5f));
                                                    }
                                                    else if (ModelId.equalsIgnoreCase("2"))
                                                    {
                                                        markerDrivers = googleMap.addMarker(new MarkerOptions().position(sydney).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car_six)).anchor(0.5f, 0.5f));
                                                    }
                                                    else if (ModelId.equalsIgnoreCase("3"))
                                                    {
                                                        markerDrivers = googleMap.addMarker(new MarkerOptions().position(sydney).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car_four)).anchor(0.5f, 0.5f));
                                                    }
                                                    else if (ModelId.equalsIgnoreCase("4"))
                                                    {
                                                        markerDrivers = googleMap.addMarker(new MarkerOptions().position(sydney).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car_one)).anchor(0.5f, 0.5f));
                                                    }
                                                    else if (ModelId.equalsIgnoreCase("5"))
                                                    {
                                                        markerDrivers = googleMap.addMarker(new MarkerOptions().position(sydney).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car_five)).anchor(0.5f, 0.5f));
                                                    }
                                                    else if (ModelId.equalsIgnoreCase("6"))
                                                    {
                                                        markerDrivers = googleMap.addMarker(new MarkerOptions().position(sydney).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car_three)).anchor(0.5f, 0.5f));
                                                    }
                                                    else
                                                    {
                                                        markerDrivers = googleMap.addMarker(new MarkerOptions().position(sydney).icon(BitmapDescriptorFactory.fromResource(R.drawable.car_image)).anchor(0.5f, 0.5f));
                                                    }
                                                }
                                                else
                                                {
                                                    markerDrivers = googleMap.addMarker(new MarkerOptions().position(sydney).icon(BitmapDescriptorFactory.fromResource(R.drawable.car_image)).anchor(0.5f, 0.5f));
                                                }
                                            }
                                            else
                                            {
                                                Log.e("call","lat long null or blank or zero");
                                            }
                                        }

                                        dialogClass.hideDialog();
                                        gotoMainScreen();
                                    }
                                    else
                                    {
                                        Log.e("driverObject", "null");
                                        dialogClass.hideDialog();
                                        gotoMainScreen();
                                    }
                                }
                                if(json.has("car_class"))
                                {
                                    JSONArray car_class = json.getJSONArray("car_class");
                                    if (car_class!=null && car_class.length()>0)
                                    {
                                        Log.e("call","car_class.size = "+car_class.length());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_CAR_CLASS,car_class.toString(),activity);
                                        dialogClass.hideDialog();
                                    }
                                }
                            }
                            else
                            {
                                Log.e("status", "false");
                                dialogClass.hideDialog();
                                gotoMainScreen();
                            }
                        }
                        else
                        {
                            Log.e("status", "no status");
                            dialogClass.hideDialog();
                            gotoMainScreen();
                        }
                    }
                    else
                    {
                        Log.e(TAG, "getMessage = " + "null");
                        dialogClass.hideDialog();
                        gotoMainScreen();
                    }
                }
                catch (Exception e)
                {
                    Log.e(TAG, "Exception = " + e.getMessage() + "something_is_wrong");
                    dialogClass.hideDialog();
                    gotoMainScreen();
                }


            }
        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        handler1.removeCallbacks(runnable,null);
        Intent intent = new Intent(Map_AllCar_Display_Activity.this,MainActivity.class);
        intent.putExtra("from","Splash_Activity");
        startActivity(intent);
        finish();
    }

    public void gotoMainScreen()
    {
        rippleBackground.startRippleAnimation();
        handler1.postDelayed(runnable,3000);
    }
}

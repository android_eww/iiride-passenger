package iiride.app.rider.activity;

import android.Manifest;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.media.MediaPlayer;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.Settings;

import androidx.annotation.NonNull;

import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.textfield.TextInputLayout;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.format.Formatter;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;

import iiride.app.rider.R;
import iiride.app.rider.adapter.CarListAdapter;
import iiride.app.rider.adapter.MenuListAdapter;
import iiride.app.rider.application.TicktocApplication;
import iiride.app.rider.been.BookingDriverLocation_Been;
import iiride.app.rider.been.BookingRequest_Been;
import iiride.app.rider.been.CarType_Been;
import iiride.app.rider.been.CreditCard_List_Been;
import iiride.app.rider.been.GetEstimateFare_Been;
import iiride.app.rider.been.LatLong_Been;
import iiride.app.rider.been.MenuList_Been;
import iiride.app.rider.been.initData.InitData;
import iiride.app.rider.comman.Common;
import iiride.app.rider.comman.Constants;
import iiride.app.rider.comman.SessionSave;
import iiride.app.rider.comman.SocketSingleObject;
import iiride.app.rider.comman.TaxiUtil;
import iiride.app.rider.comman.Utility;
import iiride.app.rider.comman.WebServiceAPI;
import iiride.app.rider.kalmani.KalmanLocationManager;
import iiride.app.rider.map.DirectionsJSONParser;
import iiride.app.rider.notification.FCMUtil;
import iiride.app.rider.other.DialogClass;
import iiride.app.rider.other.DoubleArrayEvaluator;
import iiride.app.rider.other.GPSTracker;
import iiride.app.rider.other.Global;
import iiride.app.rider.other.InternetDialog;
import iiride.app.rider.other.MyAlertDialog;
import iiride.app.rider.other.RequestPendingDialogClass;
import iiride.app.rider.roundedimage.RoundedTransformationBuilder;
import iiride.app.rider.view.CustomSpinner;
import iiride.app.rider.view.MySnackBar;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener, GoogleMap.OnCameraChangeListener, CarListAdapter.SetCarListener {//, GoogleMap.OnMarkerDragListener

    public static MainActivity activity;
    private Toolbar toolbar;
    private MenuListAdapter drawerListAdapter;
    private ListView lv_drawer;
    private DrawerLayout drawer;
    private List<MenuList_Been> menuList_beens = new ArrayList<MenuList_Been>();
    private LinearLayout ll_DrawerLayout, ll_Menu, ll_Favorite;
    private ImageView iv_Menu, iv_MyLocation, iv_Favorite;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private LatLng coordinate;
    private GoogleMap googleMap;
    private Marker marker, originMarker, destinationMarker;
    public static boolean myLocation = true;
    private GPSTracker gpsTracker;
    private static final int PERMISSION_REQUEST_CODE = 1;
    private CircleImageView iv_ProfileImage;
    private Transformation mTransformation;
    private TextView tv_UserName, tv_UserPhone;
    private Runnable runnable = null;
    private Runnable runnableProfile = null;
    private Runnable runnableDriver = null;
    private List<LatLong_Been> latLong_beens = new ArrayList<LatLong_Been>();
    public static JSONArray carClass = null;
    public static List<CarType_Been> carType_beens = new ArrayList<CarType_Been>();

    private CarListAdapter carListAdapter;
    private RecyclerView rvCars;
    private LinearLayout ll_CarOne, ll_CarTwo, ll_CarThree, ll_CarFour, ll_CarFive, ll_CarSix;
    private LinearLayout ll_CarBackgroundOne, ll_CarBackgroundTwo, ll_CarBackgroundThree, ll_CarBackgroundFour, ll_CarBackgroundFive, ll_CarBackgroundSix;
    private ImageView iv_CarOne, iv_CarTwo, iv_CarThree, iv_CarFour, iv_CarFive, iv_CarSix;
    private TextView tv_AvailableOne, tv_AvailableTwo, tv_AvailableThree, tv_AvailableFour, tv_AvailableFive, tv_AvailableSix;
    private TextView tv_TimeOne, tv_TimeTwo, tv_TimeThree, tv_TimeFour, tv_TimeFive, tv_TimeSix;
    private TextView tv_PriceOne, tv_PriceTwo, tv_PriceThree, tv_PriceFour, tv_PriceFive, tv_PriceSix;
    private TextView tv_BookNow, tv_BookLater;
    public static int selectedCar = 0;
    private String passangerId = "";
    private Handler handler;
    private Handler handlerProfile;
    private Handler handlerDriver;

    private AutoCompleteTextView auto_Pickup, auto_Dropoff;
    private TextView tvPickup, tvDropoff;

    private String jsonurl = "";
    private ArrayList<String> names, Place_id_type, secondryAddress;
    private ParseData parse;
    public static JSONObject json;
    private JSONArray contacts = null;
    private static final String TAG_RESULT = "predictions";
    private String place_id = "";
    private String pickUpSubArb = "", dropOffSubAArb = "";
    public static int addFlag = 1;
    private DialogClass dialogClass;
    private RequestPendingDialogClass requestPendingDialogClass;
    private AQuery aQuery;
    public static List<BookingRequest_Been> bookingRequest_beens = new ArrayList<BookingRequest_Been>();
    public static List<BookingDriverLocation_Been> bookingDriverLocation_beens = new ArrayList<BookingDriverLocation_Been>();
    private LinearLayout ll_BookingLayout, ll_AllCarLayout;
    public static int tripFlag = 0;
    public static int selectedCarPosition = -1;
    public static JSONObject requestConfirmJsonObject = null;
    public static String driverId = "";
    public static int zoomFlag = 0;
    public static LatLng pickUp = null;
    public static LatLng driverLatLng = null;
    public static String BookingId = "";
    private LinearLayout ll_BottomSheet;
    private BottomSheetBehavior mBottomSheetBehavior;
    private TextView peakView;
    private TextView tv_CancelRequest, tv_DriverInfo;
    private LinearLayout ll_AfterRequestAccept;
    private TextView tv_CarModel, tv_CarCompany, tv_Pickup, tv_Dropoff, tv_DriverName;
    private ImageView iv_CarImage, iv_Close, iv_DriverImage, iv_CallDriver, iv_SmsDriver;
    public static String driverPhone = "";
    private LinearLayout ll_ClearPickup, ll_ClearDropoff, address_Layout;
    public static double upcomming_pickup_lat = 0;
    public static double upcomming_pickup_long = 0;
    public static double upcomming_dropoff_lat = 0;
    public static double upcomming_dropoff_long = 0;
    private LinearLayout ll_ProfileDetail;
    private LinearLayout ll_RootLayour;
    private MySnackBar mySnackBar;
    public static int advanceBooking = 0;
    private List<GetEstimateFare_Been> getEstimateFare_beens = new ArrayList<GetEstimateFare_Been>();
    public static String from = "";
    public static int favorite = 0;
    public static String favorite_address = "";
    private boolean pickup_Touch = false;
    private boolean dropoff_Touch = false;
    public static int totalAvailableCount = 0;
    private String promocodeStr = "";
    private String notes = "";
    public static int timeFlag = 0;
    public static double getEstimated_lat = 0, getEstimated_long = 0;
    public static String getEstimated_Pickup = "";
    public static boolean callResumeScreen = false;
    public static ArrayList<CreditCard_List_Been> cardListBeens = new ArrayList<CreditCard_List_Been>();

    public static String cardId = "";
    public static String cardNumber = "";
    public static String cardType = "";
    private boolean isBabySeatInclude = false;
    public static boolean isCardSelected = false;
    public static String selectedPaymentType = "";
    public static int spinnerSelectedPosition = 0;
    private CustomSpinner Spinner_paymentMethod;
    private CustomerAdapter customerAdapter;

    //kalmani........
    private static final long GPS_TIME = 5000;
    private static final long NET_TIME = 5000;
    private static final long FILTER_TIME = 5000;
    private KalmanLocationManager mKalmanLocationManager;
    // GoogleMaps own OnLocationChangedListener (not android's LocationListener)
    public static double kalmanLat = 0;
    public static double kalmanLong = 0;

    private Dialog commonDialog;
    private InternetDialog commonInternetDialog;
    public static boolean isAlreadySendRequest = false;
    public static int handlerCount = 0;
    private Handler handlerSendRequest;

    public static int PasscodeBackPress = 0;
    public static String token = "";
//    private ArrayList<LatLng> points1 = null;

    public static MediaPlayer ringTone = null;
    public static String driverName = "";
    public static float ratting = 0;
    private TextView tv_Rate;
    private RelativeLayout current_marker_layout;
    public static int addCardFlag = 0;
    private String fromSplash = "";
    public static String redirectBooking, redirectBookingAdrs;

    private SharedPreferences permissionStatus;
    private int PICK_CONTACT = 105;
    //    private String finalMessage = "";
    public static boolean isBookLaterActivate = false;

    public static String cityName = "", cityId = "";
    int PICKUP_AUTOCOMPLETE_REQUEST_CODE = 201;
    int DROPOFF_AUTOCOMPLETE_REQUEST_CODE = 202;
    List<Place.Field> fields;

    private boolean isCityGet = false;
    public static boolean isBookingDetailEdited = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        activity = MainActivity.this;

        permissionStatus = getSharedPreferences("permissionStatus", MODE_PRIVATE);
        carType_beens.clear();
        driverName = "";
        isBookingDetailEdited = false;
        ratting = 0;
        addCardFlag = 0;
        token = "";
        handlerCount = 0;
        ringTone = null;
        PasscodeBackPress = 0;
        isAlreadySendRequest = false;
        ringTone = MediaPlayer.create(activity, R.raw.tone);
        aQuery = new AQuery(activity);
        dialogClass = new DialogClass(activity, 0);
        callResumeScreen = true;
        advanceBooking = 0;
        cardListBeens.clear();
        favorite_address = "";
        kalmanLat = 0;
        kalmanLong = 0;
        handlerSendRequest = new Handler();
        handlerProfile = new Handler();
        commonDialog = new Dialog(activity);
        commonInternetDialog = new InternetDialog(activity);
        mKalmanLocationManager = new KalmanLocationManager(this);
        redirectBooking = "";
        redirectBookingAdrs = "";
        isBookLaterActivate = false;
        /*finalMessage = name + invitationMessag + getResources().getString(R.string.click_here) + inviteCodeMessage + iniviteCode + "\n" + getResources().getString(R.string.app_link_two);

        Log.e("call", "finalMessage = " + finalMessage);*/
        rvCars = findViewById(R.id.rvCars);
        carListAdapter = new CarListAdapter(activity, carType_beens, getEstimateFare_beens, this);
        rvCars.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
        rvCars.setAdapter(carListAdapter);

        getCityName();

        if (this.getIntent() != null) {
            if (this.getIntent().getStringExtra("from") != null) {
                fromSplash = this.getIntent().getStringExtra("from");
            } else {
                fromSplash = "";
            }
        } else {
            fromSplash = "";
        }

        Places.initialize(getApplicationContext(), getString(R.string.map_key));
        fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG);

        Handler handlerToken = new Handler();
        handlerToken.postDelayed(new Runnable() {
            @Override
            public void run() {

                String strToken = FCMUtil.getFcmToken(activity);
                token = strToken;
                Log.e("call", "token@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ = " + FCMUtil.getFcmToken(activity));
            }
        }, 500);

//        points1 = new ArrayList<LatLng>();
//
//        points1.add(new LatLng(23.072287, 72.516225));
//        points1.add(new LatLng(23.072259, 72.516274));
//        points1.add(new LatLng(23.072238, 72.516312));
//        points1.add(new LatLng(23.072219, 72.516371));
//        points1.add(new LatLng(23.072190, 72.516417));
//        points1.add(new LatLng(23.072175, 72.516465));
//        points1.add(new LatLng(23.072154, 72.516511));
//        points1.add(new LatLng(23.072131, 72.516558));
//        points1.add(new LatLng(23.072109, 72.516602));
//        points1.add(new LatLng(23.072084, 72.516647));
//        points1.add(new LatLng(23.072060, 72.516684));
//        points1.add(new LatLng(23.072037, 72.516733));
//        points1.add(new LatLng(23.072009, 72.516778));
//        points1.add(new LatLng(23.071983, 72.516824));
//        points1.add(new LatLng(23.071959, 72.516870));
//        points1.add(new LatLng(23.071934, 72.516913));
//        points1.add(new LatLng(23.071902, 72.516878));
//        points1.add(new LatLng(23.071859, 72.516865));
//        points1.add(new LatLng(23.071808, 72.516878));
//        points1.add(new LatLng(23.071776, 72.516909));
//        points1.add(new LatLng(23.071755, 72.516952));
//        points1.add(new LatLng(23.071752, 72.517004));
//        points1.add(new LatLng(23.071755, 72.517055));
//        points1.add(new LatLng(23.071751, 72.517054));

        if (Global.isNetworkconn(activity)) {
            resumeScreen();
        } else {
            InternetDialog internetDialog = new InternetDialog(activity);
            internetDialog.showDialog("Please check your internet connection!");
        }
    }

    public void getCarClass() {
        try {
            if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_CAR_CLASS, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_CAR_CLASS, activity).equals("")) {
                carClass = new JSONArray(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_CAR_CLASS, activity));
                Log.e("carClassgetCarClass", "" + carClass);
            } else {
                carClass = null;
            }

            if (carClass != null && carClass.length() > 0)
            {
                carType_beens.clear();
                for (int i = 0; i < carClass.length(); i++) {
                    JSONObject data = carClass.getJSONObject(i);

                    if (data != null) {
                        if (data.has("CategoryId")) {
                            String Id = "", CategoryId = "", Name = "", Sort = "", BaseFare = "", MinKm = "", PerKmCharge = "", CancellationFee = "", NightCharge = "", NightTimeFrom = "";
                            String NightTimeTo = "", SpecialEventSurcharge = "", SpecialEventTimeFrom = "", SpecialEventTimeTo = "", WaitingTimeCost = "", MinuteFare = "";
                            String BookingFee = "", Capacity = "", Image = "", Description = "", Status = "";

                            CategoryId = data.getString("CategoryId");

                            if (CategoryId != null && !CategoryId.equals("") && CategoryId.equals("1"))
                            {
                                if (data.has("Id")) {
                                    Id = data.getString("Id");
                                }

                                if (data.has("Name")) {
                                    Name = data.getString("Name");
                                }

                                if (data.has("Sort")) {
                                    Sort = data.getString("Sort");
                                }

                                if (data.has("BaseFare")) {
                                    BaseFare = data.getString("BaseFare");
                                }

                                if (data.has("MinKm")) {
                                    MinKm = data.getString("MinKm");
                                }

                                if (data.has("PerKmCharge")) {
                                    PerKmCharge = data.getString("PerKmCharge");
                                }

                                if (data.has("CancellationFee")) {
                                    CancellationFee = data.getString("CancellationFee");
                                }

                                if (data.has("NightCharge")) {
                                    NightCharge = data.getString("NightCharge");
                                }

                                if (data.has("NightTimeFrom")) {
                                    NightTimeFrom = data.getString("NightTimeFrom");
                                }

                                if (data.has("NightTimeTo")) {
                                    NightTimeTo = data.getString("NightTimeTo");
                                }

                                if (data.has("SpecialEventSurcharge")) {
                                    SpecialEventSurcharge = data.getString("SpecialEventSurcharge");
                                }

                                if (data.has("SpecialEventTimeFrom")) {
                                    SpecialEventTimeFrom = data.getString("SpecialEventTimeFrom");
                                }

                                if (data.has("SpecialEventTimeTo")) {
                                    SpecialEventTimeTo = data.getString("SpecialEventTimeTo");
                                }

                                if (data.has("WaitingTimeCost")) {
                                    WaitingTimeCost = data.getString("WaitingTimeCost");
                                }

                                if (data.has("MinuteFare")) {
                                    MinuteFare = data.getString("MinuteFare");
                                }

                                if (data.has("BookingFee")) {
                                    BookingFee = data.getString("BookingFee");
                                }

                                if (data.has("Capacity")) {
                                    Capacity = data.getString("Capacity");
                                }

                                if (data.has("Image")) {
                                    Image = data.getString("Image");
                                }

                                if (data.has("Description")) {
                                    Description = data.getString("Description");
                                }

                                if (data.has("Status")) {
                                    Status = data.getString("Status");
                                }

                                carType_beens.add(new CarType_Been(
                                        Id,
                                        CategoryId,
                                        Name,
                                        Sort,
                                        BaseFare,
                                        MinKm,
                                        PerKmCharge,
                                        CancellationFee,
                                        NightCharge,
                                        NightTimeFrom,
                                        NightTimeTo,
                                        SpecialEventSurcharge,
                                        SpecialEventTimeFrom,
                                        SpecialEventTimeTo,
                                        WaitingTimeCost,
                                        MinuteFare,
                                        BookingFee,
                                        Capacity,
                                        Image,
                                        Description,
                                        Status,
                                        0
                                ));
                            }
                        }
                    }
                }

                /*Collections.sort(
                        carType_beens,
                        new Comparator<CarType_Been>()
                        {
                            public int compare(CarType_Been lhs, CarType_Been rhs)
                            {
                                return lhs.getSort().compareTo(rhs.getSort());
                            }
                        }
                );*/
                Log.e("call", "carType_beens.size() = " + carType_beens.size());

                for (int i = 0; i < carType_beens.size(); i++) {
                    Log.e("call", "carType_beens sorted id , name = " + carType_beens.get(i).getSort() + ", " + carType_beens.get(i).getName());
                }
            } else {
                carType_beens.clear();
            }
        } catch (Exception e) {
            Log.e("call", "Exception from getting car class");
        }
    }

    public void resumeScreen() {
        dialogClass.showDialog();
        String url = WebServiceAPI.API_CURRENT_BOOCING + SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID, activity);

        Log.e("call", "url = " + url);

        aQuery.ajax(url.trim(), null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    cardListBeens.clear();
                    int responseCode = status.getCode();
                    Log.e("responseCode", " = " + responseCode);
                    Log.e("Response", " = " + json);
                    if (json != null) {
                        if (json.has("rating")) {
                            if (json.getString("rating") != null && !json.getString("rating").equalsIgnoreCase("") && !json.getString("rating").equalsIgnoreCase("NULL") && !json.getString("rating").equalsIgnoreCase("null")) {
                                ratting = Float.parseFloat(json.getString("rating"));
                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_RATTING, ratting + "", activity);
                            } else {
                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_RATTING, "0", activity);
                            }
                        } else {
                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_RATTING, "0", activity);
                        }

                        Log.e("call", "sdjflsjfljsdflj = " + ratting);

                        if (json.has("cards")) {
                            JSONArray cards = json.getJSONArray("cards");

                            if (cards != null && cards.length() > 0) {
                                for (int i = 0; i < cards.length(); i++) {
                                    JSONObject cardsData = cards.getJSONObject(i);

                                    if (cardsData != null) {
                                        String Id = "", CardNum = "", CardNum_ = "", Type = "", Alias = "";

                                        if (cardsData.has("Id")) {
                                            Id = cardsData.getString("Id");
                                        }

                                        if (cardsData.has("CardNum")) {
                                            CardNum = cardsData.getString("CardNum");
                                        }

                                        if (cardsData.has("CardNum2")) {
                                            CardNum_ = cardsData.getString("CardNum2");
                                        }

                                        if (cardsData.has("Type")) {
                                            Type = cardsData.getString("Type");
                                        }

                                        if (cardsData.has("Alias")) {
                                            Alias = cardsData.getString("Alias");
                                        }

                                        cardListBeens.add(new CreditCard_List_Been(Id, CardNum, CardNum_, Type, Alias));
                                    }
                                }
                            }
                        }

                        //cardListBeens.add(new CreditCard_List_Been("","","Add a Card","",""));
//                        cardListBeens.add(new CreditCard_List_Been("","","wallet","",""));

                        if (cardListBeens.size() > 0) {

                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_CARD_LIST, json.toString(), activity);
                        } else {
                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_CARD_LIST, "", activity);
                        }

                        if (json.has("status")) {
                            if (json.getBoolean("status")) {
                                if (json.has("BookingType")) {
                                    if (json.getString("BookingType") != null && json.getString("BookingType").equalsIgnoreCase("BookLater")) {
                                        advanceBooking = 1;
                                        if (json.has("Status")) {
                                            if (json.getString("Status").equalsIgnoreCase("accepted")) {
                                                if (json.has("BookingInfo")) {
                                                    JSONArray BookingInfo = json.getJSONArray("BookingInfo");

                                                    if (BookingInfo != null && BookingInfo.length() > 0) {
                                                        JSONObject BookingInfoData = BookingInfo.getJSONObject(0);

                                                        if (BookingInfoData != null) {
                                                            if (BookingInfoData.has("OnTheWay")) {
                                                                if (BookingInfoData.getString("OnTheWay") != null && BookingInfoData.getString("OnTheWay").equalsIgnoreCase("1")) {
                                                                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "1", activity);
                                                                    //for on the way
                                                                    if (json.has("CarInfo")) {
                                                                        JSONArray jsonArray = json.getJSONArray("CarInfo");

                                                                        if (jsonArray != null && jsonArray.length() > 0) {
                                                                            JSONObject jsonObject = jsonArray.getJSONObject(0);

                                                                            if (jsonObject != null) {
                                                                                if (jsonObject.has("VehicleModel")) {
                                                                                    String model = jsonObject.getString("VehicleModel");

                                                                                    int selectedCar = -1;

                                                                                    if (carType_beens != null && carType_beens.size() > 0) {
                                                                                        for (int car = 0; car < carType_beens.size(); car++) {
                                                                                            if (model.equalsIgnoreCase(carType_beens.get(car).getId())) {
                                                                                                selectedCar = Integer.parseInt(carType_beens.get(car).getSort());
                                                                                            }
                                                                                        }
                                                                                    }

//                                                                                    if (model.equalsIgnoreCase("1"))
//                                                                                    {
//                                                                                        selectedCar = 1;
//                                                                                    }
//                                                                                    else if (model.equalsIgnoreCase("2"))
//                                                                                    {
//                                                                                        selectedCar = 5;
//                                                                                    }
//                                                                                    else if (model.equalsIgnoreCase("3"))
//                                                                                    {
//                                                                                        selectedCar = 3;
//                                                                                    }
//                                                                                    else if (model.equalsIgnoreCase("4"))
//                                                                                    {
//                                                                                        selectedCar = 0;
//                                                                                    }
//                                                                                    else if (model.equalsIgnoreCase("5"))
//                                                                                    {
//                                                                                        selectedCar = 4;
//                                                                                    }
//                                                                                    else if (model.equalsIgnoreCase("6"))
//                                                                                    {
//                                                                                        selectedCar = 2;
//                                                                                    }

                                                                                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, selectedCar + "", activity);
                                                                                }
                                                                            }

                                                                        }
                                                                    }

                                                                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, json.toString(), activity);
                                                                    dialogClass.hideDialog();
                                                                    initOnCreate();
                                                                } else {
                                                                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, "-1", activity);
                                                                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "0", activity);
                                                                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, "", activity);
                                                                    Log.e("call", "status false");

                                                                    dialogClass.hideDialog();
                                                                    initOnCreate();
                                                                }
                                                            } else {
                                                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, "-1", activity);
                                                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "0", activity);
                                                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, "", activity);
                                                                Log.e("call", "status false");

                                                                dialogClass.hideDialog();
                                                                initOnCreate();
                                                            }
                                                        } else {
                                                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, "-1", activity);
                                                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "0", activity);
                                                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, "", activity);
                                                            Log.e("call", "status false");

                                                            dialogClass.hideDialog();
                                                            initOnCreate();
                                                        }
                                                    } else {
                                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, "-1", activity);
                                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "0", activity);
                                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, "", activity);
                                                        Log.e("call", "status false");

                                                        dialogClass.hideDialog();
                                                        initOnCreate();
                                                    }
                                                }
                                            } else if (json.getString("Status").equalsIgnoreCase("traveling")) {
                                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "2", activity);

                                                if (json.has("CarInfo")) {
                                                    JSONArray jsonArray = json.getJSONArray("CarInfo");

                                                    if (jsonArray != null && jsonArray.length() > 0) {
                                                        JSONObject jsonObject = jsonArray.getJSONObject(0);

                                                        if (jsonObject != null) {
                                                            if (jsonObject.has("VehicleModel")) {
                                                                String model = jsonObject.getString("VehicleModel");

                                                                int selectedCar = -1;

                                                                if (carType_beens != null && carType_beens.size() > 0) {
                                                                    for (int car = 0; car < carType_beens.size(); car++) {
                                                                        if (model.equalsIgnoreCase(carType_beens.get(car).getId())) {
                                                                            selectedCar = Integer.parseInt(carType_beens.get(car).getSort());
                                                                        }
                                                                    }
                                                                }

//                                                                if (model.equalsIgnoreCase("1"))
//                                                                {
//                                                                    selectedCar = 1;
//                                                                }
//                                                                else if (model.equalsIgnoreCase("2"))
//                                                                {
//                                                                    selectedCar = 5;
//                                                                }
//                                                                else if (model.equalsIgnoreCase("3"))
//                                                                {
//                                                                    selectedCar = 3;
//                                                                }
//                                                                else if (model.equalsIgnoreCase("4"))
//                                                                {
//                                                                    selectedCar = 0;
//                                                                }
//                                                                else if (model.equalsIgnoreCase("5"))
//                                                                {
//                                                                    selectedCar = 4;
//                                                                }
//                                                                else if (model.equalsIgnoreCase("6"))
//                                                                {
//                                                                    selectedCar = 2;
//                                                                }

                                                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, selectedCar + "", activity);
                                                            }
                                                        }

                                                    }
                                                }

                                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, json.toString(), activity);

                                                dialogClass.hideDialog();
                                                initOnCreate();
                                            }
                                        }

                                    } else {
                                        advanceBooking = 0;
                                        if (json.has("CarInfo")) {
                                            JSONArray jsonArray = json.getJSONArray("CarInfo");

                                            if (jsonArray != null && jsonArray.length() > 0) {
                                                JSONObject jsonObject = jsonArray.getJSONObject(0);

                                                if (jsonObject != null) {
                                                    if (jsonObject.has("VehicleModel")) {
                                                        String model = jsonObject.getString("VehicleModel");

                                                        int selectedCar = -1;

                                                        if (carType_beens != null && carType_beens.size() > 0) {
                                                            for (int car = 0; car < carType_beens.size(); car++) {
                                                                if (model.equalsIgnoreCase(carType_beens.get(car).getId())) {
                                                                    selectedCar = Integer.parseInt(carType_beens.get(car).getSort());
                                                                }
                                                            }
                                                        }

//                                                        if (model.equalsIgnoreCase("1"))
//                                                        {
//                                                            selectedCar = 1;
//                                                        }
//                                                        else if (model.equalsIgnoreCase("2"))
//                                                        {
//                                                            selectedCar = 5;
//                                                        }
//                                                        else if (model.equalsIgnoreCase("3"))
//                                                        {
//                                                            selectedCar = 3;
//                                                        }
//                                                        else if (model.equalsIgnoreCase("4"))
//                                                        {
//                                                            selectedCar = 0;
//                                                        }
//                                                        else if (model.equalsIgnoreCase("5"))
//                                                        {
//                                                            selectedCar = 4;
//                                                        }
//                                                        else if (model.equalsIgnoreCase("6"))
//                                                        {
//                                                            selectedCar = 2;
//                                                        }

                                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, selectedCar + "", activity);
                                                    }
                                                }

                                            }
                                        }

                                        if (json.has("Status")) {
                                            if (json.getString("Status").equalsIgnoreCase("accepted")) {
                                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "1", activity);
                                            } else if (json.getString("Status").equalsIgnoreCase("traveling")) {
                                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "2", activity);
                                            } else {
                                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, "-1", activity);
                                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "0", activity);
                                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, "", activity);
                                            }
                                        } else {
                                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, "-1", activity);
                                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "0", activity);
                                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, "", activity);
                                        }

                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, json.toString(), activity);

                                        dialogClass.hideDialog();

                                        initOnCreate();
                                    }
                                } else {
                                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, "-1", activity);
                                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "0", activity);
                                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, "", activity);
                                    Log.e("call", "status false");
                                    dialogClass.hideDialog();
                                    initOnCreate();
                                }
                            } else {
                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, "-1", activity);
                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "0", activity);
                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, "", activity);
                                Log.e("call", "status false");
                                dialogClass.hideDialog();
                                initOnCreate();
                            }
                        } else {
                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, "-1", activity);
                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "0", activity);
                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, "", activity);
                            callResumeScreen = false;
                            dialogClass.hideDialog();
                            Log.e("call", "status not found");
                        }
                    } else {
                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, "-1", activity);
                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "0", activity);
                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, "", activity);
                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_RATTING, "0", activity);
                        callResumeScreen = false;
                        dialogClass.hideDialog();
                        Log.e("call", "json null ");
                    }
                } catch (Exception e) {
                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, "-1", activity);
                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "0", activity);
                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, "", activity);
                    callResumeScreen = false;
                    Log.e("Exception", "Exception " + e.toString());
                    dialogClass.hideDialog();
                    InternetDialog internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog("Something went wrong!");
                }
            }
        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    public void initOnCreate() {
        callResumeScreen = false;
        myLocation = true;
        from = "";
        getEstimated_lat = 0;
        getEstimated_long = 0;
        getEstimated_Pickup = "";
        totalAvailableCount = 0;
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        gpsTracker = new GPSTracker(activity);
        initMap();
        handler = new Handler();
        handlerDriver = new Handler();
        passangerId = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID, activity);
        requestPendingDialogClass = new RequestPendingDialogClass(activity, 0);

        ll_RootLayour = (LinearLayout) findViewById(R.id.main_content);
        mySnackBar = new MySnackBar(activity);

        getEstimateFare_beens.clear();
        getEstimateFare_beens.add(new GetEstimateFare_Been("", "", "", "", "", "", "", "0", "0", "1"));
        getEstimateFare_beens.add(new GetEstimateFare_Been("", "", "", "", "", "", "", "0", "0", "2"));
        getEstimateFare_beens.add(new GetEstimateFare_Been("", "", "", "", "", "", "", "0", "0", "3"));
        getEstimateFare_beens.add(new GetEstimateFare_Been("", "", "", "", "", "", "", "0", "0", "4"));
        getEstimateFare_beens.add(new GetEstimateFare_Been("", "", "", "", "", "", "", "0", "0", "5"));
        getEstimateFare_beens.add(new GetEstimateFare_Been("", "", "", "", "", "", "", "0", "0", "6"));

        pickup_Touch = false;
        dropoff_Touch = false;
        addFlag = 1;
        carClass = null;
        pickUp = null;
        driverLatLng = null;
        selectedCar = -1;
        zoomFlag = 0;
        BookingId = "";
        driverPhone = "";
//        tripFlag1 = 0;
        driverId = "";
        selectedCarPosition = -1;
        bookingRequest_beens.clear();
        bookingDriverLocation_beens.clear();
        requestConfirmJsonObject = null;

        upcomming_pickup_lat = 0;
        upcomming_pickup_long = 0;
        upcomming_dropoff_lat = 0;
        upcomming_dropoff_long = 0;
        timeFlag = 0;


        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity).equals("")) {
            tripFlag = Integer.parseInt(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity));
            Log.e("call", "onCreate() tripFlag if = " + tripFlag);
        } else {
            tripFlag = 0;
            Log.e("call", "onCreate() tripFlag else = " + tripFlag);
        }

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, activity).equals("")) {
            selectedCarPosition = Integer.parseInt(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, activity));
        }

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, activity).equals("")) {
            try {
                requestConfirmJsonObject = new JSONObject(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, activity));
            } catch (Exception e) {
                Log.e("call", "getting requestConfirmJsonObject exception = " + e.getMessage());
            }

        }

        Log.e("tripFlag", "tripFlag = " + tripFlag);
        Log.e("tripFlag", "selectedCarPosition = " + selectedCarPosition);

        mTransformation = new RoundedTransformationBuilder()
                .cornerRadiusDp(100)
                .borderColor(getResources().getColor(R.color.colorWhite))
                .borderWidthDp(2)
                .oval(true)
                .build();


        init();
    }


    public void connectSocket() {
        Log.e("call", "connectSocket()");
        try {
            if (Common.socket != null && Common.socket.connected()) {
                Log.e("call", "Ping socket already connected connectSocket()");
            } else {
                Log.e("call", "Ping socket try to connecting...connectSocket()");
                Common.socket = null;
                SocketSingleObject.instance = null;
                Common.socket = SocketSingleObject.get(MainActivity.this).getSocket();
                Common.socket.on("NearByDriverList", onDriverList);
                Common.socket.on(Socket.EVENT_CONNECT, onConnect);
                Common.socket.on("AcceptBookingRequestNotification", onAcceptBookingRequestNotification);
                Common.socket.on("RejectBookingRequestNotification", onRejectBookingRequestNotification);
                Common.socket.on("GetDriverLocation", onGetDriverLocation);
                Common.socket.on("TripHoldNotification", onTripHoldNotification);
                Common.socket.on("PickupPassengerNotification", onPickupPassengerNotification);
                Common.socket.on("BookingDetails", onBookingDetails);
                Common.socket.on("GetEstimateFare", onGetEstimateFare);
                Common.socket.on("ReceiveMoneyNotify", onReceiveMoneyNotify);
                Common.socket.on("AcceptAdvancedBookingRequestNotification", onAcceptAdvancedBookingRequestNotification);
                Common.socket.on("RejectAdvancedBookingRequestNotification", onRejectAdvancedBookingRequestNotification);
//                Common.socket.on("InformPassengerForAdvancedTrip",InformPassengerForAdvancedTrip);
                Common.socket.on("AcceptAdvancedBookingRequestNotify", AcceptAdvancedBookingRequestNotify);
                Common.socket.on("AdvancedBookingPickupPassengerNotification", onAdvancedBookingPickupPassengerNotification);
                Common.socket.on("AdvancedBookingTripHoldNotification", onAdvancedBookingTripHoldNotification);
                Common.socket.on("AdvancedBookingDetails", onAdvancedBookingDetails);
                Common.socket.on("CancelAdvanceBookingManually", onCancelAdvanceBookingManually);
                Common.socket.on(Socket.EVENT_DISCONNECT, onDisconnect);
                Common.socket.connect();
            }
        } catch (Exception e) {
            Log.e("call", "Socket connect exception = " + e.getMessage());
        }
    }

    private Emitter.Listener onBookingDetails = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    try {
                        Log.e("call", "onBookingDetails()");

                        if (args != null && args.length > 0) {
                            Log.e("call", "onBookingDetails response = " + args[0].toString());
                            JSONObject tripCompleteObject = new JSONObject(args[0].toString());
                            if (tripCompleteObject != null) {
                                if (commonInternetDialog != null && commonInternetDialog.isShowing()) {
                                    commonInternetDialog.hideDialog();
                                }

                                if (commonDialog != null && commonDialog.isShowing()) {
                                    commonDialog.dismiss();
                                }

                                String walletBalance = "0";

                                if (tripCompleteObject.has("UpdatedBal")) {
                                    walletBalance = tripCompleteObject.getString("UpdatedBal");
                                }

                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_WALLET_BALLENCE, walletBalance, activity);

                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "0", activity);
                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, "-1", activity);
                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, "", activity);

                                ll_BookingLayout.setVisibility(View.VISIBLE);
                                ll_AllCarLayout.setVisibility(View.VISIBLE);
                                ll_AfterRequestAccept.setVisibility(View.GONE);
                                tv_CancelRequest.setVisibility(View.GONE);
                                address_Layout.setVisibility(View.VISIBLE);

                                if (marker != null) {
                                    marker.remove();
                                }

                                if (originMarker != null) {
                                    originMarker.remove();
                                }

                                if (destinationMarker != null) {
                                    destinationMarker.remove();
                                }

                                if (googleMap != null) {
                                    googleMap.clear();
                                }

                                handlerDriver.removeCallbacks(runnableDriver, null);

                                String message = "Your trip has been completed successfully!";

                                if (tripCompleteObject.has("message")) {
                                    message = tripCompleteObject.getString("message");
                                }
                                SessionSave.saveUserSession("TripCompleted", tripCompleteObject.toString(), activity);

                                if (TicktocApplication.getCurrentActivity() != null) {
                                    showTripCompletePopup(message, TicktocApplication.getCurrentActivity(), BookingId, WebServiceAPI.BOOK_NOW);
                                } else {
                                    showTripCompletePopup(message, activity, BookingId, WebServiceAPI.BOOK_NOW);
                                }
                            }
                        }
                    } catch (Exception e) {
                        Log.e("call", "Exception onBookingDetails = " + e.getMessage());
                    }
                }
            });
        }
    };

    public void showTripCompletePopup(String message, final Activity activity, final String bookingId, final String bookingType) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.trip_complete_rate_dialog);

        TextView tv_Ok = (TextView) dialog.findViewById(R.id.Ok);
        TextView tv_Message = (TextView) dialog.findViewById(R.id.message);
        LinearLayout ll_Ok = (LinearLayout) dialog.findViewById(R.id.ok_layout);
        final EditText et_Comment = (EditText) dialog.findViewById(R.id.et_Comment);
        final RatingBar rate = (RatingBar) dialog.findViewById(R.id.rate);


        try {
            if (requestConfirmJsonObject != null) {
                if (requestConfirmJsonObject.has("DriverInfo")) {
                    JSONArray DriverInfoArray = requestConfirmJsonObject.getJSONArray("DriverInfo");

                    if (DriverInfoArray != null && DriverInfoArray.length() > 0) {
                        JSONObject DriverInfoData = DriverInfoArray.getJSONObject(0);

                        if (DriverInfoData != null) {
                            if (DriverInfoData.has("Fullname")) {
                                driverName = DriverInfoData.getString("Fullname");
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            Log.e("call", "exception = " + e.getMessage());
        }

        tv_Message.setText("How was your experience with " + driverName + "?");

        ll_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (Global.isNetworkconn(activity)) {
                    giveRate(rate.getRating() + "", et_Comment.getText().toString(), bookingId, bookingType);
                } else {
                    InternetDialog internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog("Please check your internet connection!");
                }

            }
        });

        tv_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (Global.isNetworkconn(activity)) {
                    giveRate(rate.getRating() + "", et_Comment.getText().toString(), bookingId, bookingType);
                } else {
                    InternetDialog internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog("Please check your internet connection!");
                }
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        dialog.show();
        commonDialog = dialog;
    }

    public void giveRate(String rate, String comment, String bookingId, String bookingtype) {
        final DialogClass dialogClass = new DialogClass(activity, 0);
        dialogClass.showDialog();
        String url = WebServiceAPI.API_GIVE_RATTING;

        Map<String, Object> params = new HashMap<String, Object>();
        params.put(WebServiceAPI.PARAM_BOOKING_ID, bookingId);
        params.put(WebServiceAPI.PARAM_BOOKING_TYPE, bookingtype);
        params.put(WebServiceAPI.PARAM_RATTING, rate);
        params.put(WebServiceAPI.PARAM_COMMENT, comment);

        Log.e("call", "url = " + url);
        Log.e("call", "params = " + params);

        aQuery.ajax(url.trim(), params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("responseCode", " = " + responseCode);
                    Log.e("Response", " = " + json);
                    dialogClass.hideDialog();
                    openTripCompleteScreen();
                } catch (Exception e) {
                    Log.e("Exception", "Exception " + e.toString());
                    dialogClass.hideDialog();
                    openTripCompleteScreen();
                }
            }

        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    public void openTripCompleteScreen() {
        if (TicktocApplication.getCurrentActivity() instanceof MainActivity) {
            Log.e("TiCKTOC_Driver_App", "");
        } else {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }

        from = "TripCompleteActivity";
        Intent intent = new Intent(MainActivity.this, TripCompleteActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
    }

    private Emitter.Listener onPickupPassengerNotification = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    try {
                        Log.e("call", "onPickupPassengerNotification() =========================commonDialog null or not showing");
                        if (commonDialog != null && commonDialog.isShowing()) {
                            commonDialog.dismiss();
                        }

                        if (commonInternetDialog != null && commonInternetDialog.isShowing()) {
                            commonInternetDialog.hideDialog();
                        }

                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "2", activity);

                        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity).equalsIgnoreCase("")) {
                            tripFlag = Integer.parseInt(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity));
                        } else {
                            tripFlag = 0;
                        }

                        if (originMarker != null) {
                            originMarker.remove();
                        }

                        if (destinationMarker != null) {
                            destinationMarker.remove();
                        }

                        if (googleMap != null) {
                            googleMap.clear();
                        }

                        if (args != null && args.length > 0) {
                            Log.e("call", "onPickupPassengerNotification response = " + args[0].toString());
                            tv_CancelRequest.setVisibility(View.GONE);

                            JSONObject jsonObject = new JSONObject(args[0].toString());

                            if (jsonObject != null) {
                                showPathFromPickupToDropoff(requestConfirmJsonObject);
                                String message = "Your trip is start now";
                                if (jsonObject.has("message")) {
                                    message = jsonObject.getString("message");

                                    if (TicktocApplication.getCurrentActivity() != null) {
                                        InternetDialog internetDialog = new InternetDialog(TicktocApplication.getCurrentActivity());
                                        internetDialog.showDialog(message);
                                        commonInternetDialog = internetDialog;
                                    } else {
                                        InternetDialog internetDialog = new InternetDialog(activity);
                                        internetDialog.showDialog(message);
                                        commonInternetDialog = internetDialog;
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        Log.e("call", "Exception onGetDriverLocation = " + e.getMessage());
                    }
                }
            });
        }
    };


    private Emitter.Listener onReceiveMoneyNotify = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    try {
                        if (args != null && args.length > 0) {
                            Log.e("call", "arg not null and lenth greater 0 = " + args[0].toString());

                            JSONObject jsonObject = new JSONObject(args[0].toString());

                            if (jsonObject != null) {
                                if (jsonObject.has("message")) {
                                    String message = jsonObject.getString("message");
                                    if (TicktocApplication.getCurrentActivity() != null) {
                                        InternetDialog internetDialog = new InternetDialog(TicktocApplication.getCurrentActivity());
                                        internetDialog.showDialog(message);
                                    } else {
                                        InternetDialog internetDialog = new InternetDialog(activity);
                                        internetDialog.showDialog(message);
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        Log.e("call", "Exception onGetDriverLocation = " + e.getMessage());
                    }
                }
            });
        }
    };

    private Emitter.Listener onTripHoldNotification = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    try {
                        if (args != null && args.length > 0) {
                            if (commonInternetDialog != null && commonInternetDialog.isShowing()) {
                                commonInternetDialog.hideDialog();
                            }

                            if (commonDialog != null && commonDialog.isShowing()) {
                                commonDialog.dismiss();
                            }

//                            {"message":"Your trip waiting time ended"}
                            Log.e("call", "arg not null and lenth greater 0 = " + args[0].toString());

                            JSONObject jsonObject = new JSONObject(args[0].toString());

                            if (jsonObject != null) {
                                if (jsonObject.has("message")) {
                                    String message = jsonObject.getString("message");
                                    if (TicktocApplication.getCurrentActivity() != null) {
                                        InternetDialog internetDialog = new InternetDialog(TicktocApplication.getCurrentActivity());
                                        internetDialog.showDialog(message);
                                        commonInternetDialog = internetDialog;
                                    } else {
                                        InternetDialog internetDialog = new InternetDialog(activity);
                                        internetDialog.showDialog(message);
                                        commonInternetDialog = internetDialog;
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        Log.e("call", "Exception onGetDriverLocation = " + e.getMessage());
                    }
                }
            });
        }
    };

    private Emitter.Listener onGetDriverLocation = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    try {
                        Log.e("call", "onGetDriverLocation()");
                        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity).equalsIgnoreCase("")) {
                            tripFlag = Integer.parseInt(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity));
                        } else {
                            tripFlag = 0;
                        }

                        if (tripFlag != 0) {
                            bookingDriverLocation_beens.clear();

                            if (args != null && args.length > 0) {
                                String driverLocation = args[0].toString();
                                Log.e("call", "driverLocation args[0].toString() ============================== " + driverLocation);
                                if (driverLocation != null && !driverLocation.equals("")) {
                                    JSONObject driverLocationObject = new JSONObject(driverLocation);

                                    if (driverLocationObject != null) {
                                        if (driverLocationObject.has("Location")) {
                                            JSONArray Location = driverLocationObject.getJSONArray("Location");

                                            if (Location != null && Location.length() > 0) {
                                                bookingDriverLocation_beens.add(new BookingDriverLocation_Been(Location.getDouble(0), Location.getDouble(1)));

                                                if (bookingDriverLocation_beens != null && bookingDriverLocation_beens.size() > 0) {
                                                    if (googleMap != null) {
                                                        if (marker != null) {
                                                            marker.remove();
                                                        }

//                                                        if (originMarker!=null)
//                                                        {
//                                                            originMarker.remove();
//                                                        }

//                                                        LatLng destLatLng= new LatLng(points1.get(i).latitude,points1.get(i).longitude);
//                                                        i++;
                                                        coordinate = new LatLng(bookingDriverLocation_beens.get(0).getLatitude(), bookingDriverLocation_beens.get(0).getLongitude());

                                                        if (originMarker == null) {
                                                            Log.e("call", "originMarker is null");
                                                            if (selectedCar == 0) {
                                                                originMarker = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car_one)));
                                                            } else if (selectedCar == 1) {
                                                                originMarker = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car_two)));
                                                            } else if (selectedCar == 2) {
                                                                originMarker = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car_three)));
                                                            } else if (selectedCar == 3) {
                                                                originMarker = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car_four)));
                                                            } else if (selectedCar == 4) {
                                                                originMarker = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car_five)));
                                                            } else if (selectedCar == 5) {
                                                                originMarker = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car_six)));
                                                            }

                                                            animateMarker(originMarker, coordinate);
                                                        } else {
                                                            animateMarker(originMarker, coordinate);
                                                        }

                                                        if (zoomFlag == 0) {
                                                            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder().target(coordinate).zoom(17.5f).build()));
                                                            zoomFlag = 1;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        Log.e("call", "Exception onGetDriverLocation = " + e.getMessage());
                    }
                }
            });
        }
    };

//    int i=1;

    public void animateMarker(final Marker marker, final LatLng destLatLng) {
        Log.e("call", "destLatLng lat = " + destLatLng.latitude);
        Log.e("call", "destLatLng long = " + destLatLng.longitude);

        double[] startValues = new double[]{marker.getPosition().latitude, marker.getPosition().longitude};
        double[] endValues = new double[]{destLatLng.latitude, destLatLng.longitude};
        ValueAnimator latLngAnimator = ValueAnimator.ofObject(new DoubleArrayEvaluator(), startValues, endValues);
        latLngAnimator.setDuration(4900);
        latLngAnimator.setInterpolator(new LinearInterpolator());
        latLngAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {

                double[] animatedValue = (double[]) animation.getAnimatedValue();

                float bearing = getBearing(marker.getPosition(), destLatLng);
                if (bearing > 0) {
                    marker.setFlat(true);
                    marker.setPosition(new LatLng(animatedValue[0], animatedValue[1]));
                    marker.setRotation(bearing);
                }
            }
        });
        latLngAnimator.start();
    }

    private float getBearing(LatLng begin, LatLng end) {
        double lat = Math.abs(begin.latitude - end.latitude);
        double lng = Math.abs(begin.longitude - end.longitude);

        if (begin.latitude < end.latitude && begin.longitude < end.longitude)
            return (float) ((Math.toDegrees(Math.atan(lng / lat))) + 90);
        else if (begin.latitude >= end.latitude && begin.longitude < end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 90 + 90);
        else if (begin.latitude >= end.latitude && begin.longitude >= end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)) + 180 + 90);
        else if (begin.latitude < end.latitude && begin.longitude >= end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 270 + 90);
        return -1;
    }

    private Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if (Common.socket == null) {
                        handler.removeCallbacks(runnable);
                        Log.e("call", "socket disconnected successfully");
                    }
                }
            });
        }
    };

    public void disconnectSocket() {
        Log.e("call", "disconnectSocket()==============================");
        try {
            if (Common.socket != null && Common.socket.connected()) {
                Common.socket.disconnect();
                Common.socket.off("NearByDriverList", onDriverList);
                Common.socket.off(Socket.EVENT_CONNECT, onConnect);
                Common.socket.off("AcceptBookingRequestNotification", onAcceptBookingRequestNotification);
                Common.socket.off("RejectBookingRequestNotification", onRejectBookingRequestNotification);
                Common.socket.off("GetDriverLocation", onGetDriverLocation);
                Common.socket.off("TripHoldNotification", onTripHoldNotification);
                Common.socket.off("PickupPassengerNotification", onPickupPassengerNotification);
                Common.socket.off("BookingDetails", onBookingDetails);
                Common.socket.off("GetEstimateFare", onGetEstimateFare);
                Common.socket.off("ReceiveMoneyNotify", onReceiveMoneyNotify);
                Common.socket.off("AcceptAdvancedBookingRequestNotification", onAcceptAdvancedBookingRequestNotification);
                Common.socket.off("RejectAdvancedBookingRequestNotification", onRejectAdvancedBookingRequestNotification);
//                Common.socket.off("InformPassengerForAdvancedTrip",InformPassengerForAdvancedTrip);
                Common.socket.off("AcceptAdvancedBookingRequestNotify", AcceptAdvancedBookingRequestNotify);
                Common.socket.off("AdvancedBookingPickupPassengerNotification", onAdvancedBookingPickupPassengerNotification);
                Common.socket.off("AdvancedBookingTripHoldNotification", onAdvancedBookingTripHoldNotification);
                Common.socket.off("AdvancedBookingDetails", onAdvancedBookingDetails);
                Common.socket.off("CancelAdvanceBookingManually", onCancelAdvanceBookingManually);
                Common.socket.off(Socket.EVENT_DISCONNECT, onDisconnect);
                Common.socket = null;
                SocketSingleObject.instance = null;
                Log.e("call", "disconnectSocket()111111==============================");
            } else {
                Log.e("call", "Socket already disconnected.");
            }

            if (carType_beens != null && carType_beens.size() == 6) {
                if (carType_beens.size() > 0) {
                    for (int x = 0; x < carType_beens.size(); x++) {
                        carType_beens.get(x).setAvai(0);
                    }
                }
            }
            setTimeAndPriceToZero();
        } catch (Exception e) {
            Log.e("call", "Socket connect exception = " + e.getMessage());
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cityName = "";
        cityId = "";
        Log.e("call", "onDestroy onDestroy onDestroy onDestroy");
        handler.removeCallbacks(runnable, null);
        handlerProfile.removeCallbacks(runnableProfile, null);
        handlerSendRequest.removeCallbacks(runnableSendRequest, null);
        handlerDriver.removeCallbacks(runnableDriver, null);
        disconnectSocket();
    }

    public void startTimer() {
        Log.e("call", "timer is started 111");
        if (handlerDriver!=null && runnableDriver!=null)
        {
            handlerDriver.removeCallbacks(runnableDriver, null);
        }
        if (handler!=null && runnable!=null)
        {
            handler.removeCallbacks(runnable, null);
        }
        runnable = new Runnable() {

            @Override
            public void run() {
                try {
                    if (Common.socket != null && Common.socket.connected()) {
                        if (gpsTracker.canGetLocation()) {
                            gpsTracker = new GPSTracker(MainActivity.this);
                            Constants.newgpsLatitude = gpsTracker.location.getLatitude() + "";
                            Constants.newgpsLongitude = gpsTracker.location.getLongitude() + "";

                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("PassengerId", passangerId);

                            if (kalmanLat != 0 && kalmanLong != 0) {
                                jsonObject.put("Lat", kalmanLat);
                                jsonObject.put("Long", kalmanLong);
                            } else {
                                jsonObject.put("Lat", gpsTracker.getLatitude());
                                jsonObject.put("Long", gpsTracker.getLongitude());
                            }

                            jsonObject.put("Token", token);


                            if (token != null && !token.equalsIgnoreCase("")) {
                                Log.e("call", "UpdatePassengerLatLong json = " + jsonObject.toString());
                                Common.socket.emit("UpdatePassengerLatLong", jsonObject);

                                Log.w("isCityGet", "" + cityName);
                                if (cityName == null || cityName.equalsIgnoreCase("")) {
                                    isCityGet = true;
                                    getCityName();
                                }
                            }
                        } else {
                            Constants.newgpsLatitude = "0";
                            Constants.newgpsLongitude = "0";
                        }
                    } else {
                        Log.e("call", "socket is not connected.......");

                        if (carType_beens.size() > 0) {
                            for (int x = 0; x < carType_beens.size(); x++) {
                                carType_beens.get(x).setAvai(0);
                            }
                        }


                        setTimeAndPriceToZero();
                        carListAdapter.setCarList(carType_beens);
                    }
                    handler.postDelayed(runnable, 5000);
                } catch (Exception e) {

                    Log.e("call", "error in sending latlong");
                    if (carType_beens.size() > 0) {
                        for (int x = 0; x < carType_beens.size(); x++) {
                            carType_beens.get(x).setAvai(0);
                        }
                    }
                    setTimeAndPriceToZero();
                    carListAdapter.setCarList(carType_beens);
                }
            }
        };
        handler.postDelayed(runnable, 0);
    }

    public void startTimerForDriverLocation() {
        if (handlerDriver!=null && runnableDriver!=null)
        {
            handlerDriver.removeCallbacks(runnableDriver, null);
        }
        if (handler!=null && runnable!=null)
        {
            handler.removeCallbacks(runnable, null);
        }
        Log.e("call", "startTimerForDriverLocation() tripFlag = " + tripFlag);
        runnableDriver = new Runnable() {

            @Override
            public void run() {
                try {
                    if (tripFlag != 0) {
                        if (Common.socket != null) {
                            if (Common.socket.connected()) {
                                if (requestConfirmJsonObject != null) {
                                    if (requestConfirmJsonObject.has("DriverInfo")) {
                                        JSONArray DriverInfo = requestConfirmJsonObject.getJSONArray("DriverInfo");
                                        if (DriverInfo != null && DriverInfo.length() > 0) {
                                            JSONObject driverObject = DriverInfo.getJSONObject(0);

                                            if (driverObject != null) {
                                                driverId = driverObject.getString("Id");

                                                if (driverId != null && !driverId.equals("")) {
                                                    JSONObject jsonObject = new JSONObject();
                                                    jsonObject.put("PassengerId", passangerId + "");
                                                    jsonObject.put("DriverId", driverId + "");
                                                    Log.e("call", "startTimerForDriverLocation object = " + jsonObject.toString());
                                                    Common.socket.emit("DriverLocation", jsonObject);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        handlerDriver.postDelayed(runnableDriver, 5000);
                    }
                } catch (Exception e) {
                    Log.e("call", "error in sending latlong");
                }
            }
        };
        handlerDriver.postDelayed(runnableDriver, 0);
    }


    private void init() {
        disconnectSocket();
        toolbar = new Toolbar(activity);
        setSupportActionBar(toolbar);
        lv_drawer = (ListView) findViewById(R.id.lv_drawer);
        ll_DrawerLayout = (LinearLayout) findViewById(R.id.ll_drawer_layout);
        ll_Menu = (LinearLayout) findViewById(R.id.menu_layout);
        ll_Favorite = (LinearLayout) findViewById(R.id.favorite_layout);
        iv_Menu = (ImageView) findViewById(R.id.menu_image);
        iv_Favorite = (ImageView) findViewById(R.id.favorite_image);
        iv_MyLocation = (ImageView) findViewById(R.id.iv_my_location);

        ll_BookingLayout = (LinearLayout) findViewById(R.id.booking_layout);
        ll_AllCarLayout = (LinearLayout) findViewById(R.id.all_cars);

        ll_ProfileDetail = (LinearLayout) findViewById(R.id.profile_detail_layout);

        auto_Pickup = findViewById(R.id.autoCompleteTextView_pickup);
        auto_Dropoff = findViewById(R.id.autoCompleteTextView_drop_off);

        tvPickup = findViewById(R.id.tvPickup);
        tvDropoff = findViewById(R.id.tvDropoff);

        iv_ProfileImage = (CircleImageView) findViewById(R.id.image_profile_drawer);
        tv_UserName = (TextView) findViewById(R.id.user_name);
        tv_UserPhone = (TextView) findViewById(R.id.user_phone);

        ll_ClearDropoff = (LinearLayout) findViewById(R.id.close_dropoff);
        ll_ClearPickup = (LinearLayout) findViewById(R.id.close_pickup);

        rvCars = findViewById(R.id.rvCars);

        ll_CarOne = (LinearLayout) findViewById(R.id.layout_car_one);
        ll_CarTwo = (LinearLayout) findViewById(R.id.layout_car_two);
        ll_CarThree = (LinearLayout) findViewById(R.id.layout_car_three);
        ll_CarFour = (LinearLayout) findViewById(R.id.layout_car_four);
        ll_CarFive = (LinearLayout) findViewById(R.id.layout_car_five);
        ll_CarSix = (LinearLayout) findViewById(R.id.layout_car_six);

        ll_CarBackgroundOne = (LinearLayout) findViewById(R.id.car_image_layout_one);
        ll_CarBackgroundTwo = (LinearLayout) findViewById(R.id.car_image_layout_two);
        ll_CarBackgroundThree = (LinearLayout) findViewById(R.id.car_image_layout_three);
        ll_CarBackgroundFour = (LinearLayout) findViewById(R.id.car_image_layout_four);
        ll_CarBackgroundFive = (LinearLayout) findViewById(R.id.car_image_layout_five);
        ll_CarBackgroundSix = (LinearLayout) findViewById(R.id.car_image_layout_six);

        ll_BottomSheet = (LinearLayout) findViewById(R.id.bottom_sheet_Layout);

        address_Layout = (LinearLayout) findViewById(R.id.address_layout);
        current_marker_layout = (RelativeLayout) findViewById(R.id.current_marker_layout);
        tv_CancelRequest = (TextView) findViewById(R.id.cancel_request);
        tv_DriverInfo = (TextView) findViewById(R.id.driver_info);
        ll_AfterRequestAccept = (LinearLayout) findViewById(R.id.after_accept_layout);

        tv_Dropoff = (TextView) findViewById(R.id.dropoff_location_bottomsheet);
        tv_Pickup = (TextView) findViewById(R.id.pickup_location_bottomsheet);
        tv_CarCompany = (TextView) findViewById(R.id.car_company_bottomsheet);
        tv_CarModel = (TextView) findViewById(R.id.car_model_bottomsheet);
        tv_DriverName = (TextView) findViewById(R.id.driver_name_bottomsheet);
        iv_CarImage = (ImageView) findViewById(R.id.car_image_bottomsheet);
        iv_DriverImage = (ImageView) findViewById(R.id.driver_image_bottomsheet);
        iv_CallDriver = (ImageView) findViewById(R.id.call_to_driver_bottomsheet);
        iv_SmsDriver = (ImageView) findViewById(R.id.sms_to_driver_bottomsheet);
        tv_Rate = (TextView) findViewById(R.id.tv_Rate);

        iv_Close = (ImageView) findViewById(R.id.close);

        iv_CarOne = (ImageView) findViewById(R.id.imageview_car_one);
        iv_CarTwo = (ImageView) findViewById(R.id.imageview_car_two);
        iv_CarThree = (ImageView) findViewById(R.id.imageview_car_three);
        iv_CarFour = (ImageView) findViewById(R.id.imageview_car_four);
        iv_CarFive = (ImageView) findViewById(R.id.imageview_car_five);
        iv_CarSix = (ImageView) findViewById(R.id.imageview_car_six);

        tv_AvailableOne = (TextView) findViewById(R.id.car_one_textview);
        tv_AvailableTwo = (TextView) findViewById(R.id.car_two_textview);
        tv_AvailableThree = (TextView) findViewById(R.id.car_three_textview);
        tv_AvailableFour = (TextView) findViewById(R.id.car_four_textview);
        tv_AvailableFive = (TextView) findViewById(R.id.car_five_textview);
        tv_AvailableSix = (TextView) findViewById(R.id.car_six_textview);

        tv_TimeOne = (TextView) findViewById(R.id.time_one);
        tv_TimeTwo = (TextView) findViewById(R.id.time_two);
        tv_TimeThree = (TextView) findViewById(R.id.time_three);
        tv_TimeFour = (TextView) findViewById(R.id.time_four);
        tv_TimeFive = (TextView) findViewById(R.id.time_five);
        tv_TimeSix = (TextView) findViewById(R.id.time_six);

        tv_PriceOne = (TextView) findViewById(R.id.price_one);
        tv_PriceTwo = (TextView) findViewById(R.id.price_two);
        tv_PriceThree = (TextView) findViewById(R.id.price_three);
        tv_PriceFour = (TextView) findViewById(R.id.price_four);
        tv_PriceFive = (TextView) findViewById(R.id.price_five);
        tv_PriceSix = (TextView) findViewById(R.id.price_six);

        tv_BookNow = (TextView) findViewById(R.id.book_now_textview);
        tv_BookLater = (TextView) findViewById(R.id.book_later_textview);

        ll_CarBackgroundOne.setBackgroundResource(R.drawable.circle_white_border);
        ll_CarBackgroundTwo.setBackgroundResource(R.drawable.circle_white_border);
        ll_CarBackgroundThree.setBackgroundResource(R.drawable.circle_white_border);
        ll_CarBackgroundFour.setBackgroundResource(R.drawable.circle_white_border);
        ll_CarBackgroundFive.setBackgroundResource(R.drawable.circle_white_border);
        ll_CarBackgroundSix.setBackgroundResource(R.drawable.circle_white_border);

        menuList_beens.add(new MenuList_Been("My Bookings", "", "", R.drawable.ic_menu_driver));
        menuList_beens.add(new MenuList_Been("Previous Due", "", "", R.drawable.ic_paynow));
        menuList_beens.add(new MenuList_Been("Payment Options", "", "", R.drawable.ic_menu_payment_option));
//        menuList_beens.add(new MenuList_Been("Wallet","","",R.drawable.ic_menu_wallet));
//        menuList_beens.add(new MenuList_Been("Pay","","",R.drawable.ic_menu_pay));
        menuList_beens.add(new MenuList_Been("Favourite", "", "", R.drawable.ic_menu_favorite));
        menuList_beens.add(new MenuList_Been("My Receipts", "", "", R.drawable.ic_menu_my_receipts));
        menuList_beens.add(new MenuList_Been("Invite Friends", "", "", R.drawable.ic_menu_invite_friends));
        menuList_beens.add(new MenuList_Been("Bars and Clubs", "", "", R.drawable.ic_menu_bar_clubs));
        menuList_beens.add(new MenuList_Been("Hotel Reservation", "", "", R.drawable.ic_menu_hotel));
        menuList_beens.add(new MenuList_Been("Book a Table", "", "", R.drawable.ic_menu_book_table));
        menuList_beens.add(new MenuList_Been("Shopping", "", "", R.drawable.ic_menu_shopping));
        menuList_beens.add(new MenuList_Been("Setting", "", "", R.drawable.ic_menu_settings));
        menuList_beens.add(new MenuList_Been("Become a", "iiRide", "Driver", R.drawable.ic_menu_driver));
        menuList_beens.add(new MenuList_Been("Logout", "", "", R.drawable.ic_menu_about));

        ll_CarOne.setOnClickListener(activity);
        ll_CarTwo.setOnClickListener(activity);
        ll_CarThree.setOnClickListener(activity);
        ll_CarFour.setOnClickListener(activity);
        ll_CarFive.setOnClickListener(activity);
        ll_CarSix.setOnClickListener(activity);

        tv_BookLater.setOnClickListener(activity);
        tv_BookNow.setOnClickListener(activity);
        iv_Close.setOnClickListener(activity);
        iv_CallDriver.setOnClickListener(activity);
        iv_SmsDriver.setOnClickListener(activity);
        tv_DriverInfo.setOnClickListener(activity);
        tv_CancelRequest.setOnClickListener(activity);
        ll_ProfileDetail.setOnClickListener(activity);

        ll_Favorite.setOnClickListener(activity);
        iv_Favorite.setOnClickListener(activity);

        checkGPS();

        tvDropoff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields)
//                        .setCountry("AU")
                        .setCountry("IN")
                        .build(activity);
                startActivityForResult(intent, DROPOFF_AUTOCOMPLETE_REQUEST_CODE);
            }
        });

        tvPickup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields)
//                        .setCountry("AU")
                        .setCountry("IN")
                        .build(activity);
                startActivityForResult(intent, PICKUP_AUTOCOMPLETE_REQUEST_CODE);
            }
        });

        auto_Pickup.addTextChangedListener(new TextWatcher() {
            String search_text[];

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

                Log.e("call", "auto_Pickup.addTextChangedListener text = " + s.toString());
                if (s.length() > 0) {
                    addFlag = 1;
                    double latDoulle = 0, longDouble = 0;
                    if (gpsTracker.canGetLocation()) {
                        latDoulle = gpsTracker.getLatitude();
                        longDouble = gpsTracker.getLongitude();
                    }
                    search_text = auto_Pickup.getText().toString().split(",");
                    //jsonurl = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=" + search_text[0].replace(" ", "%20") + "&location=" +latDoulle+","+longDouble + "&radius=1000&sensor=true&key="+Constants.GOOGLE_API_KEY+"&components=&language=en";
                    jsonurl = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=" + search_text[0].replace(" ", "%20") + "&location=" + latDoulle + "," + longDouble + "&radius=1000&sensor=true&key=" + Constants.GOOGLE_API_KEY + "&components=country:au&language=en";
                    Log.e("url", "auto_Pickup autocomplete = " + jsonurl);
                    names = new ArrayList<String>();
                    Place_id_type = new ArrayList<String>();
                    secondryAddress = new ArrayList<String>();
                    if (parse != null) {
                        parse.cancel(true);
                        parse = null;
                    }
                    parse = new ParseData();
                    parse.execute();

                    //https://maps.googleapis.com/maps/api/place/autocomplete/json?input=ban&types=(cities)&key=API_KEY

                    //The way to do that is to add the country as the components parameter to the web service URL:
                    /*StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
                    sb.append("?key=" + API_KEY);
                    sb.append("&components=country:in");*/

                    //cities you can use either the administrative_area_level_3 tag:
                    /*sb.append("&components=administrative_area_level_3:bengaluru");*/

                    //alternatively the locality tag:
                    /*sb.append("&components=locality:redmond");*/

                } else {
                    pickup_Touch = false;
                    TaxiUtil.picup_Long = 0;
                    TaxiUtil.picup_Lat = 0;
                    TaxiUtil.picup_Address = "";

                    setTimeAndPriceToZero();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        auto_Pickup.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (event != null || actionId == EditorInfo.IME_ACTION_DONE) {
                    if (auto_Pickup.getText().toString().trim().length() > 0) {
//                        getLocationFromAddress(auto_Pickup.getText().toString());
                    } else {
                        TaxiUtil.picup_Lat = 0;
                        TaxiUtil.picup_Long = 0;
                        TaxiUtil.picup_Address = "";
                    }
                    // finish();
                }
                return false;
            }
        });

        auto_Dropoff.addTextChangedListener(new TextWatcher() {
            String search_text[];

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (s.length() > 0) {
                    addFlag = 2;
                    double latDoulle = 0, longDouble = 0;

                    if (gpsTracker.canGetLocation()) {
                        latDoulle = gpsTracker.getLatitude();
                        longDouble = gpsTracker.getLongitude();
                    }
                    search_text = auto_Dropoff.getText().toString().split(",");
                    //jsonurl = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=" + search_text[0].replace(" ", "%20") + "&location=" +latDoulle+","+longDouble+ "&radius=1000&sensor=true&key="+Constants.GOOGLE_API_KEY+"&components=&language=en";
                    jsonurl = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=" + search_text[0].replace(" ", "%20") + "&location=" + latDoulle + "," + longDouble + "&radius=1000&sensor=true&key=" + Constants.GOOGLE_API_KEY + "&components=country:au&language=en";
                    Log.e("url", "auto_Dropoff autocomplete = " + jsonurl);
                    names = new ArrayList<String>();
                    Place_id_type = new ArrayList<String>();
                    secondryAddress = new ArrayList<String>();
                    if (parse != null) {
                        parse.cancel(true);
                        parse = null;
                    }
                    parse = new ParseData();
                    parse.execute();
                } else {
                    dropoff_Touch = false;
                    TaxiUtil.dropoff_Long = 0;
                    TaxiUtil.dropoff_Lat = 0;
                    TaxiUtil.dropoff_Address = "";

                    setTimeAndPriceToZero();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        auto_Dropoff.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (event != null || actionId == EditorInfo.IME_ACTION_DONE) {
                    if (auto_Dropoff.getText().toString().trim().length() > 0) {
//                        getLocationFromAddress(auto_Dropoff.getText().toString());
                    } else {
                        TaxiUtil.dropoff_Lat = 0;
                        TaxiUtil.dropoff_Long = 0;
                        TaxiUtil.dropoff_Address = "";
                    }
                    // finish();
                }
                return false;
            }
        });
        connectSocket();
        if (tripFlag == 0) {
            selectedCar = -1;
            myLocation = true;
            ll_BookingLayout.setVisibility(View.VISIBLE);
            ll_AllCarLayout.setVisibility(View.VISIBLE);
            address_Layout.setVisibility(View.VISIBLE);
            ll_AfterRequestAccept.setVisibility(View.GONE);

            if (googleMap != null) {
                googleMap.clear();
            }
        } else {
            ll_BookingLayout.setVisibility(View.GONE);
            ll_AllCarLayout.setVisibility(View.GONE);
            address_Layout.setVisibility(View.GONE);
            ll_AfterRequestAccept.setVisibility(View.VISIBLE);
            if (tripFlag == 1) {
                myLocation = false;
                tv_CancelRequest.setVisibility(View.VISIBLE);
            } else if (tripFlag == 2) {
                myLocation = false;
                tv_CancelRequest.setVisibility(View.GONE);
            }
        }

        ll_ClearDropoff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvDropoff.setText("");
                TaxiUtil.dropoff_Address = "";
                TaxiUtil.dropoff_Lat = 0;
                TaxiUtil.dropoff_Long = 0;
                dropoff_Touch = false;
                favorite = 0;
                favorite_address = "";
                from = "";
                setTimeAndPriceToZero();

                /*if (auto_Pickup.getText().toString().trim().length()>0)
                {
                    auto_Dropoff.setFocusableInTouchMode(true);
                    auto_Dropoff.requestFocus();
                }
                else
                {
                    auto_Pickup.setFocusableInTouchMode(true);
                    auto_Pickup.requestFocus();
                }*/
                getEstimatedFareCheckValidation();
            }
        });

        ll_ClearPickup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvPickup.setText("");
                TaxiUtil.picup_Address = "";
                TaxiUtil.picup_Long = 0;
                TaxiUtil.picup_Lat = 0;
                pickup_Touch = false;
                setTimeAndPriceToZero();
                //auto_Pickup.setFocusableInTouchMode(true);
                //auto_Pickup.requestFocus();
                getEstimatedFareCheckValidation();
            }
        });

        SetDrawer();

    }

    private void setUserDetail() {
        if (ratting > 0) {
            tv_Rate.setText(ratting + "");
        } else {
            tv_Rate.setText("0");
        }

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_IMAGE, MainActivity.this) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_IMAGE, MainActivity.this).equals("")) {
            Picasso.with(activity)
                    .load(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_IMAGE, MainActivity.this))
                    .fit()
                    .transform(mTransformation)
                    .into(iv_ProfileImage);
        } else {
            iv_ProfileImage.setImageResource(R.mipmap.man_demo);
        }

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_FULL_NAME, MainActivity.this) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_FULL_NAME, MainActivity.this).equals("")) {
            tv_UserName.setText(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_FULL_NAME, MainActivity.this));
        } else {
            tv_UserName.setVisibility(View.GONE);
        }

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_PHONE_NUMBER, MainActivity.this) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_PHONE_NUMBER, MainActivity.this).equals("")) {
            tv_UserPhone.setText(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_PHONE_NUMBER, MainActivity.this));
        } else {
            tv_UserPhone.setVisibility(View.GONE);
        }
    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    } // Author: silentnuke

    private void SetDrawer() {

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerListAdapter = new MenuListAdapter(MainActivity.this, menuList_beens, drawer, ll_DrawerLayout);
        lv_drawer.setAdapter(drawerListAdapter);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {

                super.onDrawerClosed(drawerView);
                invalidateOptionsMenu();
                //auto_Dropoff.setEnabled(true);
                //auto_Pickup.setEnabled(true);
                ll_RootLayour.setEnabled(true);
            }

            @Override
            public void onDrawerOpened(View drawerView) {

                super.onDrawerOpened(drawerView);
                //auto_Dropoff.setEnabled(false);
                //auto_Pickup.setEnabled(false);
                ll_RootLayour.setEnabled(false);
                setUserDetail();
                invalidateOptionsMenu();
            }
        };

        drawer.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        iv_Menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawer.isDrawerVisible(GravityCompat.START)) {
                    drawer.closeDrawer(ll_DrawerLayout);
                } else {
                    drawer.openDrawer(ll_DrawerLayout);
                }
//                Uri.Builder directionsBuilder = new Uri.Builder()
//                        .scheme("https")
//                        .authority("www.google.com")
//                        .appendPath("maps")
//                        .appendPath("dir")
//                        .appendPath("")
//                        .appendQueryParameter("api", "1")
//                        .appendQueryParameter("destination", "23.0918312,72.5447241");
//
//                startActivity(new Intent(Intent.ACTION_VIEW, directionsBuilder.build()));
            }
        });

        ll_Menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawer.isDrawerVisible(GravityCompat.START)) {
                    drawer.closeDrawer(ll_DrawerLayout);
                } else {
                    drawer.openDrawer(ll_DrawerLayout);
                }
//                Uri.Builder directionsBuilder = new Uri.Builder()
//                        .scheme("https")
//                        .authority("www.google.com")
//                        .appendPath("maps")
//                        .appendPath("dir")
//                        .appendPath("")
//                        .appendQueryParameter("api", "1")
//                        .appendQueryParameter("destination", "23.0918312,72.5447241");
//
//                startActivity(new Intent(Intent.ACTION_VIEW, directionsBuilder.build()));
            }
        });

        displayImage();

        Log.e("call", "?????????????????????????????????? onCreate ???????????????????????????????");

        iv_MyLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myLocation = true;
                selectedCar = -1;
                carListAdapter.setPosition(selectedCar);
                /*ll_CarBackgroundOne.setBackgroundResource(R.drawable.circle_white_border);
                ll_CarBackgroundTwo.setBackgroundResource(R.drawable.circle_white_border);
                ll_CarBackgroundThree.setBackgroundResource(R.drawable.circle_white_border);
                ll_CarBackgroundFour.setBackgroundResource(R.drawable.circle_white_border);
                ll_CarBackgroundFive.setBackgroundResource(R.drawable.circle_white_border);
                ll_CarBackgroundSix.setBackgroundResource(R.drawable.circle_white_border);*/
                checkGPS();
            }
        });

        getAddressFromLatLong();

        call_DriverInfo();

        String notificationType = SessionSave.getUserSession(Common.PREFRENCE_NOTIFICATION, activity);

        if (fromSplash != null && !fromSplash.equalsIgnoreCase("") && fromSplash.equalsIgnoreCase("Splash_Activity")) {

        } else {
            if (notificationType != null && !notificationType.equalsIgnoreCase("") && notificationType.equalsIgnoreCase("AcceptBooking")) {
                SessionSave.saveUserSession(Common.PREFRENCE_NOTIFICATION, "", activity);
                Intent loginScreen = new Intent(activity, MyBookingActivity.class);
                loginScreen.putExtra("notification", "AcceptBooking");
                startActivity(loginScreen);
            } else if (notificationType != null && !notificationType.equalsIgnoreCase("") && notificationType.equalsIgnoreCase("OnTheWay")) {
                SessionSave.saveUserSession(Common.PREFRENCE_NOTIFICATION, "", activity);
                Intent loginScreen = new Intent(activity, MyBookingActivity.class);
                loginScreen.putExtra("notification", "OnTheWay");
                startActivity(loginScreen);
                finish();
            } else if (notificationType != null && !notificationType.equalsIgnoreCase("") && notificationType.equalsIgnoreCase("RejectBooking")) {
                SessionSave.saveUserSession(Common.PREFRENCE_NOTIFICATION, "", activity);
                Intent loginScreen = new Intent(activity, MyBookingActivity.class);
                loginScreen.putExtra("notification", "RejectBooking");
                startActivity(loginScreen);
                finish();
            } else if (notificationType != null && !notificationType.equalsIgnoreCase("") && notificationType.equalsIgnoreCase("Booking")) {
                SessionSave.saveUserSession(Common.PREFRENCE_NOTIFICATION, "", activity);
                Intent loginScreen = new Intent(activity, MyBookingActivity.class);
                loginScreen.putExtra("notification", "Booking");
                startActivity(loginScreen);
                finish();
            } else if (notificationType != null && !notificationType.equalsIgnoreCase("") && notificationType.equalsIgnoreCase("AdvanceBooking")) {
                SessionSave.saveUserSession(Common.PREFRENCE_NOTIFICATION, "", activity);
                Intent loginScreen = new Intent(activity, MyBookingActivity.class);
                loginScreen.putExtra("notification", "AdvanceBooking");
                startActivity(loginScreen);
                finish();
            } else if (notificationType != null && !notificationType.equalsIgnoreCase("") && notificationType.equalsIgnoreCase("AddMoney")) {
                SessionSave.saveUserSession(Common.PREFRENCE_NOTIFICATION, "", activity);
                Intent loginScreen = new Intent(activity, Wallet_Transfer_History_Activity.class);
                loginScreen.putExtra("notification", "AddMoney");
                startActivity(loginScreen);
                finish();
            } else if (notificationType != null && !notificationType.equalsIgnoreCase("") && notificationType.equalsIgnoreCase("TransferMoney")) {
                SessionSave.saveUserSession(Common.PREFRENCE_NOTIFICATION, "", activity);
                Intent loginScreen = new Intent(activity, Wallet_Transfer_History_Activity.class);
                loginScreen.putExtra("notification", "TransferMoney");
                startActivity(loginScreen);
                finish();
            } else if (notificationType != null && !notificationType.equalsIgnoreCase("") && notificationType.equalsIgnoreCase("Tickpay")) {
                SessionSave.saveUserSession(Common.PREFRENCE_NOTIFICATION, "", activity);
                Intent loginScreen = new Intent(activity, TickPayActivity.class);
                loginScreen.putExtra("notification", "Tickpay");
                startActivity(loginScreen);
                finish();
            }else if (notificationType != null && !notificationType.equalsIgnoreCase("") && notificationType.equalsIgnoreCase("CancelledBooking")) {
                SessionSave.saveUserSession(Common.PREFRENCE_NOTIFICATION, "", activity);
                Intent loginScreen = new Intent(activity, MyBookingActivity.class);
                loginScreen.putExtra("notification", "CancelledBooking");
                startActivity(loginScreen);
                finish();
            }
        }
    }

    public void getAddressFromLatLong() {
        try {
            Log.e("111111111111", "getAddressFromLatLong trip flag = " + tripFlag);

            if (tripFlag == 0) {
                gpsTracker = new GPSTracker(activity);

                if (gpsTracker != null && gpsTracker.canGetLocation()) {
                    Geocoder geocoder;
                    List<Address> addresses;
                    geocoder = new Geocoder(this, Locale.getDefault());

                    double latitude = gpsTracker.getLatitude();
                    double longitude = gpsTracker.getLongitude();

                    if (latitude != 0 && longitude != 0) {
                        getEstimated_lat = latitude;
                        getEstimated_long = longitude;

                        addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

                        String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                        String city = addresses.get(0).getLocality();
                        String state = addresses.get(0).getAdminArea();
                        String country = addresses.get(0).getCountryName();
                        String postalCode = addresses.get(0).getPostalCode();
                        String knownName = addresses.get(0).getFeatureName();

                        Log.e("111111111111", "address = " + address);
                        Log.e("111111111111", "city = " + city);
                        Log.e("111111111111", "state = " + state);
                        Log.e("111111111111", "country = " + country);
                        Log.e("111111111111", "postalCode = " + postalCode);
                        Log.e("111111111111", "knownName = " + knownName);
                        getEstimated_Pickup = address;
                        tvPickup.setText(address);
                        TaxiUtil.picup_Address = address;
                        TaxiUtil.picup_Lat = latitude;
                        TaxiUtil.picup_Long = longitude;
                        pickup_Touch = true;
                        //auto_Pickup.dismissDropDown();

                        pickUpSubArb = "";
                        if (addresses.get(0).getSubLocality() != null) {
                            pickUpSubArb = addresses.get(0).getSubLocality();
                        }
                        if (addresses.get(0).getFeatureName() != null) {
                            if (pickUpSubArb.isEmpty()) {
                                pickUpSubArb = addresses.get(0).getFeatureName();
                            } else {
                                pickUpSubArb = pickUpSubArb + "," + addresses.get(0).getFeatureName();
                            }
                        }
                        if (addresses.get(0).getLocality() != null || addresses.get(0).getSubAdminArea() != null) {
                            if (addresses.get(0).getLocality() != null) {
                                if (pickUpSubArb.isEmpty()) {
                                    pickUpSubArb = addresses.get(0).getLocality();
                                } else {
                                    pickUpSubArb = pickUpSubArb + "," + addresses.get(0).getLocality();
                                }
                            } else {
                                if (pickUpSubArb.isEmpty()) {
                                    pickUpSubArb = addresses.get(0).getSubAdminArea();
                                } else {
                                    pickUpSubArb = pickUpSubArb + "," + addresses.get(0).getSubAdminArea();
                                }
                            }
                        }


                        //auto_Dropoff.setFocusableInTouchMode(true);
                        //auto_Dropoff.requestFocus();
                    }
                }
            }
        } catch (Exception e) {
            Log.e("call", "Exception = " + e.getMessage());
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap1) {

        Log.e("call", "onMapReady()");
        googleMap = googleMap1;
        googleMap.getUiSettings().setZoomControlsEnabled(false);
        MapsInitializer.initialize(MainActivity.this);
//        googleMap.setOnMarkerDragListener(this);
        googleMap.setOnCameraChangeListener(this);

        int tripFlag = 0;
        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity).equalsIgnoreCase("")) {
            tripFlag = Integer.parseInt(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity));
        }
        Log.e("call", "onMapReady tripFlag = " + tripFlag);
        if (tripFlag != 0) {
            if (tripFlag == 1) {
                tv_CancelRequest.setVisibility(View.VISIBLE);
                parseAcceptRequestObject(requestConfirmJsonObject, 1);
            } else if (tripFlag == 2) {
                tv_CancelRequest.setVisibility(View.GONE);
                showPathFromPickupToDropoff(requestConfirmJsonObject);
            }
        } else {
            if (myLocation) {
                googleMap.clear();
                Log.e("call", "initMap 3333333333");
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder().target(coordinate).zoom(17.5f).build()));
            }

            Log.e("call", "onMapReady tripFlag = " + tripFlag);
        }
    }

    public void checkGPS() {
        Log.e("call", "1111111111111");
        if (checkPermission()) {
            gpsTracker = new GPSTracker(activity);
            Log.e("call", "222222222222");
            if (gpsTracker.canGetLocation()) {
                Log.e("call", "3333333333333");
                gpsTracker.getLocation();

                Constants.newgpsLatitude = gpsTracker.getLatitude() + "";
                Constants.newgpsLongitude = gpsTracker.getLongitude() + "";

                Log.e("3333333333333 latitude", "" + Constants.newgpsLatitude);
                Log.e("3333333333333 longitude", "" + Constants.newgpsLongitude);

                getMyLocation();
            } else {
                Log.e("call", "44444444444444");
                AlertMessageNoGps();
            }
        } else {
            Log.e("call", "5555555555555");
            requestPermission();
        }
    }

    private void getCityName() {

        final InitData initData = SessionSave.getInitData();
        /*gpsTracker = new GPSTracker(activity);
        if (checkPermission() && gpsTracker.canGetLocation())
        {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses = null;
            try {
                addresses = geocoder.getFromLocation(gpsTracker.getLatitude(), gpsTracker.getLongitude(), 1);
                //addresses = geocoder.getFromLocation(-37.946048, 145.085055, 1);
                cityName = addresses.get(0).getLocality();
                Log.e("TAG", "***** cityName = "+ cityName);
                if(initData != null && initData.getCityList() != null){
                    for(int  i = 0;i<initData.getCityList().size();i++){
                        if(initData.getCityList().get(i).getCityName().equalsIgnoreCase(cityName)){
                            cityId = initData.getCityList().get(i).getId();
                            callCarListAPi();
                            break;
                        }
                    }
                }
                String stateName = addresses.get(0).getAddressLine(1);
                String countryName = addresses.get(0).getAddressLine(2);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else {*/
        /*if (cityName == null || cityName.equalsIgnoreCase("")) {
            String ip = "";
            Utility.getMACAddress("wlan0");
            Utility.getMACAddress("eth0");
            ip = Utility.getIPAddress(true);
            Log.e("TAG", "***** IP = " + ip);// IPv4
            Log.e("TAG", "***** IP = " + Utility.getIPAddress(false));// IPv6
            String url = "http://ip-api.com/json/" + ip;
            Log.e("TAG", "URL = " + url);
            dialogClass.showDialog();
            aQuery.ajax(url.trim(), null, JSONObject.class, new AjaxCallback<JSONObject>() {

                @Override
                public void callback(String url, JSONObject json, AjaxStatus status) {

                    try {
                        int responseCode = status.getCode();
                        Log.e("TAG", "responseCode = " + responseCode);
                        Log.e("TAG", "Response = " + json);
                        dialogClass.hideDialog();
                        cityName = json.optString("city");
                        Log.w("cityName", "cityName CardList => " + cityName);
                        Log.w("initData.getCityList()", "" + initData.getCityList());
                        boolean isFoundCity = false;
                        if (initData != null && initData.getCityList() != null) {
                            for (int i = 0; i < initData.getCityList().size(); i++) {
                                Log.w("getCityName()", "" + initData.getCityList().get(i).getCityName());
                                if (initData.getCityList().get(i).getCityName().equalsIgnoreCase(cityName)) {
                                    isFoundCity = true;
                                    cityId = initData.getCityList().get(i).getId();
                                    callCarListAPi();
                                    break;
                                }
                            }
                        }

                        if(!isFoundCity){
                            cityId = "4";
                            callCarListAPi();
                        }
                        //{"status":"success","country":"India","countryCode":"IN","region":"GJ","regionName":"Gujarat","city":"Ahmedabad","zip":"380001","lat":23.0276,"lon":72.5871,"timezone":"Asia\/Kolkata","isp":"Reliance Jio Infocomm Limited","org":"RJIL Gujarat LTE SUBSCRIBER PUBLIC","as":"AS55836 Reliance Jio Infocomm Limited","query":"157.32.236.138"}
                    } catch (Exception e) {
                        Log.e("Exception", "Exception " + e.toString());
                        dialogClass.hideDialog();

                    }
                }

            }.method(AQuery.METHOD_GET));
        }*/

        Log.w("KalmalLatLng", "" + kalmanLat + " : " + kalmanLong);
        Log.w("Constants.newgpsLatLng", "" + Constants.newgpsLatitude + " : " + Constants.newgpsLongitude);


        try {
            if (!Constants.newgpsLatitude.equalsIgnoreCase("") && !Constants.newgpsLongitude.equalsIgnoreCase("") && !Constants.newgpsLatitude.equalsIgnoreCase("0") && !Constants.newgpsLongitude.equalsIgnoreCase("0")) {

                String urlLocation = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + Constants.newgpsLatitude + "," + Constants.newgpsLongitude + "&key=" + getString(R.string.map_key);
                Log.w("urlLocation", "" + urlLocation);
                dialogClass.showDialog();
                aQuery.ajax(urlLocation.trim(), null, JSONObject.class, new AjaxCallback<JSONObject>() {

                    @Override
                    public void callback(String url, JSONObject json, AjaxStatus status) {

                        try {
                            int responseCode = status.getCode();
                            Log.e("TAGCityFind", "responseCode = " + responseCode);
                            Log.e("TAGCityFind", "Response = " + json);
                            dialogClass.hideDialog();

                            JSONArray resultsAry = json.getJSONArray("results");
                            JSONObject jObj = new JSONObject(resultsAry.getString(0));
                            JSONArray addCompAry = jObj.getJSONArray("address_components");

                            String short_name = "";
                            int idxCity = -1;
                            boolean isContainsCity = false;

                            for (int adComp = 0; adComp < addCompAry.length(); adComp++) {
                                JSONObject cityObj = addCompAry.getJSONObject(adComp);
                                JSONArray typesAry = cityObj.getJSONArray("types");

                                for (int j = 0; j < typesAry.length(); j++) {
                                    if (typesAry.get(j).toString().equalsIgnoreCase("administrative_area_level_2")) {
                                        isContainsCity = true;
                                        idxCity = adComp;
                                        break;
                                    }
                                }
                            }
                            boolean isFoundCity = false;
                            for (int adComp = 0; adComp < addCompAry.length(); adComp++) {
                                JSONObject cityObj = addCompAry.getJSONObject(adComp);
                                JSONArray typesAry = cityObj.getJSONArray("types");
                                if (typesAry.length() > 1) {
                                    if (/*typesAry.getString(0).equalsIgnoreCase("administrative_area_level_2") &&*/ isContainsCity && adComp == idxCity && typesAry.getString(1).equalsIgnoreCase("political")) {
                                        cityName = cityObj.getString("long_name");
                                        short_name = cityObj.getString("short_name");
                                        for (int i = 0; i < initData.getCityList().size(); i++) {
                                            String listCityName = initData.getCityList().get(i).getCityName().toLowerCase().trim();
                                            Log.w("getCityName()", "" + initData.getCityList().get(i).getCityName());
                                            if (cityName.equalsIgnoreCase(listCityName)) {
                                                isFoundCity = true;
                                                cityId = initData.getCityList().get(i).getId();
                                                callCarListAPi();
                                                break;
                                            } else if (short_name.equalsIgnoreCase(listCityName)) {
                                                cityName = short_name;
                                                isFoundCity = true;
                                                cityId = initData.getCityList().get(i).getId();
                                                callCarListAPi();
                                                break;
                                            }
                                        }
                                        break;
                                    } else if (!isContainsCity && typesAry.getString(0).equalsIgnoreCase("locality") && typesAry.getString(1).equalsIgnoreCase("political")) {
                                        // JSONObject cityObjFinal = new JSONObject(addCompAry.getString(1));
                                        cityName = cityObj.getString("long_name");
                                        for (int i = 0; i < initData.getCityList().size(); i++) {
                                            String listCityName = initData.getCityList().get(i).getCityName().toLowerCase().trim();
                                            Log.w("getCityName()", "" + initData.getCityList().get(i).getCityName());
                                            if (cityName.equalsIgnoreCase(listCityName)) {
                                                isFoundCity = true;
                                                cityId = initData.getCityList().get(i).getId();
                                                callCarListAPi();
                                                break;
                                            } else if (short_name.equalsIgnoreCase(listCityName)) {
                                                isFoundCity = true;
                                                cityName = short_name;
                                                cityId = initData.getCityList().get(i).getId();
                                                callCarListAPi();
                                                break;
                                            }
                                        }
                                        break;
                                    }
                                }
                            }

                            if (!isFoundCity) {
                                cityId = "4";
                                cityName = "Melbourne";
                                callCarListAPi();
                            }

                            Log.w("resultAry", "" + cityName);
                        } catch (JSONException e) {
                            Log.e("Exception", "Exception " + e.toString());
                            dialogClass.hideDialog();

                        }
                    }

                }.method(AQuery.METHOD_GET));

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void callCarListAPi() {
        String url = WebServiceAPI.API_CAR_LIST + cityId;
        Log.e("TAG", "getCarList URL = " + url);
        dialogClass.showDialog();
        aQuery.ajax(url.trim(), null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("TAG", "getCarList responseCode = " + responseCode);
                    Log.e("TAG", "getCarList Response = " + json);
                    dialogClass.hideDialog();

                    if (json.optBoolean("status")) {
                        if (json.has("model_list")) {
                            JSONArray jsonArray = json.getJSONArray("model_list");
                            Log.w("jsonArrayCarList", "" + jsonArray.length());
                            if (jsonArray.length() == 0) {
                                cityId = "4";
                                callCarListAPi();
                            } else {
                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_CAR_CLASS, jsonArray.toString(), activity);
                            }
                        }
                    }
                    //SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_CAR_CLASS,car_class.toString(),activity);
                    //{"status":"success","country":"India","countryCode":"IN","region":"GJ","regionName":"Gujarat","city":"Ahmedabad","zip":"380001","lat":23.0276,"lon":72.5871,"timezone":"Asia\/Kolkata","isp":"Reliance Jio Infocomm Limited","org":"RJIL Gujarat LTE SUBSCRIBER PUBLIC","as":"AS55836 Reliance Jio Infocomm Limited","query":"157.32.236.138"}
                    getCarClass();
                } catch (Exception e) {
                    Log.e("Exception", "Exception " + e.toString());
                    dialogClass.hideDialog();

                }
            }

        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));

    }

    public void initMap() {
        Log.e("call", "initMap");
        try {
            gpsTracker = new GPSTracker(activity);
            coordinate = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());
            GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
            if (googleMap == null) {
                Log.e("call", "initMap googleMap null");
                SupportMapFragment mapFrag = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
                mapFrag.getMapAsync(this);
            } else {
                Log.e("call", "initMap googleMap not null");
                googleMap.getUiSettings().setZoomControlsEnabled(false);
//                googleMap.setOnMarkerDragListener(this);

                MapsInitializer.initialize(MainActivity.this);
                googleMap.setOnCameraChangeListener(this);

                if (marker != null) {
                    marker.remove();
                }

                if (originMarker != null) {
                    originMarker.remove();
                }

                if (destinationMarker != null) {
                    destinationMarker.remove();
                }

                googleMap.clear();
                Log.e("call", "initMap 3333333333");
//                marker = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.current_location)));
//                marker.setDraggable(true);
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder().target(coordinate).zoom(17.5f).build()));
            }
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            Log.e("call", "intMap map load exception" + e.getMessage());
        }
    }

    public void getMyLocation() {
        Log.e("call", "initMap");
        try {
            coordinate = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());
            GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
            if (googleMap == null) {
                Log.e("call", "initMap 11111111111111");
                SupportMapFragment mapFrag = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
                mapFrag.getMapAsync(this);
            } else {
                Log.e("call", "initMap 22222222222");

                googleMap.getUiSettings().setZoomControlsEnabled(false);
                MapsInitializer.initialize(MainActivity.this);
//                googleMap.setOnMarkerDragListener(this);
                googleMap.setOnCameraChangeListener(this);

                if (marker != null) {
                    marker.remove();
                }

                if (originMarker != null) {
                    originMarker.remove();

                }

                if (destinationMarker != null) {
                    destinationMarker.remove();
                }

                googleMap.clear();
                Log.e("call", "initMap 3333333333");
//                marker = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.current_location)));
//                marker.setDraggable(true);
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder().target(coordinate).zoom(17.5f).build()));
            }
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            System.out.println("map load" + e.getMessage());
        }
    }

    private boolean checkPermission() {

        int result = ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.ACCESS_FINE_LOCATION)) {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
        } else {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        Log.e("call", "onRequestPermissionsResult() 11 = " + requestCode);

        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("call", "onRequestPermissionsResult() 22 = ");
                    if (gpsTracker.canGetLocation()) {
                        Log.e("call", "onRequestPermissionsResult() 33 = ");
                        gpsTracker.getLocation();

                        Constants.newgpsLatitude = gpsTracker.getLatitude() + "";
                        Constants.newgpsLongitude = gpsTracker.getLongitude() + "";
                    } else {
                        Log.e("call", "onRequestPermissionsResult() 44 = ");
                        AlertMessageNoGps();
                    }
                } else {
                    Log.e("call", "onRequestPermissionsResult() 55 = ");
                    MyAlertDialog dialog = new MyAlertDialog(MainActivity.this);
                    dialog.setCancelable(false);
                    dialog.setAlertDialog(1, getResources().getString(R.string.permission_denied_for_location));
                }
                break;
        }
    }

    public void AlertMessageNoGps() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(MainActivity.this);
        builder.setCancelable(false);
        builder.setMessage(Html.fromHtml("<font color='#000000'>" + getResources().getString(R.string.new_gps_settings_message) + "</font>"));

        String positiveText = getResources().getString(R.string.new_gps_settings_positive_button);
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(intent);
                    }
                });

        String negativeText = getResources().getString(R.string.new_gps_settings_nagative_button);
        builder.setNegativeButton(negativeText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        final android.app.AlertDialog dialog = builder.create();

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface arg0) {
                dialog.getButton(android.app.AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(MainActivity.this, R.color.colorPrimaryDark));
                dialog.getButton(android.app.AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(MainActivity.this, R.color.colorPrimaryDark));
            }
        });

        dialog.show();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            if (Constants.newgpsLongitude.equals("0") && Constants.newgpsLongitude.equals("0")) {
                gpsTracker = new GPSTracker(activity);

                if (gpsTracker.canGetLocation()) {
                    Log.e("onPostResum", "call");
                }
            }
        }
    }

    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    Log.e("call", "onConnect");

                    if (Common.socket != null && Common.socket.connected()) {
                        int tripFlag = 0;

                        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity).equalsIgnoreCase("")) {
                            tripFlag = Integer.parseInt(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity));
                        }

                        startTimer();
                    }
                }
            });
        }
    };

    private Emitter.Listener onAcceptBookingRequestNotification = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    try {
                        isAlreadySendRequest = false;
                        handlerCount = 0;
                        handlerSendRequest.removeCallbacks(runnableSendRequest, null);
                        if (args != null && args.length > 0) {
                            try {
                                ringTone = MediaPlayer.create(activity, R.raw.tone);
                                ringTone.start();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                            if (commonInternetDialog != null && commonInternetDialog.isShowing()) {
                                commonInternetDialog.hideDialog();
                            }

                            if (commonDialog != null && commonDialog.isShowing()) {
                                commonDialog.dismiss();
                            }

                            advanceBooking = 0;
                            zoomFlag = 0;

                            tripFlag = 1;
                            Log.e("call", "onAcceptBookingRequestNotification response = " + args[0].toString());

                            JSONObject jsonObject = new JSONObject(args[0].toString());
                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, selectedCar + "", activity);
                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, jsonObject.toString(), activity);
                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "1", activity);

                            requestPendingDialogClass.hideDialog();
                            parseAcceptRequestObject(jsonObject, 0);
                        }
                    } catch (Exception e) {
                        Log.e("call", "Exception in onAcceptBookingRequestNotification = " + e.getMessage());
                    }
                }
            });
        }
    };

    public void parseAcceptRequestObject(JSONObject jsonObject, int flag) {
        try {
            Log.e("call", "parseAcceptRequestObject");
            if (jsonObject != null) {
                if (jsonObject.has("BookingInfo")) {
                    JSONArray BookingInfo = jsonObject.getJSONArray("BookingInfo");

                    if (BookingInfo != null && BookingInfo.length() > 0) {
                        JSONObject BookingInfoObject = BookingInfo.getJSONObject(0);

                        if (BookingInfoObject != null) {
                            double lat = 0, lng = 0;

                            String ModelId="";

                            if (BookingInfoObject.has("ModelId")) {
                                ModelId = BookingInfoObject.getString("ModelId");
                            }

                            if (carClass!=null && carClass.length()>0)
                            {
                                for (int i=0; i<carClass.length(); i++)
                                {
                                    JSONObject carObject = carClass.getJSONObject(i);

                                    if (carObject!=null &&
                                            carObject.has("Id") &&
                                            carObject.getString("Id").equalsIgnoreCase(ModelId))
                                    {
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR,i+"",activity);
                                    }
                                }
                            }

                            if (BookingInfoObject.has("PickupLat")) {
                                lat = BookingInfoObject.getDouble("PickupLat");
                            }

                            if (BookingInfoObject.has("PickupLng")) {
                                lng = BookingInfoObject.getDouble("PickupLng");
                            }

                            if (BookingInfoObject.has("Id")) {
                                BookingId = BookingInfoObject.getString("Id");
                            }

                            Log.e("Call", "parseAcceptRequestObject Details ====================================== pickup lat = " + lat);
                            Log.e("Call", "parseAcceptRequestObject Details ====================================== pickup long = " + lng);
                            pickUp = new LatLng(lat, lng);
                        }
                    }
                }

                if (jsonObject.has("DriverInfo")) {
                    JSONArray DriverInfoArray = jsonObject.getJSONArray("DriverInfo");

                    if (DriverInfoArray != null && DriverInfoArray.length() > 0) {
                        JSONObject dropObject = DriverInfoArray.getJSONObject(0);

                        if (dropObject != null) {
                            double lat = 0, lng = 0;

                            if (dropObject.has("Lat")) {
                                lat = Double.parseDouble(dropObject.getString("Lat"));
                            }

                            if (dropObject.has("Lng")) {
                                lng = Double.parseDouble(dropObject.getString("Lng"));
                            }

                            if (lat != 0.0 && lng != 0.0) {
                                Log.e("Call", "parseAcceptRequestObject DriverInfo ====================================== driverLatLng = " + lat);
                                Log.e("Call", "parseAcceptRequestObject DriverInfo ====================================== driverLatLng = " + lng);
                                driverLatLng = new LatLng(lat, lng);//23.0158596,
                            } else {
                                driverLatLng = null;
                            }
                        }
                    }
                }
            }


            requestConfirmJsonObject = new JSONObject(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, activity));

            Log.e("call", "-----------------flag------------ = " + flag);
            if (flag == 1) {
                if (pickUp != null && driverLatLng != null) {
                    Log.e("call", "-----------------showRoute------------ = ");
                    showRoute();
                }
            } else {
                if (requestConfirmJsonObject != null) {
                    ll_BookingLayout.setVisibility(View.GONE);
                    ll_AllCarLayout.setVisibility(View.GONE);
                    ll_AfterRequestAccept.setVisibility(View.VISIBLE);
                    tv_CancelRequest.setVisibility(View.VISIBLE);
                    address_Layout.setVisibility(View.GONE);

                    if (pickUp != null && driverLatLng != null) {
                        Log.e("call", "----------------- displayAcceptRequestPopup showRoute--------------");
                        showRoute();
                    } else {
                        Log.e("call", "----------------- displayAcceptRequestPopup showRoute not call--------------");
                    }

                    Log.e("call", "-----------------displayAcceptRequestPopup requestConfirmJsonObject!=null ------------ = ");
                    String message = "Your booking request has been confirmed";
                    if (requestConfirmJsonObject.has("message")) {
                        message = requestConfirmJsonObject.getString("message");
                    }

                    if (TicktocApplication.getCurrentActivity() != null) {
                        displayAcceptRequestPopup(message, TicktocApplication.getCurrentActivity());
                    } else {
                        displayAcceptRequestPopup(message, activity);
                    }

                }
            }
        } catch (Exception e) {
            Log.e("call", "parseAcceptRequestObject exception = " + e.getMessage());
        }

    }

    public void showPathFromPickupToDropoff(JSONObject jsonObject) {
        try {
            Log.e("call", "showPathFromPickupToDropoff");
            if (jsonObject != null) {
                if (jsonObject.has("BookingInfo")) {
                    JSONArray BookingInfoArray = jsonObject.getJSONArray("BookingInfo");

                    if (BookingInfoArray != null && BookingInfoArray.length() > 0) {
                        JSONObject BookingInfo = BookingInfoArray.getJSONObject(0);

                        if (BookingInfo != null) {
                            double latPickup = 0, lngPickup = 0;
                            double latDropoff = 0, lngDropoff = 0;

                            String ModelId="";

                            if (BookingInfo.has("ModelId")) {
                                ModelId = BookingInfo.getString("ModelId");
                            }

                            if (carClass!=null && carClass.length()>0)
                            {
                                for (int i=0; i<carClass.length(); i++)
                                {
                                    JSONObject carObject = carClass.getJSONObject(i);

                                    if (carObject!=null &&
                                            carObject.has("Id") &&
                                            carObject.getString("Id").equalsIgnoreCase(ModelId))
                                    {
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR,i+"",activity);
                                    }
                                }
                            }

                            if (BookingInfo.has("PickupLat")) {
                                latPickup = BookingInfo.getDouble("PickupLat");
                            }

                            if (BookingInfo.has("PickupLng")) {
                                lngPickup = BookingInfo.getDouble("PickupLng");
                            }

                            if (BookingInfo.has("DropOffLat")) {
                                latDropoff = BookingInfo.getDouble("DropOffLat");
                            }

                            if (BookingInfo.has("DropOffLon")) {
                                lngDropoff = BookingInfo.getDouble("DropOffLon");
                            }

                            if (BookingInfo.has("Id")) {
                                BookingId = BookingInfo.getString("Id");
                            }

                            Log.e("Call", "BookingInfo ====================================== pickup lat = " + latPickup);
                            Log.e("Call", "BookingInfo ====================================== pickup long = " + lngPickup);
                            Log.e("Call", "BookingInfo ====================================== dropoff lat = " + latDropoff);
                            Log.e("Call", "BookingInfo ====================================== dropoff long = " + lngDropoff);

                            pickUp = new LatLng(latDropoff, lngDropoff);
//                            driverLatLng = new LatLng(latPickup, lngPickup);
                        }
                    }
                }

                if (jsonObject.has("DriverInfo")) {
                    JSONArray DriverInfoArray = jsonObject.getJSONArray("DriverInfo");

                    if (DriverInfoArray != null && DriverInfoArray.length() > 0) {
                        JSONObject dropObject = DriverInfoArray.getJSONObject(0);

                        if (dropObject != null) {
                            double lat = 0, lng = 0;

                            if (dropObject.has("Lat")) {
                                lat = Double.parseDouble(dropObject.getString("Lat"));
                            }

                            if (dropObject.has("Lng")) {
                                lng = Double.parseDouble(dropObject.getString("Lng"));
                            }

                            if (lat != 0.0 && lng != 0.0) {
                                Log.e("Call", "parseAcceptRequestObject DriverInfo ====================================== driverLatLng = " + lat);
                                Log.e("Call", "parseAcceptRequestObject DriverInfo ====================================== driverLatLng = " + lng);
                                driverLatLng = new LatLng(lat, lng);//23.0158596,
                            } else {
                                driverLatLng = null;
                            }
                        }
                    }
                }
            }

            requestConfirmJsonObject = new JSONObject(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, activity));

            if (pickUp != null && driverLatLng != null) {
                showRoute();
            }
        } catch (Exception e) {
            Log.e("call", "parseAcceptRequestObject exception = " + e.getMessage());
        }

    }

    public void displayAcceptRequestPopup(String message, Activity activity) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.my_dialog_class);

        TextView tv_Ok = (TextView) dialog.findViewById(R.id.Ok);
        TextView tv_Message = (TextView) dialog.findViewById(R.id.message);
        LinearLayout ll_Ok = (LinearLayout) dialog.findViewById(R.id.ok_layout);

        tv_Message.setText(message);

        ll_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
                if (ringTone != null && ringTone.isPlaying()) {
                    ringTone.stop();
                }
            }
        });

        tv_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (ringTone != null && ringTone.isPlaying()) {
                    ringTone.stop();
                }
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        dialog.show();

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                if (ringTone != null && ringTone.isPlaying()) {
                    ringTone.stop();
                }
            }
        });
        commonDialog = dialog;
    }

    public void showDriverInfoPopup() {
        Log.e("call", "showDriverInfoPopup()");
        if (mBottomSheetBehavior != null) {
            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        }

        String model = "", company = "", pickupLocation = "", dropOffLocation = "", image = "";
        String PickupLocation = "", DropoffLocation = "", driver_name = "", driver_image = "", driver_phone = "";

        try {
            if (requestConfirmJsonObject != null) {
                if (requestConfirmJsonObject.has("BookingInfo")) {
                    JSONArray BookingInfoArray = requestConfirmJsonObject.getJSONArray("BookingInfo");

                    if (BookingInfoArray != null && BookingInfoArray.length() > 0) {
                        JSONObject BookingInfo = BookingInfoArray.getJSONObject(0);

                        if (BookingInfo != null) {
                            if (BookingInfo.has("PickupLocation")) {
                                PickupLocation = BookingInfo.getString("PickupLocation");
                            }

                            if (BookingInfo.has("DropoffLocation")) {
                                DropoffLocation = BookingInfo.getString("DropoffLocation");
                            }
                        }
                    }
                }

                if (requestConfirmJsonObject.has("CarInfo")) {
                    JSONArray CarInfoArray = requestConfirmJsonObject.getJSONArray("CarInfo");

                    if (CarInfoArray != null && CarInfoArray.length() > 0) {
                        JSONObject CarInfo = CarInfoArray.getJSONObject(0);

                        if (CarInfo != null) {
                            if (CarInfo.has("VehicleModel")) {
                                model = CarInfo.getString("VehicleModel");
                            }

                            if (CarInfo.has("Company")) {
                                company = CarInfo.getString("Company");
                            }

                            if (CarInfo.has("Color")) {
                                company = company + " " + CarInfo.getString("Color");
                            }

                            if (CarInfo.has("VehicleImage")) {
                                image = WebServiceAPI.BASE_URL_IMAGE + CarInfo.getString("VehicleImage");
                            }
                        }
                    }
                }

                if (requestConfirmJsonObject.has("DriverInfo")) {
                    JSONArray DriverInfoArray = requestConfirmJsonObject.getJSONArray("DriverInfo");

                    if (DriverInfoArray != null && DriverInfoArray.length() > 0) {
                        JSONObject DriverInfo = DriverInfoArray.getJSONObject(0);

                        if (DriverInfo != null) {
                            if (DriverInfo.has("Fullname")) {
                                driver_name = DriverInfo.getString("Fullname");
                            }

                            if (DriverInfo.has("MobileNo")) {
                                driver_phone = DriverInfo.getString("MobileNo");
                            }

                            if (DriverInfo.has("Image")) {
                                driver_image = WebServiceAPI.BASE_URL_IMAGE + DriverInfo.getString("Image");
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            Log.e("call", "Exception =" + e.getMessage());
        }

        if (model != null && !model.equalsIgnoreCase("")) {
            tv_CarModel.setVisibility(View.VISIBLE);

            String strModel = "";

            if (carType_beens != null && carType_beens.size() > 0) {
                for (int car = 0; car < carType_beens.size(); car++) {
                    if (carType_beens.get(car).getId().equalsIgnoreCase(model)) {
                        strModel = carType_beens.get(car).getName();
                    }
                }
            }

//            if (model.equals("1"))
//            {
//                strModel = "Business Class";
//            }
//            else if (model.equals("2"))
//            {
//                strModel = "Disability";
//            }
//            else if (model.equals("3"))
//            {
//                strModel = "Taxi";
//            }
//            else if (model.equals("4"))
//            {
//                strModel = "First Class";
//            }
//            else if (model.equals("5"))
//            {
//                strModel = "LUX-VAN";
//            }
//            else if (model.equals("6"))
//            {
//                strModel = "Economy";
//            }
//            else
//            {
//                tv_CarModel.setVisibility(View.GONE);
//            }

            if (strModel != null && !strModel.equalsIgnoreCase("")) {
                tv_CarModel.setVisibility(View.VISIBLE);
            } else {
                tv_CarModel.setVisibility(View.VISIBLE);
            }

            tv_CarModel.setText(strModel);
        } else {
            tv_CarModel.setVisibility(View.GONE);
        }

        if (company != null && !company.equalsIgnoreCase("")) {
            tv_CarCompany.setVisibility(View.VISIBLE);
            tv_CarCompany.setText(company);
        } else {
            tv_CarCompany.setVisibility(View.GONE);
        }


        if (PickupLocation != null && !PickupLocation.equalsIgnoreCase("")) {
            tv_Pickup.setVisibility(View.VISIBLE);
            tv_Pickup.setText("Pickup Location : " + PickupLocation);
        } else {
            tv_Pickup.setVisibility(View.GONE);
        }

        if (DropoffLocation != null && !DropoffLocation.equalsIgnoreCase("")) {
            tv_Dropoff.setVisibility(View.VISIBLE);
            tv_Dropoff.setText("Dropoff Location : " + DropoffLocation);
        } else {
            tv_Dropoff.setVisibility(View.GONE);
        }

        if (image != null && !image.trim().equals("")) {
            Picasso.with(activity)
                    .load(image)
                    .fit()
                    .transform(mTransformation)
                    .into(iv_CarImage);
        } else {
            iv_CarImage.setImageResource(R.drawable.car_image);
        }

        if (driver_name != null && !driver_name.trim().equalsIgnoreCase("")) {
            tv_DriverName.setVisibility(View.VISIBLE);
            tv_DriverName.setText(driver_name);
        } else {
            tv_DriverName.setVisibility(View.GONE);
        }

        if (driver_phone != null && !driver_phone.trim().equalsIgnoreCase("")) {
            iv_CallDriver.setVisibility(View.VISIBLE);
            driverPhone = driver_phone;
        } else {
            iv_CallDriver.setVisibility(View.GONE);
            driverPhone = "";
        }

        if (driver_phone != null && !driver_phone.trim().equalsIgnoreCase("")) {
            iv_SmsDriver.setVisibility(View.VISIBLE);
            driverPhone = driver_phone;
        } else {
            iv_SmsDriver.setVisibility(View.GONE);
            driverPhone = "";
        }

        Log.e("call", "image = " + image);

        if (driver_image != null && !driver_image.trim().equals("")) {
            iv_DriverImage.setVisibility(View.VISIBLE);
            Picasso.with(activity)
                    .load(driver_image)
                    .fit()
                    .transform(mTransformation)
                    .into(iv_DriverImage);
        } else {
            iv_DriverImage.setVisibility(View.GONE);
        }
    }

    public void canelBookingRequest() {
        try {
            Log.e("call", "canelBookingRequest");
            if (Common.socket != null && Common.socket.connected()) {
                Log.e("call", "socket connected");
                Log.e("call", "BookingId = " + BookingId);
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("BookingId", BookingId);
                Common.socket.emit("CancelTripByPassenger", jsonObject);
                Log.e("call", "CancelTripByPassenger = " + jsonObject.toString());
                gpsTracker = new GPSTracker(activity);

                coordinate = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());

                if (googleMap != null) {
                    googleMap.clear();
                }

                if (marker != null) {
                    marker.remove();
                }

                if (originMarker != null) {
                    originMarker.remove();
                }

                if (destinationMarker != null) {
                    destinationMarker.remove();
                }

//                marker = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.defaultMarker(Mark)));
//                marker.setDraggable(true);
                googleMap.setOnCameraChangeListener(this);
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder().target(coordinate).zoom(17.5f).build()));

                ll_BookingLayout.setVisibility(View.VISIBLE);
                ll_AllCarLayout.setVisibility(View.VISIBLE);
                ll_AfterRequestAccept.setVisibility(View.GONE);
                address_Layout.setVisibility(View.VISIBLE);

                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "0", activity);
                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, "-1", activity);
                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, "", activity);

                tripFlag = 0;
                selectedCarPosition = -1;
                InternetDialog internetDialog = new InternetDialog(activity);
                internetDialog.showDialog("Your request canceled successfully!");
                selectedCar = -1;
                myLocation = true;

                carListAdapter.setPosition(selectedCar);
                /*ll_CarBackgroundOne.setBackgroundResource(R.drawable.circle_white_border);
                ll_CarBackgroundTwo.setBackgroundResource(R.drawable.circle_white_border);
                ll_CarBackgroundThree.setBackgroundResource(R.drawable.circle_white_border);
                ll_CarBackgroundFour.setBackgroundResource(R.drawable.circle_white_border);
                ll_CarBackgroundFive.setBackgroundResource(R.drawable.circle_white_border);
                ll_CarBackgroundSix.setBackgroundResource(R.drawable.circle_white_border);*/

                handlerDriver.removeCallbacks(runnableDriver, null);

                displayImage();
                getAddressFromLatLong();

                handler.removeCallbacks(runnable, null);
                startTimer();

            }
        } catch (Exception e) {
            Log.e("call", "Exception = " + e.getMessage());
        }
    }

    public void cancelAdvanceBookingRequest() {
        try {
            Log.e("call", "cancelAdvanceBookingRequest");
            if (Common.socket != null && Common.socket.connected()) {
                Log.e("call", "socket connected");
                Log.e("call", "BookingId = " + BookingId);
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("BookingId", BookingId);
                Common.socket.emit("AdvancedBookingCancelTripByPassenger", jsonObject);
                Log.e("call", "AdvancedBookingCancelTripByPassenger = " + jsonObject.toString());
                gpsTracker = new GPSTracker(activity);

                coordinate = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());

                if (googleMap != null) {
                    googleMap.clear();
                }

                if (marker != null) {
                    marker.remove();
                }

                if (originMarker != null) {
                    originMarker.remove();
                }

                if (destinationMarker != null) {
                    destinationMarker.remove();
                }

//                marker = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.current_location)));
//                marker.setDraggable(true);
                googleMap.setOnCameraChangeListener(this);
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder().target(coordinate).zoom(17.5f).build()));

                ll_BookingLayout.setVisibility(View.VISIBLE);
                ll_AllCarLayout.setVisibility(View.VISIBLE);
                ll_AfterRequestAccept.setVisibility(View.GONE);
                address_Layout.setVisibility(View.VISIBLE);

                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "0", activity);
                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, "-1", activity);
                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, "", activity);

                tripFlag = 0;
                selectedCarPosition = -1;
                InternetDialog internetDialog = new InternetDialog(activity);
                internetDialog.showDialog("Your request canceled successfully!");
                selectedCar = -1;
                myLocation = true;

                /*ll_CarBackgroundOne.setBackgroundResource(R.drawable.circle_white_border);
                ll_CarBackgroundTwo.setBackgroundResource(R.drawable.circle_white_border);
                ll_CarBackgroundThree.setBackgroundResource(R.drawable.circle_white_border);
                ll_CarBackgroundFour.setBackgroundResource(R.drawable.circle_white_border);
                ll_CarBackgroundFive.setBackgroundResource(R.drawable.circle_white_border);
                ll_CarBackgroundSix.setBackgroundResource(R.drawable.circle_white_border);*/
                carListAdapter.setPosition(selectedCar);

                handlerDriver.removeCallbacks(runnableDriver, null);

                displayImage();
                getAddressFromLatLong();
                handler.removeCallbacks(runnable, null);
                startTimer();
            }
        } catch (Exception e) {
            Log.e("call", "Exception = " + e.getMessage());
        }
    }

    public void showRoute() {

        if (googleMap != null) {
            googleMap.clear();
        }

        current_marker_layout.setVisibility(View.GONE);

        gpsTracker = new GPSTracker(activity);
        MarkerOptions options = new MarkerOptions();

        options.position(pickUp);
        options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));

        MarkerOptions options1 = new MarkerOptions();
        options1.position(driverLatLng);


        int selectedCar = 0;

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, activity).equalsIgnoreCase("")) {
            selectedCar = Integer.parseInt(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, activity));
        }

        Log.e("call", "showRoute showRoute showRoute === " + selectedCar);

        if (selectedCar == 0) {
            options1.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car_one));
        } else if (selectedCar == 1) {
            options1.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car_two));
        } else if (selectedCar == 2) {
            options1.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car_three));
        } else if (selectedCar == 3) {
            options1.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car_four));
        } else if (selectedCar == 4) {
            options1.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car_five));
        } else if (selectedCar == 5) {
            options1.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car_six));
        }
        options1.anchor(0.5f, 0.5f);

        destinationMarker = googleMap.addMarker(options);
        originMarker = googleMap.addMarker(options1);

        String url = getDirectionsUrl(driverLatLng, pickUp);
        Log.e("url", "--------- " + url);
        DownloadTask downloadTask = new DownloadTask();
        // Start downloading json data from Google Directions API
        downloadTask.execute(url);
        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG,activity)!=null &&
                !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG,activity).equalsIgnoreCase(""))
            tripFlag = Integer.parseInt(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG,activity));

        if (tripFlag==0)
        {
            startTimer();
        }
        else
        {
            startTimerForDriverLocation();
        }
    }

    @Override
    public void onCarClick(int poition, CarType_Been carType_been) {
        Log.e("TAG", "poition = " + poition);
        selectedCar = poition;
        InternetDialog internetDialog = new InternetDialog(activity);
        internetDialog.showDialog(carType_been.getDescription());
        startTimer();
    }

    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            // Traversing through all the routes
            if (result != null) {
                int pointSize = 0;
                for (int i = 0; i < result.size(); i++) {
                    points = new ArrayList<LatLng>();
                    lineOptions = new PolylineOptions();

                    // Fetching i-th route
                    List<HashMap<String, String>> path = result.get(i);

                    // Fetching all the points in i-th route
                    for (int j = 0; j < path.size(); j++) {
                        HashMap<String, String> point = path.get(j);

                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        LatLng position = new LatLng(lat, lng);
                        builder.include(position);
                        points.add(position);
                    }
                    // Adding all the points in the route to LineOptions
                    lineOptions.addAll(points);
                    lineOptions.width(10);

                    if (i == 0) {
                        lineOptions.color(getResources().getColor(R.color.colorPoliLine));
                    } else {
                        lineOptions.color(getResources().getColor(R.color.colorGray));
                    }

                    lineOptions.geodesic(true);
                    lineOptions.zIndex(8);
                    pointSize = pointSize + 1;
                }


                // Drawing polyline in the Google Map for the i-th route

                if (lineOptions != null) {
                    googleMap.addPolyline(lineOptions);

                    Location loc1 = new Location("");
                    loc1.setLatitude(driverLatLng.latitude);
                    loc1.setLongitude(driverLatLng.longitude);

                    Location loc2 = new Location("");
                    loc2.setLatitude(pickUp.latitude);
                    loc2.setLongitude(pickUp.longitude);

                    ////https://stackoverflow.com/questions/38264483/google-map-zoom-in-between-latlng-bounds
                    double circleRad = ((loc1.distanceTo(loc2))) * 1000;//multiply by 1000 to make units in KM

                    float zoomLevel = getZoomLevel(circleRad);

                    int tripFlag = 1;
                    if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity).equalsIgnoreCase("")) {
                        tripFlag = Integer.parseInt(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity));
                    }

                    if (tripFlag == 1) {
                        if (upcomming_dropoff_lat != 0 && upcomming_dropoff_long != 0 && upcomming_pickup_lat != 0 && upcomming_pickup_long != 0) {
                            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder().target(new LatLng(upcomming_pickup_lat, upcomming_pickup_long)).zoom(17.5f).build()));
                        } else {
                            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder().target(pickUp).zoom(17.5f).build()));
                        }
                    } else {
                        if (upcomming_dropoff_lat != 0 && upcomming_dropoff_long != 0 && upcomming_pickup_lat != 0 && upcomming_pickup_long != 0) {
                            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder().target(new LatLng(upcomming_pickup_lat, upcomming_pickup_long)).zoom(17.5f).build()));
                        } else {
//                            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder().target(driverLatLng).zoom(zoomLevel).build()));

                            LatLngBounds bounds = builder.build();
                            int padding = 200; // offset from edges of the map in pixels
                            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                            googleMap.animateCamera(cu);
//                            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Math.abs(pickUp.latitude+driverLatLng.latitude)/2, Math.abs(pickUp.longitude+driverLatLng.longitude)/2), zoomLevel));
                        }
                    }
                } else {
                    InternetDialog internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog("Not able to get a Location, please restart application.");/*due to free api key*/
                }
            } else {
                InternetDialog internetDialog = new InternetDialog(activity);
                internetDialog.showDialog("Not able to get a Location, please restart application.");/* due to free api key please restart application.*/
            }

            upcomming_pickup_long = 0;
            upcomming_pickup_lat = 0;
            upcomming_dropoff_long = 0;
            upcomming_dropoff_lat = 0;
        }
    }

    private int getZoomLevel(double radius) {
        double scale = radius / 500000;
        return ((int) (16 - Math.log(scale) / Math.log(2)));
    }

    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }

            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }

    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        String key = "key=" + Constants.GOOGLE_API_KEY;

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + key + "&" + sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

        return url;
    }

    /**
     * A method to download json data from url
     */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("call", "Exception while downloading url" + e.getMessage());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }

        return data;
    }

    private Emitter.Listener onRejectBookingRequestNotification = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        handlerCount = 0;
                        isAlreadySendRequest = false;
                        handlerSendRequest.removeCallbacks(runnableSendRequest, null);

                        if (requestPendingDialogClass != null) {
                            requestPendingDialogClass.hideDialog();
                        }

                        if (commonInternetDialog != null && commonInternetDialog.isShowing()) {
                            commonInternetDialog.hideDialog();
                        }

                        if (commonDialog != null && commonDialog.isShowing()) {
                            commonDialog.dismiss();
                        }
                        Log.e("call", "onRejectBookingRequestNotification ");
                        advanceBooking = 0;
                        tripFlag = 0;
                        selectedCarPosition = -1;
                        requestConfirmJsonObject = null;
                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "0", activity);
                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, "", activity);
                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, "-1", activity);

                        requestPendingDialogClass.hideDialog();
                        if (args != null && args.length > 0) {
                            Log.e("call", "onRejectBookingRequestNotification() response = " + args[0]);

                            JSONObject jsonObject = new JSONObject(args[0].toString());

                            if (jsonObject != null) {
                                if (jsonObject.has("message")) {
                                    String message = jsonObject.getString("message");
                                    if (TicktocApplication.getCurrentActivity() != null) {
                                        InternetDialog internetDialog = new InternetDialog(TicktocApplication.getCurrentActivity());
                                        internetDialog.showDialog(message);
                                        commonInternetDialog = internetDialog;
                                    } else {
                                        InternetDialog internetDialog = new InternetDialog(activity);
                                        internetDialog.showDialog(message);
                                        commonInternetDialog = internetDialog;
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        Log.e("call", "onRejectBookingRequestNotification exception = " + e.getMessage());
                    }
                }
            });
        }
    };

    private Emitter.Listener onDriverList = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    Log.e("call", "onDriverList()");
                    if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity).equalsIgnoreCase("")) {
                        tripFlag = Integer.parseInt(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity));
                    } else {
                        tripFlag = 0;
                    }

                    Log.e("call", "onDriverList tripFlag = " + tripFlag);

                    if (tripFlag == 0) {
                        String driverIdSession = "";

                        try {
                            latLong_beens.clear();
                            Log.e("call", "onDriverList 111 = ");
                            if (args != null && args.length > 0) {
                                Log.e("call", "onDriverList() = " + args[0].toString());

                                String driverList = args[0].toString();

                                Log.e("call", "onDriverList driverList = " + driverList);
                                if (driverList != null && !driverList.equals("")) {
                                    Log.e("call", "onDriverList 222");
                                    JSONObject jsonObject = new JSONObject(driverList);

                                    if (jsonObject != null) {
                                        Log.e("call", "onDriverList 333");
                                        if (jsonObject.has("driver")) {
                                            Log.e("call", "onDriverList 444");
                                            JSONArray jsonArray = jsonObject.getJSONArray("driver");

                                            if (jsonArray != null && jsonArray.length() > 0) {
                                                Log.e("call", "onDriverList 555");

                                                if (carType_beens.size() > 0) {
                                                    for (int x = 0; x < carType_beens.size(); x++) {
                                                        carType_beens.get(x).setAvai(0);
                                                    }
                                                }

                                                for (int i = 0; i < jsonArray.length(); i++) {
                                                    Log.e("call", "onDriverList 666");
                                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                                    if (jsonObject1 != null) {
                                                        Log.e("call", "onDriverList 777");
                                                        if (jsonObject1.has("DriverId")) {
                                                            Log.e("call", "onDriverList 888");
                                                            if (i == 0) {
                                                                driverIdSession = jsonObject1.getString("DriverId");
                                                            } else if (driverIdSession != null && driverIdSession.equalsIgnoreCase("")) {
                                                                driverIdSession = jsonObject1.getString("DriverId");
                                                            } else {
                                                                driverIdSession = driverIdSession + "," + jsonObject1.getString("DriverId");
                                                            }
                                                        }


                                                        if (jsonObject1.has("CarType")) {
                                                            Log.e("call", "onDriverList 999");
                                                            JSONArray CarType = jsonObject1.getJSONArray("CarType");

                                                            /*if (carType_beens.size()>0)
                                                            {
                                                                for(int x = 0;x<carType_beens.size();x++)
                                                                {
                                                                    carType_beens.get(x).setAvai(0);
                                                                }
                                                            }*/

                                                            if (CarType != null && CarType.length() > 0) {
                                                                for (int j = 0; j < CarType.length(); j++) {
                                                                    if (carType_beens.size() > 0) {
                                                                        for (int x = 0; x < carType_beens.size(); x++) {
                                                                            if (CarType.getString(j).equals(carType_beens.get(x).getId())) {
                                                                                carType_beens.get(x).setAvai(carType_beens.get(x).getAvai() + 1);
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }

                                                            carListAdapter.setCarList(carType_beens);
//                                                            rvCars.setAdapter(carListAdapter);

                                                        }
                                                    }
                                                }

                                                Log.e("call", "driverId = " + driverIdSession);

                                                if (selectedCar >= 0) {
                                                    for (int i = 0; i < jsonArray.length(); i++) {
                                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                                        if (jsonObject1 != null) {
                                                            if (jsonObject1.has("CarType")) {
                                                                JSONArray CarType = jsonObject1.getJSONArray("CarType");

                                                                if (CarType != null && CarType.length() > 0) {
                                                                    int flag = 0;
                                                                    first:
                                                                    for (int k = 0; k < CarType.length(); k++) {
                                                                        if (carType_beens.get(selectedCar).getId().equals(CarType.getString(k))) {
                                                                            flag = 1;
                                                                            break first;
                                                                        }
                                                                    }

                                                                    if (flag == 1) {
                                                                        if (jsonObject1.has("Location")) {
                                                                            JSONArray jsonArray1 = jsonObject1.getJSONArray("Location");

                                                                            if (jsonArray1 != null && jsonArray1.length() > 0) {
                                                                                double lat = 0, longi = 0;

                                                                                for (int j = 0; j < jsonArray1.length(); j++) {
                                                                                    if (j == 0) {
                                                                                        lat = jsonArray1.getDouble(j);
                                                                                    } else {
                                                                                        longi = jsonArray1.getDouble(j);
                                                                                    }
                                                                                }
                                                                                latLong_beens.add(new LatLong_Been(lat, longi));
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }

                                                Log.e("call", "onDriverList latLong_Beens size = " + latLong_beens.size());

                                                if (myLocation == false) {
                                                    if (latLong_beens.size() > 0) {
                                                        Log.e("coordinate", "selectedCar ================= latLong_beens.size = " + latLong_beens.size());

                                                        if (googleMap != null) {
                                                            googleMap.clear();
                                                        }

                                                        for (int i = 0; i < latLong_beens.size(); i++) {
                                                            coordinate = new LatLng(latLong_beens.get(i).getLat(), latLong_beens.get(i).getLong());

                                                            Log.e("coordinate", "coordinate ================= lat = " + coordinate.latitude);
                                                            Log.e("coordinate", "coordinate ================= lat = " + coordinate.longitude);
                                                            Log.e("coordinate", "selectedCar ================= lat = " + selectedCar);

                                                            if (selectedCar == 0) {
                                                                marker = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car_one)));
                                                            } else if (selectedCar == 1) {
                                                                marker = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car_two)));
                                                            } else if (selectedCar == 2) {
                                                                marker = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car_three)));
                                                            } else if (selectedCar == 3) {
                                                                marker = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car_four)));
                                                            } else if (selectedCar == 4) {
                                                                marker = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car_five)));
                                                            } else if (selectedCar == 5) {
                                                                marker = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car_six)));
                                                            }

                                                            if (zoomFlag == 0) {
                                                                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder().target(coordinate).zoom(17.5f).build()));
                                                                zoomFlag = 1;
                                                            }
                                                        }
                                                    }
                                                }
                                            } else {
                                                zoomFlag = 0;
                                                driverIdSession = "";//came this

                                                if (carType_beens.size() > 0) {
                                                    for (int x = 0; x < carType_beens.size(); x++) {
                                                        carType_beens.get(x).setAvai(0);
                                                    }
                                                }

                                                if (googleMap != null) {
                                                    googleMap.clear();
                                                }

                                                setTimeAndPriceToZero();
                                            }
                                        } else {
                                            zoomFlag = 0;
                                            if (googleMap != null) {
                                                googleMap.clear();
                                            }

                                            driverIdSession = "";

                                            if (carType_beens.size() > 0) {
                                                for (int x = 0; x < carType_beens.size(); x++) {
                                                    carType_beens.get(x).setAvai(0);
                                                }
                                            }

                                            setTimeAndPriceToZero();
                                        }
                                    } else {
                                        zoomFlag = 0;
                                        if (googleMap != null) {
                                            googleMap.clear();
                                        }

                                        driverIdSession = "";

                                        if (carType_beens.size() > 0) {
                                            for (int x = 0; x < carType_beens.size(); x++) {
                                                carType_beens.get(x).setAvai(0);
                                            }
                                        }
                                        setTimeAndPriceToZero();
                                    }
                                } else {
                                    zoomFlag = 0;
                                    if (googleMap != null) {
                                        googleMap.clear();
                                    }

                                    driverIdSession = "";

                                    if (carType_beens.size() > 0) {
                                        for (int x = 0; x < carType_beens.size(); x++) {
                                            carType_beens.get(x).setAvai(0);
                                        }
                                    }

                                    setTimeAndPriceToZero();
                                }
                            } else {
                                zoomFlag = 0;
                                if (googleMap != null) {
                                    googleMap.clear();
                                }

                                driverIdSession = "";

                                if (carType_beens.size() > 0) {
                                    for (int x = 0; x < carType_beens.size(); x++) {
                                        carType_beens.get(x).setAvai(0);
                                    }
                                }
                                setTimeAndPriceToZero();
                            }
                            carListAdapter.setCarList(carType_beens);
                        } catch (Exception e) {
                            zoomFlag = 0;
                            Log.e("call", "exception = " + e.getMessage());
                            if (googleMap != null) {
                                googleMap.clear();
                            }

                            driverIdSession = "";
                            if (carType_beens != null && carType_beens.size() > 0) {
                                if (carType_beens.size() > 0) {
                                    for (int x = 0; x < carType_beens.size(); x++) {
                                        carType_beens.get(x).setAvai(0);
                                    }
                                }

                                setTimeAndPriceToZero();

                                carListAdapter.setCarList(carType_beens);
                            } else {
                                setTimeAndPriceToZero();
                                if (carType_beens.size() > 0) {
                                    for (int x = 0; x < carType_beens.size(); x++) {
                                        carType_beens.get(x).setAvai(0);
                                    }
                                }
                                carListAdapter.setCarList(carType_beens);
                            }

                        }

                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_DRIVER_ID, driverIdSession, activity);
                        //getEstimatedFareCheckValidation();//saurav (24-2-2020)
                    }
                }
            });
        }
    };

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.layout_car_one:
                call_One();
                break;

            case R.id.layout_car_two:
                call_Two();
                break;

            case R.id.layout_car_three:
                call_Three();
                break;

            case R.id.layout_car_four:
                call_Four();
                break;

            case R.id.layout_car_five:
                call_Five();
                break;

            case R.id.layout_car_six:
                call_Six();
                break;

            case R.id.book_later_textview:

                call_Booking(1);

                break;

            case R.id.book_now_textview:
                if (isAlreadySendRequest == false) {
                    call_Booking(0);
                } else {
                    InternetDialog internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog("Your can not send request after 30 second");
                }
                break;

            case R.id.cancel_request:
                showYesNoDialog();
                break;

            case R.id.driver_info:
                if (mBottomSheetBehavior != null) {
                    if (requestConfirmJsonObject != null) {
                        showDriverInfoPopup();
                    }
                }
                break;

            case R.id.close:
                if (mBottomSheetBehavior != null) {
                    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                }
                break;

            case R.id.call_to_driver_bottomsheet:
                if (mBottomSheetBehavior != null) {
                    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                }

                if (driverPhone != null && !driverPhone.trim().equalsIgnoreCase("")) {
                    callToDriver();
                } else {
                    mySnackBar.showSnackBar(ll_RootLayour, "Phone number not found!", 0);
                }
                break;

            case R.id.sms_to_driver_bottomsheet:
                if (mBottomSheetBehavior != null) {
                    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                }

                if (driverPhone != null && !driverPhone.trim().equalsIgnoreCase("")) {
                    takePermission();
                } else {
                    mySnackBar.showSnackBar(ll_RootLayour, "Phone number not found!", 0);
                }
                break;

            case R.id.profile_detail_layout:
                if (drawer.isDrawerVisible(GravityCompat.START)) {
                    drawer.closeDrawer(ll_DrawerLayout);
                    gotoProfileScreen();
                }

                break;

            case R.id.favorite_layout:
                if (tvPickup.getText() != null && !tvPickup.getText().toString().trim().equals("") && TaxiUtil.picup_Lat != 0 && TaxiUtil.picup_Long != 0) {
                    if (tvDropoff.getText() != null && !tvDropoff.getText().toString().trim().equals("") && TaxiUtil.dropoff_Lat != 0 && TaxiUtil.dropoff_Long != 0) {
                        AddToFavotie();
                    } else {
                        mySnackBar.showSnackBar(ll_RootLayour, "Please enter a destination!", 0);
                    }
                } else {
                    mySnackBar.showSnackBar(ll_RootLayour, "Please enter a Pickup Location!", 0);
                }
                break;

            case R.id.favorite_image:
                if (tvPickup.getText() != null && !tvPickup.getText().toString().trim().equals("") && TaxiUtil.picup_Lat != 0 && TaxiUtil.picup_Long != 0) {
                    if (tvDropoff.getText() != null && !tvDropoff.getText().toString().trim().equals("") && TaxiUtil.dropoff_Lat != 0 && TaxiUtil.dropoff_Long != 0) {
                        AddToFavotie();
                    } else {
                        mySnackBar.showSnackBar(ll_RootLayour, "Please enter a destination!", 0);
                    }
                } else {
                    mySnackBar.showSnackBar(ll_RootLayour, "Please enter a Pickup Location!", 0);
                }
                break;
        }
    }

    public void showYesNoDialog()
    {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.confirmation_dialog);

        TextView tv_yes = (TextView) dialog.findViewById(R.id.yes);
        TextView tv_No = (TextView) dialog.findViewById(R.id.no);
        TextView tv_Message = (TextView) dialog.findViewById(R.id.message);
        LinearLayout ll_Yes = (LinearLayout) dialog.findViewById(R.id.yes_layout);
        LinearLayout ll_No = (LinearLayout) dialog.findViewById(R.id.no_layout);

        tv_Message.setText("Are you sure you want to cancel your trip?");

        ll_Yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (advanceBooking == 1) {
                    cancelAdvanceBookingRequest();
                } else {
                    canelBookingRequest();
                }
            }
        });

        tv_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (advanceBooking == 1) {
                    cancelAdvanceBookingRequest();
                } else {
                    canelBookingRequest();
                }
            }
        });

        ll_No.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        tv_No.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        dialog.show();
    }

    Runnable runnableSendRequest = new Runnable() {
        @Override
        public void run() {
            isAlreadySendRequest = false;
            handlerCount = 0;
        }
    };

    public void callToDriver() {
        TelephonyManager telMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        int simState = telMgr.getSimState();
        InternetDialog internetDialog = null;
        switch (simState) {
            case TelephonyManager.SIM_STATE_ABSENT:
                internetDialog = new InternetDialog(activity);
                internetDialog.showDialog("Sim card not available");
                break;
            case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
                internetDialog = new InternetDialog(activity);
                internetDialog.showDialog("Sim state network locked");
                break;
            case TelephonyManager.SIM_STATE_PIN_REQUIRED:
                internetDialog = new InternetDialog(activity);
                internetDialog.showDialog("Sim state pin required");
                break;
            case TelephonyManager.SIM_STATE_PUK_REQUIRED:
                internetDialog = new InternetDialog(activity);
                internetDialog.showDialog("Sim state puk required");
                break;
            case TelephonyManager.SIM_STATE_READY:
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + driverPhone));
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                startActivity(intent);
                break;
            case TelephonyManager.SIM_STATE_UNKNOWN:
                internetDialog = new InternetDialog(activity);
                internetDialog.showDialog("Sim state unknown");
                break;
        }
    }

    public void getEstimatedFareCheckValidation() {
        Log.e("call", "getEstimatedFareCheckValidation() pickup_Touch = " + pickup_Touch);
        Log.e("call", "getEstimatedFareCheckValidation() getEstimated_Pickup = " + getEstimated_Pickup);
        Log.e("call", "getEstimatedFareCheckValidation() getEstimated_lat = " + getEstimated_lat);
        Log.e("call", "getEstimatedFareCheckValidation() getEstimated_long = " + getEstimated_long);
        Log.e("call", "getEstimatedFareCheckValidation() timeFlag = " + timeFlag);
        Log.e("call", "getEstimatedFareCheckValidation() TaxiUtil.picup_Address = " + TaxiUtil.picup_Address);
        Log.e("call", "getEstimatedFareCheckValidation() auto_Pickup.getText() = " + tvPickup.getText());
        /*if (pickup_Touch==false)
        {
            if (getEstimated_Pickup!=null && !getEstimated_Pickup.toString().trim().equals("") &&
                    getEstimated_lat!=0 && getEstimated_long!=0)
            {
//                if (timeFlag == 0)
//                {
//                    timeFlag = 1;
//                    findPriceAndTime();
//                }
//                else
//                {
//                    timeFlag = 0;
//                    Log.e("timeFlag == "," === "+timeFlag);
//                }
                findPriceAndTime();
            }
        }
        else
        {
            if (tvPickup.getText()!=null && !tvPickup.getText().toString().trim().equals("") &&
                    TaxiUtil.picup_Address!=null && !TaxiUtil.picup_Address.trim().equals("") && pickup_Touch==true)
            {
//                if (timeFlag == 0)
//                {
//                    timeFlag = 1;
//                    findPriceAndTime();
//                }
//                else
//                {
//                    timeFlag = 0;
//                }
                findPriceAndTime();
            }
        }*/

        if (tvPickup.getText() != null && !tvPickup.getText().toString().trim().equals("") &&
                tvDropoff.getText() != null && !tvDropoff.getText().toString().trim().equals("")) {
            findPriceAndTime();
        }
    }

    private void AddToFavotie() {
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.favorite_option_dialog, null);

        final Dialog dialog = new Dialog(activity, 0);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(view);

        LinearLayout ll_Home, ll_Office, ll_Airport, ll_Others;

        ll_Home = (LinearLayout) dialog.findViewById(R.id.favorite_option_home);
        ll_Office = (LinearLayout) dialog.findViewById(R.id.favorite_option_office);
        ll_Airport = (LinearLayout) dialog.findViewById(R.id.favorite_option_airport);
        ll_Others = (LinearLayout) dialog.findViewById(R.id.favorite_option_others);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        dialog.show();

        ll_Home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (Global.isNetworkconn(activity)) {
                    call_AddToFavorite("Home", tvDropoff.getText().toString(), TaxiUtil.dropoff_Lat, TaxiUtil.dropoff_Long);
                } else {
                    InternetDialog internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog("Please check your internet connection!");
                }

            }
        });

        ll_Office.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (Global.isNetworkconn(activity)) {
                    call_AddToFavorite("Office", tvDropoff.getText().toString(), TaxiUtil.dropoff_Lat, TaxiUtil.dropoff_Long);
                } else {
                    InternetDialog internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog("Please check your internet connection!");
                }
            }
        });

        ll_Airport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (Global.isNetworkconn(activity)) {
                    call_AddToFavorite("Airport", tvDropoff.getText().toString(), TaxiUtil.dropoff_Lat, TaxiUtil.dropoff_Long);
                } else {
                    InternetDialog internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog("Please check your internet connection!");
                }
            }
        });

        ll_Others.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (Global.isNetworkconn(activity)) {
                    call_AddToFavorite("Others", tvDropoff.getText().toString(), TaxiUtil.dropoff_Lat, TaxiUtil.dropoff_Long);
                } else {
                    InternetDialog internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog("Please check your internet connection!");
                }
            }
        });
    }

    public void call_AddToFavorite(String type, String address, double latitude, double longitude) {
        dialogClass.showDialog();
        String url = WebServiceAPI.API_ADD_ADDRESS;//PassengerId,Type,Address,Lat,Lng

        Map<String, Object> params = new HashMap<String, Object>();
        params.put(WebServiceAPI.PARAM_PASSENGER_ID, SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID, activity));
        params.put(WebServiceAPI.PARAM_TYPE, type);
        params.put(WebServiceAPI.PARAM_ADDRESS, address);
        params.put(WebServiceAPI.PARAM_LAT, latitude);
        params.put(WebServiceAPI.PARAM_LONG, longitude);

        Log.e("call", "url = " + url);
        Log.e("call", "params = " + params);

        aQuery.ajax(url.trim(), params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("responseCode", " = " + responseCode);
                    Log.e("Response", " = " + json);

                    if (json != null) {
                        if (json.has("status")) {
                            if (json.getBoolean("status")) {
                                String message = "Address added to favourite.";
                                if (json.has("message")) {
                                    message = json.getString("message");
                                }
                                dialogClass.hideDialog();
                                InternetDialog internetDialog = new InternetDialog(activity);
                                internetDialog.showDialog(message);
                            } else {
                                String message = "Something went wrong prlease try again later";
                                if (json.has("message")) {
                                    message = json.getString("message");
                                }
                                dialogClass.hideDialog();
                                InternetDialog internetDialog = new InternetDialog(activity);
                                internetDialog.showDialog(message);
                            }
                        } else {
                            dialogClass.hideDialog();
                            InternetDialog internetDialog = new InternetDialog(activity);
                            internetDialog.showDialog("Something went wrong prlease try again later");
                        }
                    } else {
                        dialogClass.hideDialog();
                        InternetDialog internetDialog = new InternetDialog(activity);
                        internetDialog.showDialog("Something went wrong prlease try again later");
                    }
                } catch (Exception e) {
                    Log.e("Exception", "Exception " + e.toString());
                    dialogClass.hideDialog();
                    InternetDialog internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog("Something went wrong prlease try again later");
                }
            }

        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    public void gotoProfileScreen() {
        runnableProfile = new Runnable() {

            @Override
            public void run() {

                Intent intent = new Intent(MainActivity.this, UpdateProfileActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                handlerProfile.removeCallbacks(runnableProfile, null);
            }
        };
        handlerProfile.postDelayed(runnableProfile, 300);
    }

    public void call_DriverInfo() {
        FrameLayout parentThatHasBottomSheetBehavior = (FrameLayout) ll_BottomSheet.getParent().getParent();
        mBottomSheetBehavior = BottomSheetBehavior.from(parentThatHasBottomSheetBehavior);

        if (mBottomSheetBehavior != null) {
            mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState) {

                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {

                }
            });
        }
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
    }

    public void showCreaditCardPopup() {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.yes_no_dialog_layout);

        TextView tv_Message = (TextView) dialog.findViewById(R.id.message);

        TextView tv_Yes = (TextView) dialog.findViewById(R.id.yes);
        TextView tv_No = (TextView) dialog.findViewById(R.id.no);

        LinearLayout ll_Yes = (LinearLayout) dialog.findViewById(R.id.yes_layout);
        LinearLayout ll_No = (LinearLayout) dialog.findViewById(R.id.no_layout);

        tv_Message.setText("Please add card first.");

        ll_No.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                //openPromocodePopup();
            }
        });

        tv_No.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                //openPromocodePopup();
            }
        });

        ll_Yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                addCardFlag = 1;
                Intent intent = new Intent(activity, Add_Card_In_List_Activity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.no_change);

            }
        });

        tv_Yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                addCardFlag = 1;
                Intent intent = new Intent(activity, Add_Card_In_List_Activity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.no_change);

            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        dialog.show();
    }

    public boolean checkCardList() {
        try {
            Log.e("call", "111111111111111");
            String strCardList = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_CARD_LIST, activity);
            List<CreditCard_List_Been> cardListBeens = new ArrayList<CreditCard_List_Been>();

            if (strCardList != null && !strCardList.equalsIgnoreCase("")) {
                Log.e("call", "222222222222222");
                JSONObject json = new JSONObject(strCardList);

                if (json != null) {
                    Log.e("call", "33333333333333");
                    if (json.has("cards")) {
                        Log.e("call", "6666666666666666");
                        JSONArray cards = json.getJSONArray("cards");

                        if (cards != null && cards.length() > 0) {
                            return true;
                        } else {
                            Log.e("call", "no cards available");
                            return false;
                        }
                    } else {
                        Log.e("call", "no cards found");
                        return false;
                    }
                } else {
                    Log.e("call", "json null");
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception e) {
            Log.e("call", "Exception in getting card list = " + e.getMessage());
            return false;
        }
    }

    private void call_Booking(int flag) {
        Log.w("carType_beens", "" + carType_beens);
        if (flag == 0) {
            if (tvPickup.getText() != null && !tvPickup.getText().toString().trim().equals("") &&
                    TaxiUtil.picup_Address != null && !TaxiUtil.picup_Address.trim().equals("")) {
                if (tvDropoff.getText() != null && !tvDropoff.getText().toString().trim().equals("") &&
                        TaxiUtil.dropoff_Address != null && !TaxiUtil.dropoff_Address.trim().equals("")) {
                    if (pickup_Touch == true) {
                        if (dropoff_Touch == true) {
                            if (selectedCar != -1) {
                                if (carType_beens.get(selectedCar).getAvai() > 0) {
                                    if (checkCardList()) {
                                        openPromocodePopup();
                                    } else {
                                        showCreaditCardPopup();
                                    }
                                } else {
                                    Log.e("call", "from booknow onBookLater();");

                                    //onBookLater();saurav(24-2-2020)
                                }
                            } else {
                                mySnackBar.showSnackBar(ll_RootLayour, "Please select car model!", 0);
                            }
                        } else {
                            mySnackBar.showSnackBar(ll_RootLayour, "Invalid destination location!", 0);
                            //auto_Dropoff.setFocusableInTouchMode(true);
                            //auto_Dropoff.requestFocus();
                        }
                    } else {
                        mySnackBar.showSnackBar(ll_RootLayour, "Invalid pickup location!", 0);
                        //auto_Pickup.setFocusableInTouchMode(true);
                        //auto_Pickup.requestFocus();
                    }
                } else {
                    mySnackBar.showSnackBar(ll_RootLayour, "Please enter a valid destination!", 0);
                    //auto_Dropoff.setFocusableInTouchMode(true);
                    //auto_Dropoff.requestFocus();
                }
            } else {
                mySnackBar.showSnackBar(ll_RootLayour, "Please enter a valid Pickup Location!", 0);
                //auto_Pickup.setFocusableInTouchMode(true);
                //auto_Pickup.requestFocus();
            }
        } else {
            if (selectedCar >= 0) {
                if (tvPickup.getText().toString().trim().equalsIgnoreCase("")) {
                    TaxiUtil.picup_Address = "";
                    TaxiUtil.picup_Long = 0;
                    TaxiUtil.picup_Lat = 0;
                } else if (tvDropoff.getText().toString().trim().equalsIgnoreCase("")) {
                    TaxiUtil.dropoff_Address = "";
                    TaxiUtil.dropoff_Long = 0;
                    TaxiUtil.dropoff_Lat = 0;
                } else {
                    isBookLaterActivate = true;
                }

                from = "BookLater";
                Intent intent = new Intent(MainActivity.this, BookLaterActivity.class);
                intent.putExtra("pickUpSubArb", pickUpSubArb);
                intent.putExtra("dropOffSubAArb", dropOffSubAArb);
                startActivity(intent);
            } else {
                mySnackBar.showSnackBar(ll_RootLayour, "Please select car!", 0);
            }
        }
    }

    /*public void call_BookLaterDialog(String message)
    {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.yes_no_dialog_layout);

        TextView tv_Ok = (TextView) dialog.findViewById(R.id.yes);
        TextView tv_Cancel = (TextView) dialog.findViewById(R.id.no);
        TextView tv_Message = (TextView) dialog.findViewById(R.id.message);
        LinearLayout ll_Ok = (LinearLayout) dialog.findViewById(R.id.yes_layout);
        LinearLayout ll_Cancel = (LinearLayout) dialog.findViewById(R.id.no_layout);

        tv_Ok.setText(getResources().getString(R.string.ok));
        tv_Cancel.setText(getResources().getString(R.string.cancel));

        tv_Message.setText(message);

        ll_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                call_Booking(1);
            }
        });

        tv_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                call_Booking(1);
            }
        });

        ll_Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        tv_Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {

                if (MainActivity.ringTone!=null)
                {
                    MainActivity.ringTone.stop();
                }
            }
        });

        dialog.show();
    }*/

    private void call_BookNow() {
        dialogClass.showDialog();
        String url = WebServiceAPI.API_BOOKING_REQUEST;

        bookingRequest_beens.clear();
        //Booking now parameter
        // PassengerId,ModelId,PickupLocation,DropoffLocation,PickupLat,PickupLng,DropOffLat,DropOffLon;

        Map<String, Object> params = new HashMap<String, Object>();
        params.put(WebServiceAPI.PARAM_PASSENGER_ID, passangerId);
        params.put(WebServiceAPI.PARAM_MODEL_ID, carType_beens.get(selectedCar).getId());
        params.put(WebServiceAPI.PARAM_PICKUP_LOCATION, TaxiUtil.picup_Address);
        params.put(WebServiceAPI.PARAM_DROPOFF_LOCATION, TaxiUtil.dropoff_Address);
        params.put(WebServiceAPI.PARAM_PICKUP_LAT, TaxiUtil.picup_Lat);
        params.put(WebServiceAPI.PARAM_PICKUP_LONG, TaxiUtil.picup_Long);
        params.put(WebServiceAPI.PARAM_DROPOFF_LAT, TaxiUtil.dropoff_Lat);
        params.put(WebServiceAPI.PARAM_DROPOFF_LONG, TaxiUtil.dropoff_Long);
        params.put(WebServiceAPI.PARAM_PROMO_CODE, promocodeStr);
        params.put(WebServiceAPI.PARAM_NOTES, notes);
        params.put(WebServiceAPI.PARAM_PAYMENT_TYPE, selectedPaymentType);
        params.put(WebServiceAPI.PARAM_BABYSEAT, isBabySeatInclude ? "1" : "0");
        params.put(WebServiceAPI.PARAM_CITYID, cityId);
        if (selectedPaymentType.equalsIgnoreCase("card")) {
            params.put(WebServiceAPI.PARAM_CARD_ID, cardId);
        }
        Log.e("call", "cardNumber = " + cardNumber);
        Log.e("call", "cardId = " + cardId);
        Log.e("call", "selectedPaymentType = " + selectedPaymentType);
        Log.e("call", "url = " + url);

        if (pickUpSubArb != null && dropOffSubAArb != null) {
            params.put(WebServiceAPI.PARAM_PICKUP_SUBURB, pickUpSubArb);
            params.put(WebServiceAPI.PARAM_DROPOFF_SUBURB, dropOffSubAArb);
        }
        Log.e("call", "param = " + params);

        aQuery.ajax(url.trim(), params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("responseCode", " = " + responseCode);
                    Log.e("Response", " = " + json);

                    if (json != null) {
                        if (json.has("status")) {
                            if (json.getBoolean("status")) {
                                isAlreadySendRequest = true;
                                handlerCount = 1;
                                handlerSendRequest.postDelayed(runnableSendRequest, 30000);
                                if (json.has("details")) {
                                    JSONObject details = json.getJSONObject("details");

                                    if (details != null) {
                                        String PassengerId = "", ModelId = "", PickupLocation = "", DropoffLocation = "";
                                        double PickupLat = 0, PickupLng = 0, DropOffLat = 0, DropOffLon = 0;
                                        String CreatedDate = "", Status = "", BookingId = "";

                                        if (details.has("PassengerId")) {
                                            PassengerId = details.getString("PassengerId");
                                        }

                                        if (details.has("ModelId")) {
                                            ModelId = details.getString("ModelId");
                                        }

                                        if (details.has("PickupLocation")) {
                                            PickupLocation = details.getString("PickupLocation");
                                        }

                                        if (details.has("DropoffLocation")) {
                                            DropoffLocation = details.getString("DropoffLocation");
                                        }

                                        if (details.has("PickupLat")) {
                                            PickupLat = details.getDouble("PickupLat");
                                        }

                                        if (details.has("PickupLng")) {
                                            PickupLng = details.getDouble("PickupLng");
                                        }

                                        if (details.has("DropOffLat")) {
                                            DropOffLat = details.getDouble("DropOffLat");
                                        }

                                        if (details.has("DropOffLon")) {
                                            DropOffLon = details.getDouble("DropOffLon");
                                        }

                                        if (details.has("CreatedDate")) {
                                            CreatedDate = details.getString("CreatedDate");
                                        }

                                        if (details.has("Status")) {
                                            Status = details.getString("Status");
                                        }

                                        if (details.has("BookingId")) {
                                            BookingId = details.getString("BookingId");
                                        }

                                        bookingRequest_beens.add(new BookingRequest_Been(
                                                PassengerId,
                                                ModelId,
                                                PickupLocation,
                                                DropoffLocation,
                                                PickupLat,
                                                PickupLng,
                                                DropOffLat,
                                                DropOffLon,
                                                CreatedDate,
                                                Status,
                                                BookingId
                                        ));

                                        dialogClass.hideDialog();
                                        requestPendingDialogClass.showDialog();
                                    } else {
                                        dialogClass.hideDialog();
                                    }
                                } else {
                                    dialogClass.hideDialog();
                                }
                            } else {
                                isAlreadySendRequest = false;
                                handlerCount = 0;
                                dialogClass.hideDialog();
                                if (json.has("message")) {
                                    //1111111111111111111111111111111111111111111111111
                                    if (json.get("message").toString().contains("past dues")) {
                                        InternetDialog internetDialog = new InternetDialog(activity);
                                        internetDialog.showPastDueDialog(json.getString("message"));
                                    } else {
                                        InternetDialog internetDialog = new InternetDialog(activity);
                                        internetDialog.showDialog(json.getString("message"));
                                    }
                                } else {
                                    InternetDialog internetDialog = new InternetDialog(activity);
                                    internetDialog.showDialog("Something went wrong.");
                                }
                            }
                        } else {
                            isAlreadySendRequest = false;
                            handlerCount = 0;
                            dialogClass.hideDialog();
                            InternetDialog internetDialog = new InternetDialog(activity);
                            internetDialog.showDialog("Something went wrong!");
                        }
                    } else {
                        isAlreadySendRequest = false;
                        handlerCount = 0;
                        dialogClass.hideDialog();
                        InternetDialog internetDialog = new InternetDialog(activity);
                        internetDialog.showDialog("Something went wrong!");
                    }
                } catch (Exception e) {
                    isAlreadySendRequest = false;
                    handlerCount = 0;
                    Log.e("Exception", "Exception " + e.toString());
                    dialogClass.hideDialog();
                    InternetDialog internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog("Something went wrong!");
                }
            }
        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }


    //For first car
    private void call_One() {
        current_marker_layout.setVisibility(View.GONE);
        zoomFlag = 0;
        myLocation = false;
        if (marker != null) {
            marker.remove();
        }
        handler.removeCallbacks(runnable, null);
        //selectedCar = 0;
        ll_CarBackgroundOne.setBackgroundResource(R.drawable.circle_red_border);
        ll_CarBackgroundTwo.setBackgroundResource(R.drawable.circle_white_border);
        ll_CarBackgroundThree.setBackgroundResource(R.drawable.circle_white_border);
        ll_CarBackgroundFour.setBackgroundResource(R.drawable.circle_white_border);
        ll_CarBackgroundFive.setBackgroundResource(R.drawable.circle_white_border);
        ll_CarBackgroundSix.setBackgroundResource(R.drawable.circle_white_border);

        startTimer();
    }

    //For second car
    private void call_Two() {
        current_marker_layout.setVisibility(View.GONE);
        zoomFlag = 0;
        myLocation = false;
        if (marker != null) {
            marker.remove();
        }
        handler.removeCallbacks(runnable);
        //selectedCar = 1;
        ll_CarBackgroundOne.setBackgroundResource(R.drawable.circle_white_border);
        ll_CarBackgroundTwo.setBackgroundResource(R.drawable.circle_red_border);
        ll_CarBackgroundThree.setBackgroundResource(R.drawable.circle_white_border);
        ll_CarBackgroundFour.setBackgroundResource(R.drawable.circle_white_border);
        ll_CarBackgroundFive.setBackgroundResource(R.drawable.circle_white_border);
        ll_CarBackgroundSix.setBackgroundResource(R.drawable.circle_white_border);
        startTimer();
    }

    //For third car
    private void call_Three() {
        current_marker_layout.setVisibility(View.GONE);
        zoomFlag = 0;
        myLocation = false;
        if (marker != null) {
            marker.remove();
        }
        handler.removeCallbacks(runnable);
        //selectedCar = 2;
        ll_CarBackgroundOne.setBackgroundResource(R.drawable.circle_white_border);
        ll_CarBackgroundTwo.setBackgroundResource(R.drawable.circle_white_border);
        ll_CarBackgroundThree.setBackgroundResource(R.drawable.circle_red_border);
        ll_CarBackgroundFour.setBackgroundResource(R.drawable.circle_white_border);
        ll_CarBackgroundFive.setBackgroundResource(R.drawable.circle_white_border);
        ll_CarBackgroundSix.setBackgroundResource(R.drawable.circle_white_border);
        startTimer();
    }

    //For four car
    private void call_Four() {
        current_marker_layout.setVisibility(View.GONE);
        zoomFlag = 0;
        myLocation = false;
        if (marker != null) {
            marker.remove();
        }
        handler.removeCallbacks(runnable);
        //selectedCar = 3;
        ll_CarBackgroundOne.setBackgroundResource(R.drawable.circle_white_border);
        ll_CarBackgroundTwo.setBackgroundResource(R.drawable.circle_white_border);
        ll_CarBackgroundThree.setBackgroundResource(R.drawable.circle_white_border);
        ll_CarBackgroundFour.setBackgroundResource(R.drawable.circle_red_border);
        ll_CarBackgroundFive.setBackgroundResource(R.drawable.circle_white_border);
        ll_CarBackgroundSix.setBackgroundResource(R.drawable.circle_white_border);
        startTimer();
    }

    //For Fifth car
    private void call_Five() {
        current_marker_layout.setVisibility(View.GONE);
        zoomFlag = 0;
        myLocation = false;
        if (marker != null) {
            marker.remove();
        }
        handler.removeCallbacks(runnable);
        //selectedCar = 4;
        ll_CarBackgroundOne.setBackgroundResource(R.drawable.circle_white_border);
        ll_CarBackgroundTwo.setBackgroundResource(R.drawable.circle_white_border);
        ll_CarBackgroundThree.setBackgroundResource(R.drawable.circle_white_border);
        ll_CarBackgroundFour.setBackgroundResource(R.drawable.circle_white_border);
        ll_CarBackgroundFive.setBackgroundResource(R.drawable.circle_red_border);
        ll_CarBackgroundSix.setBackgroundResource(R.drawable.circle_white_border);
        startTimer();
    }

    //For first car
    private void call_Six() {
        current_marker_layout.setVisibility(View.GONE);
        zoomFlag = 0;
        myLocation = false;
        if (marker != null) {
            marker.remove();
        }
        handler.removeCallbacks(runnable);
        //selectedCar = 5;
        ll_CarBackgroundOne.setBackgroundResource(R.drawable.circle_white_border);
        ll_CarBackgroundTwo.setBackgroundResource(R.drawable.circle_white_border);
        ll_CarBackgroundThree.setBackgroundResource(R.drawable.circle_white_border);
        ll_CarBackgroundFour.setBackgroundResource(R.drawable.circle_white_border);
        ll_CarBackgroundFive.setBackgroundResource(R.drawable.circle_white_border);
        ll_CarBackgroundSix.setBackgroundResource(R.drawable.circle_red_border);
        startTimer();
    }

//    public void getLocationFromAddressPickup(String strAddress){
//
//        Geocoder coder = new Geocoder(this);
//        List<Address> address;
//        Barcode.GeoPoint p1 = null;
//
//        try
//        {
//            address = coder.getFromLocationName(strAddress,1);
//
//            if (address!=null)
//            {
//                if (address.size()>0)
//                {
//                    Address location=address.get(0);
//                    location.getLatitude();
//                    location.getLongitude();
//
//                    p1 = new Barcode.GeoPoint((double) (location.getLatitude() * 1E6),
//                            (double) (location.getLongitude() * 1E6));
//                }
//            }
//        }
//        catch (Exception e)
//        {
//            Log.e("call","exception getLocationFromAddress = "+e.getMessage());
//        }
//    }

    /////////////for pickup location......................................................................

    // Get the lat and lng from given address
    public void getLocationFromAddress(String strAddress) {
        try {
            if (addFlag == 1) {
                if (auto_Pickup.getText().toString().trim().length() > 0) {
                    TaxiUtil.picup_Address = auto_Pickup.getText().toString();
                    String encode_url = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + place_id + "&key=" + Constants.GOOGLE_API_KEY;

                    System.out.println("encode_url" + encode_url);
                    new GetGeoCoderAddress(encode_url, strAddress).execute();
                }
            } else if (addFlag == 2) {
                if (auto_Dropoff.getText().toString().trim().length() > 0) {
                    TaxiUtil.dropoff_Address = auto_Dropoff.getText().toString();
                    String encode_url = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + place_id + "&key=" + Constants.GOOGLE_API_KEY;

                    System.out.println("encode_url" + encode_url);
                    new GetGeoCoderAddress(encode_url, strAddress).execute();
                }
            }
        } catch (IndexOutOfBoundsException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            // TODO: handle exception
            if (addFlag == 1) {
                TaxiUtil.picup_Lat = 0.0;
                TaxiUtil.picup_Long = 0.0;
                TaxiUtil.picup_Address = "";
            } else if (addFlag == 2) {
                TaxiUtil.dropoff_Lat = 0.0;
                TaxiUtil.dropoff_Long = 0.0;
                TaxiUtil.dropoff_Address = "";
            }
        }
    }

    // Parse the google API result and loaded into list
    public class ParseData extends AsyncTask<Void, Integer, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            // TODO Auto-generated method stub
            try {

                json = GetAddress(jsonurl.toString());

                Log.e("call", "json ParseData = " + json.toString());

                if (json != null) {
                    names.clear();
                    Place_id_type.clear();
                    secondryAddress.clear();
                    // Getting Array of Contacts
                    contacts = json.getJSONArray(TAG_RESULT);
                    for (int i = 0; i < contacts.length(); i++) {
                        JSONObject c = contacts.getJSONObject(i);
                        String description = c.getString("description");
                        String plc_id = c.getString("place_id");
                        String secondAddress = "";
                        if (c.has("structured_formatting")) {
                            secondAddress = c.getJSONObject("structured_formatting").optString("secondary_text");
                        }
                        names.add(description);
                        Place_id_type.add(plc_id);
                        secondryAddress.add(secondAddress);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.e("names", "onPostExecute addFlag = " + addFlag);

            if (addFlag == 1) {
                try {
                    ArrayAdapter<String> adp;
                    adp = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, names) {
                        @Override
                        public View getView(final int position, View convertView, ViewGroup parent) {
                            View view = super.getView(position, convertView, parent);
                            view.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                            final TextView text = (TextView) view.findViewById(android.R.id.text1);
                            text.setTextColor(getResources().getColor(R.color.colorBlack));
                            text.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    // TODO Auto-generated method stub
                                    if (Place_id_type.size() > 0) {
                                        place_id = Place_id_type.get(position);
                                    }
                                    if (secondryAddress.size() > 0) {
                                        pickUpSubArb = secondryAddress.get(position);
                                        Log.e("TAG", "pickUpSubArb = " + pickUpSubArb);
                                    }

                                    pickup_Touch = true;
                                    auto_Pickup.setText(text.getText().toString());
                                    getLocationFromAddress(text.getText().toString());
                                    auto_Pickup.dismissDropDown();
                                    //auto_Dropoff.setFocusableInTouchMode(true);
                                    //auto_Dropoff.requestFocus();
                                }
                            });
                            return view;
                        }
                    };
                    auto_Pickup.setAdapter(adp);
                    adp.notifyDataSetChanged();
                } catch (Exception e) {
                    // TODO: handle exception
                }
            } else if (addFlag == 2) {
                try {
                    ArrayAdapter<String> adp;
                    adp = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, names) {
                        @Override
                        public View getView(final int position, View convertView, ViewGroup parent) {
                            View view = super.getView(position, convertView, parent);
                            view.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                            final TextView text = (TextView) view.findViewById(android.R.id.text1);
                            text.setTextColor(getResources().getColor(R.color.colorBlack));
                            text.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    // TODO Auto-generated method stub
                                    if (Place_id_type.size() > 0) {
                                        place_id = Place_id_type.get(position);
                                        System.out.println("Place_id_type" + Place_id_type.get(position));
                                    }
                                    if (secondryAddress.size() > 0) {
                                        dropOffSubAArb = secondryAddress.get(position);
                                        Log.e("TAG", "dropOffSubAArb = " + dropOffSubAArb);
                                    }
                                    dropoff_Touch = true;

                                    auto_Dropoff.setText(text.getText().toString());
                                    getLocationFromAddress(text.getText().toString());
                                    auto_Dropoff.dismissDropDown();
                                    if (auto_Pickup.getText().toString().trim().length() <= 0) {
                                        //auto_Pickup.setFocusableInTouchMode(true);
                                        //auto_Pickup.requestFocus();
                                    }
                                }
                            });
                            return view;
                        }
                    };

                    auto_Dropoff.setAdapter(adp);
                    adp.notifyDataSetChanged();
                } catch (Exception e) {
                    // TODO: handle exception
                }
            }
        }
    }

    // Get the google API result and convert into JSON format.
    private JSONObject GetAddress(String Url) {
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpGet httppost = new HttpGet(Url);
            HttpResponse response = httpclient.execute(httppost);
            String jsonResult = Utility.inputStreamToString(response.getEntity().getContent()).toString();
            JSONObject json = new JSONObject(jsonResult);
            return json;
        } catch (Exception e) {
            // TODO: handle exception
        }
        return null;
    }

    public class GetGeoCoderAddress extends AsyncTask<String, Void, Void> {
        private final String Urlcoreconfig;
        String lat, lng;
        private String jsonResult;
        private String strAddress;

        public GetGeoCoderAddress(String url, String strAddress) {
            this.Urlcoreconfig = url;
            this.strAddress = strAddress;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... params) {
            try {
                HttpClient httpclient = new DefaultHttpClient();
                HttpGet httppost = new HttpGet(Urlcoreconfig);
                HttpResponse response = httpclient.execute(httppost);
                jsonResult = inputStreamToString(response.getEntity().getContent()).toString();
                JSONObject json = new JSONObject(jsonResult);
                json = json.getJSONObject("result");
                json = json.getJSONObject("geometry");
                json = json.getJSONObject("location");
                lat = json.getString("lat");
                lng = json.getString("lng");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            try {
                Geocoder coder = new Geocoder(activity);
                List<Address> address;
                Address location = null;

                if (lat != null && !lat.equalsIgnoreCase("") && lng != null && !lng.equalsIgnoreCase("")) {
                    if (addFlag == 1) {
                        TaxiUtil.picup_Lat = Double.valueOf(lat);
                        TaxiUtil.picup_Long = Double.valueOf(lng);
                        LatLng pickupLanlong = new LatLng(TaxiUtil.picup_Lat, TaxiUtil.picup_Long);
                        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder().target(pickupLanlong).zoom(17.5f).build()));
                    } else if (addFlag == 2) {
                        TaxiUtil.dropoff_Lat = Double.valueOf(lat);
                        TaxiUtil.dropoff_Long = Double.valueOf(lng);
                    }
                } else {
                    address = coder.getFromLocationName(strAddress, 5);
                    address = coder.getFromLocationName(strAddress, 5);
                    if (!address.isEmpty()) {
                        location = address.get(0);
                        location.getLatitude();
                        location.getLongitude();
                    }

                    if (!address.isEmpty()) {
                        if (addFlag == 1) {
                            TaxiUtil.picup_Lat = location.getLatitude();
                            TaxiUtil.picup_Long = location.getLongitude();
                            if (auto_Pickup.getText().toString().trim().length() > 0) {
                                TaxiUtil.picup_Address = auto_Pickup.getText().toString();
                            }
                        } else if (addFlag == 2) {
                            TaxiUtil.dropoff_Lat = location.getLatitude();
                            TaxiUtil.dropoff_Long = location.getLongitude();
                            if (auto_Dropoff.getText().toString().trim().length() > 0) {
                                TaxiUtil.dropoff_Address = auto_Dropoff.getText().toString();
                            }
                        }
                    }
                }
                favorite = 0;
                favorite_address = "";
                from = "";

                getEstimatedFareCheckValidation();
            } catch (Exception e) {

            }
        }
    }

    public StringBuilder inputStreamToString(InputStream is) {
        String rLine = "";
        StringBuilder answer = new StringBuilder();
        BufferedReader rd = new BufferedReader(new InputStreamReader(is));
        try {
            while ((rLine = rd.readLine()) != null) {
                answer.append(rLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return answer;
    }

    public void saveCardList() {
        try {
            String strCardList = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_CARD_LIST, activity);
            cardListBeens.clear();

            if (strCardList != null && !strCardList.equalsIgnoreCase("")) {
                JSONObject json = new JSONObject(strCardList);

                if (json != null) {
                    if (json.has("cards")) {
                        JSONArray cards = json.getJSONArray("cards");

                        if (cards != null && cards.length() > 0) {
                            for (int i = 0; i < cards.length(); i++) {
                                JSONObject cardsData = cards.getJSONObject(i);

                                if (cardsData != null) {
                                    String Id = "", CardNum = "", CardNum_ = "", Type = "", Alias = "";

                                    if (cardsData.has("Id")) {
                                        Id = cardsData.getString("Id");
                                    }

                                    if (cardsData.has("CardNum")) {
                                        CardNum = cardsData.getString("CardNum");
                                    }

                                    if (cardsData.has("CardNum2")) {
                                        CardNum_ = cardsData.getString("CardNum2");
                                    }

                                    if (cardsData.has("Type")) {
                                        Type = cardsData.getString("Type");
                                    }

                                    if (cardsData.has("Alias")) {
                                        Alias = cardsData.getString("Alias");
                                    }

                                    cardListBeens.add(new CreditCard_List_Been(Id, CardNum, CardNum_, Type, Alias));
                                }
                            }
                            //cardListBeens.add(new CreditCard_List_Been("","","Add a Card","",""));
//                            cardListBeens.add(new CreditCard_List_Been("","","wallet","",""));
                        } else {
                            //cardListBeens.add(new CreditCard_List_Been("","","Add a Card","",""));
//                            cardListBeens.add(new CreditCard_List_Been("","","wallet","",""));
                        }
                    } else {
                        //cardListBeens.add(new CreditCard_List_Been("","","Add a Card","",""));
//                        cardListBeens.add(new CreditCard_List_Been("","","wallet","",""));
                    }
                } else {
                    //cardListBeens.add(new CreditCard_List_Been("","","Add a Card","",""));
//                    cardListBeens.add(new CreditCard_List_Been("","","wallet","",""));
                }
            } else {
                //cardListBeens.add(new CreditCard_List_Been("","","Add a Card","",""));
//                cardListBeens.add(new CreditCard_List_Been("","","wallet","",""));
            }
        } catch (Exception e) {

        }
    }

    /**
     * Listener used to get updates from KalmanLocationManager (the good old Android LocationListener).
     */
    private LocationListener mLocationListener = new LocationListener() {

        @Override
        public void onLocationChanged(Location location) {

            if (location.getProvider().equals(LocationManager.GPS_PROVIDER)) {


            } else if (location.getProvider().equals(LocationManager.NETWORK_PROVIDER)) {


            } else {

                kalmanLat = location.getLatitude();
                kalmanLong = location.getLongitude();
                Log.e("call", "LocationManager.KALMAN_PROVIDER");
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

            String statusString = "Unknown";

            switch (status) {

                case LocationProvider.OUT_OF_SERVICE:

                    break;

                case LocationProvider.TEMPORARILY_UNAVAILABLE:

                    break;

                case LocationProvider.AVAILABLE:

                    break;
            }
        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };


    @Override
    protected void onResume() {
        super.onResume();

        if (mKalmanLocationManager != null) {
            mKalmanLocationManager.requestLocationUpdates(KalmanLocationManager.UseProvider.GPS_AND_NET, FILTER_TIME, GPS_TIME, NET_TIME, mLocationListener, true);
        }

        TicktocApplication.setCurrentActivity(activity);
        saveCardList();
        isBookLaterActivate = false;
        if (upcomming_pickup_long != 0 && upcomming_pickup_lat != 0 && upcomming_dropoff_long != 0 && upcomming_dropoff_lat != 0) {
            showUpCommingTripLocation();
        } else if (from != null && !from.equalsIgnoreCase("") && from.equalsIgnoreCase("TripCompleteActivity")) {
            startNew();
        } else if (addCardFlag == 1) {
            addCardFlag = 0;
            //openPromocodePopup();Saurav 12-02-2020
        }

        if (favorite != 0 && from != null && !from.equalsIgnoreCase("") && from.equalsIgnoreCase("FavoriteActivity")) {
            dropoff_Touch = true;
            if (tvDropoff != null) {

            } else {
                auto_Dropoff = findViewById(R.id.autoCompleteTextView_drop_off);
                tvDropoff = findViewById(R.id.tvDropoff);
            }
            tvDropoff.setText(favorite_address);
            //auto_Dropoff.dismissDropDown();
        }

        if (redirectBooking != null && !redirectBooking.equalsIgnoreCase("")) {
            if (redirectBooking.equalsIgnoreCase("booknow")) {
                if (selectedCar == -1) {
                    mySnackBar.showSnackBar(ll_RootLayour, "Please select car model", 0);
                }
                dropoff_Touch = true;
                if (redirectBookingAdrs != null && !redirectBookingAdrs.equalsIgnoreCase("")) {
                    tvDropoff.setText(redirectBookingAdrs);
                    //auto_Dropoff.dismissDropDown();
                }
                redirectBooking = "";
                redirectBookingAdrs = "";
            } else if (redirectBooking.equalsIgnoreCase("booklater")) {
                tvDropoff.setText("");
                mySnackBar.showSnackBar(ll_RootLayour, "Please select car model and click booklater", 0);
            }
        } else if (from != null && !from.equalsIgnoreCase("") && from.equalsIgnoreCase("BookLater")) {
            redirectBooking = "";
            redirectBookingAdrs = "";
            from = "";
            startNew();
        }

        if (isBookingDetailEdited)
        {
            isBookingDetailEdited=false;
            try
            {
                if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL,activity)!=null &&
                !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL,activity).equalsIgnoreCase(""))
                {
                    requestConfirmJsonObject = new JSONObject(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL,activity));
                    if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG,activity)!=null &&
                            SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG,activity).equalsIgnoreCase("1"))
                    {
                        parseAcceptRequestObject(requestConfirmJsonObject,0);
                    }
                    else
                    {
                        showPathFromPickupToDropoff(requestConfirmJsonObject);
                    }
                }
            }
            catch (Exception e)
            {
                Log.e("call","exception : "+e.getMessage());
            }
        }
    }

    public void startNew() {
        driverName = "";
        from = "";
        myLocation = true;
        disconnectSocket();
        favorite_address = "";
        totalAvailableCount = 0;
        gpsTracker = new GPSTracker(activity);
        timeFlag = 0;
        activity = MainActivity.this;
        handler = new Handler();
        handlerDriver = new Handler();
        passangerId = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID, activity);
        dialogClass = new DialogClass(activity, 0);
        requestPendingDialogClass = new RequestPendingDialogClass(activity, 0);
        aQuery = new AQuery(activity);
        ll_RootLayour = (LinearLayout) findViewById(R.id.main_content);
        mySnackBar = new MySnackBar(activity);

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_RATTING, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_RATTING, activity).equalsIgnoreCase("")) {
            ratting = Float.parseFloat(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_RATTING, activity));
        }

        getEstimateFare_beens.clear();
        getEstimateFare_beens.add(new GetEstimateFare_Been("", "", "", "", "", "", "", "0", "0", "1"));
        getEstimateFare_beens.add(new GetEstimateFare_Been("", "", "", "", "", "", "", "0", "0", "2"));
        getEstimateFare_beens.add(new GetEstimateFare_Been("", "", "", "", "", "", "", "0", "0", "3"));
        getEstimateFare_beens.add(new GetEstimateFare_Been("", "", "", "", "", "", "", "0", "0", "4"));
        getEstimateFare_beens.add(new GetEstimateFare_Been("", "", "", "", "", "", "", "0", "0", "5"));
        getEstimateFare_beens.add(new GetEstimateFare_Been("", "", "", "", "", "", "", "0", "0", "6"));
        pickup_Touch = false;
        dropoff_Touch = false;
        advanceBooking = 0;
        addFlag = 1;
        carClass = null;
        pickUp = null;
        driverLatLng = null;
        selectedCar = -1;
        zoomFlag = 0;
        BookingId = "";
        driverPhone = "";
        carType_beens.clear();
        tripFlag = 0;
        driverId = "";
        getEstimated_lat = 0;
        getEstimated_long = 0;
        getEstimated_Pickup = "";
        selectedCarPosition = -1;
        bookingRequest_beens.clear();
        bookingDriverLocation_beens.clear();
        requestConfirmJsonObject = null;

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_FAVORITE_TYPE, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_FAVORITE_TYPE, activity).equalsIgnoreCase("")) {
            favorite = Integer.parseInt(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_FAVORITE_TYPE, activity));

            if (favorite != 0) {
                iv_Favorite.setImageResource(R.drawable.favorite_selected);
            } else {
                iv_Favorite.setImageResource(R.drawable.favorite_unselected);
            }
        } else {
            iv_Favorite.setImageResource(R.drawable.favorite_unselected);
        }

        upcomming_pickup_lat = 0;
        upcomming_pickup_long = 0;
        upcomming_dropoff_lat = 0;
        upcomming_dropoff_long = 0;

        /*ll_CarBackgroundOne.setBackgroundResource(R.drawable.circle_white_border);
        ll_CarBackgroundTwo.setBackgroundResource(R.drawable.circle_white_border);
        ll_CarBackgroundThree.setBackgroundResource(R.drawable.circle_white_border);
        ll_CarBackgroundFour.setBackgroundResource(R.drawable.circle_white_border);
        ll_CarBackgroundFive.setBackgroundResource(R.drawable.circle_white_border);
        ll_CarBackgroundSix.setBackgroundResource(R.drawable.circle_white_border);*/
        carListAdapter.setPosition(selectedCar);

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity).equals("")) {
            tripFlag = Integer.parseInt(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity));
        } else {
            tripFlag = 0;
        }

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, activity).equals("")) {
            selectedCarPosition = Integer.parseInt(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, activity));
        }

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, activity).equals("")) {
            try {
                requestConfirmJsonObject = new JSONObject(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, activity));
            } catch (Exception e) {

            }
        }

        try
        {
            if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_CAR_CLASS, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_CAR_CLASS, activity).equals("")) {
                carClass = new JSONArray(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_CAR_CLASS, activity));
            } else {
                carClass = null;
            }

            if (carClass != null && carClass.length() > 0)
            {
                carType_beens.clear();

                for (int i = 0; i < carClass.length(); i++) {
                    JSONObject data = carClass.getJSONObject(i);

                    if (data != null) {
                        if (data.has("CategoryId")) {
                            String Id = "", CategoryId = "", Name = "", Sort = "", BaseFare = "", MinKm = "", PerKmCharge = "", CancellationFee = "", NightCharge = "", NightTimeFrom = "";
                            String NightTimeTo = "", SpecialEventSurcharge = "", SpecialEventTimeFrom = "", SpecialEventTimeTo = "", WaitingTimeCost = "", MinuteFare = "";
                            String BookingFee = "", Capacity = "", Image = "", Description = "", Status = "";

                            CategoryId = data.getString("CategoryId");

                            if (CategoryId != null && !CategoryId.equals("") && CategoryId.equals("1")) {
                                if (data.has("Id")) {
                                    Id = data.getString("Id");
                                }

                                if (data.has("Name")) {
                                    Name = data.getString("Name");
                                }

                                if (data.has("Sort")) {
                                    Sort = data.getString("Sort");
                                }

                                if (data.has("BaseFare")) {
                                    BaseFare = data.getString("BaseFare");
                                }

                                if (data.has("MinKm")) {
                                    MinKm = data.getString("MinKm");
                                }

                                if (data.has("PerKmCharge")) {
                                    PerKmCharge = data.getString("PerKmCharge");
                                }

                                if (data.has("CancellationFee")) {
                                    CancellationFee = data.getString("CancellationFee");
                                }

                                if (data.has("NightCharge")) {
                                    NightCharge = data.getString("NightCharge");
                                }

                                if (data.has("NightTimeFrom")) {
                                    NightTimeFrom = data.getString("NightTimeFrom");
                                }

                                if (data.has("NightTimeTo")) {
                                    NightTimeTo = data.getString("NightTimeTo");
                                }

                                if (data.has("SpecialEventSurcharge")) {
                                    SpecialEventSurcharge = data.getString("SpecialEventSurcharge");
                                }

                                if (data.has("SpecialEventTimeFrom")) {
                                    SpecialEventTimeFrom = data.getString("SpecialEventTimeFrom");
                                }

                                if (data.has("SpecialEventTimeTo")) {
                                    SpecialEventTimeTo = data.getString("SpecialEventTimeTo");
                                }

                                if (data.has("WaitingTimeCost")) {
                                    WaitingTimeCost = data.getString("WaitingTimeCost");
                                }

                                if (data.has("MinuteFare")) {
                                    MinuteFare = data.getString("MinuteFare");
                                }

                                if (data.has("BookingFee")) {
                                    BookingFee = data.getString("BookingFee");
                                }

                                if (data.has("Capacity")) {
                                    Capacity = data.getString("Capacity");
                                }

                                if (data.has("Image")) {
                                    Image = data.getString("Image");
                                }

                                if (data.has("Description")) {
                                    Description = data.getString("Description");
                                }

                                if (data.has("Status")) {
                                    Status = data.getString("Status");
                                }

                                carType_beens.add(new CarType_Been(
                                        Id,
                                        CategoryId,
                                        Name,
                                        Sort,
                                        BaseFare,
                                        MinKm,
                                        PerKmCharge,
                                        CancellationFee,
                                        NightCharge,
                                        NightTimeFrom,
                                        NightTimeTo,
                                        SpecialEventSurcharge,
                                        SpecialEventTimeFrom,
                                        SpecialEventTimeTo,
                                        WaitingTimeCost,
                                        MinuteFare,
                                        BookingFee,
                                        Capacity,
                                        Image,
                                        Description,
                                        Status,
                                        0
                                ));
                            }
                        }
                    }
                }

                carListAdapter.setCarList(carType_beens);
                /*Collections.sort(
                        carType_beens,
                        new Comparator<CarType_Been>()
                        {
                            public int compare(CarType_Been lhs, CarType_Been rhs)
                            {
                                return lhs.getSort().compareTo(rhs.getSort());
                            }
                        }
                );*/
            } else {
                carType_beens.clear();
            }
        } catch (Exception e) {

        }

        displayImage();
        connectSocket();
        if (tripFlag == 0) {
            ll_BookingLayout.setVisibility(View.VISIBLE);
            ll_AllCarLayout.setVisibility(View.VISIBLE);
            address_Layout.setVisibility(View.VISIBLE);
            ll_AfterRequestAccept.setVisibility(View.GONE);

            selectedCar = -1;
            myLocation = true;
            if (googleMap != null) {
                googleMap.clear();
            }
        } else {
            ll_BookingLayout.setVisibility(View.GONE);
            ll_AllCarLayout.setVisibility(View.GONE);
            address_Layout.setVisibility(View.GONE);
            ll_AfterRequestAccept.setVisibility(View.VISIBLE);
            if (tripFlag == 1) {
                myLocation = false;
                tv_CancelRequest.setVisibility(View.VISIBLE);
                parseAcceptRequestObject(requestConfirmJsonObject, 1);
            } else if (tripFlag == 2) {
                myLocation = false;
                tv_CancelRequest.setVisibility(View.GONE);
                showPathFromPickupToDropoff(requestConfirmJsonObject);
            }
        }

        checkGPS();
        tvPickup.setText("");
        tvDropoff.setText("");
        TaxiUtil.dropoff_Address = "";
        TaxiUtil.picup_Address = "";
        TaxiUtil.picup_Lat = 0;
        TaxiUtil.picup_Long = 0;
        TaxiUtil.dropoff_Lat = 0;
        TaxiUtil.dropoff_Long = 0;

        iv_MyLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                current_marker_layout.setVisibility(View.VISIBLE);
                myLocation = true;
                selectedCar = -1;
                /*ll_CarBackgroundOne.setBackgroundResource(R.drawable.circle_white_border);
                ll_CarBackgroundTwo.setBackgroundResource(R.drawable.circle_white_border);
                ll_CarBackgroundThree.setBackgroundResource(R.drawable.circle_white_border);
                ll_CarBackgroundFour.setBackgroundResource(R.drawable.circle_white_border);
                ll_CarBackgroundFive.setBackgroundResource(R.drawable.circle_white_border);
                ll_CarBackgroundSix.setBackgroundResource(R.drawable.circle_white_border);*/
                carListAdapter.setPosition(selectedCar);
                checkGPS();
            }
        });

        if (marker != null) {
            marker.remove();
        }

        getAddressFromLatLong();
        call_DriverInfo();
    }

    public void displayImage() {
        /*if (tripFlag==0)
        {
            if (carType_beens!=null && carType_beens.size()>0)
            {
                if (carType_beens.get(0).getImage()!=null && !carType_beens.get(0).getImage().equalsIgnoreCase(""))
                {
                    Picasso.with(activity).load(carType_beens.get(0).getImage()).into(iv_CarOne);
                }
                else
                {
                    iv_CarOne.setVisibility(View.INVISIBLE);
                }

                if (carType_beens.get(1).getImage()!=null && !carType_beens.get(1).getImage().equalsIgnoreCase(""))
                {
                    Picasso.with(activity).load(carType_beens.get(1).getImage()).into(iv_CarTwo);
                }
                else
                {
                    iv_CarTwo.setVisibility(View.INVISIBLE);
                }

                if (carType_beens.get(2).getImage()!=null && !carType_beens.get(2).getImage().equalsIgnoreCase(""))
                {
                    Picasso.with(activity).load(carType_beens.get(2).getImage()).into(iv_CarThree);
                }
                else
                {
                    iv_CarThree.setVisibility(View.INVISIBLE);
                }

                if (carType_beens.get(3).getImage()!=null && !carType_beens.get(3).getImage().equalsIgnoreCase(""))
                {
                    Picasso.with(activity).load(carType_beens.get(3).getImage()).into(iv_CarFour);
                }
                else
                {
                    iv_CarFour.setVisibility(View.INVISIBLE);
                }

                if (carType_beens.get(4).getImage()!=null && !carType_beens.get(4).getImage().equalsIgnoreCase(""))
                {
                    Picasso.with(activity).load(carType_beens.get(4).getImage()).into(iv_CarFive);
                }
                else
                {
                    iv_CarFive.setVisibility(View.INVISIBLE);
                }

                if (carType_beens.get(5).getImage()!=null && !carType_beens.get(5).getImage().equalsIgnoreCase(""))
                {
                    Picasso.with(activity).load(carType_beens.get(5).getImage()).into(iv_CarSix);
                }
                else
                {
                    iv_CarSix.setVisibility(View.INVISIBLE);
                }
            }
            Log.e("TAG","notifyDataSetChanged");
            carListAdapter.notifyDataSetChanged();
        }*/
    }

    private void showUpCommingTripLocation() {
        handler.removeCallbacks(runnable, null);
        handlerDriver.removeCallbacks(runnableDriver, null);

        MarkerOptions options = new MarkerOptions();
        options.position(new LatLng(upcomming_pickup_lat, upcomming_pickup_long));
        options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));

        MarkerOptions options1 = new MarkerOptions();
        options1.position(new LatLng(upcomming_dropoff_lat, upcomming_dropoff_long));

        if (selectedCar == 0) {
            options1.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car_one));
        } else if (selectedCar == 1) {
            options1.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car_two));
        } else if (selectedCar == 2) {
            options1.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car_three));
        } else if (selectedCar == 3) {
            options1.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car_four));
        } else if (selectedCar == 4) {
            options1.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car_five));
        } else if (selectedCar == 5) {
            options1.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car_six));
        }

        // Add new marker to the Google Map Android API V2

        if (marker != null) {
            marker.remove();
        }

        if (originMarker != null) {
            originMarker.remove();
        }


        if (destinationMarker != null) {
            destinationMarker.remove();
        }

        originMarker = googleMap.addMarker(options);
        destinationMarker = googleMap.addMarker(options1);
        String url = getDirectionsUrl(new LatLng(upcomming_pickup_lat, upcomming_pickup_long), new LatLng(upcomming_dropoff_lat, upcomming_dropoff_long));

        Log.e("url", "--------- " + url);
        DownloadTask downloadTask = new DownloadTask();

        // Start downloading json data from Google Directions API
        downloadTask.execute(url);
    }

    public void onBookLater() {
        if (commonDialog != null && commonDialog.isShowing()) {
            commonDialog.dismiss();
        }

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.book_later_cancle_dialog_layout);

        TextView tv_BookLater = (TextView) dialog.findViewById(R.id.tv_BookLater);
        TextView tv_Message = (TextView) dialog.findViewById(R.id.message);
        LinearLayout ll_BookLater = (LinearLayout) dialog.findViewById(R.id.ll_BookLater);
        TextView tv_Cancel = (TextView) dialog.findViewById(R.id.tv_Cancel);
        LinearLayout ll_Cancel = (LinearLayout) dialog.findViewById(R.id.ll_Cancel);

        tv_Message.setText(getResources().getString(R.string.book_later_message));

        ll_BookLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (tvPickup.getText().toString().equalsIgnoreCase("")) {
                    dialog.dismiss();
                    mySnackBar.showSnackBar(ll_RootLayour, "Please reselect pickup location", 0);
                } else if (selectedCar == -1) {
                    dialog.dismiss();
                    mySnackBar.showSnackBar(ll_RootLayour, "Please select car!", 0);
                } else if (tvDropoff.getText().toString().equalsIgnoreCase("")) {
                    dialog.dismiss();
                    mySnackBar.showSnackBar(ll_RootLayour, "Please reselect dropoff location!", 0);
                } else if (TaxiUtil.picup_Lat == 0) {
                    dialog.dismiss();
                    mySnackBar.showSnackBar(ll_RootLayour, "Please reselect pickup location", 0);
                } else if (TaxiUtil.picup_Long == 0) {
                    dialog.dismiss();
                    mySnackBar.showSnackBar(ll_RootLayour, "Please reselect pickup location", 0);
                } else if (TaxiUtil.dropoff_Lat == 0) {
                    dialog.dismiss();
                    mySnackBar.showSnackBar(ll_RootLayour, "Please reselect dropoff location!", 0);
                } else if (TaxiUtil.dropoff_Long == 0) {
                    dialog.dismiss();
                    mySnackBar.showSnackBar(ll_RootLayour, "Please reselect dropoff location!", 0);
                } else if (TaxiUtil.picup_Address == null || TaxiUtil.picup_Address.equalsIgnoreCase("")) {
                    dialog.dismiss();
                    mySnackBar.showSnackBar(ll_RootLayour, "Please reselect pickup location", 0);
                } else if (TaxiUtil.dropoff_Address == null || TaxiUtil.dropoff_Address.equalsIgnoreCase("")) {
                    dialog.dismiss();
                    mySnackBar.showSnackBar(ll_RootLayour, "Please reselect dropoff location!", 0);
                } else {
                    dialog.dismiss();
                    call_Booking(1);
                }
            }
        });

        tv_BookLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (tvPickup.getText().toString().equalsIgnoreCase("")) {
                    dialog.dismiss();
                    mySnackBar.showSnackBar(ll_RootLayour, "Please reselect pickup location", 0);
                } else if (selectedCar == -1) {
                    dialog.dismiss();
                    mySnackBar.showSnackBar(ll_RootLayour, "Please select car!", 0);
                } else if (tvDropoff.getText().toString().equalsIgnoreCase("")) {
                    dialog.dismiss();
                    mySnackBar.showSnackBar(ll_RootLayour, "Please reselect dropoff location!", 0);
                } else if (TaxiUtil.picup_Lat == 0) {
                    dialog.dismiss();
                    mySnackBar.showSnackBar(ll_RootLayour, "Please reselect pickup location", 0);
                } else if (TaxiUtil.picup_Long == 0) {
                    dialog.dismiss();
                    mySnackBar.showSnackBar(ll_RootLayour, "Please reselect pickup location", 0);
                } else if (TaxiUtil.dropoff_Lat == 0) {
                    dialog.dismiss();
                    mySnackBar.showSnackBar(ll_RootLayour, "Please reselect dropoff location!", 0);
                } else if (TaxiUtil.dropoff_Long == 0) {
                    dialog.dismiss();
                    mySnackBar.showSnackBar(ll_RootLayour, "Please reselect dropoff location!", 0);
                } else if (TaxiUtil.picup_Address == null || TaxiUtil.picup_Address.equalsIgnoreCase("")) {
                    dialog.dismiss();
                    mySnackBar.showSnackBar(ll_RootLayour, "Please reselect pickup location", 0);
                } else if (TaxiUtil.dropoff_Address == null || TaxiUtil.dropoff_Address.equalsIgnoreCase("")) {
                    dialog.dismiss();
                    mySnackBar.showSnackBar(ll_RootLayour, "Please reselect dropoff location!", 0);
                } else {
                    dialog.dismiss();
                    call_Booking(1);
                }
            }
        });

        tv_Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        ll_Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {

                if (MainActivity.ringTone != null) {
                    MainActivity.ringTone.stop();
                }
            }
        });
        commonDialog = dialog;
        dialog.show();
    }

    public void findPriceAndTime() {
        try {
            Log.e("call", "findPriceAndTime() 111");
            if (Common.socket != null && Common.socket.connected()) {
                passangerId = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID, activity);
                String pickup_Location = tvPickup.getText().toString().trim();
                String dropoff_Location = tvDropoff.getText().toString().trim();

                JSONObject jsonObject = new JSONObject();
                jsonObject.put(WebServiceAPI.PARAM_PASSENGER_ID, passangerId);
                Log.e("call", "findPriceAndTime() pickup_Touch = " + pickup_Touch);
                if (pickup_Touch == true) {
                    jsonObject.put(WebServiceAPI.PARAM_PICKUP_LOCATION, pickup_Location);
                    Log.e("call", "findPriceAndTime() dropoff_Touch = " + dropoff_Touch);
                    if (dropoff_Touch == true) {
                        jsonObject.put(WebServiceAPI.PARAM_DROPOFF_LOCATION, dropoff_Location);
                    } else {
                        jsonObject.put(WebServiceAPI.PARAM_DROPOFF_LOCATION, pickup_Location);
                    }
                    jsonObject.put(WebServiceAPI.PARAM_PICKUP_LAT, TaxiUtil.picup_Lat);
                    jsonObject.put(WebServiceAPI.PARAM_PICKUP_LONG_, TaxiUtil.picup_Long);
                } else {
                    jsonObject.put(WebServiceAPI.PARAM_PICKUP_LOCATION, getEstimated_Pickup);
                    Log.e("call", "findPriceAndTime() dropoff_Touch 111 = " + dropoff_Touch);
                    if (dropoff_Touch == true) {
                        jsonObject.put(WebServiceAPI.PARAM_DROPOFF_LOCATION, dropoff_Location);
                    } else {
                        jsonObject.put(WebServiceAPI.PARAM_DROPOFF_LOCATION, getEstimated_Pickup);
                    }
                    jsonObject.put(WebServiceAPI.PARAM_PICKUP_LAT, getEstimated_lat);
                    jsonObject.put(WebServiceAPI.PARAM_PICKUP_LONG_, getEstimated_long);
                }
                jsonObject.put(WebServiceAPI.PARAM_CITYID, cityId);
                jsonObject.put(WebServiceAPI.PARAM_IDS, SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_DRIVER_ID, activity));
                Log.e("call", "findPriceAndTime() EstimateFare jsonObject = " + jsonObject.toString());

                if (pickup_Location == null || dropoff_Location == null || pickup_Location.equalsIgnoreCase("") || dropoff_Location.equalsIgnoreCase("")) {
                    return;
                }

                Common.socket.emit("EstimateFare", jsonObject);
            }
        } catch (Exception e) {
            Log.e("call", "Exception = " + e.getMessage());
        }
    }

    private Emitter.Listener onGetEstimateFare = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    try {
                        Log.e("call", "onGetEstimateFare 111");
                        if (args != null && args.length > 0)
                        {
                            Log.e("call", "onGetEstimateFare response = " + args[0].toString());
                            JSONObject json = new JSONObject(args[0].toString());
                            getEstimateFare_beens.clear();
                            if (json != null)
                            {
                                if (json.has("status"))
                                {
                                    if (json.getBoolean("status"))
                                    {
                                        if (json.has("estimate_fare"))
                                        {
                                            JSONArray estimate_fare = json.getJSONArray("estimate_fare");

                                            if (estimate_fare != null && estimate_fare.length() > 0)
                                            {
                                                for (int i = 0; i < estimate_fare.length(); i++)
                                                {
                                                    JSONObject jsonObject = estimate_fare.getJSONObject(i);

                                                    if (jsonObject != null)
                                                    {
                                                        String per_km_charge = "", id = "", name = "", base_fare = "", km = "";
                                                        String trip_fare = "", booking_fee = "", total = "0", duration = "0", sortFlag = "";

                                                        if (jsonObject.has("per_km_charge"))
                                                        {
                                                            per_km_charge = jsonObject.getString("per_km_charge");
                                                        }

                                                        if (jsonObject.has("id")) {
                                                            id = jsonObject.getString("id");
                                                        }

                                                        if (jsonObject.has("name")) {
                                                            name = jsonObject.getString("name");
                                                        }

                                                        if (jsonObject.has("base_fare")) {
                                                            base_fare = jsonObject.getString("base_fare");
                                                        }

                                                        if (jsonObject.has("km")) {
                                                            km = jsonObject.getString("km");
                                                        }

                                                        if (jsonObject.has("trip_fare")) {
                                                            trip_fare = jsonObject.getString("trip_fare");
                                                        }

                                                        if (jsonObject.has("booking_fee")) {
                                                            booking_fee = jsonObject.getString("booking_fee");
                                                        }

                                                        if (jsonObject.has("total")) {
                                                            double totalDouble = Double.parseDouble(jsonObject.getString("total"));
                                                            total = String.format("%.2f", totalDouble);
                                                            Log.e("TAG1", "total = " + total);
                                                        }

                                                        if (jsonObject.has("sort")) {
                                                            sortFlag = jsonObject.getString("sort");
                                                        }

                                                        if (jsonObject.has("duration")) {
                                                            duration = jsonObject.getString("duration");
                                                        }

//                                                        if (id!=null && !id.trim().equalsIgnoreCase("") && id.trim().equalsIgnoreCase("1"))
//                                                        {
//                                                            sortFlag = "2";
//                                                        }
//                                                        else if (id!=null && !id.trim().equalsIgnoreCase("") && id.trim().equalsIgnoreCase("2"))
//                                                        {
//                                                            sortFlag = "6";
//                                                        }
//                                                        else if (id!=null && !id.trim().equalsIgnoreCase("") && id.trim().equalsIgnoreCase("3"))
//                                                        {
//                                                            sortFlag = "4";
//                                                        }
//                                                        else if (id!=null && !id.trim().equalsIgnoreCase("") && id.trim().equalsIgnoreCase("4"))
//                                                        {
//                                                            sortFlag = "1";
//                                                        }
//                                                        else if (id!=null && !id.trim().equalsIgnoreCase("") && id.trim().equalsIgnoreCase("5"))
//                                                        {
//                                                            sortFlag = "5";
//                                                        }
//                                                        else if (id!=null && !id.trim().equalsIgnoreCase("") && id.trim().equalsIgnoreCase("6"))
//                                                        {
//                                                            sortFlag = "3";
//                                                        }

                                                        getEstimateFare_beens.add(new GetEstimateFare_Been(
                                                                per_km_charge,
                                                                id,
                                                                name,
                                                                base_fare,
                                                                km,
                                                                trip_fare,
                                                                booking_fee,
                                                                total,
                                                                duration,
                                                                sortFlag
                                                        ));
                                                    }
                                                }

                                                carListAdapter.notifyDataSetChanged();

                                                tv_TimeOne.setText(getEstimateFare_beens.get(0).getDuration() + " min");
                                                tv_PriceOne.setText("$" + getEstimateFare_beens.get(0).getTotal());

                                                tv_TimeTwo.setText(getEstimateFare_beens.get(1).getDuration() + " min");
                                                tv_PriceTwo.setText("$" + getEstimateFare_beens.get(1).getTotal());

                                                tv_TimeThree.setText(getEstimateFare_beens.get(2).getDuration() + " min");
                                                tv_PriceThree.setText("$" + getEstimateFare_beens.get(2).getTotal());

                                                tv_TimeFour.setText(getEstimateFare_beens.get(3).getDuration() + " min");
                                                tv_PriceFour.setText("$" + getEstimateFare_beens.get(3).getTotal());

                                                tv_TimeFive.setText(getEstimateFare_beens.get(4).getDuration() + " min");
                                                tv_PriceFive.setText("$" + getEstimateFare_beens.get(4).getTotal());

                                                tv_TimeSix.setText(getEstimateFare_beens.get(5).getDuration() + " min");
                                                tv_PriceSix.setText("$" + getEstimateFare_beens.get(5).getTotal());

                                                String driverId = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_DRIVER_ID, activity);

                                                Log.e("call", "driverId = " + driverId);

                                                if (tvPickup.getText() != null && !tvPickup.getText().toString().equalsIgnoreCase("") && tvDropoff.getText() != null && !tvDropoff.getText().toString().trim().equalsIgnoreCase("")) {
                                                    Log.e("call", "auto_Pickup.getText()!=null");

                                                    if (driverId == null) {
                                                        Log.e("call", "driverId==null");
                                                        //onBookLater();saurav(24-2-2020)
                                                    } else if (driverId.equalsIgnoreCase("")) {
                                                        Log.e("call", "driverId.equalsIgnoreCase(\"\")");
                                                        //onBookLater();saurav(24-2-2020)
                                                    } else {
                                                        Log.e("call", "(((((((((((((((((((((((((((((( driver id ) ==== " + driverId);
                                                    }
                                                }

                                                timeFlag = 0;
                                            } else {
                                                timeFlag = 0;
                                                setTimeAndPriceToZero();
                                            }
                                        } else {
                                            timeFlag = 0;
                                            setTimeAndPriceToZero();
                                        }
                                    } else {
                                        timeFlag = 0;
                                        setTimeAndPriceToZero();
                                    }
                                } else {
                                    timeFlag = 0;
                                    setTimeAndPriceToZero();
                                }
                            } else {
                                timeFlag = 0;
                                setTimeAndPriceToZero();
                            }
                        }
                    } catch (Exception e) {
                        setTimeAndPriceToZero();
                    }
                }
            });
        }
    };

    public void setTimeAndPriceToZero() {
        if (tvDropoff.getText().toString().equalsIgnoreCase("") || tvPickup.getText().toString().equalsIgnoreCase("")) {
            for (int x = 0; x < getEstimateFare_beens.size(); x++) {
                getEstimateFare_beens.get(x).setDuration("0");
                getEstimateFare_beens.get(x).setTotal("0");
            }
            carListAdapter.notifyDataSetChanged();

            if (tv_TimeOne != null && tv_PriceOne != null) {
                tv_TimeOne.setText("0 min");
                tv_PriceOne.setText("$0");
            }

            if (tv_TimeTwo != null && tv_PriceTwo != null) {
                tv_TimeTwo.setText("0 min");
                tv_PriceTwo.setText("$0");
            }

            if (tv_TimeThree != null && tv_PriceThree != null) {
                tv_TimeThree.setText("0 min");
                tv_PriceThree.setText("$0");
            }

            if (tv_TimeFour != null && tv_PriceFour != null) {
                tv_TimeFour.setText("0 min");
                tv_PriceFour.setText("$0");
            }

            if (tv_TimeFive != null && tv_PriceFive != null) {
                tv_TimeFive.setText("0 min");
                tv_PriceFive.setText("$0");
            }

            if (tv_TimeSix != null && tv_PriceSix != null) {
                tv_TimeSix.setText("0 min");
                tv_PriceSix.setText("$0");
            }
        }
    }

    // FOR ADVANCE BOOKING SOCKET CALL..............................................................................

    private Emitter.Listener onAcceptAdvancedBookingRequestNotification = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    try {
                        int tripFlag = 0;

                        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity).equalsIgnoreCase("")) {
                            tripFlag = Integer.parseInt(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity));
                        }

                        if (tripFlag == 0) {
                            advanceBooking = 1;

                            if (args != null && args.length > 0) {
                                Log.e("call", "onAcceptAdvancedBookingRequestNotification = " + args[0].toString());
                                if (commonInternetDialog != null && commonInternetDialog.isShowing()) {
                                    commonInternetDialog.hideDialog();
                                }

                                if (commonDialog != null && commonDialog.isShowing()) {
                                    commonDialog.dismiss();
                                }

                                zoomFlag = 0;

                                JSONObject jsonObject = new JSONObject(args[0].toString());
                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, args[0].toString(), activity);

                                selectedCar = 0;

                                if (jsonObject != null) {
                                    if (jsonObject.has("BookingInfo")) {
                                        JSONArray BookingInfo = jsonObject.getJSONArray("BookingInfo");

                                        if (BookingInfo != null && BookingInfo.length() > 0) {
                                            JSONObject BookingInfoObject = BookingInfo.getJSONObject(0);

                                            if (BookingInfoObject != null) {
                                                double lat = 0, lng = 0;

                                                if (BookingInfoObject.has("ModelId")) {
                                                    if (carType_beens != null && carType_beens.size() > 0) {
                                                        for (int i = 0; i < carType_beens.size(); i++) {
                                                            if (BookingInfoObject.getString("ModelId").equalsIgnoreCase(carType_beens.get(i).getId())) {
//                                                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR,carType_beens.get(i).getSort(),activity);
                                                                selectedCar = Integer.parseInt(carType_beens.get(i).getSort());
                                                            }
                                                        }
                                                    }
                                                }

                                                if (BookingInfoObject.has("PickupLat")) {
                                                    lat = BookingInfoObject.getDouble("PickupLat");
                                                }

                                                if (BookingInfoObject.has("PickupLng")) {
                                                    lng = BookingInfoObject.getDouble("PickupLng");
                                                }

                                                if (BookingInfoObject.has("Id")) {
                                                    BookingId = BookingInfoObject.getString("Id");
                                                }

                                                pickUp = new LatLng(lat, lng);
                                            }
                                        }
                                    }

                                    if (jsonObject.has("DriverInfo")) {
                                        JSONArray DriverInfo = jsonObject.getJSONArray("DriverInfo");

                                        if (DriverInfo != null && DriverInfo.length() > 0) {
                                            JSONObject dropObject = DriverInfo.getJSONObject(0);

                                            if (dropObject != null) {
                                                double lat = 0, lng = 0;

                                                if (dropObject.has("Lat")) {
                                                    lat = Double.parseDouble(dropObject.getString("Lat"));
                                                }

                                                if (dropObject.has("Lng")) {
                                                    lng = Double.parseDouble(dropObject.getString("Lng"));
                                                }

                                                if (lat != 0.0 && lng != 0.0) {
                                                    driverLatLng = new LatLng(lat, lng);//23.0158596,
                                                } else {
                                                    driverLatLng = null;
                                                }
                                            }
                                        }
                                    }
                                }

                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "1", activity);
                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, selectedCar + "", activity);

                                requestConfirmJsonObject = new JSONObject(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, activity));
                                tripFlag = 1;

                                requestPendingDialogClass.hideDialog();

                                if (requestConfirmJsonObject != null) {
                                    ll_BookingLayout.setVisibility(View.GONE);
                                    ll_AllCarLayout.setVisibility(View.GONE);
                                    ll_AfterRequestAccept.setVisibility(View.VISIBLE);
                                    tv_CancelRequest.setVisibility(View.VISIBLE);
                                    address_Layout.setVisibility(View.GONE);

                                    if (pickUp != null && driverLatLng != null) {
                                        showRoute();
                                    }

                                    String message = "Your booking request has been confirmed";
                                    if (requestConfirmJsonObject.has("message")) {
                                        message = requestConfirmJsonObject.getString("message");
                                    }

                                    if (TicktocApplication.getCurrentActivity() != null) {
                                        displayAcceptRequestPopup(message, TicktocApplication.getCurrentActivity());
                                    } else {
                                        displayAcceptRequestPopup(message, activity);
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {

                    }
                }
            });
        }
    };

    private Emitter.Listener onRejectAdvancedBookingRequestNotification = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    try {
                        Log.e("call", "onRejectAdvancedBookingRequestNotification ");
                        int tripFlag = 0;

                        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity).equalsIgnoreCase("")) {
                            tripFlag = Integer.parseInt(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity));
                        }

                        Log.e("call", "onRejectAdvancedBookingRequestNotification tripFlag = " + tripFlag);

                        if (tripFlag == 0) {
                            if (requestPendingDialogClass != null) {
                                requestPendingDialogClass.hideDialog();
                            }

                            if (commonInternetDialog != null && commonInternetDialog.isShowing()) {
                                commonInternetDialog.hideDialog();
                            }

                            if (commonDialog != null && commonDialog.isShowing()) {
                                commonDialog.dismiss();
                            }

                            advanceBooking = 0;
                            tripFlag = 0;
                            selectedCarPosition = -1;
                            requestConfirmJsonObject = null;
                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "0", activity);
                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, "", activity);
                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, "-1", activity);

                            if (args != null && args.length > 0) {
                                Log.e("call", "onRejectAdvancedBookingRequestNotification args[0] = " + args[0]);
                                JSONObject jsonObject = new JSONObject(args[0].toString());

                                if (jsonObject != null) {
                                    if (jsonObject.has("message")) {
                                        String message = jsonObject.getString("message");
                                        if (TicktocApplication.getCurrentActivity() != null) {
                                            InternetDialog internetDialog = new InternetDialog(TicktocApplication.getCurrentActivity());
                                            internetDialog.showDialog(message);
                                            commonInternetDialog = internetDialog;
                                        } else {
                                            InternetDialog internetDialog = new InternetDialog(activity);
                                            internetDialog.showDialog(message);
                                            commonInternetDialog = internetDialog;
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        Log.e("call", "onRejectAdvancedBookingRequestNotification Exception = " + e.getMessage());
                    }
                }
            });
        }
    };

//    private Emitter.Listener InformPassengerForAdvancedTrip = new Emitter.Listener() {
//        @Override
//        public void call(final Object... args) {
//
//            runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//
//                    try
//                    {
//                        if (args!=null && args.length>0)
//                        {
//                            if (commonInternetDialog!=null && commonInternetDialog.isShowing())
//                            {
//                                commonInternetDialog.hideDialog();
//                            }
//
//                            if (commonDialog!=null && commonDialog.isShowing())
//                            {
//                                commonDialog.dismiss();
//                            }
//
//                            JSONObject jsonObject = new JSONObject(args[0].toString());
//
//                            if (jsonObject!=null)
//                            {
//                                if (jsonObject.has("message"))
//                                {
//                                    String message = jsonObject.getString("message");
//                                    if (TicktocApplication.getCurrentActivity()!=null)
//                                    {
//                                        InternetDialog internetDialog = new InternetDialog(TicktocApplication.getCurrentActivity());
//                                        internetDialog.showDialog(message);
//                                        commonInternetDialog = internetDialog;
//                                    }
//                                    else
//                                    {
//                                        InternetDialog internetDialog = new InternetDialog(activity);
//                                        internetDialog.showDialog(message);
//                                        commonInternetDialog = internetDialog;
//                                    }
//                                }
//                            }
//                        }
//                    }
//                    catch (Exception e)
//                    {
//                        Log.e("call","Exception = "+e.getMessage());
//                    }
//                }
//            });
//        }
//    };


    private Emitter.Listener AcceptAdvancedBookingRequestNotify = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    try {
                        if (args != null && args.length > 0) {
                            Log.e("call", "AcceptAdvancedBookingRequestNotify = " + args[0].toString());
                            JSONObject jsonObject = new JSONObject(args[0].toString());

                            try {
                                ringTone = MediaPlayer.create(activity, R.raw.tone);
                                ringTone.start();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            if (commonInternetDialog != null && commonInternetDialog.isShowing()) {
                                commonInternetDialog.hideDialog();
                            }

                            if (commonDialog != null && commonDialog.isShowing()) {
                                commonDialog.dismiss();
                            }

                            if (jsonObject != null) {
                                if (jsonObject.has("message")) {
                                    String message = jsonObject.getString("message");
                                    if (TicktocApplication.getCurrentActivity() != null) {
                                        InternetDialog internetDialog = new InternetDialog(TicktocApplication.getCurrentActivity());
                                        internetDialog.showDialog(message);
                                        commonInternetDialog = internetDialog;
                                    } else {
                                        InternetDialog internetDialog = new InternetDialog(activity);
                                        internetDialog.showDialog(message);
                                        commonInternetDialog = internetDialog;
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {

                    }
                }
            });
        }
    };

    private Emitter.Listener onAdvancedBookingPickupPassengerNotification = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    try {
                        Log.e("call", "onAdvancedBookingPickupPassengerNotification " + args[0]);
                        int tripFlag = 0;

                        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity).equalsIgnoreCase("")) {
                            tripFlag = Integer.parseInt(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity));
                        }

                        if (tripFlag != 0) {
                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "2", activity);

                            if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity).equalsIgnoreCase("")) {
                                tripFlag = Integer.parseInt(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity));
                            } else {
                                tripFlag = 0;
                            }

                            if (originMarker != null) {
                                originMarker.remove();
                            }

                            if (destinationMarker != null) {
                                destinationMarker.remove();
                            }

                            if (googleMap != null) {
                                googleMap.clear();
                            }

                            if (args != null && args.length > 0) {

                                tv_CancelRequest.setVisibility(View.GONE);

                                if (commonInternetDialog != null && commonInternetDialog.isShowing()) {
                                    commonInternetDialog.hideDialog();
                                }

                                if (commonDialog != null && commonDialog.isShowing()) {
                                    commonDialog.dismiss();
                                }

                                JSONObject jsonObject = new JSONObject(args[0].toString());

                                if (requestConfirmJsonObject != null) {
                                    showPathFromPickupToDropoff(requestConfirmJsonObject);
                                    String message = "Your trip is start now";
                                    if (jsonObject.has("message")) {
                                        message = jsonObject.getString("message");
                                    }

                                    if (TicktocApplication.getCurrentActivity() != null) {
                                        InternetDialog internetDialog = new InternetDialog(TicktocApplication.getCurrentActivity());
                                        internetDialog.showDialog(message);
                                        commonInternetDialog = internetDialog;
                                    } else {
                                        InternetDialog internetDialog = new InternetDialog(activity);
                                        internetDialog.showDialog(message);
                                        commonInternetDialog = internetDialog;
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        Log.e("call", "Exception onGetDriverLocation = " + e.getMessage());
                    }
                }
            });
        }
    };

    private Emitter.Listener onAdvancedBookingTripHoldNotification = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    try {
                        int tripFlag = 0;

                        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity).equalsIgnoreCase("")) {
                            tripFlag = Integer.parseInt(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity));
                        }

                        if (tripFlag != 0) {

                            if (args != null && args.length > 0) {
                                if (commonInternetDialog != null && commonInternetDialog.isShowing()) {
                                    commonInternetDialog.hideDialog();
                                }

                                if (commonDialog != null && commonDialog.isShowing()) {
                                    commonDialog.dismiss();
                                }

                                JSONObject jsonObject = new JSONObject(args[0].toString());

                                if (jsonObject != null) {
                                    if (jsonObject.has("message")) {
                                        String message = jsonObject.getString("message");
                                        if (TicktocApplication.getCurrentActivity() != null) {
                                            InternetDialog internetDialog = new InternetDialog(TicktocApplication.getCurrentActivity());
                                            internetDialog.showDialog(message);
                                            commonInternetDialog = internetDialog;
                                        } else {
                                            InternetDialog internetDialog = new InternetDialog(activity);
                                            internetDialog.showDialog(message);
                                            commonInternetDialog = internetDialog;
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        Log.e("call", "Exception onAdvancedBookingTripHoldNotification = " + e.getMessage());
                    }
                }
            });
        }
    };

    private Emitter.Listener onCancelAdvanceBookingManually = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.w("onCancelBookingCalled","123..."+args[0]);
                    try {
                        JSONObject jObj = new JSONObject(args[0].toString());
                        InternetDialog internetDialog = new InternetDialog(activity);
                        internetDialog.showDialog(jObj.getString("message"));
                    }catch (JSONException e){
                        e.printStackTrace();
                    }
                }
            });
        }
    };

                    private Emitter.Listener onAdvancedBookingDetails = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    try {
                        int tripFlag = 0;

                        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity).equalsIgnoreCase("")) {
                            tripFlag = Integer.parseInt(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity));
                        }

                        if (tripFlag != 0) {
                            if (args != null && args.length > 0) {
                                Log.e("call", "onAdvancedBookingDetails = " + args[0].toString());
                                if (commonInternetDialog != null && commonInternetDialog.isShowing()) {
                                    commonInternetDialog.hideDialog();
                                }

                                if (commonDialog != null && commonDialog.isShowing()) {
                                    commonDialog.dismiss();
                                }

                                JSONObject tripCompleteObject = new JSONObject(args[0].toString());
                                if (tripCompleteObject != null) {
                                    String walletBalance = "0";

                                    String passengerType = "";

                                    if (tripCompleteObject.has("Info")) {
                                        JSONArray InfoArray = tripCompleteObject.getJSONArray("Info");

                                        if (InfoArray != null && InfoArray.length() > 0) {
                                            JSONObject InfoData = InfoArray.getJSONObject(0);

                                            if (InfoData != null) {
                                                if (InfoData.has("PassengerType")) {
                                                    passengerType = InfoData.getString("PassengerType");
                                                } else {
                                                    passengerType = "";
                                                }
                                            } else {
                                                passengerType = "";
                                            }
                                        } else {
                                            passengerType = "";
                                        }
                                    } else {
                                        passengerType = "";
                                    }

                                    if (tripCompleteObject.has("UpdatedBal")) {
                                        walletBalance = tripCompleteObject.getString("UpdatedBal");
                                    }

                                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_WALLET_BALLENCE, walletBalance, activity);

                                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "0", activity);
                                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, "-1", activity);
                                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, "", activity);

                                    ll_BookingLayout.setVisibility(View.VISIBLE);
                                    ll_AllCarLayout.setVisibility(View.VISIBLE);
                                    ll_AfterRequestAccept.setVisibility(View.GONE);
                                    tv_CancelRequest.setVisibility(View.GONE);
                                    address_Layout.setVisibility(View.VISIBLE);

                                    if (marker != null) {
                                        marker.remove();
                                    }

                                    if (originMarker != null) {
                                        originMarker.remove();
                                    }

                                    if (destinationMarker != null) {
                                        destinationMarker.remove();
                                    }

                                    if (googleMap != null) {
                                        googleMap.clear();
                                    }

                                    handlerDriver.removeCallbacks(runnableDriver, null);

                                    String message = "Your trip has been completed successfully!";
                                    if (tripCompleteObject.has("message")) {
                                        message = tripCompleteObject.getString("message");
                                    }
                                    SessionSave.saveUserSession("TripCompleted", tripCompleteObject.toString(), activity);

                                    if (passengerType != null && (passengerType.equalsIgnoreCase("other") || passengerType.equalsIgnoreCase("others"))) {
                                        if (TicktocApplication.getCurrentActivity() != null) {
                                            showTripCompletePopupOk(TicktocApplication.getCurrentActivity(), message);
                                        } else {
                                            showTripCompletePopupOk(activity, message);
                                        }
                                    } else {
                                        if (TicktocApplication.getCurrentActivity() != null) {
                                            showTripCompletePopup(message, TicktocApplication.getCurrentActivity(), BookingId, WebServiceAPI.BOOK_LATER);
                                        } else {
                                            showTripCompletePopup(message, activity, BookingId, WebServiceAPI.BOOK_LATER);
                                        }
                                    }

                                }
                            }
                        }
                    } catch (Exception e) {
                        Log.e("call", "Exception onAdvancedBookingDetails = " + e.getMessage());
                    }
                }
            });
        }
    };

    public void showTripCompletePopupOk(Context context, String message) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.my_dialog_class);

        TextView tv_Ok = (TextView) dialog.findViewById(R.id.Ok);
        TextView tv_Message = (TextView) dialog.findViewById(R.id.message);
        LinearLayout ll_Ok = (LinearLayout) dialog.findViewById(R.id.ok_layout);

        tv_Message.setText(message);

        ll_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                openTripCompleteScreen();
            }
        });

        tv_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                openTripCompleteScreen();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {

                if (MainActivity.ringTone != null) {
                    MainActivity.ringTone.stop();
                }
            }
        });

        dialog.show();
    }

    @Override
    protected void onPause() {
        mKalmanLocationManager.removeUpdates(mLocationListener);
        if (Spinner_paymentMethod != null) {
            Spinner_paymentMethod.onDetachedFromWindow();
        }
        super.onPause();
    }

    public void openPromocodePopup() {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.promocode_booknow_layout);

        TextView tv_RequestNow = (TextView) dialog.findViewById(R.id.request_now_textview);

        final EditText et_Promocode = (EditText) dialog.findViewById(R.id.promocode_edittext);
        final TextInputLayout til_Promocode = (TextInputLayout) dialog.findViewById(R.id.promocode_input_layout);
        final EditText et_Notes = (EditText) dialog.findViewById(R.id.input_note);
        final CheckBox checkBox = (CheckBox) dialog.findViewById(R.id.checkBox);
        final CheckBox checkBoxBabySeat = (CheckBox) dialog.findViewById(R.id.checkBoxBabySeat);
        cardId = "";
        cardNumber = "";
        cardType = "";
        isCardSelected = false;
        selectedPaymentType = "";
        spinnerSelectedPosition = 0;
        isBabySeatInclude = false;

        checkBoxBabySeat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isBabySeatInclude = isChecked;
            }
        });

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked == true) {
                    til_Promocode.setVisibility(View.VISIBLE);
                } else {
                    til_Promocode.setVisibility(View.GONE);
                }
            }
        });

        Spinner_paymentMethod = (CustomSpinner) dialog.findViewById(R.id.Spinner_paymentMethod);
        customerAdapter = new CustomerAdapter(activity, cardListBeens);
        Spinner_paymentMethod.setAdapter(customerAdapter);

        Spinner_paymentMethod.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                cardId = cardListBeens.get(position).getId();
                cardNumber = cardListBeens.get(position).getCardNum_();

//                if (position == (cardListBeens.size() -1))
//                {
//                    selectedPaymentType = "wallet";//PaymentType : cash,wallet,card
//
//                }
                if (position == (cardListBeens.size() - 1)) {
                    selectedPaymentType = "cash";//PaymentType : cash,wallet,card
                } else {
                    selectedPaymentType = "card";//PaymentType : cash,wallet,card
                }
                selectedPaymentType = "card";
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });

        promocodeStr = "";
        notes = "";
        et_Promocode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (charSequence.toString().length() > 0) {
                    til_Promocode.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        tv_RequestNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                promocodeStr = et_Promocode.getText().toString();
                notes = et_Notes.getText().toString().trim();
                dialog.dismiss();
                if (Global.isNetworkconn(activity)) {
                    if (TaxiUtil.picup_Lat == 0 || TaxiUtil.picup_Long == 0) {
                        InternetDialog internetDialog = new InternetDialog(activity);
                        internetDialog.showDialog("Please enter your pickup location again.");
                    } else {
                        if (TaxiUtil.dropoff_Lat == 0 || TaxiUtil.dropoff_Long == 0) {
                            InternetDialog internetDialog = new InternetDialog(activity);
                            internetDialog.showDialog("Please enter your destination again.");
                        } else {
                            call_BookNow();
                        }
                    }
                } else {
                    InternetDialog internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog("Please check your internet connection!");
                }
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        dialog.show();
    }

    public class CustomerAdapter extends ArrayAdapter<CreditCard_List_Been> {
        ArrayList<CreditCard_List_Been> customers, tempCustomer, suggestions;

        public CustomerAdapter(Context context, ArrayList<CreditCard_List_Been> objects) {
            super(context, R.layout.row_spinner_item, R.id.spinner_cardnumber, objects);
            this.customers = objects;
            this.tempCustomer = new ArrayList<CreditCard_List_Been>(objects);
            this.suggestions = new ArrayList<CreditCard_List_Been>(objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return initView(position, convertView, parent);
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return initView(position, convertView, null);
        }

        private View initView(int position, View convertView, ViewGroup parent) {
            CreditCard_List_Been customer = getItem(position);
            if (convertView == null) {
                if (parent == null)
                    convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_spinner_item, null);
                else
                    convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_spinner_item, parent, false);
            }
            TextView txtCustomer = (TextView) convertView.findViewById(R.id.spinner_cardnumber);
            LinearLayout ll_Image = (LinearLayout) convertView.findViewById(R.id.spinner_image_layout);
            ImageView iv_Image = (ImageView) convertView.findViewById(R.id.image);
            LinearLayout ll_AddPaymentMethod = (LinearLayout) convertView.findViewById(R.id.ll_add_payment_method);
            if (txtCustomer != null)
                txtCustomer.setText(customer.getCardNum_());

            if (customer.getCardType() != null && customer.getCardType().equalsIgnoreCase("visa")) {
                iv_Image.setImageResource(R.drawable.ic_card_icon_visa);
            } else if (customer.getCardType() != null && customer.getCardType().equalsIgnoreCase("mastercard")) {
                iv_Image.setImageResource(R.drawable.ic_card_icon_master);
            } else if (customer.getCardType() != null && customer.getCardType().equalsIgnoreCase("amex")) {
                iv_Image.setImageResource(R.drawable.ic_card_icon_american);
            } else if (customer.getCardType() != null && customer.getCardType().equalsIgnoreCase("diners")) {
                iv_Image.setImageResource(R.drawable.ic_card_icon_dinner);
            } else if (customer.getCardType() != null && customer.getCardType().equalsIgnoreCase("discover")) {
                iv_Image.setImageResource(R.drawable.ic_card_icon_discover);
            } else if (customer.getCardType() != null && customer.getCardType().equalsIgnoreCase("jcb")) {
                iv_Image.setImageResource(R.drawable.ic_card_icon_jcb);
            } else if (customer.getCardType() != null && customer.getCardType().equalsIgnoreCase("other")) {
                iv_Image.setImageResource(R.drawable.ic_card_icon_visa);
            } else if (customer.getCardNum_() != null && customer.getCardNum_().equalsIgnoreCase("cash")) {
                iv_Image.setImageResource(R.drawable.ic_card_icon_cash);
            } else if (customer.getCardNum_() != null && customer.getCardNum_().equalsIgnoreCase("wallet")) {
                iv_Image.setImageResource(R.drawable.ic_card_icon_wallet);
            } else if (customer.getCardNum_() != null && customer.getCardNum_().equalsIgnoreCase("Add a Card")) {
                iv_Image.setImageResource(R.drawable.icon_plus_black);
            }

            ll_AddPaymentMethod.setVisibility(View.GONE);

            ll_AddPaymentMethod.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });

            return convertView;
        }
    }

//    @Override
//    public void onMarkerDrag(Marker marker)
//    {
//
//    }
//
//    @Override
//    public void onMarkerDragEnd(Marker marker) {
//
//
//        TaxiUtil.picup_Lat = marker.getPosition().latitude;
//        TaxiUtil.picup_Long = marker.getPosition().longitude;
//
//        Log.e("call","Marker drag end lat " + TaxiUtil.picup_Lat);
//        Log.e("call","Marker drag end long " + TaxiUtil.picup_Long);
//
//        getAddressFromMarkerLatLong(TaxiUtil.picup_Lat,TaxiUtil.picup_Long);
//    }
//
//    @Override
//    public void onMarkerDragStart(Marker marker) {
//
//    }

    public void getAddressFromMarkerLatLong(double latitude, double longitude) {
        try {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this, Locale.getDefault());

            if (latitude != 0 && longitude != 0) {
                getEstimated_lat = latitude;
                getEstimated_long = longitude;

                addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();

                Log.e("111111111111", "address = " + address);
                Log.e("111111111111", "city = " + city);
                Log.e("111111111111", "state = " + state);
                Log.e("111111111111", "country = " + country);
                Log.e("111111111111", "postalCode = " + postalCode);
                Log.e("111111111111", "knownName = " + knownName);
                getEstimated_Pickup = address;
                tvPickup.setText(address);
                TaxiUtil.picup_Address = address;
                TaxiUtil.picup_Lat = latitude;
                TaxiUtil.picup_Long = longitude;
                pickup_Touch = true;
                //auto_Pickup.dismissDropDown();

                //auto_Dropoff.setFocusableInTouchMode(true);
                //auto_Dropoff.requestFocus();
            }
        } catch (Exception e) {
            Log.e("call", "Exception = " + e.getMessage());
        }
    }

    @Override
    public void onCameraChange(CameraPosition cameraPosition) {

        Log.e("call", "onCameraChange()");
        if (googleMap != null) {
            Log.e("call", "onCameraChange() 1111111");
            Log.e("call", "onCameraChange() selectedCar = " + selectedCar);
            Log.e("call", "onCameraChange() myLocation = " + myLocation);
            if (selectedCar == -1 && myLocation == true) {
                if (tripFlag == 0) {
                    Log.e("call", "onCameraChange() 222222222");
                    current_marker_layout.setVisibility(View.VISIBLE);
                    // Get the center of the Map.
                    LatLng centerOfMap = googleMap.getCameraPosition().target;
                    Log.e("call", "onCameraChange() centerOfMap.latitude = " + centerOfMap.latitude);
                    Log.e("call", "onCameraChange() centerOfMap.longitude = " + centerOfMap.longitude);
                    getAddressFromMarkerLatLong(centerOfMap.latitude, centerOfMap.longitude);
                } else {
                    current_marker_layout.setVisibility(View.GONE);
                }
            } else {
                Log.e("call", "onCameraChange() 333333");
                current_marker_layout.setVisibility(View.GONE);
            }
        } else {
            Log.e("call", "onCameraChange() 4444444444");
            current_marker_layout.setGravity(View.GONE);
        }
    }

    public void takePermission() {
        if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, Manifest.permission.READ_CONTACTS)) {
                Log.e("call", " permision 222222222222");
                //Show Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle(getResources().getString(R.string.storage_permission));
                builder.setMessage(getResources().getString(R.string.this_app_need_storage_permission));
                builder.setPositiveButton(getResources().getString(R.string.grant), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Log.e("call", " permision 33333333");
                        ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_CONTACTS}, 100);
                    }
                });
                builder.setNegativeButton(getResources().getString(R.string.cancle), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Log.e("call", " permision 44444444444");
                    }
                });
                builder.show();
            } else {
                Log.e("call", " permision 5555555555");
                //just request the permission
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_CONTACTS}, 100);
            }
            Log.e("call", " permision 6666666666666");
            SharedPreferences.Editor editor = permissionStatus.edit();
            editor.putBoolean(Manifest.permission.READ_CONTACTS, true);
            editor.commit();
        } else if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, Manifest.permission.SEND_SMS)) {
                Log.e("call", " permision 222222222222");
                //Show Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Send SMS");
                builder.setMessage("This app need send sms permission");
                builder.setPositiveButton(getResources().getString(R.string.grant), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Log.e("call", " permision 33333333");
                        ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.SEND_SMS}, 100);
                    }
                });
                builder.setNegativeButton(getResources().getString(R.string.cancle), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Log.e("call", " permision 44444444444");
                    }
                });
                builder.show();
            } else {
                Log.e("call", " permision 5555555555");
                //just request the permission
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.SEND_SMS}, 100);
            }
            Log.e("call", " permision 6666666666666");
            SharedPreferences.Editor editor = permissionStatus.edit();
            editor.putBoolean(Manifest.permission.SEND_SMS, true);
            editor.commit();
        } else if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, Manifest.permission.READ_PHONE_STATE)) {
                Log.e("call", " permision 222222222222");
                //Show Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Read Phone State permission");
                builder.setMessage("This app need to read phone state permission");
                builder.setPositiveButton(getResources().getString(R.string.grant), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Log.e("call", " permision 33333333");
                        ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_PHONE_STATE}, 100);
                    }
                });

                builder.setNegativeButton(getResources().getString(R.string.cancle), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Log.e("call", " permision 44444444444");
                    }
                });
                builder.show();
            } else {
                Log.e("call", " permision 5555555555");
                //just request the permission
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_PHONE_STATE}, 100);
            }
            Log.e("call", " permision 6666666666666");
            SharedPreferences.Editor editor = permissionStatus.edit();
            editor.putBoolean(Manifest.permission.READ_PHONE_STATE, true);
            editor.commit();
        } else {
//            OpenSMS("9409017667","vishal test");
            OpenSMS(driverPhone, "");
//            pickAContactNumber();
        }
    }

    /*//Todo when button is clicked
    public void pickAContactNumber()
    {
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(intent, PICK_CONTACT);
    }*/

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

        if (reqCode == PICKUP_AUTOCOMPLETE_REQUEST_CODE && resultCode == Activity.RESULT_OK) {

            pickup_Touch = true;

            Place place = Autocomplete.getPlaceFromIntent(data);
            Log.e("TAG", "Place: " + place.getName() + ", " + place.getAddress());
            tvPickup.setText(place.getName() + "," + place.getAddress());
            final InitData initData = SessionSave.getInitData();

          /*  Geocoder geocoder = new Geocoder(this);
            try {
                List<Address> addresses = geocoder.getFromLocation(place.getLatLng().latitude, place.getLatLng().longitude, 1);
                //String address = addresses.get(0).getAddressLine(0);
                String city = addresses.get(0).getAddressLine(1);
                Log.w("cityNameActivityResult", "" + city);
                if (city != null && !city.equalsIgnoreCase("null") && !city.equalsIgnoreCase("")) {
                    cityName = city;
                }
                //String country = addresses.get(0).getAddressLine(2);

            } catch (IOException e) {

                e.printStackTrace();
            }*/

            if (!Constants.newgpsLatitude.equalsIgnoreCase("") && !Constants.newgpsLongitude.equalsIgnoreCase("") && !Constants.newgpsLatitude.equalsIgnoreCase("0") && !Constants.newgpsLongitude.equalsIgnoreCase("0")) {

                String urlLocation = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + place.getLatLng().latitude + "," + place.getLatLng().longitude + "&key=" + getString(R.string.map_key);
                Log.w("urlLocation", "" + urlLocation);
                dialogClass.showDialog();
                aQuery.ajax(urlLocation.trim(), null, JSONObject.class, new AjaxCallback<JSONObject>() {

                    @Override
                    public void callback(String url, JSONObject json, AjaxStatus status) {

                        try {
                            int responseCode = status.getCode();
                            Log.e("TAGCityFind", "responseCode = " + responseCode);
                            Log.e("TAGCityFind", "Response = " + json);
                            dialogClass.hideDialog();

                            JSONArray resultsAry = json.getJSONArray("results");
                            JSONObject jObj = new JSONObject(resultsAry.getString(0));
                            JSONArray addCompAry = jObj.getJSONArray("address_components");

                            String short_name = "";
                            int idxCity = -1;
                            boolean isContainsCity = false;
                            boolean isFoundCity = false;

                            for (int adComp = 0; adComp < addCompAry.length(); adComp++) {
                                JSONObject cityObj = addCompAry.getJSONObject(adComp);
                                JSONArray typesAry = cityObj.getJSONArray("types");

                                for (int j = 0; j < typesAry.length(); j++) {
                                    if (typesAry.get(j).toString().equalsIgnoreCase("administrative_area_level_2")) {
                                        isContainsCity = true;
                                        idxCity = adComp;
                                        break;
                                    }
                                }
                            }


                            for (int adComp = 0; adComp < addCompAry.length(); adComp++) {
                                JSONObject cityObj = addCompAry.getJSONObject(adComp);
                                JSONArray typesAry = cityObj.getJSONArray("types");
                                if (typesAry.length() > 1) {
                                    if (/*typesAry.getString(0).equalsIgnoreCase("administrative_area_level_2") && */isContainsCity && adComp == idxCity && typesAry.getString(1).equalsIgnoreCase("political")) {
                                        cityName = cityObj.getString("long_name");
                                        short_name = cityObj.getString("short_name");
                                        for (int i = 0; i < initData.getCityList().size(); i++) {
                                            String listCityName = initData.getCityList().get(i).getCityName().toLowerCase().trim();
                                            Log.w("getCityName()", "" + initData.getCityList().get(i).getCityName());
                                            if (cityName.equalsIgnoreCase(listCityName)) {
                                                isFoundCity = true;
                                                //    cityId = initData.getCityList().get(i).getId();
                                                //  callCarListAPi();
                                                break;
                                            } else if (short_name.equalsIgnoreCase(listCityName)) {
                                                cityName = short_name;
                                                isFoundCity = true;
                                                //   cityId = initData.getCityList().get(i).getId();
                                                // callCarListAPi();
                                                break;
                                            }
                                        }
                                        break;
                                    } else if (!isContainsCity && typesAry.getString(0).equalsIgnoreCase("locality") && typesAry.getString(1).equalsIgnoreCase("political")) {
                                        // JSONObject cityObjFinal = new JSONObject(addCompAry.getString(1));
                                        cityName = cityObj.getString("long_name");
                                        for (int i = 0; i < initData.getCityList().size(); i++) {
                                            String listCityName = initData.getCityList().get(i).getCityName().toLowerCase().trim();
                                            Log.w("getCityName()", "" + initData.getCityList().get(i).getCityName());
                                            if (cityName.equalsIgnoreCase(listCityName)) {
                                                isFoundCity  = true;
                                                //   cityId = initData.getCityList().get(i).getId();
                                                //    callCarListAPi();
                                                break;
                                            } else if (short_name.equalsIgnoreCase(listCityName)) {
                                                isFoundCity = true;
                                                cityName = short_name;
                                                //   cityId = initData.getCityList().get(i).getId();
                                                //   callCarListAPi();
                                                break;
                                            }
                                        }
                                        break;
                                    }
                                }
                            }

                            if(!isFoundCity){
                                cityName = "Melbourne";
                                cityId = "4";
                            }
                            Log.w("resultAry", "" + cityName);
                        } catch (JSONException e) {
                            Log.e("Exception", "Exception " + e.toString());
                            dialogClass.hideDialog();

                        }
                    }

                }.method(AQuery.METHOD_GET));

            }


            tvPickup.setSelected(true);
            tvPickup.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            tvPickup.setSingleLine(true);

            TaxiUtil.picup_Address = place.getName() + "," + place.getAddress();
            TaxiUtil.picup_Lat = place.getLatLng().latitude;
            TaxiUtil.picup_Long = place.getLatLng().longitude;

            LatLng pickupLanlong = new LatLng(TaxiUtil.picup_Lat, TaxiUtil.picup_Long);
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder().target(pickupLanlong).zoom(17.5f).build()));

            getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
            );
            /*View view = this.getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }*/
            //cityName = "Melbourne";
            getCityName();
            if (!tvPickup.getText().toString().trim().equalsIgnoreCase("") && !tvDropoff.getText().toString().trim().equalsIgnoreCase(""))
            {
                if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG,activity)!=null &&
                        !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG,activity).equalsIgnoreCase(""))
                {
                    tripFlag = Integer.parseInt(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG,activity));

                    if (tripFlag==0)
                    {
                        startTimer();
                    }
                    else
                    {
                        startTimerForDriverLocation();
                    }
                }
            }
            getEstimatedFareCheckValidation();

        } else if (reqCode == DROPOFF_AUTOCOMPLETE_REQUEST_CODE && resultCode == Activity.RESULT_OK) {

            dropoff_Touch = true;

            Place place = Autocomplete.getPlaceFromIntent(data);
            Log.e("TAG", "Place: " + place.getName() + ", " + place.getAddress());
            tvDropoff.setText(place.getName() + "," + place.getAddress());

            tvDropoff.setSelected(true);
            tvDropoff.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            tvDropoff.setSingleLine(true);

            TaxiUtil.dropoff_Address = place.getName() + "," + place.getAddress();
            TaxiUtil.dropoff_Lat = place.getLatLng().latitude;
            ;
            TaxiUtil.dropoff_Long = place.getLatLng().longitude;

            getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
            );
            /*View view = this.getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }*/

            if (!tvPickup.getText().toString().trim().equalsIgnoreCase("") && !tvDropoff.getText().toString().trim().equalsIgnoreCase(""))
            {
                if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG,activity)!=null &&
                        !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG,activity).equalsIgnoreCase(""))
                {
                    tripFlag = Integer.parseInt(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG,activity));

                    if (tripFlag==0)
                    {
                        startTimer();
                    }
                    else
                    {
                        startTimerForDriverLocation();
                    }
                }
            }
            getEstimatedFareCheckValidation();

        } else {
            setTimeAndPriceToZero();
        }
    }

    private void OpenSMS(String phoneNo, String messageszz) {

        final String phone = phoneNo.trim().replace(" ", "");
        /*String message = name+" has invited you to become a TiCKTOC Passenger.\n" +
                "click here goo.gl/uqxHTs"+
                "\nYour invite code is: " + iniviteCode + "\n"+
                "www.facebook.com/ticktoc.net";*/

        Log.e("call", "phone number = " + phone);
        Log.e("call", "message = " + messageszz);
        //Check if the phoneNumber is empty
        if (phone.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please Enter a Valid Phone Number", Toast.LENGTH_SHORT).show();
        } else {
//            SmsManager.getDefault().sendTextMessage(phone, null, messageszz, null,null);

            Uri uri = Uri.parse("smsto:" + phone);
            Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
            intent.putExtra("sms_body", messageszz);
            startActivity(intent);

            /*final SmsManager sms = SmsManager.getDefault();
            // if message length is too long messages are divided
            List<String> messages = sms.divideMessage(message);
            for (final String msg : messages)
            {

                final PendingIntent sentIntent = PendingIntent.getBroadcast(this, 0, new Intent("SMS_SENT"), 0);
                final PendingIntent deliveredIntent = PendingIntent.getBroadcast(this, 0, new Intent("SMS_DELIVERED"), 0);

                new Handler().postDelayed(new Runnable() {
                    public void run() {

                        sms.sendTextMessage(phone, null, msg, sentIntent, deliveredIntent);
                    }
                }, 1000);
            }*/
        }
    }


}
package iiride.app.rider.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import iiride.app.rider.R;
import iiride.app.rider.application.TicktocApplication;
import iiride.app.rider.been.CreditCard_List_Been;
import iiride.app.rider.comman.Common;
import iiride.app.rider.comman.SessionSave;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class UpdateProfileActivity extends AppCompatActivity implements View.OnClickListener{

    public static UpdateProfileActivity activity;
    private LinearLayout ll_Back;
    private ImageView iv_Back;
    private TextView tv_Title;
    private CardView card_EditProfile,card_EditAccount;
    private Animation animationProfile,animationAccount;
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);

        activity = UpdateProfileActivity.this;
        animationProfile = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.card_slide);
        animationAccount = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.card_slide_left);
        handler = new Handler();
        init();
    }

    private void init() {

        ll_Back = (LinearLayout) findViewById(R.id.back_layout);
        iv_Back = (ImageView) findViewById(R.id.back_imageview);
        tv_Title = (TextView) findViewById(R.id.title_textview);
        tv_Title.setText("Profile");

        card_EditProfile = (CardView) findViewById(R.id. card_EditProfile);
        card_EditAccount = (CardView) findViewById(R.id. card_EditAccount);

        ll_Back.setOnClickListener(activity);
        iv_Back.setOnClickListener(activity);
        card_EditProfile.setOnClickListener(activity);
        card_EditAccount.setOnClickListener(activity);

        handler.postDelayed(runnable,700);
    }

    Runnable runnable = new Runnable() {
        @Override
        public void run()
        {

            card_EditProfile.startAnimation(animationProfile);
            card_EditProfile.setVisibility(View.VISIBLE);
            card_EditAccount.startAnimation(animationAccount);
            card_EditAccount.setVisibility(View.VISIBLE);
        }
    };

    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.back_layout:
                onBackPressed();
                break;

            case R.id.back_imageview:
                onBackPressed();
                break;

            case R.id.card_EditProfile:
                gotoProfile();
                break;

            case R.id.card_EditAccount:
                gotoAccount();
                break;
        }
    }

    private void gotoAccount()
    {
        handler.removeCallbacks(runnable,null);
//        Intent intent = new Intent(activity,Profile_AccountDetail_Activity.class);
//        startActivity(intent);
//        overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
        if (checkCardList())
        {
            Intent intent = new Intent(UpdateProfileActivity.this,Wallet_Add_Cards_Activity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        }
        else
        {
            Intent intent = new Intent(UpdateProfileActivity.this,Add_Card_In_List_Activity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        }
    }

    public boolean checkCardList()
    {
        try
        {
            Log.e("call","111111111111111");
            String strCardList = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_CARD_LIST,activity);
            List<CreditCard_List_Been> cardListBeens = new ArrayList<CreditCard_List_Been>();

            if (strCardList!=null && !strCardList.equalsIgnoreCase(""))
            {
                Log.e("call","222222222222222");
                JSONObject json = new JSONObject(strCardList);

                if (json!=null)
                {
                    Log.e("call","33333333333333");
                    if (json.has("cards"))
                    {
                        Log.e("call","6666666666666666");
                        JSONArray cards = json.getJSONArray("cards");

                        if (cards!=null && cards.length()>0)
                        {
                            return true;
                        }
                        else
                        {
                            Log.e("call","no cards available");
                            return false;
                        }
                    }
                    else
                    {
                        Log.e("call","no cards found");
                        return false;
                    }
                }
                else
                {
                    Log.e("call","json null");
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        catch (Exception e)
        {
            Log.e("call","Exception in getting card list = "+e.getMessage());
            return false;
        }
    }

    private void gotoProfile()
    {
        handler.removeCallbacks(runnable,null);
        Intent intent = new Intent(activity,ProfileActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();

        TicktocApplication.setCurrentActivity(activity);

    }
}

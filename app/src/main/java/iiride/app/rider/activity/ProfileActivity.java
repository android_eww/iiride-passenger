package iiride.app.rider.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;


import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import iiride.app.rider.R;
import iiride.app.rider.application.TicktocApplication;
import iiride.app.rider.comman.Common;
import iiride.app.rider.comman.SessionSave;
import iiride.app.rider.comman.WebServiceAPI;
import iiride.app.rider.other.DialogClass;
import iiride.app.rider.other.Global;
import iiride.app.rider.other.InternetDialog;
import iiride.app.rider.roundedimage.RoundedTransformationBuilder;
import iiride.app.rider.view.MySnackBar;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener{

    public static ProfileActivity activity;
    private LinearLayout ll_Back;
    private ImageView iv_Back;
    private TextView tv_Title;
    private CardView cardViewChangePassword;
    private ImageView iv_ProfileImage;
    private TextView tv_Email,tv_PhoneNumber,tv_Save;
    private EditText et_FirstName,et_LastName,et_Address;
    private RadioButton radioButtonMale,radioButtonFemale;
    private String maleFemal = "";

    private DialogClass dialogClass;
    private AQuery aQuery;
    private Transformation mTransformation;
    private byte[] image;
    private LinearLayout ll_RootLayout;
    private MySnackBar mySnackBar;
    public static boolean changeImage = false;

    private String filename="";
    private SharedPreferences permissionStatus;
    public static String base64 = "";
    public static Bitmap bmp = null;
    private boolean sentToSettings = false;
    private Dialog dialog;
    private Uri selectedImageUri;
    private CircleImageView imageProfile;

    Uri mImageUri;
    private Uri imageUri;
    Bitmap bitmapImage;
    String picturePath;
    static Uri uriGallery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        activity = ProfileActivity.this;

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        changeImage = false;
        image = null;
        maleFemal = "";
        bmp = null;
        base64 = "";
        mImageUri=null;
        imageUri=null;
        bitmapImage=null;
        picturePath=null;
        uriGallery=null;
        permissionStatus = getSharedPreferences("permissionStatus",MODE_PRIVATE);

        dialogClass = new DialogClass(activity,0);
        aQuery = new AQuery(activity);

        mTransformation = new RoundedTransformationBuilder()
                .cornerRadiusDp(100)
                .borderColor(getResources().getColor(R.color.colorRed))
                .borderWidthDp(2)
                .oval(true)
                .build();

        init();
    }




    private void init() {

        ll_Back = (LinearLayout) findViewById(R.id.back_layout);
        iv_Back = (ImageView) findViewById(R.id.back_imageview);
        tv_Title = (TextView) findViewById(R.id.title_textview);
        tv_Title.setText("Profile");

        tv_Email = (TextView) findViewById(R.id.email_textview);
        tv_PhoneNumber = (TextView) findViewById(R.id.phone_textview);
        tv_Save = (TextView) findViewById(R.id.save_textview);
        imageProfile = (CircleImageView) findViewById(R.id.image_profile);
        et_FirstName = (EditText) findViewById(R.id.input_first_name);
        et_LastName = (EditText) findViewById(R.id.input_last_name);
        et_Address = (EditText) findViewById(R.id.input_address);

        ll_RootLayout = (LinearLayout) findViewById(R.id.rootView);

        radioButtonMale = (RadioButton) findViewById(R.id.radio_button_male);
        radioButtonFemale = (RadioButton) findViewById(R.id.radio_button_female);

        iv_ProfileImage = (ImageView) findViewById(R.id.profile_activity_image);

        cardViewChangePassword = (CardView) findViewById(R.id.change_password_card_view);

        mySnackBar = new MySnackBar(activity);

        ll_Back.setOnClickListener(activity);
        iv_Back.setOnClickListener(activity);
        cardViewChangePassword.setOnClickListener(activity);
        tv_Save.setOnClickListener(activity);
        iv_ProfileImage.setOnClickListener(activity);
        imageProfile.setOnClickListener(activity);

        radioButtonMale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                maleFemal = "male";
                radioButtonFemale.setChecked(false);
                radioButtonMale.setChecked(true);
            }
        });

        radioButtonFemale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                maleFemal = "female";
                radioButtonFemale.setChecked(true);
                radioButtonMale.setChecked(false);
            }
        });

        setUserDetail();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.back_layout:
                onBackPressed();
                break;

            case R.id.back_imageview:
                onBackPressed();
                break;

            case R.id.change_password_card_view:
                gotoChangePassword();
                break;

            case R.id.save_textview:
                Log.e("call","save clicked");
                checkValidation();
                break;

            case R.id.profile_activity_image:
                Log.e("call","image clicked");
                takePermission();
                break;

            case R.id.image_profile:
                Log.e("call","image clicked");
                takePermission();
                break;
        }
    }

    public void gotoChangePassword()
    {
        Intent intent = new Intent(ProfileActivity.this,ChangePasswordActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);
    }

    public void setUserDetail()
    {
        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_FULL_NAME,activity)!=null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_FULL_NAME,activity).equalsIgnoreCase(""))
        {
            String[] name = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_FULL_NAME,activity).toString().trim().split(" ");

            if (name.length>0)
            {
                et_FirstName.setText(name[0]);
                et_LastName.setText(name[1]);
            }
            else
            {
                et_FirstName.setText("");
                et_LastName.setText("");
            }
        }
        else
        {
            et_FirstName.setText("");
            et_LastName.setText("");
        }

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_EMAIL,activity)!=null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_EMAIL,activity).equalsIgnoreCase(""))
        {
            tv_Email.setText(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_EMAIL,activity));
        }
        else
        {
            tv_Email.setText("");
        }

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_PHONE_NUMBER,activity)!=null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_PHONE_NUMBER,activity).equalsIgnoreCase(""))
        {
            tv_PhoneNumber.setText(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_PHONE_NUMBER,activity));
        }
        else
        {
            tv_PhoneNumber.setText("");
        }

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ADDRESS,activity)!=null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ADDRESS,activity).equalsIgnoreCase(""))
        {
            et_Address.setText(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ADDRESS,activity));
        }
        else
        {
            et_Address.setText("");
        }

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_GENDER,activity)!=null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_GENDER,activity).equalsIgnoreCase(""))
        {
            maleFemal = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_GENDER,activity);

            if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_GENDER,activity).equalsIgnoreCase("male"))
            {
                radioButtonFemale.setChecked(false);
                radioButtonMale.setChecked(true);
            }
            else if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_GENDER,activity).equalsIgnoreCase("female"))
            {
                radioButtonFemale.setChecked(true);
                radioButtonMale.setChecked(false);
            }
            else
            {
                radioButtonFemale.setChecked(false);
                radioButtonMale.setChecked(false);
            }
        }
        else
        {
            maleFemal = "";
            radioButtonFemale.setChecked(false);
            radioButtonMale.setChecked(false);
        }

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_IMAGE,activity)!=null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_IMAGE,activity).equals(""))
        {
            iv_ProfileImage.setVisibility(View.VISIBLE);
            imageProfile.setVisibility(View.GONE);
            Picasso.with(activity)
                    .load(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_IMAGE,activity))
                    .fit()
                    .transform(mTransformation)
                    .into(iv_ProfileImage);
        }
        else
        {
            iv_ProfileImage.setVisibility(View.VISIBLE);
            imageProfile.setVisibility(View.GONE);
            iv_ProfileImage.setImageResource(R.mipmap.man);
        }
    }

    public void checkValidation()
    {
        if (et_FirstName.getText().toString().trim().equals(""))
        {
            mySnackBar.showSnackBar(ll_RootLayout,"Please enter First Name!");
            et_FirstName.setFocusableInTouchMode(true);
            et_FirstName.requestFocus();
        }
        else if (et_LastName.getText().toString().trim().equals(""))
        {
            mySnackBar.showSnackBar(ll_RootLayout,"Please enter Last Name!");
            et_LastName.setFocusableInTouchMode(true);
            et_LastName.requestFocus();
        }
        else if (et_Address.getText().toString().trim().equals(""))
        {
            mySnackBar.showSnackBar(ll_RootLayout,"Please enter Address!");
            et_Address.setFocusableInTouchMode(true);
            et_Address.requestFocus();
        }
        else if (maleFemal.equals(""))
        {
            mySnackBar.showSnackBar(ll_RootLayout,"Please select Gender!");
        }
        else
        {
            if (Global.isNetworkconn(activity))
            {
                call_UpdateProfile();
            }
            else
            {
                InternetDialog internetDialog = new InternetDialog(activity);
                internetDialog.showDialog("Please check your internet connection!");
            }
        }
    }

    private void call_UpdateProfile()
    {
        dialogClass.showDialog();
        String url = WebServiceAPI.API_UPDATE_PROFILE;

        //http://54.206.55.185/web/Passenger_Api/UpdateProfile
        //PassengerId,Fullname,Gender,Address,Image
        Log.e("call","url = "+url);

        String passangerId = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID,activity);
        String fullName = et_FirstName.getText().toString().trim() + " " + et_LastName.getText().toString().trim();
        String address = et_Address.getText().toString().trim();
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(WebServiceAPI.PARAM_PASSENGER_ID,passangerId);
        params.put(WebServiceAPI.PARAM_FULL_NAME,fullName);
        params.put(WebServiceAPI.PARAM_GENDER,maleFemal);
        params.put(WebServiceAPI.PARAM_ADDRESS,address);

        Log.e("call","changeImage flag = "+changeImage);

        if (changeImage)
        {
            if (image!=null)
            {
                Log.e("call","image byte array = "+image.toString());
                params.put(WebServiceAPI.PARAM_IMAGE,image);
            }
            else
            {
                Log.e("call","image byte array null");
            }
        }
        else
        {
            Log.e("call","image not change");
        }

        Log.e("call", "param = " + params);

        aQuery.ajax(url.trim(), params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e("responseCode", " = " + responseCode);
                    Log.e("Response", " = " + json);
                    dialogClass.hideDialog();
                    if (json!=null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                if (json.has("profile"))
                                {
                                    JSONObject profile = json.getJSONObject("profile");

                                    if (profile!=null)
                                    {
                                        if (profile.has("Fullname"))
                                        {
                                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_FULL_NAME,profile.getString("Fullname"),activity);
                                        }

                                        if (profile.has("Image"))
                                        {
                                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_IMAGE,profile.getString("Image"),activity);
                                        }

                                        if (profile.has("Gender"))
                                        {
                                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_GENDER,profile.getString("Gender"),activity);
                                        }

                                        if (profile.has("Address"))
                                        {
                                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_ADDRESS,profile.getString("Address"),activity);
                                        }
                                    }
                                }
                                dialogClass.hideDialog();
                                showSuccessMessage("Your Profile update successfully!");
                            }
                            else
                            {
                                dialogClass.hideDialog();
                                showSuccessMessage("Please try again later!");
                            }
                        }
                        else
                        {
                            dialogClass.hideDialog();
                            showSuccessMessage("Please try again later!");
                        }
                    }
                    else
                    {
                        dialogClass.hideDialog();
                        showSuccessMessage("Please try again later!");
                    }
                }
                catch (Exception e)
                {
                    Log.e("Exception","Exception "+e.toString());
                    dialogClass.hideDialog();
                }
            }

        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    public void showSuccessMessage(String message)
    {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.my_dialog_class);

        TextView tv_Ok = (TextView) dialog.findViewById(R.id.Ok);
        TextView tv_Message = (TextView) dialog.findViewById(R.id.message);
        LinearLayout ll_Ok = (LinearLayout) dialog.findViewById(R.id.ok_layout);

        tv_Message.setText(message);

        ll_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        tv_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        dialog.show();
    }

    //forselect image
    public void takePermission()
    {
        if (ActivityCompat.checkSelfPermission(ProfileActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
        {
            Log.e("call"," permision 1111111111");
            if (ActivityCompat.shouldShowRequestPermissionRationale(ProfileActivity.this, Manifest.permission.CAMERA))
            {
                Log.e("call"," permision 222222222222");
                //Show Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
                builder.setTitle(getResources().getString(R.string.need_camera_permission));
                builder.setMessage(getResources().getString(R.string.this_app_need_camera_permission));
                builder.setPositiveButton(getResources().getString(R.string.grant), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Log.e("call"," permision 33333333");
                        ActivityCompat.requestPermissions(ProfileActivity.this, new String[]{Manifest.permission.CAMERA}, 100);
                    }
                });
                builder.setNegativeButton(getResources().getString(R.string.cancle), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Log.e("call"," permision 44444444444");
                    }
                });
                builder.show();
            }
            else
            {
                Log.e("call"," permision 5555555555");
                //just request the permission
                ActivityCompat.requestPermissions(ProfileActivity.this, new String[]{Manifest.permission.CAMERA}, 100);
            }
            Log.e("call"," permision 6666666666666");
            SharedPreferences.Editor editor = permissionStatus.edit();
            editor.putBoolean(Manifest.permission.CAMERA,true);
            editor.commit();
        }
        else if (ActivityCompat.checkSelfPermission(ProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            if (ActivityCompat.shouldShowRequestPermissionRationale(ProfileActivity.this, Manifest.permission.CAMERA))
            {
                Log.e("call"," permision 222222222222");
                //Show Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
                builder.setTitle(getResources().getString(R.string.storage_permission));
                builder.setMessage(getResources().getString(R.string.this_app_need_storage_permission));
                builder.setPositiveButton(getResources().getString(R.string.grant), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Log.e("call"," permision 33333333");
                        ActivityCompat.requestPermissions(ProfileActivity.this, new String[]{Manifest.permission.CAMERA}, 100);
                    }
                });
                builder.setNegativeButton(getResources().getString(R.string.cancle), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Log.e("call"," permision 44444444444");
                    }
                });
                builder.show();
            }
            else
            {
                Log.e("call"," permision 5555555555");
                //just request the permission
                ActivityCompat.requestPermissions(ProfileActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 100);
            }
            Log.e("call"," permision 6666666666666");
            SharedPreferences.Editor editor = permissionStatus.edit();
            editor.putBoolean(Manifest.permission.WRITE_EXTERNAL_STORAGE,true);
            editor.commit();
        }
        else
        {
            seletcOption();
        }
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        if (sentToSettings)
        {
            if (ActivityCompat.checkSelfPermission(ProfileActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                //Got Permission
            }

            if (ActivityCompat.checkSelfPermission(ProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                //Got Permission
            }
        }
    }

    public void seletcOption()
    {
        LayoutInflater layoutInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.select_option, null);

        dialog = new Dialog(ProfileActivity.this,R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(view);

        final LinearLayout ll_Gallery,ll_Camera,ll_Cancel;
        final TextView tv_Gallery,tv_Camera,tv_Cancel;

        ll_Camera = (LinearLayout) view.findViewById(R.id.select_option_camera_layout);
        ll_Gallery = (LinearLayout) view.findViewById(R.id.select_option_gallery_layout);
        ll_Cancel = (LinearLayout) view.findViewById(R.id.select_option_cancel_layout);

        tv_Camera = (TextView) view.findViewById(R.id.select_option_camera_textview);
        tv_Gallery = (TextView) view.findViewById(R.id.select_option_gallery_textview);
        tv_Cancel = (TextView) view.findViewById(R.id.select_option_cancel_textview);

        ll_Camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                openCamera();
            }
        });

        tv_Camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                openCamera();
            }
        });

        ll_Gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                selectPhoto();
            }
        });

        tv_Gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                selectPhoto();
            }
        });

        ll_Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        tv_Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        dialog.show();
    }

    //Image Pick From Camera
    private void openCamera() {

        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "NewPicture");
        imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        startActivityForResult(intent, 0);
    }

    //Image Pick From Gallary
    private void selectPhoto() {
        Intent intent_gallery = new Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent_gallery, 1);
    }

//    public void selectPhoto()
//    {
//        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//        intent.setType("image/*");
//        startActivityForResult(intent,1);
//    }
//
//    public void openCamera()
//    {
//        ContentValues values = new ContentValues();
//        values.put(MediaStore.Images.Media.TITLE, "NewPicture");
//        imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
//        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
//        startActivityForResult(intent, 2);
//    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try
        {
            Log.e("resultCode"," = "+resultCode);

            if ( resultCode == RESULT_OK )
            {

                if (requestCode == 1 && resultCode == RESULT_OK && null != data)
                {
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = { MediaStore.Images.Media.DATA };

                    uriGallery = data.getData();
                    Log.d("2222",""+data.getData());
                    Cursor cursor = getContentResolver().query(selectedImage,
                            filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    picturePath = cursor.getString(columnIndex);
                    cursor.close();
                    Log.d("filePathUri", "" + picturePath);
                    mImageUri = Uri.parse(picturePath);
                /*frameActivity( filePathUri );*/
                    bitmapImage = BitmapFactory.decodeFile(String.valueOf(mImageUri));
                    bitmapImage = getResizedBitmap(bitmapImage,400);
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmapImage.compress(Bitmap.CompressFormat.PNG, 10, stream);
                    byte[] byteArray = stream.toByteArray();
                    image = byteArray;
                    changeImage = true;
                    imageProfile.setVisibility(View.VISIBLE);
                    iv_ProfileImage.setVisibility(View.GONE);
                    imageProfile.setImageBitmap(bitmapImage);

                }
                else if(requestCode==0)
                {
                    Bitmap thumbnail = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
                    Log.e("#########","bmp height = "+thumbnail.getHeight());
                    Log.e("#########","bmp width = "+thumbnail.getWidth());
                    thumbnail = getResizedBitmap(thumbnail,400);
                    bitmapImage=thumbnail;
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    thumbnail.compress(Bitmap.CompressFormat.PNG, 10, bytes);
                    byte[] byteArray = bytes.toByteArray();
                    image = byteArray;
                    changeImage = true;
                    imageProfile.setVisibility(View.VISIBLE);
                    iv_ProfileImage.setVisibility(View.GONE);
                    imageProfile.setImageBitmap(bitmapImage);
                }
                else
                {
                    image = null;
                    changeImage = false;
                }
            }
        }
        catch (Exception e)
        {
            Log.e("call","exception = "+e.getMessage());
        }

    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    @Override
    protected void onResume() {
        super.onResume();

        TicktocApplication.setCurrentActivity(activity);

    }
}

package iiride.app.rider.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;


import iiride.app.rider.R;
import iiride.app.rider.comman.Common;
import iiride.app.rider.comman.SessionSave;


public class SettingActivity extends AppCompatActivity implements View.OnClickListener {

    public static SettingActivity activity;

    public static int falge = 0,flageChangePasscode = 0;

    private LinearLayout ll_Back, ll_change_passcode,ll_profile;
    private ImageView iv_Back;
    private TextView tv_Title;
    private Switch sw_passcode;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        activity = SettingActivity.this;
        falge = 0;
        flageChangePasscode = 0;

        init();

    }

    private void init()
    {
        ll_Back = (LinearLayout) findViewById(R.id.back_layout);
        ll_change_passcode = (LinearLayout) findViewById(R.id.ll_change_passcode);
        ll_profile = (LinearLayout) findViewById(R.id.ll_profile);
        iv_Back = (ImageView) findViewById(R.id.back_imageview);
        tv_Title = (TextView) findViewById(R.id.title_textview);

        sw_passcode = (Switch)findViewById(R.id.sw_passcode);


        tv_Title.setText("Settings");

        iv_Back.setOnClickListener(this);
        ll_change_passcode.setOnClickListener(this);
        ll_profile.setOnClickListener(this);


        if (SessionSave.getUserSession(Common.IS_PASSCODE_REQUIRED,activity) != null && SessionSave.getUserSession(Common.IS_PASSCODE_REQUIRED,activity).equalsIgnoreCase("0"))
        {
            sw_passcode.setChecked(false);
            ll_change_passcode.setVisibility(View.GONE);
        }
        else if (SessionSave.getUserSession(Common.IS_PASSCODE_REQUIRED,activity) != null && SessionSave.getUserSession(Common.IS_PASSCODE_REQUIRED,activity).equalsIgnoreCase("1"))
        {
            sw_passcode.setChecked(true);
            ll_change_passcode.setVisibility(View.VISIBLE);
        }

        sw_passcode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if (isChecked)
                {
                    Log.e("switchState" , "switchStateTrue" );

                    if (SessionSave.getUserSession(Common.CREATED_PASSCODE,activity) != null && SessionSave.getUserSession(Common.CREATED_PASSCODE,activity).equalsIgnoreCase("") )
                    {
                        ll_change_passcode.setVisibility(View.GONE);

                        Handler handler = new Handler();
                        Runnable runnable = new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                call_create_pass();
                            }
                        };
                        handler.postDelayed(runnable,300);

                    }
                    else
                    {

                        SessionSave.saveUserSession(Common.IS_PASSCODE_REQUIRED,"1",activity);
                        ll_change_passcode.setVisibility(View.VISIBLE);
                    }
                }
                else
                {
                    Log.e("switchState" , "switchStateFauls" );
                    SessionSave.saveUserSession(Common.IS_PASSCODE_REQUIRED,"0",activity);
                    ll_change_passcode.setVisibility(View.GONE);
                }

            }
        });


    }

    private void call_create_pass()
    {
        Intent intent = new Intent(activity,Create_Passcode_Activity.class);
        intent.putExtra("from","Setting");
        startActivity(intent);
        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.back_imageview:
                onBackPressed();
                break;

            case R.id.ll_change_passcode:
                flageChangePasscode = 1;
                call_change_passcode();
                break;

            case R.id.ll_profile:
                call_profile();
                break;
        }

    }

    private void call_profile()
    {
        Intent intentSettings = new Intent(SettingActivity.this,UpdateProfileActivity.class);
        startActivity(intentSettings);
        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
    }

    private void call_change_passcode()
    {
        Intent intentChangePass = new Intent(SettingActivity.this,Create_Passcode_Activity.class);
        intentChangePass.putExtra("from","Setting");
        startActivity(intentChangePass);
        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

    }

    @Override
    protected void onResume()
    {
        super.onResume();
        if (falge == 1)
        {
            flageChangePasscode = 0;
            if (SessionSave.getUserSession(Common.CREATED_PASSCODE,activity) != null && SessionSave.getUserSession(Common.CREATED_PASSCODE,activity).equalsIgnoreCase("") )
            {
                sw_passcode.setChecked(false);
            }
            else
            {
                sw_passcode.setChecked(true);
                SessionSave.saveUserSession(Common.IS_PASSCODE_REQUIRED,"1",activity);
                ll_change_passcode.setVisibility(View.VISIBLE);
            }
        }
    }
}

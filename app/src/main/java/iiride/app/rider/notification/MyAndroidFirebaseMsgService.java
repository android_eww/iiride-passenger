package iiride.app.rider.notification;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;

import com.google.firebase.messaging.RemoteMessage;

import iiride.app.rider.R;
import iiride.app.rider.activity.MainActivity;
import iiride.app.rider.activity.MyBookingActivity;
import iiride.app.rider.comman.Common;
import iiride.app.rider.comman.SessionSave;

import java.util.Map;
import java.util.Random;

public class MyAndroidFirebaseMsgService extends FirebaseMessagingService {
    private SharedPreferences pref;
    private Intent intent;
    private Random random;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.e("call", "onMessageReceived dsflkjsdalfjldsajflsda;jfl;dsajflsdjafsj");

        if (remoteMessage != null)
        {
            Map<String, String> data = remoteMessage.getData();

            if (data!=null)
            {
                String body = data.get("body");
                String title = data.get("title");
                String type = data.get("type");

                Log.e("call","onMessageReceived body = "+body);
                Log.e("call","onMessageReceived title = "+title);
                Log.e("call","onMessageReceived type = "+type);
                random = new Random();
                int m = random.nextInt(9999 - 1000) + 1000;
                createNotificationAccountVerify(body,title,m,type);
            }
            else
            {
                Log.e("call","onMessageReceived data null");
            }
        }
        else
        {
            Log.e("call","onMessageReceived remoteMessage null");
        }
    }

    private void createNotificationAccountVerify(String message,String title, int m, String type) {

        Log.e("call", "createNotification");

        if (type!=null && type.equalsIgnoreCase("AcceptBooking"))
        {
            SessionSave.saveUserSession(Common.PREFRENCE_NOTIFICATION,"AcceptBooking",this);
            intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |Intent.FLAG_ACTIVITY_SINGLE_TOP);
        }
        else if (type!=null && type.equalsIgnoreCase("OnTheWay"))
        {
            SessionSave.saveUserSession(Common.PREFRENCE_NOTIFICATION,"OnTheWay",this);
            intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |Intent.FLAG_ACTIVITY_SINGLE_TOP);
        }
        else if (type!=null && type.equalsIgnoreCase("RejectBooking"))
        {
            SessionSave.saveUserSession(Common.PREFRENCE_NOTIFICATION,"RejectBooking",this);
            intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |Intent.FLAG_ACTIVITY_SINGLE_TOP);
        }
        else if (type!=null && type.equalsIgnoreCase("Booking"))
        {
            SessionSave.saveUserSession(Common.PREFRENCE_NOTIFICATION,"Booking",this);
            intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |Intent.FLAG_ACTIVITY_SINGLE_TOP);
        }
        else if (type!=null && type.equalsIgnoreCase("AdvanceBooking"))
        {
            SessionSave.saveUserSession(Common.PREFRENCE_NOTIFICATION,"AdvanceBooking",this);
            intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |Intent.FLAG_ACTIVITY_SINGLE_TOP);
        }
        else if (type!=null && type.equalsIgnoreCase("AddMoney"))
        {
            SessionSave.saveUserSession(Common.PREFRENCE_NOTIFICATION,"AddMoney",this);
            intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |Intent.FLAG_ACTIVITY_SINGLE_TOP);
        }
        else if (type!=null && type.equalsIgnoreCase("TransferMoney"))
        {
            SessionSave.saveUserSession(Common.PREFRENCE_NOTIFICATION,"TransferMoney",this);
            intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |Intent.FLAG_ACTIVITY_SINGLE_TOP);
        }
        else if (type!=null && type.equalsIgnoreCase("Tickpay"))
        {
            SessionSave.saveUserSession(Common.PREFRENCE_NOTIFICATION,"Tickpay",this);
            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_VERIFY_USER,"2",this);
            intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |Intent.FLAG_ACTIVITY_SINGLE_TOP);
        }
        else if (type!=null && type.equalsIgnoreCase("CancelledBooking"))
        {
            SessionSave.saveUserSession(Common.PREFRENCE_NOTIFICATION,"CancelledBooking",this);
            intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |Intent.FLAG_ACTIVITY_SINGLE_TOP);
        }
        else
        {
            SessionSave.saveUserSession(Common.PREFRENCE_NOTIFICATION,"",this);
            intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |Intent.FLAG_ACTIVITY_SINGLE_TOP);
        }

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        Uri notificationSoundURI = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationManager notificationManager1 = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager1.cancelAll();

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O)
        {

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            String channelId = "All Notifications";
            CharSequence channelName = getString(R.string.default_notification_channel_id);
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(channelId, channelName, importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(R.color.main_icon_colour);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            notificationManager.createNotificationChannel(notificationChannel);

            Notification notification = new Notification.Builder(this)
                    .setSmallIcon(R.drawable.notification_small_icon)
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.notification_big))
                    .setContentTitle(title)
                    .setContentText(message)
                    .setAutoCancel(true)
                    .setColor(ContextCompat.getColor(MyAndroidFirebaseMsgService.this, R.color.main_icon_colour))
                    .setSound(notificationSoundURI)
                    .setStyle(new Notification.BigTextStyle().bigText(message))
                    .setContentIntent(contentIntent)
                    .setGroupSummary(true)
                    .setGroup("KEY_NOTIFICATION_GROUP")
                    .setChannelId(channelId)
                    .build();

            notificationManager.notify(m, notification);
        }
        else
        {
            NotificationCompat.Builder mNotificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.notification_small_icon)
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.notification_big))
                    .setContentTitle(title)
                    .setContentText(message)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setAutoCancel(true)
                    .setColor(ContextCompat.getColor(MyAndroidFirebaseMsgService.this, R.color.main_icon_colour))
                    .setSound(notificationSoundURI)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                    .setContentIntent(contentIntent)
                    .setGroupSummary(true)
                    .setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400})
                    .setGroup("KEY_NOTIFICATION_GROUP");

            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
            notificationManager.notify(m, mNotificationBuilder.build());
        }
//            NotificationCompat.Builder mNotificationBuilder = new NotificationCompat.Builder(this)
//                .setSmallIcon(R.drawable.ic_push_icon)
//                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.app_icon))
//                .setContentTitle(title)
//                .setContentText(message)
//                .setDefaults(Notification.DEFAULT_ALL)
//                .setPriority(NotificationCompat.PRIORITY_HIGH)
//                .setAutoCancel(true)
//                .setColor(ContextCompat.getColor(MyAndroidFirebaseMsgService.this, R.color.main_icon_colour))
//                .setSound(notificationSoundURI)
//                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
//                .setContentIntent(contentIntent)
//                .setGroupSummary(true)
//                .setGroup("KEY_NOTIFICATION_GROUP");

//        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
//        notificationManager.notify(m, mNotificationBuilder.build());
    }
}
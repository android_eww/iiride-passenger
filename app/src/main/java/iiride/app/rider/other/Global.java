package iiride.app.rider.other;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.text.SimpleDateFormat;
import java.util.Date;


public class Global
{
	public static Context context;

	public static boolean isNetworkconn(Context ctx)
	{
		context = ctx;

		ConnectivityManager connectivity = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity != null)
		{
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null)
				for (int i = 0; i < info.length; i++)
					if (info[i].getState() == NetworkInfo.State.CONNECTED)
					{
						return true;
					}

		}
		return false;
	}

	public static String getDateInFormat(String dateStr)
	{
		try {
			SimpleDateFormat input = new SimpleDateFormat("hh:mm a dd/MM/yyyy");
			Date dateObj = input.parse(dateStr);
			SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a dd MMMM yyyy");
			return sdf.format(dateObj);
		}
		catch (Exception e)
		{
			return "";
		}
	}
}

package iiride.app.rider.view;

import android.content.Context;
import com.google.android.material.snackbar.Snackbar;


import androidx.core.content.ContextCompat;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import iiride.app.rider.R;


/**
 * Created by ADMIN on 11/9/2017.
 */

public class MySnackBar {

    private Context context;
    private Snackbar snackbar;

    public MySnackBar(Context context)
    {
        this.context=context;
    }

    //for linear layout
    public void showSnackBar(LinearLayout ll_RootView, String message)
    {
        snackbar = Snackbar
                .make(ll_RootView, message, Snackbar.LENGTH_LONG)
                .setAction("Okay", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    }
                });
        snackbar.setActionTextColor(context.getResources().getColor(R.color.colorWhite));
        View sbView = snackbar.getView();
        FrameLayout.LayoutParams params =(FrameLayout.LayoutParams)sbView.getLayoutParams();
        params.gravity = Gravity.CENTER;
        params.setMargins(20,0,20,0);
        sbView.setLayoutParams(params);
        TextView textView = (TextView) sbView.findViewById(R.id.snackbar_text);
        textView.setTextColor(context.getResources().getColor(R.color.colorWhite));
        sbView.setBackgroundColor(ContextCompat.getColor(context, R.color.colorRed));
        snackbar.show();
    }

    //for linear layout
    public void showSnackBar(LinearLayout ll_RootView, String message, int flag)
    {
        snackbar = Snackbar
                .make(ll_RootView, message, Snackbar.LENGTH_LONG)
                .setAction("Okay", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    }
                });
        snackbar.setActionTextColor(context.getResources().getColor(R.color.colorWhite));
        View sbView = snackbar.getView();
        FrameLayout.LayoutParams params =(FrameLayout.LayoutParams)sbView.getLayoutParams();
        params.gravity = Gravity.CENTER;
        params.setMargins(20,0,20,0);
        sbView.setLayoutParams(params);
        TextView textView = (TextView) sbView.findViewById(R.id.snackbar_text);
        textView.setTextColor(context.getResources().getColor(R.color.colorWhite));
        sbView.setBackgroundColor(ContextCompat.getColor(context, R.color.colorRed));
        snackbar.show();
    }
}

package iiride.app.rider.view;

import android.content.Context;
import androidx.appcompat.widget.AppCompatSpinner;
import android.util.AttributeSet;

/**
 * Created by ADMIN on 11/30/2017.
 */

public class CustomSpinner extends AppCompatSpinner {
    Context context=null;

    public CustomSpinner(Context context) {
        super(context);
        this.context=context;
    }

    public CustomSpinner(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public CustomSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }
}
